﻿using BusinessLayer.DomainValidation.Expression;
using DomainModel;
using DomainValidation.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.DomainValidation
{
    public class IsInvoiceDetailsValid : Validator<InvoiceDetail>
    {
        public IsInvoiceDetailsValid()
        {
            Add("ModifiedUserIsNotNullOrWhiteSpace", new Rule<InvoiceDetail>(new IsNotNull<InvoiceDetail>(x => x.ModifiedUser), "ModifiedUser field is missing"));
            Add("ModifiedDateIsNotNullOrWhiteSpace", new Rule<InvoiceDetail>(new IsNotNull<InvoiceDetail>(x => x.ModifiedDate), "ModifiedDate field is missing"));
            Add("RecordUserIsNotNullOrWhiteSpace", new Rule<InvoiceDetail>(new IsNotNull<InvoiceDetail>(x => x.RecordUser), "RecordBy field is missing"));
            Add("RecordDateIsNotNullOrWhiteSpace", new Rule<InvoiceDetail>(new IsNotNull<InvoiceDetail>(x => x.RecordDate), "RecordDate field is missing"));
            Add("ConditionIDIsNotNullOrWhiteSpace", new Rule<InvoiceDetail>(new IsNotNull<InvoiceDetail>(x => x.ConditionID), "ConditionID field is missing"));
            Add("InvoiceNoIsNotNullOrWhiteSpace", new Rule<InvoiceDetail>(new IsNotNull<InvoiceDetail>(x => x.InvoiceNo), "InvoiceNo field is missing"));
            Add("QuantityIsNotNullOrWhiteSpace", new Rule<InvoiceDetail>(new IsNotNull<InvoiceDetail>(x => x.Quantity), "Quantity field is missing"));
            Add("UnitPriceIsNotNullOrWhiteSpace", new Rule<InvoiceDetail>(new IsNotNull<InvoiceDetail>(x => x.UnitPrice), "UnitPrice field is missing"));

            Add("QuantityValid", new Rule<InvoiceDetail>(new IsExpressionValid<InvoiceDetail>(x => x.Quantity > 0), "Quantity จะต้องมากกว่า 0"));
        }
    }
}
