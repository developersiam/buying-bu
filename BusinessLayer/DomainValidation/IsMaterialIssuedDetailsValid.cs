﻿using BusinessLayer.DomainValidation.Expression;
using DomainModel;
using DomainValidation.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.DomainValidation
{
    public class IsMaterialIssuedDetailsValid : Validator<MaterialIssuedDetail>
    {
        public IsMaterialIssuedDetailsValid()
        {
            Add("ModifiedUserIsNotNullOrWhiteSpace", new Rule<MaterialIssuedDetail>(new IsNotNull<MaterialIssuedDetail>(x => x.ModifiedUser), "ModifiedUser field is missing"));
            Add("ModifiedDateIsNotNullOrWhiteSpace", new Rule<MaterialIssuedDetail>(new IsNotNull<MaterialIssuedDetail>(x => x.ModifiedDate), "ModifiedDate field is missing"));
            Add("RecordUserIsNotNullOrWhiteSpace", new Rule<MaterialIssuedDetail>(new IsNotNull<MaterialIssuedDetail>(x => x.RecordUser), "RecordBy field is missing"));
            Add("RecordDateIsNotNullOrWhiteSpace", new Rule<MaterialIssuedDetail>(new IsNotNull<MaterialIssuedDetail>(x => x.RecordDate), "RecordDate field is missing"));
            Add("ItemIDIsNotNullOrWhiteSpace", new Rule<MaterialIssuedDetail>(new IsNotNull<MaterialIssuedDetail>(x => x.ItemID), "ItemID field is missing"));
            Add("InvoiceNoIsNotNullOrWhiteSpace", new Rule<MaterialIssuedDetail>(new IsNotNull<MaterialIssuedDetail>(x => x.IssuedCode), "InvoiceNo field is missing"));
            Add("QuantityIsNotNullOrWhiteSpace", new Rule<MaterialIssuedDetail>(new IsNotNull<MaterialIssuedDetail>(x => x.Quantity), "Quantity field is missing"));
            Add("UnitPriceIsNotNullOrWhiteSpace", new Rule<MaterialIssuedDetail>(new IsNotNull<MaterialIssuedDetail>(x => x.UnitPrice), "UnitPrice field is missing"));

            Add("QuantityValid", new Rule<MaterialIssuedDetail>(new IsExpressionValid<MaterialIssuedDetail>(x => x.Quantity > 0), "Quantity จะต้องมากกว่า 0"));
        }
    }
}
