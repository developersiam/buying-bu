﻿using BusinessLayer.DomainValidation.Expression;
using DomainModel;
using DomainValidation.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.DomainValidation
{
    public class IsCropInputDistributionValid : Validator<CropInputDistribution>
    {
        public IsCropInputDistributionValid()
        {

            Add("CropIsNotNullOrWhiteSpace", new Rule<CropInputDistribution>(new IsNotNull<CropInputDistribution>(x => x.Crop), "Crop field is missing"));
            Add("FarmerCodeIsNotNullOrWhiteSpace", new Rule<CropInputDistribution>(new IsNotNull<CropInputDistribution>(x => x.FarmerCode), "FarmerCode field is missing"));
            Add("CropInputItemIDIsNotNullOrWhiteSpace", new Rule<CropInputDistribution>(new IsNotNull<CropInputDistribution>(x => x.CropInputItemID), "CropInputItemID field is missing"));
            Add("DistributionDateIsNotNullOrWhiteSpace", new Rule<CropInputDistribution>(new IsNotNull<CropInputDistribution>(x => x.DistributionDate), "DistributionDate field is missing"));
            Add("DistributionUserIsNotNullOrWhiteSpace", new Rule<CropInputDistribution>(new IsNotNull<CropInputDistribution>(x => x.DistributionUser), "DistributionUser field is missing"));
            Add("QuantityIsNotNullOrWhiteSpace", new Rule<CropInputDistribution>(new IsNotNull<CropInputDistribution>(x => x.Quantity), "Quantity field is missing"));
            Add("ModifiedUserIsNotNullOrWhiteSpace", new Rule<CropInputDistribution>(new IsNotNull<CropInputDistribution>(x => x.ModifiedUser), "ModifiedUser field is missing"));

            Add("QuantityValid", new Rule<CropInputDistribution>(new IsExpressionValid<CropInputDistribution>(x => x.Quantity > 0), "จำนวนที่จ่ายจะต้องมากกว่า 0"));
        }
    }
}
