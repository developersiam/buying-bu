﻿using BusinessLayer.DomainValidation.Expression;
using DomainModel;
using DomainValidation.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.DomainValidation
{
    public class IsHessianDistributionValid : Validator<HessianDistribution>
    {
        public IsHessianDistributionValid()
        {
            Add("ModifiedByIsNotNullOrWhiteSpace",new Rule<HessianDistribution>(new IsNotNull<HessianDistribution>(x => x.ModifiedBy), "ModifiedBy field is missing"));
            Add("ModifiedDateIsNotNullOrWhiteSpace", new Rule<HessianDistribution>(new IsNotNull<HessianDistribution>(x => x.ModifiedDate), "ModifiedDate field is missing"));
            Add("DistributedByIsNotNullOrWhiteSpace", new Rule<HessianDistribution>(new IsNotNull<HessianDistribution>(x => x.DistributedBy), "DistributedBy field is missing"));
            Add("DistributedDateIsNotNullOrWhiteSpace", new Rule<HessianDistribution>(new IsNotNull<HessianDistribution>(x => x.DistributedDate), "DistributedDate field is missing"));
            Add("CropIsNotNullOrWhiteSpace", new Rule<HessianDistribution>(new IsNotNull<HessianDistribution>(x => x.Crop), "Crop field is missing"));
            Add("FarmerCodeIsNotNullOrWhiteSpace", new Rule<HessianDistribution>(new IsNotNull<HessianDistribution>(x => x.FarmerCode), "FarmerCode field is missing"));
            Add("StationCodeIsNotNullOrWhiteSpace", new Rule<HessianDistribution>(new IsNotNull<HessianDistribution>(x => x.BuyingStationCode), "BuyingStationCode field is missing"));
            Add("DocumentNumberIsNotNullOrWhiteSpace", new Rule<HessianDistribution>(new IsNotNull<HessianDistribution>(x => x.BuyingDocumentNumber), "BuyingDocumentNumber field is missing"));
            Add("QuantityIsNotNullOrWhiteSpace", new Rule<HessianDistribution>(new IsNotNull<HessianDistribution>(x => x.Quantity), "Quantity field is missing"));
            Add("ReceivingCodeIsNotNullOrWhiteSpace", new Rule<HessianDistribution>(new IsNotNull<HessianDistribution>(x => x.ReceivingCode), "ReceivingCode field is missing"));

            Add("QuantityValid", new Rule<HessianDistribution>(new IsExpressionValid<HessianDistribution>(x => x.Quantity > 0), "จำนวนกระสอบที่จ่ายจะต้องมากกว่า 0"));
            Add("DistributedDateValid", new Rule<HessianDistribution>(new IsExpressionValid<HessianDistribution>(x => x.DistributedDate.Date <= DateTime.Now.Date), "ไม่สามารถบันทึกวันที่จ่ายกระสอบล่วงหน้าได้"));
        }
    }
}
