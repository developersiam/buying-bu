﻿using AGMInventorySystemBL.DomainValidation.Expression;
using DomainModel;
using DomainValidation.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.DomainValidation
{
    public class IsCropInputsPackageDetailsValid : Validator<CropInputsPackageDetail>
    {
        public IsCropInputsPackageDetailsValid()
        {
            Add("ConditionIDIsNotNullOrWhiteSpace", new Rule<CropInputsPackageDetail>(new IsNotNull<CropInputsPackageDetail>(x => x.ConditionID), "ConditionID field is missing"));
            Add("PackageCodeIsNotNullOrWhiteSpace", new Rule<CropInputsPackageDetail>(new IsNotNull<CropInputsPackageDetail>(x => x.PackageCode), "InvoiceNo field is missing"));
            Add("QuantityIsNotNullOrWhiteSpace", new Rule<CropInputsPackageDetail>(new IsNotNull<CropInputsPackageDetail>(x => x.Quantity), "Quantity field is missing"));
            Add("ModifiedUserIsNotNullOrWhiteSpace", new Rule<CropInputsPackageDetail>(new IsNotNull<CropInputsPackageDetail>(x => x.ModifiedUser), "ModifiedUser field is missing"));
            Add("ModifiedDateIsNotNullOrWhiteSpace", new Rule<CropInputsPackageDetail>(new IsNotNull<CropInputsPackageDetail>(x => x.ModifiedDate), "ModifiedDate field is missing"));

            Add("QuantityValid", new Rule<CropInputsPackageDetail>(new IsExpressionValid<CropInputsPackageDetail>(x => x.Quantity > 0), "Quantity จะต้องมากกว่า 0"));
        }
    }
}
