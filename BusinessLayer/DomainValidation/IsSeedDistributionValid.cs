﻿using BusinessLayer.DomainValidation.Expression;
using DomainModel;
using DomainValidation.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.DomainValidation
{
    public class IsSeedDistributionValid : Validator<SeedDistribution>
    {
        public IsSeedDistributionValid()
        {
            Add("FarmerCodeIsNotNullOrWhiteSpace", new Rule<SeedDistribution>(new IsNotNull<SeedDistribution>(x => x.FarmerCode), "ไม่ได้ระบุ Farmer Code"));
            Add("SeedForDistributionIDIsNotNullOrWhiteSpace", new Rule<SeedDistribution>(new IsNotNull<SeedDistribution>(x => x.SeedForDistributionID), "ไม่ได้ระบุรายการเมล็ดพันธุ์"));
            Add("QuantityIsNotNullOrWhiteSpace", new Rule<SeedDistribution>(new IsNotNull<SeedDistribution>(x => x.Quantity), "ไม่ได้ระบุจำนวนที่จะจ่าย"));
            Add("DistributionDateIsNotNullOrWhiteSpace", new Rule<SeedDistribution>(new IsNotNull<SeedDistribution>(x => x.DistributionDate), "ไม่ได้ระบุวันที่ที่จ่ายเมล็ดพันธุ์"));
            Add("DistributionUserIsNotNullOrWhiteSpace", new Rule<SeedDistribution>(new IsNotNull<SeedDistribution>(x => x.DistributionUser), "ไม่ได้ระบุชื่อผู้บันทึกข้อมูล"));
            Add("ModifiedUserIsNotNullOrWhiteSpace", new Rule<SeedDistribution>(new IsNotNull<SeedDistribution>(x => x.ModifiedUser), "ไม่ได้ระบุชื่อผู้บันทึกข้อมูล"));

            Add("QuantityValid", new Rule<SeedDistribution>(new IsExpressionValid<SeedDistribution>(x => x.Quantity > 0), "จำนวนเมล็ดพันธุ์ที่จ่ายให้กับชาวไร่จะต้องมากกว่า 0"));
        }
    }
}
