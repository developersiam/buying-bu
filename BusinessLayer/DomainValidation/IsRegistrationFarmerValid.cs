﻿using BusinessLayer.DomainValidation.Expression;
using DomainModel;
using DomainValidation.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.DomainValidation
{
    public class IsRegistrationFarmerValid : Validator<RegistrationFarmer>
    {
        public IsRegistrationFarmerValid()
        {
            Add("CropIsNotNullOrWhiteSpace", new Rule<RegistrationFarmer>(new IsNotNull<RegistrationFarmer>(x => x.Crop), "ไม่ได้ระบุ Crop"));
            Add("FarmerCodeIsNotNullOrWhiteSpace", new Rule<RegistrationFarmer>(new IsNotNull<RegistrationFarmer>(x => x.FarmerCode), "ไม่ได้ระบุ Farmer Code"));
            Add("RegistrationDateIsNotNullOrWhiteSpace", new Rule<RegistrationFarmer>(new IsNotNull<RegistrationFarmer>(x => x.RegistrationDate), "ไม่ได้ระบุวันที่กรอกใบสมัครเข้าร่วมโครงการ"));
            Add("QuotaFromFarmerRequestIsNotNullOrWhiteSpace", new Rule<RegistrationFarmer>(new IsNotNull<RegistrationFarmer>(x => x.QuotaFromFarmerRequest), "ไม่ได้ระบุจำนวนโควต้าที่ชาวไร่ต้องการ"));
            Add("LastModifiedIsNotNullOrWhiteSpace", new Rule<RegistrationFarmer>(new IsNotNull<RegistrationFarmer>(x => x.LastModified), "ไม่ได้ระบุเวลาที่บันทึกข้อมูล"));
            Add("ModifiedByUserIsNotNullOrWhiteSpace", new Rule<RegistrationFarmer>(new IsNotNull<RegistrationFarmer>(x => x.ModifiedByUser), "ไม่ได้ระบุชื่อผู้บันทึกข้อมูล"));

            Add("QuotaFromSignContractValid", new Rule<RegistrationFarmer>(new IsExpressionValid<RegistrationFarmer>(x => x.QuotaFromFarmerRequest > 0), "โควต้าที่จะบันทึกในสัญญาจะต้องมากกว่า 0"));
        }
    }
}
