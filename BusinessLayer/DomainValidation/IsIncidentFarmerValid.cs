﻿using BusinessLayer.DomainValidation.Expression;
using DomainModel;
using DomainValidation.Validation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.DomainValidation
{
   public  class IsIncidentFarmerValid : Validator<IncidentFarmer>
    {
        public IsIncidentFarmerValid()
        {
            Add("ModifiedByIsNotNullOrWhiteSpace", new Rule<IncidentFarmer>(new IsNotNull<IncidentFarmer>(x => x.ModifiedBy), "ModifiedBy field is missing"));
            Add("ModifiedDateIsNotNullOrWhiteSpace", new Rule<IncidentFarmer>(new IsNotNull<IncidentFarmer>(x => x.ModifiedDate), "ModifiedDate field is missing"));
            Add("RecordByIsNotNullOrWhiteSpace", new Rule<IncidentFarmer>(new IsNotNull<IncidentFarmer>(x => x.RecordBy), "RecordBy field is missing"));
            Add("RecordDateIsNotNullOrWhiteSpace", new Rule<IncidentFarmer>(new IsNotNull<IncidentFarmer>(x => x.RecordDate), "RecordDate field is missing"));
            Add("CropIsNotNullOrWhiteSpace", new Rule<IncidentFarmer>(new IsNotNull<IncidentFarmer>(x => x.Crop), "Crop field is missing"));
            Add("CitizenIDIsNotNullOrWhiteSpace", new Rule<IncidentFarmer>(new IsNotNull<IncidentFarmer>(x => x.CitizenID), "CitizenID field is missing"));
            Add("DescriptionIDIsNotNullOrWhiteSpace", new Rule<IncidentFarmer>(new IsNotNull<IncidentFarmer>(x => x.DescriptionID), "DescriptionID field is missing"));
            Add("IncidentDateIsNotNullOrWhiteSpace", new Rule<IncidentFarmer>(new IsNotNull<IncidentFarmer>(x => x.IncidentDate), "IncidentDate field is missing"));
            Add("IncidentStatusIsNotNullOrWhiteSpace", new Rule<IncidentFarmer>(new IsNotNull<IncidentFarmer>(x => x.IncidentStatus), "IncidentStatus field is missing"));
        }
    }
}
