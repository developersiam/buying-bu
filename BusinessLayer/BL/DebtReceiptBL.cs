﻿using DataAccessLayer;
using DomainModel;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IDebtReceiptBL
    {
        void Add(short crop, string stationCode, string farmerCode, short documentNumber,
            string receiptTypeCode, decimal amount, DateTime receiptDate, string receiptBy, string debtSetupCode);
        void Delete(string receiptCode);
        DebtReceipt GetSingle(string receiptCode);
        List<DebtReceipt> GetByDebtSetupCode(string debtSetupCode);
        List<DebtReceipt> GetByFarmer(string farmerCode);
        List<DebtReceipt> GetFarmerDripProjectBy3YearContinue(short currentCrop, string farmerCode);
        List<DebtReceipt> GetFarmerDripProjectByDeductionCrop(short deductionCrop, string farmerCode);
        List<DebtReceipt> GetByFarmerDeductionCrop(short deductionCrop, string farmerCode);
        List<DebtReceipt> GetByBuyingDocument(short crop, string stationCode, string farmerCode, short documentNumber);
    }

    public class DebtReceiptBL : IDebtReceiptBL
    {
        UnitOfWork uow;
        public DebtReceiptBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(short crop, string stationCode, string farmerCode, short documentNumber,
            string receiptTypeCode, decimal amount, DateTime receiptDate, string receiptBy, string debtSetupCode)
        {
            try
            {
                if (amount <= 0)
                    throw new ArgumentException("ยอดชำระจะต้องมากกว่า 0 บาท");

                // crop and farmer code use for a other added function e.g.receipt from cash payment.
                // station code and document number use for added receipt function from buying.
                var debtSetup = uow.DebtSetupRepository
                    .GetSingle(x => x.DebtSetupCode == debtSetupCode);
                if (debtSetup == null)
                    throw new ArgumentException("ไม่พบรหัสการตั้งหนี้ (Debt setup code) #" + debtSetupCode + " นี้ในระบบ");

                var receiptList = uow.DebtReceiptRepository
                    .Get(x => x.DebtSetup.DeductionCrop == crop &&
                    x.DebtSetup.FarmerCode == farmerCode &&
                    x.DebtSetupCode == debtSetupCode)
                    .ToList();

                var receiptAmount = Math.Round(receiptList.Sum(x => x.Amount), 2);
                var debtAmount = Math.Round(debtSetup.Amount, 2);
                var totalReceipt = Math.Round(receiptList.Sum(x => x.Amount) + amount,2);
                if (receiptAmount >= debtAmount)
                    throw new ArgumentException("มีการชำระครบตามจำนวนแล้ว");

                if (totalReceipt > debtAmount)
                    throw new ArgumentException("ยอดที่จะชำระเกินกว่ายอดหนี้ที่ตั้งไว้ " + Environment.NewLine +
                        "ยอดหนี้ที่ตั้งไว้ " + debtAmount + " บาท" + Environment.NewLine +
                        "ยอดที่ชำระไว้ก่อนหน้า " + receiptAmount + " บาท" + Environment.NewLine +
                        "ยอดคงเหลือที่ต้องชำระ " + (debtAmount - receiptAmount) + " บาท" + Environment.NewLine +
                        "ยอดที่จะหักครั้งนี้ " + amount + " บาท" + Environment.NewLine +
                        "เกินมา " + (debtAmount - totalReceipt) + " บาท");

                // RB : this code from "Receipt From Buying".
                // RC : this code from "Receipt From Cash"
                if (receiptTypeCode == "RB")
                {
                    var doc = BuyingFacade.BuyingDocumentBL()
                        .GetSingle(crop, stationCode, farmerCode, documentNumber);
                    if (doc == null)
                        throw new ArgumentException("ไม่พบใบเวาเชอร์นี้ในระบบ ไม่สามารถบันทึกการชำระหนี้นี้ได้");

                    if (doc.IsPrinted)
                        throw new ArgumentException("ใบเวาเชอร์นี้ถูกพิมพ์ออกจากระบบแล้ว ไม่สามารถเปลี่ยนแปลงข้อมูลการหักชำระได้");

                    var buyingList = uow.BuyingRepository
                        .Get(x => x.Crop == doc.Crop &&
                        x.BuyingStationCode == doc.BuyingStationCode &&
                        x.FarmerCode == doc.FarmerCode &&
                        x.BuyingDocumentNumber == doc.BuyingDocumentNumber &&
                        x.Grade != null
                        , null
                        , x => x.BuyingGrade)
                        .ToList();

                    var receiptInVoucher = receiptList
                        .Where(x => x.Crop == doc.Crop &&
                        x.BuyingStationCode == doc.BuyingStationCode &&
                        x.FarmerCode == doc.FarmerCode &&
                        x.BuyingDocumentNumber == doc.BuyingDocumentNumber &&
                        x.DebtSetupCode == debtSetup.DebtSetupCode)
                        .ToList();

                    var deductInVoucherAmount = receiptInVoucher.Sum(x => x.Amount);
                    var buyingAmount = buyingList.Sum(x => x.Weight * x.BuyingGrade.UnitPrice);
                    if (amount > buyingAmount - deductInVoucherAmount)
                        throw new ArgumentException("เงินจากการขายไม่พอที่จะหักหนี้" + Environment.NewLine +
                            "===========================" + Environment.NewLine +
                            "ยอดเงินที่ได้จากการขายในรอบนี้ " + Convert.ToDecimal(buyingAmount).ToString("N1") + " บาท" + Environment.NewLine +
                            "หักเงินจากการขายไปแล้วในรอบนี้ " + Convert.ToDecimal(deductInVoucherAmount).ToString("N1") + " บาท" + Environment.NewLine +
                            "ยอดเงินสุทธิก่อนหัก " + Convert.ToDecimal(buyingAmount - deductInVoucherAmount).ToString("N1") + " บาท" + Environment.NewLine +
                            "ยอดเงินสุทธิหลังหัก " + Convert.ToDecimal(buyingAmount - deductInVoucherAmount - amount).ToString("N1") + " บาท");

                    if (receiptInVoucher.Count() > 0)
                        throw new ArgumentException("ยอดหนี้รหัส " + debtSetup.DebtSetupCode +
                            " ถูกหักจากการขายในใบเวาเชอร์นี้แล้ว ไม่สามารถทำซ้ำได้ " + Environment.NewLine +
                            "ดูข้อมูลการหักหนี้ได้จากตารางด้านล่าง " + Environment.NewLine +
                            "ReceiptCode: " + receiptInVoucher.FirstOrDefault().ReceiptCode);
                }

                var receiptCode = GenerateCode(crop, farmerCode);
                var receipt = new DebtReceipt
                {
                    ReceiptCode = receiptCode,
                    DebtSetupCode = debtSetupCode,
                    Amount = amount,
                    ReceiptTypeCode = receiptTypeCode,
                    ReceiptBy = receiptBy,
                    ReceiptDate = DateTime.Now,
                    ModifiedBy = receiptBy,
                    ModifiedDate = DateTime.Now
                };

                if (receipt.ReceiptCode != "RB")
                {
                    receipt.Crop = crop;
                    receipt.BuyingStationCode = stationCode;
                    receipt.FarmerCode = farmerCode;
                    receipt.BuyingDocumentNumber = documentNumber;
                }

                uow.DebtReceiptRepository.Add(receipt);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string receiptCode)
        {
            try
            {
                var receipt = GetSingle(receiptCode);
                if (receipt == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                if (DateTime.Now.Date > receipt.ReceiptDate.Date)
                    throw new Exception("หากเลยวันที่ทำรายการไปแล้วจะไม่สามารถลบข้อมูลได้ ท่านสามารถลบข้อมูลได้ภายในวันที่บันทึกรายการเท่านั้น");

                if (receipt.ReceiptTypeCode == "RB")
                {
                    var voucher = BuyingFacade.BuyingDocumentBL()
                        .GetSingle((short)receipt.Crop, receipt.BuyingStationCode,
                        receipt.FarmerCode, (short)receipt.BuyingDocumentNumber);
                    if (voucher == null)
                        throw new ArgumentException("ไม่พบ voucher อ้างอิงในระบบ กรณีลบข้อมูลการหักเงินจากการขาย");

                    if (voucher.IsAccountFinish == true)
                        throw new ArgumentException("voucher นี้ถูกล็อคแล้วจากแผนกบัญชี");

                    if (voucher.IsPrinted == true)
                        throw new ArgumentException("voucher นี้ถูกพิมพ์ออกจากระบบแล้วเมื่อ " +
                            voucher.PrintDate + " ไม่สามารถลบข้อมูลการหักหนี้นี้ได้ ท่านต้องยกเลิกปิดการขายก่อน");
                }

                uow.DebtReceiptRepository.Remove(receipt);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DebtReceipt> GetByBuyingDocument(short crop, string stationCode,
            string farmerCode, short documentNumber)
        {
            return uow.DebtReceiptRepository
                .Get(x => x.BuyingDocument.Crop == crop &&
                x.BuyingStationCode == stationCode &&
                x.FarmerCode == farmerCode &&
                x.BuyingDocumentNumber == documentNumber
                , null
                , x => x.BuyingDocument
                , x => x.DebtSetup
                , x => x.DebtSetup.DebtType);
        }

        public List<DebtReceipt> GetByDebtSetupCode(string debtSetupCode)
        {
            return uow.DebtReceiptRepository
                .Get(x => x.DebtSetupCode == debtSetupCode
                , null
                , x => x.DebtSetup
                , x => x.BuyingDocument);
        }

        public List<DebtReceipt> GetByFarmer(string farmerCode)
        {
            return uow.DebtReceiptRepository
                .Get(x => x.DebtSetup.FarmerCode == farmerCode
                , null
                , x => x.DebtSetup);
        }

        public List<DebtReceipt> GetByFarmerDeductionCrop(short deductionCrop, string farmerCode)
        {
            return uow.DebtReceiptRepository
                .Get(x => x.DebtSetup.DeductionCrop == deductionCrop &&
                x.DebtSetup.FarmerCode == farmerCode
                , null
                , x => x.DebtSetup);
        }

        public List<DebtReceipt> GetFarmerDripProjectBy3YearContinue(short currentCrop, string farmerCode)
        {
            return uow.DebtReceiptRepository
                .Get(x => x.DebtSetup.DebtTypeCode == "DP" &&
                x.DebtSetup.DeductionCrop >= currentCrop &&
                x.DebtSetup.DeductionCrop <= x.DebtSetup.Crop + 2
                , null
                , x => x.DebtSetup);
        }

        public List<DebtReceipt> GetFarmerDripProjectByDeductionCrop(short deductionCrop, string farmerCode)
        {
            return uow.DebtReceiptRepository
                .Get(x => x.DebtSetup.DeductionCrop == deductionCrop &&
                x.DebtSetup.FarmerCode == farmerCode &&
                x.DebtSetup.DebtTypeCode == "DP"
                , null
                , x => x.DebtSetup);
        }

        public DebtReceipt GetSingle(string receiptCode)
        {
            return uow.DebtReceiptRepository
                .GetSingle(x => x.ReceiptCode == receiptCode
                , x => x.DebtSetup);
        }

        private string GenerateCode(short crop, string farmerCode)
        {
            try
            {
                var debtReceiptList = uow.DebtReceiptRepository
                                .Get(x => x.Crop == crop &&
                                x.DebtSetup.FarmerCode == farmerCode)
                                .ToList();
                var max = 0;
                if (debtReceiptList.Count() > 0)
                    max = debtReceiptList
                       .Max(x =>
                       Convert.ToInt16(x.ReceiptCode
                           .Substring(x.ReceiptCode.Length - 2, 2)));

                max = max + 1;

                var cropText = crop.ToString();
                var receiptCode = "RC" +
                    cropText.Substring(2, 2) +
                    "-" + farmerCode +
                    "-" +
                    max.ToString().PadLeft(2, '0');
                return receiptCode;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
