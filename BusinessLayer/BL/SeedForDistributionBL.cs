﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface ISeedForDistributionBL
    {
        void Add(SeedForDistribution model);
        void Update(SeedForDistribution model);
        void Delete(SeedForDistribution model);
        SeedForDistribution GetByID(Guid Id);
        List<SeedForDistribution> GetByCrop(short crop);
    }

    public class SeedForDistributionBL : ISeedForDistributionBL
    {
        UnitOfWork uow;
        public SeedForDistributionBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(SeedForDistribution model)
        {
            try
            {
                if (model.UnitPrice < 0)
                    throw new ArgumentException("UnitPrice must more than zero.");

                if (model.Unit == null)
                    throw new ArgumentException("Unit not defind.");

                if (model.SeedCode == null)
                    throw new ArgumentException("SeedCode not defind.");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("Modified User not defind.");

                model.ID = Guid.NewGuid();
                model.ModifiedDate = DateTime.Now;

                uow.SeedForDistributionRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(SeedForDistribution model)
        {
            try
            {
                if (model.Unit == null)
                    throw new ArgumentException("Unit not defind.");

                if (model.SeedCode == null)
                    throw new ArgumentException("SeedCode not defind.");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("Modified User not defind.");

                var seedForDistribution = GetByID(model.ID);

                if (seedForDistribution == null)
                    throw new ArgumentException("Find not found!");

                seedForDistribution.UnitPrice = model.UnitPrice;
                seedForDistribution.Unit = model.Unit;
                seedForDistribution.ModifiedDate = DateTime.Now;
                seedForDistribution.ModifiedUser = model.ModifiedUser;

                uow.SeedForDistributionRepository.Update(seedForDistribution);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(SeedForDistribution model)
        {
            try
            {
                var seedForDistribution = GetByID(model.ID);
                if (seedForDistribution == null)
                    throw new ArgumentException("Find not found!");

                uow.SeedForDistributionRepository.Remove(seedForDistribution);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SeedForDistribution GetByID(Guid Id)
        {
            var seedForDistribution = uow.SeedForDistributionRepository
                .GetSingle(s => s.ID == Id, s => s.SeedDistributionType, s => s.SeedVariety);

            return seedForDistribution;
        }

        public List<SeedForDistribution> GetByCrop(short crop)
        {
            return uow.SeedForDistributionRepository
                .Query(s => s.Crop == crop,
                null,
                s => s.SeedDistributionType,
                s => s.SeedVariety)
                .ToList();
        }
    }
}
