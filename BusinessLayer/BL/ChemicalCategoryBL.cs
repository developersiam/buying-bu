﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
   public interface IChemicalCategoryBL
    {
        List<ChemicalCategory> GetAll();
    }

    public class ChemicalCategoryBL : IChemicalCategoryBL
    {
        UnitOfWork uow;
        public ChemicalCategoryBL()
        {
            uow = new UnitOfWork();
        }
        public List<ChemicalCategory> GetAll()
        {
            try
            {
                return uow.ChemicalCategoryRepository.Get().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
