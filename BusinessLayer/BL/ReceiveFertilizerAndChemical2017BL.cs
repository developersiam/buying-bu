﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IReceiveFertilizerAndChemicalBL<T> where T : class
    {
        void Add(T model);
        void Update(T model);
        void Delete(T model);
        List<T> GetByFarmer(short crop, string farmerCode);
        T GetById(T model);
    }

    public class ReceiveFertilizerAndChemical2017BL : IReceiveFertilizerAndChemicalBL<ReceiveFertilizerAndChemical>
    {
        UnitOfWork uow;
        public ReceiveFertilizerAndChemical2017BL()
        {
            uow = new UnitOfWork();
        }

        public void Add(ReceiveFertilizerAndChemical model)
        {
            try
            {
                if (model.Crop.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล Crop");

                if (model.FarmerCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล FarmerCode");

                if (model.FertilizerAndChemicalID == null)
                    throw new ArgumentException("ไม่พบข้อมูล FertilizerAndChemicalID");

                if (GetById(model) != null)
                    throw new ArgumentException("มีข้อมูลนี้ซ้ำในระบบ");

                uow.ReceiveFertilizerAndChemicalRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Update(ReceiveFertilizerAndChemical model)
        {
            try
            {
                if (model.Crop.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล Crop");

                if (model.FarmerCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล FarmerCode");

                if (model.FertilizerAndChemicalID == null)
                    throw new ArgumentException("ไม่พบข้อมูล FertilizerAndChemicalID");

                if (GetById(model) == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.ReceiveFertilizerAndChemicalRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(ReceiveFertilizerAndChemical model)
        {
            try
            {
                if (model.Crop.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล Crop");

                if (model.FarmerCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล FarmerCode");

                if (model.FertilizerAndChemicalID == null)
                    throw new ArgumentException("ไม่พบข้อมูล FertilizerAndChemicalID");

                if (GetById(model) == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.ReceiveFertilizerAndChemicalRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        List<ReceiveFertilizerAndChemical> IReceiveFertilizerAndChemicalBL<ReceiveFertilizerAndChemical>.GetByFarmer(short crop, string farmerCode)
        {
            return uow.ReceiveFertilizerAndChemicalRepository
                .Query(rf => rf.Crop == crop && 
                rf.FarmerCode == farmerCode,
                null,
                rf => rf.FertilizerAndChemical,
                rf => rf.FertilizerAndChemical.FertilizerAndChemicalUnit,
                rf => rf.FertilizerAndChemical.FertilizerAndChemicalType)
                .ToList();
        }
        public ReceiveFertilizerAndChemical GetById(ReceiveFertilizerAndChemical fertilizerAndChemical)
        {
            return uow.ReceiveFertilizerAndChemicalRepository
                .GetSingle(rf => rf.Crop == fertilizerAndChemical.Crop && 
                rf.FarmerCode == fertilizerAndChemical.FarmerCode &&
                rf.FertilizerAndChemicalID == fertilizerAndChemical.FertilizerAndChemicalID);
        }
    }
}
