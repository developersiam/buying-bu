﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IFarmerEstimateTransplantingPeriodBL
    {
        List<FarmerEstimateTransplantingPeriod> GetByCitizenID(short crop, string citizenId);
        FarmerEstimateTransplantingPeriod GetSingle(short crop, string citizenId, Guid estimateTransplantingPeriodID);
        void Add(FarmerEstimateTransplantingPeriod model);
        void Delete(short crop, string citizenId, Guid estimateTransplantingPeriodID);
    }
    public class FarmerEstimateTransplantingPeriodBL : IFarmerEstimateTransplantingPeriodBL
    {
        UnitOfWork uow;
        public FarmerEstimateTransplantingPeriodBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(FarmerEstimateTransplantingPeriod model)
        {
            try
            {
                if (model.CitizenID == "")
                    throw new ArgumentException("โปรดระบุรหัสประจำตัวประชาชน");

                if (model.EstimateTransplantingPeriodID == null)
                    throw new ArgumentException("โปรดระบุช่วงเวลาที่คาดว่าจะปลูกต้นยาสูบ");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("โปรดระบุผู้บันทึกข้อมูล");

                var list = BuyingFacade.FarmerEstimateTransplantingPeriodBL()
                    .GetByCitizenID(model.Crop, model.CitizenID);

                if (list.Where(x => x.EstimateTransplantingPeriodID == model.EstimateTransplantingPeriodID)
                    .Count() > 0)
                    throw new ArgumentException("มีการบันทึกข้อมูลนี้ในระบบแล้ว");

                if (BuyingFacade.FarmerEstimateTransplantingPeriodBL()
                    .GetByCitizenID(model.Crop, model.CitizenID)
                    .Count() >= 1)
                    throw new ArgumentException("ท่านสามารถเลือกระบุช่วงเวลาที่คาดว่าจะปลูกต้นยาสูบได้เพียง 1 รายการเท่านั้น");

                uow.FarmerEstimateTransplantingPeriodRepository
                    .Add(new FarmerEstimateTransplantingPeriod
                    {
                        EstimateTransplantingPeriodID = model.EstimateTransplantingPeriodID,
                        CitizenID = model.CitizenID,
                        Crop = model.Crop,
                        ModifiedDate = DateTime.Now,
                        ModifiedUser = model.ModifiedUser
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(short crop, string citizenId, Guid estimateTransplantingPeriodID)
        {
            try
            {
                var model = GetSingle(crop, citizenId, estimateTransplantingPeriodID);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ!");

                uow.FarmerEstimateTransplantingPeriodRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FarmerEstimateTransplantingPeriod> GetByCitizenID(short crop, string citizenId)
        {
            try
            {
                return uow.FarmerEstimateTransplantingPeriodRepository
                    .Query(f => f.Crop == crop &&
                    f.CitizenID == citizenId,
                    null,
                    f => f.EstimateTransplantingPeriod)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public FarmerEstimateTransplantingPeriod GetSingle(short crop, string citizenId, Guid estimateTransplantingPeriodID)
        {
            try
            {
                return uow.FarmerEstimateTransplantingPeriodRepository
                    .GetSingle(f => f.Crop == crop &&
                    f.CitizenID == citizenId &&
                    f.EstimateTransplantingPeriodID == estimateTransplantingPeriodID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
