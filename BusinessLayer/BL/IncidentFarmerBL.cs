﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IIncidentFarmerBL
    {
        void Add(short crop, string cititzenID, Guid descriptionID,
            DateTime incidentDate, string incidentStatus, DateTime recordDate, string recordBy);
        void Update(Guid id, short crop, string citizenID, Guid descriptionID,
            DateTime incidentDate, string incidentStatus, DateTime? followUpDate, string modifiedUser);
        void Delete(Guid id);
        List<IncidentFarmer> GetByCrop(short crop);
        List<IncidentFarmer> GetByFarmer(short crop, string citizenID);
        List<IncidentFarmer> GetByCropAndDescription(short crop, Guid descriptionID);
        IncidentFarmer GetSingle(Guid id);
    }

    public class IncidentFarmerBL : IIncidentFarmerBL
    {
        UnitOfWork uow;

        public IncidentFarmerBL()
        {
            uow = new UnitOfWork();
        }
        public void Add(short crop, string cititzenID, Guid descriptionID,
            DateTime incidentDate, string incidentStatus, DateTime recordDate, string recordBy)
        {
            try
            {
                var model = uow.IncidentFarmerRepository
                    .Get(x => x.Crop == crop &&
                    x.CitizenID == cititzenID &&
                    x.DescriptionID == descriptionID,
                    null,
                    x => x.IncidentDescription);

                if (model.Where(x => x.IncidentDate == incidentDate).Count() > 0)
                    throw new ArgumentException("ข้อมูลซ้ำ มีการบันทึกข้อมูลนี้แล้วในระบบ" + Environment.NewLine +
                        "-> Desctiption: " + model.FirstOrDefault().IncidentDescription.Description + Environment.NewLine +
                        "-> Incident Date: " + incidentDate.ToShortDateString());

                if (incidentStatus != "Ongoing")
                    throw new ArgumentException("การบันทึกข้อมูลใหม่จะไม่สามารถเลือกสถานะเป็น Closed ได้");

                uow.IncidentFarmerRepository.Add(new IncidentFarmer
                {
                    ID = Guid.NewGuid(),
                    Crop = crop,
                    CitizenID = cititzenID,
                    DescriptionID = descriptionID,
                    IncidentDate = incidentDate,
                    IncidentStatus = incidentStatus,
                    FollowUpDate = null,
                    RecordBy = recordBy,
                    RecordDate = recordDate,
                    ModifiedBy = recordBy,
                    ModifiedDate = recordDate
                });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var model = GetSingle(id);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.IncidentFarmerRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<IncidentFarmer> GetByCrop(short crop)
        {
            return uow.IncidentFarmerRepository
                .Get(x => x.Crop == crop,
                null,
                x => x.Person,
                x => x.IncidentDescription,
                x => x.IncidentDescription.IncidentTopic,
                x => x.IncidentDescription.IncidentTopic.IncidentCategory)
                .ToList();
        }

        public List<IncidentFarmer> GetByCropAndDescription(short crop, Guid descriptionID)
        {
            try
            {
                return uow.IncidentFarmerRepository
                    .Get(x => x.Crop == crop &&
                    x.DescriptionID == descriptionID,
                    null,
                    x => x.Person,
                    x => x.IncidentDescription.IncidentTopic.IncidentCategory,
                    x => x.IncidentDescription.IncidentTopic,
                    x => x.IncidentDescription)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<IncidentFarmer> GetByFarmer(short crop, string citizenID)
        {
            return uow.IncidentFarmerRepository
                .Get(x => x.Crop == crop &&
                x.CitizenID == citizenID,
                null,
                x => x.IncidentDescription,
                x => x.IncidentDescription.IncidentTopic,
                x => x.IncidentDescription.IncidentTopic.IncidentCategory)
                .ToList();
        }

        public IncidentFarmer GetSingle(Guid id)
        {
            return uow.IncidentFarmerRepository
                    .GetSingle(x => x.ID == id,
                    x => x.Person,
                    x => x.IncidentDescription,
                    x => x.IncidentDescription.IncidentTopic,
                    x => x.IncidentDescription.IncidentTopic.IncidentCategory);
        }

        public void Update(Guid id, short crop, string citizenID, Guid descriptionID,
            DateTime incidentDate, string incidentStatus, DateTime? followUpDate, string modifiedUser)
        {
            try
            {
                var model = GetSingle(id);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (incidentStatus == "Ongoing" && followUpDate != null)
                    throw new ArgumentException("ไม่สามารถบันทึก Follow up date ได้เนื่องจากสถานะยังเป็น Ongoing");

                if (incidentStatus == "Closed" && followUpDate == null)
                    throw new ArgumentException("ถ้าสถานะเป็น Closed จะต้องระบุ Follow up date ด้วย");

                ///กรณีแก้ไขตัวเลือก description 
                ///ให้ตรวจสอบว่าไปซ้ำกับของเดิมที่มีอยู่แล้วในระบบหรือไม่ descriptionID และ date ต้องไม่ซ้ำ
                ///
                if (model.DescriptionID != descriptionID)
                {
                    var list = uow.IncidentFarmerRepository
                    .Get(x => x.Crop == crop &&
                    x.CitizenID == citizenID &&
                    x.DescriptionID == descriptionID,
                    null,
                    x => x.IncidentDescription)
                    .Where(x => x.IncidentDate.DayOfYear == incidentDate.DayOfYear);

                    if (list.Count() > 0)
                        throw new ArgumentException("ข้อมูลซ้ำ มีการบันทึกข้อมูลนี้แล้วในระบบ" + Environment.NewLine +
                            "-> Desctiption: " + list.FirstOrDefault().IncidentDescription.Description + Environment.NewLine +
                            "-> Incident Date: " + incidentDate.ToShortDateString());
                }

                model.DescriptionID = descriptionID;
                model.IncidentDate = incidentDate;
                model.IncidentStatus = incidentStatus;
                model.FollowUpDate = followUpDate;
                model.ModifiedBy = modifiedUser;
                model.ModifiedDate = DateTime.Now;

                uow.IncidentFarmerRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
