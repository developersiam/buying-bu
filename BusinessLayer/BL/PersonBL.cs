﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IPersonBL
    {
        void Add(Person person);
        void Update(Person person);
        Person GetSingle(string citizenID);
        List<Person> GetByName(string firstName);
    }

    public class PersonBL : IPersonBL
    {
        UnitOfWork uow;
        public PersonBL()
        {
            uow = new UnitOfWork();
        }

        public bool IsValidCitizenID(string citizenID)
        {
            //ตรวจสอบว่าทุก ๆ ตัวอักษรเป็นตัวเลข
            if (citizenID.ToCharArray().All(c => char.IsNumber(c)) == false) return false;

            //ตรวจสอบว่าข้อมูลมีทั้งหมด 13 ตัวอักษร
            if (citizenID.Trim().Length != 13) return false;

            int sumValue = 0;

            for (int i = 0; i < citizenID.Length - 1; i++)
                sumValue += int.Parse(citizenID[i].ToString()) * (13 - i);

            int v = 11 - (sumValue % 11);

            //return PID[12].ToString() == v.ToString();

            if (v.ToString().Length > 1)
            {
                if (v.ToString()[1] == citizenID[12])
                    return true;
                else
                    return false;
            }
            else
            {
                if (v.ToString() == citizenID[12].ToString())
                    return true;
                else
                    return false;
            }
        }

        public void Add(Person person)
        {
            try
            {
                if (person.CitizenID == null)
                    throw new ArgumentException("ไม่พบข้อมูล CitizenID");

                if (IsValidCitizenID(person.CitizenID) == false)
                    throw new ArgumentException("รูปแบบรหัสประจำตัวประชาชนไม่ถูกต้อง");

                if (person.Prefix == null)
                    throw new ArgumentException("ไม่พบข้อมูล Prefix");

                if (person.FirstName == null)
                    throw new ArgumentException("ไม่พบข้อมูล FirstName");

                if (person.LastName == null)
                    throw new ArgumentException("ไม่พบข้อมูล LastName");

                if (person.BirthDate == null)
                    throw new ArgumentException("ไม่พบข้อมูล BirthDate");

                if (person.HouseNumber == null)
                    throw new ArgumentException("ไม่พบข้อมูล HouseNumber");

                if (person.Village == null)
                    throw new ArgumentException("ไม่พบข้อมูล Village");

                if (person.Tumbon == null)
                    throw new ArgumentException("ไม่พบข้อมูล Tumbon");

                if (person.Amphur == null)
                    throw new ArgumentException("ไม่พบข้อมูล Amphur");

                if (person.Province == null)
                    throw new ArgumentException("ไม่พบข้อมูล Province");

                if (person.ModifiedByUser == null)
                    throw new ArgumentException("ไม่พบข้อมูล ModifiedByUser");

                if (person.StartAgriculture == null)
                    throw new ArgumentException("โปรดระบุปีที่เริ่มต้นทำการเกษตร");

                if (person.StartGrowingTobacco == null)
                    throw new ArgumentException("โปรดระบุปีที่เริ่มปลูกยาสูบ");

                if (uow.PersonRepository
                    .GetSingle(per => per.CitizenID == person.CitizenID) != null)
                    throw new ArgumentException("มีข้อมูลชาวไร่รหัสประจำตัวประชาชน " + person.CitizenID + " นี้แล้วในระบบ");

                var birthYear = person.BirthDate.Year;
                var currentYear = DateTime.Now.Year;

                if (currentYear - birthYear > 100)
                    throw new ArgumentException("อายุชาวไร่จะต้องไม่เกิน 100 ปี");

                if (currentYear - birthYear < 18)
                    throw new ArgumentException("อายุชาวไร่จะต้องไม่ต่ำกว่า 18 ปี");

                if (person.StartAgriculture < birthYear + 18 ||
                    person.StartAgriculture > currentYear)
                    throw new ArgumentException("ปีที่เริ่มทำการเกษตรจะต้องอยู่ในช่วงระหว่างปี " +
                        (birthYear + 18) + " ถึง " + currentYear);

                if (person.StartGrowingTobacco < person.YearsInAgriculture ||
                    person.StartGrowingTobacco > currentYear)
                    throw new ArgumentException("ปีที่เริ่มทำการเกษตรจะต้องอยู่ในช่วงระหว่างปี " +
                        person.StartAgriculture + " ถึง " + currentYear);

                person.LastModified = DateTime.Now;
                uow.PersonRepository.Add(person);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(Person person)
        {
            try
            {
                if (person.CitizenID == null)
                    throw new ArgumentException("ไม่พบข้อมูล CitizenID");

                if (person.Prefix == null)
                    throw new ArgumentException("ไม่พบข้อมูล Prefix");

                if (person.FirstName == null)
                    throw new ArgumentException("ไม่พบข้อมูล FirstName");

                if (person.LastName == null)
                    throw new ArgumentException("ไม่พบข้อมูล LastName");

                if (person.BirthDate == null)
                    throw new ArgumentException("ไม่พบข้อมูล BirthDate");

                if (person.HouseNumber == null)
                    throw new ArgumentException("ไม่พบข้อมูล HouseNumber");

                if (person.Village == null)
                    throw new ArgumentException("ไม่พบข้อมูล Village");

                if (person.Tumbon == null)
                    throw new ArgumentException("ไม่พบข้อมูล Tumbon");

                if (person.Amphur == null)
                    throw new ArgumentException("ไม่พบข้อมูล Amphur");

                if (person.Province == null)
                    throw new ArgumentException("ไม่พบข้อมูล Province");

                if (person.ModifiedByUser == null)
                    throw new ArgumentException("ไม่พบข้อมูล ModifiedByUser");

                if (person.StartAgriculture == null)
                    throw new ArgumentException("โปรดระบุปีที่เริ่มต้นทำการเกษตร");

                if (person.StartGrowingTobacco == null)
                    throw new ArgumentException("โปรดระบุปีที่เริ่มปลูกยาสูบ");

                var personEdit = uow.PersonRepository.GetSingle(per => per.CitizenID == person.CitizenID);
                if (personEdit == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                var birthYear = person.BirthDate.Year;
                var currentYear = DateTime.Now.Year;

                if (currentYear - birthYear > 100)
                    throw new ArgumentException("อายุชาวไร่จะต้องไม่เกิน 100 ปี");

                if (currentYear - birthYear < 18)
                    throw new ArgumentException("อายุชาวไร่จะต้องไม่ต่ำกว่า 18 ปี");

                if (person.StartAgriculture < birthYear + 18 ||
                    person.StartAgriculture > currentYear)
                    throw new ArgumentException("ปีที่เริ่มทำการเกษตรจะต้องอยู่ในช่วงระหว่างปี " + (birthYear + 18) +
                        " ถึง " + currentYear);

                if (person.StartGrowingTobacco < person.YearsInAgriculture ||
                    person.StartGrowingTobacco > currentYear)
                    throw new ArgumentException("ปีที่เริ่มทำการเกษตรจะต้องอยู่ในช่วงระหว่างปี " + person.StartAgriculture +
                        " ถึง " + currentYear);

                personEdit.Prefix = person.Prefix;
                personEdit.FirstName = person.FirstName;
                personEdit.LastName = person.LastName;
                personEdit.HouseNumber = person.HouseNumber;
                personEdit.Village = person.Village;
                personEdit.Tumbon = person.Tumbon;
                personEdit.Amphur = person.Amphur;
                personEdit.Province = person.Province;
                personEdit.BirthDate = person.BirthDate;
                personEdit.MaritalStatusCode = person.MaritalStatusCode;
                personEdit.SpousePrefix = person.SpousePrefix;
                personEdit.SpouseCitizenID = person.SpouseCitizenID;
                personEdit.SpouseFirstName = person.SpouseFirstName;
                personEdit.SpouseLastName = person.SpouseLastName;
                personEdit.SpouseBirthDate = person.SpouseBirthDate;
                personEdit.TelephoneNumber = person.TelephoneNumber;
                personEdit.StartAgriculture = person.StartAgriculture;
                personEdit.StartGrowingTobacco = person.StartGrowingTobacco;
                person.LastModified = DateTime.Now;

                uow.PersonRepository.Update(personEdit);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Person GetSingle(string citizenID)
        {
            try
            {
                return uow.PersonRepository.GetSingle(p =>
                     p.CitizenID == citizenID,
                     p => p.Farmers,
                     p => p.MaritalStatu); ;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Person> GetByName(string firstName)
        {
            return uow.PersonRepository
                .Get(x => x.FirstName.Contains(firstName),
                null,
                x => x.Farmers);
        }
    }
}
