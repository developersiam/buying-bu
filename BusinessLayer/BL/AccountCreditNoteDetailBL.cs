﻿using BusinessLayer.Helper;
using BusinessLayer.Model;
using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IAccountCreditNoteDetailBL
    {
        void Add(string creditNoteCode, Guid conditionID, int quantity, DateTime recordDate, string recordUser);
        void Delete(Guid id);
        AccountCreditNoteDetail GetSingle(Guid id);
        AccountCreditNoteDetail GetSingleByItem(string creditNoteCode, Guid conditionID);
        List<AccountCreditNoteDetail> GetByAccountInvoiceNo(string accountInvoiceNo);
        List<AccountCreditNoteDetail> GetByCreditNoteCode(string creditNoteCode);
        List<AccountCreditNoteDetail> GetByFarmerCode(int crop, string farmerCode);
        List<AccountCreditNoteDetail> GetByExtensionAgent(int crop, string extensionAgentCode);
    }

    public class AccountCreditNoteDetailBL : IAccountCreditNoteDetailBL
    {
        UnitOfWork uow;
        public AccountCreditNoteDetailBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(string creditNoteCode, Guid conditionID, int quantity, DateTime recordDate, string recordUser)
        {
            try
            {
                if (string.IsNullOrEmpty(creditNoteCode))
                    throw new ArgumentException("CreditNoteCode cannot be empty.");

                if (string.IsNullOrEmpty(recordUser))
                    throw new ArgumentException("RecordUser cannot be empty.");

                if (conditionID == null)
                    throw new ArgumentException("ConditionID cannot be empty.");

                if (quantity <= 0)
                    throw new ArgumentException("quantity จะต้องมากกว่า 0");

                ///Get a total of distribution and retrun item.
                ///Check maximum return quantity of item.
                ///==============================================

                var creditNote = uow.AccountCreditNoteRepository
                    .GetSingle(x => x.CreditNoteCode == creditNoteCode,
                    x => x.AccountInvoice,
                    x => x.AccountInvoice.Invoice);
                if (creditNote == null)
                    throw new ArgumentException("Credit note cannot be null");

                var distributionList = uow.InvoiceDetailRepository
                    .Get(x => x.AccountInvoiceNo == creditNote.AccountInvoiceNo,
                    null,
                    x => x.CropInputsCondition,
                    x => x.CropInputsCondition.Supplier)
                    .ToList();
                if (distributionList.Count() <= 0)
                    throw new ArgumentException("ไม่พบข้อมูลรายการจ่ายปัจจัยการผลิต โปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                var creditNoteDetailList = uow.AccountCreditNoteDetailRepository
                    .Get(x => x.AccountCreditNote.AccountInvoiceNo == creditNote.AccountInvoiceNo
                    , null,
                    x => x.AccountCreditNote)
                    .ToList();
                if (creditNoteDetailList
                    .Where(x => x.ConditionID == conditionID && x.CreditNoteCode == creditNoteCode)
                    .Count() > 0)
                    throw new ArgumentException("มีการเพิ่มสินค้านี้ในใบลดหนี้/ใบเสร็จฉบับนี้แล้ว หากต้องการเปลี่ยนแปลงจำนวนสินค้าให้ลบและเพิ่มใหม่อีกครั้ง");

                var totalDistribution = distributionList
                    .Where(x => x.ConditionID == conditionID)
                    .Sum(x => x.Quantity);

                var totalReturn = creditNoteDetailList
                    .Where(x => x.ConditionID == conditionID)
                    .Sum(x => x.Quantity);

                if (totalReturn + quantity > totalDistribution)
                    throw new ArgumentException("คืนสินค้าเกินมา " +
                        (totalReturn + quantity - totalDistribution) + " รายการ " +
                        "(สามารถคืนได้ " + totalDistribution + " รายการ ขณะนี้คืนไปแล้วรวม " + totalReturn + " รายการ)");



                #region Return basket promotion calculation
                /// *****************************************************************************************************
                /// *****************************************************************************************************
                /// 
                var invoice = creditNote.AccountInvoice.Invoice;
                var itemInPromotionList = BuyingFacade.CY202201ReturnItemBL().GetAll()
                    .Where(x => x.ItemType == "ตะกร้า");

                var item = distributionList.SingleOrDefault(x => x.CropInputsCondition.ConditionID == conditionID);
                decimal unitPrice = item.UnitPrice;

                var promotionPass = false;
                var returnSummary = CY202201ReturnSummaryHelper.GetByFarmer(invoice.Crop, invoice.FarmerCode);
                if (returnSummary.CPAConatainerReturnPercent >= 85 &&
                    returnSummary.BasketReturnPercent >= 100)
                    promotionPass = true;

                //promotionPass = false;
                if (itemInPromotionList
                    .Where(x => x.ReturnItemID == item.CropInputsCondition.ItemID)
                    .Count() > 0 && promotionPass == true)
                    unitPrice = (decimal)10;

                /// *****************************************************************************************************
                /// *****************************************************************************************************
                /// 
                #endregion



                uow.AccountCreditNoteDetailRepository
                    .Add(new AccountCreditNoteDetail
                    {
                        ID = Guid.NewGuid(),
                        CreditNoteCode = creditNoteCode,
                        ConditionID = conditionID,
                        UnitPrice = unitPrice,
                        Quantity = quantity,
                        RecordDate = DateTime.Now,
                        RecordUser = recordUser,
                        ModifiedDate = DateTime.Now,
                        ModifiedUser = recordUser
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var deleteItem = GetSingle(id);
                if (deleteItem == null)
                    throw new ArgumentException("item cannot be null.");

                if (deleteItem.AccountCreditNote.IsPrinted == true)
                    throw new ArgumentException("Credit note นี้สถานะถูกล็อคอยู่ ไม่สามารถลบข้อมูลได้");

                var creditNote = uow.AccountCreditNoteRepository
                    .GetSingle(x => x.CreditNoteCode == deleteItem.CreditNoteCode);
                if (creditNote == null)
                    throw new ArgumentException("Credit note cannot be null");

                uow.AccountCreditNoteDetailRepository.Remove(deleteItem);
                uow.Save();

                ///If this is maked a credit note from PIN PIV. 
                ///The system will issued this item from the AGM Inventory System.
                ///=================================================================

                var condition = uow.CropInputsConditionRepository
                    .GetSingle(x => x.ConditionID == deleteItem.ConditionID,
                    x => x.Supplier);
                if (condition == null)
                    throw new ArgumentException("ไม่พบรายการสินค้านี้ในข้อมูลการจ่ายปัจจัยการผลิต โปรดติดต่อแผนกไอทีเพื่อตรวจสอบข้อมูล");

                var item = AGMInventorySystemBL.BLServices.material_itemBL()
                    .GetSingle(condition.ItemID);
                if (item == null)
                    throw new ArgumentException("ไม่พบรายการสินค้านี้ในระบบคลังสินค้า โปรดติดต่อแผนกไอทีเพื่อตรวจสอบข้อมูล");

                //var location = "";
                //var area = condition.Supplier.SupplierArea;

                //if (area == "PET")
                //    location = "01";
                //else if (area == "SUK")
                //    location = "02";
                //else
                //    location = "";

                //if (creditNote.AccountInvoiceNo.Substring(1, 2) == "IV" ||
                //    creditNote.AccountInvoiceNo.Substring(1, 2) == "IN")
                //{
                //    AGMInventorySystemBL.BLServices.material_transactionBL()
                //        .In(new AGMInventorySystemEntities.material_transaction
                //        {
                //            crop = condition.Crop,
                //            refid = deleteItem.CreditNoteCode,
                //            itemid = item.itemid,
                //            unitid = item.unitid,
                //            quantity = deleteItem.Quantity,
                //            unitprice = deleteItem.UnitPrice,
                //            locationid = location,
                //            transactiontype = "Out",
                //            transactiondate = DateTime.Now,
                //            modifieddate = DateTime.Now,
                //            modifieduser = "credit note system."
                //        });
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AccountCreditNoteDetail> GetByAccountInvoiceNo(string accountInvoiceNo)
        {
            return uow.AccountCreditNoteDetailRepository
                .Get(x => x.AccountCreditNote.AccountInvoiceNo == accountInvoiceNo, null,
                x => x.AccountCreditNote,
                x => x.CropInputsCondition);
        }

        public List<AccountCreditNoteDetail> GetByCreditNoteCode(string creditNoteCode)
        {
            return uow.AccountCreditNoteDetailRepository
                .Get(x => x.CreditNoteCode == creditNoteCode, null,
                x => x.AccountCreditNote);
        }

        public List<AccountCreditNoteDetail> GetByFarmerCode(int crop, string farmerCode)
        {
            return uow.AccountCreditNoteDetailRepository
                .Get(x => x.AccountCreditNote
                .AccountInvoice
                .Invoice
                .Crop == crop &&
                x.AccountCreditNote
                .AccountInvoice
                .Invoice.FarmerCode == farmerCode
                , null
                , x => x.CropInputsCondition
                , x => x.AccountCreditNote
                , x => x.AccountCreditNote.AccountInvoice
                , x => x.AccountCreditNote.AccountInvoice.Invoice);
        }

        public List<AccountCreditNoteDetail> GetByExtensionAgent(int crop, string extensionAgentCode)
        {
            return uow.AccountCreditNoteDetailRepository
                .Get(x => x.AccountCreditNote.AccountInvoice.Invoice.Crop == crop &&
                x.AccountCreditNote.AccountInvoice.Invoice.RegistrationFarmer.Farmer.Supplier == extensionAgentCode
                , null
                , x => x.CropInputsCondition
                , x => x.AccountCreditNote);
        }

        public AccountCreditNoteDetail GetSingle(Guid id)
        {
            return uow.AccountCreditNoteDetailRepository
                .GetSingle(x => x.ID == id, x => x.AccountCreditNote);
        }

        public AccountCreditNoteDetail GetSingleByItem(string creditNoteCode, Guid conditionID)
        {
            return uow.AccountCreditNoteDetailRepository
                .GetSingle(x => x.CreditNoteCode == creditNoteCode && x.ConditionID == conditionID);
        }
    }
}
