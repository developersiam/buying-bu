﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface ICPAContainerItemBL
    {
        void Add(string code, string name, string description, string unit, string user);
        void Edit(string code, string name, string description, string unit, string user);
        void Delete(string code);
        CPAContainerItem GetSingle(string code);
        List<CPAContainerItem> GetAll();
    }

    public class CPAContainerItemBL : ICPAContainerItemBL
    {
        UnitOfWork uow;
        public CPAContainerItemBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(string code, string name, string description, string unit, string user)
        {
            try
            {
                if (string.IsNullOrEmpty(code))
                    throw new ArgumentException("code cannot be empty.");


                if (string.IsNullOrEmpty(code))
                    throw new ArgumentException("name cannot be empty.");

                if (string.IsNullOrEmpty(code))
                    throw new ArgumentException("description cannot be empty.");

                if (string.IsNullOrEmpty(code))
                    throw new ArgumentException("unit cannot be empty.");

                if (string.IsNullOrEmpty(code))
                    throw new ArgumentException("user cannot be empty.");

                if (GetSingle(code) != null)
                    throw new ArgumentException("code " + code + " มีแล้วในระบบ");

                uow.CPAContainerItemRepository
                    .Add(new CPAContainerItem
                    {
                        Code = code,
                        Name = name,
                        Description = description,
                        Unit = unit,
                        ModifiedBy = user,
                        ModifiedDate = DateTime.Now
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string code)
        {
            try
            {
                var item = GetSingle(code);
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                uow.CPAContainerItemRepository.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(string code, string name, string description, string unit, string user)
        {
            try
            {
                var item = GetSingle(code);
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                item.Name = name;
                item.Description = description;
                item.Unit = unit;
                item.ModifiedDate = DateTime.Now;
                item.ModifiedBy= user;

                uow.CPAContainerItemRepository.Update(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CPAContainerItem> GetAll()
        {
            return uow.CPAContainerItemRepository.Get();
        }

        public CPAContainerItem GetSingle(string code)
        {
            return uow.CPAContainerItemRepository.GetSingle(x => x.Code == code);
        }
    }
}
