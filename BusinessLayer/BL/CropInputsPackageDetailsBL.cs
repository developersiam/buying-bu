﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface ICropInputsPackageDetailsBL
    {
        void Add(CropInputsPackageDetail model);
        void Delete(string packageCode, Guid conditionID);
        void Update(CropInputsPackageDetail model);
        CropInputsPackageDetail GetSingle(string packageCode, Guid conditionID);
        List<CropInputsPackageDetail> GetByPackageCode(string packageCode);
    }

    public class CropInputsPackageDetailsBL : ICropInputsPackageDetailsBL
    {
        UnitOfWork uow;
        public CropInputsPackageDetailsBL()
        {
            uow = new UnitOfWork();
        }
        public void Add(CropInputsPackageDetail model)
        {
            try
            {
                var validation = new DomainValidation.IsCropInputsPackageDetailsValid().Validate(model);

                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var package = BuyingFacade.CropInputsPackageBL().GetSingle(model.PackageCode);
                if (package == null)
                    throw new ArgumentException("ไม่พบ package code ดังกล่าวในระบบ");

                var items = BuyingFacade.CropInputsConditionBL()
                    .GetBySupplierCode(package.Crop, package.SupplierCode);

                if (items.Where(x=>x.ConditionID == model.ConditionID).Count() <= 0)
                    throw new ArgumentException("ไม่พบการกำหนดข้อมูลรายการ item นี้ใน extension agent รายนี้");

                //if (item.purchaseorder_details.Count <= 0)
                //    throw new ArgumentException("ไม่สามารถหาค่าเฉลียของ Unit price ได้เนื่องจากยังไม่ปรากฏรายการข้อมูลนี้ใน PO ใดๆ");

                var itemsInPackage = GetByPackageCode(model.PackageCode);
                if (itemsInPackage.Where(x => x.ConditionID == model.ConditionID).Count() > 0)
                    throw new ArgumentException("มี item นี้แล้วใน package code " + model.PackageCode);

                model.ModifiedDate = DateTime.Now;

                uow.CropInputsPackageDetailsRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string packageCode, Guid conditionID)
        {
            try
            {
                var model = GetSingle(packageCode, conditionID);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.CropInputsPackageDetailsRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CropInputsPackageDetail> GetByPackageCode(string packageCode)
        {
            return uow.CropInputsPackageDetailsRepository
                .Get(x => x.PackageCode == packageCode,
                null,
                x => x.CropInputsCondition)
                .ToList();
        }

        public CropInputsPackageDetail GetSingle(string packageCode, Guid conditionID)
        {
            return uow.CropInputsPackageDetailsRepository
                .GetSingle(x => x.PackageCode == packageCode &&
                x.CropInputsCondition.ConditionID == conditionID);
        }

        public void Update(CropInputsPackageDetail model)
        {
            try
            {
                var editModel = GetSingle(model.PackageCode, model.ConditionID);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                editModel.Quantity = model.Quantity;
                editModel.ModifiedDate = DateTime.Now;
                editModel.ModifiedUser = model.ModifiedUser;

                uow.CropInputsPackageDetailsRepository.Update(editModel);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
