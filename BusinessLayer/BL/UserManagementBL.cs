﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IUserManagementBL
    {
        void AddSupplierUser(SupplierUser model);
        void DeleteSupplierUser(SupplierUser model);
        SupplierUser GetSupplierUser(string supplierCode, string userId);
        SupplierUser GetSupplierUserByUserName(string userName);
        List<SupplierUser> GetSupplierUsers();
        List<SupplierUser> GetSupplierUserBySupplier(string supplierCode);

        void AddSupervisorUser(SupervisorUser model);
        void DeleteSupervisorUser(SupervisorUser model);
        SupervisorUser GetSupervisorUser(string areaCode, string userId);
        SupervisorUser GetSupervisorUserByUserName(string userName);
        List<SupervisorUser> GetSupervisorUsers();
        List<SupervisorUser> GetSupervisorUserByArea(string areaCode);

        List<AspNetUser> GetAspNetUserByRole(string roleName);
        AspNetUser GetAspNetUserByUsername(string userName);
    }

    public class UserManagementBL : IUserManagementBL
    {
        UnitOfWork uow;
        public UserManagementBL()
        {
            uow = new UnitOfWork();
        }

        public void AddSupplierUser(SupplierUser model)
        {
            try
            {
                if (model.UserId == null)
                    throw new ArgumentException("You don't specify UserId.");

                if (model.SupplierCode == null)
                    throw new ArgumentException("You don't specify SupplierCode.");

                if (model.ModifiedByUser == null)
                    throw new ArgumentException("You don't specify ModifiedByUser.");

                if (GetSupplierUser(model.SupplierCode, model.UserId) != null)
                    throw new ArgumentException("Dupplicate data.");

                model.LastModifiedDate = DateTime.Now;

                uow.SupplierUserRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteSupplierUser(SupplierUser model)
        {
            try
            {
                var _delete = GetSupplierUser(model.SupplierCode, model.UserId);

                if (_delete == null)
                    throw new ArgumentException("Find data not found.");

                uow.SupplierUserRepository.Remove(_delete);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public SupplierUser GetSupplierUser(string supplierCode, string userId)
        {
            return uow.SupplierUserRepository
                .GetSingle(s => s.SupplierCode == supplierCode &&
                s.UserId == userId,
                su => su.AspNetUser);
        }
        public List<SupplierUser> GetSupplierUsers()
        {
            return uow.SupplierUserRepository
                .Get(null,
                null,
                su => su.AspNetUser,
                su => su.Supplier)
                .ToList();
        }
        public List<SupplierUser> GetSupplierUserBySupplier(string supplierCode)
        {
            return uow.SupplierUserRepository
                .Query(s => s.SupplierCode == supplierCode,
                null,
                su => su.AspNetUser)
                .ToList();
        }
        public void AddSupervisorUser(SupervisorUser model)
        {
            try
            {
                if (model.UserId == null)
                    throw new ArgumentException("You don't specify UserId.");

                if (model.AreaCode == null)
                    throw new ArgumentException("You don't specify AreaCode.");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("You don't specify ModifiedUser.");

                if (GetSupervisorUser(model.AreaCode, model.UserId) != null)
                    throw new ArgumentException("Dupplicate data.");

                model.ModifiedDate = DateTime.Now;

                uow.SupervisorUserRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteSupervisorUser(SupervisorUser model)
        {
            try
            {
                var _delete = uow.SupervisorUserRepository
                    .GetSingle(x => x.AreaCode == model.AreaCode && x.UserId == model.UserId);

                if (_delete == null)
                    throw new ArgumentException("Find data not found.");

                uow.SupervisorUserRepository.Remove(_delete);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public SupervisorUser GetSupervisorUser(string areaCode, string userId)
        {
            return uow.SupervisorUserRepository
                .GetSingle(s => s.AreaCode == areaCode &&
                s.UserId == userId,
                s => s.AspNetUser);
        }
        public List<SupervisorUser> GetSupervisorUsers()
        {
            return uow.SupervisorUserRepository
                .Get(null, null, s => s.AspNetUser).ToList();
        }
        public List<SupervisorUser> GetSupervisorUserByArea(string areaCode)
        {
            return uow.SupervisorUserRepository
                .Query(s => s.AreaCode == areaCode,
                null,
                s => s.AspNetUser)
                .ToList();
        }
        public List<AspNetUser> GetAspNetUserByRole(string roleName)
        {
            List<AspNetRole> userRoles = uow.AspNetRoleRepository
                .Query(r => r.Name.Contains(roleName), null, r => r.AspNetUsers).ToList();

            List<AspNetUser> users = new List<AspNetUser>();
            foreach (AspNetRole item in userRoles)
            {
                users.AddRange(item.AspNetUsers);
            }

            return users;
        }
        public SupplierUser GetSupplierUserByUserName(string userName)
        {
            return uow.SupplierUserRepository
                .GetSingle(s => s.AspNetUser.UserName == userName, s => s.Supplier);
        }
        public SupervisorUser GetSupervisorUserByUserName(string userName)
        {
            return uow.SupervisorUserRepository
                .GetSingle(s => s.AspNetUser.UserName == userName);
        }
        public AspNetUser GetAspNetUserByUsername(string userName)
        {
            return uow.AspNetUserRepository
                .GetSingle(u => u.UserName == userName,
                u => u.AspNetRoles,
                u => u.SupplierUsers,
                u => u.SupervisorUsers);
        }
    }
}