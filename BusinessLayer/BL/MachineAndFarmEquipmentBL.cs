﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IMachineAndFarmEquipmentBL
    {
        void Add(MachineAndFarmEquipment model);
        void Update(MachineAndFarmEquipment model);
        void Delete(Guid machineAndFarmEquipmentID);
        MachineAndFarmEquipment GetByID(Guid id);
        List<MachineAndFarmEquipment> GetAll();
    }

    public class MachineAndFarmEquipmentBL : IMachineAndFarmEquipmentBL
    {
        UnitOfWork uow;
        public MachineAndFarmEquipmentBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(MachineAndFarmEquipment model)
        {
            try
            {
                var obj = uow.MachineAndFarmEquipmentRepository
                    .Query(x => x.MachineAndFarmEquipmentName
                    .Contains(model.MachineAndFarmEquipmentName));

                if (obj.Count() > 0)
                    throw new ArgumentException("มีข้อมูลนี้ซ้ำแล้วในระบบ");

                uow.MachineAndFarmEquipmentRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Update(MachineAndFarmEquipment model)
        {
            try
            {
                var obj = GetByID(model.MachineAndFarmEquipmentID);
                if (obj == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                obj.MachineAndFarmEquipmentName = model.MachineAndFarmEquipmentName;
                obj.IsActive = model.IsActive;
                obj.ModifiedDate = DateTime.Now;
                obj.ModifiedUser = model.ModifiedUser;

                uow.MachineAndFarmEquipmentRepository.Update(obj);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(Guid id)
        {
            try
            {
                var model = GetByID(id);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลที่ต้องการลบ");

                uow.MachineAndFarmEquipmentRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public MachineAndFarmEquipment GetByID(Guid id)
        {
            try
            {
                return uow.MachineAndFarmEquipmentRepository
                    .GetSingle(x => x.MachineAndFarmEquipmentID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<MachineAndFarmEquipment> GetAll()
        {
            try
            {
                return uow.MachineAndFarmEquipmentRepository.Get()
                    .OrderBy(x => x.MachineAndFarmEquipmentName).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
