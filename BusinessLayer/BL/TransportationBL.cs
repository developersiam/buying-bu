﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;
using BusinessLayer.Model;

namespace BusinessLayer.BL
{
    public interface ITransportationBL
    {
        string Add(short crop, string buyingStationCode, DateTime createDate,
            string createBy, string truckNumber, short maximumLoadingWeight);
        void Update(string transportationCode, string truckNumber, short maximumLoadingWeight, string modifiedUser, DateTime createDate);
        void Delete(string transportationCode);
        void Finish(string transportationCode, string transportationUser);
        void Unfinish(string transportationCode, string transportationUser);
        void LoadBaleToTruck(string baleBarcode, string transportationCode, string loadBaleToTruckUser);
        void RemoveBaleFromTruck(string baleBarcode);
        void MoveBale(string baleBarcode, string destinationTransportionCode,
            string movingBy, DateTime movingDate, string movingReason);
        TransportationDocument GetSingle(string transportationCode);
        List<TransportationDocument> GetByCreateDate(DateTime createDate);
        List<TransportationDocument> GetByStation(short crop, string stationCode);
    }

    public class TransportationBL : ITransportationBL
    {
        UnitOfWork uow;
        public TransportationBL()
        {
            uow = new UnitOfWork();
        }

        public string Add(short crop, string buyingStationCode, DateTime createDate,
            string createBy, string truckNumber, short maximumLoadingWeight)
        {
            try
            {
                if (buyingStationCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล buying Station Code");

                if (createDate == null)
                    throw new ArgumentException("ไม่พบข้อมูล create Date");

                if (createBy == null)
                    throw new ArgumentException("ไม่พบข้อมูล create by");

                if (truckNumber == null)
                    throw new ArgumentException("ไม่พบข้อมูล truck Number");

                if (maximumLoadingWeight <= 0)
                    throw new ArgumentException("maximum weight ควรมากกว่า 0 กก. ขึ้นไป");

                int sequenceNumber = 0;

                var list = uow.TransportationDocumentRepository
                    .Query(t => t.Crop == crop &&
                    t.BuyingStationCode == buyingStationCode)
                    .ToList();

                //Generate sequence number by year.
                if (list.Count() <= 0)
                    sequenceNumber = 1;
                else
                    sequenceNumber = list.Max(t => t.SequenceNumber) + 1;

                TransportationDocument newTransportationDocument = new TransportationDocument
                {
                    Crop = crop,
                    BuyingStationCode = buyingStationCode,
                    SequenceNumber = Convert.ToInt16(sequenceNumber),
                    TruckNumber = truckNumber,
                    CreateDate = createDate,
                    CreateUser = createBy,
                    MaximumWeight = maximumLoadingWeight,
                    ModifieBy = createBy,
                    ModifiedDate = DateTime.Now,
                    IsFinish = false,
                    IsTruckApprove = false,
                    TransportationDocumentCode = crop + "-" + buyingStationCode + "-" + sequenceNumber
                };

                uow.TransportationDocumentRepository.Add(newTransportationDocument);
                uow.Save();

                return newTransportationDocument.TransportationDocumentCode;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(string transportationCode, string truckNumber, short maximumLoadingWeight,
            string modifiedUser, DateTime createDate)
        {
            try
            {
                if (transportationCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล transportation Code");

                if (createDate == null)
                    throw new ArgumentException("ไม่พบข้อมูล create Date");

                if (modifiedUser == null)
                    throw new ArgumentException("ไม่พบข้อมูล modified User");

                if (truckNumber == null)
                    throw new ArgumentException("ไม่พบข้อมูล truck Number");

                if (maximumLoadingWeight.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล maximum loading weight");

                if (maximumLoadingWeight <= 0)
                    throw new ArgumentException("maximum loading weight ควรมากกว่า 0 กิโลกรัมขึ้นไป");

                var model = uow.TransportationDocumentRepository
                    .GetSingle(t => t.TransportationDocumentCode == transportationCode,
                    t => t.Buyings);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลใบรายการขนส่งหมายเลข " + transportationCode + " นี้ในระบบ");

                if (model.IsFinish == true)
                    throw new ArgumentException("ใบนำส่งรหัสนี้ถูกเปลี่ยนสถานะเป็น Finish แล้ว");

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลใบรายการขนส่งหมายเลข " + transportationCode + " นี้ในระบบ");

                if (model.Buyings.Sum(b => b.Weight) > maximumLoadingWeight)
                    throw new ArgumentException("น้ำหนักสูงสุดที่บันทุกได้ที่ต้องการแก้ไขใหม่คือ " + maximumLoadingWeight.ToString("N0") +
                        " กิโลกรัม น้อยกว่าน้ำหนักรวมของห่อยาที่ยิงขึ้นรถบรรทุกไปแล้วคือ " + model.Buyings.Sum(b => b.Weight).ToString() + " กิโลกรัม");

                model.TruckNumber = truckNumber;
                model.MaximumWeight = maximumLoadingWeight;
                model.CreateDate = createDate;
                model.ModifieBy = modifiedUser;
                model.ModifiedDate = DateTime.Now;

                uow.TransportationDocumentRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string transportationCode)
        {
            try
            {
                if (transportationCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล transportation Code");

                var model = uow.TransportationDocumentRepository
                    .GetSingle(t => t.TransportationDocumentCode == transportationCode,
                    x => x.Buyings);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลใบรายการขนส่งหมายเลข " + transportationCode + " นี้ในระบบ");

                if (model.IsFinish == true)
                    throw new ArgumentException("ใบนำส่งรหัสนี้ถูกเปลี่ยนสถานะเป็น Finish แล้ว ไม่สามารถลบได้");

                if (model.Buyings.Count() > 0)
                    throw new ArgumentException("มีการบันทึกห่อยาไปกับรถบรรทุกรหัสการขนส่งนี้แล้ว ไม่สามารถลบรายการข้อมูลนี้ได้");

                uow.TransportationDocumentRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Finish(string transportationCode, string transportationUser)
        {
            try
            {
                if (transportationCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล transportation Code");

                var model = uow.TransportationDocumentRepository
                    .GetSingle(t => t.TransportationDocumentCode == transportationCode);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลใบรายการขนส่งหมายเลข " + transportationCode + " นี้ในระบบ");

                model.IsFinish = true;
                model.ModifieBy = transportationUser;
                model.ModifiedDate = DateTime.Now;

                uow.TransportationDocumentRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Unfinish(string transportationCode, string transportationUser)
        {
            try
            {
                if (transportationCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล transportation Code");

                var model = uow.TransportationDocumentRepository
                    .GetSingle(t => t.TransportationDocumentCode == transportationCode);

                if (model == null) throw new ArgumentException("ไม่พบข้อมูลใบรายการขนส่งหมายเลข " + transportationCode + " นี้ในระบบ");

                model.IsFinish = false;
                model.ModifieBy = transportationUser;
                model.ModifiedDate = DateTime.Now;

                uow.TransportationDocumentRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void LoadBaleToTruck(string baleBarcode, string transportationCode, string loadBaleToTruckUser)
        {
            try
            {
                if (baleBarcode == "")
                    throw new ArgumentException("ไม่พบข้อมูล baleBarcode");

                var _buying = uow.BuyingRepository.GetSingle(x => x.BaleBarcode == baleBarcode);

                if (_buying == null)
                    throw new ArgumentException("ไม่พบข้อมูลห่อยาหมายเลขบาร์โค้ต " + baleBarcode + " นี้ในระบบ");

                if (_buying.Weight == null)
                    throw new ArgumentException("ยาห่อนี้ยังไม่ได้บันทึกน้ำหนัก");

                if (_buying.TransportationDocumentCode != null)
                    throw new ArgumentException("มีข้อมูลการขนห่อยาขึ้นรถบรรทุกแล้ว รายละเอียดดังนี้" + Environment.NewLine +
                "FarmerCode : " + _buying.FarmerCode + Environment.NewLine +
                "BaleBarcode : " + _buying.BaleBarcode + Environment.NewLine +
                "ProjectType : " + _buying.ProjectType + Environment.NewLine +
                "Grade : " + _buying.Grade + Environment.NewLine +
                "Weight : " + _buying.Weight + Environment.NewLine +
                "TransportationCode : " + _buying.TransportationDocumentCode);

                //if (buyingInfo.BuyingDocument.IsFinish == true)
                //    throw new ArgumentException("ใบซื้อนี้ถูก Finish แล้ว ไม่สามารถบันทึกข้อมูลการขนขึ้นรถบรรทุกได้");

                //ใบรายการขนส่งถ้า Finish แล้วจะไม่ให้ดำเนินการใดๆ ได้อีก
                var transportationDocument = uow.TransportationDocumentRepository
                    .GetSingle(t => t.TransportationDocumentCode == transportationCode,
                    t => t.Buyings);

                if (transportationDocument.Buyings.Sum(b => b.Weight) + _buying.Weight >= transportationDocument.MaximumWeight)
                    throw new ArgumentException("น้ำหนักบรรทุกเกินกำหนด (" + transportationDocument.MaximumWeight + " กก.)" + Environment.NewLine
                        + "นน. ห่อยาบนรถ = " + transportationDocument.Buyings.Sum(b => b.Weight) + " กก." + Environment.NewLine
                        + "นน. ยาห่อนี้ = " + _buying.Weight + " กก.");

                if (transportationDocument.IsFinish == true)
                    throw new ArgumentException("ใบรายการขนส่งหมายเลข " +
                        transportationDocument.TransportationDocumentCode + " ถูก Finish แล้ว");

                _buying.TransportationDocumentCode = transportationCode;
                _buying.LoadBaleToTruckDate = DateTime.Now;
                _buying.LoadBaleToTruckUser = loadBaleToTruckUser;

                uow.BuyingRepository.Update(_buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveBaleFromTruck(string baleBarcode)
        {
            try
            {
                if (baleBarcode == "") throw new ArgumentException("ไม่พบข้อมูล baleBarcode");

                var _buying = uow.BuyingRepository.GetSingle(x => x.BaleBarcode == baleBarcode);

                if (_buying == null) throw new ArgumentException("ไม่พบข้อมูลห่อยาหมายเลขบาร์โค้ต " + baleBarcode + " นี้ในระบบ");

                if (_buying.TransportationDocumentCode == null)
                    throw new ArgumentException("ยาห่อนี้ยังไม่ถูกนำขึ้นรถบรรทุก รายละเอียดดังนี้" + Environment.NewLine +
                "FarmerCode : " + _buying.FarmerCode + Environment.NewLine +
                "BaleBarcode : " + _buying.BaleBarcode + Environment.NewLine +
                "ProjectType : " + _buying.ProjectType + Environment.NewLine +
                "Grade : " + _buying.Grade + Environment.NewLine +
                "Weight : " + _buying.Weight + Environment.NewLine +
                "TransportationCode : " + _buying.TransportationDocumentCode);

                if (_buying.BuyingDocument.IsFinish == true)
                    throw new ArgumentException("ใบซื้อนี้ถูก Finish แล้ว");

                //ใบรายการขนส่งถ้า Finish แล้วจะไม่ให้ดำเนินการใดๆ ได้อีก
                var transportationDocument = uow.TransportationDocumentRepository
                    .GetSingle(t => t.TransportationDocumentCode == _buying.TransportationDocumentCode);

                if (transportationDocument.IsFinish == true)
                    throw new ArgumentException("ใบรายการขนส่งหมายเลข " +
                        transportationDocument.TransportationDocumentCode + " ถูก Finish แล้ว");

                _buying.TransportationDocumentCode = null;
                _buying.LoadBaleToTruckUser = null;
                _buying.LoadBaleToTruckDate = DateTime.Now;

                uow.BuyingRepository.Update(_buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void MoveBale(string baleBarcode,
            string destinationTransportionCode,
            string movingBy,
            DateTime movingDate,
            string movingReason)
        {
            try
            {
                if (baleBarcode == "")
                    throw new ArgumentException("ไม่พบข้อมูล baleBarcode");

                if (destinationTransportionCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล destination Transportion Code");

                if (movingBy == "")
                    throw new ArgumentException("ไม่พบข้อมูล moving By");

                var _buying = uow.BuyingRepository
                    .GetSingle(x => x.BaleBarcode == baleBarcode);

                if (_buying == null)
                    throw new ArgumentException("ไม่พบข้อมูลห่อยาหมายเลขบาร์โค้ต " + baleBarcode + " นี้ในระบบ");

                if (_buying.Weight == null)
                    throw new ArgumentException("ยาห่อนี้ยังไม่ได้บันทึกน้ำหนัก");

                /// ตรวจสอบ truck ต้นทางว่าได้มีการ finish ไปแล้วหรือไม่?
                var source = uow.TransportationDocumentRepository
                    .GetSingle(t => t.TransportationDocumentCode == _buying.TransportationDocumentCode);
                if (source == null)
                    throw new ArgumentException("ไม่พบข้อมูล transportation code ต้นทาง");

                if (source.IsFinish == true)
                    throw new ArgumentException("ใบรายการขนส่งหมายเลข " +
                        source.TransportationDocumentCode + " ถูก Finish แล้ว ไม่สามารถย้ายข้อมูลการขนส่งได้");

                // ตรวจสอบ truck ปลายทางว่าได้มีการ finish ไปแล้วหรือไม่?
                var destination = uow.TransportationDocumentRepository
                    .GetSingle(t => t.TransportationDocumentCode == destinationTransportionCode);
                if (destination == null)
                    throw new ArgumentException("ไม่พบข้อมูล transportation code ปลายทาง");

                if (destination.IsFinish == true)
                    throw new ArgumentException("ใบรายการขนส่งหมายเลข " +
                        destination.TransportationDocumentCode + " ถูก Finish แล้ว");

                _buying.TransportationDocumentCode = destinationTransportionCode;
                _buying.MovingTransportationFrom = _buying.TransportationDocumentCode;
                _buying.MovingTransportationDate = movingDate;
                _buying.MovingTransportationReason = movingReason;
                _buying.MovingBy = movingBy;

                uow.BuyingRepository.Update(_buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public TransportationDocument GetSingle(string transportationCode)
        {
            return uow.TransportationDocumentRepository
                .GetSingle(t => t.TransportationDocumentCode == transportationCode,
                t => t.Buyings);

        }

        public List<TransportationDocument> GetByCreateDate(DateTime createDate)
        {
            return uow.TransportationDocumentRepository
               .Get(x => x.CreateDate != null,
               null,
               x => x.BuyingStation)
               .ToList()
               .Where(x => Convert.ToDateTime(x.CreateDate).Date == createDate.Date)
               .ToList();
        }

        public List<TransportationDocument> GetByStation(short crop, string stationCode)
        {
            return uow.TransportationDocumentRepository
                .Get(x => x.Crop == crop && x.BuyingStationCode == stationCode);
        }
    }
}
