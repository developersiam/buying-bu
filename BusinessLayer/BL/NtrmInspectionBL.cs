﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface INtrmInspectionBL
    {
        void Add(string baleBarcode, string ntrmTypeCode, byte ntrmQuantity, string ntrmUser);
        void NTRMInspection(string baleBarcode);
        void Delete(string baleBarcode, string ntrmTypeCode, DateTime inspectionDate);
        double GetPercentagesByFarmer(string farmerCode);
        bool IsHighlyDangerousInspection(string farmerCode);
        List<NTRMInspection> GetByBaleBarcode(string baleBarcode);
        List<NTRMInspection> GetByFarmerAndCrop(short crop, string farmerCode);
        List<NTRMType> GetAllNTRMType();
        List<NTRMInspection> GetByFarmer(string farmerCode);
    }

    public class NtrmInspectionBL : INtrmInspectionBL
    {
        UnitOfWork uow;
        public NtrmInspectionBL()
        {
            uow = new UnitOfWork();
        }

        public double GetPercentagesByFarmer(string farmerCode)
        {
            try
            {
                /// 1. ถ้าเป็นชาวไร่รายใหม่ให้เซต NTRM Inspection Percentages = 100%
                /// 2. ถ้าเป็นชาวไร่รายเก่าให้คำนวณหาร้อยละของ NTRM ที่พบในห่อยา หากเกิน 10% ให้แจ้งเตือนไปยังผู้ใช้
                /// 

                var regisModel = uow.RegistrationFarmerRepository.Query(reg => reg.FarmerCode == farmerCode).ToList();

                if (regisModel.Count <= 0)
                    throw new ArgumentException("ไม่พบข้อมูลชาวไร่รายดังกล่าวในระบบ");

                ///หากจำนวนรายการสัญญาชาวไร่มีน้อยกว่าหรือเท่ากับ 1 รายการนั่นแสดงว่าเป็นชาวไร่รายใหม่ที่พึ่งทำสัญญากับทางบริษัท
                if (regisModel.Count <= 1)
                {
                    return 100;
                }
                else
                {
                    /// กรณีเป็นชาวไร่รายเก่าให้ตรวจสอบหาค่าร้อยละของ NTRM ที่พบในจำนวนของห่อยาทั้งหมดที่ชาวไร่นำมาขาย
                    /// 
                    /// Total bale sold of farmer.
                    var totalBaleToSoldOfFarmer = uow.BuyingRepository
                        .Query(b => b.FarmerCode == farmerCode && b.Weight != null)
                        .ToList();


                    /// Total of NTRM inspection by farmer.
                    var totalNTRMInspectionOfFarmer = uow.NTRMInspectionRepository
                        .Query(ntrm => ntrm.Buying.FarmerCode == farmerCode)
                        .ToList();

                    var ntrmGroupByBaleBarcode = from nt in totalNTRMInspectionOfFarmer
                                                 group nt by nt.BaleBarcode;

                    double totalBaleSold = totalBaleToSoldOfFarmer.Count();
                    double totalBaleOfNtrmInspection = ntrmGroupByBaleBarcode.Count();
                    double returnPercentages = Convert.ToDouble(totalBaleOfNtrmInspection / totalBaleSold);

                    return returnPercentages * 100;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<NTRMInspection> GetByFarmer(string farmerCode)
        {
            try
            {
                return uow.NTRMInspectionRepository
                    .Query(ntrm => ntrm.Buying.FarmerCode == farmerCode,
                    null,
                    ntrm => ntrm.Buying,
                    ntrm => ntrm.NTRMType).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsHighlyDangerousInspection(string farmerCode)
        {
            try
            {
                var _defaultCrop = uow.CropRepository.GetSingle(x => x.DefaultStatus == true);

                if (_defaultCrop == null)
                    throw new ArgumentException("ไม่พบการตั้งค่า Default Crop ในระบบ โปรดติดต่อแผนกไอทีเพื่อทำการตั้งค่านี้");

                var regisModel = uow.RegistrationFarmerRepository
                    .Query(reg => reg.FarmerCode == farmerCode).ToList();

                if (regisModel.Count <= 0)
                    throw new ArgumentException("ไม่พบข้อมูลชาวไร่รายดังกล่าวในระบบ");

                ///หากจำนวนรายการสัญญาชาวไร่มีน้อยกว่าหรือเท่ากับ 1 รายการนั่นแสดงว่าเป็นชาวไร่รายใหม่ที่พึ่งทำสัญญากับทางบริษัท
                if (regisModel.Count <= 1)
                    return true;

                //ดึงข้อมูลการตรวจพบ NTRM ของชาวไร่ในปีปัจจุบัน และปีที่ผ่านมา
                var ntrmList = uow.NTRMInspectionRepository
                    .Query(n => n.Buying.FarmerCode == farmerCode &&
                    (n.Buying.Crop == _defaultCrop.Crop1 || n.Buying.Crop == _defaultCrop.Crop1 - 1))
                    .ToList();

                //ตรวจสอบข้อมูลการตรวจ NTRM ที่พบ หากพบ NTRM ในกลุ่ม Highly Dangerous ให้ return true.
                if (ntrmList.Where(n => n.NTRMTypeCode == "01"
                    || n.NTRMTypeCode == "02"
                    || n.NTRMTypeCode == "03"
                    || n.NTRMTypeCode == "04"
                    || n.NTRMTypeCode == "10"
                    || n.NTRMTypeCode == "14"
                    || n.NTRMTypeCode == "15").Count() > 0)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Add(string baleBarcode, string ntrmTypeCode, byte ntrmQuantity, string ntrmUser)
        {
            try
            {
                if (baleBarcode == "")
                    throw new ArgumentException("ไม่พบข้อมูล bale Barcode");

                if (ntrmTypeCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล NTRM Type Code");

                if (ntrmUser == "")
                    throw new ArgumentException("ไม่พบข้อมูล User");

                if (ntrmQuantity <= 0 && ntrmTypeCode != "16")
                    throw new ArgumentException("หากไม่ใช่ Not Found จำนวนที่ระบุจะต้องมากกว่า 0");

                var buying = uow.BuyingRepository
                    .GetSingle(x => x.BaleBarcode == baleBarcode);

                if (buying == null)
                    throw new ArgumentException("ไม่พบข้อมูลยาห่อนี้ในระบบ");

                if (buying.Crop != DateTime.Now.Year)
                    throw new ArgumentException("ห่อยานี้ไม่อยู่ในฤดูกาลปัจจุบัน ระบบจึงไม่สามารถบันทึกข้อมูลได้");

                if (ntrmTypeCode == "16")
                    ntrmQuantity = 0;

                NTRMInspection ntrmInspection = new NTRMInspection
                {
                    BaleBarcode = baleBarcode,
                    NTRMTypeCode = ntrmTypeCode,
                    InspectionDate = DateTime.Now,
                    InspectionUser = ntrmUser,
                    Quantity = ntrmQuantity,
                    InspectionLocation = "Buying Station"
                };

                buying.IsNTRMInspection = true;
                uow.NTRMInspectionRepository.Add(ntrmInspection);

                uow.BuyingRepository.Update(buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void NTRMInspection(string baleBarcode)
        {
            try
            {
                if (baleBarcode == "")
                    throw new ArgumentException("ไม่พบหมายเลข Bale barcode โปรดสแกนใหม่อีกครั้ง");

                var buying = uow.BuyingRepository.GetSingle(b => b.BaleBarcode == baleBarcode);
                if (buying == null)
                    throw new ArgumentException("ไม่พบข้อมูลห่อยาหมายเลข " + baleBarcode + " ในระบบ");

                if (buying.Crop != DateTime.Now.Year)
                    throw new ArgumentException("ห่อยานี้ไม่อยู่ในฤดูกาลปัจจุบัน ระบบจึงไม่สามารถบันทึกข้อมูลได้");

                buying.IsNTRMInspection = true;

                uow.BuyingRepository.Update(buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string baleBarcode, string ntrmTypeCode, DateTime inspectionDate)
        {
            try
            {
                if (baleBarcode == "")
                    throw new ArgumentException("ไม่พบข้อมูล bale Barcode");

                if (ntrmTypeCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล NTRM Type Code");

                var buying = uow.BuyingRepository.GetSingle(x => x.BaleBarcode == baleBarcode);
                if (buying == null)
                    throw new ArgumentException("ไม่พบข้อมูลยาห่อนี้ในระบบ");

                if (buying.Crop != DateTime.Now.Year)
                    throw new ArgumentException("ห่อยานี้ไม่อยู่ในฤดูกาลปัจจุบัน ระบบจึงไม่สามารถบันทึกข้อมูลได้");

                var model = uow.NTRMInspectionRepository
                    .GetSingle(x => x.BaleBarcode == baleBarcode &&
                    x.NTRMTypeCode == ntrmTypeCode &&
                    x.InspectionDate == inspectionDate);

                uow.NTRMInspectionRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<NTRMInspection> GetByBaleBarcode(string baleBarcode)
        {
            try
            {
                return uow.NTRMInspectionRepository
                    .Query(n => n.BaleBarcode == baleBarcode,
                    null,
                    n => n.NTRMType)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<NTRMInspection> GetByFarmerAndCrop(short crop, string farmerCode)
        {
            try
            {
                var buyingList = uow.BuyingRepository.Query(b => b.Crop == crop &&
                    b.FarmerCode == farmerCode &&
                    b.RejectReason == "NTRM",
                    null,
                    b => b.NTRMInspections);

                List<NTRMInspection> ntrmInspectionList = new List<NTRMInspection>();

                foreach (Buying item in buyingList)
                {
                    if (ntrmInspectionList.Count >= 1)
                        ntrmInspectionList.AddRange(item.NTRMInspections);
                }

                return ntrmInspectionList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<NTRMType> GetAllNTRMType()
        {
            return uow.NTRMTypeRepository.Get()
                .OrderBy(x => x.NTRMTypeName).ToList();
        }
    }
}