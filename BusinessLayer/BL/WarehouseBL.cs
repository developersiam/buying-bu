﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IWarehouseBL
    {
        void Add(Warehouse model);
        void Update(Warehouse model);
        void Delete(Guid id);
        List<Warehouse> GetAll();
        List<Warehouse> GetByArea(string areaCode);
        Warehouse GetBySingle(Guid id);
    }
    public class WarehouseBL : IWarehouseBL
    {
        UnitOfWork uow;
        public WarehouseBL()
        {
            uow = new UnitOfWork();
        }
        public void Add(Warehouse model)
        {
            throw new NotImplementedException();
        }

        public void Delete(Guid id)
        {
            throw new NotImplementedException();
        }

        public List<Warehouse> GetAll()
        {
            return uow.WarehouseRepository.Get().ToList();
        }

        public List<Warehouse> GetByArea(string areaCode)
        {
            return uow.WarehouseRepository.Get(x => x.AreaCode == areaCode).ToList();
        }

        public Warehouse GetBySingle(Guid id)
        {
            return uow.WarehouseRepository.GetSingle(x => x.WarehouseID == id);
        }

        public void Update(Warehouse model)
        {
            throw new NotImplementedException();
        }
    }
}
