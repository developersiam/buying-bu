﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IBuyerBL
    {
        List<Buyer> GetAll();
    }

    public class BuyerBL : IBuyerBL
    {
        UnitOfWork uow;
        public BuyerBL()
        {
            uow = new UnitOfWork();
        }
        public List<Buyer> GetAll()
        {
            return uow.BuyerRepository.Get().ToList();
        }
    }
}
