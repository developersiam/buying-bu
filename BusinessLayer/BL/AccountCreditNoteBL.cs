﻿using BusinessLayer.Helper;
using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IAccountCreditNoteBL
    {
        string Add(DateTime createDate, string accountInvoiceNo,
            bool isPayForCash, string description, string createUser);
        void Delete(string creditNoteCode);
        string GenerateCreditNoteCode(DateTime createDate, string farmerCode, bool isPayForCash);
        void ReturnItemToAGMInventory(string creditNoteCode, string returnBy);
        void Lock(string creditNoteCode, string printBy);
        void UnLock(string creditNoteCode);
        List<AccountCreditNote> GetByAccountInvoice(string accountInvoiceNo);
        List<AccountCreditNote> GetByFarmer(short crop, string farmerCode);
        List<AccountCreditNote> GetByExtensionAgentCode(short crop, string extensionAgentCode);
        List<AccountCreditNote> Search(string creditNoteCode);
        AccountCreditNote GetSingle(string creditNoteCode);
    }

    public class AccountCreditNoteBL : IAccountCreditNoteBL
    {
        UnitOfWork uow;
        public AccountCreditNoteBL()
        {
            uow = new UnitOfWork();
        }

        public string Add(DateTime createDate, string accountInvoiceNo,
            bool isPayForCash, string description, string createUser)
        {
            try
            {
                if (string.IsNullOrEmpty(accountInvoiceNo))
                    throw new ArgumentException("โปรดระบุ account invoice no");

                if (Math.Abs(DateTime.Now.DayOfYear - createDate.DayOfYear) > 30)
                    throw new ArgumentException("วันที่ในเอกสารใบลดหนี้/ใบเสร็จ สามารถเลือกบันทึกย้อนหลังได้ 30 วัน" +
                        " โดยนับจากวันที่ปัจจุบัน ไม่อนุญาตให้เลือกวันที่ล่วงหน้าได้");

                if (createDate.DayOfYear > DateTime.Now.DayOfYear)
                    throw new ArgumentException("วันที่ในเอกสารใบลดหนี้/ใบเสร็จ สามารถเลือกบันทึกย้อนหลังได้ 30 วัน" +
                        " โดยนับจากวันที่ปัจจุบัน ไม่อนุญาตให้เลือกวันที่ล่วงหน้าได้");

                if (!accountInvoiceNo.Contains("I"))
                    throw new ArgumentException("ใบส่งปัจจัยการผลิตที่จะนำมาทำใบลดหนี้/ใบเสร็จได้ จะต้องเป็นใบส่งปัจจัยฯ " +
                        "ที่เป็นเงินเชื่อเท่านั้น ได้แก่ PIV, PIN, SIV, SIN");

                if (string.IsNullOrEmpty(description))
                    throw new ArgumentException("โปรดระบุหมายเหตุ เนื่องจากเป็นเงื่อนไขทางบัญชี");

                if (string.IsNullOrEmpty(createUser))
                    throw new ArgumentException("โปรดระบุ create user");

                var accountInvoice = uow.AccountInvoiceRepository
                    .GetSingle(x => x.InvoiceNo == accountInvoiceNo,
                    x => x.AccountCreditNotes,
                    x => x.Invoice);
                if (accountInvoice == null)
                    throw new ArgumentException("Account invoice object cannot be null.");

                //var debtSetupAssociationList = BuyingFacade.DebtSetupAccountInvoiceBL()
                //    .GetByAccountInvoiceNo(accountInvoiceNo);
                //if (debtSetupAssociationList.Count() > 0)
                //    throw new ArgumentException("Account Invoice No " + accountInvoiceNo + " นี้ถูกนำไปตั้งหนี้แล้ว");

                SystemDeadlinesHelper.BeforeStartBuyingDeadLine(accountInvoice.Invoice.Crop, accountInvoice.Invoice.FarmerCode);

                if (accountInvoice.IsPrinted == false)
                    throw new ArgumentException("ยังไม่มีการพิมพ์ใบส่งปัจจัยการผลิตรหัส " + accountInvoiceNo +
                        " ให้กับชาวไร่ ไม่สามารถออกใบลดหนี้/ใบเสร็จรับเงินได้");

                if (accountInvoice.AccountCreditNotes.Where(x => x.IsPrinted == false).Count() > 0)
                    throw new ArgumentException("มีใบลดหนี้บางรายการที่ยังไม่ได้พิมพ์ให้กับชาวไร่ ไม่สามารถสร้างใบลดหนี้ใหม่ได้");

                // account invoice ที่ถูกนำไปทำลดหนี้แล้ว จะไม่สามารถเอามาทำตั้งหนี้ได้
                // account invoice ที่ถูกนำไปตั้งหนี้แล้ว จะไม่สามารถเอามาทำลดหนี้ได้
                var toDebtSetupList = uow.DebtSetupAccountInvoiceRepository
                    .Get(x => x.AccountInvoiceNo == accountInvoiceNo);
                if (toDebtSetupList.Count() > 0)
                    throw new ArgumentException("Account invoice นี้ผ่านขั้นตอนการตั้งหนี้แล้วไม่สามารถทำลดหนี้ได้");

                var creditNoteCode = GenerateCreditNoteCode(createDate, accountInvoice.Invoice.FarmerCode, isPayForCash);
                uow.AccountCreditNoteRepository
                    .Add(new AccountCreditNote
                    {
                        CreditNoteCode = creditNoteCode,
                        AccountInvoiceNo = accountInvoiceNo,
                        CreateBy = createUser,
                        CreateDate = createDate.Add(DateTime.Now.TimeOfDay),
                        IsPayForCash = isPayForCash,
                        IsPrinted = false,
                        PrintDate = null,
                        PrintBy = null,
                        Description = description
                    });
                uow.Save();

                return creditNoteCode;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string creditNoteCode)
        {
            try
            {
                var item = GetSingle(creditNoteCode);
                if (item == null)
                    throw new ArgumentException("Object cannot be null.");

                var list = uow.AccountCreditNoteDetailRepository
                    .Get(x => x.CreditNoteCode == creditNoteCode);
                if (list.Count() > 0)
                    throw new ArgumentException("มีรายการสินค้าอยู่ในใบลดหนี้ ไม่สามารถลบใบลดหนี้นี้ได้");

                if (item.IsPrinted == true)
                    throw new ArgumentException("ใบลดหนี้นี้ถูกยืนยันข้อมูลไปแล้ว ไม่สามารถลบได้");

                var invoice = item.AccountInvoice.Invoice;
                if (invoice == null)
                    throw new ArgumentException("ไม่พบ invoice ต้นทางของใบลดหนี้นี้ โปรดแจ้งแผนกไอทีเพื่อตรวจสอบข้อมูล");

                var documentList = BuyingFacade.BuyingDocumentBL()
                    .GetByFarmer(invoice.Crop, invoice.FarmerCode);
                if (documentList.Count() > 0)
                    throw new ArgumentException("ไม่อนุญาตให้ลบข้อมูลเนื่องจากมีการเริ่มบันทึกข้อมูลการขายไปแล้ว");

                uow.AccountCreditNoteRepository.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GenerateCreditNoteCode(DateTime createDate, string farmerCode, bool isPayForCash)
        {
            try
            {
                int max = 0;
                var year = createDate.Year.ToString().Substring(2, 2);
                var month = createDate.Month.ToString().PadLeft(2, '0');

                var farmer = uow.FarmerRepository
                    .GetSingle(x => x.FarmerCode == farmerCode,
                    x => x.Supplier1);
                if (farmer == null)
                    throw new ArgumentException("ไม่พบข้อมูล farmer รายนี้ในระบบ โปรดติดต่อแผนกไอทีเพื่อตรวจสอบข้อมูล");

                var area = farmer.Supplier1.SupplierArea;
                var listA = uow.AccountCreditNoteRepository
                    .Get(x => x.CreditNoteCode.Substring(x.CreditNoteCode.Length - 6, 2) == year);
                var listB = new List<AccountCreditNote>();

                if (isPayForCash == true)
                    listB = listA.Where(x => !x.CreditNoteCode.Contains("C")).ToList();
                else
                    listB = listA.Where(x => x.CreditNoteCode.Contains("C")).ToList();

                var list = listB
                    .Select(x => new
                    {
                        RunNumber = Convert.ToInt16(x.CreditNoteCode.Substring(x.CreditNoteCode.Length - 2, 2))
                    })
                    .ToList();

                if (list.Count > 0)
                    max = list.Max(x => x.RunNumber);

                return area.Substring(0, 1) + (isPayForCash == true ? "" : "C") + year + month + (max + 1).ToString().PadLeft(2, '0');
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AccountCreditNote> GetByAccountInvoice(string accountInvoiceNo)
        {
            return uow.AccountCreditNoteRepository
                .Get(x => x.AccountInvoiceNo == accountInvoiceNo, null,
                x => x.AccountInvoice,
                x => x.AccountCreditNoteDetails);
        }

        public List<AccountCreditNote> GetByExtensionAgentCode(short crop, string extensionAgentCode)
        {
            return uow.AccountCreditNoteRepository
                .Get(x => x.AccountInvoice.Invoice.Crop == crop &&
                x.AccountInvoice.Invoice.RegistrationFarmer.Farmer.Supplier == extensionAgentCode,
                null,
                x => x.AccountCreditNoteDetails);
        }

        public List<AccountCreditNote> GetByFarmer(short crop, string farmerCode)
        {
            var list = uow.AccountCreditNoteRepository
                .Get(x => x.AccountInvoice.Invoice.Crop == crop &&
                x.AccountInvoice.Invoice.FarmerCode == farmerCode,
                null,
                x => x.AccountInvoice,
                x => x.AccountInvoice.Invoice,
                x => x.AccountCreditNoteDetails);
            return list;
        }

        public AccountCreditNote GetSingle(string creditNoteCode)
        {
            return uow.AccountCreditNoteRepository
                .GetSingle(x => x.CreditNoteCode == creditNoteCode,
                x => x.AccountInvoice,
                x => x.AccountInvoice.Invoice,
                x => x.AccountInvoice.Invoice.InvoiceDetails,
                x => x.AccountCreditNoteDetails);
        }

        public void Lock(string creditNoteCode, string printBy)
        {
            try
            {
                var model = GetSingle(creditNoteCode);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลใบลดหนี้รหัส " + creditNoteCode + " นี้ในระบบ");

                model.IsPrinted = true;
                model.PrintBy = printBy;
                model.PrintDate = DateTime.Now;
                uow.AccountCreditNoteRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ReturnItemToAGMInventory(string creditNoteCode, string returnBy)
        {
            try
            {
                var master = GetSingle(creditNoteCode);
                if (master == null)
                    throw new ArgumentException("Credit Note object cannot be null.");

                if (master.IsPrinted == true)
                    throw new ArgumentException("ใบลดหนี้นี้ถูกยืนยันข้อมูลไปแล้ว ไม่สามารถแก้ไขได้อีก");

                var accountInvoice = uow.AccountInvoiceRepository
                    .GetSingle(x => x.InvoiceNo == master.AccountInvoiceNo,
                    x => x.AccountCreditNotes,
                    x => x.Invoice);
                if (accountInvoice == null)
                    throw new ArgumentException("Account invoice object cannot be null.");

                if (accountInvoice.IsPrinted == false)
                    throw new ArgumentException("ยังไม่มีการพิมพ์ใบส่งปัจจัยการผลิตรหัส " + accountInvoice.Invoice +
                        " ให้กับชาวไร่ ไม่สามารถยืนยันข้อมูลการลดหนี้/ใบเสร็จนี้ได้");

                if (master.AccountCreditNoteDetails.Count() <= 0)
                    throw new ArgumentException("โปรดเพิ่มรายการสินค้าในใบลดหนี้/ใบเสร็จ อย่างน้อย 1 รายการ");

                ///If this is maked a credit note from PIN PIV. 
                ///The system will retrun this item to the AGM Inventory System.
                ///=================================================================
                ///

                var creditNoteDetailList = uow.AccountCreditNoteDetailRepository
                    .Get(x => x.CreditNoteCode == creditNoteCode,
                    null,
                    x => x.AccountCreditNote,
                    x => x.CropInputsCondition,
                    x => x.CropInputsCondition.Supplier)
                    .ToList();

                var agmTransactionList = AGMInventorySystemBL.BLServices
                    .material_transactionBL()
                    .GetTransactionlistByRefId(creditNoteCode);

                var invoice = accountInvoice.Invoice;
                var returnSummary = CY202201ReturnSummaryHelper.GetByFarmer(invoice.Crop, invoice.FarmerCode);
                var itemInPromotionList = BuyingFacade.CY202201ReturnItemBL().GetAll()
                    .Where(x => x.ItemType == "ตะกร้า");

                var isInPromotion = false;
                if (returnSummary.CPAConatainerReturnPercent >= 85 && returnSummary.BasketReturnPercent >= 100)
                    isInPromotion = true;

                foreach (var item in creditNoteDetailList)
                {
                    var agmItem = AGMInventorySystemBL.BLServices.material_itemBL()
                        .GetSingle(item.CropInputsCondition.ItemID);
                    if (item == null)
                        throw new ArgumentException("ไม่พบรายการสินค้านี้ในระบบคลังสินค้า โปรดติดต่อแผนกไอทีเพื่อตรวจสอบข้อมูล");

                    ///ตรวจสอบว่ารายการนี้เป็นตะกร้าหรือไม่ ถ้าใช่ตรวจสอบว่าเข้าเงื่อนไขการลดค่าตะกร้า 10 บาทหรือไม่
                    ///หากเป็นตะกร้าและเข้าเงื่อนไขโปรโมชัน จะไม่คืนเข้า AGM Inventory.
                    ///
                    if (itemInPromotionList.Where(x => x.ReturnItemID == agmItem.itemid).Count() > 0 &&
                        isInPromotion == true)
                        continue;

                    if (agmTransactionList
                        .Where(x => x.itemid == item.CropInputsCondition.ItemID)
                        .Count() > 0)
                        continue;

                    var location = "";
                    var area = item.CropInputsCondition.Supplier.SupplierArea;

                    if (area == "PET")
                        location = "01";
                    else if (area == "SUK")
                        location = "02";
                    else
                        location = "";

                    if (master.IsPayForCash == false)
                        AGMInventorySystemBL.BLServices.material_transactionBL()
                            .In(new AGMInventorySystemEntities.material_transaction
                            {
                                crop = item.CropInputsCondition.Crop,
                                refid = creditNoteCode,
                                itemid = agmItem.itemid,
                                unitid = agmItem.unitid,
                                quantity = item.Quantity,
                                unitprice = item.UnitPrice,
                                locationid = location,
                                transactiontype = "In",
                                transactiondate = DateTime.Now,
                                modifieddate = DateTime.Now,
                                modifieduser = returnBy
                            });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AccountCreditNote> Search(string creditNoteCode)
        {
            var list = uow.AccountCreditNoteRepository
                .Get(x => x.CreditNoteCode.Contains(creditNoteCode));
            return list;
        }

        public void UnLock(string creditNoteCode)
        {
            try
            {
                var model = GetSingle(creditNoteCode);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลใบลดหนี้รหัส " + creditNoteCode + " นี้ในระบบ");

                model.IsPrinted = false;
                uow.AccountCreditNoteRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
