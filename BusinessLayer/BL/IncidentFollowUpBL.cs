﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IIncidentFollowUpBL
    {
        void Add(Guid incidentFarmerID, DateTime followUpDate, string FollowUpDescription, string recodingUser);
        void Update(Guid id, DateTime followUpDate, string FollowUpDescription, string recodingUser);
        void Delete(Guid id);
        IncidentFollowUp GetSingle(Guid id);
        List<IncidentFollowUp> GetByIncidentFarmer(Guid incidentFarmerID);
    }

    public class IncidentFollowUpBL : IIncidentFollowUpBL
    {
        UnitOfWork uow;
        public IncidentFollowUpBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(Guid incidentFarmerID, DateTime followUpDate, string FollowUpDescription, string recodingUser)
        {
            try
            {
                uow.IncidentFollowUpRepository.Add(new IncidentFollowUp
                {
                    ID = Guid.NewGuid(),
                    IncidentFarmerID = incidentFarmerID,
                    FollowUpDate = followUpDate,
                    FollowUpDescription = FollowUpDescription,
                    RecordBy = recodingUser,
                    RecordDate = DateTime.Now,
                    ModifiedBy = recodingUser,
                    ModifiedDate = DateTime.Now
                });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var model = GetSingle(id);
                uow.IncidentFollowUpRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<IncidentFollowUp> GetByIncidentFarmer(Guid incidentFarmerID)
        {
            return uow.IncidentFollowUpRepository.Get(x => x.IncidentFarmerID == incidentFarmerID);
        }

        public IncidentFollowUp GetSingle(Guid id)
        {
            return uow.IncidentFollowUpRepository.GetSingle(x => x.ID == id);
        }

        public void Update(Guid id, DateTime followUpDate, string FollowUpDescription, string recodingUser)
        {
            try
            {
                var model = GetSingle(id);

                model.FollowUpDate = followUpDate;
                model.FollowUpDescription = FollowUpDescription;
                model.ModifiedDate = DateTime.Now;
                model.ModifiedBy = recodingUser;

                uow.IncidentFollowUpRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
