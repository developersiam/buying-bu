﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface ICropBL
    {
        void Add(Crop crop);
        void Update(Crop crop);
        void Delete(short crop);
        void SetDefault(short crop, string modifyUser);
        Crop GetDefault();
        List<Crop> GetAll();
        Crop GetSingle(short crop);
    }

    public class CropBL : ICropBL
    {
        UnitOfWork uow;
        public CropBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(Crop crop)
        {
            try
            {
                if (crop == null)
                    throw new ArgumentException("ไม่มีการระบุข้อมูลให้กับอ็อปเจ็ค");

                if (crop.Crop1.ToString() == null)
                    throw new ArgumentException("ไม่มีการระบุปีการเพาะปลูก");

                if (crop.CropName.ToString() == null)
                    throw new ArgumentException("ไม่มีการระบุ CropName");

                if (crop.StartCrop == null)
                    throw new ArgumentException("ไม่มีการระบุ StartCrop");

                if (crop.ModifiedByUser == null)
                    throw new ArgumentException("ไม่มีการระบุผู้บันทึกข้อมูล");

                if (uow.CropRepository.Query(c => c.Crop1 == crop.Crop1).Count() > 0)
                    throw new ArgumentException("มีข้อมูลนี้อยู่แล้วในระบบ");

                uow.CropRepository.Add(crop);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(Crop crop)
        {
            try
            {
                var cropFromDB = uow.CropRepository.GetSingle(c => c.Crop1 == crop.Crop1);

                if (cropFromDB == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                /// ยังมิได้ implement แบบละเอียด
                /// 
                cropFromDB.CropName = crop.CropName;
                cropFromDB.ModifiedByUser = crop.ModifiedByUser;
                cropFromDB.LastModifed = crop.LastModifed;

                if (crop.DefaultStatus == true)
                    SetDefault(crop.Crop1, crop.ModifiedByUser);

                uow.CropRepository.Update(cropFromDB);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(short crop)
        {
            try
            {
                var cropFromDB = uow.CropRepository.GetSingle(c => c.Crop1 == crop);

                if (cropFromDB == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.CropRepository.Remove(cropFromDB);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetDefault(short crop, string modifyUser)
        {
            try
            {
                var cropFromDB = uow.CropRepository.GetSingle(c => c.Crop1 == crop);

                if (cropFromDB == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                foreach (Crop item in uow.CropRepository.Query(c => c.DefaultStatus == true))
                {
                    item.DefaultStatus = false;
                    uow.CropRepository.Update(item);
                }

                cropFromDB.DefaultStatus = true;
                cropFromDB.ModifiedByUser = modifyUser;
                cropFromDB.LastModifed = DateTime.Now;

                uow.CropRepository.Update(cropFromDB);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Crop GetDefault()
        {
            try
            {
                return uow.CropRepository.GetSingle(c => c.DefaultStatus == true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Crop> GetAll()
        {
            return uow.CropRepository.Get().ToList();
        }

        public Crop GetSingle(short crop)
        {
            try
            {
                return uow.CropRepository.GetSingle(c => c.Crop1 == crop);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
