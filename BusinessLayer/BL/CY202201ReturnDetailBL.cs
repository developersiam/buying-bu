﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface ICY202201ReturnDetailBL
    {
        void Add(CY202201ReturnDetail model);
        void Update(Guid returnDetailID, int quantity, string modifiedUser);
        void Delete(Guid returnDetailID);
        CY202201ReturnDetail GetSingle(Guid returnDetailID);
        List<CY202201ReturnDetail> GetByReturnNo(string returnNo);
        List<CY202201ReturnDetail> GetByFarmer(short crop, string farmerCode);
    }

    public class CY202201ReturnDetailBL : ICY202201ReturnDetailBL
    {
        UnitOfWork uow;
        public CY202201ReturnDetailBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(CY202201ReturnDetail model)
        {
            try
            {
                if (string.IsNullOrEmpty(model.ReturnNo))
                    throw new ArgumentException("โปรดระบุ ReturnNo");

                if (string.IsNullOrEmpty(model.ModifiedUser))
                    throw new ArgumentException("โปรดระบุ ModifiedUser");

                if (string.IsNullOrEmpty(model.RecordUser))
                    throw new ArgumentException("โปรดระบุ RecordUser");

                if (model.Quantity <= 0)
                    throw new ArgumentException("โปรดระบุ Quantity โดยที่ Quantity ต้องมากกว่า 0");

                var returnNo = uow.CY202201ReturnRepository.GetSingle(x => x.ReturnNo == model.ReturnNo);
                if (returnNo == null)
                    throw new ArgumentException("ไม่พบ ReturnNo นี้ในระบบ");

                Helper.SystemDeadlinesHelper.BeforeStartBuyingDeadLine(returnNo.Crop, returnNo.FarmerCode);

                if (returnNo.IsFinish == true)
                    throw new ArgumentException("ReturnNo นี้ถูก Finish แล้ว");

                var documentList = BuyingFacade.BuyingDocumentBL()
                    .GetByFarmer(returnNo.Crop, returnNo.FarmerCode);
                if (documentList.Count() > 0)
                    throw new ArgumentException("มีการเริ่มบันทึกข้อมูลการขายไปแล้ว");

                var item = uow.CY202201ReturnItemRepository
                    .GetSingle(x => x.ReturnItemID == model.ReturnItemID);
                if (item == null)
                    throw new ArgumentException("ไม่พบรายการ ItemID " + " ในระบบ โปรดแจ้งผู้ดูแลระบบให้เพิ่มรายการนี้ในระบบ");

                model.ReturnDetailID = Guid.NewGuid();
                model.ModifiedDate = DateTime.Now;
                model.RecordDate = DateTime.Now;
                uow.CY202201ReturnDetailRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid returnDetailID)
        {
            try
            {
                var model = GetSingle(returnDetailID);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                var returnNo = uow.CY202201ReturnRepository.GetSingle(x => x.ReturnNo == model.ReturnNo);
                if (returnNo == null)
                    throw new ArgumentException("ไม่พบ ReturnNo นี้ในระบบ");

                if (returnNo.IsFinish == true)
                    throw new ArgumentException("ReturnNo นี้ถูก Finish แล้ว");

                var documentList = BuyingFacade.BuyingDocumentBL()
                    .GetByFarmer(returnNo.Crop, returnNo.FarmerCode);
                if (documentList.Count() > 0)
                    throw new ArgumentException("มีการเริ่มบันทึกข้อมูลการขายไปแล้ว");

                uow.CY202201ReturnDetailRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CY202201ReturnDetail> GetByFarmer(short crop, string farmerCode)
        {
            return uow.CY202201ReturnDetailRepository
                .Get(x => x.CY202201Return.Crop == crop &&
                x.CY202201Return.FarmerCode == farmerCode,
                null,
                x => x.CY202201Return,
                x => x.CY202201ReturnItem);
        }

        public List<CY202201ReturnDetail> GetByReturnNo(string returnNo)
        {
            return uow.CY202201ReturnDetailRepository
                .Get(x => x.ReturnNo == returnNo,
                null,
                x => x.CY202201Return,
                x => x.CY202201ReturnItem);
        }

        public CY202201ReturnDetail GetSingle(Guid returnDetailID)
        {
            return uow.CY202201ReturnDetailRepository.GetSingle(x => x.ReturnDetailID == returnDetailID);
        }

        public void Update(Guid returnDetailID, int quantity, string modifiedUser)
        {
            try
            {
                if (string.IsNullOrEmpty(modifiedUser))
                    throw new ArgumentException("โปรดระบุ ModifiedUser");

                if (quantity <= 0)
                    throw new ArgumentException("โปรดระบุ Quantity โดยที่ Quantity ต้องมากกว่า 0");

                var model = GetSingle(returnDetailID);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                var returnNo = uow.CY202201ReturnRepository.GetSingle(x => x.ReturnNo == model.ReturnNo);
                if (returnNo == null)
                    throw new ArgumentException("ไม่พบ ReturnNo นี้ในระบบ");

                if (returnNo.IsFinish == true)
                    throw new ArgumentException("ReturnNo นี้ถูก Finish แล้ว");

                var item = uow.CY202201ReturnItemRepository
                    .GetSingle(x => x.ReturnItemID == model.ReturnItemID);
                if (item == null)
                    throw new ArgumentException("ไม่พบรายการ ItemID " + " ในระบบ โปรดแจ้งผู้ดูแลระบบให้เพิ่มรายการนี้ในระบบ");

                var documentList = BuyingFacade.BuyingDocumentBL()
                    .GetByFarmer(returnNo.Crop, returnNo.FarmerCode);
                if (documentList.Count() > 0)
                    throw new ArgumentException("มีการเริ่มบันทึกข้อมูลการขายไปแล้ว");

                model.Quantity = quantity;
                model.ModifiedUser = modifiedUser;
                model.ModifiedDate = DateTime.Now;
                uow.CY202201ReturnDetailRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
