﻿using BusinessLayer.Model;
using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface ICPASampleCollectionBL
    {
        void Add(CPASampleCollection model);
        void Edit(CPASampleCollection model);
        void Delete(Guid id);
        List<CPASampleCollection> GetByFarmer(short crop, string farmerCode);
        List<sp_GetCPASampleCollectionByGapGroup_Result> GetByGAPGroup(string gapGroupCode);
    }

    public class CPASampleCollectionBL : ICPASampleCollectionBL
    {
        UnitOfWork uow;
        public CPASampleCollectionBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(CPASampleCollection model)
        {
            try
            {
                model.CollectionDate = DateTime.Now;
                uow.CPASampleCollectionRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var fromDB = uow.CPASampleCollectionRepository.GetSingle(c => c.Id == id);
                if (fromDB == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.CPASampleCollectionRepository.Remove(fromDB);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(CPASampleCollection model)
        {
            try
            {
                var fromDB = uow.CPASampleCollectionRepository.GetSingle(c => c.Id == model.Id);
                if (fromDB == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                fromDB.Position = model.Position;
                fromDB.Bag = model.Bag;
                fromDB.CollectionDate = DateTime.Now;

                uow.CPASampleCollectionRepository.Update(fromDB);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CPASampleCollection> GetByFarmer(short crop, string farmerCode)
        {
            try
            {
                return uow.CPASampleCollectionRepository
                    .Query(c => c.Crop == crop && c.FarmerCode == farmerCode).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<sp_GetCPASampleCollectionByGapGroup_Result> GetByGAPGroup(string gapGroupCode)
        {
            return StoreProcedureRepository.GetGetCPASampleCollectionByGapGroup(gapGroupCode);

            //var gapGroupLeader = BuyingFacade.GapGroupRepository().GetSingle(g => g.GAPGroupCode == gapGroupCode,
            //    g => g.GAPGroupLeaderByCrop,
            //    g => g.GAPGroupLeaderByCrop.GAPGroupLeader,
            //    g => g.GAPGroupLeaderByCrop.GAPGroupLeader.Person);

            //var leaderName = "";

            //if (gapGroupLeader != null)
            //    leaderName = gapGroupLeader.GAPGroupLeaderByCrop.GAPGroupLeader.Person.Prefix +
            //        gapGroupLeader.GAPGroupLeaderByCrop.GAPGroupLeader.Person.FirstName + " " +
            //        gapGroupLeader.GAPGroupLeaderByCrop.GAPGroupLeader.Person.LastName;

            //return (from reg in BuyingFacade.RegistrationBL().GetRegistrationByGAPGroup(gapGroupCode)
            //        from cpa in uow.CPASampleCollectionRepository.Get(g => g.RegistrationFarmer.GAPGroupCode == gapGroupCode)
            //        where reg.Crop == cpa.Crop && reg.FarmerCode == cpa.FarmerCode
            //        select new CPASampleCollectionFarmer
            //        {
            //            Crop = reg.Crop,
            //            FarmerCode = reg.FarmerCode,
            //            GAPGroupCode = reg.GAPGroupCode,
            //            Position = cpa.Position,
            //            Bag = cpa.Bag,
            //            CollectionDate = cpa.CollectionDate,
            //            Prefix = reg.Farmer.Person.Prefix,
            //            FirstName = reg.Farmer.Person.FirstName,
            //            LastName = reg.Farmer.Person.LastName,
            //            CitizenID = reg.Farmer.Person.CitizenID,
            //            LeaderName = leaderName
            //        }).ToList();
        }
    }
}
