﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IFarmerMachineAndFarmEquipmentBL
    {
        void Add(FarmerMachineAndFarmEquipment model);
        void Update(FarmerMachineAndFarmEquipment model);
        void Delete(short crop, string citizenID, Guid machineID);
        FarmerMachineAndFarmEquipment GetByID(short crop, string citizenID, Guid machineID);
        List<FarmerMachineAndFarmEquipment> GetByCitizenID(short crop, string citizenID);
    }

    public class FarmerMachineAndFarmEquipmentBL : IFarmerMachineAndFarmEquipmentBL
    {
        UnitOfWork uow;
        public FarmerMachineAndFarmEquipmentBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(FarmerMachineAndFarmEquipment model)
        {
            try
            {
                if (model.CitizenID == null)
                    throw new ArgumentException("โปรดระบุรหัสประจำตัวประชาชน.");

                if (model.MachineAndFarmEquipmentID == null)
                    throw new ArgumentException("โปรดระบุอุปกรณ์ที่จะบันทึก");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("โปรดระบุผู้บันทึกข้อมูล");

                if (GetByID(model.Crop, model.CitizenID, model.MachineAndFarmEquipmentID) != null)
                    throw new ArgumentException("มีข้อมูลนี้ซ้ำแล้วในระบบ!");

                model.ModifiedDate = DateTime.Now;

                uow.FarmerMachineAndFarmEquipmentRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(FarmerMachineAndFarmEquipment model)
        {
            try
            {
                if (model.CitizenID == null)
                    throw new ArgumentException("โปรดระบุรหัสประจำตัวประชาชน.");

                if (model.MachineAndFarmEquipmentID == null)
                    throw new ArgumentException("โปรดระบุอุปกรณ์ที่จะบันทึก");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("โปรดระบุผู้บันทึกข้อมูล");

                var edit = GetByID(model.Crop, model.CitizenID, model.MachineAndFarmEquipmentID);
                if (edit == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                edit.Quantity = model.Quantity;
                edit.LifeTime = model.LifeTime;
                edit.ModifiedDate = DateTime.Now;
                edit.ModifiedUser = model.ModifiedUser;

                uow.FarmerMachineAndFarmEquipmentRepository.Update(edit);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(short crop, string citizenID, Guid machineID)
        {
            try
            {
                var model = GetByID(crop, citizenID, machineID);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.FarmerMachineAndFarmEquipmentRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public FarmerMachineAndFarmEquipment GetByID(short crop, string citizenID, Guid machineID)
        {
            return uow.FarmerMachineAndFarmEquipmentRepository
                .GetSingle(fm => fm.Crop == crop &&
                fm.CitizenID == citizenID &&
                fm.MachineAndFarmEquipmentID == machineID,
                fm => fm.MachineAndFarmEquipment);
        }

        public List<FarmerMachineAndFarmEquipment> GetByCitizenID(short crop, string citizenID)
        {
            return uow.FarmerMachineAndFarmEquipmentRepository
                    .Query(fm => fm.Crop == crop &&
                    fm.CitizenID == citizenID,
                    null,
                    fm => fm.MachineAndFarmEquipment)
                    .ToList();
        }
    }
}
