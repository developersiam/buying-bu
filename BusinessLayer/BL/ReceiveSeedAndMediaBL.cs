﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;
using BusinessLayer.Model;

namespace BusinessLayer.BL
{
    public interface IReceiveSeedAndMediaBL
    {
        void Add(ReceiveSeedAndMedia model);
        void Update(ReceiveSeedAndMedia model);
        void Delete(short crop, string farmerCode, Guid seedAndMediaID);
        ReceiveSeedAndMedia GetById(short crop, string farmerCode, Guid seedAndMediaID);
        List<ReceiveSeedAndMedia> GetByFarmerAndCrop(short crop, string farmerCode);
    }

    public class ReceiveSeedAndMediaBL : IReceiveSeedAndMediaBL
    {
        UnitOfWork uow;
        public ReceiveSeedAndMediaBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(ReceiveSeedAndMedia model)
        {
            try
            {
                if (model.Crop.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล Crop");

                if (model.FarmerCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล FarmerCode");

                if (model.ModifiedBy == "")
                    throw new ArgumentException("ไม่พบข้อมูล ModifiedBy");

                if (model.Quantity.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล Quantity");

                if (model.SeedAndMediaID.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล SeedAndMediaID");

                if (model.SeedCode.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล SeedCode");

                if (model.LastModified == null)
                    throw new ArgumentException("ไม่พบข้อมูล วันที่จ่ายเมล็ดพันธุ์");

                if (GetById(model.Crop, model.FarmerCode, model.SeedAndMediaID) != null)
                    throw new ArgumentException("มีข้อมูลนี้ซ้ำแล้วในระบบ");

                uow.ReceiveSeedAndMediaRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Update(ReceiveSeedAndMedia model)
        {
            try
            {
                if (model.Crop.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล Crop");

                if (model.FarmerCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล FarmerCode");

                if (model.ModifiedBy == "")
                    throw new ArgumentException("ไม่พบข้อมูล ModifiedBy");

                if (model.Quantity.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล Quantity");

                if (model.SeedAndMediaID.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล SeedAndMediaID");

                if (model.SeedCode.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล SeedCode");

                if (GetById(model.Crop, model.FarmerCode, model.SeedAndMediaID) == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                uow.ReceiveSeedAndMediaRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(short crop, string farmerCode, Guid seedAndMediaID)
        {
            try
            {
                var receiveSeedAndMedia = GetById(crop, farmerCode, seedAndMediaID);
                if (receiveSeedAndMedia == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                uow.ReceiveSeedAndMediaRepository.Remove(receiveSeedAndMedia);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ReceiveSeedAndMedia GetById(short crop, string farmerCode, Guid seedAndMediaID)
        {
            return uow.ReceiveSeedAndMediaRepository
                .GetSingle(rs => rs.Crop == crop && 
                rs.FarmerCode == farmerCode &&
                rs.SeedAndMediaID == seedAndMediaID, 
                rs => rs.SeedAndMedia, 
                rs => rs.SeedVariety,
                rs => rs.SeedAndMedia.SeedAndMediaType, 
                rs => rs.SeedAndMedia.SeedAndMediaUnit);
        }
        public List<ReceiveSeedAndMedia> GetByFarmerAndCrop(short crop, string farmerCode)
        {
            return uow.ReceiveSeedAndMediaRepository
                .Query(rs => rs.Crop == crop && 
                rs.FarmerCode == farmerCode, 
                null,
                rs => rs.SeedAndMedia, 
                rs => rs.SeedVariety,
                rs => rs.SeedAndMedia.SeedAndMediaType, 
                rs => rs.SeedAndMedia.SeedAndMediaUnit)
                .ToList();
        }
    }
}