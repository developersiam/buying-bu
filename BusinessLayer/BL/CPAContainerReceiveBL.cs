﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface ICPAContainerReceiveBL
    {
        void Add(short crop, string farmerCode, string itemCode, int quantity, string user);
        void Edit(Guid receiveID, int quantity, string user);
        void Delete(Guid receiveID);
        List<CPAContainerReceive> GetByFarmerCode(short crop, string farmerCode);
        List<CPAContainerReceive> GetByCitizenID(short crop, string citizenID);
        CPAContainerReceive GetSingle(Guid id);
    }

    public class CPAContainerReceiveBL : ICPAContainerReceiveBL
    {
        UnitOfWork uow;
        public CPAContainerReceiveBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(short crop, string farmerCode, string itemCode, int quantity, string user)
        {
            try
            {
                if (quantity <= 0)
                    throw new ArgumentException("จำนวนที่รับ จะต้องมากกว่า 0");

                var item = uow.CPAContainerReceiveRepository
                    .GetSingle(x => x.Crop == crop &&
                    x.FarmerCode == farmerCode &&
                    x.Code == itemCode);
                if (item != null)
                    throw new ArgumentException("ชาวไร่รายนี้มีการบันทึกรับรายการนี้ไปแล้วในระบบ ไม่สามารถบันทึกซ้ำได้");

                uow.CPAContainerReceiveRepository
                    .Add(new CPAContainerReceive
                    {
                        ReceiveID = Guid.NewGuid(),
                        Crop = crop,
                        FarmerCode = farmerCode,
                        Code = itemCode,
                        Quantity = quantity,
                        ModifiedBy = user,
                        ModifiedDate = DateTime.Now
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid receiveID)
        {
            try
            {
                var receive = GetSingle(receiveID);
                if (receive == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                var returnList = uow.CPAContainerReturnRepository
                    .Get(x => x.ReceiveID == receiveID);
                if (returnList.Count() > 0)
                    throw new ArgumentException("รายการนี้มีข้อมูลการคืนเชื่อมโยงอยู่ ไม่สามารถลบได้");

                uow.CPAContainerReceiveRepository.Remove(receive);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(Guid receiveID, int quantity, string user)
        {
            try
            {
                if (quantity <= 0)
                    throw new ArgumentException("จำนวนที่รับ จะต้องมากกว่า 0");

                var item = GetSingle(receiveID);
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                item.Quantity = quantity;
                item.ModifiedDate = DateTime.Now;
                item.ModifiedBy = user;

                uow.CPAContainerReceiveRepository.Update(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CPAContainerReceive> GetByCitizenID(short crop, string citizenID)
        {
            return uow.CPAContainerReceiveRepository
                .Get(x => x.Crop == crop &&
                x.RegistrationFarmer.Farmer.CitizenID == citizenID
                , null
                , x => x.CPAContainerItem
                , x => x.RegistrationFarmer
                , x => x.RegistrationFarmer.Farmer);
        }

        public List<CPAContainerReceive> GetByFarmerCode(short crop, string farmerCode)
        {
            return uow.CPAContainerReceiveRepository
                .Get(x => x.Crop == crop &&
                x.FarmerCode == farmerCode
                , null
                , x => x.CPAContainerItem);
        }

        public CPAContainerReceive GetSingle(Guid id)
        {
            return uow.CPAContainerReceiveRepository.GetSingle(x => x.ReceiveID == id);
        }
    }
}
