﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;
using BusinessLayer.Model;
using BusinessLayer.Helper;

namespace BusinessLayer.BL
{
    public interface IFarmerProjectBL
    {
        void Add(FarmerProject farmerProject);
        void UpdateActualQuota(short crop, string farmerCode, string projectType, int newActualQuota, string modifiedBy);
        void UpdateExtraQuota(short crop, string farmerCode, string projectType, short newExtraQuota, string modifiedBy);
        void UpdateAll(short crop, string farmerCode, string projectType, byte actualRai, short extraQuota, string modifiedBy);
        void Delete(short crop, string farmerCode, string projectType);
        FarmerProject GetSingle(short crop, string farmerCode, string projectType);
        List<FarmerProject> GetByFarmer(short crop, string farmerCode);
        List<FarmerProject> GetByExtensionAgentCode(string extentionAgentCode, short crop);
        List<ProjectType> GetAll();
    }

    public class FarmerProjectBL : IFarmerProjectBL
    {
        UnitOfWork uow;
        public FarmerProjectBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(FarmerProject model)
        {
            try
            {
                if (model.ActualRai <= 0)
                    throw new ArgumentException("จำนวนไร่ที่ต้องการแบ่งโปรเจคจะต้องมากกว่า 0");

                if (string.IsNullOrEmpty(model.ModifiedByUser))
                    throw new ArgumentException("ModifiedByUser cannot be empty.");

                var project = GetSingle(model.Crop, model.FarmerCode, model.ProjectType);
                if (project != null)
                    throw new ArgumentException("มีข้อมูล Project " + model.ProjectType + " นี้แล้วในระบบ");

                var regisModel = BuyingFacade.RegistrationBL().GetSingle(model.Crop, model.FarmerCode);
                if (regisModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รหัส " + model.FarmerCode + " ในปี " + model.Crop);

                var totalQuota = GetByFarmer(model.Crop, model.FarmerCode).Sum(x => x.ActualRai);
                if (model.ActualRai + totalQuota > regisModel.QuotaFromSignContract)
                    throw new ArgumentException("โควต้ารวมในทุกโปรเจค " + totalQuota.ToString("N0") + " ไร่"+
                        " รวมกับของใหม่ " + model.ActualRai.ToString("N0") + " ไร่" +
                        " รวมเป็น " + ((int)(model.ActualRai + totalQuota)).ToString("N0") + " ไร่" +
                        " เกินจากที่ระบุไว้ในสัญญา " + ((int)regisModel.QuotaFromSignContract).ToString("N0") + " ไร่" +
                        " ไม่สามารถเพิ่มโควต้าในโปรเจคนี้ได้");

                uow.FarmerProjectRepository.
                    Add(new FarmerProject
                    {
                        Crop = model.Crop,
                        FarmerCode = model.FarmerCode.ToUpper(),
                        ProjectType = model.ProjectType,
                        ActualRai = model.ActualRai,
                        ExtraQuota = model.ExtraQuota,
                        ModifiedByUser = model.ModifiedByUser,
                        LastModified = DateTime.Now
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateActualQuota(short crop, string farmerCode, string projectType, int newActualQuota, string modifiedBy)
        {
            try
            {
                if (newActualQuota <= 0)
                    throw new ArgumentException("จำนวนไร่ที่ต้องการแบ่งโปรเจคจะต้องมากกว่า 0");

                var farmerProject = GetSingle(crop, farmerCode, projectType);
                if (farmerProject == null)
                    throw new ArgumentException("ไม่พบข้อมูล Project " + projectType + " ของชาวไร่รายนี้ในระบบ");

                //If increase quota.
                if (newActualQuota > farmerProject.ActualRai)
                {
                    var regisModel = BuyingFacade.RegistrationBL().GetSingle(crop, farmerCode);
                    if (regisModel == null)
                        throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รหัส " + farmerProject.FarmerCode + " ในปี " + farmerProject.Crop);

                    //if over quota after update
                    int sumOfActualRaiInAllProject = uow.FarmerProjectRepository
                        .Query(f => f.Crop == crop && f.FarmerCode == farmerCode)
                        .Sum(x => x.ActualRai);

                    if ((sumOfActualRaiInAllProject - farmerProject.ActualRai) + newActualQuota > regisModel.QuotaFromSignContract)
                        throw new ArgumentException("การเพิ่มจำนวนไร่ในโปรเจค " + projectType +
                            " ของชาวไร่รหัส " + farmerCode + " นี้ เกินจากจำนวนไร่ที่ลงทะเบียนไว้ โปรดตรวจสอบข้อมูลอีกครั้ง");

                    farmerProject.ActualRai = Convert.ToByte(newActualQuota);
                    farmerProject.ModifiedByUser = modifiedBy;
                    farmerProject.LastModified = DateTime.Now;

                    uow.FarmerProjectRepository.Update(farmerProject);
                }
                else
                {
                    //เหตุการณ์นี้เกิดขึ้นในกรณีที่มีการลดจำนวนโตวต้าในโปรเจคที่กำหนด
                    var soldInProject = FarmerProjectHelper.GetQuotaAndSoldByProject(crop, farmerCode, projectType);

                    decimal kgPerRaiFromActualRai = farmerProject.ActualRai * 400;
                    decimal totalSoldInProject = Convert.ToDecimal(soldInProject.Sold);
                    decimal balanceQuotaAfterDecrease = newActualQuota * 400;

                    if (balanceQuotaAfterDecrease < totalSoldInProject)
                        throw new ArgumentException("ไม่สามารถลดจำนวนโควต้าลงได้ เนื่องจากมีการขายยาไปจำนวน " + totalSoldInProject +
                            " kg.แล้ว ซึ่งมากกว่าจำนวนโควต้าที่ต้องการปรับแก้คือ " + balanceQuotaAfterDecrease +
                            " Kg. (" + newActualQuota + " rai)");

                    farmerProject.ActualRai = Convert.ToByte(newActualQuota);
                    farmerProject.ModifiedByUser = modifiedBy;
                    farmerProject.LastModified = DateTime.Now;

                    uow.FarmerProjectRepository.Update(farmerProject);
                }

                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateExtraQuota(short crop, string farmerCode, string projectType, short newExtraQuota, string modifiedBy)
        {
            try
            {
                if (newExtraQuota <= 0)
                    throw new ArgumentException("จำนวน Extra quota จะต้องมากกว่า 0");

                var model = FarmerProjectHelper.GetQuotaAndSoldByFarmerVersion2(crop, farmerCode);
                var farmerProject = model.SingleOrDefault(x => x.ProjectType == projectType);

                if (farmerProject == null)
                    throw new ArgumentException("ไม่พบข้อมูล Project " + projectType + " ของชาวไร่รายนี้ในระบบ");

                if ((farmerProject.ActualKgPerRai + newExtraQuota) - farmerProject.Sold <= 0)
                    throw new ArgumentException("ไม่สามารถลดจำนวนโควต้าลงได้ เนื่องจากมีการขายยาไปจำนวน " + farmerProject.Sold +
                        " kg. มากกว่าจำนวนโควต้าที่ต้องการกำหนดใหม่คือ " + (farmerProject.ActualKgPerRai + newExtraQuota));

                var updateModel = GetSingle(crop, farmerCode, projectType);

                updateModel.ExtraQuota = newExtraQuota;
                updateModel.ModifiedByUser = modifiedBy;
                updateModel.LastModified = DateTime.Now;

                uow.FarmerProjectRepository.Update(updateModel);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(short crop, string farmerCode, string projectType)
        {
            try
            {
                var farmerProject = GetSingle(crop, farmerCode, projectType);
                if (farmerProject == null)
                    throw new ArgumentException("ไม่พบข้อมูล Project " + projectType + " ของชาวไร่รายนี้ในระบบ");

                var sold = BuyingFacade.BuyingBL()
                    .GetByFarmerProject(crop, farmerCode, projectType)
                    .Sum(x => x.Weight);

                if (sold > 0)
                    throw new ArgumentException("ชาวไร่รายนี้มีการขายใบยาในโปรเจคนี้ไปแล้วจำนวน " +
                        ((decimal)sold).ToString("N1") + " กก." +
                        " ไม่สามารถลบโปรเจคนี้ออกจากระบบได้");

                uow.FarmerProjectRepository.Remove(farmerProject);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public FarmerProject GetSingle(short crop, string farmerCode, string projectType)
        {
            return uow.FarmerProjectRepository
                .GetSingle(f => f.Crop == crop &&
                f.FarmerCode == farmerCode &&
                f.ProjectType == projectType,
                f => f.RegistrationFarmer,
                f => f.RegistrationFarmer.Farmer,
                f => f.RegistrationFarmer.Farmer.Person);
        }

        public List<FarmerProject> GetByFarmer(short crop, string farmerCode)
        {
            return uow.FarmerProjectRepository
                .Query(f => f.Crop == crop &&
                f.FarmerCode == farmerCode)
                .ToList();
        }

        public List<ProjectType> GetAll()
        {
            return uow.ProjectTypeRepository
                .Get()
                .OrderBy(x => x.ProjectType1)
                .ToList();
        }

        public List<FarmerProject> GetByExtensionAgentCode(string extentionAgentCode, short crop)
        {
            return uow.FarmerProjectRepository
                .Get(x => x.RegistrationFarmer.Farmer.Supplier == extentionAgentCode &&
                x.Crop == crop,
                null,
                x => x.RegistrationFarmer,
                x => x.RegistrationFarmer.Farmer,
                x => x.RegistrationFarmer.Farmer.Person);
        }

        public void UpdateAll(short crop, string farmerCode, string projectType, byte actualRai, short extraQuota, string modifiedBy)
        {
            try
            {
                var project = GetSingle(crop, farmerCode, projectType);
                if (project == null)
                    throw new ArgumentException("ไม่พบ farmer project นี้ในระบบ");

                var totalQuota = BuyingFacade.FarmerProjectBL()
                    .GetByFarmer(crop, farmerCode).Sum(x => x.ActualRai);
                if (totalQuota - project.ActualRai + actualRai > project.RegistrationFarmer.QuotaFromSignContract)
                    throw new ArgumentException("โควต้ารวมในทุก project เมื่อแก้ไขข้อมูลแล้วจะอยู่ที่ " +
                        totalQuota + " ไร่ เกินจากที่ระบุในสัญญาคือ " +
                        project.RegistrationFarmer.QuotaFromSignContract +
                        " ไร่ ไม่สามารถแก้ไขโควต้าใน project นี้ได้");

                var kgPerRai = BuyingFacade.KilogramsPerRaiConfigurationBL()
                    .GetSingle(crop, project.RegistrationFarmer.Farmer.Supplier);
                if (kgPerRai == null)
                    throw new ArgumentException("ยังไม่มีการกำหนด kg/rai ในระบบ");

                var quotaKg = project.ActualRai * kgPerRai.KilogramsPerRai;
                var sold = BuyingFacade.BuyingBL()
                    .GetByFarmerProject(crop, farmerCode, projectType)
                    .Sum(x => x.Weight);

                if (actualRai != project.ActualRai)
                {
                    var newRequest = actualRai * kgPerRai.KilogramsPerRai;
                    if (sold > newRequest)
                        throw new ArgumentException("ชาวไร่รายนี้ขายใบยาใน project นี้ไปแล้ว " + ((decimal)sold).ToString("N1") +
                            " กก. จำนวนโควต้าใหม่ที่ต้องการแก้ไขคือ " + newRequest.ToString("N0") +
                            " กก. น้อยกว่าจำนวนที่ขายไปแล้ว ไม่สามารถแก้ไขจำนวนไร่ใน farmer project");
                }
                else
                {
                    if (extraQuota < project.ExtraQuota)
                        throw new ArgumentException("โควต้าส่วนเกิน จะต้องไม่มีการปรับลดลงจากเดิม หากต้องการปรับลดโปรดติดต่อผู้ดูแลระบบ");
                }                

                project.ActualRai = actualRai;
                project.ExtraQuota = extraQuota;
                project.ModifiedByUser = modifiedBy;
                project.LastModified = DateTime.Now;

                uow.FarmerProjectRepository.Update(project);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
