﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IFarmerCreditProjectBL
    {
        void Add(FarmerCreditProject model);
        void Edit(FarmerCreditProject model);
        void Delete(Guid id);
        List<CreditProject> GetCreditProjects();
        List<FarmerCreditProject> GetByFarmer(short crop, string farmerCode);
        FarmerCreditProject GetById(Guid id);
    }

    public class FarmerCreditProjectBL : IFarmerCreditProjectBL
    {
        UnitOfWork uow;
        public FarmerCreditProjectBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(FarmerCreditProject model)
        {
            try
            {
                if (model.FarmerCode == null || model.FarmerCode == "")
                    throw new ArgumentException("Please input farmer code.");
                if (model.ModifiedUser == null || model.FarmerCode == "")
                    throw new ArgumentException("Please input Modified User.");
                if (model.CreateUser == null || model.FarmerCode == "")
                    throw new ArgumentException("Please input Create User.");
                if (model.CreateDate == null)
                    throw new ArgumentException("Please input Create Date.");
                //if (model.Rai < 1)
                //    throw new ArgumentException("Rai must be more than zero.");

                model.Id = Guid.NewGuid();
                model.ModifiedDate = DateTime.Now;

                uow.FarmerCreditProjectRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var model = uow.FarmerCreditProjectRepository.GetSingle(f => f.Id == id);
                if (model == null)
                    throw new ArgumentException("Find not found.");

                uow.FarmerCreditProjectRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(FarmerCreditProject model)
        {
            try
            {
                if (uow.FarmerCreditProjectRepository.GetSingle(f => f.Id == model.Id) == null)
                    throw new ArgumentException("Find not found.");

                if (model.ModifiedUser == null || model.FarmerCode == "")
                    throw new ArgumentException("Please input Modified User.");
                //if (model.Rai < 1)
                //    throw new ArgumentException("Rai must be more than zero.");

                model.ModifiedDate = DateTime.Now;

                uow.FarmerCreditProjectRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FarmerCreditProject> GetByFarmer(short crop, string farmerCode)
        {
            return uow.FarmerCreditProjectRepository
                .Query(f => f.Crop == crop &&
                f.FarmerCode == farmerCode,
                null,
                f => f.CreditProject).ToList();
        }

        public FarmerCreditProject GetById(Guid id)
        {
            return uow.FarmerCreditProjectRepository.GetSingle(f => f.Id == id);
        }

        public List<CreditProject> GetCreditProjects()
        {
            return uow.CreditProjectRepository.Get().ToList();
        }
    }
}
