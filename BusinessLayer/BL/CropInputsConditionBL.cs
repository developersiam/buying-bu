﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface ICropInputsConditionBL
    {
        void Add(CropInputsCondition model);
        void Edit(CropInputsCondition model);
        void Delete(Guid conditionID);
        List<CropInputsCondition> GetByPackage(short crop, string supplierCode);
        List<CropInputsCondition> GetBySupplierCode(short crop, string supplierCode);
        CropInputsCondition GetSingle(Guid conditionID);
    }

    public class CropInputsConditionBL : ICropInputsConditionBL
    {
        UnitOfWork uow;
        public CropInputsConditionBL()
        {
            uow = new UnitOfWork();
        }

        public List<CropInputsCondition> GetBySupplierCode(short crop, string supplierCode)
        {
            return uow.CropInputsConditionRepository
                .Get(x => x.Crop == crop &&
                x.SupplierCode == supplierCode)
                .ToList();
        }

        public List<CropInputsCondition> GetByPackage(short crop, string supplierCode)
        {
            return uow.CropInputsConditionRepository
                .Get(x => x.Crop == crop &&
                x.SupplierCode == supplierCode)
                .ToList();
        }

        public CropInputsCondition GetSingle(Guid conditionID)
        {
            return uow.CropInputsConditionRepository
                .GetSingle(x => x.ConditionID == conditionID);
        }

        public void Add(CropInputsCondition model)
        {
            try
            {
                if (model.IsNotToInvoice == true && model.UnitPrice != 0)
                    throw new Exception("รายการที่บันทึกข้อมูลไว้ในระบบเฉยๆ unit price จะต้องเป็น 0");

                /// Check item in the inventory stock.
                /// 
                if (model.IsReferToInventory == true)
                {
                    var balanceStock = AGMInventorySystemBL
                        .BLServices
                        .material_transactionBL()
                        .GetByItem(model.ItemID);

                    if (balanceStock.Sum(x => x.quantity) <= 0)
                        throw new Exception("ไม่มี item id " + model.ItemID + " นี้เหลืออยู่ใน AGM Inventory.");
                }

                var existsList = uow.CropInputsConditionRepository
                    .Get(x => x.Crop == model.Crop &&
                    x.SupplierCode == model.SupplierCode &&
                    x.ItemID == model.ItemID);
                if (existsList.Count() > 0)
                    throw new Exception("มีรายการสินค้ารหัส item id " + model.ItemID + " นี้แล้วในระบบ");

                model.ConditionID = Guid.NewGuid();
                model.ModifiedDate = DateTime.Now;

                uow.CropInputsConditionRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(CropInputsCondition model)
        {
            try
            {
                if (model.IsNotToInvoice == true && model.UnitPrice != 0)
                    throw new Exception("รายการที่บันทึกข้อมูลไว้ในระบบเฉยๆ unit price จะต้องเป็น 0");

                var editModel = GetSingle(model.ConditionID);
                if (editModel == null)
                    throw new Exception("ไม่พบข้อมูลในระบบ");

                /// Check item in the inventory stock.
                /// 
                if (model.IsReferToInventory == true)
                {
                    var balanceStock = AGMInventorySystemBL
                        .BLServices
                        .material_transactionBL()
                        .GetByItem(model.ItemID);

                    if (balanceStock.Sum(x => x.quantity) <= 0)
                        throw new Exception("ไม่มี item id " + model.ItemID + " นี้เหลืออยู่ใน AGM Inventory.");
                }

                uow.CropInputsConditionRepository.Update(editModel);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid conditionID)
        {
            try
            {
                var model = GetSingle(conditionID);
                if (model == null)
                    throw new Exception("ไม่พบข้อมูลในระบบ");

                var packages = uow.CropInputsPackageDetailsRepository
                    .Get(x => x.ConditionID == conditionID);
                if (packages.Count() > 0)
                    throw new Exception("ไม่สามารถลบข้อมูลได้เนื่องจาก Item นี้ถูกนำไปใช้ใน package แล้ว");

                var list = uow.InvoiceDetailRepository
                    .Get(x => x.Invoice.Crop == model.Crop &&
                    x.ConditionID == model.ConditionID
                    , null
                    , x => x.Invoice);

                if (list.Count() > 0)
                    throw new Exception("ไม่สามารถลบข้อมูลได้เนื่องจาก Item นี้ถูกนำไปใช้ใน invoice แล้ว");

                uow.CropInputsConditionRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
