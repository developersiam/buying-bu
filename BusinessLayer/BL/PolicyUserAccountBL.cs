﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IPolicyUserAccountBL
    {
        void Add(string username, string createBy);
        void Delete(string Username);
        AppPolicyUser GetSingle(string Username);
        List<AppPolicyUser> GetAll();
    }

    public class PolicyUserAccountBL : IPolicyUserAccountBL
    {
        static Guid appID = Guid.Parse("92ACD5C1-69CB-42F4-AEBF-160DBB445082");

        IUnitOfWork _uow;

        public PolicyUserAccountBL()
        {
            _uow = new UnitOfWork();
        }

        public void Add(string username, string createBy)
        {
            try
            {
                if (string.IsNullOrEmpty(username))
                    throw new ArgumentException(nameof(username) + " can not be empty.");

                if (string.IsNullOrEmpty(createBy))
                    throw new ArgumentException(nameof(createBy) + " can not be empty.");

                if (_uow.AppPolicyUserRepository
                    .Get(x => x.Username == username)
                    .Count() <= 0)
                {
                    _uow.AppPolicyUserRepository
                        .Add(new AppPolicyUser
                        {
                            Username = username.ToLower(),
                            CreateBy = createBy,
                            CreateDate = DateTime.Now
                        });
                    _uow.Save();
                }

                /// If the system not have any role. 
                /// The system should be created a new role (role name "public").
                /// The public role is default role for all new user.
                /// 
                if (_uow.AppPolicyRoleRepository
                    .Get(x => x.ApplicationID == appID)
                    .Count() <= 0)
                {
                    _uow.AppPolicyRoleRepository
                        .Add(new AppPolicyRole
                        {
                            ApplicationID = appID,
                            RoleID = Guid.NewGuid(),
                            RoleName = "public",
                            CreateDate = DateTime.Now,
                            CreateBy = "system",
                            ModifiedBy = "system",
                            ModifiedDate = DateTime.Now
                        });
                    _uow.Save();
                }

                if (_uow.AppPolicyUserRoleRepository
                    .Get(x => x.Username == username
                    && x.AppPolicyRole.ApplicationID == appID)
                    .Count() > 0)
                    return;

                var roleID = _uow.AppPolicyRoleRepository
                    .GetSingle(x => x.ApplicationID == appID && x.RoleName == "public")
                    .RoleID;

                _uow.AppPolicyUserRoleRepository
                    .Add(new AppPolicyUserRole
                    {
                        Username = username,
                        RoleID = roleID,
                        CreateBy = createBy,
                        CreateDate = DateTime.Now,
                    });
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string Username)
        {
            try
            {
                var model = GetSingle(Username);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                var list = model.AppPolicyUserRoles
                    .Where(x => x.AppPolicyRole.ApplicationID == appID)
                    .ToList();

                if (model.AppPolicyUserRoles
                    .Where(x => x.AppPolicyRole.ApplicationID == appID)
                    .Count() > 0)
                    throw new ArgumentException("This account cannot be deleted." +
                        "They have at least 1 association role in the system." +
                        "Please delete all association role before delete this account.");

                _uow.AppPolicyUserRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AppPolicyUser> GetAll()
        {
            var list = _uow.AppPolicyUserRoleRepository
                .Get(x => x.AppPolicyRole.ApplicationID == appID
                , null
                , x => x.AppPolicyRole
                , x => x.AppPolicyUser)
                .Select(x => new AppPolicyUser
                {
                    Username = x.Username,
                    CreateBy = x.AppPolicyUser.CreateBy,
                    CreateDate = x.AppPolicyUser.CreateDate
                })
                .GroupBy(x => new { x.Username, x.CreateBy, x.CreateDate })
                .Select(x => new AppPolicyUser
                {
                    Username = x.Key.Username,
                    CreateBy = x.Key.CreateBy,
                    CreateDate = x.Key.CreateDate
                })
                .ToList();

            return list;
        }

        public AppPolicyUser GetSingle(string Username)
        {
            return _uow.AppPolicyUserRepository
                .GetSingle(x => x.Username == Username);
        }
    }
}
