﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IIrrigationWaterBL
    {
        void Add(IrrigationWater model);
        void Delete(Guid id);
        IrrigationWater GetByID(Guid id);
        IrrigationWater GetSingle(Guid irrigrationWaterTypeID, string citizenID, short crop);
        List<IrrigationWater> GetByCitizenID(string citizenID, short crop);
        List<IrrigationWaterType> GetIrrigationWaterTypes();
    }
    public class IrrigationWaterBL : IIrrigationWaterBL
    {
        UnitOfWork uow;
        public IrrigationWaterBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(IrrigationWater model)
        {
            try
            {
                if (model.CitizenID == null)
                    throw new ArgumentException("โปรดระบุรหัสประจำตัวประชาชน");

                if (model.IrrigationWaterTypeID == null)
                    throw new ArgumentException("โปรดระบุประเภทแหล่งน้ำ");

                if (model.ModifiedBy == null)
                    throw new ArgumentException("โปรดระบุผู้บันทึกข้อมูล");

                var irrigrationWaterType = uow.IrrigationWaterTypeRepository
                    .GetSingle(x => x.IrrigationWaterTypeID == model.IrrigationWaterTypeID);
                if (irrigrationWaterType == null)
                    throw new ArgumentException("ไม่พบประเภทแหล่งน้ำนี้ในระบบ โปรดตรวจสอบกับแผนกไอที");

                var type = irrigrationWaterType.IrrigationWaterTypeName;
                var defaultCrop = BuyingFacade.CropBL().GetDefault();
                if (defaultCrop == null)
                    throw new ArgumentException("ไม่มีการตั้งค่า crop ปัจจุบันในระบบ โปรดติดต่อผู้ดูแลระบบ");

                ///ถ้ากรณีแหล่งน้ำไม่ใช่ประเภทน้ำบาดาล ให้กำหนดค่าเกี่ยวกับใบอนุญาตเป็น null.
                ///
                if (!type.Contains("บาดาล"))
                {
                    model.GroundWaterLicence = null;
                    model.GroundWaterLicenceRemark = null;
                }

                if (type.Contains("บาดาล"))
                {
                    if (model.IrrigationWaterDeep == null)
                        throw new ArgumentException("กรณีน้ำบาดาล จะต้องระบุความลึกด้วย");

                    if (model.IrrigationWaterDeep <= 0)
                        throw new ArgumentException("ระดับความลึกของแหล่งน้ำ จะต้องมีค่ามากกว่า 0");
                }

                var record = GetSingle(model.IrrigationWaterTypeID, model.CitizenID, defaultCrop.Crop1);
                if (record != null)
                    throw new ArgumentException("มีการบันทึกข้อมูลแหล่งน้ำประเภท " + type +
                        " แล้วในปี " + defaultCrop.Crop1);

                uow.IrrigationWaterRepository
                    .Add(new IrrigationWater
                    {
                        FarmerIrrigationWaterID = Guid.NewGuid(),
                        CitizenID = model.CitizenID,
                        Crop = defaultCrop.Crop1,
                        IrrigationWaterTypeID = model.IrrigationWaterTypeID,
                        IrrigationWaterDeep = model.IrrigationWaterDeep,
                        RiverName = model.RiverName,
                        GroundWaterLicence = model.GroundWaterLicence,
                        GroundWaterLicenceRemark = model.GroundWaterLicenceRemark,
                        ModifiedDate = DateTime.Now,
                        ModifiedBy = model.ModifiedBy
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var model = GetByID(id);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลที่ต้องการลบ");

                uow.IrrigationWaterRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteIrrigationWater(Guid id)
        {
            try
            {
                var model = GetByID(id);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.IrrigationWaterRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<IrrigationWater> GetByCitizenID(string citizenID, short crop)
        {
            return uow.IrrigationWaterRepository
                .Query(i => i.Crop == crop &&
                i.CitizenID == citizenID,
                null,
                i => i.IrrigationWaterType)
                .ToList();
        }

        public IrrigationWater GetByID(Guid id)
        {
            return uow.IrrigationWaterRepository
                .GetSingle(i => i.FarmerIrrigationWaterID == id,
                i => i.IrrigationWaterType);
        }

        public List<IrrigationWaterType> GetIrrigationWaterTypes()
        {
            return uow.IrrigationWaterTypeRepository.Get().ToList();
        }

        public IrrigationWater GetSingle(Guid irrigrationWaterTypeID, string citizenID, short crop)
        {
            return uow.IrrigationWaterRepository
                .GetSingle(x => x.Crop == crop &&
                x.CitizenID == citizenID &&
                x.IrrigationWaterTypeID == irrigrationWaterTypeID
            );
        }
    }
}
