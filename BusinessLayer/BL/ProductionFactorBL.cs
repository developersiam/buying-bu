﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;
using BusinessLayer.Model;

namespace BusinessLayer.BL
{
    public interface IProductionFactorBL
    {
        void AddFerilizerAndChemical(FertilizerAndChemical model);
        void UpdateFertilizerAndChemical(FertilizerAndChemical model);
        void DeleteFertilizerAndChemical(Guid id);
        FertilizerAndChemical GetFertilizerAndChemicalById(Guid id);
        List<FertilizerAndChemical> GetFertilizerAndChemicalByArea(short crop, string areaCode);
        List<FertilizerAndChemical> GetFertilizerAndChemicalBySupplier(short crop, string supplierCode);
        List<FertilizerAndChemicalType> GetFertilizerAndChemicalType();
        List<FertilizerAndChemicalUnit> GetFertilizerAndChemicalUnit();
        void AddSeedAndMedia(SeedAndMedia model);
        void UpdateSeedAndMedia(SeedAndMedia model);
        void DeleteSeedAndMedia(Guid seedAndMediaId);
        List<SeedAndMedia> GetSeedAndMediaByCrop(short crop);
        SeedAndMedia GetSeedAndMediaById(Guid id);
        List<SeedAndMedia> GetSeedAndMediaByArea(short crop, string areaCode);
        List<SeedAndMediaUnit> GetSeedAndMediaUnit();
        List<SeedAndMediaType> GetSeedAndMediaType();
        List<SeedVariety> GetSeedVarietyByCrop(short crop);
        SeedVariety GetSeedVarietyByID(Guid seedVarietyCode);
        void AddSeedVariety(SeedVariety model);
        void UpdateSeedVariety(SeedVariety model);
        void DeleteSeedVariety(Guid seedVarietyCode);
        List<InputFactor> GetInputFactorBySupplierCode(short crop, string supplierCode);
        InputFactor GetInputFactorByID(Guid id);
        void AddInputFactor(InputFactor model);
        void UpdateInputFactor(InputFactor model);
        void DeleteInputFactor(Guid id);
        List<m_ReceiveInputFactor> GetReceiveInputFactorInfoByFarmer(short crop, string farmerCode);
        List<ReceiveInputFactor> GetReceiveInputFactorByFarmer(short crop, string farmerCode);
        ReceiveInputFactor GetReceiveInputFactorByID(short crop, string farmerCode, Guid inputFactorID);
        void AddReceiveInputFacor(ReceiveInputFactor model);
        void UpdateReceiveInputFacor(ReceiveInputFactor model);
        void DeleteReceiveInputFacor(ReceiveInputFactor model);
    }

    public class ProductionFactorBL : IProductionFactorBL
    {
        UnitOfWork uow;
        public ProductionFactorBL()
        {
            uow = new UnitOfWork();
        }

        public void AddFerilizerAndChemical(FertilizerAndChemical model)
        {
            try
            {
                if (model.AreaCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล AreaCode");

                if (model.Crop.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล Crop");

                if (model.Name == "")
                    throw new ArgumentException("ไม่พบข้อมูล Name");

                if (model.UnitCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล UnitCode");

                if (model.TypeCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล TypeCode");

                if (model.UnitPrice.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล UnitPrice");

                if (model.ModifiedBy == "")
                    throw new ArgumentException("ไม่พบข้อมูล ModifiedBy");

                if (model.SupplierCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล SupplierCode");

                if (model.UnitPrice < 0)
                    throw new ArgumentException("ราคาต่อหน่วยไม่ควรต่ำกว่า 0");

                uow.FertilizerAndChemicalRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateFertilizerAndChemical(FertilizerAndChemical model)
        {
            try
            {
                if (model.ID.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล ID");

                if (model.AreaCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล AreaCode");

                if (model.Crop.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล Crop");

                if (model.Name == "")
                    throw new ArgumentException("ไม่พบข้อมูล Name");

                if (model.UnitCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล UnitCode");

                if (model.TypeCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล TypeCode");

                if (model.UnitPrice.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล UnitPrice");

                if (model.ModifiedBy == "")
                    throw new ArgumentException("ไม่พบข้อมูล ModifiedBy");

                if (model.SupplierCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล SupplierCode");

                if (model.UnitPrice < 0)
                    throw new ArgumentException("ราคาต่อหน่วยไม่ควรต่ำกว่า 0");

                var fertAndChe = GetFertilizerAndChemicalById(model.ID);
                if (fertAndChe == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.FertilizerAndChemicalRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteFertilizerAndChemical(Guid id)
        {
            try
            {
                if (id.ToString() == "" || id == null)
                    throw new ArgumentException("ไม่พบข้อมูล ID");

                var fertAndChe = GetFertilizerAndChemicalById(id);
                if (fertAndChe == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.FertilizerAndChemicalRepository.Remove(fertAndChe);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public FertilizerAndChemical GetFertilizerAndChemicalById(Guid id)
        {
            return uow.FertilizerAndChemicalRepository
                .GetSingle(f => f.ID == id,
                f => f.FertilizerAndChemicalType,
                f => f.FertilizerAndChemicalUnit);
        }

        public List<FertilizerAndChemical> GetFertilizerAndChemicalByArea(short crop, string areaCode)
        {
            return uow.FertilizerAndChemicalRepository
                .Query(f => f.Crop == crop &&
                f.AreaCode == areaCode, null,
                f => f.FertilizerAndChemicalType,
                f => f.FertilizerAndChemicalUnit)
                .ToList();
        }

        public List<FertilizerAndChemical> GetFertilizerAndChemicalBySupplier(short crop, string supplierCode)
        {
            return uow.FertilizerAndChemicalRepository
                .Query(f => f.Crop == crop &&
                f.SupplierCode == supplierCode,
                null, f => f.FertilizerAndChemicalType,
                f => f.FertilizerAndChemicalUnit)
                .ToList();
        }

        public List<FertilizerAndChemicalType> GetFertilizerAndChemicalType()
        {
            return uow.FertilizerAndChemicalTypeRepository.Get().ToList();
        }

        public List<FertilizerAndChemicalUnit> GetFertilizerAndChemicalUnit()
        {
            return uow.FertilizerAndChemicalUnitRepository.Get().ToList();
        }

        public void AddSeedAndMedia(SeedAndMedia model)
        {
            try
            {
                if (model.Crop.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล Crop");

                if (model.Name == "")
                    throw new ArgumentException("ไม่พบข้อมูล Name");

                if (model.TypeCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล TypeCode");

                if (model.AreaCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล AreaCode");

                if (model.ModifiedBy == "")
                    throw new ArgumentException("ไม่พบข้อมูล ModifiedBy");

                if (model.UnitCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล UnitCode");

                if (model.UnitPrice.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล UnitPrice");

                if (model.UnitPrice < 0)
                    throw new ArgumentException("ราคาต่อหน่วยจะต้องไม่น้อยกว่า 0");

                uow.SeedAndMediaRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSeedAndMedia(SeedAndMedia model)
        {
            try
            {
                if (model.ID == null)
                    throw new ArgumentException("ไม่พบข้อมูล ID");

                if (model.Crop.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล Crop");

                if (model.Name == "")
                    throw new ArgumentException("ไม่พบข้อมูล Name");

                if (model.TypeCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล TypeCode");

                if (model.AreaCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล AreaCode");

                if (model.ModifiedBy == "")
                    throw new ArgumentException("ไม่พบข้อมูล ModifiedBy");

                if (model.UnitCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล UnitCode");

                if (model.UnitPrice.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล UnitPrice");

                if (model.UnitPrice < 0)
                    throw new ArgumentException("ราคาต่อหน่วยจะต้องไม่น้อยกว่า 0");

                if (GetSeedAndMediaById(model.ID) == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.SeedAndMediaRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteSeedAndMedia(Guid seedAndMediaId)
        {
            var seedAndMedia = GetSeedAndMediaById(seedAndMediaId);
            if (seedAndMedia == null)
                throw new ArgumentException("ไม่พบข้อมูลในระบบ");

            uow.SeedAndMediaRepository.Remove(seedAndMedia);
            uow.Save();
        }

        public List<SeedAndMedia> GetSeedAndMediaByCrop(short crop)
        {
            return uow.SeedAndMediaRepository
                .Query(s => s.Crop == crop,
                null,
                s => s.SeedAndMediaType,
                s => s.SeedAndMediaUnit)
                .ToList();
        }

        public SeedAndMedia GetSeedAndMediaById(Guid id)
        {
            return uow.SeedAndMediaRepository
                .GetSingle(s => s.ID == id,
                s => s.SeedAndMediaType,
                s => s.SeedAndMediaUnit);
        }

        public List<SeedAndMedia> GetSeedAndMediaByArea(short crop, string areaCode)
        {
            return uow.SeedAndMediaRepository
                .Query(s => s.AreaCode == areaCode &&
                s.Crop == crop,
                null,
                s => s.SeedAndMediaType,
                s => s.SeedAndMediaUnit)
                .ToList();
        }

        public List<SeedAndMediaUnit> GetSeedAndMediaUnit()
        {
            return uow.SeedAndMediaUnitRepository.Get().ToList();
        }

        public List<SeedAndMediaType> GetSeedAndMediaType()
        {
            return uow.SeedAndMediaTypeRepository.Get().ToList();
        }

        public List<SeedVariety> GetSeedVarietyByCrop(short crop)
        {
            return uow.SeedVarietyRepository.Query(s => s.SeedCrop == crop).ToList();
        }

        public SeedVariety GetSeedVarietyByID(Guid seedVarietyCode)
        {
            return uow.SeedVarietyRepository.GetSingle(s => s.SeedCode == seedVarietyCode);
        }

        public void AddSeedVariety(SeedVariety model)
        {
            try
            {
                if (model.SeedCrop.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล SeedCrop");

                if (model.SeedVarietyCode.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล SeedVarietyCode");

                if (model.SeedLotNo == "")
                    throw new ArgumentException("ไม่พบข้อมูล SeedLotNo");

                if (model.SeedVarietyName == "")
                    throw new ArgumentException("ไม่พบข้อมูล SeedVarietyName");

                if (model.ModifiedByUser == "")
                    throw new ArgumentException("ไม่พบข้อมูล ModifiedByUser");

                if (uow.SeedVarietyRepository
                    .Query(s => s.SeedCrop == model.SeedCrop &&
                    s.SeedVarietyCode == model.SeedVarietyCode &&
                    s.SeedLotNo == model.SeedLotNo)
                    .Count() > 0)
                    throw new ArgumentException("มีข้อมูลนี้ซ้ำแล้วในระบบ");

                uow.SeedVarietyRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSeedVariety(SeedVariety model)
        {
            try
            {
                if (model.SeedCode.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล SeedCode");

                if (model.SeedCrop.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล SeedCrop");

                if (model.SeedVarietyCode.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล SeedVarietyCode");

                if (model.SeedLotNo == "")
                    throw new ArgumentException("ไม่พบข้อมูล SeedLotNo");

                if (model.SeedVarietyName == "")
                    throw new ArgumentException("ไม่พบข้อมูล SeedVarietyName");

                if (model.ModifiedByUser == "")
                    throw new ArgumentException("ไม่พบข้อมูล ModifiedByUser");

                if (uow.SeedVarietyRepository
                    .Query(s => s.SeedCrop == model.SeedCrop &&
                    s.SeedVarietyCode == model.SeedVarietyCode &&
                    s.SeedLotNo == model.SeedLotNo).Count() <= 0)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.SeedVarietyRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteSeedVariety(Guid seedCode)
        {
            try
            {
                var seedVariety = uow.SeedVarietyRepository.GetSingle(s => s.SeedCode == seedCode);
                if (seedVariety == null)
                    throw new ArgumentException("ไม่พบข้อมูล seedCode");

                uow.SeedVarietyRepository.Remove(seedVariety);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<InputFactor> GetInputFactorBySupplierCode(short crop, string supplierCode)
        {
            return uow.InputFactorRepository
                .Query(i => i.Crop == crop && i.SupplierCode == supplierCode)
                .ToList();
        }

        public InputFactor GetInputFactorByID(Guid id)
        {
            return uow.InputFactorRepository.GetSingle(i => i.ID == id);
        }

        public void AddInputFactor(InputFactor model)
        {
            try
            {
                if (model.ModifiedBy == "")
                    throw new ArgumentException("ไม่พบข้อมูล ModifiedBy");

                if (model.Name == "")
                    throw new ArgumentException("ไม่พบข้อมูล Name");

                if (model.Crop.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล Crop");

                if (model.UnitPrice < 0)
                    throw new ArgumentException("ไม่พบข้อมูล seedCode");

                if (model.SupplierCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล SupplierCode");

                if (model.UnitPrice.ToString() == "")
                    throw new ArgumentException("UnitPrice จะต้องมากกว่าหรือเท่ากับ 0");

                model.ID = Guid.NewGuid();
                model.ModifiedDate = DateTime.Now;

                uow.InputFactorRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateInputFactor(InputFactor model)
        {
            try
            {
                if (model.ID.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล ID");

                if (model.ModifiedBy == "")
                    throw new ArgumentException("ไม่พบข้อมูล ModifiedBy");

                if (model.Name == "")
                    throw new ArgumentException("ไม่พบข้อมูล Name");

                if (model.Crop.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล Crop");

                if (model.UnitPrice < 0)
                    throw new ArgumentException("ไม่พบข้อมูล seedCode");

                if (model.SupplierCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล SupplierCode");

                if (model.UnitPrice.ToString() == "")
                    throw new ArgumentException("UnitPrice จะต้องมากกว่าหรือเท่ากับ 0");

                var inputFactorFromDB = uow.InputFactorRepository.GetSingle(i => i.ID == model.ID);
                if (inputFactorFromDB == null) throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                model.ModifiedDate = DateTime.Now;

                uow.InputFactorRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteInputFactor(Guid id)
        {
            try
            {
                var inputFactorFromDB = uow.InputFactorRepository.GetSingle(i => i.ID == id);
                if (inputFactorFromDB == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.InputFactorRepository.Remove(inputFactorFromDB);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ReceiveInputFactor> GetReceiveInputFactorByFarmer(short crop, string farmerCode)
        {
            return uow.ReceiveInputFactorRepository
                .Query(ri => ri.Crop == crop &&
                ri.FarmerCode == farmerCode)
                .ToList();
        }

        public List<m_ReceiveInputFactor> GetReceiveInputFactorInfoByFarmer(short crop, string farmerCode)
        {
            return (from rin in uow.ReceiveInputFactorRepository
                    .Query(ri => ri.Crop == crop
                    && ri.FarmerCode == farmerCode,
                    null,
                    ri => ri.InputFactor)
                    select new m_ReceiveInputFactor
                    {
                        Crop = rin.Crop,
                        FarmerCode = rin.FarmerCode,
                        InputFactorID = rin.InputFactorID,
                        InputFactorName = rin.InputFactor.Name,
                        UnitPrice = rin.InputFactor.UnitPrice,
                        Quantity = rin.Quantity,
                        Price = rin.Quantity * rin.InputFactor.UnitPrice
                    }).ToList();
        }

        public ReceiveInputFactor GetReceiveInputFactorByID(short crop, string farmerCode, Guid inputFactorID)
        {
            return uow.ReceiveInputFactorRepository
                .GetSingle(ri => ri.Crop == crop &&
                ri.FarmerCode == farmerCode &&
                ri.InputFactorID == inputFactorID);
        }

        public void AddReceiveInputFacor(ReceiveInputFactor model)
        {
            try
            {
                if (model.InputFactorID.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล InputFactorID");

                if (model.Crop.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล Crop");

                if (model.FarmerCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล IDFarmerCode");

                if (model.Quantity.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล Quantity");

                if (model.Quantity <= 0)
                    throw new ArgumentException("Quantity ไม่ควรน้อยกว่า 0");

                if (model.ModifiedBy == "")
                    throw new ArgumentException("ไม่พบข้อมูล ModifiedBy");

                if (model.Dispenser == "")
                    throw new ArgumentException("ไม่พบข้อมูล Dispenser");

                if (model.Quantity.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล Quantity");

                model.ModifiedDate = DateTime.Now;
                model.ReceiveDate = DateTime.Now;

                uow.ReceiveInputFactorRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateReceiveInputFacor(ReceiveInputFactor model)
        {
            try
            {
                if (model.InputFactorID.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล InputFactorID");

                if (model.Crop.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล Crop");

                if (model.FarmerCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล IDFarmerCode");

                if (model.Quantity.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล Quantity");

                if (model.Quantity <= 0)
                    throw new ArgumentException("Quantity ไม่ควรน้อยกว่า 0");

                if (model.ModifiedBy == "")
                    throw new ArgumentException("ไม่พบข้อมูล ModifiedBy");

                if (model.Dispenser == "")
                    throw new ArgumentException("ไม่พบข้อมูล Dispenser");

                if (model.Quantity.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล Quantity");

                var update = uow.ReceiveInputFactorRepository
                    .GetSingle(ri => ri.Crop == model.Crop &&
                    ri.FarmerCode == model.FarmerCode &&
                    ri.InputFactorID == model.InputFactorID);

                if (update == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                model.ModifiedDate = DateTime.Now;

                uow.ReceiveInputFactorRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteReceiveInputFacor(ReceiveInputFactor model)
        {
            try
            {
                var delete = uow.ReceiveInputFactorRepository
                    .GetSingle(ri => ri.Crop == model.Crop &&
                    ri.FarmerCode == model.FarmerCode &&
                    ri.InputFactorID == model.InputFactorID);

                if (delete == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.ReceiveInputFactorRepository.Remove(delete);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}