﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public interface IMaterialIssuedBL
    {
        void Add(MaterialIssued model);
        void Delete(string issuedCode);
        MaterialIssued GetSingle(string issuedCode);
        List<MaterialIssued> GetByCrop(short crop);
        string GetNewIssuedCode(short crop);
    }

    public class MaterialIssuedBL : IMaterialIssuedBL
    {
        UnitOfWork uow;
        public MaterialIssuedBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(MaterialIssued model)
        {
            try
            {
                uow.MaterialIssuedRepository
                    .Add(new MaterialIssued
                    {
                        IssueCode = GetNewIssuedCode(model.Crop),
                        Crop = model.Crop,
                        Description = model.Description,
                        CreateDate = DateTime.Now,
                        CreateBy = model.Description,
                        ModifiedUser = model.ModifiedUser,
                        ModifiedDate = DateTime.Now
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string issuedCode)
        {
            try
            {
                var model = GetSingle(issuedCode);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.MaterialIssuedRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<MaterialIssued> GetByCrop(short crop)
        {
            return uow.MaterialIssuedRepository
                .Get(x => x.Crop == crop,
                null,
                x => x.MaterialIssuedDetails);
        }

        public string GetNewIssuedCode(short crop)
        {
            try
            {
                /// Generate issued Code.
                /// 
                int _max;
                string _issuedCode;

                var list = GetByCrop(crop);
                if (list.Count() <= 0)
                    _max = 1;
                else
                    _max = list.Max(x => Convert.ToInt16(x.IssueCode.Substring(5, 3))) + 1;

                _issuedCode = "IS" + crop.ToString().Substring(2, 2) + "-" + _max.ToString().PadLeft(3, '0');

                return _issuedCode;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MaterialIssued GetSingle(string issuedCode)
        {
            return uow.MaterialIssuedRepository.GetSingle(x => x.IssueCode == issuedCode);
        }
    }
}
