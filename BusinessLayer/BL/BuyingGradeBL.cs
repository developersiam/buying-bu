﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IBuyingGradeBL
    {
        void Add(short crop, byte pricingNumber, string grade, decimal unitPrice, string modifiedBy);
        void Delete(short crop, byte pricingNumber, string grade);
        void Update(short crop, byte pricingNumber, string grade, decimal unitPrice, string modifiedBy);
        BuyingGrade GetSingle(short crop, byte pricingNumber, string grade);
        List<BuyingGrade> GetByPricingSet(short crop, byte pricingNumber);
        List<BuyingGrade> GetByCrop(short crop);
        List<BuyingGrade> GetByDefaultPricingSet();
    }

    public class BuyingGradeBL : IBuyingGradeBL
    {
        UnitOfWork uow;
        public BuyingGradeBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(short crop, byte pricingNumber, string grade, decimal unitPrice, string modifiedBy)
        {
            try
            {
                if (grade == "")
                    throw new ArgumentException("ไม่พบข้อมูล grade");

                if (unitPrice.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล unitPrice");

                if (unitPrice <= 0)
                    throw new ArgumentException("unitPrice จะต้องไม่ต่ำกว่า 0");

                if (modifiedBy == "")
                    throw new ArgumentException("ไม่พบข้อมูล modifiedBy");

                var buyingGrade = uow.BuyingGradeRepository
                    .GetSingle(bg => bg.PricingCrop == crop && 
                    bg.PricingNumber == pricingNumber && 
                    bg.Grade == grade);

                if (buyingGrade != null)
                    throw new ArgumentException("มีเกรดนี้ซ้ำอยู่แล้วในระบบ");

                buyingGrade.PricingCrop = crop;
                buyingGrade.PricingNumber = pricingNumber;
                buyingGrade.Grade = grade;
                buyingGrade.UnitPrice = unitPrice;
                buyingGrade.AssignDate = DateTime.Now;
                buyingGrade.ActiveStatus = true;
                buyingGrade.ModifiedBy = modifiedBy;
                buyingGrade.ModifiedDate = DateTime.Now;

                uow.BuyingGradeRepository.Add(buyingGrade);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(short crop, byte pricingNumber, string grade)
        {
            try
            {
                var buyingGrade = uow.BuyingGradeRepository
                    .GetSingle(bg => bg.PricingCrop == crop && 
                    bg.PricingNumber == pricingNumber && 
                    bg.Grade == grade);

                if (buyingGrade == null)
                    throw new ArgumentException("ไม่พบข้อมูลเกรดนี้ในระบบ");

                uow.BuyingGradeRepository.Remove(buyingGrade);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(short crop, byte pricingNumber, string grade, decimal unitPrice, string modifiedBy)
        {
            try
            {
                if (grade == "")
                    throw new ArgumentException("ไม่พบข้อมูล grade");

                if (unitPrice.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล unitPrice");

                if (unitPrice <= 0)
                    throw new ArgumentException("unitPrice จะต้องไม่ต่ำกว่า 0");

                if (modifiedBy == "")
                    throw new ArgumentException("ไม่พบข้อมูล modifiedBy");

                var buyingGrade = uow.BuyingGradeRepository
                    .GetSingle(bg => bg.PricingCrop == crop && 
                    bg.PricingNumber == pricingNumber && 
                    bg.Grade == grade);

                if (buyingGrade == null)
                    throw new ArgumentException("ไม่พบข้อมูลเกรดนี้ในระบบ");

                buyingGrade.UnitPrice = unitPrice;
                buyingGrade.ModifiedBy = modifiedBy;
                buyingGrade.ModifiedDate = DateTime.Now;

                uow.BuyingGradeRepository.Update(buyingGrade);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BuyingGrade GetSingle(short crop, byte pricingNumber, string grade)
        {
            try
            {
                return uow.BuyingGradeRepository
                    .GetSingle(bg => bg.PricingCrop == crop && 
                    bg.PricingNumber == pricingNumber && 
                    bg.Grade == grade);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BuyingGrade> GetByPricingSet(short crop, byte pricingNumber)
        {
            try
            {
                return uow.BuyingGradeRepository
                    .Query(g => g.PricingCrop == crop && 
                    g.PricingNumber == pricingNumber)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BuyingGrade> GetByCrop(short crop)
        {
            try
            {
                return uow.BuyingGradeRepository.Query(b => b.PricingCrop == crop).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BuyingGrade> GetByDefaultPricingSet()
        {
            try
            {
                var pricingSet = uow.PricingSetRepository.GetSingle(ps => ps.IsDefault == true);
                if (pricingSet == null)
                    throw new ArgumentException("ไม่มีการกำหนดชุดราคาในระบบ โปรดแจ้งแผนกไอทีเพื่อดำเนินการแก้ไข");

                return uow.BuyingGradeRepository.Query(ps => ps.PricingCrop == pricingSet.Crop && 
                ps.PricingNumber == pricingSet.PricingNumber)
                .ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
