﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public struct ReturnItemType
    {
        public string ItemType { get; set; }
    }

    public interface ICY202201ReturnItemBL
    {
        void Add(CY202201ReturnItem model);
        void Update(CY202201ReturnItem model);
        void Delete(int itemID);
        CY202201ReturnItem GetSingle(int itemID);
        List<CY202201ReturnItem> GetAll();
        List<ReturnItemType> GetItemType();
    }

    public class CY202201ReturnItemBL : ICY202201ReturnItemBL
    {
        UnitOfWork uow;
        public CY202201ReturnItemBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(CY202201ReturnItem model)
        {
            try
            {
                if (model.RecordUser == null)
                    throw new ArgumentException("โปรดระบุผู้บันทึกข้อมูล");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("โปรดระบุผู้บันทึกข้อมูล");

                if (model.ItemType == null)
                    throw new ArgumentException("โปรดระบุประเภทของรายการปัจจัย (ขยะสารเคมี/ตะกร้า)");

                var item = AGMInventorySystemBL.BLServices.material_itemBL().GetSingle(model.ReturnItemID);
                if (item == null)
                    throw new ArgumentException("ไม่พบ Item ID นี้ในระบบ AGM Inventory");

                var dupplicateItem = GetSingle(model.ReturnItemID);
                if (dupplicateItem != null)
                    throw new ArgumentException("มีการบันทึกข้อมูล Item ID นี้แล้วในระบบ");

                model.RecordDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;
                uow.CY202201ReturnItemRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(int itemID)
        {
            try
            {
                var delete = GetSingle(itemID);
                if (delete == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                var list = uow.CY202201ReturnDetailRepository.Get(x => x.ReturnItemID == itemID);
                if (list.Count() > 0)
                    throw new ArgumentException("มีการนำ Item ID นี้ไปบันทึกข้อมูลการลดหนี้แล้ว ไม่สามารถลบข้อมูลนี้ได้");

                uow.CY202201ReturnItemRepository.Remove(delete);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CY202201ReturnItem> GetAll()
        {
            return uow.CY202201ReturnItemRepository.Get();
        }

        public List<ReturnItemType> GetItemType()
        {
            var list = new List<ReturnItemType>();
            list.Add(new ReturnItemType { ItemType = "ตะกร้า" });
            list.Add(new ReturnItemType { ItemType = "ขยะสารเคมี" });
            return list;
        }

        public CY202201ReturnItem GetSingle(int itemID)
        {
            return uow.CY202201ReturnItemRepository.GetSingle(x => x.ReturnItemID == itemID);
        }

        public void Update(CY202201ReturnItem model)
        {
            try
            {
                if (model.ItemType == null)
                    throw new ArgumentException("โปรดระบุประเภทของรายการปัจจัย (ขยะสารเคมี/ตะกร้า)");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("โปรดระบุผู้บันทึกข้อมูล");

                var edit = GetSingle(model.ReturnItemID);
                if (edit == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                edit.ModifiedDate = model.ModifiedDate;
                edit.ModifiedUser = model.ModifiedUser;
                edit.IsActive = model.IsActive;
                edit.ItemType = model.ItemType;
                edit.Remark = model.Remark;
                uow.CY202201ReturnItemRepository.Update(edit);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
