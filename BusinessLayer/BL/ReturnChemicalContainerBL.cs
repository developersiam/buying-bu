﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IReturnChemicalContainerBL
    {
        void Add(short crop, string farmerCode, int itemID, short quantity, DateTime returnDate, string description, string modifiedUser);
        void Delete(Guid id);
        ReturnChemicalContainer GetSingle(Guid id);
        List<ReturnChemicalContainer> GetByFarmer(short crop, string farmerCode);
        List<ReturnChemicalContainer> GetByCrop(short crop);
    }

    public class ReturnChemicalContainerBL : IReturnChemicalContainerBL
    {
        UnitOfWork _uow;
        public ReturnChemicalContainerBL()
        {
            _uow = new UnitOfWork();
        }

        public void Add(short crop, string farmerCode, int itemID, short quantity, DateTime returnDate, string description, string modifiedUser)
        {
            try
            {
                if (string.IsNullOrEmpty(farmerCode))
                    throw new ArgumentException("The farmer code cannot be empty.");

                if (string.IsNullOrEmpty(modifiedUser))
                    throw new ArgumentException("The modified user cannot be empty.");

                if (itemID == 0)
                    throw new ArgumentException("Item ID cannot be zero.");

                if (quantity <= 0)
                    throw new ArgumentException("The quantity should more than zero.");

                _uow.ReturnChemicalContainerRepository
                    .Add(new ReturnChemicalContainer
                    {
                        ID = Guid.NewGuid(),
                        Crop = crop,
                        FarmerCode = farmerCode,
                        ItemID = itemID,
                        Quantity = quantity,
                        ReturnDate = returnDate,
                        Description = description,
                        ModifiedBy = modifiedUser,
                        ModifiedDate = DateTime.Now
                    });
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var model = GetSingle(id);
                if (model == null)
                    throw new ArgumentException("This item not found!");

                _uow.ReturnChemicalContainerRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ReturnChemicalContainer> GetByCrop(short crop)
        {
            return _uow.ReturnChemicalContainerRepository
                .Get(x => x.Crop == crop)
                .ToList();
        }

        public List<ReturnChemicalContainer> GetByFarmer(short crop, string farmerCode)
        {
            return _uow.ReturnChemicalContainerRepository
                .Get(x => x.Crop == crop && x.FarmerCode == farmerCode)
                .ToList();
        }

        public ReturnChemicalContainer GetSingle(Guid id)
        {
            return _uow.ReturnChemicalContainerRepository.GetSingle(x => x.ID == id);
        }
    }
}
