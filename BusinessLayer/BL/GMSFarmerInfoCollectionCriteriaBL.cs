﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IGMSFarmerInfoCollectionCriteriaBL
    {
        GMSFarmerInfoCollectionCriteria GetSingle(string citizenID);
    }

    public class GMSFarmerInfoCollectionCriteriaBL : IGMSFarmerInfoCollectionCriteriaBL
    {
        UnitOfWork uow;
        public GMSFarmerInfoCollectionCriteriaBL()
        {
            uow = new UnitOfWork();
        }

        public GMSFarmerInfoCollectionCriteria GetSingle(string citizenID)
        {
            return uow.GMSFarmerInfoCollectionCriteriaRepository.GetSingle(x => x.CitizenID == citizenID);
        }
    }
}
