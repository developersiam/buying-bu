﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface ICuringBarnBL
    {
        void Add(CuringBarnInformation model);
        void Update(CuringBarnInformation model);
        void Delete(Guid barnID);
        CuringBarnInformation GetByID(Guid barnID);
        List<CuringBarnInformation> GetByCitizenID(string citizenID, short crop);
    }

    public class CuringBarnBL : ICuringBarnBL
    {
        UnitOfWork uow;
        public CuringBarnBL()
        {
            uow = new UnitOfWork();
        }
        public void Add(CuringBarnInformation model)
        {
            try
            {
                if (model.CitizenID == null)
                    throw new ArgumentException("โปรดระบุรหัสประจำตัวประชาชน");

                if (model.PoleMaterial == null)
                    throw new ArgumentException("โปรดระบุวัสดุทำเสา");

                if (model.RackMaterial == null)
                    throw new ArgumentException("โปรดระบุวัสดุทำชั้น");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("โปรดระบุผู้บันทึกข้อมูล");

                model.ModifiedDate = DateTime.Now;
                uow.CuringBarnInformationRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Update(CuringBarnInformation model)
        {
            try
            {
                if (model.CitizenID == null)
                    throw new ArgumentException("โปรดระบุรหัสประจำตัวประชาชน");

                if (model.PoleMaterial == null)
                    throw new ArgumentException("โปรดระบุวัสดุทำเสา");

                if (model.RackMaterial == null)
                    throw new ArgumentException("โปรดระบุวัสดุทำชั้น");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("โปรดระบุผู้บันทึกข้อมูล");

                var edit = GetByID(model.BarnID);
                if (edit == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                edit.Age = model.Age;
                edit.Length = model.Length;
                edit.PoleMaterial = model.PoleMaterial;
                edit.RackMaterial = model.RackMaterial;
                edit.Tier = model.Tier;
                edit.Width = model.Width;
                edit.ModifiedDate = DateTime.Now;
                edit.ModifiedUser = model.ModifiedUser;

                uow.CuringBarnInformationRepository.Update(edit);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(Guid barnID)
        {
            try
            {
                var delete = GetByID(barnID);
                if (delete == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.CuringBarnInformationRepository.Remove(delete);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public CuringBarnInformation GetByID(Guid barnID)
        {
            return uow.CuringBarnInformationRepository.GetSingle(c => c.BarnID == barnID);
        }
        public List<CuringBarnInformation> GetByCitizenID(string citizenID, short crop)
        {
            return uow.CuringBarnInformationRepository
                .Query(c => c.CitizenID == citizenID && c.Crop == crop)
                .ToList();
        }
    }
}
