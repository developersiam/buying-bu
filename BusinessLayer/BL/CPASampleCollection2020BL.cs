﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface ICPASampleCollection2020BL
    {
        void Add(CPASampleCollection2020 model);
        void Delete(Guid id);
        List<CPASampleCollection2020> GetByFarmer(string citizenID, string gapGroupCode);
        List<CPASampleCollection2020> GetByFarmer2(string citizenID, short crop);
        List<CPASampleCollection2020> GetByGAPGroup(string gapGroupCode);
    }
    public class CPASampleCollection2020BL : ICPASampleCollection2020BL
    {
        UnitOfWork uow;
        public CPASampleCollection2020BL()
        {
            uow = new UnitOfWork();
        }

        public void Add(CPASampleCollection2020 model)
        {
            try
            {
                if (model.GAPGroupCode == null)
                    throw new ArgumentException("GAP group code cannot be empty.");

                if (model.CitizenID == null)
                    throw new ArgumentException("CitizenID cannot be empty.");

                if (model.CollectionUser == null)
                    throw new ArgumentException("Collection user cannot be empty.");

                if (model.Position == null)
                    throw new ArgumentException("Position cannot be empty.");

                if (model.SampleType == null)
                    throw new ArgumentException("Sample type cannot be empty.");

                if (model.Bag <= 0)
                    throw new ArgumentException("จำนวนกระสอบที่ระบุจะต้องมากกว่า 0");

                model.Id = Guid.NewGuid();
                model.CollectionDate = DateTime.Now;
                uow.CPASampleCollection2020Repository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var model = uow.CPASampleCollection2020Repository.GetSingle(x => x.Id == id);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.CPASampleCollection2020Repository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CPASampleCollection2020> GetByFarmer(string citizenID, string gapGroupCode)
        {
            return uow.CPASampleCollection2020Repository
                .Query(x => x.CitizenID == citizenID &&
                x.GAPGroupCode == gapGroupCode)
                .ToList();
        }

        public List<CPASampleCollection2020> GetByFarmer2(string citizenID, short crop)
        {
            return uow.CPASampleCollection2020Repository
                .Get(x => x.CitizenID == citizenID &&
                x.GAPGroupMember2020.GAPGroup2020.Crop == crop,
                null,
                x => x.GAPGroupMember2020,
                x => x.GAPGroupMember2020.GAPGroup2020)
                .ToList();
        }

        public List<CPASampleCollection2020> GetByGAPGroup(string gapGroupCode)
        {
            return uow.CPASampleCollection2020Repository
                .Query(x => x.GAPGroupCode == gapGroupCode)
                .ToList();
        }
    }
}
