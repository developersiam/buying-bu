﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface ICropInputCategoryBL
    {
        List<CropInputCategory> GetAll();
    }

    public class CropInputCategoryBL : ICropInputCategoryBL
    {
        UnitOfWork uow;
        public CropInputCategoryBL()
        {
            uow = new UnitOfWork();
        }

        public List<CropInputCategory> GetAll()
        {
            try
            {
                return uow.CropInputCategoryRepository.Get().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
