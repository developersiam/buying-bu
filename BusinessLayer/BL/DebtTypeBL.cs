﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IDebtTypeBL {
        List<DebtType> GetAll();
    }

    public class DebtTypeBL : IDebtTypeBL
    {
        UnitOfWork uow;
        public DebtTypeBL()
        {
            uow = new UnitOfWork();
        }

        public List<DebtType> GetAll()
        {
            return uow.DebtTypeRepository.Get();
        }
    }
}
