﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;
using System.Text.RegularExpressions;

namespace BusinessLayer.BL
{
    public interface IFarmerBL
    {
        string Add(string citizenID, string supplierCode);
        void Delete(string farmerCode);
        List<Farmer> GetByCitizenID(string citizenID);
        List<Farmer> GetBySupplierCode(string supplierCode);
        List<Farmer> GetByFarmerName(string farmerName);
        Farmer GetByFarmerCode(string farmerCode);
        Farmer GetBySupplierCodeAndCitizenID(string supplierCode, string citizenID);
        List<MaritalStatu> GetMaritalStatus();
    }

    struct FarmerCodeNumber
    {
        public int Number { get; set; }
    }

    public class FarmerBL : IFarmerBL
    {
        UnitOfWork uow;
        public FarmerBL()
        {
            uow = new UnitOfWork();
        }

        public string Add(string citizenID, string supplierCode)
        {
            try
            {
                if (string.IsNullOrEmpty(citizenID))
                    throw new ArgumentException("CitizenID cannot be empty.");

                if (string.IsNullOrEmpty(supplierCode))
                    throw new ArgumentException("Supplier code cannot be empty.");

                var person = uow.PersonRepository.GetSingle(x => x.CitizenID == citizenID);
                if (person == null)
                    throw new ArgumentException("ชาวไร่รายนี้ยังไม่ได้บันทึกข้อมูลประวัติตามบัตรประจำตัวประชาชนในระบบ");

                if (uow.FarmerRepository.Get(frm => frm.Supplier == supplierCode &&
                    frm.CitizenID == citizenID)
                    .Count() > 0)
                    throw new ArgumentException("มีการลงทะเบียนไว้กับตัวแทน " + supplierCode + " นี้แล้ว");

                //ชาวไร่จะลงทะเบียนได้กับตัวแทนภายในจังหวัดของตนเท่านั้น
                var supplier = uow.SupplierRepository
                    .GetSingle(x => x.SupplierCode == supplierCode);
                if (supplier == null)
                    throw new ArgumentException("Find supplier not found.");

                var farmerList = uow.FarmerRepository
                    .Get(x => x.CitizenID == citizenID,
                    null,
                    x => x.Supplier1)
                    .ToList();

                if (farmerList
                    .Where(x => x.Supplier1.SupplierArea != supplier.SupplierArea)
                    .Count() > 0)
                    throw new ArgumentException("ระบบไม่อนุญาตให้ชาวไร่ลงทะเบียนเป็นสมาชิกกับตัวแทนข้ามจังหวัดได้");

                Farmer farmer = new Farmer
                {
                    FarmerCode = GetNewFarmerCode(supplierCode),
                    RegistrationDate = DateTime.Now,
                    Supplier = supplierCode,
                    CitizenID = citizenID
                };

                uow.FarmerRepository.Add(farmer);
                uow.Save();

                return farmer.FarmerCode;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetNewFarmerCode(string supplierCode)
        {
            try
            {
                var supplier = uow.SupplierRepository
                    .GetSingle(x => x.SupplierCode == supplierCode);

                if (supplier == null)
                    throw new ArgumentException("Find supplier not found.");

                var farmerList = uow.FarmerRepository
                    .Get(frm => frm.Supplier == supplierCode)
                    .ToList();

                var codeList = farmerList
                    .Select(x => new FarmerCodeNumber
                    {
                        Number = int.Parse(Regex.Replace(x.FarmerCode, "[^0-9]", ""))
                    })
                    .ToList();

                int maxNumber = 1;

                if (farmerList.Count() > 0)
                    maxNumber = codeList.Max(x => x.Number) + 1;

                ///return a new farmercode.
                ///
                var farmerCode = supplier.SupplierPrefix + maxNumber.ToString()
                    .PadLeft(7 - supplier.SupplierPrefix.Length, '0');
                return supplier.SupplierPrefix + maxNumber.ToString()
                    .PadLeft(7 - supplier.SupplierPrefix.Length, '0');
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string farmerCode)
        {
            try
            {
                if (farmerCode == "")
                    throw new ArgumentException("Farmer code cannot be empty.");

                var farmer = uow.FarmerRepository
                    .GetSingle(x => x.FarmerCode == farmerCode);
                if (farmer == null)
                    throw new ArgumentException("Find this farmer not found.");

                var registrations = uow.RegistrationFarmerRepository
                    .Get(x => x.FarmerCode == farmerCode);

                if (registrations.Count() > 0)
                    throw new ArgumentException("ชาวไร่รหัส " + farmerCode +
                        " มีข้อมูลเชื่อมโยงกับข้อูลการลงทะเบียนและอื่นๆ ในระบบ ไม่สามารถลบข้อมูลได้");

                uow.FarmerRepository.Remove(farmer);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Farmer> GetByCitizenID(string citizenID)
        {
            return uow.FarmerRepository
                .Get(fm => fm.CitizenID == citizenID,
                null,
                fm => fm.Person,
                fm => fm.RegistrationFarmers,
                fm => fm.Supplier1)
                .ToList();
        }

        public List<Farmer> GetBySupplierCode(string supplierCode)
        {
            return uow.FarmerRepository
                .Get(fm => fm.Supplier == supplierCode,
                null,
                fm => fm.Person)
                .ToList();
        }

        public Farmer GetByFarmerCode(string farmerCode)
        {
            return uow.FarmerRepository
                .GetSingle(fm =>
                fm.FarmerCode == farmerCode,
                fm => fm.Person,
                fm => fm.RegistrationFarmers,
                fm => fm.Supplier1);
        }

        public Farmer GetBySupplierCodeAndCitizenID(string supplierCode, string citizenID)
        {
            return uow.FarmerRepository
                .GetSingle(fm => fm.Supplier == supplierCode &&
                fm.CitizenID == citizenID,
                fm => fm.Person,
                fm => fm.RegistrationFarmers);
        }

        public List<MaritalStatu> GetMaritalStatus()
        {
            return uow.MaritalStatuRepository
                .Get()
                .OrderBy(x => x.MaritalStatusCode)
                .ToList();
        }

        public List<Farmer> GetByFarmerName(string farmerName)
        {
            return uow.FarmerRepository
                .Get(x => x.Person.FirstName.Contains(farmerName),
                null,
                x => x.Person,
                x => x.RegistrationFarmers,
                x => x.Supplier1,
                x => x.Supplier1.Area)
                .ToList();
        }
    }
}
