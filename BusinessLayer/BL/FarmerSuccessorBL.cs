﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IFarmerSuccessorBL
    {
        void Add(string citizenID, bool successor, short crop, string modifiedBy);
        void Update(string citizenID, bool successor, short crop, string modifiedBy);
        void Delete(string citizenID, short crop);
        FarmerSuccessor GetSingle(string citizenID, short crop);
        List<FarmerSuccessor> GetByCitizenID(string citizenID);
    }

    public class FarmerSuccessorBL : IFarmerSuccessorBL
    {
        UnitOfWork uow;
        public FarmerSuccessorBL()
        {
            uow = new UnitOfWork();
        }
        public void Add(string citizenID, bool successor, short crop, string modifiedBy)
        {
            try
            {
                if (string.IsNullOrEmpty(citizenID))
                    throw new ArgumentException("CitizenID cannot be empty.");

                if (string.IsNullOrEmpty(modifiedBy))
                    throw new ArgumentException("Modified By cannot be empty.");

                var cropDef = uow.CropRepository.GetSingle(x => x.DefaultStatus == true);
                if (cropDef == null)
                    throw new ArgumentException("Default crop cannot be empty.");

                if (cropDef.Crop1 != crop)
                    throw new ArgumentException("Crop ที่จะบันทึกข้อมูลไม่ตรงกับ default crop ในระบบ");

                var model = GetSingle(citizenID, crop);
                if (model != null)
                    throw new ArgumentException("มีข้อมูลนี้แล้วในระบบ");

                uow.FarmerSuccessorRepository
                    .Add(new FarmerSuccessor
                    {
                        Crop = crop,
                        CitizenID = citizenID,
                        Successor = successor,
                        ModifiedBy = modifiedBy,
                        ModifiedDate = DateTime.Now
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string citizenID, short crop)
        {
            try
            {
                if (string.IsNullOrEmpty(citizenID))
                    throw new ArgumentException("CitizenID cannot be empty.");

                var cropDef = uow.CropRepository.GetSingle(x => x.DefaultStatus == true);
                if (cropDef == null)
                    throw new ArgumentException("Default crop cannot be empty.");

                if (cropDef.Crop1 != crop)
                    throw new ArgumentException("Crop ที่จะลบข้อมูลไม่ตรงกับ default crop ในระบบ");

                var model = GetSingle(citizenID, crop);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                uow.FarmerSuccessorRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FarmerSuccessor> GetByCitizenID(string citizenID)
        {
            return uow.FarmerSuccessorRepository.Get(x => x.CitizenID == citizenID);
        }

        public FarmerSuccessor GetSingle(string citizenID, short crop)
        {
            return uow.FarmerSuccessorRepository
                .GetSingle(x => x.CitizenID == citizenID && x.Crop == crop);
        }

        public void Update(string citizenID, bool successor, short crop, string modifiedBy)
        {
            try
            {
                if (string.IsNullOrEmpty(citizenID))
                    throw new ArgumentException("CitizenID cannot be empty.");

                if (string.IsNullOrEmpty(modifiedBy))
                    throw new ArgumentException("Modified By cannot be empty.");

                var cropDef = uow.CropRepository.GetSingle(x => x.DefaultStatus == true);
                if (cropDef == null)
                    throw new ArgumentException("Default crop cannot be empty.");

                if (cropDef.Crop1 != crop)
                    throw new ArgumentException("Crop ที่จะบันทึกข้อมูลไม่ตรงกับ default crop ในระบบ");

                var model = GetSingle(citizenID, crop);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                model.Successor = successor;
                model.ModifiedBy = modifiedBy;
                model.ModifiedDate = DateTime.Now;

                uow.FarmerSuccessorRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
