﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IHessianReceivingBL
    {
        void Add(HessianReceiving model);
        void Update(HessianReceiving model);
        void Delete(string receivingCode);
        List<HessianReceiving> GetByArea(short crop, string areaCode);
        List<HessianReceiving> GetByWarehouse(short crop, Guid warehouseId);
        HessianReceiving GetBySingle(string receivingCode);
    }
    public class HessianReceivingBL : IHessianReceivingBL
    {
        UnitOfWork uow;

        public HessianReceivingBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(HessianReceiving model)
        {
            try
            {
                if (model.ReceivedBy == null)
                    throw new ArgumentException("โปรดระบุผู้บันทึกข้อมูล (Received By)");

                if (model.TruckNumber == null)
                    throw new ArgumentException("โปรดระบุทะเบียนรถ (Truck Number)");

                if (model.WarehouseID == null)
                    throw new ArgumentException("โปรดระบุผู้สถานที่เก็บ (Warehouse)");

                if (model.Quantity <= 0)
                    throw new ArgumentException("จำนวนกระสอบที่รับจะต้องมากกว่า 0");

                /// Generate a new receivingCode
                /// 2020-RC-001
                /// 
                var list = uow.HessianReceivingRepository
                    .Get(x => x.Crop == model.Crop)
                    .Select(x => new HessianReceiving
                    {
                        ReceivingCode = x.ReceivingCode.Substring(8, 4)
                    })
                    .ToList();

                var code = model.Crop + "-RC-";

                if (list.Count() <= 0)
                    code = code + "0001";
                else
                    code = code + (list
                        .Max(x => Convert.ToInt32(x.ReceivingCode)) + 1)
                        .ToString().PadLeft(4, '0');

                model.ReceivingCode = code;
                model.ReceivedDate = model.ReceivedDate;
                model.ModifiedBy = model.ReceivedBy;
                model.ModifiedDate = DateTime.Now;

                uow.HessianReceivingRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string receivingCode)
        {
            try
            {
                var model = uow.HessianReceivingRepository
                    .GetSingle(x => x.ReceivingCode == receivingCode,
                    x => x.HessianDistributions);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลที่ต้องการลบ");

                if (model.HessianDistributions.Count > 0)
                    throw new ArgumentException("กระสอบใน Receiving Code (" +
                        model.ReceivingCode + ") นี้ถูกจ่ายให้กับชาวไร่ไปแล้วจำนวน " +
                        model.HessianDistributions.Sum(x => x.Quantity) +
                        " กระสอบ ไม่สามารถลบข้อมูลนี้ได้");

                uow.HessianReceivingRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HessianReceiving> GetByArea(short crop, string areaCode)
        {
            return uow.HessianReceivingRepository
                .Get(x => x.Crop == crop && x.Warehouse.AreaCode == areaCode,
                null,
                x => x.Warehouse,
                x => x.Warehouse.Area)
                .ToList();
        }

        public HessianReceiving GetBySingle(string receivingCode)
        {
            return uow.HessianReceivingRepository.GetSingle(x => x.ReceivingCode == receivingCode);
        }

        public List<HessianReceiving> GetByWarehouse(short crop, Guid warehouseId)
        {
            return uow.HessianReceivingRepository
                .Get(x => x.Crop == crop && x.WarehouseID == warehouseId,
                null,
                x => x.Warehouse,
                x => x.Warehouse.Area,
                x => x.HessianDistributions)
                .ToList();
        }

        public void Update(HessianReceiving model)
        {
            try
            {
                if (model.ReceivedBy == null)
                    throw new ArgumentException("โปรดระบุผู้บันทึกข้อมูล (Received By)");

                if (model.TruckNumber == null)
                    throw new ArgumentException("โปรดระบุทะเบียนรถ (Truck Number)");

                if (model.WarehouseID == null)
                    throw new ArgumentException("โปรดระบุผู้สถานที่เก็บ (Warehouse)");

                if (model.Quantity <= 0)
                    throw new ArgumentException("จำนวนกระสอบที่รับจะต้องมากกว่า 0");

                var editModel = uow.HessianReceivingRepository
                    .GetSingle(x => x.ReceivingCode == model.ReceivingCode,
                    x => x.HessianDistributions);

                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลที่ต้องการแก้ไข");

                var distributed = editModel.HessianDistributions.Sum(x => x.Quantity);
                if (model.Quantity < distributed)
                    throw new ArgumentException("จำนวนที่ต้องการแก้ไข น้อยกว่าจำนวนที่จ่ายไปแล้ว ไม่สามารถแก้ไขข้อมูลนี้ได้" +
                        Environment.NewLine + "จำนวนที่จ่ายไปคือ " + distributed);

                editModel.ReceivedBy = model.ReceivedBy;
                editModel.ReceivedDate = model.ReceivedDate;
                editModel.WarehouseID = model.WarehouseID;
                editModel.TruckNumber = model.TruckNumber;
                editModel.Quantity = model.Quantity;
                editModel.ModifiedBy = model.ReceivedBy;
                editModel.ModifiedDate = DateTime.Now;

                uow.HessianReceivingRepository.Update(editModel);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
