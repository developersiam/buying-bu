﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IInvoiceDetailsBL
    {
        void Add(string invoiceNo, Guid conditionID, int quantity, string user);
        void AddByPackage(string packageCode, string invoiceNo, string recordUser);
        void Delete(string invoiceNo, Guid conditionID);
        void DeleteByPackage(string invoiceNo, string packageCode);
        void ChangeIsCashStatus(InvoiceDetail model);
        void Update(InvoiceDetail model);
        InvoiceDetail GetSingle(string invoiceNo, Guid conditionID);
        List<InvoiceDetail> GetByAccountInvoiceNo(string accountInvoiceNo);
        List<InvoiceDetail> GetByInvoiceNo(string invoiceNo);
        List<InvoiceDetail> GetByFarmer(short crop, string farmerCode);
        List<InvoiceDetail> GetByExtensionAgent(short crop, string extensionAgentCode);
    }

    public class InvoiceDetailsBL : IInvoiceDetailsBL
    {
        UnitOfWork uow;
        public InvoiceDetailsBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(string invoiceNo, Guid conditionID, int quantity, string user)
        {
            try
            {
                var invoice = uow.InvoiceRepository
                    .GetSingle(x => x.InvoiceNo == invoiceNo
                    , x => x.InvoiceDetails);
                if (invoice == null)
                    throw new Exception("ไม่พบข้อมูล invoice นี้ในระบบ");

                if (invoice.IsFinish == true)
                    throw new Exception("Invoice รหัส " + invoice.InvoiceNo +
                        " ถูก finish ไปแล้วไม่สามารถเพิ่มข้อมูลได้");

                if (uow.AccountInvoiceRepository
                    .Get(x => x.RefInvoiceNo == invoiceNo).Count() > 0)
                    throw new Exception("Invoice นี้มีข้อมูลการออกใบส่งปัจจัยการผลิตไปแล้ว ไม่สามารถแก้ไขข้อมูลได้");

                var condition = BuyingFacade.CropInputsConditionBL().GetSingle(conditionID);
                if (condition == null)
                    throw new Exception("ผู้ดูแลระบบยังไม่ได้กำหนด item นี้ให้กับตัวแทนต้นสังกัดของชาวไร่");

                ///หากมีการเพิ่ม item นี้เข้าไปแล้วใน invoice แล้วไม่อนุญาตให้เพิ่มข้อมูลได้อีก
                var invoiceDetails = invoice.InvoiceDetails;
                if (invoiceDetails
                    .Where(x => x.ConditionID == condition.ConditionID)
                    .Count() > 0)
                    throw new Exception("Item นี้ถูกเพิ่มเข้าไปใน invoice นี้แล้ว โปรดตรวจสอบอีกครั้ง");

                ///ตรวจสอบว่าเป็น item ที่มีการกำหนดไว้ใน crop inputs condition หรือไม่?
                var conditions = Helper.CropInputsItemHelper
                    .GetBySupplierCode(condition.Crop, condition.SupplierCode);

                if (conditions.Where(x => x.ConditionID == condition.ConditionID).Count() <= 0)
                    throw new Exception("item นี้ไม่ได้ถูกกำหนดไว้ในรายการที่จะจ่ายปัจจัยการผลิตของตัวแทนรายนี้ โปรดตรวจสอบข้อมูลไปยังผู้ดูแลระบบ");

                ///กรณีเป็น item ที่ต้องเช็คกับของที่มีอยู่ในสต็อค(IsReferToInventory)
                var location = "";
                if (condition.IsReferToInventory == true)
                {
                    ///ใช้สำหรับตรวจเช็ค item ในสต็อคแยกตาม location ของ item ใน transaction
                    var supplier = uow.SupplierRepository
                        .GetSingle(x => x.SupplierCode == condition.SupplierCode);

                    if (supplier.SupplierArea == "PET")
                        location = "01";
                    else if (supplier.SupplierArea == "SUK")
                        location = "02";
                    else if (supplier.SupplierArea == "CM")
                        location = "03";
                    else
                        location = "";

                    var balanceStock = AGMInventorySystemBL
                        .BLServices
                        .material_transactionBL()
                        .GetByItem(condition.ItemID)
                        .Where(x => x.locationid == location)
                        .Sum(x => x.quantity);

                    if (balanceStock - quantity < 0)
                        throw new Exception("Item id " +
                            condition.ItemID + " นี้ไม่พอจ่ายให้กับชาวไร่.");
                }

                uow.InvoiceDetailRepository
                    .Add(new InvoiceDetail
                    {
                        InvoiceNo = invoiceNo,
                        ConditionID = conditionID,
                        Quantity = quantity,
                        UnitPrice = condition.UnitPrice,
                        RecordDate = DateTime.Now,
                        ModifiedDate = DateTime.Now,
                        RecordUser = user,
                        ModifiedUser = user,
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddByPackage(string packageCode, string invoiceNo, string recordUser)
        {
            try
            {
                var invoice = uow.InvoiceRepository
                    .GetSingle(x => x.InvoiceNo == invoiceNo);
                if (invoice == null)
                    throw new Exception("ไม่พบ invoice รหัส " + invoiceNo + " ในระบบ");

                var invoiceItems = GetByInvoiceNo(invoiceNo);
                if (invoice.IsFinish == true)
                    throw new Exception("Invoice รหัส " + invoice.InvoiceNo +
                        " ถูกเปลี่ยนสถานะเป็น finish แล้ว จึงไม่สามารถเพิ่มข้อมูลได้");

                if (uow.AccountInvoiceRepository
                    .Get(x => x.RefInvoiceNo == invoiceNo).Count() > 0)
                    throw new Exception("Invoice นี้มีข้อมูลการออกใบส่งปัจจัยการผลิตไปแล้ว ไม่สามารถแก้ไขข้อมูลได้");

                var package = BuyingFacade.CropInputsPackageBL().GetSingle(packageCode);
                if (package == null)
                    throw new Exception("ไม่พบข้อมูล package นี้ในระบบ");

                var packages = package.CropInputsPackageDetails;

                if (invoiceItems.Where(x => x.PackageCode == package.PackageCode).Count() > 0)
                    throw new Exception("package นี้ถูกเพิ่มเข้าไปใน invoice no " + invoiceNo + " นี้แล้ว" +
                        "ใน 1 invoice แพ็คเกจที่เลือกจะต้องไม่ซ้ำกัน");

                foreach (var item in invoiceItems)
                    if (packages.Where(x => x.ConditionID == item.ConditionID).Count() > 0)
                        throw new Exception("มีบางรายการสินค้าซ้ำใน invoice ไม่สามารถเพิ่มแพ็คเกจนี้ได้" +
                            " (ใน 1 invoice สินค้าจะต้องไม่ซ้ำกัน)");

                ///ใช้เป็นตัวคูณกรณีเป็น item ที่จ่ายให้แบบ quantity * sign contract rai
                var contract = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == invoice.Crop &&
                    x.FarmerCode == invoice.FarmerCode);

                ///ใช้สำหรับตรวจเช็ค item ในสต็อคแยกตาม location ของ item ใน transaction
                var location = "";
                var supplier = uow.SupplierRepository
                    .GetSingle(x => x.SupplierCode == package.SupplierCode);

                if (supplier.SupplierArea == "PET")
                    location = "01";
                else if (supplier.SupplierArea == "SUK")
                    location = "02";
                else if (supplier.SupplierArea == "CM")
                    location = "03";
                else
                    location = "";

                foreach (var item in BuyingFacade.CropInputsPackageDetailsBL()
                    .GetByPackageCode(package.PackageCode)
                    .Where(x => x.CropInputsCondition.IsVat == invoice.IsIncludeVAT))
                {
                    var requestQty = item.Quantity * Convert.ToInt16(item.CropInputsCondition.IsUsePerRai == true ?
                        contract.QuotaFromSignContract : 1);

                    ///กรณีเป็น item ที่ต้องเช็คกับของที่มีอยู่ในสต็อค(IsReferToInventory)
                    if (item.CropInputsCondition.IsReferToInventory == true)
                    {
                        var balanceStock = AGMInventorySystemBL
                            .BLServices
                            .material_transactionBL()
                            .GetByItem(item.CropInputsCondition.ItemID)
                            .Where(x => x.locationid == location);

                        if (balanceStock.Sum(x => x.quantity) - requestQty < 0)
                            throw new Exception("Item id " +
                                item.CropInputsCondition.ItemID + " นี้ไม่พอจ่ายให้กับชาวไร่.");
                    }

                    uow.InvoiceDetailRepository
                        .Add(new InvoiceDetail
                        {
                            InvoiceNo = invoiceNo,
                            ConditionID = item.ConditionID,
                            Quantity = requestQty,
                            UnitPrice = item.CropInputsCondition.UnitPrice,
                            PackageCode = item.PackageCode,
                            RecordDate = DateTime.Now,
                            RecordUser = recordUser,
                            ModifiedDate = DateTime.Now,
                            ModifiedUser = recordUser
                        });

                    ///ตัดสต๊อกออกจากระบบ AGM Inventory System
                    ///
                    //var inventoryItem = AGMInventorySystemBL
                    //    .BLServices
                    //    .material_itemBL()
                    //    .GetSingle(item.CropInputsCondition.ItemID);
                    //if (inventoryItem == null)
                    //    throw new Exception("ไม่พบข้อมูล item นี้ใน AGM inventory โปรดติดต่อผู้ดูแลระบบ");

                    //if (item.CropInputsCondition.IsReferToInventory == true)
                    //    AGMInventorySystemBL.BLServices
                    //        .material_transactionBL()
                    //        .Out(new AGMInventorySystemEntities
                    //        .material_transaction
                    //        {
                    //            crop = item.CropInputsCondition.Crop,
                    //            refid = invoice.InvoiceNo,
                    //            itemid = inventoryItem.itemid,
                    //            unitid = inventoryItem.unitid,
                    //            quantity = totalQuantity,
                    //            unitprice = item.CropInputsCondition.UnitPrice,
                    //            locationid = location,
                    //            transactiontype = "Out",
                    //            modifieduser = recordUser,
                    //            modifieddate = DateTime.Now
                    //        });
                }
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string invoiceNo, Guid conditionID)
        {
            try
            {
                var model = GetSingle(invoiceNo, conditionID);
                if (model == null)
                    throw new Exception("ไม่พบข้อมูลในระบบ");

                if (model.Invoice.IsFinish == true)
                    throw new Exception("Invoice รหัส " + model.InvoiceNo +
                        " ถูกเปลี่ยนสถานะเป็น finish แล้ว จึงไม่สามารถลบข้อมูลได้");

                if (uow.AccountInvoiceRepository
                    .Get(x => x.RefInvoiceNo == model.InvoiceNo).Count() > 0)
                    throw new Exception("Invoice นี้มีข้อมูลการออกใบส่งปัจจัยการผลิตไปแล้ว ไม่สามารถแก้ไขข้อมูลได้");

                if (model.PackageCode != null)
                    throw new Exception("ไม่สามารถลบรายการนี้ได้เนื่องจากเป็นรายการที่อยู่ใน minimum package.");

                uow.InvoiceDetailRepository.Remove(model);
                uow.Save();

                /// return item to AGM Inventory.
                /// 
                //var item = AGMInventorySystemBL.BLServices.material_itemBL()
                //    .GetSingle(model.CropInputsCondition.ItemID);
                //if (item == null)
                //    throw new Exception("ไม่พบข้อมูล item นี้ใน AGM inventory โปรดติดต่อผู้ดูแลระบบ");

                //if (model.CropInputsCondition.IsReferToInventory == true)
                //{
                //    var location = "";
                //    var supplier = uow.SupplierRepository
                //        .GetSingle(x => x.SupplierCode == model.CropInputsCondition.SupplierCode);

                //    if (supplier.SupplierArea == "PET")
                //        location = "01";
                //    else if (supplier.SupplierArea == "SUK")
                //        location = "02";
                //    else
                //        location = "";

                //    AGMInventorySystemBL.BLServices
                //            .material_transactionBL()
                //            .In(new AGMInventorySystemEntities
                //            .material_transaction
                //            {
                //                crop = model.CropInputsCondition.Crop,
                //                refid = model.InvoiceNo,
                //                itemid = item.itemid,
                //                unitid = item.unitid,
                //                quantity = model.Quantity,
                //                unitprice = model.UnitPrice,
                //                locationid = location,
                //                transactiontype = "In",
                //                modifieduser = model.ModifiedUser,
                //                modifieddate = DateTime.Now
                //            });
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByPackage(string invoiceNo, string packageCode)
        {
            try
            {
                var invoice = uow.InvoiceRepository
                    .GetSingle(x => x.InvoiceNo == invoiceNo);
                if (invoice == null)
                    throw new Exception("ไม่พบข้อมูล invoice นี้ในระบบ");

                if (invoice.IsFinish == true)
                    throw new Exception("Invoice รหัส " + invoice.InvoiceNo +
                        " ถูกเปลี่ยนสถานะเป็น finish แล้ว จึงไม่สามารถลบข้อมูลได้");

                if (uow.AccountInvoiceRepository
                    .Get(x => x.RefInvoiceNo == invoiceNo).Count() > 0)
                    throw new Exception("Invoice นี้มีข้อมูลการออกใบส่งปัจจัยการผลิตไปแล้ว ไม่สามารถแก้ไขข้อมูลได้");

                foreach (var model in BuyingFacade.InvoiceDetailsBL()
                    .GetByInvoiceNo(invoiceNo)
                    .Where(x => x.PackageCode == packageCode))
                {
                    if (model == null)
                        throw new Exception("ไม่พบข้อมูลในระบบ");

                    if (model.PackageCode == null)
                        throw new Exception("ไม่สามารถลบข้อมูลนี้ได้เนื่องจากไม่พบรหัส package code ของรายการข้อมูลดังกล่าว");

                    uow.InvoiceDetailRepository.Remove(model);

                    /// return item to AGM Inventory.
                    /// 
                    //var item = AGMInventorySystemBL.BLServices.material_itemBL()
                    //    .GetSingle(model.CropInputsCondition.ItemID);
                    //if (item == null)
                    //    throw new Exception("ไม่พบข้อมูล item นี้ใน AGM inventory โปรดติดต่อผู้ดูแลระบบ");

                    //if (model.CropInputsCondition.IsReferToInventory == true)
                    //{
                    //    var location = "";
                    //    var supplier = uow.SupplierRepository
                    //        .GetSingle(x => x.SupplierCode == model.CropInputsCondition.SupplierCode);

                    //    if (supplier.SupplierArea == "PET")
                    //        location = "01";
                    //    else if (supplier.SupplierArea == "SUK")
                    //        location = "02";
                    //    else
                    //        location = "";

                    //    AGMInventorySystemBL.BLServices
                    //            .material_transactionBL()
                    //            .In(new AGMInventorySystemEntities
                    //            .material_transaction
                    //            {
                    //                crop = model.CropInputsCondition.Crop,
                    //                refid = model.InvoiceNo,
                    //                itemid = item.itemid,
                    //                unitid = item.unitid,
                    //                quantity = model.Quantity,
                    //                unitprice = model.UnitPrice,
                    //                locationid = location,
                    //                transactiontype = "In",
                    //                modifieduser = model.ModifiedUser,
                    //                modifieddate = DateTime.Now
                    //            });
                    //}
                }
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<InvoiceDetail> GetByInvoiceNo(string invoiceNo)
        {
            return uow.InvoiceDetailRepository
                .Get(x => x.InvoiceNo == invoiceNo
                , null
                , x => x.CropInputsCondition);
        }

        public List<InvoiceDetail> GetByAccountInvoiceNo(string accountInvoiceNo)
        {
            return uow.InvoiceDetailRepository
                .Get(x => x.AccountInvoiceNo == accountInvoiceNo
                , null
                , x => x.CropInputsCondition);
        }

        public InvoiceDetail GetSingle(string invoiceNo, Guid conditionID)
        {
            return uow.InvoiceDetailRepository
                .GetSingle(x => x.InvoiceNo == invoiceNo &&
                x.ConditionID == conditionID
                , x => x.Invoice
                , x => x.CropInputsCondition);
        }

        public void ChangeIsCashStatus(InvoiceDetail model)
        {
            try
            {
                var invoice = uow.InvoiceRepository
                    .GetSingle(x => x.InvoiceNo == model.InvoiceNo);
                if (invoice == null)
                    throw new Exception("ไม่พบข้อมูล invoice นี้ในระบบ");

                if (invoice.IsFinish == true)
                    throw new Exception("Invoice รหัส " + invoice.InvoiceNo +
                        " ถูกเปลี่ยนสถานะเป็น finish แล้วก่อนหน้านี้");

                if (uow.AccountInvoiceRepository
                    .Get(x => x.RefInvoiceNo == model.InvoiceNo).Count() > 0)
                    throw new Exception("Invoice นี้มีข้อมูลการออกรหัส account invoice ไปแล้ว ไม่สามารถแก้ไขข้อมูลได้");

                var edit = GetSingle(model.InvoiceNo, model.ConditionID);

                edit.IsCash = model.IsCash;
                edit.Quantity = model.Quantity;
                edit.ModifiedDate = DateTime.Now;
                edit.ModifiedUser = model.ModifiedUser;

                uow.InvoiceDetailRepository.Update(edit);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(InvoiceDetail model)
        {
            try
            {
                var invoice = uow.InvoiceRepository
                    .GetSingle(x => x.InvoiceNo == model.InvoiceNo);
                if (invoice == null)
                    throw new Exception("ไม่พบข้อมูล invoice นี้ในระบบ");

                if (invoice.IsFinish == true)
                    throw new Exception("Invoice รหัส " + invoice.InvoiceNo +
                        " ถูกเปลี่ยนสถานะเป็น finish แล้วก่อนหน้านี้");

                if (model.PackageCode != null)
                    throw new Exception("ไม่สามารถแก้ไขข้อมูลนี้ได้เนื่องจากถูกระบุว่าเป็นรายการใน Minimum package.");

                var edit = GetSingle(model.InvoiceNo, model.ConditionID);

                edit.Quantity = model.Quantity;
                edit.ModifiedDate = DateTime.Now;
                edit.ModifiedUser = model.ModifiedUser;

                uow.InvoiceDetailRepository.Update(edit);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<InvoiceDetail> GetByFarmer(short crop, string farmerCode)
        {
            return uow.InvoiceDetailRepository
                .Get(x => x.Invoice.Crop == crop &&
                x.Invoice.FarmerCode == farmerCode
                , null
                , x => x.Invoice
                , x => x.CropInputsCondition);
        }

        public List<InvoiceDetail> GetByExtensionAgent(short crop, string extensionAgentCode)
        {
            return uow.InvoiceDetailRepository
                .Get(x => x.Invoice.Crop == crop &&
                x.Invoice.RegistrationFarmer.Farmer.Supplier == extensionAgentCode
                , null
                , x => x.Invoice
                , x => x.AccountInvoice
                , x => x.AccountInvoice.AccountCreditNotes
                , x => x.CropInputsCondition
                , x => x.Invoice.RegistrationFarmer
                , x => x.Invoice.RegistrationFarmer.Farmer);
        }
    }
}
