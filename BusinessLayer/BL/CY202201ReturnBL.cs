﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface ICY202201ReturnBL
    {
        void Add(short crop, string farmerCode, string createUser);
        void Finish(string returnNo, string modifiedUser);
        void UnFinish(string returnNo, string modifiedUser);
        void Delete(string returnNo);
        CY202201Return GetSingle(string returnNo);
        List<CY202201Return> GetByCrop(short crop);
        List<CY202201Return> GetByFarmerCode(short crop, string farmerCode);
    }

    public class CY202201ReturnBL : ICY202201ReturnBL
    {
        UnitOfWork uow;
        public CY202201ReturnBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(short crop, string farmerCode, string createUser)
        {
            try
            {
                Helper.SystemDeadlinesHelper.BeforeStartBuyingDeadLine(crop, farmerCode);
                if (createUser == null)
                    throw new ArgumentException("โปรดระบุผู้บันทึกข้อมูล");

                if (createUser == null)
                    throw new ArgumentException("โปรดระบุผู้บันทึกข้อมูล");

                if (string.IsNullOrEmpty(farmerCode))
                    throw new ArgumentException("โปรดระบุ Farmer Code");

                var documentList = BuyingFacade.BuyingDocumentBL()
                    .GetByFarmer(crop, farmerCode);
                if (documentList.Count() > 0)
                    throw new ArgumentException("มีการเริ่มบันทึกข้อมูลการขายไปแล้ว");

                var list = GetByFarmerCode(crop, farmerCode)
                    .Select(x => new { No = x.ReturnNo.Substring(x.ReturnNo.Length - 2) })
                    .ToList();

                var max = 0;
                if (list.Count() < 1)
                    max = 1;
                else
                    max = Convert.ToInt16(list.Max(x => x.No)) + 1;

                var returnNo = "CY2022/01-" + farmerCode + "-" + max.ToString().PadLeft(2, '0');

                uow.CY202201ReturnRepository
                    .Add(new CY202201Return
                    {
                        ReturnNo = returnNo,
                        Crop = crop,
                        FarmerCode = farmerCode,
                        CreateDate = DateTime.Now.Date,
                        CreateUser = createUser,
                        IsFinish = false,
                        FinishDate = null,
                        ModifiedDate = DateTime.Now,
                        ModifiedUser = createUser
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string returnNo)
        {
            try
            {
                var model = GetSingle(returnNo);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (model.IsFinish == true)
                    throw new ArgumentException("ReturnNo นี้ถูก finish ไปแล้ว");

                if (model.CY202201ReturnDetail.Count() > 0)
                    throw new ArgumentException("ReturnNo นี้มีรายการคืนปัจจัยอยู่ภายใน โปรดลบข้อมูลภายในออกทั้งหมดก่อน");

                uow.CY202201ReturnRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Finish(string returnNo, string modifiedUser)
        {
            try
            {
                var edit = GetSingle(returnNo);
                if (edit == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                edit.ModifiedDate = DateTime.Now;
                edit.ModifiedUser = modifiedUser;
                edit.IsFinish = true;
                edit.FinishDate = DateTime.Now;

                uow.CY202201ReturnRepository.Update(edit);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CY202201Return> GetByCrop(short crop)
        {
            return uow.CY202201ReturnRepository.Get(x => x.Crop == crop);
        }

        public List<CY202201Return> GetByFarmerCode(short crop, string farmerCode)
        {
            return uow.CY202201ReturnRepository
                .Get(x => x.Crop == crop &&
                x.FarmerCode == farmerCode);
        }

        public CY202201Return GetSingle(string returnNo)
        {
            return uow.CY202201ReturnRepository
                .GetSingle(x => x.ReturnNo == returnNo,
                x => x.CY202201ReturnDetail);
        }

        public void UnFinish(string returnNo, string modifiedUser)
        {
            try
            {
                var edit = GetSingle(returnNo);
                if (edit == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                var documentList = BuyingFacade.BuyingDocumentBL()
                    .GetByFarmer(edit.Crop, edit.FarmerCode);
                if (documentList.Count() > 0)
                    throw new ArgumentException("มีการเริ่มบันทึกข้อมูลการขายไปแล้ว");

                edit.ModifiedDate = DateTime.Now;
                edit.ModifiedUser = modifiedUser;
                edit.IsFinish = false;

                uow.CY202201ReturnRepository.Update(edit);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
