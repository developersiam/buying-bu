﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IAccountInvoiceBL
    {
        string GenerateInvoiceNo(string refInvoiceNo, string invoiceForm, string createBy, DateTime createDate);
        void LockPrint(string accountInvoice, string printBy);
        void UnLockPrint(string accountInvoice);
        List<AccountInvoice> GetByRefInvoice(string refInvoiceNo);
        List<AccountInvoice> GetByArea(string areaCode, DateTime from, DateTime to);
        List<AccountInvoice> GetByFarmer(short crop, string farmerCode);
        List<AccountInvoice> GetByFarmerWithOutCreditNote(short crop, string farmerCode);
        List<AccountInvoice> GetByExtensionAgent(short crop, string extensionAgentCode);
        List<AccountInvoice> GetByExtensionAgentWithOutCreditNote(short crop, string extensionAgentCode);
        AccountInvoice GetSingle(string accountInvoiceNo);
        bool IsMaxInvoice(string AccountInvoice);
        List<AccountInvoice> SearchByInvoiceNo(string accountInvoiceNo);
        List<AccountInvoice> GetByFarmerName(short crop, string extensionAgentCode, string farmerName);
    }

    public class AccountInvoiceBL : IAccountInvoiceBL
    {
        UnitOfWork _uow;
        public AccountInvoiceBL()
        {
            _uow = new UnitOfWork();
        }

        public string GenerateInvoiceNo(string refInvoiceNo, string invoiceForm, string createBy, DateTime createDate)
        {
            try
            {
                /// Generate invoice no.
                /// 
                int _max;
                string _invoiceNo;
                string _year = createDate.Year.ToString().Substring(2, 2);
                string _month = createDate.Month.ToString().PadLeft(2, '0');

                var list = _uow.AccountInvoiceRepository
                    .Get(x => x.InvoiceNo.Substring(3, 2) == _year &&
                    x.InvoiceNo.Substring(5, 2) == _month &&
                    x.InvoiceNo.Substring(0, 3) == invoiceForm);

                if (list.Count() <= 0)
                    _max = 1;
                else
                    _max = list.Max(x => Convert.ToInt16(x.InvoiceNo.Substring(7, 4))) + 1;

                _invoiceNo = invoiceForm + _year + _month + _max.ToString().PadLeft(4, '0');

                _uow.AccountInvoiceRepository.Add(new AccountInvoice
                {
                    InvoiceNo = _invoiceNo,
                    RefInvoiceNo = refInvoiceNo,
                    CreateDate = createDate,
                    CreateBy = createBy
                });

                _uow.Save();
                return _invoiceNo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AccountInvoice> GetByRefInvoice(string refInvoiceNo)
        {
            return _uow.AccountInvoiceRepository
                .Get(x => x.RefInvoiceNo == refInvoiceNo,
                null,
                x => x.InvoiceDetails)
                .ToList();
        }

        public List<AccountInvoice> GetByArea(string areaCode, DateTime from, DateTime to)
        {
            return _uow.AccountInvoiceRepository
                .Get(x => x.Invoice.RegistrationFarmer.Farmer.Supplier1.SupplierArea == areaCode &&
                x.CreateDate >= from &&
                x.CreateDate <= to,
                null,
                x => x.InvoiceDetails,
                x => x.Invoice,
                x => x.Invoice.RegistrationFarmer,
                x => x.Invoice.RegistrationFarmer.Farmer,
                x => x.Invoice.RegistrationFarmer.Farmer.Supplier1,
                x => x.Invoice.RegistrationFarmer.Farmer.Supplier1.Area,
                x => x.Invoice.RegistrationFarmer.Farmer.Person)
                .ToList();
        }

        public List<AccountInvoice> GetByFarmer(short crop, string farmerCode)
        {
            return _uow.AccountInvoiceRepository
                .Get(x => x.Invoice.Crop == crop &&
                x.Invoice.FarmerCode == farmerCode,
                null,
                x => x.Invoice,
                x => x.Invoice.DebtType,
                x => x.InvoiceDetails,
                x => x.AccountCreditNotes);
        }

        public List<AccountInvoice> GetByFarmerName(short crop, string area, string farmerName)
        {
            var farmers = _uow.FarmerRepository
                .Get(x => x.Person.FirstName.Contains(farmerName) &&
                x.Supplier1.SupplierArea == area)
                .ToList();
            var invoices = new List<AccountInvoice>();
            foreach (var item in farmers)
            {
                var list = _uow.AccountInvoiceRepository
                    .Get(x => x.Invoice.FarmerCode == item.FarmerCode &&
                    x.Invoice.Crop == crop
                    , null
                    , x => x.Invoice
                    , x => x.Invoice.RegistrationFarmer
                    , x => x.Invoice.RegistrationFarmer.Farmer)
                    .ToList();
                invoices.AddRange(list);
            }

            return invoices;
        }

        public AccountInvoice GetSingle(string accountInvoiceNo)
        {
            return _uow.AccountInvoiceRepository
                .GetSingle(x => x.InvoiceNo == accountInvoiceNo,
                x => x.Invoice,
                x => x.Invoice.DebtType);
        }

        public void LockPrint(string accountInvoice, string printBy)
        {
            try
            {
                var model = _uow.AccountInvoiceRepository.GetSingle(x => x.InvoiceNo == accountInvoice);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล account invoice รหัส " + accountInvoice + " นี้ในระบบ");

                model.IsPrinted = true;
                model.PrintDate = DateTime.Now;
                model.PrintBy = printBy;

                _uow.AccountInvoiceRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UnLockPrint(string accountInvoice, string printBy)
        {
            try
            {
                var model = _uow.AccountInvoiceRepository.GetSingle(x => x.InvoiceNo == accountInvoice);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูล account invoice รหัส " + accountInvoice + " นี้ในระบบ");

                model.IsPrinted = false;

                _uow.AccountInvoiceRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UnLockPrint(string accountInvoice)
        {
            try
            {
                var model = _uow.AccountInvoiceRepository.GetSingle(x => x.InvoiceNo == accountInvoice);
                if (model == null) throw new ArgumentException("ไม่พบข้อมูล account invoice รหัส " + accountInvoice + " นี้ในระบบ");

                model.PrintDate = null;
                model.PrintBy = "";
                model.IsPrinted = false;

                _uow.AccountInvoiceRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsMaxInvoice(string AccountInvoice)
        {
            try
            {
                int _max = 0;
                var _run = Convert.ToInt16(AccountInvoice.Substring(7, 4));
                var startsWith = AccountInvoice.Substring(0, 7);
                var accList = _uow.AccountInvoiceRepository.Get(x => x.InvoiceNo.StartsWith(startsWith)).ToList();
                _max = accList.Max(x => Convert.ToInt16(x.InvoiceNo.Substring(7, 4)));

                var result = _run == _max;
                return result;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AccountInvoice> GetByExtensionAgent(short crop, string extensionAgentCode)
        {
            return _uow.AccountInvoiceRepository
                .Get(x => x.Invoice.Crop == crop
                && x.Invoice.RegistrationFarmer.Farmer.Supplier == extensionAgentCode,
                null,
                x => x.AccountCreditNotes,
                x => x.InvoiceDetails,
                x => x.Invoice,
                x => x.Invoice.DebtType,
                x => x.Invoice.RegistrationFarmer)
                .ToList();
        }

        public List<AccountInvoice> GetByExtensionAgentWithOutCreditNote(short crop, string extensionAgentCode)
        {
            try
            {
                var all = _uow.AccountInvoiceRepository
                .Get(x => x.Invoice.Crop == crop
                && x.Invoice.RegistrationFarmer.Farmer.Supplier == extensionAgentCode,
                null
                , x => x.Invoice)
                .ToList();

                var useToCreditNoteList = _uow.AccountCreditNoteRepository
                    .Get(x => x.AccountInvoice.Invoice.Crop == crop &&
                    x.AccountInvoice.Invoice.RegistrationFarmer.Farmer.Supplier == extensionAgentCode
                    , null
                    , x => x.AccountInvoice
                    , x => x.AccountInvoice.Invoice)
                    .Select(x => x.AccountInvoice)
                    .ToList();

                var result = all.Except(useToCreditNoteList).ToList();

                return result;
            }

            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AccountInvoice> GetByFarmerWithOutCreditNote(short crop, string farmerCode)
        {
            try
            {
                List<AccountInvoice> list = _uow.AccountInvoiceRepository
                    .Get(x => x.Invoice.Crop == crop &&
                    x.Invoice.FarmerCode == farmerCode
                    , null
                    , x => x.InvoiceDetails)
                    .ToList();
                List<AccountInvoice> creditNoteList = _uow.AccountCreditNoteRepository
                    .Get(x => x.AccountInvoice.Invoice.Crop == crop &&
                    x.AccountInvoice.Invoice.FarmerCode == farmerCode)
                    .Select(x => x.AccountInvoice)
                    .ToList();
                var result = list.Except(creditNoteList).ToList();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AccountInvoice> SearchByInvoiceNo(string accountInvoiceNo)
        {
            var list = _uow.AccountInvoiceRepository.Get(x => x.InvoiceNo.Contains(accountInvoiceNo));
            return list;
        }
    }
}
