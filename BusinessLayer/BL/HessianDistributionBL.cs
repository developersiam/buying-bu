﻿using BusinessLayer.Model;
using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IHessianDistributionBL
    {
        void Add(HessianDistribution model);
        void Update(HessianDistribution model, m_BuyingDocument voucher);
        void Delete(Guid Id);
        List<HessianDistribution> GetByDistributionDate(string areaCode, DateTime distributionDate);
        List<HessianDistribution> GetByFarmerDistributionDate(string farmerCode, DateTime distributionDate);
        List<HessianDistribution> GetByFarmer(short crop, string farmerCode);
        List<HessianDistribution> GetByArea(short crop, string areaCode);
        List<HessianDistribution> GetByWarehouse(short crop, Guid warehouseId);
        HessianDistribution GetBySingle(Guid Id);
        List<HessianDistribution> GetByVoucher(short crop, string farmerCode, string stationCode, short documentNumber);
    }
    public class HessianDistributionBL : IHessianDistributionBL
    {
        UnitOfWork uow;
        public HessianDistributionBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(HessianDistribution model)
        {
            try
            {
                var validation = new DomainValidation.IsHessianDistributionValid().Validate(model);

                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var regisModel = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == model.Crop && x.FarmerCode == model.FarmerCode);

                if (regisModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รหัส " + model.FarmerCode + " ไม่สามารถบันทึกข้อมูลได้");

                /// จำนวนกระสอบที่จะจ่ายต้องไม่เกินจำนวนห่อยาที่นำมาขายตามในใบเวาเชอร์
                /// 
                var buying = BuyingFacade.BuyingBL()
                    .GetByBuyingDocument(model.Crop,
                    model.FarmerCode,
                    model.BuyingStationCode,
                    model.BuyingDocumentNumber)
                    .Where(x => x.Grade != null)
                    .ToList();

                var distributed = GetByVoucher(model.Crop,
                     model.FarmerCode,
                     model.BuyingStationCode,
                     model.BuyingDocumentNumber)
                     .Sum(x => x.Quantity);

                if (model.Quantity + distributed > buying.Count * 2)
                    throw new ArgumentException("จำนวนกระสอบที่จะจ่ายต้องไม่เกินจำนวนห่อยาที่ชั่งน้ำหนักแล้ว ห่อยาที่ชั่งน้ำหนักแล้วคือ " + buying.Count +
                       " ห่อ จะต้องได้รับกระสอบจำนวนไม่เกิน " + buying.Count * 2 + " ผืน");

                //DateTime dtFrom = Convert.ToDateTime(voucher.BuyingDate);
                //var totalDistributedPerDay = uow.HessianDistributionRepository
                //    .Get(x => x.DistributedDate.Day == dtFrom.Day &&
                //    x.DistributedDate.Month == dtFrom.Month &&
                //    x.DistributedDate.Year == dtFrom.Year &&
                //    x.FarmerCode == voucher.FarmerCode)
                //    .Sum(y => y.Quantity);

                //var list = uow.BuyingRepository
                //        .Query(x => x.Crop == voucher.Crop &&
                //        x.BuyingStationCode == voucher.BuyingStationCode &&
                //        x.FarmerCode == voucher.FarmerCode &&
                //        x.BuyingDocument.BuyingDate != null,
                //        null,
                //        x => x.BuyingDocument)
                //        .ToList();

                //var totalSold = list
                //    .Where(x => Convert.ToDateTime(x.BuyingDocument.BuyingDate).Date == dtFrom.Date &&
                //    x.Weight != null)
                //    .ToList();

                //if (model.Quantity + totalDistributedPerDay > totalSold.Count * 2)
                //    throw new ArgumentException("จำนวนกระสอบที่จะจ่ายต้องไม่เกินจำนวนห่อยาที่ชั่งน้ำหนักแล้ว ห่อยาที่ชั่งน้ำหนักแล้วคือ " + totalSold.Count +
                //       " ห่อ จะต้องได้รับกระสอบจำนวนไม่เกิน " + totalSold.Count * 2 + " ผืน");

                /// ตรวจสอบว่าจ่ายกระสอบในล็อต ReceivingCode ดังกล่าวไปครบแล้วหรือไม่
                ///
                var totalReceived = uow.HessianReceivingRepository
                    .GetSingle(x => x.ReceivingCode == model.ReceivingCode);

                var totalDistributed = uow.HessianDistributionRepository
                    .Get(x => x.ReceivingCode == model.ReceivingCode)
                    .Sum(x => x.Quantity);

                if (totalDistributed >= totalReceived.Quantity)
                    throw new ArgumentException("ท่านได้จ่ายกระสอบใน Receiving Code : " + model.ReceivingCode +
                        " ไปครบตามจำนวนแล้ว โปรดเลือกกระสอบจาก Receiving Code อื่น");

                if (totalDistributed + model.Quantity > totalReceived.Quantity)
                    throw new ArgumentException("ท่านสามารถเลือกใช้กระสอบใน Receiving Code : " + model.ReceivingCode +
                        " ได้อีกเพียง " + (totalReceived.Quantity - totalDistributed) + " กระสอบเท่านั้น" +
                        " โปรดแก้ไขจำนวนที่จ่ายใน Receiving Code นี้และเลือกจ่ายกระสอบจาก Receiving Code อื่นเพิ่มเติม");

                model.ModifiedBy = model.DistributedBy;
                model.ModifiedDate = DateTime.Now;

                uow.HessianDistributionRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid Id)
        {
            try
            {
                var model = uow.HessianDistributionRepository.GetSingle(x => x.DistributionID == Id);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลที่ต้องการลบ");

                uow.HessianDistributionRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HessianDistribution> GetByArea(short crop, string areaCode)
        {
            return uow.HessianDistributionRepository
                .Get(x => x.Crop == crop && x.HessianReceiving.Warehouse.AreaCode == areaCode)
                .ToList();
        }

        public List<HessianDistribution> GetByDistributionDate(string areaCode, DateTime distributionDate)
        {
            return uow.HessianDistributionRepository
                .Get(x => x.DistributedDate.Day == distributionDate.Day &&
                x.DistributedDate.Month == distributionDate.Month &&
                x.DistributedDate.Year == distributionDate.Year &&
                x.HessianReceiving.Warehouse.AreaCode == areaCode,
                null,
                x => x.HessianReceiving,
                x => x.HessianReceiving.Warehouse)
                .ToList();
        }

        public List<HessianDistribution> GetByFarmerDistributionDate(string farmerCode, DateTime distributionDate)
        {
            return uow.HessianDistributionRepository
                .Get(x => x.DistributedDate.Day == distributionDate.Day &&
                x.DistributedDate.Month == distributionDate.Month &&
                x.DistributedDate.Year == distributionDate.Year &&
                x.FarmerCode == farmerCode,
                null,
                x => x.HessianReceiving,
                x => x.HessianReceiving.Warehouse)
                .ToList();
        }

        public List<HessianDistribution> GetByFarmer(short crop, string farmerCode)
        {
            return uow.HessianDistributionRepository
                .Get(x => x.Crop == crop && x.FarmerCode == farmerCode)
                .ToList();
        }

        public HessianDistribution GetBySingle(Guid Id)
        {
            return uow.HessianDistributionRepository.GetSingle(x => x.DistributionID == Id);
        }

        public List<HessianDistribution> GetByWarehouse(short crop, Guid warehouseId)
        {
            return uow.HessianDistributionRepository
                .Get(x => x.Crop == crop && x.HessianReceiving.WarehouseID == warehouseId)
                .ToList();
        }

        public void Update(HessianDistribution model, m_BuyingDocument voucher)
        {
            try
            {
                var regisModel = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == voucher.Crop && x.FarmerCode == voucher.FarmerCode);

                if (regisModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รหัส " + model.FarmerCode + " ไม่สามารถบันทึกข้อมูลได้");

                var editModel = uow.HessianDistributionRepository
                    .GetSingle(x => x.DistributionID == model.DistributionID);

                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลที่ต้องการแก้ไข");

                /// จำนวนกระสอบที่จะจ่ายต้องไม่เกินจำนวนห่อยาที่นำมาขายตามในใบเวาเชอร์
                /// 
                var buying = BuyingFacade.BuyingBL()
                    .GetByBuyingDocument(voucher.Crop,
                    voucher.FarmerCode,
                    voucher.BuyingStationCode,
                    voucher.BuyingDocumentNumber)
                    .Where(x => x.Grade != null)
                    .ToList();

                var distributed = GetByVoucher(voucher.Crop,
                    voucher.FarmerCode,
                    voucher.BuyingStationCode,
                    voucher.BuyingDocumentNumber)
                    .Sum(x => x.Quantity);

                if (model.Quantity + (distributed - editModel.Quantity) > buying.Count * 2)
                    throw new ArgumentException("จำนวนกระสอบที่จะจ่ายต้องไม่เกินจำนวนห่อยาที่ชั่งน้ำหนักแล้ว ห่อยาที่ชั่งน้ำหนักแล้วคือ " + buying.Count +
                       " ห่อ จะต้องได้รับกระสอบจำนวนไม่เกิน " + buying.Count * 2 + " ผืน");

                //DateTime dtFrom = Convert.ToDateTime(voucher.BuyingDate);
                //var totalDistributedPerDay = uow.HessianDistributionRepository
                //    .Get(x => x.DistributedDate.Day == dtFrom.Day &&
                //    x.DistributedDate.Month == dtFrom.Month &&
                //    x.DistributedDate.Year == dtFrom.Year &&
                //    x.FarmerCode == voucher.FarmerCode)
                //    .Sum(y => y.Quantity);

                //var list = uow.BuyingRepository
                //        .Query(x => x.Crop == voucher.Crop &&
                //        x.BuyingStationCode == voucher.BuyingStationCode &&
                //        x.FarmerCode == voucher.FarmerCode &&
                //        x.BuyingDocument.BuyingDate != null,
                //        null,
                //        x => x.BuyingDocument)
                //        .ToList();

                //var totalSold = list
                //    .Where(x => Convert.ToDateTime(x.BuyingDocument.BuyingDate).Date == dtFrom.Date &&
                //    x.Weight != null)
                //    .ToList();

                //if (model.Quantity + (totalDistributedPerDay - editModel.Quantity) > totalSold.Count * 2)
                //    throw new ArgumentException("จำนวนกระสอบที่จะจ่ายต้องไม่เกินจำนวนห่อยาที่ชั่งน้ำหนักแล้ว ห่อยาที่ชั่งน้ำหนักแล้วคือ " + totalSold.Count +
                //       " ห่อ จะต้องได้รับกระสอบจำนวนไม่เกิน " + totalSold.Count * 2 + " ผืน");

                /// ตรวจสอบว่าจ่ายกระสอบในล็อต ReceivingCode ดังกล่าวไปครบแล้วหรือไม่
                ///
                var totalReceived = uow.HessianReceivingRepository
                    .GetSingle(x => x.ReceivingCode == model.ReceivingCode);

                var totalDistributed = uow.HessianDistributionRepository
                    .Get(x => x.ReceivingCode == model.ReceivingCode)
                    .Sum(x => x.Quantity);

                if (totalDistributed >= totalReceived.Quantity)
                    throw new ArgumentException("ท่านได้จ่ายกระสอบใน Receiving Code : " + model.ReceivingCode +
                        " ไปครบตามจำนวนแล้ว โปรดเลือกกระสอบจาก Receiving Code อื่น");

                if ((totalDistributed - editModel.Quantity) + model.Quantity > totalReceived.Quantity)
                    throw new ArgumentException("ท่านสามารถเลือกใช้กระสอบใน Receiving Code : " + model.ReceivingCode +
                        " ได้อีกเพียง " + (totalReceived.Quantity - totalDistributed) + " กระสอบเท่านั้น" +
                        " โปรดแก้ไขจำนวนที่จ่ายใน Receiving Code นี้และเลือกจ่ายกระสอบจาก Receiving Code อื่นเพิ่มเติม");

                editModel.ReceivingCode = model.ReceivingCode;
                editModel.Quantity = model.Quantity;
                editModel.DistributedDate = Convert.ToDateTime(voucher.BuyingDate);
                editModel.DistributedBy = model.DistributedBy;
                editModel.ModifiedBy = model.DistributedBy;
                editModel.ModifiedDate = DateTime.Now;

                uow.HessianDistributionRepository.Update(editModel);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<HessianDistribution> GetByVoucher(short crop, string farmerCode, string stationCode, short documentNumber)
        {
            try
            {
                return uow.HessianDistributionRepository
                    .Get(x => x.Crop == crop &&
                    x.BuyingStationCode == stationCode &&
                    x.FarmerCode == farmerCode &&
                    x.BuyingDocumentNumber == documentNumber)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
