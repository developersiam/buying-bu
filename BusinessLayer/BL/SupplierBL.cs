﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface ISupplierBL
    {
        void Add(Supplier model);
        void Update(Supplier model);
        void Delete(string supplierCode);
        List<Supplier> GetAll();
        List<Supplier> GetByArea(string areaCode);
        Supplier GetBySingle(string supplierCode);
    }

    public class SupplierBL : ISupplierBL
    {
        UnitOfWork uow;
        public SupplierBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(Supplier model)
        {
            try
            {
                if (model.SupplierCode == null)
                    throw new ArgumentException("ไม่พบข้อมูล SupplierCode");

                if (model.SupplierArea == null)
                    throw new ArgumentException("ไม่พบข้อมูล SupplierArea");

                if (GetBySingle(model.SupplierCode) != null)
                    throw new ArgumentException("มีข้อมูลซ้ำในระบบ!");

                uow.SupplierRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(Supplier model)
        {
            try
            {
                if (model.SupplierCode == null)
                    throw new ArgumentException("ไม่พบข้อมูล SupplierCode");

                if (model.SupplierArea == null)
                    throw new ArgumentException("ไม่พบข้อมูล SupplierArea");

                if (GetBySingle(model.SupplierCode) == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ!");

                uow.SupplierRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string supplierCode)
        {
            try
            {
                var supplier = GetBySingle(supplierCode);
                if (GetBySingle(supplierCode) == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ!");

                uow.SupplierRepository.Remove(supplier);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Supplier> GetAll()
        {
            try
            {
                return uow.SupplierRepository.Get().ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Supplier> GetByArea(string areaCode)
        {
            try
            {
                return uow.SupplierRepository.Query(s => s.SupplierArea == areaCode).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Supplier GetBySingle(string supplierCode)
        {
            try
            {
                return uow.SupplierRepository.GetSingle(s => s.SupplierCode == supplierCode);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
