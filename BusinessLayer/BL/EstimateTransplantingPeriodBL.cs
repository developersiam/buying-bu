﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IEstimateTransplantingPeriodBL
    {
        List<EstimateTransplantingPeriod> GetAll();
    }
    public class EstimateTransplantingPeriodBL : IEstimateTransplantingPeriodBL
    {
        UnitOfWork uow;
        public EstimateTransplantingPeriodBL()
        {
            uow = new UnitOfWork();
        }

        public List<EstimateTransplantingPeriod> GetAll()
        {
            return uow.EstimateTransplantingPeriodRepository
                .Get().OrderBy(x => x.EstimateTransplantingPeriodName).ToList();
        }
    }
}
