﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface ICropInputDistributionBL
    {

        void Add(CropInputDistribution model);
        void Update(CropInputDistribution model);
        void Delete(CropInputDistribution model);
        CropInputDistribution GetByID(short crop, string farmerCode, Guid cropInputItemID);
        List<CropInputDistribution> GetByFarmer(short crop, string farmerCode);
        List<CropInputDistribution> GetByArea(short crop, string areaCode);
        List<CropInputDistribution> GetByCitizenID(short crop, string citizenID);
    }

    public class CropInputDistributionBL : ICropInputDistributionBL
    {
        UnitOfWork uow;
        public CropInputDistributionBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(CropInputDistribution model)
        {
            try
            {
                var validation = new DomainValidation.IsCropInputDistributionValid().Validate(model);

                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var registerModel = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == model.Crop &&
                    x.FarmerCode == model.FarmerCode);

                if (registerModel.FarmerCode == null)
                    throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียน {crop: " + model.Crop + 
                        ", farmerCode: " + model.FarmerCode + "}");

                if (registerModel.SignContractDate == null)
                    throw new ArgumentException("ชาวไร่รายนี้ยังไม่ผ่านขั้นตอนการเซ็นสัญญา โปรดตรวจสอบข้อมูลอีกครั้ง " +
                        "{ crop: " + registerModel.Crop + ", farmerCode: " + registerModel.FarmerCode + " }");

                /// หากมีการหักค่าเมล็ดพันธุ์ไปแล้วจะไม่ให้จ่ายเพิ่ม แก้ไข หรือลบข้อมูลได้
                /// 
                BuyingFacade.DebtorPaymentBL().IsPayment(model.Crop, model.FarmerCode);

                uow.CropInputDistributionRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(CropInputDistribution model)
        {
            try
            {
                var validation = new DomainValidation.IsCropInputDistributionValid().Validate(model);

                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var registerModel = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == model.Crop &&
                    x.FarmerCode == model.FarmerCode);

                if (registerModel.FarmerCode == null)
                    throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียน {crop: " + model.Crop +
                        ", farmerCode: " + model.FarmerCode + "}");

                if (registerModel.SignContractDate == null)
                    throw new ArgumentException("ชาวไร่รายนี้ยังไม่ผ่านขั้นตอนการเซ็นสัญญา โปรดตรวจสอบข้อมูลอีกครั้ง " +
                        "{ crop: " + registerModel.Crop + ", farmerCode: " + registerModel.FarmerCode + " }");

                /// หากมีการหักค่าเมล็ดพันธุ์ไปแล้วจะไม่ให้จ่ายเพิ่ม แก้ไข หรือลบข้อมูลได้
                /// 
                BuyingFacade.DebtorPaymentBL().IsPayment(model.Crop, model.FarmerCode);

                uow.CropInputDistributionRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(CropInputDistribution model)
        {
            try
            {
                var registerModel = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == model.Crop &&
                    x.FarmerCode == model.FarmerCode);

                if (registerModel.FarmerCode == null)
                    throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียน {crop: " + model.Crop +
                        ", farmerCode: " + model.FarmerCode + "}");

                if (registerModel.SignContractDate == null)
                    throw new ArgumentException("ชาวไร่รายนี้ยังไม่ผ่านขั้นตอนการเซ็นสัญญา โปรดตรวจสอบข้อมูลอีกครั้ง " +
                        "{ crop: " + registerModel.Crop + ", farmerCode: " + registerModel.FarmerCode + " }");

                /// หากมีการหักค่าเมล็ดพันธุ์ไปแล้วจะไม่ให้จ่ายเพิ่ม แก้ไข หรือลบข้อมูลได้
                /// 
                BuyingFacade.DebtorPaymentBL().IsPayment(model.Crop, model.FarmerCode);

                var _cropInputDistribution = GetByID(model.Crop,
                    model.FarmerCode,
                    model.CropInputItemID);

                if (_cropInputDistribution == null)
                    throw new ArgumentException("Find not found!");

                uow.CropInputDistributionRepository.Remove(_cropInputDistribution);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CropInputDistribution GetByID(short crop, string farmerCode, Guid cropInputItemID)
        {
            try
            {
                return uow.CropInputDistributionRepository
                    .GetSingle(cid => cid.Crop == crop &&
                    cid.FarmerCode == farmerCode &&
                    cid.CropInputItemID == cropInputItemID,
                    cid => cid.CropInputItem,
                    cid => cid.CropInputItem.CropInputCategory);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CropInputDistribution> GetByFarmer(short crop, string farmerCode)
        {
            return uow.CropInputDistributionRepository
                .Query(c => c.Crop == crop &&
                c.FarmerCode == farmerCode, null,
                c => c.CropInputItem,
                c => c.CropInputItem.CropInputCategory).ToList();
        }

        public List<CropInputDistribution> GetByArea(short crop, string areaCode)
        {
            return uow.CropInputDistributionRepository
                .Query(c => c.Crop == crop &&
                c.RegistrationFarmer.Farmer.Supplier1.SupplierArea == areaCode,
                null,
                c => c.CropInputItem,
                c => c.CropInputItem.CropInputCategory)
                .ToList();
        }

        public List<CropInputDistribution> GetByCitizenID(short crop, string citizenID)
        {
            return uow.CropInputDistributionRepository
                .Query(ci => ci.RegistrationFarmer.Farmer.CitizenID == citizenID &&
                ci.Crop == crop,
                null,
                ci => ci.CropInputItem,
                ci => ci.RegistrationFarmer,
                ci => ci.RegistrationFarmer.Farmer
                ).ToList();
        }

    }
}
