﻿using BusinessLayer.Helper;
using BusinessLayer.Helper.FarmerBonus;
using BusinessLayer.Model;
using BusinessLayer.Model.FarmerBonus;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IFullQuotaBonusBL
    {
        void TransferConfirm(short crop, string[] farmerCodeList);
        void PrintConfirm(short crop, string[] farmerCodeList);
        void CencelTransferred(short crop, string[] farmerCodeList);
        void CencelPrinted(short crop, string[] farmerCodeList);
        List<m_FullQuotaBonusDetails> GetByStatus(short crop, string status);
    }

    public class FullQuotaBonusBL : IFullQuotaBonusBL
    {
        IUnitOfWork uow;
        public FullQuotaBonusBL()
        {
            uow = new UnitOfWork();
        }

        public void CencelPrinted(short crop, string[] farmerCodeList)
        {
            try
            {
                if (farmerCodeList == null)
                    return;
                var whereArray = farmerCodeList.ToArray();
                var operationList = uow.RegistrationFarmerRepository
                    .Get(x => x.Crop == crop &&
                    whereArray.Contains(x.FarmerCode))
                    .ToList();

                operationList.ForEach(x => x.CreditGroupCode = "Transferred");
                uow.RegistrationFarmerRepository.UpdateRange(operationList);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CencelTransferred(short crop, string[] farmerCodeList)
        {
            try
            {
                if (farmerCodeList == null)
                    return;
                var whereArray = farmerCodeList.ToArray();
                var printedList = uow.RegistrationFarmerRepository
                    .Get(x => x.Crop == crop &&
                    whereArray.Contains(x.FarmerCode))
                    .ToList();

                printedList.ForEach(x => x.CreditGroupCode = "Pending");
                uow.RegistrationFarmerRepository.UpdateRange(printedList);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<m_FullQuotaBonusDetails> GetByStatus(short crop, string status)
        {
            return FullQuotaBonusHelper.GetFullQuotaBonusDetails(crop, status);
        }

        public void PrintConfirm(short crop, string[] farmerCodeList)
        {
            try
            {
                if (farmerCodeList == null)
                    return;
                var whereArray = farmerCodeList.ToArray();
                var printedList = uow.RegistrationFarmerRepository
                    .Get(x => x.Crop == crop &&
                    whereArray.Contains(x.FarmerCode))
                    .ToList();

                printedList.ForEach(x => x.CreditGroupCode = "Completed");
                uow.RegistrationFarmerRepository.UpdateRange(printedList);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void TransferConfirm(short crop, string[] farmerCodeList)
        {
            try
            {
                if (farmerCodeList == null)
                    return;
                var whereArray = farmerCodeList.ToArray();
                var transferList = uow.RegistrationFarmerRepository
                    .Get(x => x.Crop == crop &&
                    whereArray.Contains(x.FarmerCode))
                    .ToList();

                var transferredDate = DateTime.Now;
                var day = transferredDate.Day;
                var month = transferredDate.Month.ToString();
                var year = transferredDate.Year;
                var maxOrder = 0;
                var receiptOrderList = uow.RegistrationFarmerRepository
                    .Get(x => x.PreviousCropTotalVolume != null &&
                    x.Crop == crop)
                    .ToList();
                if (receiptOrderList.Count() <= 0)
                    maxOrder = 0;
                else
                    maxOrder = (int)(receiptOrderList
                        .Max(x => x.PreviousCropTotalVolume));

                //Generate runnumber for print receipt.
                foreach (var item in transferList
                    .OrderBy(x => x.FarmerCode))
                {
                    if (item.PreviousCropTotalVolume == null)
                    {
                        maxOrder = maxOrder + 1;
                        item.PreviousCropTotalVolume = maxOrder;
                    }
                }

                transferList.ForEach(x => x.CreditGroupCode = "Transferred");
                transferList.ForEach(x => x.TTMQuotaStation = year + "-" +
                month.PadLeft(2, '0') + "-" +
                day.ToString().PadLeft(2, '0'));
                uow.RegistrationFarmerRepository.UpdateRange(transferList);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
