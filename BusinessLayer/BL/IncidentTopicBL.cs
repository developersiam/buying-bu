﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IIncidentTopicBL
    {
        void Add(Guid categoryID, string topicTitle, string createUser);
        void Update(Guid id, string topicTitle, Guid categoryID, string modifiedUser);
        void Delete(Guid id);
        List<IncidentTopic> GetAll();
        List<IncidentTopic> GetByCategory(Guid categoryId);
        IncidentTopic GetSingle(Guid id);
    }

    public class IncidentTopicBL : IIncidentTopicBL
    {
        UnitOfWork uow;

        public IncidentTopicBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(Guid categoryID, string topicTitle, string createUser)
        {
            try
            {
                if (uow.IncidentTopicRepository
                    .Get(x => x.TopicTitle == topicTitle &&
                    x.CategoryID == categoryID)
                    .Count() > 0)
                    throw new ArgumentException("มี Topic นี้ซ้ำแล้วในระบบ");

                uow.IncidentTopicRepository.Add(new IncidentTopic
                {
                    ID = Guid.NewGuid(),
                    TopicTitle = topicTitle,
                    CategoryID = categoryID,
                    CreateBy = createUser,
                    CreateDate = DateTime.Now,
                    ModifiedBy = createUser,
                    ModifiedDate = DateTime.Now
                });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var model = uow.IncidentTopicRepository
                    .GetSingle(x => x.ID == id, x => x.IncidentDescriptions);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (model.IncidentDescriptions.Count() > 0)
                    throw new ArgumentException("มีข้อมูล Descriptions อยู่ภายใต้ Topic นี้จำนวน " +
                        model.IncidentDescriptions.Count() + " รายการ ไม่สามารถลบข้อมูลนี้ได้");

                uow.IncidentTopicRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<IncidentTopic> GetAll()
        {
            return uow.IncidentTopicRepository
                .Query(null, null, x => x.IncidentCategory)
                .ToList();
        }

        public List<IncidentTopic> GetByCategory(Guid categoryId)
        {
            return uow.IncidentTopicRepository.Get(x => x.CategoryID == categoryId);
        }

        public IncidentTopic GetSingle(Guid id)
        {
            return uow.IncidentTopicRepository.GetSingle(x => x.ID == id, x => x.IncidentCategory);
        }

        public void Update(Guid id, string topicTitle, Guid categoryID, string modifiedUser)
        {
            try
            {
                if (uow.IncidentTopicRepository
                    .Get(x => x.TopicTitle == topicTitle &&
                    x.CategoryID == categoryID).Count() > 0)
                    throw new ArgumentException("มี Topic นี้ซ้ำแล้วในระบบ");

                var model = uow.IncidentTopicRepository.GetSingle(x => x.ID == id);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                model.TopicTitle = topicTitle;
                model.CategoryID = categoryID;
                model.ModifiedBy = modifiedUser;
                model.ModifiedDate = DateTime.Now;

                uow.IncidentTopicRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
