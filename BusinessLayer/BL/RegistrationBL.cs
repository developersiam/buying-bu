﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IRegistrationBL
    {
        void Add(RegistrationFarmer model);
        void Delete(short crop, string farmerCode);
        void UpdatePreRegisterProfile(RegistrationFarmer model);
        void UpdateSignContract(short crop, string farmerCode, short quota, DateTime signContractDate, string recommender, string modifiedUser);
        void UpdateBookBank(short crop, string farmerCode, int bankID,
            string bookBank, string bankBranchCode, string BankBranch, string user);
        void UnRegister(string farmerCode, string unRegisterReason, string modifiedUser);
        void CancelUnRegister(string farmerCode, string modifiedUser);
        List<RegistrationFarmer> GetByFarmer(string farmerCode);
        RegistrationFarmer GetSingle(short crop, string farmerCode);
        List<RegistrationFarmer> GetBySupplier(short crop, string supplierCode);
        List<RegistrationFarmer> GetByArea(short crop, string areaCode);
        List<RegistrationFarmer> GetByCropAndCitizenID(short crop, string citizenID);
        List<RegistrationFarmer> GetByCitizenID(string citizenID);
        List<RegistrationFarmer> GetByGAPGroup(string gapGroupCode);
        List<RegistrationFarmer> GetByCrop(short crop);
        List<Person> GetMoreThanTwoSupplierByArea(short crop, string areaCode);
        bool CheckMoreThanTwoExtentionAgent(short crop, string citizenID);
        void RemoveGAPGroupMember(short crop, string farmerCode, string modifiedBy);
        void AddFarmerToGAPGroup(short crop, string farmerCode, string gapGroupCode, string modifiedBy);
        void UpdateEstimateBalanceKgAfterSold(short crop, string farmerCode, short estimateBalanceKg, string updateUser);
        bool NotSignContact(short crop, string farmercode);
        void EditSupplierQuota(short crop, string farmerCode, short quotaFromSupplierApprove, string user);
        void EditSTECQuota(short crop, string farmerCode, short quotaFromSTECApprove, string user);
        void ApproveSupplierQuota(short crop, string farmerCode, short quotaFromSupplierApprove, string user);
        void ApproveSTECQuota(short crop, string farmerCode, short quotaFromSTECApprove, string user);
        void CancelSupplierQuota(short crop, string farmerCode, string user);
        void CancelSTECQuota(short crop, string farmerCode, string user);
        void SignContract(short crop, string farmerCode, short contractQuota, DateTime signContractDate, string user);
        void SignContractV2023(string farmerCode, short contractQuota, DateTime signContractDate, string modifiedBy);
        void CancelSignContract(short crop, string farmerCode, string modifiedUser);
        bool IsValidMaxNumberOfContract(short crop, string citizenID);
    }

    public struct ExtensionAgentLimitQuota
    {
        public string SupplierCode { get; set; }
        public int FixedKg { get; set; }
    }

    public class RegistrationBL : IRegistrationBL
    {
        UnitOfWork uow;
        public RegistrationBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(RegistrationFarmer model)
        {
            try
            {
                var validation = new DomainValidation.IsRegistrationFarmerValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                /// ตรวจสอบช่วงเวลาที่อนุญาตในการกรอกข้อมูลการลงทะเบียน
                //Helper.SystemDeadlinesHelper.FarmerContractDeadlinesValid(model.Crop);

                var register = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == model.Crop && x.FarmerCode == model.FarmerCode);
                if (register != null)
                    throw new ArgumentException("มีข้อมูลการลงทะเบียนกรอกใบสมัครแล้ว จำนวน " + register.QuotaFromFarmerRequest + " ไร่");

                var person = uow.FarmerRepository.GetSingle(x => x.FarmerCode == model.FarmerCode, x => x.Person).Person;
                var datediff = DateTime.Now.Year - person.BirthDate.Year;
                if (datediff < 18)
                    throw new ArgumentException("ชาวไร่อายุไม่ถึง 18 ปี ไม่สามารถลงทะเบียนได้");

                //if (IsValidMaxNumberOfContract(model.Crop, person.CitizenID) == false)
                //    throw new ArgumentException("มีการสมัครเข้าร่วมโครงการครบตามจำนวนแล้ว (1 รายสมัครได้ 2 ตัวแทน)");

                //model.GAPContractCode = "CY" + model.Crop.ToString().Substring(2, 2) + "-" + model.FarmerCode;
                //GAPContractCode for CY2024 not include - (minus) character.
                model.GAPContractCode = "CY" + model.Crop.ToString().Substring(2, 2) + model.FarmerCode;
                model.ActiveStatus = true;
                model.GAPGroupCode = null;
                model.UnRegisterReason = null;
                model.QuotaFromSupplierApprove = model.Crop > 2023 ? model.QuotaFromSignContract : null;
                model.QuotaFromSTECApprove = model.Crop > 2023 ? model.QuotaFromSignContract : null;
                model.QuotaFromSignContract = model.Crop > 2023 ? model.QuotaFromSignContract : null;
                model.LastModified = DateTime.Now;

                uow.RegistrationFarmerRepository.Add(model);
                uow.Save();


                /// ตรวจสอบ Over quota limit.
                /// CY2022 รับไม่อั้น
                /// 
                //if (register != null && register.Farmer.Supplier != "STEC/PB")
                //{
                //    List<ExtensionAgentLimitQuota> list = new List<ExtensionAgentLimitQuota>();

                //    list.Add(new ExtensionAgentLimitQuota { SupplierCode = "ABC/S", FixedKg = 3000000 });
                //    list.Add(new ExtensionAgentLimitQuota { SupplierCode = "Adul/S", FixedKg = 1250000 });
                //    list.Add(new ExtensionAgentLimitQuota { SupplierCode = "Kiat", FixedKg = 1250000 });
                //    list.Add(new ExtensionAgentLimitQuota { SupplierCode = "Adul/P", FixedKg = 24000 });

                //    if (list.Where(x => x.SupplierCode == register.Farmer.Supplier).Count() <= 0)
                //        throw new ArgumentException("ยังไม่มีการกำหนดลิมิตโควต้าให้กับ " + register.Farmer.Supplier);

                //    var totalSignContractRai = GetBySupplier(model.Crop, register.Farmer.Supplier)
                //        .Sum(x => x.QuotaFromSignContract);

                //    var limitQuota = list.Single(x => x.SupplierCode == model.Farmer.Supplier).FixedKg;

                //    if ((totalSignContractRai + model.QuotaFromSignContract) > (limitQuota / 400))
                //        throw new ArgumentException("มีการลงทะเบียนกับ " +
                //            register.Farmer.Supplier +
                //            " ไปแล้วทั้งสิ้น " +
                //            Convert.ToInt32(totalSignContractRai).ToString("N0") +
                //            " ไร่ (" +
                //            Convert.ToInt32(totalSignContractRai * 400).ToString("N0") +
                //            " กก.) เมื่อรวมกับจำนวนโควต้าของชาวไร่รายนี้อีก " +
                //            Convert.ToInt32(model.QuotaFromSignContract).ToString("N0") +
                //            " ไร่ (" + Convert.ToInt32(model.QuotaFromSignContract * 400).ToString("N0") +
                //            " กก.) จะเกินโควต้าที่กำหนดไว้คือ " +
                //            (list.Single(x => x.SupplierCode == register.Farmer.Supplier).FixedKg / 400) +
                //            " ไร่ (" +
                //            (list.Single(x => x.SupplierCode == register.Farmer.Supplier).FixedKg).ToString("N0") +
                //            " กก.)");
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public void Update(RegistrationFarmer model)
        //{
        //    try
        //    {
        //       if (model == null)
        //            throw new ArgumentException("ไม่พบการกำหนดค่าให้กับออปเจ๊ค");

        //        if (model.Crop.ToString() == null)
        //            throw new ArgumentException("ไม่พบข้อมูล Crop");

        //        if (model.FarmerCode == null)
        //            throw new ArgumentException("ไม่พบข้อมูล FarmerCode");

        //        if (model.ModifiedByUser == null)
        //            throw new ArgumentException("ไม่พบข้อมูล ModifiedByUser");

        //        if (GetSingle(model.Crop, model.FarmerCode) == null)
        //            throw new ArgumentException("ไม่พบข้อมูลในระบบ!");

        //        var _update = uow.RegistrationFarmerRepository
        //           .GetSingle(x => x.Crop == model.Crop && x.FarmerCode == model.FarmerCode);

        //        _update.PreviousCropTotalRai = model.PreviousCropTotalRai;
        //        _update.PreviousCropTotalVolume = model.PreviousCropTotalVolume;
        //        _update.TTMQuotaStatus = model.TTMQuotaStatus;
        //        _update.TTMQuotaKg = model.TTMQuotaKg;
        //        _update.TTMQuotaStation = model.TTMQuotaStation;
        //        _update.BankID = model.BankID;
        //        _update.BookBank = model.BookBank;
        //        _update.BankBranchCode = model.BankBranchCode;
        //        _update.BankBranch = model.BankBranch;
        //        _update.QuotaFromFarmerRequest = model.QuotaFromFarmerRequest;
        //        _update.LastModified = DateTime.Now;

        //        uow.RegistrationFarmerRepository.Update(_update);
        //        uow.Save();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public void Delete(short crop, string farmerCode)
        {
            try
            {
                var registration = GetSingle(crop, farmerCode);
                if (registration == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ!");

                uow.RegistrationFarmerRepository.Remove(registration);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RegistrationFarmer> GetByFarmer(string farmerCode)
        {
            try
            {
                return uow.RegistrationFarmerRepository
                    .Query(reg => reg.FarmerCode == farmerCode
                    , null
                    , r => r.Farmer.Person)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RegistrationFarmer> GetByCropAndCitizenID(short crop, string citizenID)
        {
            return uow.RegistrationFarmerRepository
                .Query(reg => reg.Crop == crop &&
                reg.Farmer.CitizenID == citizenID,
                null,
                reg => reg.Farmer,
                reg => reg.Farmer.Supplier1,
                reg => reg.Farmer.Person)
                .ToList();
        }

        public RegistrationFarmer GetSingle(short crop, string farmerCode)
        {
            try
            {
                return uow.RegistrationFarmerRepository
                    .GetSingle(reg => reg.Crop == crop &&
                    reg.FarmerCode == farmerCode,
                    r => r.Farmer,
                    r => r.Farmer.Person,
                    r => r.Farmer.Supplier1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RegistrationFarmer> GetBySupplier(short crop, string supplierCode)
        {
            try
            {
                return uow.RegistrationFarmerRepository
                    .Query(reg => reg.Crop == crop &&
                    reg.Farmer.Supplier == supplierCode,
                    null,
                    reg => reg.Farmer,
                    reg => reg.Farmer.Person).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        struct DupplicatedFarmerContract
        {
            public string CitizenID { get; set; }
            public string FarmerCode { get; set; }
        }

        public List<Person> GetMoreThanTwoSupplierByArea(short crop, string areaCode)
        {
            try
            {
                var listOfFarmerRegistration = GetByArea(crop, areaCode);

                List<DupplicatedFarmerContract> list = new List<DupplicatedFarmerContract>();
                foreach (var item in listOfFarmerRegistration)
                {
                    list.Add(new DupplicatedFarmerContract
                    {
                        CitizenID = item.Farmer.CitizenID,
                        FarmerCode = item.FarmerCode
                    });
                }

                var countOfFarmerRegistrationList = list.GroupBy(info => info.CitizenID)
                    .Select(group => new
                    {
                        CitizenID = group.Key,
                        Count = group.Count()
                    });

                List<Person> result = new List<Person>();
                foreach (var item in countOfFarmerRegistrationList.Where(x => x.Count > 2))
                {
                    result.Add(BuyingFacade.PersonBL().GetSingle(item.CitizenID));
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RegistrationFarmer> GetByArea(short crop, string areaCode)
        {
            try
            {
                return uow.RegistrationFarmerRepository
                    .Query(reg => reg.Crop == crop &&
                    reg.Farmer.Supplier1.SupplierArea == areaCode,
                    null,
                    reg => reg.Farmer,
                    reg => reg.Farmer.Person)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RegistrationFarmer> GetByGAPGroup(string gapGroupCode)
        {
            return uow.RegistrationFarmerRepository
                .Query(rf => rf.GAPGroupCode == gapGroupCode,
                null,
                rf => rf.Farmer.Person)
                .ToList();
        }

        public bool CheckMoreThanTwoExtentionAgent(short crop, string citizenID)
        {
            try
            {
                var listOfFarmerCode = BuyingFacade.FarmerBL().GetByCitizenID(citizenID);
                int count = 0;
                foreach (var item in listOfFarmerCode)
                {
                    if (GetSingle(crop, item.FarmerCode) != null)
                        count = count + 1;
                }
                // ถ้า count มากกว่า 2 นั่นแสดงว่าชาวไร่รายดังกล่าวมีการลงทะเบียนไว้มากกว่า 2 ตัวแทน
                if (count > 2)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveGAPGroupMember(short crop, string farmerCode, string modifiedBy)
        {
            try
            {
                if (modifiedBy == "")
                    throw new ArgumentException("ไม่พบข้อมูล modified By");

                var model = GetSingle(crop, farmerCode);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลชาวไร่รหัส " + farmerCode + " ลงทะเบียนไว้ในระบบในปี " + crop);

                model.GAPGroupCode = null;
                model.LastModified = DateTime.Now;
                model.ModifiedByUser = modifiedBy;

                uow.RegistrationFarmerRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddFarmerToGAPGroup(short crop, string farmerCode, string gapGroupCode, string modifiedBy)
        {
            try
            {
                if (gapGroupCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล GAP Group Code");

                if (modifiedBy == "")
                    throw new ArgumentException("ไม่พบข้อมูล modified By");

                var model = GetSingle(crop, farmerCode);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลชาวไร่รหัส " + farmerCode + " ลงทะเบียนไว้ในระบบในปี " + crop);

                model.GAPGroupCode = gapGroupCode;
                model.LastModified = DateTime.Now;
                model.ModifiedByUser = modifiedBy;

                uow.RegistrationFarmerRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateEstimateBalanceKgAfterSold(short crop, string farmerCode, short estimateBalanceKg, string updateUser)
        {
            try
            {
                if (estimateBalanceKg.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล estimate Balance Kg");

                if (updateUser == "")
                    throw new ArgumentException("ไม่พบข้อมูล updateUser");

                if (estimateBalanceKg <= 0)
                    throw new ArgumentException("estimate Balance Kg จะต้องมากกว่า 0 ขึ้นไป");

                var model = GetSingle(crop, farmerCode);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ!");

                model.EstimateTobaccoAmount = estimateBalanceKg;
                model.LastModified = DateTime.Now;
                model.ModifiedByUser = updateUser;

                uow.RegistrationFarmerRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool NotSignContact(short crop, string farmercode)
        {
            var result = false;
            var regist = GetSingle(crop, farmercode);
            if (regist.QuotaFromSignContract != null)
            {
                result = true;
                return result;
            }
            return result;
        }

        public void SignContract(short crop, string farmerCode, short contractQuota, DateTime signContractDate, string user)
        {
            try
            {
                Helper.SystemDeadlinesHelper.BeforeStartBuyingDeadLine(crop, farmerCode);

                var model = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == crop &&
                    x.FarmerCode == farmerCode,
                    x => x.Farmer);

                if (model == null)
                    throw new ArgumentException("ชาวไร่รายนี้ยังไม่ผ่านขั้นตอนการกรอกข้อมูลสมัครเข้าร่วมโครงการ");

                /// ต้องผ่านการ approve ข้อมูลจาก STEC ก่อน
                if (model.ApproveFromSTECStatus == false)
                    throw new ArgumentException("ข้อมูลของชาวไร่รหัส " + farmerCode +
                        " ยังไม่ผ่านขั้นตอนการ approve โควต้าโดย Supervisor");

                /// ในปี 2021 จะไม่อนุญาตให้ทาง STEC/SK บันทึกสัญญาได้ (ต้องไปแบ่งทำสัญญากับตัวแทนรายอื่นๆ แทน)
                if (model.Farmer.Supplier == "STEC/SK")
                    throw new ArgumentException("ชาวไร่ที่ลงทะเบียนไว้กับ " +
                        model.Farmer.Supplier +
                        " จะต้องไปเลือกลงทะเบียนกับตัวแทนอื่นเท่านั้น ตามเงื่อนไขที่ทางบริษัทกำหนด");

                if (contractQuota <= 0)
                    throw new ArgumentException("โควต้าที่จะทำสัญญาจะต้องมากกว่า 0");

                if (signContractDate == null)
                    throw new ArgumentException("โปรดระบุวันที่ทำสัญญา");

                /// ตรวจสอบ Over quota limit.
                ///
                //if (model != null && model.Farmer.Supplier != "STEC/PB")
                //{
                //    List<ExtensionAgentLimitQuota> list = new List<ExtensionAgentLimitQuota>();

                //    list.Add(new ExtensionAgentLimitQuota { SupplierCode = "ABC/S", FixedKg = 3000000 });
                //    list.Add(new ExtensionAgentLimitQuota { SupplierCode = "Adul/S", FixedKg = 1250000 });
                //    list.Add(new ExtensionAgentLimitQuota { SupplierCode = "Kiat", FixedKg = 1250000 });
                //    list.Add(new ExtensionAgentLimitQuota { SupplierCode = "Adul/P", FixedKg = 24000 });

                //    if (list.Where(x => x.SupplierCode == model.Farmer.Supplier).Count() <= 0)
                //        throw new ArgumentException("ยังไม่มีการกำหนดลิมิตโควต้าให้กับ " + model.Farmer.Supplier);

                //    var totalSignContractRai = GetBySupplier(model.Crop, model.Farmer.Supplier)
                //        .Sum(x => x.QuotaFromSignContract);

                //    var limitQuota = list.Single(x => x.SupplierCode == model.Farmer.Supplier).FixedKg;

                //    var debug = totalSignContractRai + contractQuota;
                //    var debug2 = totalSignContractRai + model.QuotaFromSignContract;

                //    if ((totalSignContractRai + contractQuota) > (limitQuota / 400))
                //        throw new ArgumentException("มีการลงทะเบียนกับ " +
                //            model.Farmer.Supplier +
                //            " ไปแล้วทั้งสิ้น " +
                //            Convert.ToInt32(totalSignContractRai).ToString("N0") +
                //            " ไร่ (" +
                //            Convert.ToInt32(totalSignContractRai * 400).ToString("N0") +
                //            " กก.) เมื่อรวมกับจำนวนโควต้าของชาวไร่รายนี้อีก " +
                //            Convert.ToInt32(model.QuotaFromSignContract).ToString("N0") +
                //            " ไร่ (" + Convert.ToInt32(model.QuotaFromSignContract * 400).ToString("N0") +
                //            " กก.) จะเกินโควต้าที่กำหนดไว้คือ " +
                //            (list.Single(x => x.SupplierCode == model.Farmer.Supplier).FixedKg / 400) +
                //            " ไร่ (" +
                //            (list.Single(x => x.SupplierCode == model.Farmer.Supplier).FixedKg).ToString("N0") +
                //            " กก.)");
                //}

                model.GAPContractCode = "CY" + crop.ToString().Substring(2, 2) + "-" + farmerCode;
                model.QuotaFromSignContract = contractQuota;
                model.SignContractDate = signContractDate;
                model.ActiveStatus = true;
                model.ModifiedByUser = user;
                model.LastModified = DateTime.Now;

                uow.RegistrationFarmerRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CancelSignContract(short crop, string farmerCode, string modifiedUser)
        {
            try
            {
                Helper.SystemDeadlinesHelper.BeforeStartBuyingDeadLine(crop, farmerCode);

                var model = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == crop && x.FarmerCode == farmerCode);

                if (model == null)
                    throw new ArgumentException("ชาวไร่รายนี้ยังไม่ผ่านขั้นตอนการกรอกข้อมูลสมัครเข้าร่วมโครงการ");

                /// หากมีการแบ่งโปรเจคไปแล้วใน FarmerProject จะไม่สามารถยกเลิกสัญญาได้
                /// 
                var projects = uow.FarmerProjectRepository
                    .Query(x => x.Crop == crop && x.FarmerCode == farmerCode)
                    .ToList();

                /// หากมีการขายไปแล้วจะไม่สามารถยกเลิกสัญญาได้
                /// 
                var buyings = uow.BuyingRepository
                    .Query(x => x.Crop == crop && x.FarmerCode == farmerCode)
                    .ToList();

                if (buyings.Sum(x => x.Weight) > 0)
                    throw new ArgumentException("ไม่สามารถยกเลิกสัญญได้ เนื่องจากมีการขายไปแล้วจำนวน " +
                        buyings.Sum(x => x.Weight) + " กก.");

                if (projects.Count() > 0)
                    throw new ArgumentException("ไม่สามารถยกเลิกสัญญาได้ เนื่องจากมีการแบ่งโปรเจคไปแล้วจำนวน " +
                        projects.Count() + " โปรเจค");

                //model.QuotaFromSignContract = null;
                //model.SignContractDate = null;
                model.ActiveStatus = false;
                model.GAPContractCode = null;
                model.ModifiedByUser = modifiedUser;
                model.LastModified = DateTime.Now;

                uow.RegistrationFarmerRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EditSTECQuota(short crop, string farmerCode, short quotaFromSTECApprove, string user)
        {
            try
            {
                Helper.SystemDeadlinesHelper.FarmerContractDeadlinesValid(crop);

                var model = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == crop && x.FarmerCode == farmerCode);

                if (model == null)
                    throw new ArgumentException("ชาวไร่รายนี้ยังไม่ผ่านขั้นตอนการกรอกข้อมูลสมัครเข้าร่วมโครงการ");

                if (model.ApproveFromSupplierStatus == false)
                    throw new ArgumentException("ข้อมูลการลงทะเบียนของชาวไร่รายนี้ ยังไม่ผ่านการอนุมัติจำนวนไร่จากตัวแทน");

                if (model.ApproveFromSTECStatus == true)
                    throw new ArgumentException("ข้อมูลการลงทะเบียนของชาวไร่รายนี้ ได้รับการอนุมัติจาก STEC แล้ว กรุณายกเลิกการอนุมัติก่อน");

                model.ApproveFromSTECStatus = true;
                model.QuotaFromSTECApprove = quotaFromSTECApprove;
                model.LastModified = DateTime.Now;
                model.ModifiedByUser = user;

                uow.RegistrationFarmerRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EditSupplierQuota(short crop, string farmerCode, short quotaFromSupplierApprove, string user)
        {
            try
            {
                Helper.SystemDeadlinesHelper.FarmerContractDeadlinesValid(crop);

                var model = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == crop && x.FarmerCode == farmerCode);

                if (model == null)
                    throw new ArgumentException("ชาวไร่รายนี้ยังไม่ผ่านขั้นตอนการกรอกข้อมูลสมัครเข้าร่วมโครงการ");

                if (model.ApproveFromSTECStatus == true)
                    throw new ArgumentException("ข้อมูลการลงทะเบียนของชาวไร่รายนี้ ได้รับการอนุมัติโดย STEC แล้ว ไม่สามารถแก้ไขข้อมูลการลงทะเบียนได้");

                if (model.ApproveFromSupplierStatus == true)
                    throw new ArgumentException("ข้อมูลการลงทะเบียนของชาวไร่รายนี้ ได้รับการอนุมัติจากตัวแทนแล้ว กรุณายกเลิกการอนุมัติก่อน");

                model.ApproveFromSupplierStatus = true;
                model.QuotaFromSupplierApprove = quotaFromSupplierApprove;
                model.LastModified = DateTime.Now;
                model.ModifiedByUser = user;

                uow.RegistrationFarmerRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ApproveSupplierQuota(short crop, string farmerCode, short quotaFromSupplierApprove, string user)
        {
            try
            {
                Helper.SystemDeadlinesHelper.FarmerContractDeadlinesValid(crop);

                var model = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == crop && x.FarmerCode == farmerCode);

                if (model == null)
                    throw new ArgumentException("ชาวไร่รายนี้ยังไม่ผ่านขั้นตอนการกรอกข้อมูลสมัครเข้าร่วมโครงการ");

                if (model.ApproveFromSTECStatus == true)
                    throw new ArgumentException("ข้อมูลการลงทะเบียนของชาวไร่ " + farmerCode
                        + " ได้รับการอนุมัติโดย STEC แล้ว ไม่สามารถดำเนินการในขั้นตอนนี้ได้");

                if (model.ApproveFromSupplierStatus == true)
                    throw new ArgumentException("ข้อมูลการลงทะเบียนของชาวไร่ " + farmerCode
                        + " ได้รับการอนุมัติจากตัวแทนอยู่ก่อนหน้านี้แล้ว");

                model.ApproveFromSupplierStatus = true;
                model.QuotaFromSupplierApprove = quotaFromSupplierApprove;
                model.LastModified = DateTime.Now;
                model.ModifiedByUser = user;

                uow.RegistrationFarmerRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ApproveSTECQuota(short crop, string farmerCode, short quotaFromSTECApprove, string user)
        {
            try
            {
                Helper.SystemDeadlinesHelper.FarmerContractDeadlinesValid(crop);

                var model = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == crop &&
                    x.FarmerCode == farmerCode,
                    x => x.Farmer);

                if (model == null)
                    throw new ArgumentException("ชาวไร่รายนี้ยังไม่ผ่านขั้นตอนการกรอกข้อมูลสมัครเข้าร่วมโครงการ");

                if (model.ApproveFromSTECStatus == true)
                    throw new ArgumentException("ข้อมูลการลงทะเบียนของชาวไร่ " + farmerCode
                        + " ได้รับการอนุมัติโดย STEC อยู่ก่อนหน้านี้แล้ว");

                if (model.ApproveFromSupplierStatus == false)
                    throw new ArgumentException("ข้อมูลการลงทะเบียนของชาวไร่ " + farmerCode
                        + " ยังไม่ได้รับการอนุมัติจากตัวแทน");

                var registrationList = BuyingFacade.RegistrationBL()
                        .GetByCropAndCitizenID(model.Crop,
                        model.Farmer.CitizenID)
                        .Where(x => x.ApproveFromSTECStatus == true)
                        .ToList();

                if (registrationList.Count() >= 2)
                {
                    var extensionAgents = "\"";
                    foreach (var item in registrationList)
                        extensionAgents = extensionAgents + item.Farmer.Supplier + "\", ";

                    throw new ArgumentException("ชาวไร่รายนี้ได้รับการอนุมัติโควต้าไปแล้วกับตัวแทนมากกว่า 2 ตัวแทน ได้แก่ " +
                         extensionAgents + " ไม่สามารถอนุมัติโควต้าให้ได้ โดยสามารถตรวจสอบรายละเอียได้จากเมนู \"ชาวไร่ที่ลงทะเบียนมากกว่า 1 ตัวแทน\"");
                }

                model.ApproveFromSTECStatus = true;
                model.QuotaFromSTECApprove = quotaFromSTECApprove;
                model.LastModified = DateTime.Now;
                model.ModifiedByUser = user;

                uow.RegistrationFarmerRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CancelSupplierQuota(short crop, string farmerCode, string user)
        {
            try
            {
                Helper.SystemDeadlinesHelper.FarmerContractDeadlinesValid(crop);

                var model = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == crop && x.FarmerCode == farmerCode);

                if (model == null)
                    throw new ArgumentException("ชาวไร่รายนี้ยังไม่ผ่านขั้นตอนการกรอกข้อมูลสมัครเข้าร่วมโครงการ");

                if (model.ApproveFromSTECStatus == true)
                    throw new ArgumentException("ข้อมูลการลงทะเบียนของชาวไร่ " + farmerCode
                        + " ได้รับการอนุมัติโดย STEC แล้ว ไม่สามารถดำเนินการในขั้นตอนนี้ได้");

                if (model.ApproveFromSupplierStatus == false)
                    throw new ArgumentException("ข้อมูลการลงทะเบียนของชาวไร่ " + farmerCode
                        + " ได้ถูกยกเลิกไปก่อนหน้านี้แล้ว");

                model.ApproveFromSupplierStatus = false;
                model.QuotaFromSupplierApprove = 0;
                model.LastModified = DateTime.Now;
                model.ModifiedByUser = user;

                uow.RegistrationFarmerRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CancelSTECQuota(short crop, string farmerCode, string user)
        {
            try
            {
                Helper.SystemDeadlinesHelper.FarmerContractDeadlinesValid(crop);

                var model = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == crop && x.FarmerCode == farmerCode);

                if (model == null)
                    throw new ArgumentException("ชาวไร่รายนี้ยังไม่ผ่านขั้นตอนการกรอกข้อมูลสมัครเข้าร่วมโครงการ");

                if (model.SignContractDate != null)
                    throw new ArgumentException("มีการบันทึกข้อมูลสัญญาแล้ว ไม่สามารถยกเลิก STEC โควต้าได้");

                /// หากมีการหักหนี้ค่าเมล็ดพันธุ์จากการขายไปแล้ว ไม่สามารถยกเลิกสัญญาได้
                /// 
                BuyingFacade.DebtorPaymentBL().IsPayment(crop, farmerCode);

                /// หากมีการแบ่งโปรเจคแล้วจะไม่ให้ยกเลิกสัญญา
                /// 
                var project = uow.FarmerProjectRepository
                    .Query(x => x.Crop == crop && x.FarmerCode == farmerCode);

                if (project.Count() > 0)
                    throw new ArgumentException("มีการแบ่งโปรเจคไปแล้ว ไม่สามารถยกเลิกสัญญาได้ โปรดแจ้งผู้จัดการเพื่อดำเนินการต่อไป");

                model.ApproveFromSTECStatus = false;
                model.QuotaFromSTECApprove = 0;
                model.LastModified = DateTime.Now;
                model.ModifiedByUser = user;

                uow.RegistrationFarmerRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdatePreRegisterProfile(RegistrationFarmer model)
        {
            try
            {
                var validation = new DomainValidation.IsRegistrationFarmerValid().Validate(model);
                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var _update = uow.RegistrationFarmerRepository
                   .GetSingle(x => x.Crop == model.Crop && x.FarmerCode == model.FarmerCode);
                if (_update == null)
                    throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนในระบบ!");

                /// ตรวจสอบช่วงเวลาที่อนุญาตในการกรอกข้อมูลการลงทะเบียน
                //Helper.SystemDeadlinesHelper.FarmerContractDeadlinesValid(model.Crop);

                if (model.PreviousCropTotalRai < 0)
                    throw new ArgumentException("PreviousCropTotalRai จะต้องไม่ติดลบ");

                if (model.PreviousCropTotalVolume < 0)
                    throw new ArgumentException("PreviousCropTotalVolume จะต้องไม่ติดลบ");

                if (model.PreviousCropTotalVolume < model.PreviousCropTotalRai)
                    throw new ArgumentException("PreviousCropTotalVolume จะต้องไม่น้อยกว่า PreviousCropTotalRai");

                if (model.TTMQuotaStatus == true &&
                    model.TTMQuotaKg == null)
                    throw new ArgumentException("ถ้าระบุว่ามีโควต้ากับทาง TTM (TTMQuotaStatus = true) จะต้องระบุจำนวนโควต้า TTM ด้วยทุกครั้ง");

                if (model.TTMQuotaStatus == true &&
                    model.TTMQuotaKg <= 0)
                    throw new ArgumentException("ถ้าระบุว่ามีโควต้ากับทาง TTM (TTMQuotaStatus = true) จำนวนโควต้า TTM จะต้องมากกว่า 0");

                if (model.TTMQuotaStatus == true &&
                    model.TTMQuotaStation == null)
                    throw new ArgumentException("ถ้าระบุว่ามีโควต้ากับทาง TTM (TTMQuotaStatus = true) จะต้องระบุสถานีของ TTM ด้วยทุกครั้ง");

                if (model.ApproveFromSTECStatus == true &&
                    model.QuotaFromFarmerRequest != _update.QuotaFromFarmerRequest)
                    throw new ArgumentException("เมื่อทาง STEC อนุมัติจำนวนไร่ที่จะทำสัญญาแล้ว จะไม่สามารถแก้ไขจำนวนไร่ที่ชาวไร่ร้องขอได้อีก ต้องติดต่อ supervisor เพื่อปลดล็อค");


                var buyingTransaction = uow.BuyingRepository
                    .Get(x => x.Crop == model.Crop && x.FarmerCode == model.FarmerCode);
                if (buyingTransaction.Sum(x => x.Weight) > 0)
                    throw new ArgumentException("ชาวไร่รหัส " + model.FarmerCode +
                        " ได้มีการขายใบยาไปแล้วจำนวน " + Convert.ToDecimal(buyingTransaction.Sum(x => x.Weight)).ToString("N2") +
                        " กก." + " ไม่สามารถแก้ไขจำนวนโควต้าได้");

                _update.PreviousCropTotalRai = model.PreviousCropTotalRai;
                _update.PreviousCropTotalVolume = model.PreviousCropTotalVolume;
                _update.TTMQuotaStatus = model.TTMQuotaStatus;
                _update.TTMQuotaKg = model.TTMQuotaKg;
                _update.TTMQuotaStation = model.TTMQuotaStation;
                _update.BankID = model.BankID;
                _update.BookBank = model.BookBank;
                _update.BankBranchCode = model.BankBranchCode;
                _update.BankBranch = model.BankBranch;
                _update.QuotaFromFarmerRequest = model.QuotaFromFarmerRequest;
                _update.RegistrationDate = model.RegistrationDate;
                _update.LastModified = DateTime.Now;

                uow.RegistrationFarmerRepository.Update(_update);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateBookBank(short crop, string farmerCode, int bankID,
            string bookBank, string bankBranchCode, string bankBranch, string user)
        {
            try
            {
                ///หากมีการสร้างใบเวาเชอร์เพื่อจ่ายป้ายบาร์โค้ตให้กับชาวไร่แล้วจะไม่สามารถแก้ไขข้อมูลนี้ได้
                //Helper.SystemDeadlinesHelper.BeforeStartBuyingDeadLine(crop, farmerCode);

                var model = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == crop && x.FarmerCode == farmerCode);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนในระบบ!");

                if (bookBank == null)
                    throw new ArgumentException("ไม่ได้ระบุเลขที่บัญชี");

                if (bankBranchCode == null)
                    throw new ArgumentException("ไม่ได้ระบุรหัสสาขา");

                if (bankBranch == null)
                    throw new ArgumentException("ไม่ได้ระบุชื่อสาขา");

                if (user == null)
                    throw new ArgumentException("ไม่ได้ระบุข้อมูลผู้บันทึก");

                model.BankID = bankID;
                model.BookBank = bookBank;
                model.BankBranchCode = bankBranchCode;
                model.BankBranch = bankBranch;
                model.ModifiedByUser = user;
                model.LastModified = DateTime.Now;

                uow.RegistrationFarmerRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RegistrationFarmer> GetByCrop(short crop)
        {
            return uow.RegistrationFarmerRepository
                .Get(x => x.Crop == crop
                , null
                , x => x.Farmer
                , x => x.Farmer.Supplier1)
                .ToList();
        }

        public bool IsValidMaxNumberOfContract(short crop, string citizenID)
        {
            try
            {
                var contract = uow.RegistrationFarmerRepository
                    .Get(x => x.Crop == crop && x.Farmer.CitizenID == citizenID,
                    null,
                    x => x.Farmer)
                    .ToList();

                if (contract.Count() >= 2)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<RegistrationFarmer> GetByCitizenID(string citizenID)
        {
            return uow.RegistrationFarmerRepository
                .Query(reg => reg.Farmer.CitizenID == citizenID,
                null,
                reg => reg.Farmer,
                reg => reg.Farmer.Supplier1,
                reg => reg.Farmer.Person)
                .ToList();
        }

        public void SignContractV2023(string farmerCode, short contractQuota, DateTime signContractDate, string modifiedBy)
        {
            try
            {
                var defaultCrop = BuyingFacade.CropBL().GetDefault();
                if (defaultCrop == null)
                    throw new ArgumentException("ไม่พบ default crop ในระบบ โปรดติดต่อผู้ดูแลระบบ");

                Helper.SystemDeadlinesHelper.BeforeStartBuyingDeadLine(defaultCrop.Crop1, farmerCode);

                var farmer = BuyingFacade.FarmerBL().GetByFarmerCode(farmerCode);
                if (farmer == null)
                    throw new ArgumentException("ไม่พบข้อมูลชาวไร่รหัส farmer code " + farmerCode + " ในระบบ");

                if (contractQuota <= 0)
                    throw new ArgumentException("โควต้าที่จะทำสัญญาจะต้องมากกว่า 0");

                if (signContractDate == null)
                    throw new ArgumentException("โปรดระบุวันที่ทำสัญญา");

                uow.RegistrationFarmerRepository
                    .Add(new RegistrationFarmer
                    {
                        Crop = defaultCrop.Crop1,
                        FarmerCode = farmerCode,
                        GAPContractCode = "CY" + defaultCrop.Crop1.ToString().Substring(2, 2) + "-" + farmerCode,
                        QuotaFromFarmerRequest = contractQuota,
                        QuotaFromSupplierApprove = contractQuota,
                        QuotaFromSTECApprove = contractQuota,
                        QuotaFromSignContract = contractQuota,
                        ApproveFromSupplierStatus = true,
                        ApproveFromSTECStatus = true,
                        SignContractDate = signContractDate,
                        RegistrationDate = signContractDate,
                        ActiveStatus = true,
                        ModifiedByUser = modifiedBy,
                        LastModified = DateTime.Now
                    }); ;
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateSignContract(short crop, string farmerCode, short quota, DateTime signContractDate, string recommender, string modifiedUser)
        {
            try
            {
                if (quota <= 0)
                    throw new ArgumentException("โควต้าที่จะทำสัญญาจะต้องมากกว่า 0");

                if (signContractDate == null)
                    throw new ArgumentException("โปรดระบุวันที่ทำสัญญา");

                var registration = GetSingle(crop, farmerCode);
                if (registration == null)
                    throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รายนี้ในระบบ");

                Helper.SystemDeadlinesHelper.BeforeStartBuyingDeadLine(crop, farmerCode);

                var farmer = BuyingFacade.FarmerBL().GetByFarmerCode(farmerCode);
                if (farmer == null)
                    throw new ArgumentException("ไม่พบข้อมูลชาวไร่รหัส farmer code " + farmerCode + " ในระบบ");

                registration.QuotaFromSupplierApprove = (short)quota;
                registration.QuotaFromSTECApprove = (short)quota;
                registration.QuotaFromFarmerRequest = (short)quota;
                registration.QuotaFromSignContract = (short)quota;
                registration.ApproveFromSTECStatus = true;
                registration.ApproveFromSupplierStatus = true;
                registration.SignContractDate = signContractDate;
                registration.LastModified = DateTime.Now;
                registration.ModifiedByUser = modifiedUser;

                //CY24 ใช้ช่องนี้สำหรับเก็บข้อมูลผู้แนะนำให้ชาวไร่มาปลูกยาสูบกับทางบริษัทให้ไร่ละ 300 บาท
                registration.TTMQuotaStation = recommender;

                uow.RegistrationFarmerRepository.Update(registration);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UnRegister(string farmerCode, string unRegisterReason, string modifiedUser)
        {
            try
            {
                var cropDefault = BuyingFacade.CropBL().GetDefault();
                if (cropDefault == null)
                    throw new ArgumentException("ไม่พบ crop default โปรดติดต่อผู้ดูแลระบบ");

                var model = GetSingle(cropDefault.Crop1, farmerCode);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนชาวไร่รหัส " + farmerCode + " นี้ในระบบ");

                //var amountOfInvoice = BuyingFacade.InvoiceDetailsBL()
                //    .GetByFarmer(cropDefault.Crop1, farmerCode)
                //    .Sum(x => x.Quantity * x.UnitPrice);

                //var amountOfCreditNote = BuyingFacade.AccountCreditNoteDetailBL()
                //    .GetByFarmerCode(cropDefault.Crop1, farmerCode)
                //    .Sum(x=>x.Quantity*x.UnitPrice);

                //var amountOfBuyingDeduction = BuyingFacade.PaymentHistoryBL()
                //    .GetByFarmer(cropDefault.Crop1, farmerCode)
                //    .Sum(x=>x.PaymentAmount);

                //if (amountOfInvoice - amountOfCreditNote - amountOfBuyingDeduction > 0)
                //    throw new ArgumentException("ชาวไร่รายนี้มียอดหนี้คงค้างในระบบ ไม่สามารถยกเลิกสัญญาได้ กรุณาตรวจสอบยอดหนี้อีกครั้ง");


                ///*************************************************************
                ///เนื่องจากในรายงานต้องการยอดข้อมูลการลงทะเบียนทำสัญญาสะท้อนคววามเป็นจริง
                ///แต่ด้วยเงื่อนไขนี้ ชาวไร่เลยไม่สามารถยกเลิกสัญญาได้ จึงทำการปลดล็อคเงื่อนไขนี้ออกไปก่อน
                ///Lase Update : 2024-09-13
                //////*************************************************************
                //var cropDebt = uow.DebtSetupRepository
                //    .Get(x => x.DeductionCrop == cropDefault.Crop1 &&
                //    x.FarmerCode == farmerCode)
                //    .Sum(x => x.Amount);
                //var cropReceipt = uow.DebtReceiptRepository
                //    .Get(x => x.Crop == cropDefault.Crop1 &&
                //    x.FarmerCode == farmerCode)
                //    .Sum(x => x.Amount);

                //if (cropReceipt < cropDebt)
                //    throw new Exception("ชาวไร่รายนี้มียอดหนี้คงค้างในระบบ ไม่สามารถยกเลิกสัญญาได้ กรุณาตรวจสอบยอดหนี้อีกครั้ง");

                model.UnRegisterReason = unRegisterReason;
                model.ActiveStatus = false;
                model.ModifiedByUser = modifiedUser;
                model.LastModified = DateTime.Now;

                uow.RegistrationFarmerRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CancelUnRegister(string farmerCode, string modifiedUser)
        {
            try
            {
                var cropDefault = BuyingFacade.CropBL().GetDefault();
                if (cropDefault == null)
                    throw new ArgumentException("ไม่พบ crop default โปรดติดต่อผู้ดูแลระบบ");

                var model = GetSingle(cropDefault.Crop1, farmerCode);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนชาวไร่รหัส " + farmerCode + " นี้ในระบบ");

                model.ActiveStatus = true;
                model.UnRegisterReason = null;
                model.ModifiedByUser = modifiedUser;
                model.LastModified = DateTime.Now;

                uow.RegistrationFarmerRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}