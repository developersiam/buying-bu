﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IChemicalItemBL
    {
        void Add(ChemicalItem model);
        void Update(ChemicalItem model);
        void Delete(Guid id);
        ChemicalItem GetByID(Guid id);
        List<ChemicalItem> GetByCrop(short crop);
        List<ChemicalItem> GetByCategory(Guid chemicalCategoryID);
    }

    public class ChemicalItemBL : IChemicalItemBL
    {
        UnitOfWork uow;
        public ChemicalItemBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(ChemicalItem model)
        {
            try
            {
                if (model.UnitPrice < 0)
                    throw new ArgumentException("Unit Price cannot less than zero.");

                if (model.TypeCode == null)
                    throw new ArgumentException("Unit not defind.");

                if (model.UnitCode == null)
                    throw new ArgumentException("UnitCode not defind.");

                if (model.Name == null)
                    throw new ArgumentException("Name not defind.");

                if (model.ChemicalCategoryID == null)
                    throw new ArgumentException("Chemical Category ID not defind.");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("Modified User not defind.");

                model.ID = Guid.NewGuid();
                model.ModifiedDate = DateTime.Now;

                uow.ChemicalItemRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(ChemicalItem model)
        {
            try
            {
                if (model.UnitPrice < 0)
                    throw new ArgumentException("Unit Price cannot less than zero.");

                if (model.TypeCode == null)
                    throw new ArgumentException("Unit not defind.");

                if (model.UnitCode == null)
                    throw new ArgumentException("UnitCode not defind.");

                if (model.Name == null)
                    throw new ArgumentException("Name not defind.");

                if (model.ChemicalCategoryID == null)
                    throw new ArgumentException("Chemical Category ID not defind.");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("Modified User not defind.");

                var _chemicalItem = GetByID(model.ID);
                if (_chemicalItem == null)
                    throw new ArgumentException("ไม่พบข้อมูล!.");

                _chemicalItem.UnitPrice = model.UnitPrice;
                _chemicalItem.UnitCode = model.UnitCode;
                _chemicalItem.TypeCode = model.TypeCode;
                _chemicalItem.Name = model.Name;
                _chemicalItem.ModifiedUser = model.ModifiedUser;
                _chemicalItem.ModifiedDate = DateTime.Now;

                uow.ChemicalItemRepository.Update(_chemicalItem);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var _chemicalItem = GetByID(id);
                if (_chemicalItem == null)
                    throw new ArgumentException("ไม่พบข้อมูล!.");

                uow.ChemicalItemRepository.Remove(_chemicalItem);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ChemicalItem GetByID(Guid ID)
        {
            try
            {
                return uow.ChemicalItemRepository
                    .GetSingle(ch => ch.ID == ID,
                    ch => ch.ChemicalCategory,
                    ch => ch.ChemicalItemType,
                    ch => ch.ChemicalItemUnit);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ChemicalItem> GetByCrop(short crop)
        {
            try
            {
                return uow.ChemicalItemRepository
                    .Query(ch => ch.Crop == crop,
                    null,
                    ch => ch.ChemicalCategory,
                    ch => ch.ChemicalItemType,
                    ch => ch.ChemicalItemUnit).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ChemicalItem> GetByCategory(Guid chemicalCategoryID)
        {
            try
            {
                return uow.ChemicalItemRepository
                    .Query(ch => ch.ChemicalCategoryID == chemicalCategoryID,
                    null,
                    ch => ch.ChemicalCategory,
                    ch => ch.ChemicalItemType,
                    ch => ch.ChemicalItemUnit).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
