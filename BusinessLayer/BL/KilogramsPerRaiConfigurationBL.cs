﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IKilogramsPerRaiConfigurationBL
    {
        List<KilogramsPerRaiConfiguration> GetAll();
        List<KilogramsPerRaiConfiguration> GetByCrop(short crop);
        KilogramsPerRaiConfiguration GetSingle(short crop, string supplierCode);
    }

    public class KilogramsPerRaiConfigurationBL : IKilogramsPerRaiConfigurationBL
    {
        UnitOfWork uow;
        public KilogramsPerRaiConfigurationBL()
        {
            uow = new UnitOfWork();
        }

        public List<KilogramsPerRaiConfiguration> GetAll()
        {
            return uow.KilogramsPerRaiConfigurationRepository.Get();
        }

        public List<KilogramsPerRaiConfiguration> GetByCrop(short crop)
        {
            return uow.KilogramsPerRaiConfigurationRepository.Get(x => x.Crop == crop);
        }

        public KilogramsPerRaiConfiguration GetSingle(short crop, string supplierCode)
        {
            return uow.KilogramsPerRaiConfigurationRepository
                .GetSingle(k => k.Crop == crop &&
                k.SupplierCode == supplierCode);
        }
    }
}
