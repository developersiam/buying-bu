﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IFarmerChildInfoBL
    {
        void Add(FarmerChildInformation model);
        void Update(FarmerChildInformation model);
        void Delete(FarmerChildInformation model);
        List<FarmerChildInformation> GetByCitizenID(string citizenID);
        FarmerChildInformation GetByChildID(Guid id);
    }

    public class FarmerChildInfoBL : IFarmerChildInfoBL
    {
        UnitOfWork uow;
        public FarmerChildInfoBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(FarmerChildInformation model)
        {
            try
            {
                if (model.CitizenID == null)
                    throw new ArgumentException("You don't specify Citizen ID.");

                if (model.FirstName == null)
                    throw new ArgumentException("You don't specify FirstName.");

                if (model.LastName == null)
                    throw new ArgumentException("You don't specify LastName.");

                if (model.BirthDate == null)
                    throw new ArgumentException("You don't specify BirthDate.");

                if (model.EducationLevelID == null)
                    throw new ArgumentException("You don't specify Education Level.");

                if (model.ScoolName == null)
                    throw new ArgumentException("You don't specify Scool Name.");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("You don't specify Modified User.");

                model.ModifiedDate = DateTime.Now;

                uow.FarmerChildInformationRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Update(FarmerChildInformation model)
        {
            try
            {
                if (model.CitizenID == null)
                    throw new ArgumentException("You don't specify Citizen ID.");

                if (model.FirstName == null)
                    throw new ArgumentException("You don't specify FirstName.");

                if (model.LastName == null)
                    throw new ArgumentException("You don't specify LastName.");

                if (model.BirthDate == null)
                    throw new ArgumentException("You don't specify BirthDate.");

                if (model.EducationLevelID == null)
                    throw new ArgumentException("You don't specify Education Level.");

                if (model.ScoolName == null)
                    throw new ArgumentException("You don't specify Scool Name.");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("You don't specify Modified User.");

                if (GetByChildID(model.ChildID) == null)
                    throw new ArgumentException("Find by ID not found.");

                model.ModifiedDate = DateTime.Now;
                uow.FarmerChildInformationRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(FarmerChildInformation model)
        {
            try
            {
                if (model.ChildID == null)
                    throw new ArgumentException("You don't specify Child ID.");

                if (GetByChildID(model.ChildID) == null)
                    throw new ArgumentException("Find by ID not found.");

                uow.FarmerChildInformationRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<FarmerChildInformation> GetByCitizenID(string citizenID)
        {
            try
            {
                return uow.FarmerChildInformationRepository.Query(f => f.CitizenID == citizenID).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public FarmerChildInformation GetByChildID(Guid childID)
        {
            try
            {
                return uow.FarmerChildInformationRepository.GetSingle(f => f.ChildID == childID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
