﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IPricingSetBL
    {
        void Add(short crop, byte pricingNumber, bool isDefault, string modifiedBy);
        void Delete(short crop, byte pricingNumber);
        void SetDefault(short crop, byte pricingNumber, string modifiedBy);
        PricingSet GetSingle(short crop, byte pricingNumber);
        PricingSet GetByDefault();
        List<PricingSet> GetByCrop(short crop);
    }

    public class PricingSetBL : IPricingSetBL
    {
        UnitOfWork uow;
        public PricingSetBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(short crop, byte pricingNumber, bool isDefault, string modifiedBy)
        {
            try
            {
                if (crop.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล paymentCrop");

                if (pricingNumber.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล pricingNumber");

                if (modifiedBy == null)
                    throw new ArgumentException("ไม่พบข้อมูล modifiedBy");

                var pricingSet = GetSingle(crop, pricingNumber);

                if (pricingSet != null)
                    throw new ArgumentException("มีข้อมูลนี้ซ้ำอยู่แล้วในระบบ");

                pricingSet.Crop = crop;
                pricingSet.PricingNumber = pricingNumber;
                pricingSet.IsDefault = isDefault;
                pricingSet.AssignDate = DateTime.Now;
                pricingSet.ModifiedDate = DateTime.Now;
                pricingSet.ModifiedBy = modifiedBy;

                uow.PricingSetRepository.Add(pricingSet);
                uow.Save();

                if (isDefault == true)
                    this.SetDefault(crop, pricingNumber, modifiedBy);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(short crop, byte pricingNumber)
        {
            try
            {
                var pricingSet = GetSingle(crop, pricingNumber);

                if (pricingSet == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.PricingSetRepository.Remove(pricingSet);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetDefault(short crop, byte pricingNumber, string modifiedBy)
        {
            try
            {
                if (crop.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล paymentCrop");

                if (pricingNumber.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล pricingNumber");

                foreach (PricingSet item in uow.PricingSetRepository.Get())
                {
                    item.IsDefault = false;
                    uow.PricingSetRepository.Update(item);
                }

                var pricingSet = GetSingle(crop, pricingNumber);

                pricingSet.IsDefault = true;
                pricingSet.ModifiedBy = modifiedBy;
                pricingSet.ModifiedDate = DateTime.Now;

                uow.PricingSetRepository.Update(pricingSet);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PricingSet GetSingle(short crop, byte pricingNumber)
        {
            try
            {
                return uow.PricingSetRepository.GetSingle(ps => ps.Crop == crop && ps.PricingNumber == pricingNumber);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PricingSet GetByDefault()
        {
            try
            {
                return uow.PricingSetRepository.GetSingle(ps => ps.IsDefault);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PricingSet> GetByCrop(short crop)
        {
            try
            {
                return uow.PricingSetRepository.Query(ps => ps.Crop == crop).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
