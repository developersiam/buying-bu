﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IPolicyRoleBL
    {
        void Add(AppPolicyRole model);
        void Edit(Guid roleID, string roleName, string modifiedUser);
        void Delete(Guid roleID);
        AppPolicyRole GetSingle(Guid roleID);
        List<AppPolicyRole> GetAll();
    }

    public class PolicyRoleBL : IPolicyRoleBL
    {
        static Guid appID = Guid.Parse("92ACD5C1-69CB-42F4-AEBF-160DBB445082");

        IUnitOfWork _uow;

        public PolicyRoleBL()
        {
            _uow = new UnitOfWork();
        }

        public void Add(AppPolicyRole model)
        {
            try
            {
                if (_uow.AppPolicyRoleRepository
                    .Get(x => x.RoleName == model.RoleName && x.ApplicationID == appID)
                    .Count() > 0)
                    throw new ArgumentException("Role " + model.RoleName + " นี้มีซ้ำแล้วในระบบ");

                model.ApplicationID = appID;
                model.CreateDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;
                model.ModifiedBy = model.CreateBy;

                _uow.AppPolicyRoleRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid roleID)
        {
            try
            {
                var model = GetSingle(roleID);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (model.AppPolicyUserRoles.Count() > 0)
                    throw new ArgumentException("Role นี้ถูกกำหนดให้ user บางรายไปแล้ว โปรดลบข้อมูลการกำหนดสิทธิ์ออก่กอน");

                _uow.AppPolicyRoleRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(Guid roleID, string roleName, string modifiedUser)
        {
            try
            {
                var editModel = GetSingle(roleID);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                editModel.RoleName = roleName;
                editModel.ModifiedBy = modifiedUser;
                editModel.ModifiedDate = DateTime.Now;

                _uow.AppPolicyRoleRepository.Update(editModel);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AppPolicyRole> GetAll()
        {
            return _uow.AppPolicyRoleRepository
                .Get(x => x.ApplicationID == appID)
                .ToList();
        }

        public AppPolicyRole GetSingle(Guid roleID)
        {
            return _uow.AppPolicyRoleRepository.GetSingle(x => x.RoleID == roleID);
        }
    }
}
