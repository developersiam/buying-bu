﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface ISeedVarietyBL
    {
        List<SeedVariety> GetByCrop(short crop);
    }

    public class SeedVarietyBL : ISeedVarietyBL
    {
        UnitOfWork uow;
        public SeedVarietyBL()
        {
            uow = new UnitOfWork();
        }

        public List<SeedVariety> GetByCrop(short crop)
        {
            return uow.SeedVarietyRepository.Query(sv => sv.SeedCrop == crop).ToList();
        }
    }
}
