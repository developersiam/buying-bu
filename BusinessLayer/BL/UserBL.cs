﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IUserBL
    {
        void Add(UserAccount user);
        void Update(UserAccount user);
        void Delete(string username);
        UserAccount GetByUsername(string username);
        List<UserAccount> GetAll();
        List<UserAccount> GetByRoleName(string roleName);
    }

    public class UserBL : IUserBL
    {
        UnitOfWork uow;
        public UserBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(UserAccount user)
        {
            try
            {
                if (user.Username == null)
                    throw new ArgumentException("โปรดระบุชื่อผู้ใช้ (Username)");

                if (user.Password == null)
                    throw new ArgumentException("โปรดระบุรหัสผ่าน (Password)");

                if (user.FirstName == null)
                    throw new ArgumentException("โปรดระบุชื่อ");

                if (user.LastName == null)
                    throw new ArgumentException("โปรดระบุนามสกุล");

                if (user.LastName == null)
                    throw new ArgumentException("โปรดระบุผู้บันทึกข้อมูล");

                if (GetByUsername(user.Username) != null)
                    throw new ArgumentException("มีข้อมูลนี้ซ้ำอยู่แล้วในระบบ");

                user.DisplayName = user.FirstName + " " + user.LastName;
                user.ActiveStatus = true;
                user.CreateDate = DateTime.Now;
                user.LastModifiedDate = DateTime.Now;

                uow.UserAccountRepository.Add(user);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(UserAccount user)
        {
            try
            {
                if (user.Username == null)
                    throw new ArgumentException("โปรดระบุชื่อผู้ใช้ (Username)");

                if (user.Password == null)
                    throw new ArgumentException("โปรดระบุรหัสผ่าน (Password)");

                if (user.FirstName == null)
                    throw new ArgumentException("โปรดระบุชื่อ");

                if (user.LastName == null)
                    throw new ArgumentException("โปรดระบุนามสกุล");

                if (user.ModifiedByUser == null)
                    throw new ArgumentException("โปรดระบุผู้แก้ไขข้อมูล");

                if (GetByUsername(user.Username) == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                user.DisplayName = user.FirstName + " " + user.LastName;
                user.LastModifiedDate = DateTime.Now;

                uow.UserAccountRepository.Update(user);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string username)
        {
            try
            {
                var user = GetByUsername(username);
                if (user == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.UserAccountRepository.Remove(user);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserAccount> GetAll()
        {
            return uow.UserAccountRepository.Get().ToList();
        }

        public UserAccount GetByUsername(string username)
        {
            return uow.UserAccountRepository
                .GetSingle(u => u.Username == username,
                u => u.UserRoles,
                u => u.StaffUser,
                u => u.StaffUser.Area);
        }

        public List<UserAccount> GetByRoleName(string roleName)
        {
            Role role = new Role();
            role = uow.RoleRepository.GetSingle(r => r.RoleName == roleName);

            List<UserRole> userRoles = new List<UserRole>();
            userRoles = uow.UserRoleRepository
                .Query(ur => ur.RoleID == role.RoleID,
                null,
                ur => ur.UserAccount)
                .ToList();

            List<UserAccount> users = new List<UserAccount>();

            foreach (UserRole item in userRoles)
                users.Add(item.UserAccount);

            return users;
        }
    }
}
