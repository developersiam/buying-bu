﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface ICropInputItemBL
    {
        void Add(CropInputItem model);
        void Update(CropInputItem model);
        void Delete(CropInputItem model);
        CropInputItem GetByID(Guid id);
        List<CropInputItem> GetByCrop(short crop);
    }

    public class CropInputItemBL : ICropInputItemBL
    {
        UnitOfWork uow;
        public CropInputItemBL()
        {
            uow = new UnitOfWork();
        }
        public void Add(CropInputItem model)
        {
            try
            {
                if (model == null)
                    throw new ArgumentException("Crop input item object is null.");

                if (model.CropInputCategoryID == null)
                    throw new ArgumentException("Crop Input Category ID is null.");

                if (model.UnitPrice < 0)
                    throw new ArgumentException("Unit Price cannot less than zero.");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("Modified User is null.");

                uow.CropInputItemRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(CropInputItem model)
        {
            try
            {
                if (model.CropInputCategoryID == null)
                    throw new ArgumentException("Crop Input Category ID is null.");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("Modified User is null.");

                var _cropInputItem = GetByID(model.ID);
                if (_cropInputItem == null)
                    throw new ArgumentException("Find not found!");

                _cropInputItem.CropInputCategoryID = model.CropInputCategoryID;
                _cropInputItem.UnitPrice = model.UnitPrice;
                _cropInputItem.ModifiedUser = model.ModifiedUser;
                _cropInputItem.ModifiedDate = DateTime.Now;

                uow.CropInputItemRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(CropInputItem model)
        {
            try
            {
                var _cropInputItem = GetByID(model.ID);
                if (_cropInputItem == null)
                    throw new ArgumentException("Find not found!");

                uow.CropInputItemRepository.Remove(_cropInputItem);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public CropInputItem GetByID(Guid ID)
        {
            try
            {
                return uow.CropInputItemRepository
                    .GetSingle(ci => ci.ID == ID, ci => ci.CropInputCategory);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<CropInputItem> GetByCrop(short crop)
        {
            try
            {
                return uow.CropInputItemRepository
                    .Query(ci => ci.Crop == crop, null, ci => ci.CropInputCategory).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
