﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface ISampleResultBL
    {
        void Add(string citizenID, short crop, string sampleType, string position, decimal result, DateTime resultDate);
        void Edit(Guid id, decimal result, DateTime resultDate);
        void Delete(Guid id);
        List<SampleResult> GetByPerson(short crop, string citizenID);
        List<SampleResult> GetByType(short crop, string citizenID, string sampleType);
        List<SampleResult> GetByResultDate(DateTime resultDate);
        List<SampleResult> GetByCrop(short crop);
        SampleResult GetSingle(Guid id);
    }

    public class SampleResultBL : ISampleResultBL
    {
        UnitOfWork _uow;
        public SampleResultBL()
        {
            _uow = new UnitOfWork();
        }

        public void Add(string citizenID, short crop, string sampleType, string position, decimal result, DateTime resultDate)
        {
            try
            {
                if (string.IsNullOrEmpty(citizenID))
                    throw new ArgumentException("Citizen ID cannot be empty.");

                if (string.IsNullOrEmpty(sampleType))
                    throw new ArgumentException("Citizen ID cannot be empty.");

                if (string.IsNullOrEmpty(position))
                    throw new ArgumentException("Citizen ID cannot be empty.");

                _uow.SampleResultRepository.Add(new SampleResult
                {
                    Id = Guid.NewGuid(),
                    Crop = crop,
                    CitizenID = citizenID,
                    SampleType = sampleType,
                    Position = position,
                    Result = result,
                    ResultDate = DateTime.Now,
                    ModifiedDate = DateTime.Now
                });
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var model = GetSingle(id);
                if (model == null)
                    throw new ArgumentException("Not found.");

                _uow.SampleResultRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(Guid id, decimal result, DateTime resultDate)
        {
            try
            {
                var model = GetSingle(id);
                if (model == null)
                    throw new ArgumentException("Not found.");

                model.Result = result;
                model.ResultDate = resultDate;

                _uow.SampleResultRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SampleResult> GetByCrop(short crop)
        {
            return _uow.SampleResultRepository.Get(x => x.Crop == crop).ToList();
        }

        public List<SampleResult> GetByPerson(short crop, string citizenID)
        {
            return _uow.SampleResultRepository
                .Get(x => x.Crop == crop && x.CitizenID == citizenID).ToList();
        }

        public List<SampleResult> GetByResultDate(DateTime resultDate)
        {
            return _uow.SampleResultRepository.Get(x => x.ResultDate == resultDate).ToList();
        }

        public List<SampleResult> GetByType(short crop, string citizenID, string sampleType)
        {
            return _uow.SampleResultRepository
                .Get(x => x.Crop == crop &&
                x.CitizenID == citizenID &&
                x.SampleType == sampleType)
                .ToList();
        }

        public SampleResult GetSingle(Guid id)
        {
            return _uow.SampleResultRepository.GetSingle(x => x.Id == id);
        }
    }
}
