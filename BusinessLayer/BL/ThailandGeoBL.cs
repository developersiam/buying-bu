﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IThailandGeoBL
    {
        List<Province> GetProvinces();
        List<District> GetDistrictByProvince(string provinceCode);
        List<SubDistrict> GetSubDistrictByDistrict(string districtCode);
        List<District> GetDistrictByProvinceName(string provinceName);
        List<SubDistrict> GetSubDistrictByDistrictName(string districtName);

    }

    public class ThailandGeoBL : IThailandGeoBL
    {
        UnitOfWork uow;
        public ThailandGeoBL()
        {
            uow = new UnitOfWork();
        }

        public List<Province> GetProvinces()
        {
            return uow.ProvinceRepository.Get().ToList();
        }
        public List<District> GetDistrictByProvince(string provinceCode)
        {
            return uow.DistrictRepository
                .Query(x => x.ProvinceCode == provinceCode)
                .ToList();
        }
        public List<SubDistrict> GetSubDistrictByDistrict(string districtCode)
        {
            return uow.SubDistrictRepository
                .Query(x => x.DistrictCode == districtCode)
                .OrderBy(x => x.SubDistrictNameThai)
                .ToList();
        }

        public List<District> GetDistrictByProvinceName(string provinceName)
        {
            return uow.DistrictRepository
                .Query(x => x.Province.ProvinceNameThai
                .Contains(provinceName))
                .OrderBy(x => x.DistrictNameThai)
                .ToList();
        }

        public List<SubDistrict> GetSubDistrictByDistrictName(string districtName)
        {
            return uow.SubDistrictRepository
                .Query(x => x.District.DistrictNameThai
                .Contains(districtName))
                .OrderBy(x => x.SubDistrictNameThai)
                .ToList();
        }
    }
}
