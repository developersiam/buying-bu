﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IChemicalDistributionBL
    {        
        void Add(ChemicalDistribution model);        
        void Update(ChemicalDistribution model);        
        void Delete(short crop, string farmerCode, Guid chemicalItemID);
        ChemicalDistribution GetByID(short crop, string farmerCode, Guid chemicalItemID);
        List<ChemicalDistribution> GetByFarmer(short crop, string farmerCode);        
        bool NotChemicaldistribution(short crop, string farmerCode);
    }

    public class ChemicalDistributionBL : IChemicalDistributionBL
    {
        UnitOfWork uow;
        public ChemicalDistributionBL()
        {
            uow = new UnitOfWork();
        }
       
        public void Add(ChemicalDistribution model)
        {
            try
            {
                if (model.ChemicalItemID == null)
                    throw new ArgumentException("Chemical Item ID not define!");

                if (model.FarmerCode == null)
                    throw new ArgumentException("FarmerCode not define!");

                if (model.DistributionDate == null)
                    throw new ArgumentException("DistributionDate not define!");

                if (model.DistributionUser == null)
                    throw new ArgumentException("DistributionUser not define!");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("ModifiedUser not define!");

                if (model.Quantity <= 0)
                    throw new ArgumentException("จำนวนที่จ่ายให้กับชาวไร่ จะต้องมีค่าตั้งแต่ 1 ขึ้นไป");

                var _chemicalDistribution = GetByID(model.Crop,
                     model.FarmerCode,
                     model.ChemicalItemID);
                if (_chemicalDistribution != null)
                    throw new ArgumentException("มีข้อมูลนี้ซ้ำแล้วในระบบ!.");

                model.ModifiedDate = DateTime.Now;

                uow.ChemicalDistributionRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public void Update(ChemicalDistribution model)
        {
            try
            {
                if (model == null)
                    throw new ArgumentException("Chemical distribution not define!.");

                if (model.ChemicalItemID == null)
                    throw new ArgumentException("Chemical Item ID not define!.");

                if (model.FarmerCode == null)
                    throw new ArgumentException("FarmerCode not define!.");

                if (model.DistributionDate == null)
                    throw new ArgumentException("DistributionDate not define!.");

                if (model.DistributionUser == null)
                    throw new ArgumentException("DistributionUser not define!.");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("ModifiedUser not define!.");

                if (model.Quantity <= 0) throw
                        new ArgumentException("Quantity cannot less than 1.");

                var _chemicalDistribution = GetByID(model.Crop,
                    model.FarmerCode,
                    model.ChemicalItemID);

                if (_chemicalDistribution == null)
                    throw new ArgumentException("ไม่พบข้อมูล!.");

                _chemicalDistribution.DistributionDate = model.DistributionDate;
                _chemicalDistribution.DistributionUser = model.DistributionUser;
                _chemicalDistribution.Quantity = model.Quantity;
                _chemicalDistribution.ModifiedDate = model.ModifiedDate;
                _chemicalDistribution.ModifiedUser = model.ModifiedUser;

                uow.ChemicalDistributionRepository.Update(_chemicalDistribution);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public void Delete(short crop, string farmerCode, Guid chemicalItemID)
        {
            try
            {
                var _chemicalDistribution = GetByID(crop, farmerCode, chemicalItemID);

                if (_chemicalDistribution == null)
                    throw new ArgumentException("ไม่พบข้อมูล!.");

                uow.ChemicalDistributionRepository.Remove(_chemicalDistribution);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ChemicalDistribution GetByID(short crop, string farmerCode, Guid chemicalItemID)
        {
            try
            {
                return uow.ChemicalDistributionRepository
                    .GetSingle(cd => cd.Crop == crop &&
                    cd.FarmerCode == farmerCode &&
                    cd.ChemicalItemID == chemicalItemID,
                    cd => cd.ChemicalItem,
                    ch => ch.ChemicalItem.ChemicalCategory,
                    ch => ch.ChemicalItem.ChemicalItemType,
                    ch => ch.ChemicalItem.ChemicalItemUnit);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ChemicalDistribution> GetByFarmer(short crop, string farmerCode)
        {
            try
            {
                return uow.ChemicalDistributionRepository
                    .Query(cd => cd.Crop == crop &&
                    cd.FarmerCode == farmerCode,
                    null,
                    cd => cd.ChemicalItem,
                    ch => ch.ChemicalItem.ChemicalCategory,
                    ch => ch.ChemicalItem.ChemicalItemType,
                    ch => ch.ChemicalItem.ChemicalItemUnit).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }     
        
        public bool NotChemicaldistribution(short crop, string farmerCode)
        {
            var result = false;
            if (GetByFarmer(crop, farmerCode).Count() > 0)
            {
                result = true;
            }
            return result;
        }
    }
}
