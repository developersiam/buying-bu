﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface ICropInputsPackageBL
    {
        void Add(CropInputsPackage model);
        void Delete(string packageCode);
        CropInputsPackage GetSingle(string packageCode);
        List<CropInputsPackage> GetByCrop(short crop);
        List<CropInputsPackage> GetBySupplier(short crop, string supplierCode);
        string GetNewPackageCode(short crop);
    }

    public class CropInputsPackageBL : ICropInputsPackageBL
    {
        UnitOfWork uow;
        public CropInputsPackageBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(CropInputsPackage model)
        {
            try
            {
                uow.CropInputsPackageRepository
                    .Add(new CropInputsPackage
                    {
                        PackageCode = GetNewPackageCode(model.Crop),
                        Crop = model.Crop,
                        SupplierCode = model.SupplierCode,
                        Description = model.Description,
                        ModifiedUser = model.ModifiedUser,
                        ModifiedDate = DateTime.Now
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string packageCode)
        {
            try
            {
                var model = GetSingle(packageCode);
                if (model == null)
                    throw new Exception("ไม่พบข้อมูลในระบบ");

                if (model.CropInputsPackageDetails.Count() > 0)
                    throw new Exception("มีรายการสินค้าอยู่ภายในแพ็คเกจนี้ ไม่สามารถลบแพ็คเกจนี้ได้");

                uow.CropInputsPackageRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CropInputsPackage> GetByCrop(short crop)
        {
            return uow.CropInputsPackageRepository
                .Get(x => x.Crop == crop,
                null,
                x => x.CropInputsPackageDetails);
        }

        public List<CropInputsPackage> GetBySupplier(short crop, string supplierCode)
        {
            return uow.CropInputsPackageRepository
                .Get(x => x.Crop == crop &&
                x.SupplierCode == supplierCode,
                null,
                x => x.CropInputsPackageDetails);
        }

        public string GetNewPackageCode(short crop)
        {
            try
            {
                /// Generate issued Code.
                /// 
                int _max;
                string _packageCode;

                var list = GetByCrop(crop);
                if (list.Count() <= 0)
                    _max = 1;
                else
                    _max = list.Max(x => Convert.ToInt16(x.PackageCode.Substring(8, 3))) +1;

                _packageCode = "PC-" + crop + "-" + _max.ToString().PadLeft(3, '0');

                return _packageCode;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CropInputsPackage GetSingle(string packageCode)
        {
            return uow.CropInputsPackageRepository
                .GetSingle(x => x.PackageCode == packageCode
                , x => x.CropInputsPackageDetails);
        }
    }
}
