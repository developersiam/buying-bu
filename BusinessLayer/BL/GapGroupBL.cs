﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IGapGroupBL
    {
        void AddNewGAPGroupLeader(Person person, string supplierCode, string modifiedBy);
        void AddGAPGroupLeaderByCrop(short crop, string citizenID, string supplierCode, string modifiedBy);
        void AddGAPGroup(short crop, string citizenID, string supplierCode, string modifiedBy, string gapGroupCode);
        void DeleteGAPGroupLeader(string citizenID, string supplierCode);
        void DeleteGAPGroupLeaderByCrop(short crop, string citizenID, string supplierCode);
        void CreateGAPGroup(GAPGroup gapGroup);
        void DeleteGAPGroup(string gapGroupCode);
        GAPGroupLeader GetGAPGroupLeaderByID(string citizenID, string supplierCode);
        List<GAPGroupLeader> GetGAPGroupLeaderBySupplier(string supplierCode);
        List<GAPGroupLeader> GetGAPGroupLeaderByCitizenID(string citizenID);
        List<GAPGroupLeaderByCrop> GetGAPGroupLeaderByCropBySupplier(short crop, string supplierCode);
        List<GAPGroupLeaderByCrop> GetGAPGroupLeaderByCropByCitizenID(short crop, string citizenID);
        List<GAPGroup> GetGAPGroupBySupplier(short crop, string supplierCode);
        List<GAPGroup> GetGAPGroupByCitizenID(short crop, string citizenID);
        List<GAPGroup> GetGAPGroupByLeader(short crop, string citizenID, string supplierCode);
        GAPGroup GetGAPGroupByGapGroupCode(string gapGroupCode);
        Person GetLeaderProfileByIDCard(string citizenID);
        void AddNewPerson(Person person);
    }

    public class GapGroupBL : IGapGroupBL
    {
        UnitOfWork uow;
        public GapGroupBL()
        {
            uow = new UnitOfWork();
        }

        public void AddNewGAPGroupLeader(Person person, string supplierCode, string modifiedBy)
        {
            try
            {
                if (person.CitizenID == "")
                    throw new ArgumentException("ไม่ได้กำหนดค่า CitizenID");

                if (supplierCode == "")
                    throw new ArgumentException("ไม่ได้กำหนดค่า SupplierCode");

                if (modifiedBy == "")
                    throw new ArgumentException("ไม่ได้กำหนดค่า ModifiedBy");

                if (GetGAPGroupLeaderByID(person.CitizenID, supplierCode) != null)
                    throw new ArgumentException("หัวหน้าชาวไร่รหัส " + person.CitizenID +
                        " นี้เคยลงทะเบียนเป็นหัวหน้าชาวไร่ไว้กับ " + supplierCode + " แล้วก่อนหน้านี้!");

                if (uow.PersonRepository.GetSingle(p => p.CitizenID == person.CitizenID) == null)
                    AddNewPerson(person);

                GAPGroupLeader leader = new GAPGroupLeader
                {
                    CitizenID = person.CitizenID,
                    SupplierCode = supplierCode,
                    ModifiedBy = modifiedBy,
                    ModifiedDate = DateTime.Now
                };

                uow.GAPGroupLeaderRepository.Add(leader);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void AddGAPGroupLeaderByCrop(short crop, string citizenID, string supplierCode, string modifiedBy)
        {
            try
            {
                if (citizenID == "")
                    throw new ArgumentException("ไม่ได้กำหนดค่า CitizenID");

                if (supplierCode == "")
                    throw new ArgumentException("ไม่ได้กำหนดค่า SupplierCode");

                if (modifiedBy == "")
                    throw new ArgumentException("ไม่ได้กำหนดค่า ModifiedBy");

                if (crop.ToString() == "")
                    throw new ArgumentException("ไม่ได้กำหนดค่า Crop");

                if (uow.GAPGroupLeaderByCropRepository
                    .GetSingle(gg => gg.Crop == crop &&
                    gg.SupplierCode == supplierCode &&
                    gg.CitizenID == citizenID) != null)
                    throw new ArgumentException("มีข้อมูลนี้ซ้ำอยู่แล้วในระบบ!");

                GAPGroupLeaderByCrop gapGroupLeaderByCrop = new GAPGroupLeaderByCrop
                {
                    Crop = crop,
                    CitizenID = citizenID,
                    SupplierCode = supplierCode,
                    ModifiedBy = modifiedBy,
                    ModifiedDate = DateTime.Now
                };

                uow.GAPGroupLeaderByCropRepository.Add(gapGroupLeaderByCrop);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void AddGAPGroup(short crop, string citizenID, string supplierCode, string modifiedBy, string gapGroupCode)
        {
            try
            {
                if (citizenID == "")
                    throw new ArgumentException("ไม่ได้กำหนดค่า CitizenID");

                if (supplierCode == "")
                    throw new ArgumentException("ไม่ได้กำหนดค่า SupplierCode");

                if (modifiedBy == "")
                    throw new ArgumentException("ไม่ได้กำหนดค่า ModifiedBy");

                if (crop.ToString() == "")
                    throw new ArgumentException("ไม่ได้กำหนดค่า Crop");

                if (gapGroupCode == "")
                    throw new ArgumentException("ไม่ได้กำหนดค่า GAP Group Code");

                string gapGroupCodeResult = crop + "-" + gapGroupCode;

                if (uow.GAPGroupRepository.GetSingle(gg => gg.GAPGroupCode == gapGroupCodeResult) != null)
                    throw new ArgumentException("มีข้อมูลนี้ซ้ำอยู่แล้วในระบบ!");

                GAPGroup gapGroup = new GAPGroup
                {
                    Crop = crop,
                    SupplierCode = supplierCode,
                    CitizenID = citizenID,
                    GAPGroupCode = crop + "-" + gapGroupCode,
                    ModifiedBy = modifiedBy,
                    ModifiedDate = DateTime.Now
                };

                uow.GAPGroupRepository.Add(gapGroup);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteGAPGroupLeader(string citizenID, string supplierCode)
        {
            try
            {
                var leader = GetGAPGroupLeaderByID(citizenID, supplierCode);
                if (leader == null) throw new ArgumentException("ไม่พบข้อมูลในระบบ!");

                uow.GAPGroupLeaderRepository.Remove(leader);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteGAPGroupLeaderByCrop(short crop, string citizenID, string supplierCode)
        {
            try
            {
                var gapGroupLeaderByCrop = uow.GAPGroupLeaderByCropRepository
                    .GetSingle(gg => gg.Crop == crop &&
                    gg.SupplierCode == supplierCode &&
                    gg.CitizenID == citizenID);

                if (gapGroupLeaderByCrop == null)
                    throw new ArgumentException("ไม่มีข้อมูลในระบบ!");

                var findGAPGroup = GetGAPGroupByLeader(crop, citizenID, supplierCode);

                if (findGAPGroup.Count() > 0)
                    throw new ArgumentException("มีช้อมูลGAPGroup แล้ว ให้ทำการลบGAPGroup ให้เรียบร้อย!");

                uow.GAPGroupLeaderByCropRepository.Remove(gapGroupLeaderByCrop);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void CreateGAPGroup(GAPGroup gapGroup)
        {
            try
            {
                if (gapGroup.GAPGroupCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล GAP Group Code");

                if (gapGroup.Crop.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล Crop");

                if (gapGroup.CitizenID == "")
                    throw new ArgumentException("ไม่พบข้อมูล CitizenID");

                if (gapGroup.ModifiedBy == "")
                    throw new ArgumentException("ไม่พบข้อมูล ModifiedBy");

                if (GetGAPGroupByGapGroupCode(gapGroup.GAPGroupCode) != null)
                    throw new ArgumentException("มีข้อมูลนี้ซ้ำแล้วในระบบ!");

                gapGroup.ModifiedDate = DateTime.Now;

                uow.GAPGroupRepository.Add(gapGroup);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteGAPGroup(string gapGroupCode)
        {
            try
            {
                var gapGroup = GetGAPGroupByGapGroupCode(gapGroupCode);
                if (gapGroup == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ!");

                uow.GAPGroupRepository.Remove(gapGroup);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public GAPGroupLeader GetGAPGroupLeaderByID(string citizenID, string supplierCode)
        {
            return uow.GAPGroupLeaderRepository
                .GetSingle(l => l.CitizenID == citizenID &&
                l.SupplierCode == supplierCode, l =>
                l.GAPGroupLeaderByCrops);
        }
        public List<GAPGroupLeader> GetGAPGroupLeaderBySupplier(string supplierCode)
        {
            return uow.GAPGroupLeaderRepository
                .Query(gg => gg.SupplierCode == supplierCode,
                null,
                gg => gg.Person).ToList();
        }
        public List<GAPGroupLeader> GetGAPGroupLeaderByCitizenID(string citizenID)
        {
            return uow.GAPGroupLeaderRepository
                .Query(gg => gg.CitizenID == citizenID,
                null,
                gg => gg.Person).ToList();
        }
        public List<GAPGroupLeaderByCrop> GetGAPGroupLeaderByCropBySupplier(short crop, string supplierCode)
        {
            try
            {
                return uow.GAPGroupLeaderByCropRepository
                    .Query(ggc => ggc.Crop == crop &&
                    ggc.SupplierCode == supplierCode,
                    null,
                    ggc => ggc.GAPGroupLeader,
                    ggc => ggc.GAPGroupLeader.Person,
                    ggc => ggc.GAPGroups).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<GAPGroupLeaderByCrop> GetGAPGroupLeaderByCropByCitizenID(short crop, string citizenID)
        {
            try
            {
                return uow.GAPGroupLeaderByCropRepository
                    .Query(ggc => ggc.Crop == crop &&
                    ggc.CitizenID == citizenID,
                    null,
                    ggc => ggc.GAPGroups).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<GAPGroup> GetGAPGroupBySupplier(short crop, string supplierCode)
        {
            return uow.GAPGroupRepository
                .Query(g => g.Crop == crop &&
                g.SupplierCode == supplierCode,
                null,
                gg => gg.RegistrationFarmers,
                gg => gg.GAPGroupLeaderByCrop,
                gg => gg.GAPGroupLeaderByCrop.GAPGroupLeader,
                gg => gg.GAPGroupLeaderByCrop.GAPGroupLeader.Person)
                .ToList();
        }
        public List<GAPGroup> GetGAPGroupByCitizenID(short crop, string citizenID)
        {
            return uow.GAPGroupRepository.Query(g => g.Crop == crop &&
            g.CitizenID == citizenID,
            null,
            gg => gg.RegistrationFarmers,
            gg => gg.GAPGroupLeaderByCrop,
            gg => gg.GAPGroupLeaderByCrop.GAPGroupLeader,
            gg => gg.GAPGroupLeaderByCrop.GAPGroupLeader.Person)
            .ToList();
        }
        public List<GAPGroup> GetGAPGroupByLeader(short crop, string citizenID, string supplierCode)
        {
            return uow.GAPGroupRepository.Query(g => g.Crop == crop &&
            g.CitizenID == citizenID &&
            g.SupplierCode == supplierCode,
            null,
            gg => gg.RegistrationFarmers,
            gg => gg.GAPGroupLeaderByCrop,
            gg => gg.GAPGroupLeaderByCrop.GAPGroupLeader,
            gg => gg.GAPGroupLeaderByCrop.GAPGroupLeader.Person)
            .ToList();
        }
        public GAPGroup GetGAPGroupByGapGroupCode(string gapGroupCode)
        {
            return uow.GAPGroupRepository
                .GetSingle(g => g.GAPGroupCode == gapGroupCode, 
                gg => gg.RegistrationFarmers,
                gg => gg.GAPGroupLeaderByCrop.GAPGroupLeader.Person);
        }
        public Person GetLeaderProfileByIDCard(string citizenID)
        {
            return uow.PersonRepository.GetSingle(p => p.CitizenID == citizenID);
        }
        public void AddNewPerson(Person person)
        {
            try
            {
                if (person.CitizenID == null) throw new ArgumentException("ไม่พบข้อมูล CitizenID");
                if (person.Prefix == null) throw new ArgumentException("ไม่พบข้อมูล Prefix");
                if (person.FirstName == null) throw new ArgumentException("ไม่พบข้อมูล FirstName");
                if (person.LastName == null) throw new ArgumentException("ไม่พบข้อมูล LastName");
                if (person.BirthDate == null) throw new ArgumentException("ไม่พบข้อมูล BirthDate");
                if (person.HouseNumber == null) throw new ArgumentException("ไม่พบข้อมูล HouseNumber");
                if (person.Village == null) throw new ArgumentException("ไม่พบข้อมูล Village");
                if (person.Tumbon == null) throw new ArgumentException("ไม่พบข้อมูล Tumbon");
                if (person.Amphur == null) throw new ArgumentException("ไม่พบข้อมูล Amphur");
                if (person.Province == null) throw new ArgumentException("ไม่พบข้อมูล Province");
                if (person.ModifiedByUser == null) throw new ArgumentException("ไม่พบข้อมูล ModifiedByUser");

                if (uow.PersonRepository.GetSingle(per => per.CitizenID == person.CitizenID) != null)
                    throw new ArgumentException("มีข้อมูลซ้ำในระบบ");

                person.LastModified = DateTime.Now;

                uow.PersonRepository.Add(person);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
