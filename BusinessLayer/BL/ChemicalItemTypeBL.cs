﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IChemicalItemTypeBL
    {
        List<ChemicalItemType> GetAll();
    }

    public class ChemicalItemTypeBL : IChemicalItemTypeBL
    {
        UnitOfWork uow;
        public ChemicalItemTypeBL()
        {
            uow = new UnitOfWork();
        }

        public List<ChemicalItemType> GetAll()
        {
            return uow.ChemicalItemTypeRepository.Get().ToList();
        }

    }
}
