﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IStoreProcetureBL
    {
        List<sp_GetCPASampleCollectionByGapGroup_Result> GetGetCPASampleCollectionByGapGroup(string gapGroupCode);
        List<sp_CY2018GetDebtorPaymentByVoucher_Result> sp_CY2018GetDebtorPaymentByVoucher(short voucherCrop,
            string voucherFarmerCode,
            string voucherBuyingStationCode,
            short voucherBuyingDocumentNumber);
        List<sp_CY2018GetDebtorPaymentByFarmer_Result> sp_CY2018GetDebtorPaymentByFarmer(short crop, string farmerCode);
        List<sp_CY2018GetPPEKitsDistributionByFarmer_Result> sp_CY2018GetPPEKitsDistributionByFarmer(short crop, string farmerCode);
        List<sp_CY2018GetSeedDistributionByFarmer_Result> sp_CY2018GetSeedDistributionByFarmer(short crop, string farmerCode);
        List<sp_GetCalculationPercentOfSold_Result> sp_GetCalculationPercentOfSold(short crop, string farmerCode);
        List<sp_GetFarmerQuotaFixedAndSold_Result> sp_GetFarmerQuotaFixedAndSold(short crop, string farmerCode);
        List<sp_GetBuyingDocumentByBuyingDocument_Result> sp_GetBuyingDocumentByBuyingDocument(short crop,
            string buyingStationCode,
            string farmerCode,
            short buyingDocumentNumber);
        List<sp_GetBuyingDetailsByBuyingDocument_Result> sp_GetBuyingDetailsByBuyingDocument(short crop,
            string farmerCode,
            string stationCode,
            short documentNumber);
        List<sp_GetBuyingDetailsByFarmer_Result> sp_GetBuyingDetailsByFarmer(short crop, string farmerCode);
        List<sp_GetMoistureResultDetails_Result> sp_GetMoistureResultDetails();
        List<sp_GetTransportationDetailsByTransportationCode_Result> sp_GetTransportationDetailsByTransportationCode(string transportationDocumentCode);
        List<sp_GetTransportationDocumentByTransportationCode_Result> sp_GetTransportationDocumentByTransportationCode(string transportationDocumentCode);
        string sp_ConvertCurrencyToThaiBath(decimal currency);
    }

    public class StoreProcetureBL : IStoreProcetureBL
    {
        public List<sp_GetCPASampleCollectionByGapGroup_Result> GetGetCPASampleCollectionByGapGroup(string gapGroupCode)
        {
            return StoreProcedureRepository.GetGetCPASampleCollectionByGapGroup(gapGroupCode);
        }

        public string sp_ConvertCurrencyToThaiBath(decimal currency)
        {
            return StoreProcedureRepository.sp_ConvertCurrencyToThaiBath(currency);
        }

        public List<sp_CY2018GetDebtorPaymentByFarmer_Result> sp_CY2018GetDebtorPaymentByFarmer(short crop, string farmerCode)
        {
            return StoreProcedureRepository.sp_CY2018GetDebtorPaymentByFarmer(crop, farmerCode);
        }

        public List<sp_CY2018GetDebtorPaymentByVoucher_Result> sp_CY2018GetDebtorPaymentByVoucher(short voucherCrop,
            string voucherFarmerCode,
            string voucherBuyingStationCode,
            short voucherBuyingDocumentNumber)
        {
            return StoreProcedureRepository.sp_CY2018GetDebtorPaymentByVoucher(voucherCrop, voucherFarmerCode, voucherBuyingStationCode, voucherBuyingDocumentNumber);
        }

        public List<sp_CY2018GetPPEKitsDistributionByFarmer_Result> sp_CY2018GetPPEKitsDistributionByFarmer(short crop, string farmerCode)
        {
            return StoreProcedureRepository.sp_CY2018GetPPEKitsDistributionByFarmer(crop, farmerCode);
        }

        public List<sp_CY2018GetSeedDistributionByFarmer_Result> sp_CY2018GetSeedDistributionByFarmer(short crop, string farmerCode)
        {
            return StoreProcedureRepository.sp_CY2018GetSeedDistributionByFarmer(crop, farmerCode);
        }

        public List<sp_GetBuyingDetailsByBuyingDocument_Result> sp_GetBuyingDetailsByBuyingDocument(short crop, string farmerCode, string stationCode, short documentNumber)
        {

            return StoreProcedureRepository.sp_GetBuyingDetailsByBuyingDocument(crop, farmerCode, stationCode, documentNumber);
        }

        public List<sp_GetBuyingDetailsByFarmer_Result> sp_GetBuyingDetailsByFarmer(short crop, string farmerCode)
        {
            return StoreProcedureRepository.sp_GetBuyingDetailsByFarmer(crop, farmerCode);
        }

        public List<sp_GetBuyingDocumentByBuyingDocument_Result> sp_GetBuyingDocumentByBuyingDocument(short crop, string buyingStationCode, string farmerCode, short buyingDocumentNumber)
        {
            return StoreProcedureRepository.sp_GetBuyingDocumentByBuyingDocument(crop, buyingStationCode, farmerCode, buyingDocumentNumber);
        }

        public List<sp_GetCalculationPercentOfSold_Result> sp_GetCalculationPercentOfSold(short crop, string farmerCode)
        {
            return StoreProcedureRepository.sp_GetCalculationPercentOfSold(crop, farmerCode);
        }

        public List<sp_GetFarmerQuotaFixedAndSold_Result> sp_GetFarmerQuotaFixedAndSold(short crop, string farmerCode)
        {
            return StoreProcedureRepository.sp_GetFarmerQuotaFixedAndSold(crop, farmerCode);
        }

        public List<sp_GetMoistureResultDetails_Result> sp_GetMoistureResultDetails()
        {
            return StoreProcedureRepository.sp_GetMoistureResultDetails();
        }

        public List<sp_GetTransportationDetailsByTransportationCode_Result> sp_GetTransportationDetailsByTransportationCode(string transportationDocumentCode)
        {
            return StoreProcedureRepository.sp_GetTransportationDetailsByTransportationCode(transportationDocumentCode);
        }

        public List<sp_GetTransportationDocumentByTransportationCode_Result> sp_GetTransportationDocumentByTransportationCode(string transportationDocumentCode)
        {
            return StoreProcedureRepository.sp_GetTransportationDocumentByTransportationCode(transportationDocumentCode);
        }
    }
}
