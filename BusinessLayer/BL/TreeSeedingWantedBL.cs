﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface ITreeSeedingWantedBL
    {
        void Add(TreeSeedlingWanted model);
        void Update(TreeSeedlingWanted model);
        void Delete(TreeSeedlingWanted model);
        TreeSeedlingWanted GetByID(short crop, string citizenID, Guid treeSeedingTypeID);
        List<TreeSeedlingWanted> GetByCitizenID(short crop, string citizenID);
    }

    public class TreeSeedingWantedBL : ITreeSeedingWantedBL
    {
        UnitOfWork uow;
        public TreeSeedingWantedBL()
        {
            uow = new UnitOfWork();
        }
        public void Add(TreeSeedlingWanted model)
        {
            try
            {
                if (model.CitizenID == null)
                    throw new ArgumentException("You don't specify Citizen ID.");

                if (model.TreeSeedingTypeID == null)
                    throw new ArgumentException("You don't specify TreeSeedingTypeID.");

                if (model.ExpectingPurpose == null)
                    throw new ArgumentException("You don't specify ExpectingPurpose.");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("You don't specify Modified User.");

                model.ModifiedDate = DateTime.Now;

                uow.TreeSeedlingWantedRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Update(TreeSeedlingWanted model)
        {
            try
            {
                if (model.CitizenID == null)
                    throw new ArgumentException("You don't specify Citizen ID.");

                if (model.TreeSeedingTypeID == null)
                    throw new ArgumentException("You don't specify TreeSeedingTypeID.");

                if (model.ExpectingPurpose == null)
                    throw new ArgumentException("You don't specify ExpectingPurpose.");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("You don't specify Modified User.");

                model.ModifiedDate = DateTime.Now;

                if (GetByID(model.Crop, model.CitizenID, model.TreeSeedingTypeID) == null)
                    throw new ArgumentException("Find by ID not found.");

                uow.TreeSeedlingWantedRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(TreeSeedlingWanted model)
        {
            try
            {
                if (model.CitizenID == null)
                    throw new ArgumentException("You don't specify Citizen ID.");

                if (model.TreeSeedingTypeID == null)
                    throw new ArgumentException("You don't specify TreeSeedingTypeID.");

                if (GetByID(model.Crop, model.CitizenID, model.TreeSeedingTypeID) == null)
                    throw new ArgumentException("Find by ID not found.");

                uow.TreeSeedlingWantedRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public TreeSeedlingWanted GetByID(short crop, string citizenID, Guid treeSeedingTypeID)
        {
            return uow.TreeSeedlingWantedRepository
                       .GetSingle(t => t.Crop == crop &&
                       t.CitizenID == citizenID &&
                       t.TreeSeedingTypeID == treeSeedingTypeID);
        }
        public List<TreeSeedlingWanted> GetByCitizenID(short crop, string citizenID)
        {
            return uow.TreeSeedlingWantedRepository
                      .Query(t => t.Crop == crop &&
                      t.CitizenID == citizenID)
                      .ToList();
        }
    }
}
