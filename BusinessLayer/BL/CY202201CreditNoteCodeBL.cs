﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface ICY202201CreditNoteCodeBL
    {
        string Add(short crop, string farmerCode, DateTime createDate, string description, string createUser);
        void Delete(string creditNoteCode);
        void LockPrint(string creditNoteCode, string printBy);
        void UnLockPrint(string creditNoteCode);
        CY202201CreditNoteCode GetSingle(string creditNoteCode);
        CY202201CreditNoteCode GetByFarmer(short crop, string farmerCode);
        bool ReturnBasketPass(short crop, string farmerCode);
        bool ReturnCPAPass(short crop, string farmerCode);
        List<CY202201ReturnItem> GetReceiveItemByFarmer(short crop, string farmerCode);
        List<CY202201ReturnItem> GetReturnItemByFarmer(short crop, string farmerCode);
    }

    public class CY202201CreditNoteCodeBL : ICY202201CreditNoteCodeBL
    {
        UnitOfWork uow;
        public CY202201CreditNoteCodeBL()
        {
            uow = new UnitOfWork();
        }

        public string Add(short crop, string farmerCode, DateTime createDate, string description, string createUser)
        {
            try
            {
                if (string.IsNullOrEmpty(createUser))
                    throw new ArgumentException("โปรดระบุ createUser");

                var returnNoList = BuyingFacade.CY202201ReturnBL().GetByFarmerCode(crop, farmerCode);
                if (returnNoList.Where(x => x.IsFinish == false).Count() > 0)
                    throw new ArgumentException("มี ReturnNo บางรายการที่ยังไม่ได้ Finish จึงไม่สามารถออกใบลดหนี้ได้");

                if (ReturnBasketPass(crop, farmerCode) == false)
                    throw new ArgumentException("จำนวนตะกร้าที่นำมาคืน น้อยกว่าจำนวนที่ได้รับ");

                if (ReturnCPAPass(crop, farmerCode) == false)
                    throw new ArgumentException("จำนวนขยะสารเเคมีที่นำมาคืน น้อยกว่าจำนวนที่ได้รับ");

                var creditNoteCode = crop + "-" + farmerCode;
                uow.CY202201CreditNoteCodeRepository
                    .Add(new CY202201CreditNoteCode
                    {
                        CreditNoteCode = creditNoteCode,
                        Crop = crop,
                        FarmerCode = farmerCode,
                        CreateBy = createUser,
                        CreateDate = createDate.Add(DateTime.Now.TimeOfDay),
                        IsPrinted = false,
                        PrintDate = null,
                        PrintBy = null,
                        Description = description
                    });
                uow.Save();
                return creditNoteCode;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string creditNoteCode)
        {
            try
            {
                var model = GetSingle(creditNoteCode);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                var documentList = BuyingFacade.BuyingDocumentBL()
                    .GetByFarmer(model.Crop, model.FarmerCode);
                if (documentList.Count() > 0)
                    throw new ArgumentException("ไม่อนุญาตให้ลบข้อมูลเนื่องจากมีการเริ่มบันทึกข้อมูลการขายไปแล้ว");

                uow.CY202201CreditNoteCodeRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CY202201ReturnItem> GetReceiveItemByFarmer(short crop, string farmerCode)
        {
            try
            {
                var fromInvoiceList = uow.InvoiceDetailRepository
                    .Get(x => x.Invoice.Crop == crop &&
                    x.Invoice.FarmerCode == farmerCode,
                    null,
                    x => x.CropInputsCondition)
                    .Select(x => new
                    {
                        ItemID = x.CropInputsCondition.ItemID
                    })
                    .ToList();

                var returnItemDefineList = uow.CY202201ReturnItemRepository.Get();
                var receiveList = (from a in fromInvoiceList
                                   from b in returnItemDefineList
                                   where a.ItemID == b.ReturnItemID
                                   select new CY202201ReturnItem
                                   {
                                       ReturnItemID = a.ItemID,
                                       ItemType = b.ItemType
                                   })
                                  .ToList();
                return receiveList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CY202201ReturnItem> GetReturnItemByFarmer(short crop, string farmerCode)
        {
            try
            {
                var returnItemDefineList = uow.CY202201ReturnItemRepository.Get();
                var returnDetailList = BuyingFacade.CY202201ReturnDetailBL().GetByFarmer(crop, farmerCode);
                var returnList = (from a in returnDetailList
                                  from b in returnItemDefineList
                                  where a.ReturnItemID == b.ReturnItemID
                                  select new CY202201ReturnItem
                                  {
                                      ReturnItemID = a.ReturnItemID,
                                      ItemType = b.ItemType
                                  })
                                  .ToList();

                return returnList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public CY202201CreditNoteCode GetSingle(string creditNoteCode)
        {
            return uow.CY202201CreditNoteCodeRepository.GetSingle(x => x.CreditNoteCode == creditNoteCode);
        }

        public CY202201CreditNoteCode GetByFarmer(short crop, string farmerCode)
        {
            return uow.CY202201CreditNoteCodeRepository.GetSingle(x => x.Crop == crop && x.FarmerCode == farmerCode);
        }

        public void LockPrint(string creditNoteCode, string printBy)
        {
            try
            {
                var master = GetSingle(creditNoteCode);
                if (master == null)
                    throw new ArgumentException("ไม่พบ credit note code นี้ในระบบ");

                if (master.IsPrinted == true)
                    throw new ArgumentException("ใบลดหนี้นี้ถูกยืนยันข้อมูลไปแล้ว ไม่สามารถแก้ไขได้อีก");

                if (ReturnBasketPass(master.Crop, master.FarmerCode) == false)
                    throw new ArgumentException("จำนวนตะกร้าที่นำมาคืน น้อยกว่าจำนวนที่ได้รับ");

                if (ReturnCPAPass(master.Crop, master.FarmerCode) == false)
                    throw new ArgumentException("จำนวนขยะสารเเคมีที่นำมาคืน น้อยกว่าจำนวนที่ได้รับ");

                master.IsPrinted = true;
                master.PrintBy = printBy;
                master.PrintDate = DateTime.Now;

                uow.CY202201CreditNoteCodeRepository.Update(master);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ReturnBasketPass(short crop, string farmerCode)
        {
            try
            {
                var basketReceive = GetReceiveItemByFarmer(crop, farmerCode)
                    .Where(x => x.ItemType == "ตะกร้า").Count();
                var basketReturn = GetReturnItemByFarmer(crop, farmerCode)
                    .Where(x => x.ItemType == "ตะกร้า").Count();
                if (basketReturn < basketReceive)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ReturnCPAPass(short crop, string farmerCode)
        {
            try
            {
                var cpaReceive = GetReceiveItemByFarmer(crop, farmerCode)
                    .Where(x => x.ItemType == "ขยะสารเคมี").Count();
                var cpaReturn = GetReturnItemByFarmer(crop, farmerCode)
                    .Where(x => x.ItemType == "ขยะสารเคมี").Count();
                if (cpaReturn < cpaReceive)
                    return false;
                else
                    return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UnLockPrint(string creditNoteCode)
        {
            try
            {
                var item = GetSingle(creditNoteCode);
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูล credit note code นี้ในระบบ");

                item.IsPrinted = false;
                item.PrintDate = DateTime.Now;

                uow.CY202201CreditNoteCodeRepository.Update(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
