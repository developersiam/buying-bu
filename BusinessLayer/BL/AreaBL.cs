﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IAreaBL
    {
        List<Area> GetAll();
    }

    public class AreaBL : IAreaBL
    {
        UnitOfWork uow;
        public AreaBL()
        {
            uow = new UnitOfWork();
        }

        public List<Area> GetAll()
        {
            return uow.AreaRepository.Query().ToList().OrderBy(x => x.AreaCode).ToList();
        }
    }
}
