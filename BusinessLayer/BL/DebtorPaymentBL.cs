﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLayer.Model;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IDebtorPaymentBL
    {
        void DeductionFromBuying(m_BuyingDocument buyingDocumentInfo,
            string deductionUser,
            List<m_ReceiveSeedAndMedia> receiveSeedAndMediaInfoList,
            List<m_ReceiveInputFactor> receiveInputFactorInfoList,
            decimal totalPriceOfBuyingDocument);
        void DeductFromBuyingVersionCY2018(DebtorPayment value);
        void DeductionFromBuyingVersionCY2019(m_BuyingDocument model, string username);
        List<DebtorPayment> GetByFarmerAndCrop(short crop, string farmerCode);
        DebtorPayment GetById(short paymentCrop, string paymentFarmerCode, short paymentSequence);
        List<DebtorPayment> GetByBuyingDocument(m_BuyingDocument buyingDocumentInfo);
        List<m_PaymentForInputFactor> GetPaymentForInputFactor(short crop, string farmerCode);
        List<m_PaymentForSeedAndMedia> GetPaymentForSeedAndMedia(short crop, string farmerCode);
        decimal GetTotalDebtByCropAndFarmerCode(short crop, string farmerCode);
        void IsPayment(short crop, string farmerCode);
    }

    public class DebtorPaymentBL : IDebtorPaymentBL
    {
        UnitOfWork uow;
        public DebtorPaymentBL()
        {
            uow = new UnitOfWork();
        }

        public void DeductionFromBuying(m_BuyingDocument buyingDocumentInfo,
            string deductionUser,
            List<m_ReceiveSeedAndMedia> receiveSeedList,
            List<m_ReceiveInputFactor> receiveInputFactorList,
            decimal totalPriceOfBuyingDocument)
        {
            try
            {
                // เงื่อนไขในการตรวจสอบข้อมูล
                // 1. จะต้องมียอดค้างชำระ (จำนวนยอดเงินที่ชำระ < จำนวนยอดรวมรายการที่ต้องชำระ)
                // 2. ยอดรวมในใบซื้อนั้นจะต้องพอสำหรับใช้ในการหักเงินชาวไร่

                decimal seedAndMediaAmount = receiveSeedList.Sum(s => s.Price);
                decimal inputFactorAmount = receiveInputFactorList.Sum(ipf => ipf.Price);
                decimal totalDebtAmount = seedAndMediaAmount + inputFactorAmount;///ยอดรวมหนี้ที่ต้องชำระ

                if (buyingDocumentInfo == null)
                    throw new ArgumentException("ไม่พบข้อมูลใบซื้อนี้ในระบบ");

                var debtorPaymentList = uow.DebtorPaymentRepository
                    .Query(dp => dp.PaymentCrop == buyingDocumentInfo.Crop &&
                    dp.PaymentFarmerCode == buyingDocumentInfo.FarmerCode)
                    .ToList();

                int debtorPaymentSequence = 0;

                if (debtorPaymentList.Count > 0)
                    debtorPaymentSequence = debtorPaymentList.Max(dp => dp.PaymentSequence) + 1;
                else
                    debtorPaymentSequence = 1;

                // 1. จะต้องมียอดค้างชำระ (จำนวนยอดเงินที่ชำระ < จำนวนยอดรวมรายการที่ต้องชำระ)      
                decimal totalPayment = debtorPaymentList.Sum(dp => dp.PaymentAmount);

                if (totalPayment >= totalDebtAmount)
                    throw new ArgumentException("ยอดที่ชำระไว้ในระบบ เกินหรือครบตามจำนวนแล้ว (" +
                    debtorPaymentList.Sum(dp => dp.PaymentAmount) + ") ไม่จำเป็นต้องหักเงินชาวไร่รายนี้อีก");

                // 2. ยอดรวมในใบซื้อนั้นจะต้องพอสำหรับใช้ในการหักเงินชาวไร่
                if (totalPriceOfBuyingDocument < totalDebtAmount)
                    throw new ArgumentException("ยอดรวมในใบซื้อนี้ไม่พอชำระ โปรดชำระในครั้งต่อไปหรือปรึกษา ผู้ควบคุมการรับซื้อ ณ ลานรับซื้อ " +
                        Environment.NewLine + "ยอดเงินรวมในการขายครั้งนี้ : " + totalPriceOfBuyingDocument +
                        Environment.NewLine + "ยอดเงินที่ต้องชำระ : " + totalPayment);

                DebtorPayment debtorPayment = new DebtorPayment
                {
                    PaymentCrop = buyingDocumentInfo.Crop,
                    PaymentFarmerCode = buyingDocumentInfo.FarmerCode,
                    PaymentSequence = Convert.ToInt16(debtorPaymentSequence),
                    PaymentAmount = totalDebtAmount,
                    PaymentDate = DateTime.Now,
                    PaymentType = "Buying",
                    PaymentUser = deductionUser,
                    VoucherCrop = buyingDocumentInfo.Crop,
                    VoucherBuyingStationCode = buyingDocumentInfo.BuyingStationCode,
                    VoucherFarmerCode = buyingDocumentInfo.FarmerCode,
                    VoucherBuyingDocumentNumber = buyingDocumentInfo.BuyingDocumentNumber
                };

                uow.DebtorPaymentRepository.Add(debtorPayment);
                uow.Save();

                /// บันทึกรายละเอียดรายการเมล็ดพันธุ์ที่ถูกหักค่าใช้จ่าย
                if (receiveSeedList.Count > 0)
                {
                    foreach (m_ReceiveSeedAndMedia item in receiveSeedList)
                    {
                        uow.PaymentForSeedAndMediaRepository
                            .Add(new PaymentForSeedAndMedia
                            {
                                PaymentCrop = debtorPayment.PaymentCrop,
                                PaymentFarmerCode = debtorPayment.PaymentFarmerCode,
                                PaymentSequence = debtorPayment.PaymentSequence,
                                ReceiveSeedCrop = item.Crop,
                                ReceiveSeedFarmerCode = item.FarmerCode,
                                ReceiveSeedSeedAndMediaID = item.SeedAndMediaID,
                                PaymentForSeedAndMediaDate = DateTime.Now
                            });
                    }
                }

                /// บันทึกรายละเอียดรายการปัจจัยการผลิตที่ถูกหักค่าใช้จ่าย
                if (receiveInputFactorList.Count > 0)
                {
                    foreach (m_ReceiveInputFactor item in receiveInputFactorList)
                    {
                        uow.PaymentForInputFactorRepository
                            .Add(new PaymentForInputFactor
                            {
                                PaymentCrop = debtorPayment.PaymentCrop,
                                PaymentFarmerCode = debtorPayment.PaymentFarmerCode,
                                PaymentSequence = debtorPayment.PaymentSequence,
                                ReceiveInputCrop = item.Crop,
                                ReceiveInputFarmerCode = item.FarmerCode,
                                ReceiveInputFactorID = item.InputFactorID
                            });
                    }
                }

                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeductionFromBuyingVersionCY2019(m_BuyingDocument model, string username)
        {
            try
            {
                // ค่าเมล็ดพันธุ์ที่ต้องชำระ
                var seedCost = uow.SeedDistributionRepository
                    .Query(x => x.Crop == model.Crop &&
                    x.FarmerCode == model.FarmerCode,
                    null,
                    x => x.SeedForDistribution)
                    .Where(x => x.IsDeduction == true &&
                    x.IsPayment == false)
                    .ToList();

                // ค่าปัจจัยการผลิตที่ต้องชำระ
                var farmMaterialCost = uow.CropInputDistributionRepository
                    .Query(x => x.Crop == model.Crop &&
                    x.FarmerCode == model.FarmerCode,
                    null,
                    x => x.CropInputItem)
                    .Where(x => x.IsDeduction == true &&
                    x.IsPayment == false)
                    .ToList();

                // ยอดที่ค้างชำระ
                var waitForDeduction = seedCost.Sum(x => x.Quantity * x.SeedForDistribution.UnitPrice) +
                    farmMaterialCost.Sum(x => x.Quantity * x.CropInputItem.UnitPrice);

                // ยอดรวมการขายครั้งนี้
                var totalPrice = uow.BuyingRepository
                    .Query(x => x.Crop == model.Crop &&
                    x.FarmerCode == model.FarmerCode &&
                    x.BuyingStationCode == model.BuyingStationCode &&
                    x.BuyingDocumentNumber == model.BuyingDocumentNumber,
                    null,
                    x => x.BuyingGrade)
                    .Where(x => x.Grade != null)
                    .Sum(x => x.Weight * x.BuyingGrade.UnitPrice);

                // payment sequence number
                short sequenceNumber = 0;

                var paymentList = GetByFarmerAndCrop(model.Crop, model.FarmerCode);

                if (paymentList.Count < 1)
                    sequenceNumber = 1;
                else
                    sequenceNumber = paymentList.Max(x => x.PaymentSequence);

                if (waitForDeduction <= 0)
                    throw new ArgumentException("ไม่มียอดค้างชำระคงเหลือแล้ว โปรดติดต่อแผนกไอทีเพื่อทำการตรวจสอบข้อผิดพลาด");

                if (totalPrice < waitForDeduction)
                    throw new ArgumentException("ยอดเงินที่ได้จากการขายครั้งนี้ น้อยกว่ายอดที่ค้างชำระ ไม่สามารถหักชำระเงินได้");

                DebtorPayment item = new DebtorPayment
                {
                    PaymentCrop = model.Crop,
                    PaymentFarmerCode = model.FarmerCode,
                    PaymentAmount = waitForDeduction,
                    PaymentDate = DateTime.Now,
                    PaymentType = "Buying",
                    PaymentSequence = sequenceNumber,
                    PaymentUser = username,
                    VoucherCrop = model.Crop,
                    VoucherFarmerCode = model.FarmerCode,
                    VoucherBuyingStationCode = model.BuyingStationCode,
                    VoucherBuyingDocumentNumber = model.BuyingDocumentNumber
                };

                uow.DebtorPaymentRepository.Add(item);

                foreach (var sItem in seedCost)
                {
                    sItem.IsPayment = true;
                    sItem.PaymentDate = DateTime.Now;
                    sItem.PaymentUser = username;
                    uow.SeedDistributionRepository.Update(sItem);
                }
                foreach (var fItem in farmMaterialCost)
                {
                    fItem.IsPayment = true;
                    fItem.PaymentDate = DateTime.Now;
                    fItem.PaymentUser = username;
                    uow.CropInputDistributionRepository.Update(fItem);
                }

                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeductFromBuyingVersionCY2018(DebtorPayment value)
        {
            try
            {
                if (value.PaymentFarmerCode == null || value.PaymentFarmerCode == "")
                    throw new ArgumentException("Farmer code is null");

                if (value.PaymentFarmerCode == null || value.PaymentFarmerCode == "")
                    throw new ArgumentException("Payment user is null");

                if (value.PaymentAmount <= 0)
                    throw new ArgumentException("Payment amount less than zero.");

                uow.DebtorPaymentRepository.Add(value);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DebtorPayment> GetByFarmerAndCrop(short crop, string farmerCode)
        {
            return uow.DebtorPaymentRepository
                .Query(dp => dp.PaymentCrop == crop &&
                dp.PaymentFarmerCode == farmerCode)
                .ToList();
        }

        public DebtorPayment GetById(short paymentCrop, string paymentFarmerCode, short paymentSequence)
        {
            return uow.DebtorPaymentRepository
                .GetSingle(dp => dp.PaymentCrop == paymentCrop &&
                dp.PaymentFarmerCode == paymentFarmerCode &&
                dp.PaymentSequence == paymentSequence);
        }

        public List<DebtorPayment> GetByBuyingDocument(m_BuyingDocument buyingDocumentInfo)
        {
            return uow.DebtorPaymentRepository
                .Query(dp => dp.VoucherCrop == buyingDocumentInfo.Crop &&
                dp.VoucherFarmerCode == buyingDocumentInfo.FarmerCode &&
                dp.VoucherBuyingStationCode == buyingDocumentInfo.BuyingStationCode &&
                dp.VoucherBuyingDocumentNumber == buyingDocumentInfo.BuyingDocumentNumber)
                .ToList();
        }

        public List<m_PaymentForSeedAndMedia> GetPaymentForSeedAndMedia(short crop, string farmerCode)
        {
            try
            {
                return (from ps in uow.PaymentForSeedAndMediaRepository
                        .Query(ps => ps.PaymentCrop == crop &&
                        ps.PaymentFarmerCode == farmerCode,
                        null,
                        ps => ps.ReceiveSeedAndMedia,
                        ps => ps.ReceiveSeedAndMedia.SeedAndMedia)
                        select new m_PaymentForSeedAndMedia
                        {
                            PaymentCrop = ps.PaymentCrop,
                            PaymentFarmerCode = ps.PaymentFarmerCode,
                            PaymentSequence = ps.PaymentSequence,
                            ReceiveSeedCrop = ps.ReceiveSeedCrop,
                            ReceiveSeedFarmerCode = ps.ReceiveSeedFarmerCode,
                            ReceiveSeedSeedAndMediaID = ps.ReceiveSeedSeedAndMediaID,
                            PaymentForSeedAndMediaDate = ps.PaymentForSeedAndMediaDate,
                            Price = ps.ReceiveSeedAndMedia.Quantity * ps.ReceiveSeedAndMedia.SeedAndMedia.UnitPrice,
                            UnitPrice = ps.ReceiveSeedAndMedia.SeedAndMedia.UnitPrice,
                            SeedAndMediaName = ps.ReceiveSeedAndMedia.SeedAndMedia.Name,
                            Quantity = ps.ReceiveSeedAndMedia.Quantity
                        }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<m_PaymentForInputFactor> GetPaymentForInputFactor(short crop, string farmerCode)
        {
            try
            {
                return (from pi in uow.PaymentForInputFactorRepository
                        .Query(pi => pi.PaymentCrop == crop &&
                        pi.PaymentFarmerCode == farmerCode,
                        null,
                        pi => pi.ReceiveInputFactor,
                        pi => pi.ReceiveInputFactor.InputFactor)
                        select new m_PaymentForInputFactor
                        {
                            PaymentCrop = pi.PaymentCrop,
                            PaymentFarmerCode = pi.PaymentFarmerCode,
                            PaymentSequence = pi.PaymentSequence,
                            ReceiveInputCrop = pi.ReceiveInputCrop,
                            ReceiveInputFarmerCode = pi.ReceiveInputFarmerCode,
                            ReceiveInputFactorID = pi.ReceiveInputFactorID,
                            Quantity = pi.ReceiveInputFactor.Quantity,
                            UnitPrice = pi.ReceiveInputFactor.InputFactor.UnitPrice,
                            Price = pi.ReceiveInputFactor.InputFactor.UnitPrice * pi.ReceiveInputFactor.Quantity,
                            InputFactorName = pi.ReceiveInputFactor.InputFactor.Name
                        }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal GetTotalDebtByCropAndFarmerCode(short crop, string farmerCode)
        {
            var debtorPaymentList = uow.DebtorPaymentRepository
                .Query(dp => dp.PaymentCrop == crop && dp.PaymentFarmerCode == farmerCode).ToList();

            List<m_ReceiveInputFactor> a = new List<m_ReceiveInputFactor>();
            List<m_ReceiveSeedAndMedia> b = new List<m_ReceiveSeedAndMedia>();

            a = (from rin in uow.ReceiveInputFactorRepository
                 .Query(ri => ri.Crop == crop &&
                 ri.FarmerCode == farmerCode,
                 null,
                 ri => ri.InputFactor)
                 select new m_ReceiveInputFactor
                 {
                     Crop = rin.Crop,
                     FarmerCode = rin.FarmerCode,
                     InputFactorID = rin.InputFactorID,
                     InputFactorName = rin.InputFactor.Name,
                     UnitPrice = rin.InputFactor.UnitPrice,
                     Quantity = rin.Quantity,
                     Price = rin.Quantity * rin.InputFactor.UnitPrice
                 }).ToList();

            b = (from rs in uow.ReceiveSeedAndMediaRepository
                 .Query(rs => rs.Crop == crop &&
                 rs.FarmerCode == farmerCode,
                 null,
                 rs => rs.SeedVariety,
                 rs => rs.SeedAndMedia)
                 select new m_ReceiveSeedAndMedia
                 {
                     Crop = rs.Crop,
                     FarmerCode = rs.FarmerCode,
                     SeedAndMediaID = rs.SeedAndMediaID,
                     SeedAndMediaName = rs.SeedAndMedia.Name,
                     SeedCode = rs.SeedCode,
                     SeedVariety = rs.SeedVariety,
                     UnitPrice = rs.SeedAndMedia.UnitPrice,
                     Quantity = rs.Quantity,
                     Price = rs.Quantity * rs.SeedAndMedia.UnitPrice
                 }).ToList();

            decimal totalDept = a.Sum(rif => rif.Price) + b.Sum(rs => rs.Price);
            decimal totalDeptPayment = debtorPaymentList.Sum(dp => dp.PaymentAmount);

            return totalDept - totalDeptPayment;
        }

        public void IsPayment(short crop, string farmerCode)
        {
            try
            {
                /// หากมีการหักค่าเมล็ดพันธุ์และ PPE kits ไปแล้วจะไม่ให้จ่ายเพิ่ม แก้ไข หรือลบข้อมูลได้
                /// 
                var _debPayment = uow.DebtorPaymentRepository
                    .Get(x => x.PaymentCrop == crop &&
                    x.PaymentFarmerCode == farmerCode)
                    .ToList();

                if (_debPayment.Count() > 0)
                    throw new ArgumentException("มีการหักชำระหนี้ไปแล้ว ไม่สามารถดำเนินการกับข้อมูลนี้ได้");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
