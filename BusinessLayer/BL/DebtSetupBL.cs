﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IDebtSetupBL
    {
        void Add(short crop, string farmerCode, string[] accountInvoiceNoList,
            string debtTypeCode, int dripProjectQuota, string createBy, DateTime createDate);
        void AddDripProject(short crop, string farmerCode, int dripProjectQuota, string[] invoiceNo, string createUser);
        void AddCropInputs(short crop, string farmerCode, string[] invoiceNo, string createUser);
        void AddLoan(short crop, string farmerCode, int quota, int numberOfDeductionCrop,
            double amount, string createBy, DateTime createDate);
        void SetupCropInputsDebt(short crop, string extensionAgentCode, string username);
        void Delete(string debtSetupCode);
        void EditDeductionPerKg(string debtSetupCode, int newQuota, string editUser);
        decimal GetOutStatdingDebtAmount(short currentCrop, string farmerCode, string debtTypeCode);
        DebtSetup GetSingle(string debtSetupCode);
        DebtSetup GetSingleByDeductionCrop(short deductionCrop, string farmerCode);
        DebtSetup GetCropInputsByCrop(short deductionCrop, string farmerCode);
        List<DebtSetup> GetByFarmer(string farmerCode);
        List<DebtSetup> GetByFarmerDeductionCrop(short deductionCrop, string farmerCode);
        List<DebtSetup> GetByFarmerSetupCrop(short setupCrop, string farmerCode);
        List<DebtSetup> GetByExtensionAgentDeductionCrop(short dedctionCrop, string extensionAgentCode);
        List<DebtSetup> GetByExtensionAgentSetupCrop(short setupCrop, string extensionAgentCode);
        List<DebtSetup> GetDripProjectByCurrentCrop(short currentCrop, string farmerCode);
        List<DebtSetup> GetDripProjectBy3YearConntinue(short currentCrop, string farmerCode);
    }

    public class DebtSetupBL : IDebtSetupBL
    {
        UnitOfWork uow;
        public DebtSetupBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(short crop, string farmerCode, string[] accountInvoiceNoList, string debtTypeCode,
            int dripProjectQuota, string createBy, DateTime createDate)
        {
            try
            {
                var debtAccList = BuyingFacade.DebtSetupAccountInvoiceBL()
                    .GetByFarmer(crop, farmerCode);

                foreach (var item in debtAccList)
                    if (accountInvoiceNoList
                        .Where(x => x == item.AccountInvoiceNo)
                        .Count() > 0)
                        throw new ArgumentException(item.AccountInvoiceNo + " ถูกนำไปตั้งหนี้แล้ว");

                // account invoice ที่ถูกนำไปทำลดหนี้แล้ว จะไม่สามารถเอามาทำตั้งหนี้ได้
                // account invoice ที่ถูกนำไปตั้งหนี้แล้ว จะไม่สามารถเอามาทำลดหนี้ได้
                var accountInvoiceList = BuyingFacade.AccountInvoiceBL()
                    .GetByFarmerWithOutCreditNote(crop, farmerCode);

                if (accountInvoiceList.Count <= 0)
                    throw new ArgumentException("ไม่พบข้อมูล account invoice ของชาวไร่รหัส " + farmerCode + " ในปี " + crop);

                var register = BuyingFacade.RegistrationBL()
                    .GetSingle(crop, farmerCode);

                if (register == null)
                    throw new ArgumentException("ไม่พบข้อมูลการทำสัญญาของชาวไร่รหัส " + farmerCode + " ในปี " + crop);

                if (register.ActiveStatus == false)
                    throw new ArgumentException("ชาวไร่รายนี้ยกเลิกสัญญาเนื่องจาก " + register.UnRegisterReason);

                var buyingDocumentList = BuyingFacade.BuyingDocumentBL()
                    .GetByFarmer(crop, farmerCode);

                if (buyingDocumentList.Count() > 0)
                    throw new ArgumentException("ชาวไร่รายนี้ได้เริ่มต้นทำการซื้อขายในระบบใน crop นี้ไปแล้ว ไม่สามารถตั้งหนี้ได้");

                if (string.IsNullOrEmpty(debtTypeCode))
                    throw new ArgumentException("โปรดระบุประเภทการตั้งหนี้ ปัจจัยการผลิต/โครงการน้ำหยด/โครงการเงินกู้");

                //คำนวณยอดรวมจาก account invoice ที่ระบุเพื่อนำมาตั้งเป็นยอดหนี้
                double totalAmount = 0.0;
                foreach (var item in accountInvoiceNoList)
                {
                    if (accountInvoiceList
                        .Where(x => x.InvoiceNo == item)
                        .Count() > 0)
                    {
                        totalAmount = totalAmount +
                            (double)accountInvoiceList
                            .Where(x => x.InvoiceNo == item)
                            .Sum(x => x.InvoiceDetails
                            .Sum(y => y.Quantity * y.UnitPrice));
                    }
                }

                if (totalAmount <= 0)
                    throw new ArgumentException("ยอดรวมจาก invoice ที่เลือกจะต้องมากกว่า 0 จึงจะสามารถนำยอดดังกล่าวมาตั้งหนี้ได้");

                if (dripProjectQuota <= 0 && debtTypeCode == "DP")
                    throw new ArgumentException("หนี้โครงการน้ำหยดจะต้องระบุจำนวนโควต้าโครงการน้ำหยดด้วย");

                // ตรวจสอบหนี้เก่า
                var ountStandingDebt = GetOutStatdingDebtAmount(crop, farmerCode, debtTypeCode);
                if (ountStandingDebt > 0)
                    throw new Exception("ชาวไร่รหัส " + farmerCode + " " +
                        "มีหนี้เก่าที่ค้างชำระอยู่จำนวน " + ountStandingDebt.ToString("N2") + " " +
                        "บาท ต้องชำระหนี้เก่าให้หมดก่อน");

                //var debtSetupListByDeductionCrop = uow.DebtSetupRepository
                //    .Get(x => x.DeductionCrop < crop &&
                //    x.FarmerCode == farmerCode &&
                //    x.DebtTypeCode != "L1"
                //    , null
                //    , x => x.DebtReceipts);
                //var debtAmount = debtSetupListByDeductionCrop.Sum(x => x.Amount);
                //var receiptAmount = debtSetupListByDeductionCrop.Sum(x => x.DebtReceipts.Sum(y => y.Amount));
                //var debtBalance = debtAmount - receiptAmount;
                //if (debtBalance > 0)
                //    throw new ArgumentException("ยังมียอดหนี้ก่อนหน้านี้ที่ยังค้างชำระ ไม่สามารถตั้งหนี้ใหม่ได้");

                // ตรวจสอบหนี้ 3 ประเภท ว่ามีการนำไปตั้งหนี้แล้วหรือไม่ (ห้ามซ้ำ)
                // นับจากปีที่ตั้งหนี้คู่กับ debt type code
                // หนี้น้ำหยดมีได้ 3 ก้อน โดยนับจากปีที่ตั้งหนี้
                // หนี้ปัจจัยการผลิตมีได้ 1 ก้อน โดยนับจากปีที่ตั้งหนี้
                // หนี้เงินกู้มีได้ 5 ก้อน โดยนับจากปีที่ตั้งหนี้
                var debtSetupsBycurrentCrop = uow.DebtSetupRepository
                    .Get(x => x.Crop == crop &&
                    x.FarmerCode == farmerCode &&
                    x.DebtTypeCode != "L1"
                    , null
                    , x => x.DebtReceipts);
                var dripProjectList = debtSetupsBycurrentCrop
                    .Where(x => x.DebtTypeCode == "DP")
                    .ToList();
                var cropInputsList = debtSetupsBycurrentCrop
                    .Where(x => x.DebtTypeCode == "CI")
                    .ToList();

                if (dripProjectList.Count() >= 3 && debtTypeCode == "DP")
                    throw new ArgumentException("หนี้ประเภท DP จะตั้งยอดหนี้ได้ไม่เกิน 3 รายการต่อปี");

                if (cropInputsList.Count() >= 1 && debtTypeCode == "CI")
                    throw new ArgumentException("หนี้ประเภท CI จะตั้งยอดหนี้ได้ไม่เกิน 1 รายการต่อปี");

                if (debtTypeCode == "DP")
                {
                    short numberOfYear = 3;
                    double deductPerYear = totalAmount / numberOfYear;
                    double deductPerKg = deductPerYear / (dripProjectQuota * 0.5);

                    for (short i = crop; i < crop + numberOfYear; i++)
                    {
                        var code = GenerateCode(crop, farmerCode);
                        uow.DebtSetupRepository
                            .Add(new DebtSetup
                            {
                                DebtSetupCode = code,
                                DebtTypeCode = debtTypeCode,
                                Crop = crop,
                                FarmerCode = farmerCode,
                                Amount = (decimal)deductPerYear,
                                DeductPerKg = (decimal)deductPerKg,
                                //DripProjectQuota
                                //เนื่องจากเป็นการระบุโควต้าหน่วยเป็น kg. เหมือนกันเลยใช้คอลัมน์เดียวกัน
                                //อนาคตหากมีเวลาอาจเพิ่มอีก 1 column เพื่อเก็บแยกโดยเฉพาะหรือเปลี่ยนชื่อ column ให้สอดคล้อง
                                DripProjectQuota = dripProjectQuota,
                                DeductionCrop = i,
                                CreateBy = createBy,
                                CreateDate = createDate,
                                ModifiedBy = createBy,
                                ModifiedDate = DateTime.Now
                            });

                        foreach (var item in accountInvoiceNoList)
                            uow.DebtSetupAccountInvoiceRepository
                                .Add(new DebtSetupAccountInvoice
                                {
                                    DebtSetupCode = code,
                                    AccountInvoiceNo = item,
                                    AssignBy = createBy,
                                    AssignDate = createDate
                                });

                        uow.Save();
                    }
                }
                else
                {
                    var code = GenerateCode(crop, farmerCode);
                    uow.DebtSetupRepository
                        .Add(new DebtSetup
                        {
                            DebtSetupCode = code,
                            DebtTypeCode = debtTypeCode,
                            Crop = crop,
                            FarmerCode = farmerCode,
                            Amount = (decimal)totalAmount,
                            DeductPerKg = (decimal)0.0,
                            //DripProjectQuota
                            //เนื่องจากเป็นการระบุโควต้าหน่วยเป็น kg. เหมือนกันเลยใช้คอลัมน์เดียวกัน
                            //อนาคตหากมีเวลาอาจเพิ่มอีก 1 column เพื่อเก็บแยกโดยเฉพาะหรือเปลี่ยนชื่อ column ให้สอดคล้อง
                            DripProjectQuota = 0,
                            DeductionCrop = crop,
                            CreateBy = createBy,
                            CreateDate = createDate,
                            ModifiedBy = createBy,
                            ModifiedDate = DateTime.Now
                        });

                    foreach (var item in accountInvoiceNoList)
                        uow.DebtSetupAccountInvoiceRepository
                            .Add(new DebtSetupAccountInvoice
                            {
                                DebtSetupCode = code,
                                AccountInvoiceNo = item,
                                AssignBy = createBy,
                                AssignDate = createDate
                            });

                    uow.Save();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddLoan(short crop, string farmerCode, int quota, int numberOfDeductionCrop,
            double amount, string createBy, DateTime createDate)
        {
            try
            {
                // Prevent case from user input a lowwer text on the farmer code text input.
                farmerCode = farmerCode.ToUpper();

                if (quota <= 0)
                    throw new ArgumentException("โปรดระบุโควต้าที่ลงทะเบียนเพื่อนำไปคำนวณยอดที่จะหักในแต่ละปี");

                if (numberOfDeductionCrop < 0)
                    throw new ArgumentException("โครงการเงินกู้จะต้องระบุจำนวนปีที่จะหักชำระอย่างน้อย 1 ปีขึ้นไป");

                if (amount <= 0)
                    throw new ArgumentException("ยอดหนี้จะต้องมากกว่า 0");

                var register = BuyingFacade.RegistrationBL()
                    .GetSingle(crop, farmerCode);

                if (register == null)
                    throw new ArgumentException("ไม่พบข้อมูลการทำสัญญาของชาวไร่รหัส " + farmerCode + " ในปี " + crop);

                if (register.ActiveStatus == false)
                    throw new ArgumentException("ชาวไร่รายนี้ยกเลิกสัญญาเนื่องจาก " + register.UnRegisterReason);

                var buyingDocumentList = BuyingFacade.BuyingDocumentBL()
                    .GetByFarmer(crop, farmerCode);

                if (buyingDocumentList.Count() > 0)
                    throw new ArgumentException("ชาวไร่รายนี้ได้เริ่มต้นทำการซื้อขายในระบบใน crop นี้ไปแล้ว ไม่สามารถตั้งหนี้ได้");

                // ตรวจสอบหนี้เก่า
                // crop = default crop.
                //เลือกมาตามประเภทหนี้ (DebtTypeCode) ที่ต้องการบันทึกการตั้งหนี้ในครั้งนี้
                var outStandingDebtList = uow.DebtSetupRepository
                    .Get(x => x.Crop < crop &&
                    x.FarmerCode == farmerCode &&
                    x.DebtTypeCode == "L1" &&
                    x.Amount - x.DebtReceipts.Sum(y => y.Amount) > 0)
                    .ToList();
                var ountStandingDebtBalance = outStandingDebtList.Sum(x => x.Amount) - outStandingDebtList.Sum(x => x.DebtReceipts.Sum(y => y.Amount));
                if (ountStandingDebtBalance > 0)
                    throw new Exception("ชาวไร่รหัส " + farmerCode + " มีหนี้เก่าที่ค้างชำระอยู่จำนวน " + ountStandingDebtBalance.ToString("N2") + " บาท ต้องชำระหนี้เก่าให้หมดก่อน");

                // ตรวจสอบหนี้ 3 ประเภท ว่ามีการนำไปตั้งหนี้แล้วหรือไม่ (ห้ามซ้ำ)
                // นับจากปีที่ตั้งหนี้คู่กับ debt type code
                // หนี้น้ำหยด 3 รายการ
                // หนี้ปัจจัยการผลิต 1 รายการ
                // หนี้เงินกู้ 5 รายการ
                var loanDebtList = uow.DebtSetupRepository
                    .Get(x => x.Crop == crop &&
                    x.FarmerCode == farmerCode &&
                    x.DebtTypeCode == "L1"
                    , null
                    , x => x.DebtReceipts);

                if (loanDebtList.Count() >= 5)
                    throw new ArgumentException("หนี้ประเภท L1 จะตั้งยอดหนี้ได้ไม่เกิน 5 รายการต่อปี");

                double deductPerYear = amount / numberOfDeductionCrop;
                double deductPerKg = deductPerYear / (quota * 0.5);

                for (short i = crop; i < crop + numberOfDeductionCrop; i++)
                {
                    var code = GenerateCode(crop, farmerCode);
                    uow.DebtSetupRepository
                        .Add(new DebtSetup
                        {
                            DebtSetupCode = code,
                            DebtTypeCode = "L1",
                            Crop = crop,
                            FarmerCode = farmerCode,
                            Amount = (decimal)deductPerYear,
                            DeductPerKg = (decimal)deductPerKg,
                            //DripProjectQuota
                            //เนื่องจากเป็นการระบุโควต้าหน่วยเป็น kg. เหมือนกันเลยใช้คอลัมน์เดียวกัน
                            //อนาคตหากมีเวลาอาจเพิ่มอีก 1 column เพื่อเก็บแยกโดยเฉพาะหรือเปลี่ยนชื่อ column ให้สอดคล้อง
                            DripProjectQuota = quota,
                            DeductionCrop = i,
                            CreateBy = createBy,
                            CreateDate = createDate,
                            ModifiedBy = createBy,
                            ModifiedDate = DateTime.Now
                        });

                    uow.Save();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EditDeductionPerKg(string debtSetupCode, int newQuota, string editUser)
        {
            try
            {
                var debtSetup = uow.DebtSetupRepository
                    .GetSingle(x => x.DebtSetupCode == debtSetupCode
                    , x => x.DebtReceipts);
                if (debtSetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล debt setup code " +
                        debtSetupCode + " นี้ในระบบ");

                if (debtSetup.DebtReceipts.Count() > 0)
                    throw new ArgumentException("ไม่สามารถแก้ไข deduction per kg ได้" +
                        " เนื่องจากมีการหักชำระหนี้ในยอดหนี้นี้ไปบางส่วนแล้ว");

                var deductionPerKg = debtSetup.Amount / (newQuota * (decimal)0.5);
                debtSetup.DripProjectQuota = newQuota;
                debtSetup.DeductPerKg = deductionPerKg;
                debtSetup.ModifiedDate = DateTime.Now;
                debtSetup.ModifiedBy = editUser;

                uow.DebtSetupRepository.Update(debtSetup);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string debtSetupCode)
        {
            try
            {
                var debtSetup = uow.DebtSetupRepository
                    .GetSingle(x => x.DebtSetupCode == debtSetupCode
                    , x => x.DebtReceipts);
                if (debtSetup == null)
                    throw new ArgumentException("ไม่พบข้อมูลการตั้งหนี้รหัส " + debtSetupCode);

                if (debtSetup.DebtReceipts.Sum(x => x.Amount) > 0)
                    throw new ArgumentException("มีการชำระเงินในยอดหนี้นี้ไปแล้ว ไม่สามารถลบข้อมูลได้");

                var buyingDocumentList = BuyingFacade.BuyingDocumentBL()
                    .GetByFarmer(debtSetup.Crop, debtSetup.FarmerCode);
                if (buyingDocumentList.Count() > 0)
                    throw new ArgumentException("ชาวไร่รายนี้เริ่มทำการซื้อขายใบยาไปแล้วในฤดูปลูกนี้ ไม่สามารถลบข้อมูลได้");

                if (debtSetup.DebtTypeCode == "DP")
                {
                    var debtSetupList = uow.DebtSetupRepository
                        .Get(x => x.DebtTypeCode == "DP" &&
                        x.Crop == debtSetup.Crop &&
                        x.FarmerCode == debtSetup.FarmerCode
                        , null
                        , x => x.DebtReceipts
                        , x => x.DebtSetupAccountInvoices)
                        .ToList();

                    var debtReceiptAmt = debtSetupList
                        .Sum(x => x.DebtReceipts.Sum(y => y.Amount));
                    if (debtReceiptAmt > 0)
                        throw new Exception("มีการชำระหนี้บางส่วนในโครงการน้ำหยดไปแล้วเป็นจำนวน " +
                            debtReceiptAmt + " บาท ไม่สามารถลบได้");

                    //Delete in an association table.
                    var associationList = new List<DebtSetupAccountInvoice>();
                    foreach (var item in debtSetupList)
                        if (item.DebtSetupAccountInvoices.Count() > 0)
                            associationList.AddRange(item.DebtSetupAccountInvoices);

                    uow.DebtSetupAccountInvoiceRepository.Remove(associationList.ToArray());
                    uow.Save();

                    //Delete in a main table.
                    uow.DebtSetupRepository.Remove(debtSetupList.ToArray());
                    uow.Save();
                }
                else if (debtSetup.DebtSetupCode == "L1")
                {
                    var debtSetupList = uow.DebtSetupRepository
                        .Get(x => x.DebtTypeCode == "L1" &&
                        x.Crop == debtSetup.Crop &&
                        x.FarmerCode == debtSetup.FarmerCode
                        , null
                        , x => x.DebtReceipts
                        , x => x.DebtSetupAccountInvoices)
                        .ToList();

                    var debtReceiptAmt = debtSetupList
                        .Sum(x => x.DebtReceipts.Sum(y => y.Amount));
                    if (debtReceiptAmt > 0)
                        throw new Exception("มีการชำระหนี้บางส่วนในเงินกู้ลงทุนเพาะปลูกผูกพัน 5 ปีไปแล้วเป็นจำนวน " +
                            debtReceiptAmt + " บาท ไม่สามารถลบได้");

                    //Delete in an association table.
                    var associationList = new List<DebtSetupAccountInvoice>();
                    foreach (var item in debtSetupList)
                        if (item.DebtSetupAccountInvoices.Count() > 0)
                            associationList.AddRange(item.DebtSetupAccountInvoices);
                    uow.DebtSetupAccountInvoiceRepository
                        .Remove(associationList.ToArray());
                    uow.Save();

                    //Delete in a main table.
                    uow.DebtSetupRepository.Remove(debtSetupList.ToArray());
                    uow.Save();
                }
                else
                {
                    var associationList = uow.DebtSetupAccountInvoiceRepository
                        .Get(x => x.DebtSetupCode == debtSetupCode);
                    //Delete in an association table.
                    uow.DebtSetupAccountInvoiceRepository.Remove(associationList.ToArray());

                    //Delete in a main table.
                    uow.DebtSetupRepository.Remove(debtSetup);
                    uow.Save();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DebtSetup> GetDripProjectBy3YearConntinue(short currentCrop, string farmerCode)
        {
            /*
             *DedcutionCrop = {2023, 2024, 2025}
             *SetupCrop     = {2023, 2023, 2023}
             *
             *e.g.
             *CurrentCrop   = 2023 return {2023, 2024, 2025}
             *CurrentCrop   = 2024 return {2024, 2025}
             *CurrentCrop   = 2025 return {2025}
            */
            return uow.DebtSetupRepository
                .Get(x => x.DeductionCrop >= currentCrop &&
                x.DeductionCrop <= x.Crop + 2 &&
                x.FarmerCode == farmerCode &&
                x.DebtTypeCode == "DP"
                , null
                , x => x.DebtType
                , x => x.DebtReceipts);
        }

        public List<DebtSetup> GetDripProjectByCurrentCrop(short currentCrop, string farmerCode)
        {
            return uow.DebtSetupRepository
                .Get(x => x.DeductionCrop == currentCrop &&
                x.FarmerCode == farmerCode &&
                x.DebtTypeCode == "DP"
                , null
                , x => x.DebtType);
        }

        public List<DebtSetup> GetByExtensionAgentDeductionCrop(short dedctionCrop, string extensionAgentCode)
        {
            return uow.DebtSetupRepository
                .Get(x => x.DeductionCrop == dedctionCrop &&
                x.RegistrationFarmer.Farmer.Supplier == extensionAgentCode
                , null
                , x => x.DebtType
                , x => x.RegistrationFarmer
                , x => x.RegistrationFarmer.Farmer);
        }

        public List<DebtSetup> GetByFarmerDeductionCrop(short deductionCrop, string farmerCode)
        {
            return uow.DebtSetupRepository.
                Get(x => x.DeductionCrop == deductionCrop
                && x.FarmerCode == farmerCode
                , null
                , x => x.DebtType
                , x => x.DebtReceipts
                , x => x.RegistrationFarmer
                , x => x.RegistrationFarmer.Farmer
                , x => x.RegistrationFarmer.Farmer.Person
                , x => x.DebtSetupAccountInvoices);
        }

        private string GenerateCode(short crop, string farmerCode)
        {
            var debtSetupList = uow.DebtSetupRepository
                            .Get(x => x.Crop == crop && x.FarmerCode == farmerCode)
                            .ToList();
            var max = 0;
            if (debtSetupList.Count() > 0)
                max = debtSetupList
                   .Max(x =>
                   Convert.ToInt16(x.DebtSetupCode
                       .Substring(x.DebtSetupCode.Length - 2, 2)));

            max = max + 1;

            var cropText = crop.ToString();
            var debtSetupCode = "DS" + cropText.Substring(2, 2) + "-" + farmerCode + "-" + max.ToString().PadLeft(2, '0');

            return debtSetupCode;
        }

        public DebtSetup GetSingleByDeductionCrop(short deductionCrop, string farmerCode)
        {
            return uow.DebtSetupRepository
                .GetSingle(x => x.DeductionCrop == deductionCrop &&
                x.FarmerCode == farmerCode
                , x => x.DebtType);
        }

        public DebtSetup GetSingle(string debtSetupCode)
        {
            return uow.DebtSetupRepository
                .GetSingle(x => x.DebtSetupCode == debtSetupCode
                , x => x.DebtType);
        }

        public DebtSetup GetCropInputsByCrop(short deductionCrop, string farmerCode)
        {
            return uow.DebtSetupRepository
                .GetSingle(x => x.Crop == deductionCrop &&
                x.FarmerCode == farmerCode &&
                x.DebtTypeCode == "CI");
        }

        public List<DebtSetup> GetByExtensionAgentSetupCrop(short setupCrop, string extensionAgentCode)
        {
            return uow.DebtSetupRepository
                .Get(x => x.Crop == setupCrop &&
                x.RegistrationFarmer.Farmer.Supplier == extensionAgentCode
                , null
                , x => x.DebtType
                , x => x.RegistrationFarmer
                , x => x.RegistrationFarmer.Farmer);
        }

        public List<DebtSetup> GetByFarmerSetupCrop(short setupCrop, string farmerCode)
        {
            return uow.DebtSetupRepository.
                Get(x => x.Crop == setupCrop
                && x.FarmerCode == farmerCode
                , null
                , x => x.DebtType
                , x => x.RegistrationFarmer
                , x => x.RegistrationFarmer.Farmer
                , x => x.RegistrationFarmer.Farmer.Person
                , x => x.DebtSetupAccountInvoices);
        }

        public List<DebtSetup> GetByFarmer(string farmerCode)
        {
            return uow.DebtSetupRepository
                .Get(x => x.FarmerCode == farmerCode
                , null
                , x => x.DebtType
                , x => x.DebtReceipts);
        }

        public decimal GetOutStatdingDebtAmount(short currentCrop, string farmerCode, string debtTypeCode)
        {
            try
            {
                // ตรวจสอบหนี้เก่า
                //เลือกมาตามประเภทหนี้ (DebtTypeCode) ที่ต้องการบันทึกการตั้งหนี้ในครั้งนี้
                var outStandingDebtList = uow.DebtSetupRepository
                    .Get(x => x.Crop < currentCrop &&
                    x.FarmerCode == farmerCode &&
                    x.DebtTypeCode == debtTypeCode &&
                    x.Amount - x.DebtReceipts.Sum(y => y.Amount) > 0)
                    .ToList();
                decimal ountStandingDebtBalance = outStandingDebtList.Sum(x => x.Amount) -
                    outStandingDebtList.Sum(x => x.DebtReceipts.Sum(y => y.Amount));

                return ountStandingDebtBalance;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddDripProject(short crop, string farmerCode, int dripProjectQuota, string[] invoiceNo, string createUser)
        {
            try
            {
                var debtAccList = BuyingFacade.DebtSetupAccountInvoiceBL()
                    .GetByFarmer(crop, farmerCode);

                foreach (var item in debtAccList)
                    if (invoiceNo
                        .Where(x => x == item.AccountInvoiceNo)
                        .Count() > 0)
                        throw new ArgumentException(item.AccountInvoiceNo + " ถูกนำไปตั้งหนี้แล้ว");

                var accountInvoiceList = new List<AccountInvoice>();
                var creditNoteList = new List<AccountCreditNote>();
                foreach (var item in invoiceNo)
                {
                    var accountInvoice = BuyingFacade.AccountInvoiceBL().GetSingle(item);
                    if (accountInvoice == null)
                        throw new Exception("ไม่พบ invoice " + item + " นี้ในระบบ");

                    accountInvoiceList.Add(accountInvoice);
                    if (accountInvoice.AccountCreditNotes.Count() > 0)
                    {
                        var creditNote = BuyingFacade.AccountCreditNoteBL()
                            .GetSingle(accountInvoice.InvoiceNo);
                        creditNoteList.Add(creditNote);
                    }
                }

                foreach (var item in accountInvoiceList)
                {
                    if (item.Invoice.DebtTypeCode != "DP")
                        throw new Exception("ประเภทของ invoice " + item.InvoiceNo +
                            " ไม่ใช่โครงการน้ำหยด โปรดตรวจสอบกับทางแผนกไอทีอีกครั้ง");
                }

                // account invoice ที่ถูกนำไปทำลดหนี้แล้ว จะไม่สามารถเอามาทำตั้งหนี้ได้
                // account invoice ที่ถูกนำไปตั้งหนี้แล้ว จะไม่สามารถเอามาทำลดหนี้ได้
                if (accountInvoiceList.Count <= 0)
                    throw new ArgumentException("ไม่พบข้อมูล account invoice ของชาวไร่รหัส " + farmerCode + " ในปี " + crop);

                var register = BuyingFacade.RegistrationBL()
                    .GetSingle(crop, farmerCode);

                if (register == null)
                    throw new ArgumentException("ไม่พบข้อมูลการทำสัญญาของชาวไร่รหัส " + farmerCode + " ในปี " + crop);

                if (register.ActiveStatus == false)
                    throw new ArgumentException("ชาวไร่รายนี้ยกเลิกสัญญาเนื่องจาก " + register.UnRegisterReason);

                var buyingDocumentList = BuyingFacade.BuyingDocumentBL()
                    .GetByFarmer(crop, farmerCode);

                if (buyingDocumentList.Count() > 0)
                    throw new ArgumentException("ชาวไร่รายนี้ได้เริ่มต้นทำการซื้อขายในระบบใน crop นี้ไปแล้ว ไม่สามารถตั้งหนี้ได้");

                //คำนวณยอดรวมจาก account invoice ที่ระบุเพื่อนำมาตั้งเป็นยอดหนี้
                double totalInvoiceAmount = (double)accountInvoiceList
                    .Sum(x => x.InvoiceDetails
                    .Sum(y => y.Quantity * y.UnitPrice));

                double totalCreditNoteAmount = (double)creditNoteList
                    .Sum(x => x.AccountCreditNoteDetails
                    .Sum(y => y.Quantity * y.UnitPrice));

                double totalBalanceAmount = totalInvoiceAmount - totalCreditNoteAmount;

                if (totalBalanceAmount <= 0)
                    throw new ArgumentException("ไม่พบหนี้คงเหลือที่ต้องนำมาตั้งหนี้ " +
                        "เนื่องจากมีการลดหนี้ในระบบแล้ว โปรดตรวจสอบข้อมูลอีกครั้ง");

                // ตรวจสอบหนี้เก่า
                var ountStandingDebt = GetOutStatdingDebtAmount(crop, farmerCode, "DP");
                if (ountStandingDebt > 0)
                    throw new Exception("ชาวไร่รหัส " + farmerCode +
                        "มีหนี้เก่าที่ค้างชำระอยู่จำนวน " + ountStandingDebt.ToString("N2") + " " +
                        "บาท ต้องชำระหนี้เก่าให้หมดก่อน");

                // ตรวจสอบหนี้ 3 ประเภท ว่ามีการนำไปตั้งหนี้แล้วหรือไม่ (ห้ามซ้ำ)
                // นับจากปีที่ตั้งหนี้คู่กับ debt type code
                // หนี้น้ำหยดมีได้ 3 ก้อน โดยนับจากปีที่ตั้งหนี้
                // หนี้ปัจจัยการผลิตมีได้ 1 ก้อน โดยนับจากปีที่ตั้งหนี้
                // หนี้เงินกู้มีได้ 5 ก้อน โดยนับจากปีที่ตั้งหนี้
                var dripProjectList = uow.DebtSetupRepository
                    .Get(x => x.Crop == crop &&
                    x.FarmerCode == farmerCode &&
                    x.DebtTypeCode != "DP"
                    , null
                    , x => x.DebtReceipts);

                if (dripProjectList.Count() >= 3)
                    throw new ArgumentException("หนี้ประเภท DP จะตั้งยอดหนี้ได้ไม่เกิน 3 รายการต่อปี");

                short numberOfYear = 3;
                double deductPerYear = totalBalanceAmount / numberOfYear;
                double deductPerKg = deductPerYear / (dripProjectQuota * 0.5);

                for (short i = crop; i < crop + numberOfYear; i++)
                {
                    var code = GenerateCode(crop, farmerCode);
                    uow.DebtSetupRepository
                        .Add(new DebtSetup
                        {
                            DebtSetupCode = code,
                            DebtTypeCode = "DP",
                            Crop = crop,
                            FarmerCode = farmerCode,
                            Amount = (decimal)deductPerYear,
                            DeductPerKg = (decimal)deductPerKg,
                            //DripProjectQuota
                            //เนื่องจากเป็นการระบุโควต้าหน่วยเป็น kg. เหมือนกันเลยใช้คอลัมน์เดียวกัน
                            //อนาคตหากมีเวลาอาจเพิ่มอีก 1 column เพื่อเก็บแยกโดยเฉพาะหรือเปลี่ยนชื่อ column ให้สอดคล้อง
                            DripProjectQuota = dripProjectQuota,
                            DeductionCrop = i,
                            CreateBy = createUser,
                            CreateDate = DateTime.Now,
                            ModifiedBy = createUser,
                            ModifiedDate = DateTime.Now
                        });

                    foreach (var item in invoiceNo)
                        uow.DebtSetupAccountInvoiceRepository
                            .Add(new DebtSetupAccountInvoice
                            {
                                DebtSetupCode = code,
                                AccountInvoiceNo = item,
                                AssignBy = createUser,
                                AssignDate = DateTime.Now
                            });

                    uow.Save();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddCropInputs(short crop, string farmerCode, string[] invoiceNo, string createUser)
        {
            try
            {
                var debtAccList = BuyingFacade.DebtSetupAccountInvoiceBL()
                    .GetByFarmer(crop, farmerCode);

                foreach (var item in debtAccList)
                    if (invoiceNo
                        .Where(x => x == item.AccountInvoiceNo)
                        .Count() > 0)
                        throw new ArgumentException(item.AccountInvoiceNo + " ถูกนำไปตั้งหนี้แล้ว");

                var accountInvoiceList = new List<AccountInvoice>();
                var creditNoteList = new List<AccountCreditNote>();
                foreach (var item in invoiceNo)
                {
                    var accountInvoice = BuyingFacade.AccountInvoiceBL().GetSingle(item);
                    if (accountInvoice == null)
                        throw new Exception("ไม่พบ invoice " + item + " นี้ในระบบ");

                    accountInvoiceList.Add(accountInvoice);
                    if (accountInvoice.AccountCreditNotes.Count() > 0)
                    {
                        var creditNote = BuyingFacade.AccountCreditNoteBL()
                            .GetSingle(accountInvoice.InvoiceNo);
                        creditNoteList.Add(creditNote);
                    }
                }

                foreach (var item in accountInvoiceList)
                {
                    if (item.Invoice.DebtTypeCode != "DP")
                        throw new Exception("ประเภทของ invoice " + item.InvoiceNo +
                            " ไม่ใช่โครงการน้ำหยด โปรดตรวจสอบกับทางแผนกไอทีอีกครั้ง");
                }

                // account invoice ที่ถูกนำไปทำลดหนี้แล้ว จะไม่สามารถเอามาทำตั้งหนี้ได้
                // account invoice ที่ถูกนำไปตั้งหนี้แล้ว จะไม่สามารถเอามาทำลดหนี้ได้
                if (accountInvoiceList.Count <= 0)
                    throw new ArgumentException("ไม่พบข้อมูล account invoice ของชาวไร่รหัส " + farmerCode + " ในปี " + crop);

                var register = BuyingFacade.RegistrationBL()
                    .GetSingle(crop, farmerCode);

                if (register == null)
                    throw new ArgumentException("ไม่พบข้อมูลการทำสัญญาของชาวไร่รหัส " + farmerCode + " ในปี " + crop);

                if (register.ActiveStatus == false)
                    throw new ArgumentException("ชาวไร่รายนี้ยกเลิกสัญญาเนื่องจาก " + register.UnRegisterReason);

                var buyingDocumentList = BuyingFacade.BuyingDocumentBL()
                    .GetByFarmer(crop, farmerCode);

                if (buyingDocumentList.Count() > 0)
                    throw new ArgumentException("ชาวไร่รายนี้ได้เริ่มต้นทำการซื้อขายในระบบใน crop นี้ไปแล้ว ไม่สามารถตั้งหนี้ได้");

                //คำนวณยอดรวมจาก account invoice ที่ระบุเพื่อนำมาตั้งเป็นยอดหนี้
                double totalInvoiceAmount = (double)accountInvoiceList
                    .Sum(x => x.InvoiceDetails
                    .Sum(y => y.Quantity * y.UnitPrice));

                double totalCreditNoteAmount = (double)creditNoteList
                    .Sum(x => x.AccountCreditNoteDetails
                    .Sum(y => y.Quantity * y.UnitPrice));

                double totalBalanceAmount = totalInvoiceAmount - totalCreditNoteAmount;

                if (totalBalanceAmount <= 0)
                    throw new ArgumentException("ไม่พบหนี้คงเหลือที่ต้องนำมาตั้งหนี้ " +
                        "เนื่องจากมีการลดหนี้ในระบบแล้ว โปรดตรวจสอบข้อมูลอีกครั้ง");

                // ตรวจสอบหนี้เก่า
                var ountStandingDebt = GetOutStatdingDebtAmount(crop, farmerCode, "CI");
                if (ountStandingDebt > 0)
                    throw new Exception("ชาวไร่รหัส " + farmerCode +
                        "มีหนี้เก่าที่ค้างชำระอยู่จำนวน " + ountStandingDebt.ToString("N2") + " " +
                        "บาท ต้องชำระหนี้เก่าให้หมดก่อน");

                // ตรวจสอบหนี้ 3 ประเภท ว่ามีการนำไปตั้งหนี้แล้วหรือไม่ (ห้ามซ้ำ)
                // นับจากปีที่ตั้งหนี้คู่กับ debt type code
                // หนี้น้ำหยดมีได้ 3 ก้อน โดยนับจากปีที่ตั้งหนี้
                // หนี้ปัจจัยการผลิตมีได้ 1 ก้อน โดยนับจากปีที่ตั้งหนี้
                // หนี้เงินกู้มีได้ 5 ก้อน โดยนับจากปีที่ตั้งหนี้
                var cropInputsList = uow.DebtSetupRepository
                    .Get(x => x.Crop == crop &&
                    x.FarmerCode == farmerCode &&
                    x.DebtTypeCode == "CI"
                    , null
                    , x => x.DebtReceipts);
                if (cropInputsList.Count() >= 1)
                    throw new ArgumentException("หนี้ประเภท CI จะตั้งยอดหนี้ได้ไม่เกิน 1 รายการต่อปี");

                var code = GenerateCode(crop, farmerCode);
                uow.DebtSetupRepository
                    .Add(new DebtSetup
                    {
                        DebtSetupCode = code,
                        DebtTypeCode = "CI",
                        Crop = crop,
                        FarmerCode = farmerCode,
                        Amount = (decimal)totalBalanceAmount,
                        DeductPerKg = (decimal)0.0,
                        //DripProjectQuota
                        //เนื่องจากเป็นการระบุโควต้าหน่วยเป็น kg. เหมือนกันเลยใช้คอลัมน์เดียวกัน
                        //อนาคตหากมีเวลาอาจเพิ่มอีก 1 column เพื่อเก็บแยกโดยเฉพาะหรือเปลี่ยนชื่อ column ให้สอดคล้อง
                        DripProjectQuota = 0,
                        DeductionCrop = crop,
                        CreateBy = createUser,
                        CreateDate = DateTime.Now,
                        ModifiedBy = createUser,
                        ModifiedDate = DateTime.Now
                    });

                foreach (var item in accountInvoiceList)
                    uow.DebtSetupAccountInvoiceRepository
                        .Add(new DebtSetupAccountInvoice
                        {
                            DebtSetupCode = code,
                            AccountInvoiceNo = item.InvoiceNo,
                            AssignBy = createUser,
                            AssignDate = DateTime.Now
                        });

                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetupCropInputsDebt(short crop, string extensionAgentCode, string username)
        {
            try
            {
                //1.Find an incomplete farmer.
                var accountInvoices = uow.AccountInvoiceRepository
                    .Get(x => x.Invoice.DebtTypeCode == "CI" &&
                    x.InvoiceNo.Contains("I") &&
                    x.Invoice.Crop == crop &&
                    x.Invoice.RegistrationFarmer.Farmer.Supplier == extensionAgentCode
                    , null
                    , x => x.Invoice
                    , x => x.InvoiceDetails);
                var listOfAccountInvoiceNo = accountInvoices
                    .Select(x => x.InvoiceNo)
                    .ToList();
                var creditNotes = uow.AccountCreditNoteRepository
                    .Get(x => listOfAccountInvoiceNo.Contains(x.AccountInvoiceNo)
                    , null
                    , x => x.AccountCreditNoteDetails);
                var listOfInvoiceWithCreditNote = (from a in accountInvoices
                                                   join b in creditNotes
                                                   on a.InvoiceNo equals b.AccountInvoiceNo
                                                   into c
                                                   from d in c.DefaultIfEmpty()
                                                   select new
                                                   {
                                                       a.Invoice.FarmerCode,
                                                       AccountInvoiceNo = a.InvoiceNo,
                                                       InvoiceAmt = a.InvoiceDetails.Sum(x =>
                                                       x.Quantity * x.UnitPrice),
                                                       CreditNoteAmt = d == null ? 0 : c.Sum(x =>
                                                       x.AccountCreditNoteDetails.Sum(y =>
                                                       y.Quantity * y.UnitPrice)),
                                                       InvoiceBalanceAmt = a.InvoiceDetails.Sum(x =>
                                                       x.Quantity * x.UnitPrice) - (d == null ? 0 : d == null ? 0 : c.Sum(x =>
                                                       x.AccountCreditNoteDetails.Sum(y =>
                                                       y.Quantity * y.UnitPrice)))
                                                   })
                                                   .Where(x => x.InvoiceBalanceAmt > 0)
                                                   .ToList();

                var invoiceSummaryList = (listOfInvoiceWithCreditNote
                    .GroupBy(x => x.FarmerCode)
                    .Select(x => new
                    {
                        FarmerCode = x.Key,
                        InvoiceBalanceAmt = x.Sum(y => y.InvoiceBalanceAmt)
                    }))
                    .ToList();

                var listOfFarmerCode = invoiceSummaryList
                    .Select(x => x.FarmerCode)
                    .ToList();

                var farmerDebtSetup = uow.DebtSetupRepository
                    .Get(x => x.Crop == crop &&
                    x.DebtTypeCode == "CI" &&
                    listOfFarmerCode.Contains(x.FarmerCode))
                    .GroupBy(x => x.FarmerCode)
                    .Select(x => new
                    {
                        FarmerCode = x.Key,
                        DebtSetupAmt = x.Sum(y => y.Amount)
                    })
                    .ToList();
                var invoiceAndDebtSummary = (from a in invoiceSummaryList
                                             join b in farmerDebtSetup
                                             on a.FarmerCode equals b.FarmerCode
                                             into c
                                             from d in c.DefaultIfEmpty()
                                             select new
                                             {
                                                 a.FarmerCode,
                                                 a.InvoiceBalanceAmt,
                                                 BalanceDebtAmt = a.InvoiceBalanceAmt - (d == null ? 0 :
                                                 c.Sum(x => x.DebtSetupAmt)
                                                 )
                                             })
                                             .ToList();
                var incompleteFarmers = invoiceAndDebtSummary
                    .Where(x => x.BalanceDebtAmt != 0)
                    .ToList();

                //2.To do setup debt for each farmers.
                foreach (var item in incompleteFarmers)
                {
                    var existsSetup = farmerDebtSetup
                        .Where(x => x.FarmerCode == item.FarmerCode)
                        .ToList();
                    var invoiceList = listOfInvoiceWithCreditNote
                                .Where(x => x.FarmerCode == item.FarmerCode)
                                .ToList();
                    var debtSetupCode = GenerateCode(crop, item.FarmerCode);
                    if (existsSetup.Count() > 0)
                    {
                        //To do Recalculat and setup debt.
                        foreach (var deleteInvoice in invoiceList)
                        {
                            var assoDeleteList = uow.DebtSetupAccountInvoiceRepository
                                .Get(x => x.AccountInvoiceNo == deleteInvoice.AccountInvoiceNo)
                                .ToList();
                            foreach (var deleteItem in assoDeleteList)
                                uow.DebtSetupAccountInvoiceRepository
                                    .Remove(deleteItem);
                        }
                        uow.Save();

                        //Setup new debt.
                        uow.DebtSetupRepository
                            .Add(new DebtSetup
                            {
                                DebtSetupCode = debtSetupCode,
                                Amount = item.InvoiceBalanceAmt,
                                Crop = crop,
                                FarmerCode = item.FarmerCode,
                                DeductionCrop = crop,
                                DebtTypeCode = "CI",
                                DeductPerKg = 0,
                                DripProjectQuota = 0,
                                CreateBy = username,
                                CreateDate = DateTime.Now,
                                ModifiedBy = username,
                                ModifiedDate = DateTime.Now
                            });
                        uow.Save();

                        //Specified axassociation relation.
                        foreach (var invoice in invoiceList)
                        {
                            uow.DebtSetupAccountInvoiceRepository
                                .Add(new DebtSetupAccountInvoice
                                {
                                    AccountInvoiceNo = invoice.AccountInvoiceNo,
                                    DebtSetupCode = debtSetupCode,
                                    AssignDate = DateTime.Now,
                                    AssignBy = username
                                });
                        }
                        uow.Save();
                    }
                    else
                    {
                        //Setup new debt.
                        uow.DebtSetupRepository
                            .Add(new DebtSetup
                            {
                                DebtSetupCode = debtSetupCode,
                                Amount = item.InvoiceBalanceAmt,
                                Crop = crop,
                                FarmerCode = item.FarmerCode,
                                DeductionCrop = crop,
                                DebtTypeCode = "CI",
                                DeductPerKg = 0,
                                DripProjectQuota = 0,
                                CreateBy = username,
                                CreateDate = DateTime.Now,
                                ModifiedBy = username,
                                ModifiedDate = DateTime.Now
                            });
                        uow.Save();

                        //Specified axassociation relation.
                        foreach (var invoice in invoiceList)
                        {
                            uow.DebtSetupAccountInvoiceRepository
                                .Add(new DebtSetupAccountInvoice
                                {
                                    AccountInvoiceNo = invoice.AccountInvoiceNo,
                                    DebtSetupCode = debtSetupCode,
                                    AssignDate = DateTime.Now,
                                    AssignBy = username
                                });
                        }
                        uow.Save();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
