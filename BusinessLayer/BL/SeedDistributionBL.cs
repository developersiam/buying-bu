﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface ISeedDistributionBL
    {

        void Add(SeedDistribution model);
        void Update(SeedDistribution model);
        void Delete(SeedDistribution model);
        SeedDistribution GetByID(short crop, string farmerCode, Guid seedForDistibutionID);
        List<SeedDistribution> GetByFarmer(short crop, string farmerCode);
        bool NotDistributionSeed(short crop, string farmerCode);
    }

    public class SeedDistributionBL : ISeedDistributionBL
    {
        UnitOfWork uow;
        public SeedDistributionBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(SeedDistribution model)
        {
            try
            {
                var validation = new DomainValidation.IsSeedDistributionValid().Validate(model);

                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var _regis = uow.RegistrationFarmerRepository
                     .GetSingle(reg => reg.Crop == model.Crop &&
                     reg.FarmerCode == model.FarmerCode);

                if (_regis == null)
                    throw new ArgumentException("ไม่พบข้อมูลการทำสัญญาของชาวไร่รายนี้ในระบบ");

                /// หากมีการหักค่าเมล็ดพันธุ์ไปแล้วจะไม่ให้จ่ายเพิ่ม แก้ไข หรือลบข้อมูลได้
                /// 
                BuyingFacade.DebtorPaymentBL().IsPayment(model.Crop, model.FarmerCode);

                /// รายการข้อมูลการจ่ายเมล็ดพันธู์ให้กับชาวไร่รายนี้
                /// 
                var list = uow.SeedDistributionRepository
                    .Query(x => x.Crop == _regis.Crop &&
                    x.FarmerCode == _regis.FarmerCode,
                    null,
                    x => x.SeedForDistribution,
                    x => x.SeedForDistribution.SeedDistributionType);

                /// ตรวจสอบรายการเมล็ดพันธุ์ในระบบตามที่ user เลือกเข้ามาว่ามีอยู่ในระบบหรือไม่
                var item = uow.SeedForDistributionRepository
                    .GetSingle(x => x.ID == model.SeedForDistributionID,
                    x => x.SeedDistributionType);

                if (item == null)
                    throw new ArgumentException("รายการเมล็ดพันธุ์นี้ไม่มีอยู่ในระบบ โปรดตรวจสอบกับทางแผนกไอทีอีกครั้ง");

                /// ถ้ารายการปัจจุบันเป็นรายการที่เคยจ่ายไปแล้วให้แจ้งเตือนและไม่ให้บันทึกข้อมูล
                if (list.Where(x => x.SeedForDistributionID == model.SeedForDistributionID).Count() > 0)
                    throw new ArgumentException("มีการจ่ายเมล็ดพันธุ์รายการนี้ไปแล้ว!");

                /// หากมีการจ่ายเมล็ดพันธุ์ปกติ (noemal seed) ไปแล้วไม่ให้จ่ายซ้ำอีก
                /// normal seed คือ จะบังคับให้ชาวไร่รับเมล็ดพันธุ์ประเภทนี้เท่ากับจำนวนไร่ที่ลงทะเบียน (ขาดหรือเกินไม่ได้ บังคับนะ)
                /// 
                if (item.SeedDistributionType.Name == "Seed")
                {
                    if (list.Where(x => x.SeedForDistribution.SeedDistributionType.Name == "Seed").Count() >= 1)
                        throw new ArgumentException("มีการจ่ายเมล็ดพันธุ์ปกติ Seed หรือ Normal seed (ไม่ใช่ Extra seed) ไปก่อนหน้านี้แล้วคือ " + Environment.NewLine +
                            "จำนวนรายการ: " + list.Where(x => x.SeedForDistribution.SeedDistributionType.Name == "Seed").Count() + " รายการ" + Environment.NewLine +
                            "จำนวนที่จ่าย: " + list.Where(x => x.SeedForDistribution.SeedDistributionType.Name == "Seed").Sum(x => x.Quantity) + " Unit" + Environment.NewLine +
                            "จำนวนไร่ในสัญญา: " + _regis.QuotaFromSignContract + " ไร่");

                    if (_regis.QuotaFromSignContract != model.Quantity)
                        throw new ArgumentException("กรณีเลือกรายการเมล็ดพันธุ์ปกติ จำนวนที่จ่ายให้กับชาวไร่จะต้องเท่ากับจำนวนไร่ที่ระบุไว้ในสัญญา" + Environment.NewLine +
                           "จำนวนไร่ในสัญญา: " + _regis + " ไร่" + Environment.NewLine +
                           "จำนวนเมล็ดพันธุ์ที่จะบันทึก: " + model.Quantity + " Unit");
                }

                model.ModifiedDate = DateTime.Now;

                uow.SeedDistributionRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(SeedDistribution model)
        {
            try
            {
                var validation = new DomainValidation.IsSeedDistributionValid().Validate(model);

                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);


                /// หากมีการหักค่าเมล็ดพันธุ์ไปแล้วจะไม่ให้จ่ายเพิ่ม แก้ไข หรือลบข้อมูลได้
                ///
                BuyingFacade.DebtorPaymentBL().IsPayment(model.Crop, model.FarmerCode);

                uow.SeedDistributionRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(SeedDistribution model)
        {
            try
            {
                var _delete = GetByID(model.Crop, model.FarmerCode, model.SeedForDistributionID);

                if (_delete == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                /// หากมีการหักค่าเมล็ดพันธุ์ไปแล้วจะไม่ให้จ่ายเพิ่ม แก้ไข หรือลบข้อมูลได้
                ///
                BuyingFacade.DebtorPaymentBL().IsPayment(model.Crop, model.FarmerCode);

                uow.SeedDistributionRepository.Remove(_delete);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SeedDistribution GetByID(short crop, string farmerCode, Guid seedForDistibutionID)
        {
            return uow.SeedDistributionRepository
                .GetSingle(sd => sd.Crop == crop &&
                sd.FarmerCode == farmerCode &&
                sd.SeedForDistributionID == seedForDistibutionID,
                sd => sd.SeedForDistribution,
                sd => sd.SeedForDistribution.SeedDistributionType,
                sd => sd.SeedForDistribution.SeedVariety);
        }

        public List<SeedDistribution> GetByFarmer(short crop, string farmerCode)
        {
            return uow.SeedDistributionRepository
                .Query(sd => sd.Crop == crop &&
                sd.FarmerCode == farmerCode,
                null,
                sd => sd.SeedForDistribution,
                sd => sd.SeedForDistribution.SeedDistributionType,
                sd => sd.SeedForDistribution.SeedVariety).ToList();
        }

        public bool NotDistributionSeed(short crop, string farmerCode)
        {
            var result = false;
            if (GetByFarmer(crop, farmerCode).Count() > 0)
            {
                result = true;
            }
            return result;
        }
    }
}
