﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer;
using DomainModel;

namespace BusinessLayer.BL
{
    public interface IOtherCropProductionBL
    {
        void Add(OtherCropProduction model);
        void Update(OtherCropProduction model);
        void Delete(OtherCropProduction model);
        OtherCropProduction GetByID(Guid id);
        List<OtherCropProduction> GetByCitizenID(short crop, string citizenID);
    }

    public class OtherCropProductionBL : IOtherCropProductionBL
    {
        UnitOfWork uow;
        public OtherCropProductionBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(OtherCropProduction model)
        {
            try
            {
                if (model.CitizenID == null)
                    throw new ArgumentException("You don't specify Citizen ID.");

                if (model.StartCrop == null)
                    throw new ArgumentException("You don't specify StartCrop.");

                if (model.EndCrop == null)
                    throw new ArgumentException("You don't specify EndCrop.");

                if (model.CropType == null)
                    throw new ArgumentException("You don't specify CropType.");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("You don't specify Modified User.");

                if (model.EndCrop < model.StartCrop)
                    throw new ArgumentException("StartCrop and EndCrop range is not correct.");

                model.ModifiedDate = DateTime.Now;

                uow.OtherCropProductionRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Update(OtherCropProduction model)
        {
            try
            {
                if (model.CitizenID == null)
                    throw new ArgumentException("You don't specify Citizen ID.");

                if (model.StartCrop == null)
                    throw new ArgumentException("You don't specify StartCrop.");

                if (model.EndCrop == null)
                    throw new ArgumentException("You don't specify EndCrop.");

                if (model.CropType == null)
                    throw new ArgumentException("You don't specify CropType.");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("You don't specify Modified User.");

                if (model.OtherCropProductionID == null)
                    throw new ArgumentException("You don't specify OtherCropProductionID.");

                if (model.EndCrop < model.StartCrop)
                    throw new ArgumentException("Start Crop and EndCrop range is not correct.");

                if (GetByID(model.OtherCropProductionID) == null)
                    throw new ArgumentException("Find by ID not found.");

                model.ModifiedDate = DateTime.Now;

                uow.OtherCropProductionRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(OtherCropProduction model)
        {
            try
            {
                if (model.OtherCropProductionID == null)
                    throw new ArgumentException("You don't specify OtherCropProductionID.");

                if (GetByID(model.OtherCropProductionID) == null)
                    throw new ArgumentException("Find by ID not found.");

                uow.OtherCropProductionRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public OtherCropProduction GetByID(Guid id)
        {

            return uow.OtherCropProductionRepository
                    .GetSingle(o => o.OtherCropProductionID == id);
        }
        public List<OtherCropProduction> GetByCitizenID(short crop, string citizenID)
        {
            return uow.OtherCropProductionRepository
                .Query(o => o.CitizenID == citizenID && o.Crop == crop).ToList();
        }
    }
}
