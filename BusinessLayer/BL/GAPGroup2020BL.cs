﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IGAPGroup2020BL
    {
        GAPGroup2020 GetSingle(string gapGroupCode);
        GAPGroup2020 GetSingleByUniqueKey(short crop, string areaCode, string leaderCitizenID);
        List<GAPGroup2020> GetByArea(string areaCode);
    }
    public class GAPGroup2020BL : IGAPGroup2020BL
    {
        UnitOfWork uow;
        public GAPGroup2020BL()
        {
            uow = new UnitOfWork();
        }

        public List<GAPGroup2020> GetByArea(string areaCode)
        {
            return uow.GAPGroup2020Repository
                .Query(x => x.AreaCode == areaCode,
                null,
                x => x.Person,
                x => x.Area)
                .ToList();
        }

        public GAPGroup2020 GetSingle(string gapGroupCode)
        {
            return uow.GAPGroup2020Repository
                .GetSingle(x => x.GAPGroupCode == gapGroupCode,
                x => x.Person,
                x => x.Area,
                x => x.GAPGroupMember2020);
        }

        public GAPGroup2020 GetSingleByUniqueKey(short crop, string areaCode, string leaderCitizenID)
        {
            return uow.GAPGroup2020Repository
                .GetSingle(x => x.Crop == crop &&
                x.AreaCode == areaCode &&
                x.LeaderCitizenID == leaderCitizenID,
                x => x.Person,
                x => x.Area);
        }
    }
}
