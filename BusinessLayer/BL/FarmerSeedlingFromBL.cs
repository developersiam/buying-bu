﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IFarmerSeedlingFromBL
    {
        void Add(string citizenID, Guid seedlingFromID, short crop, string modifiedBy);
        void Delete(string citizenID, Guid seedlingFromID, short crop);
        FarmerSeedlingFrom GetSingle(string citizenID, Guid seedlingFromID, short crop);
        List<FarmerSeedlingFrom> GetByCitizenID(string citizenID, short crop);
    }

    public class FarmerSeedlingFromBL : IFarmerSeedlingFromBL
    {
        UnitOfWork uow;
        public FarmerSeedlingFromBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(string citizenID, Guid seedlingFromID, short crop, string modifiedBy)
        {
            try
            {
                if (string.IsNullOrEmpty(citizenID))
                    throw new ArgumentException("CitizenID cannot be empty.");

                if (string.IsNullOrEmpty(modifiedBy))
                    throw new ArgumentException("Modified By cannot be empty.");

                if (seedlingFromID == null)
                    throw new ArgumentException("Seedling from id cannot be empty.");

                var model = GetSingle(citizenID, seedlingFromID, crop);
                if (model != null)
                    throw new ArgumentException("มีข้อมูลนี้แล้วในระบบ");

                uow.FarmerSeedlingFromRepository
                    .Add(new FarmerSeedlingFrom
                    {
                        Crop = crop,
                        CitizenID = citizenID,
                        SeedlingFromID = seedlingFromID,
                        ModifiedBy = modifiedBy,
                        ModifiedDate = DateTime.Now
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string citizenID, Guid seedlingFromID, short crop)
        {
            try
            {
                if (string.IsNullOrEmpty(citizenID))
                    throw new ArgumentException("CitizenID cannot be empty.");

                if (seedlingFromID == null)
                    throw new ArgumentException("Seedling from id cannot be empty.");

                var model = GetSingle(citizenID, seedlingFromID, crop);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                uow.FarmerSeedlingFromRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<FarmerSeedlingFrom> GetByCitizenID(string citizenID, short crop)
        {
            return uow.FarmerSeedlingFromRepository
                .Get(x => x.CitizenID == citizenID &&
                x.Crop == crop,
                null,
                x => x.SeedlingFrom);
        }

        public FarmerSeedlingFrom GetSingle(string citizenID, Guid seedlingFromID, short crop)
        {
            return uow.FarmerSeedlingFromRepository
                .GetSingle(x => x.CitizenID == citizenID &&
                x.SeedlingFromID == seedlingFromID &&
                x.Crop == crop,
                x => x.SeedlingFrom);
        }
    }
}
