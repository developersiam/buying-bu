﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;
using BusinessLayer.Model;
using System.Runtime.InteropServices;
using Microsoft.SqlServer.Server;

namespace BusinessLayer.BL
{
    public interface IBuyingBL
    {
        void RegisterBarcodeInNormalQuota(string baleBarcode, short crop, string stationCode, string farmerCode, short documentNumber, string registerUser, bool isProject);
        void RegisterBarcodeInExtraQuota(string baleBarcode, short crop, string stationCode, string farmerCode, short documentNumber, string registerUser, bool isProject);
        void CaptureWeight(string baleBarcode, decimal buyingWeight, string weightUser);
        void CaptureGrade(string baleBarcode, short crop, byte pricingNumber, string grade, string buyerCode, string buyingGradeUser);
        void CaptureAll(string baleBarcode, short pricingCrop, byte pricingNumber, string grade, string buyerCode, decimal weight, string transportationCode, string username);
        void ChangeGradeAtWeightScale(string baleBarcode, short crop, byte pricingNumber, string newGrade, string buyerCode, string gradeUser);
        void RemoveBuyingGrade(string baleBarcode);
        void RejectBaleOnTheScale(string baleBarcode, string rejectReason, string rejectUser);
        void RejectBale(string baleBarcode, string rejectReason, string rejectUser, string buyerCode);
        void RemoveRejectBale(string baleBarcode);
        void UnRisterBarcode(string baleBarcode);
        void InputMoistureResult(string baleBarcode, decimal moistureResutt, DateTime moistureResultDate, string moistureResultUser);
        void RemoveMoistureResult(string baleBarcode);
        void UnlockBuyingInfo(string baleBarcode);
        void MoveLoadingBale(List<Buying> baleList, string moveBy, string moveFrom, string moveTo);
        Buying GetByBaleBarcode(string baleBarcode);
        List<Buying> GetByFarmerProject(short crop, string farmerCode, string projectType);
        List<Buying> GetByFarmer(short crop, string farmerCode);
        List<Buying> GetByBuyingDocument(short crop, string buyingStationCode, string farmerCode, short buyingDocumentNumber);
        List<Buying> GetByTransportationCode(string transportationCode);
        List<Buying> GetNestingBale(short crop, string farmerCode);
        List<Buying> GetByCitizenID(string citizenID);
        List<Buying> GetSoldHistoryByFarmer(string farmerCode);
        List<Buying> GetSoldHistoryByCitizenID(string citizenID);
        List<Buying> GetByExtensionAgent(short crop, string extensionAgentCode);
        List<m_SummaryBuyingByPosition> GetSummaryBuyingOfFarmerByPosition(short crop, string farmerCode);
        List<m_SummaryBuyingByQuality> GetSummaryBuyingOfFarmerByQuality(short crop, string farmerCode);
    }

    public class BuyingBL : IBuyingBL
    {
        UnitOfWork uow;
        public BuyingBL()
        {
            uow = new UnitOfWork();
        }

        public bool IsOverNormalQuota(short crop, string farmerCode, string projectType)
        {
            /*
             * ตรวจสอบยอดรวมการขายในแต่ละโปรเจคของชาวไร่ 
             * หากเป็นการขายเกินโควต้าที่กำหนดให้ return true
             * หากเป็นการขายในโควต้าปกติให้ return false
             */

            var _farmerProject = uow.FarmerProjectRepository
                .GetSingle(x => x.Crop == crop && x.FarmerCode == farmerCode && x.ProjectType == projectType);

            var _buyings = uow.BuyingRepository.Query(x => x.Crop == crop && x.FarmerCode == farmerCode && x.ProjectType == projectType);
            var _supplierCode = uow.FarmerRepository.GetSingle(x => x.FarmerCode == farmerCode).Supplier;
            var _kgPerRai = uow.KilogramsPerRaiConfigurationRepository.GetSingle(x => x.Crop == crop && x.SupplierCode == _supplierCode);
            var _balance = (_farmerProject.ActualRai * _kgPerRai.KilogramsPerRai) - _buyings.Sum(x => x.Weight);

            if (_balance <= 0)
                return true; /// ขายเกินโควต้าไปแล้ว
            else
                return false; /// อยู่ในโควต้า สามารถขายได้
        }

        public bool IsOverExtraQuota(short crop, string farmerCode, string projectType)
        {
            /*
             * ตรวจสอบยอดรวมการขายในแต่ละโปรเจคของชาวไร่ 
             * หากเป็นการขายเกินโควต้าที่กำหนดให้ return true
             * หากเป็นการขายในโควต้าปกติให้ return false
             */

            var _farmerProject = uow.FarmerProjectRepository
                .GetSingle(x => x.Crop == crop &&
                x.FarmerCode == farmerCode &&
                x.ProjectType == projectType);

            var _buyings = uow.BuyingRepository
                .Query(x => x.Crop == crop &&
                x.FarmerCode == farmerCode &&
                x.ProjectType == projectType);

            var _balance = (_farmerProject.ExtraQuota) - _buyings.Sum(x => x.Weight);

            if (_balance <= 0)
                return true; /// ขายเกินโควต้าไปแล้ว
            else
                return false; /// อยู่ในโควต้า สามารถขายได้
        }

        public bool IsAcceptGradeInOverQuota(string buyingGrade)
        {
            /*
             * เกรดที่อนุญาตให้สามารถขายในโควต้าส่วนเกินได้
             * กลุ่ม CF
             * กลุ่ม B
             * กลุ่ม T
             * หากอยู่ในกลุ่มที่กำหนดให้ return true
             * หากไม่อยู่ในกลุ่มที่กำหนดให้ return false
             */

            if (buyingGrade.Contains("CF") ||
                buyingGrade.Contains("B") ||
                buyingGrade.Contains("T"))
                return true;
            else
                return false;
        }

        public void VerifyBarcode(string baleBarcode)
        {
            try
            {
                if (baleBarcode.Length != 17)
                    throw new ArgumentException("รหัสบาร์โค้ตจะต้องมี 17 หลัก");

                if (baleBarcode.Substring(0, 2) != "00" &&
                    baleBarcode.Substring(0, 2) != "01" &&
                    baleBarcode.Substring(0, 2) != "02" &&
                    baleBarcode.Substring(0, 2) != "03" &&
                    baleBarcode.Substring(0, 2) != "04")
                    throw new ArgumentException("รหัสบาร์โค้ต 2 หลักแรกจะต้องเป็น 00,01,02,03,04 เท่านั้น");

                var buying = GetByBaleBarcode(baleBarcode);

                if (buying != null)
                    throw new ArgumentException("ป้ายบาร์โค้ตนี้ถูกใช้ไปแล้ว โดยมีรายละเอียดดังนี้" + Environment.NewLine +
                    "BaleBarcode: " + buying.BaleBarcode + Environment.NewLine +
                    "Crop: " + buying.Crop + Environment.NewLine +
                    "FarmerCode: " + buying.FarmerCode + Environment.NewLine +
                    "StationCode: " + buying.BuyingStationCode + Environment.NewLine +
                    "RegisterDate: " + buying.RegisterBarcodeDate + Environment.NewLine +
                    "BuyingGrade: " + buying.Grade);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string VerifyProjectType(string baleBarcode)
        {
            string projectType = "";

            switch (baleBarcode.Substring(0, 2))
            {
                case "00":
                    projectType = "NL";
                    break;
                case "01":
                    projectType = "HB";
                    break;
                case "02":
                    projectType = "H4";
                    break;
                case "03":
                    projectType = "SC";
                    break;
                case "04":
                    //CY2022 ป้ายขาว HPP เหลือนิดหน่อยเค้าอยากใช้ให้หมดๆ เลยให้เอาไปรวมกับป้ายม่วง HB
                    projectType = "HB";
                    break;
                default:
                    projectType = "";
                    break;
            }

            return projectType;
        }

        public void RegisterBarcodeInNormalQuota(string baleBarcode, short crop, string stationCode,
            string farmerCode, short documentNumber, string registerUser, bool isProject)
        {
            try
            {
                /// ขั้นตอนการทำงานของระบบ
                /// 1.ผู้ใช้สแกนหมายเลขบาร์โค้ตจากป้ายบาร์โค้ต
                /// 2.ระบบทำการตรวจสอบข้อมูล
                /// 3.ระบบบันทึกข้อมูลการจ่ายป้ายบาร์โค้ต
                /// 4.ระบบแสดงผลข้อมูลรายการป้ายบาร์โค้ตออกทางหน้าจอ
                /// 
                /// 
                /// เงื่อนไขที่ใช้ในการตรวจสอบ ก่อนการบันทึกข้อมูลการจ่ายป้าย
                /// 1.ป้ายบาร์โค้ตจะต้องไม่ซ้ำกัน (ไม่ถูกใช้ไปแล้วในการซื้อขายก่อนหน้านี้)
                /// 2.สถานะสัญญาชาวไร่จะต้อง Active (ActiveStaus)
                /// 3.สถานะการขายชาวไร่จะต้อง Active (SellingStatus)
                /// 4.ใบซื้อจะต้องไม่ถูก Finish
                /// 5.จะต้องไม่จ่ายป้ายเกินจากโควต้าคงเหลือ (1 bale = 45 kgs.)
                /// 6.เป็น Extra Barcode หรือไม่?
                /// 
                var _crop = uow.CropRepository.GetSingle(x => x.DefaultStatus == true);

                if (crop != _crop.Crop1)
                    throw new ArgumentException("ปีของใบซื้อของท่านคือ " + crop + " ไม่ตรงกับปีปัจจุบันในระบบคือ " + _crop.Crop1);

                VerifyBarcode(baleBarcode);

                var _regisModel = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == crop && x.FarmerCode == farmerCode);

                if (_regisModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียน");

                if (_regisModel.ActiveStatus == false)
                    throw new ArgumentException("ชาวไร่รายนี้ได้ถูกยกเลิกสัญญาแล้ว โปรดตรวจสอบข้อมูลอีกครั้ง");

                var _buyingDocument = uow.BuyingDocumentRepository
                    .GetSingle(x => x.Crop == crop &&
                    x.FarmerCode == farmerCode &&
                    x.BuyingStationCode == stationCode &&
                    x.BuyingDocumentNumber == documentNumber
                    , x => x.Buyings);

                if (_buyingDocument == null)
                    throw new ArgumentException("ไม่พบข้อมูลใบซื้อนี้ในระบบ");

                if (_buyingDocument.IsFinish == true)
                    throw new ArgumentException("ใบซื้อนี้ถูก Finish แล้ว ไม่สามารถจ่ายป้ายบาร์โค้ตได้");

                ///จะต้องไม่จ่ายป้ายเกินจากโควต้าคงเหลือ (45kg/bale)
                ///
                string _projectType = VerifyProjectType(baleBarcode);

                if (_projectType == "")
                    throw new ArgumentException("รหัสบาร์โค้ต 2 หลักแรก ไม่ตรงตามรูปแบบของบริษัท");

                var _farmerQuota = Helper.FarmerProjectHelper
                    .GetQuotaAndSoldByFarmerVersion2(crop, farmerCode)
                    .SingleOrDefault(x => x.ProjectType == _projectType);

                if (_farmerQuota == null)
                    throw new ArgumentException("ไม่พบโควต้าในโปรเจค " + _projectType);

                // หากยังขายยาไม่เต๊มโควต้าปกติให้ตรวจสอบว่ามีการยืนยันจาก Receiving แล้วหรือไม่?
                // หากยังมีบางเวาเชอร์ที่ไม่ผ่านการยืนยัน จะยังไม่ให้จ่ายป้าย
                // แสดงว่าในการขายยาในโควต้าเพื่อให้ได้ +5.5 บาท ยังไม่สิ้นสุด
                // หากยังไม่ได้เพิ่ม Extra Quota ถือว่ายังเป็นการขายในโควต้าปกติ
                //if (_farmerQuota.ExtraQuota <= 0)
                //{
                //    var normalQuotaDocumentList = uow.BuyingRepository
                //        .Get(x => x.Crop == crop &&
                //        x.FarmerCode == farmerCode &&
                //        //x.BuyingStationCode != stationCode &&
                //        x.BuyingDocumentNumber != documentNumber &&
                //        x.ProjectType == _projectType &&
                //        x.IsExtraQuota == false
                //        , null
                //        , x => x.BuyingDocument)
                //        .GroupBy(x => x.BuyingDocument)
                //        .Select(x => new BuyingDocument
                //        {
                //            Crop = x.Key.Crop,
                //            BuyingStationCode = x.Key.BuyingStationCode,
                //            FarmerCode = x.Key.FarmerCode,
                //            BuyingDocumentNumber = x.Key.BuyingDocumentNumber,
                //            IsFinish = x.Key.IsFinish,
                //            IsAccountFinish = x.Key.IsAccountFinish,
                //            BuyingDate = x.Key.BuyingDate
                //        })
                //        .ToList();

                //    if (normalQuotaDocumentList
                //        .Where(x => x.IsAccountFinish == false)
                //        .Count() > 0)
                //        throw new ArgumentException("ใบเวาเชอร์ที่ขายในวันที่ " +
                //            Convert.ToDateTime(normalQuotaDocumentList.FirstOrDefault().BuyingDate).ToString("dd/MM/yyyy") +
                //            " ซึ่งมีการขายยาในโควต้าปกติ(โปรเจค " + _projectType + ") เพื่อรับโบนัส +5.5 บาท ยังไม่ได้รับการยืนยันจากแผนก Receving" + Environment.NewLine +
                //            "โปรดตรวจสอบข้อมูลกับแผนก Receiving อีกครั้ง");
                //}

                // หาก Balance Tag เป็น 0 จะไม่อนุญาตให้จ่ายป้ายให้กับชาวไร่
                if (_farmerQuota.BalanceTag <= 0)
                    throw new ArgumentException("ไม่สามารถจ่ายป้ายในโปรเจค " + _projectType + " นี้ได้เนื่องจากเกินจากโควต้าที่กำหนด");

                // จำนวนป้ายที่จ่ายไปแล้ว และยังไม่ได้ถูกนำไปใช้บันทึกข้อมูลใดๆ reject และ grade
                var unfinishTag = _buyingDocument.Buyings
                    .Where(x => x.ProjectType == _projectType &&
                    x.IsExtraQuota == false &&
                    x.RejectReason == null &&
                    x.Weight == null)
                    .ToList();

                if (unfinishTag.Count() >= _farmerQuota.BalanceTag)
                    throw new ArgumentException("ท่านได้จ่ายป้ายครบตามจำนวนแล้ว " + Environment.NewLine +
                        "จำนวนป้ายตามโควต้าคงเหลือ : " + _farmerQuota.BalanceTag + Environment.NewLine +
                        "ป้ายที่รอบันทึกน้ำหนัก : " + unfinishTag.Count() + Environment.NewLine +
                        "โปรดชั่งยาให้ครบทุกห่อก่อน");

                /// สร้าง date time ใหม่สำหรับใช้ในกรณีที่เลือกวันที่ที่ไม่ใช่วันที่ปัจจุบันของเครื่อง
                /// 
                DateTime regisBarcodeDate = _buyingDocument.CreateDate;
                TimeSpan currentTime = DateTime.Now.TimeOfDay;
                regisBarcodeDate = regisBarcodeDate - regisBarcodeDate.TimeOfDay + currentTime;

                Buying buying = new Buying
                {
                    Crop = crop,
                    BuyingStationCode = stationCode,
                    FarmerCode = farmerCode,
                    BuyingDocumentNumber = documentNumber,
                    BaleBarcode = baleBarcode,
                    ProjectType = _projectType,
                    RegisterBarcodeDate = regisBarcodeDate,
                    RegisterBarcodeUser = registerUser,
                    IsExtraQuota = false,
                    MovingBy = isProject == true ? "Project" : null
                };

                uow.BuyingRepository.Add(buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RegisterBarcodeInExtraQuota(string baleBarcode, short crop, string stationCode,
            string farmerCode, short documentNumber, string registerUser, bool isProject)
        {
            try
            {
                /// ขั้นตอนการทำงานของระบบ
                /// 1.ผู้ใช้สแกนหมายเลขบาร์โค้ตจากป้ายบาร์โค้ต
                /// 2.ระบบทำการตรวจสอบข้อมูล
                /// 3.ระบบบันทึกข้อมูลการจ่ายป้ายบาร์โค้ต
                /// 4.ระบบแสดงผลข้อมูลรายการป้ายบาร์โค้ตออกทางหน้าจอ
                /// 
                /// 
                /// เงื่อนไขที่ใช้ในการตรวจสอบ ก่อนการบันทึกข้อมูลการจ่ายป้าย
                /// 1.ป้ายบาร์โค้ตจะต้องไม่ซ้ำกัน (ไม่ถูกใช้ไปแล้วในการซื้อขายก่อนหน้านี้)
                /// 2.สถานะสัญญาชาวไร่จะต้อง Active (ActiveStaus)
                /// 3.สถานะการขายชาวไร่จะต้อง Active (SellingStatus)
                /// 4.ใบซื้อจะต้องไม่ถูก Finish
                /// 5.จะต้องไม่จ่ายป้ายเกินจากโควต้าคงเหลือ (1 bale = 45 kgs.)
                /// 6.เป็น Extra Barcode หรือไม่?
                /// 
                var _crop = uow.CropRepository.GetSingle(x => x.DefaultStatus == true);
                if (crop != _crop.Crop1)
                    throw new ArgumentException("ปีของใบซื้อของท่านคือ " + crop + " ไม่ตรงกับปีปัจจุบันในระบบคือ " + _crop.Crop1);

                VerifyBarcode(baleBarcode);

                var _regisModel = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == crop && x.FarmerCode == farmerCode);
                if (_regisModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียน");

                if (_regisModel.ActiveStatus == false)
                    throw new ArgumentException("ชาวไร่รายนี้ได้ถูกยกเลิกสัญญาแล้ว โปรดตรวจสอบข้อมูลอีกครั้ง");

                var _buyingDocument = uow.BuyingDocumentRepository
                    .GetSingle(x => x.Crop == crop &&
                    x.FarmerCode == farmerCode &&
                    x.BuyingStationCode == stationCode &&
                    x.BuyingDocumentNumber == documentNumber
                    , x => x.Buyings);

                if (_buyingDocument == null)
                    throw new ArgumentException("ไม่สามารถตรวจสอบข้อมูลใบซื้อนี้ได้");

                if (_buyingDocument.IsFinish == true)
                    throw new ArgumentException("ใบซื้อนี้ถูก Finish แล้ว ไม่สามารถจ่ายป้ายบาร์โค้ตได้");

                ///จะต้องไม่จ่ายป้ายเกินจากโควต้าคงเหลือ (45kg/bale)
                ///
                string _projectType = VerifyProjectType(baleBarcode);

                if (_projectType == "")
                    throw new ArgumentException("รหัสบาร์โค้ต 2 หลักแรก ไม่ตรงตามรูปแบบของบริษัท");

                var _farmerQuota = Helper.FarmerProjectHelper
                    .GetQuotaAndSoldByFarmerVersion2(crop, farmerCode)
                    .SingleOrDefault(x => x.ProjectType == _projectType);

                if (_farmerQuota == null)
                    throw new ArgumentException("ไม่พบโควต้าในโปรเจค " + _projectType);

                /// หาก Balance Tag เป็น 0 จะไม่อนุญาตให้จ่ายป้ายให้กับชาวไร่
                /// 
                if (_farmerQuota.BalanceExtraTag <= 0)
                    throw new ArgumentException("ไม่สามารถจ่ายป้ายในโปรเจค " + _projectType + " นี้ได้เนื่องจากเกินจากจำนวนป้ายที่กำหนด");

                /// จำนวนป้ายที่จ่ายไปแล้ว และยังไม่ได้ถูกนำไปใช้บันทึกข้อมูลใดๆ reject และ grade
                /// 
                var unfinishTag = _buyingDocument.Buyings
                    .Where(x => x.ProjectType == _projectType &&
                    x.IsExtraQuota == true &&
                    x.RejectReason == null &&
                    x.Weight == null)
                    .ToList();

                if (unfinishTag.Count() >= _farmerQuota.BalanceExtraTag)
                    throw new ArgumentException("ท่านได้จ่ายป้ายครบตามจำนวนแล้ว " + Environment.NewLine +
                        "จำนวนป้ายตามโควต้าคงเหลือ : " + _farmerQuota.BalanceExtraTag + Environment.NewLine +
                        "ป้ายที่รอบันทึกน้ำหนัก : " + unfinishTag.Count() + Environment.NewLine +
                        "โปรดชั่งยาให้ครบทุกห่อก่อน");

                /// สร้าง date time ใหม่สำหรับใช้ในกรณีที่เลือกวันที่ที่ไม่ใช่วันที่ปัจจุบันของเครื่อง
                /// 
                DateTime regisBarcodeDate = _buyingDocument.CreateDate;
                TimeSpan currentTime = DateTime.Now.TimeOfDay;
                regisBarcodeDate = regisBarcodeDate - regisBarcodeDate.TimeOfDay + currentTime;

                Buying buying = new Buying
                {
                    Crop = crop,
                    BuyingStationCode = stationCode,
                    FarmerCode = farmerCode,
                    BuyingDocumentNumber = documentNumber,
                    BaleBarcode = baleBarcode,
                    ProjectType = _projectType,
                    RegisterBarcodeDate = regisBarcodeDate,
                    RegisterBarcodeUser = registerUser,
                    IsExtraQuota = true,
                    MovingBy = isProject == true ? "Project" : null
                };

                uow.BuyingRepository.Add(buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CaptureWeight(string baleBarcode, decimal buyingWeight, string weightUser)
        {
            try
            {
                /*  เงื่อนไขการตรวจสอบข้อมูลก่อนการบันทึกน้ำหนัก
                    1.ห่อยาจะต้องมีการบันทึกเกรดซื้อมาก่อนจึงจะบันทึกน้ำหนักได้ (หากไม่มีเกรดซื้อ ให้แจ้งเตือนผู้ใช้เพื่อให้ใส่เกรด ณ จุดชั่งก่อนการบันทึก)
                 *  2.จะต้องยังไม่ได้บันทึกข้อมูลการขนขึ้นรถ
                 *  3.จะต้องส่งค่าน้ำหนักซื้อที่ได้จากเครื่องชั่งเข้ามายังระบบ
                 *  4.ใบซื้อจะต้องไม่ถูก Finish  (Finish)
                 *  5.สถานะการซื้อขายของชาวไร่ต้องเป็น Active
                 *  6.สถานะสัญญาชาวไร่จะต้องเป็น Active
                 *  7.มีการตรวจสอบ X low grade over limit
                 *  8.มีการตรวจสอบโควต้าคงเหลือในแต่ละโปรเจค
                 *  9.(เพิ่มเติม ณ 09/06/2018) รายที่ขายในโควต้าส่วนเกินให้ตรวจสอบด้วยว่าเกรดที่จะบันทึกอยู่ในกลุ่มที่อนุญาตหรือไม่ {CF, B, T}
                 * */

                if (baleBarcode == "")
                    throw new ArgumentException("ไม่พบข้อมูล baleBarcode");

                if (buyingWeight.ToString() == "")
                    throw new ArgumentException("ไม่ได้ระบุน้ำหนักที่จะบันทึก");

                if (weightUser == "")
                    throw new ArgumentException("ไม่ได้ระบุชื่อผู้บันทึกข้อมูล");

                if (buyingWeight > 70)
                    throw new ArgumentException("น้ำหนักต่อห่อเกิน 70 กก. ระบบไม่อนุญาตให้บันทึกข้อมูล");

                if (buyingWeight < 1)
                    throw new ArgumentException("น้ำหนักต่อห่อต้อง 1 กก. ขึ้นไป ระบบไม่อนุญาตให้บันทึกข้อมูล");

                var _buying = GetByBaleBarcode(baleBarcode);
                if (_buying == null)
                    throw new ArgumentException("ไม่พบข้อมูลห่อยาหมายเลขบาร์โค้ต " + baleBarcode + " นี้ในระบบ");

                ///ห่อยาจะต้องมีการบันทึกเกรดซื้อมาก่อนจึงจะบันทึกน้ำหนักได้ (หากไม่มีเกรดซื้อ ให้แจ้งเตือนผู้ใช้เพื่อให้ใส่เกรด ณ จุดชั่งก่อนการบันทึก)
                ///
                if (_buying.Grade == null)
                    throw new ArgumentException("ยาห่อนี้ไม่มีเกรดซื้อ ไม่สามารถบันทึกน้ำหนักได้");

                ///จะต้องยังไม่ได้บันทึกข้อมูลการขนขึ้นรถ
                ///
                if (_buying.TransportationDocumentCode != null)
                    throw new ArgumentException("ยาห่อนี้ถูกบันทึกข้อมูลการขนส่งไปแล้ว ไม่สามารถบันทึกน้ำหนักซื้อได้ โปรดตรวจสอบข้อมูลอีกครั้ง");

                ///สถานะใบเวาเชอร์จะต้องเป็น unfinish 
                ///
                if (_buying.BuyingDocument.IsFinish == true)
                    throw new ArgumentException("ใบซื้อนี้ถูก Finish แล้ว ไม่สามารถบันทึกน้ำหนักซื้อได้");

                var _regisModel = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == _buying.Crop &&
                    x.FarmerCode == _buying.FarmerCode);

                if (_regisModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนสัญญของชาวไร่รายนี้");

                ///สถานะสัญญาชาวไร่จะต้องเป็น Active
                ///
                if (_regisModel.ActiveStatus == false)
                    throw new ArgumentException("ชาวไร่รายนี้ได้ถูกยกเลิกสัญญาแล้ว โปรดตรวจสอบข้อมูลอีกครั้ง");

                ///ตรวจสอบจำนวนโควต้าของชาวไร่แยกตามแต่ละโปรเจค   
                ///
                var _farmerProject = Helper.FarmerProjectHelper
                    .GetQuotaAndSoldByProject(_buying.Crop, _buying.FarmerCode, _buying.ProjectType);

                if (_farmerProject == null)
                    throw new ArgumentException("ชาวไร่รายนี้ไม่มีโควต้าในโปรเจค " + _buying.ProjectType);

                if (_buying.Weight == null)
                {
                    /// กรณีเป็นการบันทึกน้ำหนักครั้งแรก
                    /// 
                    if (buyingWeight > _farmerProject.Balance)
                        throw new ArgumentException("น้ำหนักเกินโควต้า ไม่สามารถบันทึกน้ำหนักได้ รายละเอียดดังนี้)" + Environment.NewLine +
                            "จำนวนโควต้า : " + _farmerProject.ActualKgPerRai + " กก." + Environment.NewLine +
                            "จำนวนโควต้า (extra) : " + _farmerProject.ExtraQuota + " กก." + Environment.NewLine +
                            "จำนวนโควต้า (รวม) : " + _farmerProject.TotalQuota + " กก." + Environment.NewLine +
                            "ขายไปแล้ว : " + _farmerProject.Sold + " กก." + Environment.NewLine +
                            "คงเหลือ : " + _farmerProject.Balance + " กก." + Environment.NewLine +
                            "ยาห่อนี้หนัก : " + buyingWeight + " กก." + Environment.NewLine +
                            "ต้องดึงยาออก จำนวน : " + (buyingWeight - _farmerProject.Balance) + " กก.");
                }
                else
                {
                    /// กรณีเป็นการแก้ไขน้ำหนักห่อเดิม
                    /// 
                    var newSold = _farmerProject.Sold - _buying.Weight;

                    if (newSold + buyingWeight > _farmerProject.TotalQuota)
                        throw new ArgumentException("น้ำหนักเกินโควต้า ไม่สามารถบันทึกน้ำหนักได้ รายละเอียดดังนี้)" + Environment.NewLine +
                            "จำนวนโควต้า : " + _farmerProject.ActualKgPerRai + " กก." + Environment.NewLine +
                            "จำนวนโควต้า (extra) : " + _farmerProject.ExtraQuota + " กก." + Environment.NewLine +
                            "จำนวนโควต้า (รวม) : " + _farmerProject.TotalQuota + " กก." + Environment.NewLine +
                            "ขายไปแล้ว : " + _farmerProject.Sold + " กก." + Environment.NewLine +
                            "น้ำหนักห่อก่อนการแก้ไข : " + _buying.Weight + " กก." + Environment.NewLine +
                            "น้ำหนักใหม่ : " + buyingWeight + " กก." + Environment.NewLine +
                            "เกินมา : " + ((newSold + buyingWeight) - _farmerProject.TotalQuota) + " กก.");
                }

                _buying.Weight = buyingWeight;
                _buying.WeightDate = DateTime.Now;
                _buying.WeightUser = weightUser;

                uow.BuyingRepository.Update(_buying);
                uow.Save();

                //7. การตรวจสอบ X low grade over limit--
                //MasterGrade masterGrade = new MasterGrade();
                //masterGrade = BuyingFacade.MasterGradeRepository().GetAll().Where(mg => mg.Grade == buyingInfo.Grade).SingleOrDefault();

                //if (buyingInfo.Grade.Substring(0, 1) == "X" && masterGrade.Quality == "L")
                //{
                //    var xLowGrade = from b in uow.BuyingRepository.Get(b => b.Crop == registrationFarmer.Crop && b.FarmerCode == registrationFarmer.FarmerCode)
                //                    from mg in BuyingFacade.MasterGradeRepository().GetAll()
                //                    where b.Grade == mg.Grade && mg.Quality == "L" && b.Grade.Substring(0, 1) == "X"
                //                    select b;

                //    //ดึงข้อมูลน้ำหนักรวมของห่อยาทุกห่อที่นำมาขาย เฉพาะที่เป็น X low grade
                //    double xLowGradeWeight = Convert.ToDouble(xLowGrade.Sum(x => x.Weight));

                //    //ดึงข้อมูลจำนวนโควต้ารวมในทุกๆ โปรเจคเพื่อใช้ในการคำนวนหาค่ายเปอร์เซนต์ของ X low grade
                //    List<FarmerProject> farmerprojectList = new List<FarmerProject>();
                //    farmerprojectList = uow.FarmerProjectRepository.Get(fp => fp.Crop == registrationFarmer.Crop && fp.FarmerCode == registrationFarmer.FarmerCode).ToList();

                //    //ดึงข้อมูลการกำหนดจำนวนกิโลกรัมต่อไร่ของแต่ละซัพพลายเออร์
                //    KilogramsPerRaiConfiguration kgPerRai = new KilogramsPerRaiConfiguration();
                //    kgPerRai = uow.KilogramsPerRaiConfigurationRepository.GetSingle(k => k.Crop == registrationFarmer.Crop && k.SupplierCode == registrationFarmer.Farmer.Supplier);

                //    //จำนวนผลรวมของโควต้าปกติ (คิดเป็นกิโลกรัม)
                //    double sumOfTotalNormalQuota = (farmerprojectList.Sum(fp => fp.ActualRai) * kgPerRai.KilogramsPerRai);
                //    double extraPercentBySupplier = kgPerRai.ExtraPercentage == 0 ? 0 : kgPerRai.ExtraPercentage / 100;

                //    //จำนวนผลรวมของโค้วต้าปกติ (รวมกับ จำนวนโค้วต้าที่ให้พิเศษตามแต่ละซัพพลายเออร์)
                //    double sumOfTotalNormalQuotaWithExtraPercent = sumOfTotalNormalQuota == 0 ? 0 : (extraPercentBySupplier * sumOfTotalNormalQuota) + sumOfTotalNormalQuota;
                //    double xLowGradeSoldPercentage = xLowGradeWeight / sumOfTotalNormalQuotaWithExtraPercent;

                //    if (xLowGradeSoldPercentage >= 0.035)
                //        throw new ArgumentException("จำนวนน้ำหนักรวมของยาเกรด X คุณภาพต่ำ (X low grade) ที่ชาวไร่นำมาขาย เกิน 3.5% ทำให้ระบบไม่สามารถบันทึกข้อมูลยาห่อนี้ได้" + Environment.NewLine +
                //    "รายละเอียดเพิ่มเติม" + Environment.NewLine +
                //    "น้ำหนักรวมตามโควต้า : " + sumOfTotalNormalQuotaWithExtraPercent.ToString("N0") + Environment.NewLine +
                //    "ยา X Low ที่ขายได้ : " + (sumOfTotalNormalQuota * 0.035).ToString("N0") + Environment.NewLine +
                //    "น้ำหนักรวมเฉพาะ X Low : " + xLowGradeWeight.ToString("N0") + Environment.NewLine +
                //    "เปอร์เซนต์ของ X Low : " + (xLowGradeSoldPercentage * 100).ToString("N2") + "%" + Environment.NewLine);
                //}
                //End การตรวจสอบ X low grade over limit--    


                //9.(เพิ่มเติม ณ 09/06/2018) รายที่ขายในโควต้าส่วนเกินให้ตรวจสอบด้วยว่าเกรดที่จะบันทึกอยู่ในกลุ่มที่อนุญาตหรือไม่ {CF, B, T} ---
                //if (IsOverQuotaByProject(buyingInfo.Crop, buyingInfo.FarmerCode, buyingInfo.ProjectType) == true
                //    && IsOverQuotaAcceptGrade(buyingInfo.Grade) == false)
                //{
                //    throw new ArgumentException("การขายครั้งนี้เป็นการขายในโควต้าส่วนเกิน ไม่อนุญาตให้บันทึกเกรด " +
                //        buyingInfo.Grade + " เนื่องจากไม่อยู่ในกลุ่มเกรดที่ทางบริษัทกำหนดได้แก่ CF,B,T");
                //}

                //ตรวจสอบโควต้าส่วนเกิน หากตรวจพบชาวไร่ขายมาแล้วเกินโควต้า ให้เข้าเงื่อนไขใหม่ทันทีโดยทำการบันทึกข้อมูลแล้วจบการทำงานไปเลย
                //if (IsOverQuotaByProject(buyingInfo.Crop, buyingInfo.FarmerCode, buyingInfo.ProjectType) == true 
                //    //&&
                //    //IsOverQuotaAcceptGrade(buyingInfo.Grade) == true
                //    )
                //{
                //    buyingInfo.IsExtraQuota = true; //ทำเครื่องหมายว่ายาห่อนี้เป็นการขายในโควต้าส่วนเกิน เพื่อเอาไว้ใช้ในตอนคำนวนโบนัส โดยยาเหล่านี้จะไม่นำมาคิดโบนัสรวม
                //    buyingInfo.Weight = buyingWeight;
                //    buyingInfo.WeightDate = DateTime.Now;
                //    buyingInfo.WeightUser = buyingWeightUser;

                //    uow.BuyingRepository.Update(buyingInfo);

                //    return;
                //}
                //--------


                //if (buyingInfo.Weight != null)//เป็นการแก้ไขน้ำหนักใช่หรือไม่ เพราะต้องหักน้ำหนักเดิมออกก่อนแล้วจจึงบันทึกน้ำหนักใหม่ลงไป
                //{


                //    //if (buyingInfo.IsExtraQuota == false)//เป็น Normal Quota
                //    //{
                //    //    //if (buyingWeight > (farmerQuotaInProject.Balance + buyingInfo.Weight))//ต้องการแก้ไขน้ำหนักเดิม
                //    //    //    throw new ArgumentException("น้ำหนักเกินโควต้า ไม่สามารถบันทึกน้ำหนักได้ โปรดดึงยาออกจำนวน " + (buyingWeight - (farmerQuotaInProject.Balance + buyingInfo.Weight)) + " กิโลกรัม");

                //    //    if ((farmerQuotaInProject.Balance + buyingInfo.Weight) <= 0)
                //    //        throw new ArgumentException("น้ำหนักเกินโควต้า ไม่สามารถบันทึกน้ำหนักได้ รายละเอียดดังนี้)" + Environment.NewLine +
                //    //            "จำนวนโควต้า : " + farmerQuotaInProject.ActualRai * farmerQuotaInProject.ActualKgPerRai + " กก." + Environment.NewLine +
                //    //            "ขายไปแล้ว : " + farmerQuotaInProject.Sold + " กก." + Environment.NewLine +
                //    //            "คงเหลือ : " + farmerQuotaInProject.Balance + " กก.");
                //    //}
                //    //else//เป็น Extra Quota
                //    //{
                //    //    //if (buyingWeight > (farmerQuotaInProject.BalanceExtra + buyingInfo.Weight))//ต้องการแก้ไขน้ำหนักเดิม
                //    //    //    throw new ArgumentException("น้ำหนักเกินโควต้า ไม่สามารถบันทึกน้ำหนักได้ โปรดดึงยาออกจำนวน " + (buyingWeight - (farmerQuotaInProject.BalanceExtra + buyingInfo.Weight)) + " กิโลกรัม");

                //    //    if ((farmerQuotaInProject.BalanceExtra + buyingInfo.Weight) <= 0)
                //    //        throw new ArgumentException("น้ำหนักเกินโควต้า ไม่สามารถบันทึกน้ำหนักได้ รายละเอียดดังนี้)" + Environment.NewLine +
                //    //            "จำนวนโควต้า : " + farmerQuotaInProject.ExtraQuota + " กก." + Environment.NewLine +
                //    //            "ขายไปแล้ว : " + farmerQuotaInProject.SoldInExtra + " กก." + Environment.NewLine +
                //    //            "คงเหลือ : " + farmerQuotaInProject.BalanceExtra + " กก.");
                //    //}
                //}
                //else//เป็นการบันทึกน้ำหนักครั้งแรก (ห่อยาที่ยังไม่มีน้ำหนัก)
                //{
                //    //if (buyingInfo.IsExtraQuota == false)//เป็น Normal Quota
                //    //{
                //    //    //if (buyingWeight > farmerQuotaInProject.Balance)//ต้องการแก้ไขน้ำหนักเดิม
                //    //    //    throw new ArgumentException("น้ำหนักเกินโควต้า ไม่สามารถบันทึกน้ำหนักได้ โปรดดึงยาออกจำนวน " + (buyingWeight - farmerQuotaInProject.Balance) + " กิโลกรัม");

                //    //    if (farmerQuotaInProject.Balance <= 0)
                //    //        throw new ArgumentException("น้ำหนักเกินโควต้า ไม่สามารถบันทึกน้ำหนักได้ รายละเอียดดังนี้)" + Environment.NewLine +
                //    //            "จำนวนโควต้า : " + farmerQuotaInProject.ActualRai * farmerQuotaInProject.ActualKgPerRai + " กก." + Environment.NewLine +
                //    //            "ขายไปแล้ว : " + farmerQuotaInProject.Sold + " กก." + Environment.NewLine +
                //    //            "คงเหลือ : " + farmerQuotaInProject.Balance + " กก.");
                //    //}
                //    //else//เป็น Extra Quota
                //    //{
                //    //    //if (buyingWeight > farmerQuotaInProject.BalanceExtra)//ต้องการแก้ไขน้ำหนักเดิม
                //    //    //    throw new ArgumentException("น้ำหนักเกินโควต้า ไม่สามารถบันทึกน้ำหนักได้ โปรดดึงยาออกจำนวน " + (buyingWeight - farmerQuotaInProject.BalanceExtra) + " กิโลกรัม");

                //    //    if (farmerQuotaInProject.BalanceExtra <= 0)
                //    //        throw new ArgumentException("น้ำหนักเกินโควต้า ไม่สามารถบันทึกน้ำหนักได้ รายละเอียดดังนี้)" + Environment.NewLine +
                //    //            "จำนวนโควต้า : " + farmerQuotaInProject.ExtraQuota + " กก." + Environment.NewLine +
                //    //            "ขายไปแล้ว : " + farmerQuotaInProject.SoldInExtra + " กก." + Environment.NewLine +
                //    //            "คงเหลือ : " + farmerQuotaInProject.BalanceExtra + " กก.");
                //    //}
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CaptureGrade(string baleBarcode, short pricingCrop, byte pricingNumber, string grade, string buyerCode, string buyingGradeUser)
        {
            try
            {
                /*
                /// เงื่อนไขที่ใช้ในการตรวจสอบข้อมูลก่อนการบันทึกเกรด
                /// 1. ใบซื้อจะต้องไม่ถูก Finish
                /// 2. สถานะสัญญาและสถานะการซื้อขายของชาวไร่จะต้องเป็น Active
                /// 3. การบันทึกเกรดจะทำไม่ได้หากมีการบันทึกน้ำหนักซื้อ ไม่ขนขึ้นรถ
                /// 4. หลังจากบันทึก NTRM และ NTRM Inspection ไปแล้ว และต้องการเปลี่ยนเกรด ให้ทำดังนี้
                ///     4.1 ลบข้อมูล NTRM Inspection
                ///     4.2 Set RejectReason column to null
                ///     4.3 Set new grade
                /// 5. เมื่อ Pricing Crop และ Default Crop ไม่ตรงกันจะไม่อนุญาติให้มีการบันทึกเกรดได้
                /// 6. การบันทึกเกรดทุกคร้งจะต้อง Set null in RejectReason เสมอ
                /// 7. มีการตรวจสอบ X low grade over limit
                /// 8. (เพิ่มเติม ณ 09/06/2018) รายที่ขายในโควต้าส่วนเกินให้ตรวจสอบด้วยว่าเกรดที่จะบันทึกอยู่ในกลุ่มที่อนุญาตหรือไม่ {CF, B, T}
                */

                if (baleBarcode == "")
                    throw new ArgumentException("ไม่พบข้อมูล baleBarcode");

                if (grade == "")
                    throw new ArgumentException("ไม่พบข้อมูล grade");

                if (buyingGradeUser == "")
                    throw new ArgumentException("ไม่พบข้อมูล buyingGradeUser");

                var _buying = GetByBaleBarcode(baleBarcode);
                if (_buying == null)
                    throw new ArgumentException("ไม่พบข้อมูลห่อยาหมายเลขบาร์โค้ต " + baleBarcode + " นี้ในระบบ");

                if (_buying.Weight != null)
                    throw new ArgumentException("ยาห่อนี้ถูกบันทึกน้ำหนักซื้อไปแล้ว ไม่สามารถเปลี่ยนแปลงแก้ไขเกรดซื้อได้");

                if (_buying.TransportationDocumentCode != null)
                    throw new ArgumentException("ยาห่อนี้ถูกบันทึกข้อมูลการขนส่งไปแล้ว ไม่สามารถเปลี่ยนแปลงแก้ไขเกรดซื้อได้");

                if (_buying.BuyingDocument.IsFinish == true)
                    throw new ArgumentException("ใบซื้อนี้ถูก Finish แล้ว ไม่สามารถบันทึกเกรดซื้อได้");

                /// 7. มีการตรวจสอบ X low grade over limit *
                //var masterGrade = BuyingFacade.MasterGradeRepository().GetAll().Where(mg => mg.Grade == grade).SingleOrDefault();

                //if (grade.Substring(0, 1) == "X" && masterGrade.Quality == "L")
                //{
                //    var xLowGrade = from b in uow.BuyingRepository.Get(b => b.Crop == registrationFarmer.Crop && b.FarmerCode == registrationFarmer.FarmerCode)
                //                    from mg in BuyingFacade.MasterGradeRepository().GetAll()
                //                    where b.Grade == mg.Grade && mg.Quality == "L" && b.Grade.Substring(0, 1) == "X"
                //                    select b;

                //    //ดึงข้อมูลน้ำหนักรวมของห่อยาทุกห่อที่นำมาขาย เฉพาะที่เป็น X low grade
                //    double xLowGradeWeight = Convert.ToDouble(xLowGrade.Sum(x => x.Weight));

                //    //ดึงข้อมูลจำนวนโควต้ารวมในทุกๆ โปรเจคเพื่อใช้ในการคำนวนหาค่ายเปอร์เซนต์ของ X low grade
                //    var farmerprojectList = uow.FarmerProjectRepository.Get(fp => fp.Crop == registrationFarmer.Crop && fp.FarmerCode == registrationFarmer.FarmerCode).ToList();

                //    //ดึงข้อมูลการกำหนดจำนวนกิโลกรัมต่อไร่ของแต่ละซัพพลายเออร์
                //    var kgPerRai = uow.KilogramsPerRaiConfigurationRepository.GetSingle(k => k.Crop == registrationFarmer.Crop && k.SupplierCode == registrationFarmer.Farmer.Supplier);

                //    //จำนวนผลรวมของโควต้าปกติ (คิดเป็นกิโลกรัม)
                //    double sumOfTotalNormalQuota = (farmerprojectList.Sum(fp => fp.ActualRai) * kgPerRai.KilogramsPerRai);
                //    double extraPercentBySupplier = kgPerRai.ExtraPercentage == 0 ? 0 : kgPerRai.ExtraPercentage / 100;

                //    //จำนวนผลรวมของโค้วต้าปกติ (รวมกับ จำนวนโค้วต้าที่ให้พิเศษตามแต่ละซัพพลายเออร์)
                //    double sumOfTotalNormalQuotaWithExtraPercent = sumOfTotalNormalQuota == 0 ? 0 : (extraPercentBySupplier * sumOfTotalNormalQuota) + sumOfTotalNormalQuota;
                //    double xLowGradeSoldPercentage = xLowGradeWeight / sumOfTotalNormalQuotaWithExtraPercent;

                //    if (xLowGradeSoldPercentage >= 0.035)
                //        throw new ArgumentException("จำนวนน้ำหนักรวมของยาเกรด X คุณภาพต่ำ (X low grade) ที่ชาวไร่นำมาขาย เกิน 3.5% ทำให้ระบบไม่สามารถบันทึกเกรด " + grade + " นี้ได้" + Environment.NewLine +
                //    "รายละเอียดเพิ่มเติม" + Environment.NewLine +
                //    "น้ำหนักรวมตามโควต้า : " + sumOfTotalNormalQuotaWithExtraPercent.ToString("N0") + Environment.NewLine +
                //    "ยา X Low ที่ขายได้ : " + (sumOfTotalNormalQuota * 0.035).ToString("N0") + Environment.NewLine +
                //    "น้ำหนักรวมเฉพาะ X Low : " + xLowGradeWeight.ToString("N0") + Environment.NewLine +
                //    "เปอร์เซนต์ของ X Low : " + (xLowGradeSoldPercentage * 100).ToString("N2") + "%" + Environment.NewLine);
                //}

                var _crop = uow.CropRepository.GetSingle(x => x.DefaultStatus == true);

                ///เมื่อ Pricing Crop และ Default Crop ไม่ตรงกันจะไม่อนุญาติให้มีการบันทึกเกรดได้ *
                if (_crop.Crop1 != pricingCrop)
                    throw new ArgumentException("ปีปัจจุบันที่เซตไว้ในระบบ กับปีของชุดราคาไม่ตรงกัน" +
                        " ไม่สามารถบันทึกเกรดได้ โปรดแจ้งแผนกไอทีเพื่อแก้ไขโดยด่วน");

                ///หากต้องการเปลี่ยนจากยา Back ไปเป็นยาที่มีเกรดซื้อตามปกติ จะต้องมีการเซต Reject Reason to null
                if (_buying.RejectReason != null)
                    _buying.RejectReason = null;

                ///(เพิ่มเติม ณ 09/06/2018) รายที่ขายในโควต้าส่วนเกินให้ตรวจสอบด้วยว่าเกรดที่จะบันทึกอยู่ในกลุ่มที่อนุญาตหรือไม่ {CF, B, T}
                if (IsOverExtraQuota(_buying.Crop, _buying.FarmerCode, _buying.ProjectType) == true &&
                    IsAcceptGradeInOverQuota(grade) == false)
                    throw new ArgumentException("การขายครั้งนี้เป็นการขายในโควต้าส่วนเกิน ไม่อนุญาตให้บันทึกเกรด " +
                        grade + " เนื่องจากไม่อยู่ในกลุ่มเกรดที่ทางบริษัทกำหนดได้แก่ CF,B,T");

                _buying.PricingCrop = pricingCrop;
                _buying.PricingNumber = pricingNumber;
                _buying.Grade = grade;
                _buying.BuyerCode = buyerCode;
                _buying.GradeUser = buyingGradeUser;
                _buying.GradeDate = DateTime.Now;

                uow.BuyingRepository.Update(_buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CaptureAll(string baleBarcode, short pricingCrop, byte pricingNumber, string grade, string buyerCode, decimal weight,
            string transportationCode, string username)
        {
            try
            {
                if (string.IsNullOrEmpty(baleBarcode))
                    throw new ArgumentException("โปรดระบุ Bale Barcode");

                if (grade == null)
                    throw new ArgumentException("ไม่ได้ระบุเกรดซื้อ (grade) โปรดตรวจสอบใหม่อีกครั้ง");

                if (string.IsNullOrEmpty(buyerCode))
                    throw new ArgumentException("ไม่ได้ระบุผู้ซื้อ (buyer) โปรดตรวจสอบใหม่อีกครั้ง");

                if (string.IsNullOrEmpty(username))
                    throw new ArgumentException("ไม่ได้ระบุชื่อผู้บันทึกข้อมูล (username)");

                if (weight > 70)
                    throw new ArgumentException("น้ำหนักต่อห่อเกิน 70 กก. ระบบไม่อนุญาตให้บันทึกข้อมูล");

                if (weight < 1)
                    throw new ArgumentException("น้ำหนักต่อห่อต้อง 1 กก. ขึ้นไป ระบบไม่อนุญาตให้บันทึกข้อมูล");

                if (string.IsNullOrEmpty(transportationCode))
                    throw new ArgumentException("ไม่ได้ระบุรถบรรทุก (transportation code) โปรดตรวจสอบใหม่อีกครั้ง");

                ///ตรวจสอบใบนำส่งในระบบ   
                var _transport = Helper.TransportationHelper.GetByTransportationCode(transportationCode);
                if (_transport == null)
                    throw new ArgumentException("ไม่พบข้อมูลใบนำส่งหมายเลข " + transportationCode + " นี้");

                if (_transport.IsFinish == true)
                    throw new ArgumentException("ใบนำส่งหมายเลข " + transportationCode +
                        " ถูกล็อคเพื่อพิมพ์เอกสารแล้ว ไม่สามารถนำยาขึ้นรถคันนี้ได้ (ทะเบียนรถ " + _transport.TruckNumber + ")");

                var _buying = uow.BuyingRepository
                    .GetSingle(x => x.BaleBarcode == baleBarcode,
                    x => x.BuyingGrade);
                if (_buying == null)
                    throw new ArgumentException("ไม่พบข้อมูลห่อยาหมายเลขบาร์โค้ต " + baleBarcode + " นี้ในระบบ");

                var _document = uow.BuyingDocumentRepository
                    .GetSingle(x => x.Crop == _buying.Crop &&
                    x.BuyingStationCode == _buying.BuyingStationCode &&
                    x.FarmerCode == _buying.FarmerCode &&
                    x.BuyingDocumentNumber == _buying.BuyingDocumentNumber);

                ///สถานะใบเวาเชอร์จะต้องเป็น unfinish 
                if (_document.IsFinish == true)
                    throw new ArgumentException("ใบซื้อของชาวไร่รายนี้ถูก Finish แล้ว ไม่สามารถบันทึกข้อมูลได้");

                if (_document.IsPrinted == true)
                    throw new ArgumentException("ใบซื้อของชาวไร่รายนี้ถูกพิมพ์ออกจากระบบแล้ว ไม่สามารถบันทึกข้อมูลได้");

                // กรณี buying weight = null เกิดในกรณีที่ห่อยาที่ยังไม่ได้บันทึกน้ำหนัก
                // ห่อยาที่มีการแก้ไขน้ำหนักใหม่ ให้ตรวจสอบว่าน้ำหนักที่จะนำขึ้นบนรถบรรทุกเกินที่กำหนดหรือยัง
                if (_buying.Weight == null)
                {
                    if (_transport.TotalWeight + weight > _transport.MaximumWeight)
                        throw new ArgumentException("น้ำหนักห่อยาที่อยู่บนรถ " + _transport.TotalWeight + " กก." + Environment.NewLine +
                            "เมื่อรวมกับน้ำหนักห่อยานี้อีก " + weight + " กก. เกินน้ำหนักสูงสุดที่กำหนด (" + _transport.MaximumWeight + ")" + Environment.NewLine +
                            "ไม่สามารถบันทึกข้อมูลยาห่อนี้ได้");
                }
                else
                {
                    // ห่อยาที่แก้ไขน้ำหนัก ให้หักน้ำหนักเดิมออกก่อน แล้วจึงค่อยนำไปคำนวณว่าเกินจากที่สามารถบันทุกได้หรือไม่
                    var realWeight = (decimal)(_transport.TotalWeight - _buying.Weight);
                    if (realWeight + weight > _transport.MaximumWeight)
                        throw new ArgumentException("น้ำหนักห่อยาที่อยู่บนรถ " + realWeight.ToString("N1") + " กก." + Environment.NewLine +
                            "เมื่อรวมกับน้ำหนักห่อยานี้อีก " + weight + " กก. เกินน้ำหนักสูงสุดที่กำหนด (" + _transport.MaximumWeight + ")" + Environment.NewLine +
                            "ไม่สามารถบันทึกข้อมูลยาห่อนี้ได้");
                }

                //ดึงข้อมูลจำนวนโควต้ารวมในทุกๆ โปรเจคเพื่อใช้ในการคำนวนหาค่ายเปอร์เซนต์ของ X low grade
                var farmerprojects = Helper.FarmerProjectHelper
                    .GetQuotaAndSoldByFarmerVersion2(_buying.Crop, _buying.FarmerCode);
                if (farmerprojects.Count() <= 0)
                    throw new ArgumentException("ไม่พบโควต้าของชาวไร่รายนี้ในทุกๆ โปรเจค");

                var quota = farmerprojects.SingleOrDefault(x => x.ProjectType == _buying.ProjectType);
                if (quota == null)
                    throw new ArgumentException("ไม่พบโควต้าของชาวไร่รายนี้ในโปรเจค " + _buying.ProjectType);


                //==========================================
                //ในปี 2024 ยกเลิกการเติม A ในกรณีที่นำยาส่วนเกินมาขาย
                //==========================================
                var finalGrade = grade;


                //==========================================
                //เงื่อนไขเพิ่มเติมปี 2023 หากมีการขายยาเกินโควต้าปกติไปแล้ว 
                //หลังจากมีการเปลี่ยนเงื่อนไขเป็นทุกห่อ +5.5 บาท
                //ให้ IsExtraQuota = true
                //==========================================
                //if (quota.Sold > quota.ActualKgPerRai)
                //    _buying.IsExtraQuota = true;

                //var finalGrade = "";
                //grade = grade.Replace("A", "");
                //finalGrade = grade + "A";

                //===================================================
                //ตรงนี้เป็นเงื่อนไขใหม่ ห่อยาที่ยังไม่เกินโควต้า 400 kg/rai ให้ +5.5 บาท 
                //===================================================
                //if (quota.Sold < quota.ActualKgPerRai)
                //{
                //    _buying.IsExtraQuota = false;
                //    finalGrade = grade + "A";
                //}
                //else
                //{
                //    if (_buying.Grade != null)
                //    {
                //        if (_buying.Grade.Contains("A"))
                //        {
                //            _buying.IsExtraQuota = false;
                //            finalGrade = grade + "A";
                //        }
                //        else
                //        {
                //            _buying.IsExtraQuota = true;
                //            finalGrade = grade;
                //        }
                //    }
                //    else
                //    {
                //        _buying.IsExtraQuota = true;
                //        finalGrade = grade;
                //    }
                //}

                //====================================================
                //ตรงนี้เป็นเงื่อนไขเดิมคือ หากมีกาดเปิดโควต้าส่วนเกินในระบบแล้วจะถือว่า
                //ทุกห่อที่มาขายหลังจากนี้จะไม่ได้รับโบนัส +5.5 บาทค่าปุ๋ย
                //====================================================

                //if (quota.ExtraQuota > 0)
                //{
                //    finalGrade = grade;
                //    _buying.IsExtraQuota = true;
                //}
                //else
                //{
                //    var totalSold = quota.Sold - (_buying.Weight == null ? 0 : _buying.Weight) + weight;
                //    if (totalSold > quota.ActualKgPerRai)
                //        throw new ArgumentException("Over in normal quota." + Environment.NewLine +
                //            "==========================" + Environment.NewLine +
                //            "โควต้าคงเหลือล่าสุด " + Convert.ToDecimal(quota.Balance).ToString("N1") + " กก." + Environment.NewLine +
                //            "โปรดดึงยาออกจำนวน " + Convert.ToDecimal(totalSold - quota.ActualKgPerRai).ToString("N1") + " กก. " + Environment.NewLine +
                //            "เพื่อให้สามารถขายได้ในราคาพิเศษ" + Environment.NewLine +
                //            "หรือขอเปิดโควต้าส่วนเกินเพื่อขายในราคาปกติ");

                //    finalGrade = grade + "A";
                //    _buying.IsExtraQuota = false;
                //}

                //if (_buying.IsExtraQuota == false)
                //    if (quota.Sold - (_buying.Weight != null ? _buying.Weight : 0) + weight > quota.ActualKgPerRai)
                //        _buying.IsExtraQuota = true;

                /* ตรวจสอบน้ำหนัก                
                if (_buying.Weight == null)
                {
                    /// กรณีเป็นการบันทึกน้ำหนักครั้งแรก
                    /// 
                    if (_buying.IsExtraQuota == false)
                    {
                        if (weight > quota.Balance)
                            throw new ArgumentException("น้ำหนักเกินโควต้าปกติ(Normal) ไม่สามารถบันทึกน้ำหนักได้ รายละเอียดดังนี้)" + Environment.NewLine +
                                "จำนวนโควต้า : " + quota.ActualKgPerRai + " กก." + Environment.NewLine +
                                "ขายไปแล้ว : " + quota.Sold + " กก." + Environment.NewLine +
                                "คงเหลือ : " + quota.Balance + " กก." + Environment.NewLine +
                                "ยาห่อนี้หนัก : " + weight + " กก." + Environment.NewLine +
                                "ต้องดึงยาออก จำนวน : " + (weight - quota.Balance) + " กก.");
                    }
                    else
                    {
                        if (weight > quota.BalanceExtra)
                            throw new ArgumentException("น้ำหนักเกินโควต้าพิเศษ(Extra) ไม่สามารถบันทึกน้ำหนักได้ รายละเอียดดังนี้)" + Environment.NewLine +
                                "จำนวนโควต้า : " + quota.ExtraQuota + " กก." + Environment.NewLine +
                                "ขายไปแล้ว : " + quota.SoldInExtra + " กก." + Environment.NewLine +
                                "คงเหลือ : " + quota.BalanceExtra + " กก." + Environment.NewLine +
                                "ยาห่อนี้หนัก : " + weight + " กก." + Environment.NewLine +
                                "ต้องดึงยาออก จำนวน : " + (weight - quota.BalanceExtra) + " กก.");
                    }
                }
                else
                {
                    /// กรณีเป็นการแก้ไขน้ำหนักห่อเดิม
                    /// 

                    if (_buying.IsExtraQuota == false)
                    {
                        var newSold = quota.Sold - _buying.Weight;
                        if (newSold + weight > quota.ActualKgPerRai)
                            throw new ArgumentException("น้ำหนักเกินโควต้าปกติ(Normal) ไม่สามารถบันทึกน้ำหนักได้ รายละเอียดดังนี้)" + Environment.NewLine +
                                "จำนวนโควต้า : " + quota.ActualKgPerRai + " กก." + Environment.NewLine +
                                "ขายไปแล้ว : " + quota.Sold + " กก." + Environment.NewLine +
                                "น้ำหนักห่อก่อนการแก้ไข : " + _buying.Weight + " กก." + Environment.NewLine +
                                "น้ำหนักใหม่ : " + weight + " กก." + Environment.NewLine +
                                "เกินมา : " + ((newSold + weight) - quota.TotalQuota) + " กก.");
                    }
                    else
                    {
                        var newSold = quota.SoldInExtra - _buying.Weight;
                        if (newSold + weight > quota.ExtraQuota)
                            throw new ArgumentException("น้ำหนักเกินโควต้าพิเศษ(Extra) ไม่สามารถบันทึกน้ำหนักได้ รายละเอียดดังนี้)" + Environment.NewLine +
                                "จำนวนโควต้า : " + quota.ExtraQuota + " กก." + Environment.NewLine +
                                "ขายไปแล้ว : " + quota.SoldInExtra + " กก." + Environment.NewLine +
                                "น้ำหนักห่อก่อนการแก้ไข : " + _buying.Weight + " กก." + Environment.NewLine +
                                "น้ำหนักใหม่ : " + weight + " กก." + Environment.NewLine +
                                "เกินมา : " + ((newSold + weight) - quota.ExtraQuota) + " กก.");
                    }
                }
                */

                // update buying date on Buying Document table.
                // ถ้าเป็นยาห่อแรกให้ทำการ update buying date ด้วย
                if (_document.BuyingDate == null)
                {
                    _document.BuyingDate = DateTime.Now;
                    uow.BuyingDocumentRepository.Update(_document);
                    uow.Save();
                }

                _buying.RejectReason = null;
                _buying.PricingCrop = pricingCrop;
                _buying.PricingNumber = pricingNumber;
                _buying.Grade = finalGrade;
                _buying.GradeDate = DateTime.Now;
                _buying.GradeUser = username;
                _buying.BuyerCode = buyerCode;
                _buying.Weight = weight;
                _buying.WeightDate = DateTime.Now;
                _buying.WeightUser = username;
                _buying.TransportationDocumentCode = transportationCode;
                _buying.LoadBaleToTruckDate = DateTime.Now;
                _buying.LoadBaleToTruckUser = username;

                uow.BuyingRepository.Update(_buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ChangeGradeAtWeightScale(string baleBarcode, short crop, byte pricingNumber, string newGrade, string buyerCode, string buyingGradeUser)
        {
            try
            {
                if (baleBarcode == "")
                    throw new ArgumentException("ไม่พบข้อมูล baleBarcode");

                if (pricingNumber.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล pricingNumber");

                if (newGrade == "")
                    throw new ArgumentException("ไม่พบข้อมูล grade");

                if (buyingGradeUser == "")
                    throw new ArgumentException("ไม่พบข้อมูล buyingGradeUser");

                var _buying = GetByBaleBarcode(baleBarcode);
                if (_buying == null)
                    throw new ArgumentException("ไม่พบข้อมูลห่อยาหมายเลขบาร์โค้ต " + baleBarcode + " นี้ในระบบ");

                if (_buying.Weight != null)
                    throw new ArgumentException("ยาห่อนี้ถูกบันทึกน้ำหนักซื้อไปแล้ว ไม่สามารถเปลี่ยนแปลงแก้ไขเกรดซื้อได้");

                if (_buying.TransportationDocumentCode != null)
                    throw new ArgumentException("ยาห่อนี้ถูกบันทึกข้อมูลการขนส่งไปแล้ว ไม่สามารถเปลี่ยนแปลงแก้ไขเกรดซื้อได้");

                if (_buying.BuyingDocument.IsFinish == true)
                    throw new ArgumentException("ใบซื้อนี้ถูก Finish แล้ว ไม่สามารถบันทึกเกรดซื้อได้");

                //var masterGrade = BuyingFacade.MasterGradeRepository().GetAll().Where(mg => mg.Grade == newGrade).SingleOrDefault();

                //if (newGrade.Substring(0, 1) == "X" && masterGrade.Quality == "L")
                //{
                //    var xLowGrade = from b in uow.BuyingRepository.Get(b => b.Crop == registrationFarmer.Crop && b.FarmerCode == registrationFarmer.FarmerCode)
                //                    from mg in BuyingFacade.MasterGradeRepository().GetAll()
                //                    where b.Grade == mg.Grade && mg.Quality == "L" && b.Grade.Substring(0, 1) == "X"
                //                    select b;

                //    //ดึงข้อมูลน้ำหนักรวมของห่อยาทุกห่อที่นำมาขาย เฉพาะที่เป็น X low grade
                //    double xLowGradeWeight = Convert.ToDouble(xLowGrade.Sum(x => x.Weight));


                //    //ดึงข้อมูลจำนวนโควต้ารวมในทุกๆ โปรเจคเพื่อใช้ในการคำนวนหาค่ายเปอร์เซนต์ของ X low grade
                //    var farmerprojectList = uow.FarmerProjectRepository.Get(fp => fp.Crop == registrationFarmer.Crop && fp.FarmerCode == registrationFarmer.FarmerCode).ToList();


                //    //ดึงข้อมูลการกำหนดจำนวนกิโลกรัมต่อไร่ของแต่ละซัพพลายเออร์
                //    var kgPerRai = uow.KilogramsPerRaiConfigurationRepository.GetSingle(k => k.Crop == registrationFarmer.Crop && k.SupplierCode == registrationFarmer.Farmer.Supplier);

                //    //จำนวนผลรวมของโควต้าปกติ (คิดเป็นกิโลกรัม)
                //    double sumOfTotalNormalQuota = (farmerprojectList.Sum(fp => fp.ActualRai) * kgPerRai.KilogramsPerRai);

                //    double extraPercentBySupplier = kgPerRai.ExtraPercentage == 0 ? 0 : kgPerRai.ExtraPercentage / 100;

                //    //จำนวนผลรวมของโค้วต้าปกติ (รวมกับ จำนวนโค้วต้าที่ให้พิเศษตามแต่ละซัพพลายเออร์)
                //    double sumOfTotalNormalQuotaWithExtraPercent = sumOfTotalNormalQuota == 0 ? 0 : (extraPercentBySupplier * sumOfTotalNormalQuota) + sumOfTotalNormalQuota;

                //    double xLowGradeSoldPercentage = xLowGradeWeight / sumOfTotalNormalQuotaWithExtraPercent;

                //    if (xLowGradeSoldPercentage >= 3.5 / 100)
                //        throw new ArgumentException("จำนวนน้ำหนักรวมของยาเกรด X คุณภาพต่ำ (X low grade) ที่ชาวไร่นำมาขาย เกิน 3.5% ทำให้ระบบไม่สามารถบันทึกเกรด " + newGrade + " นี้ได้" + Environment.NewLine +
                //    "รายละเอียดเพิ่มเติม" + Environment.NewLine +
                //    "น้ำหนักรวมตามโควต้า : " + sumOfTotalNormalQuotaWithExtraPercent.ToString("N0") + Environment.NewLine +
                //    "น้ำหนักรวมเฉพาะ X Low : " + xLowGradeWeight.ToString("N0") + Environment.NewLine +
                //    "เปอร์เซนต์ของ X Low : " + (xLowGradeSoldPercentage * 100).ToString("N2") + "%" + Environment.NewLine);
                //}

                _buying.PricingCrop = crop;
                _buying.PricingNumber = pricingNumber;
                _buying.Grade = newGrade;
                _buying.BuyerCode = buyerCode;
                _buying.ChangeGradeUser = buyingGradeUser;
                _buying.RejectReason = null;

                uow.BuyingRepository.Update(_buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveBuyingGrade(string baleBarcode)
        {
            try
            {
                if (baleBarcode == "") throw new ArgumentException("ไม่พบข้อมูล baleBarcode");

                var _buying = GetByBaleBarcode(baleBarcode);
                if (_buying == null)
                    throw new ArgumentException("ไม่พบข้อมูลห่อยาหมายเลขบาร์โค้ต " + baleBarcode + " นี้ในระบบ");

                if (_buying.Weight != null)
                    throw new ArgumentException("ยาห่อนี้ถูกบันทึกน้ำหนักซื้อไปแล้ว ไม่สามารถเปลี่ยนแปลงแก้ไขเกรดซื้อได้");

                if (_buying.TransportationDocumentCode != null)
                    throw new ArgumentException("ยาห่อนี้ถูกบันทึกข้อมูลการขนส่งไปแล้ว ไม่สามารถเปลี่ยนแปลงแก้ไขเกรดซื้อได้");

                if (_buying.BuyingDocument.IsFinish == true)
                    throw new ArgumentException("ใบซื้อนี้ถูก Finish แล้ว ไม่สามารถบันทึกเกรดซื้อได้");

                _buying.PricingCrop = null;
                _buying.PricingNumber = null;
                _buying.Grade = null;
                _buying.Buyer = null;
                _buying.GradeDate = null;
                _buying.GradeUser = null;

                uow.BuyingRepository.Update(_buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RejectBaleOnTheScale(string baleBarcode, string rejectReason, string rejectUser)
        {
            try
            {
                if (baleBarcode == "")
                    throw new ArgumentException("ไม่พบข้อมูล baleBarcode");

                if (rejectReason == "")
                    throw new ArgumentException("ไม่พบข้อมูล rejectReason");

                if (rejectUser == "")
                    throw new ArgumentException("ไม่พบข้อมูล rejectUser");

                var _buying = GetByBaleBarcode(baleBarcode);
                if (_buying == null)
                    throw new ArgumentException("ไม่พบข้อมูลยาห่อนี้ในระบบ");

                if (_buying.BuyingDocument.IsFinish == true)
                    throw new ArgumentException("ใบซื้อนี้ถูก Finish แล้ว ไม่สามารถดำเนินการใดได้อีก");

                _buying.PricingCrop = null;
                _buying.PricingNumber = null;
                _buying.Grade = null;
                _buying.Weight = null;

                _buying.ChangeGradeUser = rejectUser;
                _buying.RejectReason = rejectReason;
                _buying.GradeDate = DateTime.Now;

                uow.BuyingRepository.Update(_buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RejectBale(string baleBarcode, string rejectReason, string rejectUser, string buyerCode)
        {
            try
            {
                if (baleBarcode == "")
                    throw new ArgumentException("ไม่พบข้อมูล baleBarcode");

                if (rejectReason == "")
                    throw new ArgumentException("ไม่พบข้อมูล rejectReason");

                if (rejectUser == "")
                    throw new ArgumentException("ไม่พบข้อมูล rejectUser");

                if (buyerCode == "")
                    throw new ArgumentException("ไม่พบข้อมูล Buyer Code");

                var _buying = GetByBaleBarcode(baleBarcode);
                if (_buying == null)
                    throw new ArgumentException("ไม่พบข้อมูลยาห่อนี้ในระบบ");

                //if (_buying.Grade != null)
                //    throw new ArgumentException("ยาห่อนี้มีการบันทึกข้อมูลเกรดแล้ว ไม่สามารถแบ็คยาได้");

                //if (_buying.Weight != null)
                //    throw new ArgumentException("ยาห่อนี้มีการบันทึกข้อมูลน้ำหนักแล้ว ไม่สามารถแบ็คยาได้");

                //if (_buying.TransportationDocumentCode != null)
                //    throw new ArgumentException("ยาห่อนี้ถูกขนขึ้นรถบรรทุกแล้ว ไม่สามารถแบ็คยาได้");

                if (_buying.BuyingDocument.IsFinish == true)
                    throw new ArgumentException("ใบซื้อนี้ถูก Finish แล้ว ไม่สามารถดำเนินการใดได้อีก");

                _buying.PricingCrop = null;
                _buying.PricingNumber = null;
                _buying.Grade = null;

                /// เพิ่มตรงนี้เข้ามาเนื่องจากปี 2020 มีการยุบจุดต่างๆ เหลือจุดชั่งจุดเดียว
                /// 
                _buying.TransportationDocumentCode = null;
                _buying.LoadBaleToTruckDate = null;
                _buying.LoadBaleToTruckUser = null;
                _buying.Weight = null;
                _buying.WeightDate = null;
                _buying.WeightUser = null;

                _buying.BuyerCode = buyerCode;
                _buying.RejectReason = rejectReason;
                _buying.GradeUser = rejectUser;
                _buying.GradeDate = DateTime.Now;

                uow.BuyingRepository.Update(_buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveRejectBale(string baleBarcode)
        {
            try
            {
                if (baleBarcode == "")
                    throw new ArgumentException("ไม่พบข้อมูล baleBarcode");

                var _buying = GetByBaleBarcode(baleBarcode);
                if (_buying == null)
                    throw new ArgumentException("ไม่พบข้อมูลยาห่อนี้ในระบบ");

                if (_buying.BuyingDocument.IsFinish == true)
                    throw new ArgumentException("ใบซื้อของชาวไร่รายนี้ถูก Finish แล้ว ไม่สามารถดำเนินการได้");

                _buying.RejectReason = null;
                uow.BuyingRepository.Update(_buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UnRisterBarcode(string baleBarcode)
        {
            try
            {
                /// 1.หากมีการบันทึกเกรดซื้อแล้วจะไม่อนุญาตให้ลบข้อมูลได้
                /// 2.หากมีการบันทึกข้อมูล Reject, NTRM จะไม่อนุญาตให้ลบข้อมูลได้
                /// 
                var _buying = GetByBaleBarcode(baleBarcode);
                if (_buying == null)
                    throw new ArgumentException("ไม่พบข้อมูลยาหมายเลขบาร์โค้ต " + baleBarcode);

                if (_buying.Grade != null)
                    throw new ArgumentException("ยาหมายเลขบาร์โค้ต " +
                        _buying.BaleBarcode + " นี้ มีการบันทึกเกรดซื้อในระบบแล้ว เป็นเกรด " +
                        _buying.Grade + " ไม่สามารถลบข้อมูลได้");

                if (_buying.RejectReason != null)
                    throw new ArgumentException("ยาหมายเลขบาร์โค้ต " +
                        _buying.BaleBarcode + " นี้ มีการบันทึกข้อมูล Reject ในระบบแล้ว คือ " +
                        _buying.RejectReason + " ไม่สามารถลบข้อมูลได้");

                if (_buying.BuyingDocument.IsFinish == true)
                    throw new ArgumentException("ใบซื้อนี้ถูก Finish แล้วไม่สามารถลบข้อมูลได้");

                uow.BuyingRepository.Remove(_buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InputMoistureResult(string baleBarcode, decimal moistureResutt, DateTime moistureResultDate, string moistureResultUser)
        {
            try
            {
                var buying = GetByBaleBarcode(baleBarcode);
                if (buying == null)
                    throw new ArgumentException("ไม่พบข้อมูลห่อยาหมายเลข " + baleBarcode + " นี้ในระบบ");

                if (buying.Grade == null)
                    throw new ArgumentException("ไม่พบข้อมูลเกรดซื้อของยาห่อนี้ ไม่สามารถบันทึกผลตรวจได้");

                if (buying.TransportationDocumentCode == null)
                    throw new ArgumentException("ไม่พบข้อมูลรหัสใบนำส่งของยาห่อนี้ ไม่สามารถบันทึกผลตรวจได้");

                buying.MoistureResult = moistureResutt;
                buying.MoistureResultDate = moistureResultDate;
                buying.MoistureResultUser = moistureResultUser;

                uow.BuyingRepository.Update(buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RemoveMoistureResult(string baleBarcode)
        {
            try
            {
                var buying = GetByBaleBarcode(baleBarcode);
                if (buying == null)
                    throw new ArgumentException("ไม่พบข้อมูลห่อยาหมายเลข " + baleBarcode + " นี้ในระบบ");

                buying.MoistureResult = null;
                buying.MoistureResultDate = null;
                buying.MoistureResultUser = null;

                uow.BuyingRepository.Update(buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UnlockBuyingInfo(string baleBarcode)
        {
            try
            {
                var buying = uow.BuyingRepository
                    .GetSingle(x => x.BaleBarcode == baleBarcode,
                    x => x.TransportationDocument,
                    x => x.BuyingDocument);

                if (buying == null)
                    throw new ArgumentException("ไม่พบข้อมูลห่อยาหมายเลข " + baleBarcode + " นี้ในระบบ");

                if (buying.TransportationDocument.IsFinish == true)
                    throw new ArgumentException("ใบนำส่งที่บรรทุกยาห่อนี้ถูก Finish แล้ว กรุณาปลดล็อคก่อน");

                if (buying.BuyingDocument.IsFinish == true)
                    throw new ArgumentException("ใบเวาเชอร์ของยาห่อนี้ถูก Finish แล้ว กรุณาปลดล็อคก่อน");

                buying.Weight = null;
                buying.WeightDate = null;
                buying.WeightUser = null;

                uow.BuyingRepository.Update(buying);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Buying GetByBaleBarcode(string baleBarcode)
        {
            return uow.BuyingRepository
                .GetSingle(b => b.BaleBarcode == baleBarcode,
                b => b.BuyingDocument,
                b => b.BuyingGrade);
        }

        public List<Buying> GetByFarmerProject(short crop, string farmerCode, string projectType)
        {
            return uow.BuyingRepository
                .Query(b => b.Crop == crop && b.FarmerCode == farmerCode && b.ProjectType == projectType)
                .ToList();
        }

        public List<Buying> GetByFarmer(short crop, string farmerCode)
        {
            return uow.BuyingRepository
                .Get(b => b.Crop == crop &&
                b.FarmerCode == farmerCode,
                null,
                x => x.BuyingGrade)
                .ToList();
        }

        public List<Buying> GetByBuyingDocument(short crop, string buyingStationCode, string farmerCode, short buyingDocumentNumber)
        {
            return uow.BuyingRepository
                .Query(b => b.Crop == crop &&
                b.BuyingStationCode == buyingStationCode &&
                b.FarmerCode == farmerCode &&
                b.BuyingDocumentNumber == buyingDocumentNumber,
                null,
                b => b.BuyingGrade)
                .ToList();
        }

        public List<Buying> GetByTransportationCode(string transportationCode)
        {
            return uow.BuyingRepository
                .Query(b => b.TransportationDocumentCode == transportationCode).ToList();
        }

        public List<Buying> GetNestingBale(short crop, string farmerCode)
        {
            try
            {
                return uow.BuyingRepository.Query(x => x.RejectReason == "" &&
                x.Crop == crop && x.FarmerCode == farmerCode)
                .ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<m_SummaryBuyingByPosition> GetSummaryBuyingOfFarmerByPosition(short crop, string farmerCode)
        {
            try
            {
                var buyingList = uow.BuyingRepository.Query(b => b.Crop == crop
                    && b.FarmerCode == farmerCode
                    && b.Weight != null, null,
                    b => b.BuyingGrade)
                    .ToList();

                return (from b in buyingList
                        group b by new { Grade = b.Grade.Substring(0, 1) } into g
                        select new m_SummaryBuyingByPosition
                        {
                            Position = g.Key.Grade,
                            TotalWeight = Convert.ToDecimal(g.Sum(b => b.Weight)),
                            Percentages = Convert.ToDecimal(g.Sum(b => b.Weight) / buyingList.Sum(b => b.Weight) * 100)
                        }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<m_SummaryBuyingByQuality> GetSummaryBuyingOfFarmerByQuality(short crop, string farmerCode)
        {
            try
            {
                List<Buying> buyingList = new List<Buying>();
                buyingList = uow.BuyingRepository
                    .Query(b => b.Crop == crop &&
                    b.FarmerCode == farmerCode &&
                    b.Weight != null,
                    null,
                    x => x.BuyingGrade,
                    x => x.BuyingGrade.MasterGrade)
                    .ToList();

                return (from b in buyingList
                        group b by new { b.BuyingGrade.MasterGrade.Quality } into g
                        select new m_SummaryBuyingByQuality
                        {
                            Quality = g.Key.Quality,
                            TotalWeight = Convert.ToDecimal(g.Sum(x => x.Weight)),
                            Percentages = Convert.ToDecimal(g.Sum(x => x.Weight) / buyingList.Sum(b => b.Weight) * 100)
                        }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Buying> GetByCitizenID(string citizenID)
        {
            var _crop = uow.CropRepository.GetSingle(x => x.DefaultStatus == true);
            if (_crop == null)
                throw new ArgumentException("ไม่พบการตั้งค่า default crop ในระบบ โปรดแจ้งผู้ดูแลระบบหรือแผนกไอที");

            return uow.BuyingRepository
                .Get(x => x.Weight != null &&
                x.Crop >= _crop.Crop1 - 2 &&
                x.BuyingDocument
                .RegistrationFarmer
                .Farmer
                .CitizenID == citizenID,
                null,
                x => x.BuyingGrade,
                x => x.BuyingDocument,
                x => x.BuyingDocument.RegistrationFarmer,
                x => x.BuyingDocument.RegistrationFarmer.Farmer);
        }

        public List<Buying> GetSoldHistoryByFarmer(string farmerCode)
        {
            return uow.BuyingRepository
                .Get(x => x.FarmerCode == farmerCode)
                .GroupBy(x => new { Crop = x.Crop, FarmerCode = x.FarmerCode })
                .Select(x => new Buying
                {
                    Crop = x.Key.Crop,
                    FarmerCode = x.Key.FarmerCode,
                    Weight = x.Sum(y => y.Weight)
                })
                .ToList();
        }

        public List<Buying> GetSoldHistoryByCitizenID(string citizenID)
        {
            return uow.BuyingRepository
                .Get(x => x.BuyingDocument.RegistrationFarmer.Farmer.CitizenID == citizenID)
                .GroupBy(x => new { Crop = x.Crop, FarmerCode = x.FarmerCode })
                .Select(x => new Buying
                {
                    Crop = x.Key.Crop,
                    FarmerCode = x.Key.FarmerCode,
                    Weight = x.Sum(y => y.Weight)
                })
                .ToList();
        }

        public List<Buying> GetByExtensionAgent(short crop, string extensionAgentCode)
        {
            return uow.BuyingRepository
                .Get(x => x.Weight != null &&
                x.Crop >= crop &&
                x.BuyingDocument
                .RegistrationFarmer
                .Farmer
                .Supplier == extensionAgentCode,
                null,
                x => x.BuyingGrade,
                x => x.BuyingDocument,
                x => x.BuyingDocument.RegistrationFarmer,
                x => x.BuyingDocument.RegistrationFarmer.Farmer);
        }

        public void MoveLoadingBale(List<Buying> baleList, string moveBy, string moveFrom, string moveTo)
        {
            try
            {
                if (string.IsNullOrEmpty(moveFrom))
                    throw new ArgumentException("โปรดระบุ transportation code ต้นทาง");

                if (string.IsNullOrEmpty(moveTo))
                    throw new ArgumentException("โปรดระบุ transportation code ปลายทาง");

                if (string.IsNullOrEmpty(moveBy))
                    throw new ArgumentException("โปรดระบุ username ผู้บันทึกข้อมูล");

                if (baleList.Count() > 0)
                {
                    foreach (var item in baleList)
                    {
                        item.MovingBy = moveBy;
                        item.MovingTransportationDate = DateTime.Now;
                        item.MovingTransportationFrom = moveFrom;
                        item.MovingTransportationReason = "N/A";
                        item.TransportationDocumentCode = moveTo;
                    }
                    uow.BuyingRepository.UpdateRange(baleList);
                    uow.Save();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}