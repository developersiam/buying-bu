﻿using DomainModel;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IReturnCPAContainerBL
    {
        void Add(ReturnCPAContainer model);
        void Update(ReturnCPAContainer model);
        void Delete(ReturnCPAContainer model);
        ReturnCPAContainer GetById(Guid id);
        List<ReturnCPAContainer> GetByFarmer(short crop, string farmerCode);
    }

    public class ReturnCPAContainerBL : IReturnCPAContainerBL
    {
        UnitOfWork uow;
        public ReturnCPAContainerBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(ReturnCPAContainer model)
        {
            try
            {
                uow.ReturnCPAContainerRepository.Add(model);
                uow.Save();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(ReturnCPAContainer model)
        {
            try
            {
                var _delete = uow.ReturnCPAContainerRepository.GetSingle(x => x.ID == model.ID);

                if (_delete == null)
                    throw new ArgumentException("ไม่พบรายการข้อมูลดังกล่าวในระบบ");

                uow.ReturnCPAContainerRepository.Remove(_delete);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ReturnCPAContainer GetById(Guid id)
        {
            try
            {
                return uow.ReturnCPAContainerRepository.GetSingle(r => r.ID == id,
                r => r.ChemicalDistribution,
                r => r.ChemicalDistribution.ChemicalItem);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ReturnCPAContainer> GetByFarmer(short crop, string farmerCode)
        {
            try
            {
                return uow.ReturnCPAContainerRepository.Query(r => r.Crop == crop
                && r.FarmerCode == farmerCode,
                null,
                r => r.ChemicalDistribution,
                r => r.ChemicalDistribution.ChemicalItem).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Update(ReturnCPAContainer model)
        {
            try
            {
                var item = GetById(model.ID);

                if (item == null)
                    throw new ArgumentException("ไม่พบรายการข้อมูลดังกล่าวในระบบ");

                uow.ReturnCPAContainerRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
