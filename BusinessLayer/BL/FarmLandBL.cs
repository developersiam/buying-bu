﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IFarmLandBL
    {
        void Add(FarmLandInformation model);
        void Update(FarmLandInformation model);
        void Delete(Guid id);
        FarmLandInformation GetByID(Guid id);
        List<FarmLandInformation> GetByCitizenID(string citizenID, short crop);
    }

    public class FarmLandBL : IFarmLandBL
    {
        UnitOfWork uow;
        public FarmLandBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(FarmLandInformation model)
        {
            try
            {
                if (model.CitizenID == null)
                    throw new ArgumentException("ไม่ได้ระบุ Citizen ID.");

                if (model.SubDistrictCode == null)
                    throw new ArgumentException("ไม่ได้ระบุ Sub District Code.");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("ไม่ได้ระบุ Modified User.");

                if (model.FieldNo.ToString() == null || model.FieldNo.ToString() == "")
                    throw new ArgumentException("ไม่ได้ระบุ FieldNo");

                model.FarmLandInfoID = Guid.NewGuid();
                model.ModifiedDate = DateTime.Now;

                uow.FarmLandInformationRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(FarmLandInformation model)
        {
            try
            {
                if (model.CitizenID == null)
                    throw new ArgumentException("ไม่ได้ระบุ Citizen ID.");

                if (model.SubDistrictCode == null)
                    throw new ArgumentException("ไม่ได้ระบุ Sub District Code.");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("ไม่ได้ระบุ Modified User.");

                if (model.FieldNo.ToString() == null || model.FieldNo.ToString() == "")
                    throw new ArgumentException("ไม่ได้ระบุ FieldNo");

                var edit = GetByID(model.FarmLandInfoID);
                if (edit == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                edit.FieldNo = model.FieldNo;
                edit.LandSize = model.LandSize;
                edit.OwnerShip = model.OwnerShip;
                edit.SubDistrictCode = model.SubDistrictCode;
                edit.UseForGrowingTobacco = model.UseForGrowingTobacco;
                edit.ModifiedDate = DateTime.Now;
                edit.ModifiedUser = model.ModifiedUser;

                uow.FarmLandInformationRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var delete = GetByID(id);
                if (delete == null)
                    throw new ArgumentException("ไม่พบข้อมูล");

                uow.FarmLandInformationRepository.Remove(delete);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public FarmLandInformation GetByID(Guid farmLandInfoID)
        {
            return uow.FarmLandInformationRepository
                .GetSingle(f => f.FarmLandInfoID == farmLandInfoID,
                f => f.SubDistrict,
                f => f.SubDistrict.District,
                f => f.SubDistrict.District.Province);
        }

        public List<FarmLandInformation> GetByCitizenID(string citizenID, short crop)
        {
            return uow.FarmLandInformationRepository
                .Query(f => f.CitizenID == citizenID &&
                f.Crop == crop,
                null,
                f => f.SubDistrict,
                f => f.SubDistrict.District,
                f => f.SubDistrict.District.Province).ToList();
        }
    }
}
