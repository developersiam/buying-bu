﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public interface IMaterialIssuedDetailsBL
    {
        void Add(MaterialIssuedDetail model);
        void Delete(string issuedCode, int itemID);
        void Update(MaterialIssuedDetail model);
        MaterialIssuedDetail GetSingle(string issuedCode, int itemID);
        List<MaterialIssuedDetail> GetByIssuedCode(string issuedCode);
    }
    public class MaterialIssuedDetailsBL : IMaterialIssuedDetailsBL
    {
        UnitOfWork uow;
        public MaterialIssuedDetailsBL()
        {
            uow = new UnitOfWork();
        }
        public void Add(MaterialIssuedDetail model)
        {
            try
            {
                var validation = new DomainValidation.IsMaterialIssuedDetailsValid().Validate(model);

                if (!validation.IsValid)
                    throw new ArgumentException(validation.Erros.FirstOrDefault().Message);

                var item = AGMInventorySystemBL.BLServices
                    .material_itemBL()
                    .GetSingle(model.ItemID);

                if (item == null)
                    throw new ArgumentException("ไม่พบรายการ item นี้ในระบบ");

                if (item.purchaseorder_details.Count <= 0)
                    throw new ArgumentException("ไม่สามารถหาค่าเฉลียของ Unit price ได้เนื่องจากยังไม่ปรากฏรายการข้อมูลนี้ใน PO ใดๆ");

                var itemsInIssuedCode = GetByIssuedCode(model.IssuedCode);
                if (itemsInIssuedCode.Where(x => x.ItemID == model.ItemID).Count() > 0)
                    throw new ArgumentException("มี " + item.item_name + " แล้วใน issued code " + model.IssuedCode);

                model.UnitPrice = item.purchaseorder_details.Average(x => x.unitprice);
                model.RecordDate = DateTime.Now;
                model.ModifiedDate = DateTime.Now;

                uow.MaterialIssuedDetailRepository.Add(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string issuedCode, int itemID)
        {
            try
            {
                var model = GetSingle(issuedCode, itemID);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.MaterialIssuedDetailRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<MaterialIssuedDetail> GetByIssuedCode(string issuedCode)
        {
            return uow.MaterialIssuedDetailRepository
                .Get(x => x.IssuedCode == issuedCode)
                .ToList();
        }

        public MaterialIssuedDetail GetSingle(string issuedCode, int itemID)
        {
            return uow.MaterialIssuedDetailRepository
                .GetSingle(x => x.IssuedCode == issuedCode && x.ItemID == itemID);
        }

        public void Update(MaterialIssuedDetail model)
        {
            try
            {
                var editModel = GetSingle(model.IssuedCode, model.ItemID);
                if (editModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                editModel.Quantity = model.Quantity;
                editModel.ModifiedDate = DateTime.Now;
                editModel.ModifiedUser = model.ModifiedUser;

                uow.MaterialIssuedDetailRepository.Update(editModel);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
