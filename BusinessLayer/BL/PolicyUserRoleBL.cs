﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IPolicyUserRoleBL
    {
        void Add(AppPolicyUserRole model);
        void Delete(string username, Guid roleid);
        AppPolicyUserRole GetSingle(string username, Guid roleid);
        List<AppPolicyUserRole> GetAll();
        List<AppPolicyUserRole> GetByUsername(string username);
    }

    public class PolicyUserRoleBL : IPolicyUserRoleBL
    {
        IUnitOfWork _uow;
        public PolicyUserRoleBL()
        {
            _uow = new UnitOfWork();
        }
        public void Add(AppPolicyUserRole model)
        {
            try
            {
                var list = GetByUsername(model.Username.ToLower());
                if (list.Where(x => x.RoleID == model.RoleID).Count() > 0)
                    throw new ArgumentException("มีการกำหนด role นี้ซ้ำแล้วในระบบ");

                model.CreateDate = DateTime.Now;
                _uow.AppPolicyUserRoleRepository.Add(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string username, Guid roleid)
        {
            try
            {
                var model = GetSingle(username, roleid);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                _uow.AppPolicyUserRoleRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<AppPolicyUserRole> GetAll()
        {
            return _uow.AppPolicyUserRoleRepository
                .Get(null, null, x => x.AppPolicyRole)
                .ToList();
        }

        public List<AppPolicyUserRole> GetByUsername(string username)
        {
            return _uow.AppPolicyUserRoleRepository
                .Get(x => x.Username == username, null,
                x => x.AppPolicyRole).ToList();
        }

        public AppPolicyUserRole GetSingle(string username, Guid roleid)
        {
            return _uow.AppPolicyUserRoleRepository
                .GetSingle(x => x.Username == username &&
                x.RoleID == roleid);
        }
    }
}
