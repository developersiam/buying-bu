﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IDebtSetupAccountInvoiceBL
    {
        List<DebtSetupAccountInvoice> GetByFarmer(short crop, string farmerCode);
        List<DebtSetupAccountInvoice> GetByDebSetupCode(string debtSetupCode);
        List<DebtSetupAccountInvoice> GetByAccountInvoiceNo(string accountInvoiceNo);
        List<DebtSetupAccountInvoice> GetByExtensionAgent(short crop, string extensionAgentCode);
    }

    public class DebtSetupAccountInvoiceBL : IDebtSetupAccountInvoiceBL
    {
        UnitOfWork uow;
        public DebtSetupAccountInvoiceBL()
        {
            uow = new UnitOfWork();
        }

        public List<DebtSetupAccountInvoice> GetByAccountInvoiceNo(string accountInvoiceNo)
        {
            return uow.DebtSetupAccountInvoiceRepository
                .Get(x => x.AccountInvoiceNo == accountInvoiceNo);
        }

        public List<DebtSetupAccountInvoice> GetByDebSetupCode(string debtSetupCode)
        {
            return uow.DebtSetupAccountInvoiceRepository
                .Get(x => x.DebtSetupCode == debtSetupCode
                , null
                , x => x.DebtSetup
                , x => x.AccountInvoice
                , x => x.AccountInvoice.Invoice
                , x => x.AccountInvoice.Invoice.InvoiceDetails);
        }

        public List<DebtSetupAccountInvoice> GetByExtensionAgent(short crop, string extensionAgentCode)
        {
            return uow.DebtSetupAccountInvoiceRepository
                .Get(x => x.DebtSetup.Crop == crop
                && x.DebtSetup.RegistrationFarmer.Farmer.Supplier == extensionAgentCode
                , null
                , x => x.DebtSetup
                , x => x.AccountInvoice);
        }

        public List<DebtSetupAccountInvoice> GetByFarmer(short crop, string farmerCode)
        {
            return uow.DebtSetupAccountInvoiceRepository
                .Get(x => x.DebtSetup.Crop == crop
                && x.DebtSetup.FarmerCode == farmerCode
                , null
                , x => x.DebtSetup
                , x => x.AccountInvoice
                , x => x.AccountInvoice.Invoice
                , x => x.AccountInvoice.Invoice.InvoiceDetails);
        }

    }
}
