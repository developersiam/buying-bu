﻿using AGMInventorySystemEntities;
using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IInvoiceBL
    {
        void Add(short crop, string farmerCode, string type, bool isVat, string createUser);
        void Add2(short crop, string farmerCode, bool isVat, string debtTypeCode, string createUser);
        void Delete(string invoiceNo);
        Invoice GetSingle(string invoiceNo);
        List<Invoice> GetByFarmer(short crop, string farmerCode);
        List<Invoice> GetBySupplier(short crop, string supplierCode);
        List<Invoice> GetByCreateDate(DateTime date);
        void Finish(string invoiceNo, string user, DateTime accountInvoiceDate);
    }

    public class InvoiceBL : IInvoiceBL
    {
        UnitOfWork uow;
        public InvoiceBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(short crop, string farmerCode, string type, bool isVat, string createUser)
        {
            try
            {
                Helper.SystemDeadlinesHelper.BeforeStartBuyingDeadLine(crop, farmerCode);

                int _max;
                string _invoiceNo;

                var invoices = GetByFarmer(crop, farmerCode);
                var list = new List<Invoice>();

                if (type == "CD")
                    list = invoices.Where(x => x.InvoiceNo.Substring(0, 2) == "CD").ToList();
                else
                    list = invoices.Where(x => x.InvoiceNo.Substring(0, 2) == "IV").ToList();

                if (list.Count() <= 0)
                    _max = 1;
                else
                    _max = list.Max(x => Convert.ToInt16(x.InvoiceNo.Split('-')[2])) + 1;

                _invoiceNo = (type == "CD" ? "CD" : "IV") +
                    crop.ToString() + "-" +
                    farmerCode + "-" +
                    _max.ToString().PadLeft(3, '0');

                uow.InvoiceRepository
                    .Add(new Invoice
                    {
                        InvoiceNo = _invoiceNo,
                        Crop = crop,
                        FarmerCode = farmerCode,
                        CreateDate = DateTime.Now,
                        CreateUser = createUser,
                        IsFinish = false,
                        IsActive = true,
                        IsIncludeVAT = isVat,
                        ModifiedUser = createUser,
                        ModifiedDate = DateTime.Now
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Add2(short crop, string farmerCode, bool isVat, string debtTypeCode, string createUser)
        {
            try
            {
                var contract = BuyingFacade.RegistrationBL()
                    .GetSingle(crop, farmerCode);
                if (contract == null)
                    throw new Exception("ชาวไร่รายนี้ไม่ได้ลงทะเบียนทำสัญญาในฤดูปลูกปัจจุบัน");

                if (contract.ActiveStatus == false)
                    throw new Exception("ชาวไร่รายนี้ได้ทำการยกเลิกสัญญาแล้วเนื่องจาก " + contract.UnRegisterReason);

                if (contract.QuotaFromSignContract == null || contract.QuotaFromSignContract < 1)
                    throw new Exception("ไม่ได้ระบุโควต้าในการทำสัญญา");

                if (!string.IsNullOrEmpty(contract.UnRegisterReason))
                    throw new Exception("ชาวไร่รายนี้มีการยกเลิกสัญญาแล้ว");

                var debtSetupList = BuyingFacade.DebtSetupBL()
                    .GetByFarmerSetupCrop(crop, farmerCode);
                var debtTypes = debtSetupList
                    .Where(x => x.DebtTypeCode == debtTypeCode);
                if (debtTypes.Count() > 0)
                    throw new Exception("มีการนำยอดหนี้ของใบส่งของประเภท " +
                        debtTypes.FirstOrDefault().DebtType.DebtTypeName +
                        " ไปตั้งหนี้ในระบบแล้ว ไม่สามารถบันทึกการจ่ายปัจจัยได้");

                //Use debtSetupList for check the old debt.
                //If a farmer not clear them old debt.
                //A user will not be create a new invoice to that farmer.
                if (debtTypeCode == "DP")
                {
                    var dripProjectDebtList = debtSetupList
                        .Where(x => x.DebtTypeCode == "DP" && x.Crop < crop);
                    var debtAmount = dripProjectDebtList.Sum(x => x.Amount);
                    var receiptAmount = dripProjectDebtList.Sum(x =>
                    x.DebtReceipts.Sum(y =>
                    y.Amount));
                    if (debtAmount - receiptAmount > 0)
                        throw new Exception("ชาวไร่รายนี้มีหนี้เก่าในโครงการน้ำหยดค้างอยู่ในระบบ " +
                            "ไม่สามารถจ่ายปัจจัยในโครงการน้ำหยดได้ ต้องทำการชำระหนี้ที่ค้างอยู่ในระบบทั้งหมดก่อน");
                }

                var buyingDocumentList = BuyingFacade.BuyingDocumentBL()
                    .GetByFarmer(crop, farmerCode);
                if (buyingDocumentList.Count() > 0)
                    throw new Exception("ได้มีการเริ่มสร้างใบเวาเชอร์เพื่อทำการซื้อขายแล้ว ไม่สามารถบันทึกการจ่ายปัจจัยการผลิตได้");

                int _max;
                string _invoiceNo;
                var invoices = GetByFarmer(crop, farmerCode);
                var list = new List<Invoice>();
                list = invoices.Where(x => x.InvoiceNo.Substring(0, 2) == "IV").ToList();
                if (list.Count() <= 0)
                    _max = 1;
                else
                    _max = list.Max(x => Convert.ToInt16(x.InvoiceNo.Split('-')[2])) + 1;
                _invoiceNo = ("IV") +
                   crop.ToString() + "-" +
                   farmerCode + "-" +
                   _max.ToString().PadLeft(3, '0');

                uow.InvoiceRepository
                    .Add(new Invoice
                    {
                        InvoiceNo = _invoiceNo,
                        Crop = crop,
                        FarmerCode = farmerCode,
                        CreateDate = DateTime.Now,
                        CreateUser = createUser,
                        IsFinish = false,
                        IsActive = true,
                        IsIncludeVAT = isVat,
                        DebtTypeCode = debtTypeCode,
                        ModifiedUser = createUser,
                        ModifiedDate = DateTime.Now
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string invoiceNo)
        {
            try
            {
                var model = GetSingle(invoiceNo);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (model.IsFinish == true)
                    throw new ArgumentException("Invoice นี้ถูก finish ไปแล้วไม่สามารถลบได้");

                if (model.InvoiceDetails.Count() > 0)
                    throw new ArgumentException("ไม่สามารถลบ invoice " + invoiceNo +
                        " ได้ เนื่องจากมีรายการ item อยู่ใน invoice นี้");

                if (model.AccountInvoices.Count() > 0)
                    throw new ArgumentException("ไม่สามารถลบ invoice " + invoiceNo +
                        " ได้ เนื่องจากเคยมีการออกรหัสทางบัญชี (account invoice no) ไปแล้ว ให้ทำการยกเลิก invoice นี้แทน");

                uow.InvoiceRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Invoice> GetByCreateDate(DateTime date)
        {
            return uow.InvoiceRepository.Get(x => x.CreateDate.Date == date.Date);
        }

        public List<Invoice> GetBySupplier(short crop, string supplierCode)
        {
            return uow.InvoiceRepository
                .Get(x => x.Crop == crop &&
                x.RegistrationFarmer.Farmer.Supplier == supplierCode,
                null,
                x => x.RegistrationFarmer,
                x => x.RegistrationFarmer.Farmer);
        }

        public List<Invoice> GetByFarmer(short crop, string farmerCode)
        {
            return uow.InvoiceRepository
                .Get(x => x.Crop == crop &&
                x.FarmerCode == farmerCode,
                null,
                x => x.AccountInvoices,
                x => x.InvoiceDetails,
                x => x.DebtType);
        }

        public Invoice GetSingle(string invoiceNo)
        {
            return uow.InvoiceRepository
                .GetSingle(x => x.InvoiceNo == invoiceNo,
                x => x.InvoiceDetails,
                x => x.AccountInvoices,
                x => x.DebtType);
        }

        public void Finish(string invoiceNo, string user, DateTime accountInvoiceDate)
        {
            try
            {
                var invoice = uow.InvoiceRepository
                    .GetSingle(x => x.InvoiceNo == invoiceNo);
                if (invoice == null)
                    throw new ArgumentException("ไม่พบข้อมูล invoice นี้ในระบบ");

                ///หากพบว่า invoice นี้ถูก finish ไปแล้วก่อนหน้านี้ ให้จบการทำงานไปเลย
                ///เพื่อป้องกันไม่ให้มีการตัดรายการสินค้าออกจากสต๊อคซ้ำอีก
                if (invoice.IsFinish == true)
                    throw new ArgumentException("สถานะของ invoice no# " + invoiceNo + " อยู่ในสถานะ finish แล้ว");

                var accountInvoiceList = BuyingFacade.AccountInvoiceBL().GetByRefInvoice(invoiceNo);
                var accountInvoiceExtract = "";

                foreach (var item in accountInvoiceList)
                    accountInvoiceExtract = accountInvoiceExtract + ", " + item.InvoiceNo;

                ///ถ้ามีการออก account invoice no ไปแล้วให้แจ้งเตือนไปยังหน้าจอ
                if (accountInvoiceList.Count() > 0)
                    throw new ArgumentException("มีการออก account invoice no ไปแล้วจำนวน " +
                        accountInvoiceList.Count() + "รายการ (" + accountInvoiceExtract + ")");

                var invoiceDetailList = uow.InvoiceDetailRepository
                    .Get(x => x.InvoiceNo == invoiceNo, null,
                    x => x.CropInputsCondition);

                if (invoiceDetailList.Count() <= 0)
                    throw new ArgumentException("โปรดเพิ่มปัจจัยการผลิตใน invoice อย่างน้อย 1 รายการ");

                //if (invoiceDetailList.Where(x => x.AccountInvoiceNo != null).Count() > 0)
                //    throw new ArgumentException("มีการออกเลขที่ใบเสร็จของทางบัญชีไปแล้ว " +
                //        "ไม่สามารถ finish ได้ หากต้องการแก้ไขข้อมูลกรุณาแจ้งแผนกบัญชี");

                var location = "";
                var farmer = uow.FarmerRepository
                    .GetSingle(x => x.FarmerCode == invoice.FarmerCode);
                if (farmer == null)
                    throw new ArgumentException("farmer cannot be null.");

                var areaCode = uow.SupplierRepository
                    .GetSingle(x => x.SupplierCode == farmer.Supplier)
                    .SupplierArea;

                if (areaCode == "PET")
                    location = "01";
                else if (areaCode == "SUK")
                    location = "02";
                else if (areaCode == "CM")
                    location = "03";
                else
                    location = "";

                ///********************************************************************************
                ///ตรวจสอบ item_id ในระบบ AGM Inventory ว่าตรงตามที่มีอยู่จริงหรือไม่
                ///********************************************************************************
                List<material_item> itemCheckedList = new List<material_item>();
                foreach (var item in invoiceDetailList)
                {
                    if (item.CropInputsCondition.IsReferToInventory == true)
                    {
                        var inventoryItem = AGMInventorySystemBL
                            .BLServices
                            .material_itemBL()
                            .GetSingle(item.CropInputsCondition.ItemID);
                        if (inventoryItem == null)
                            throw new ArgumentException("ไม่พบข้อมูล item id " + item.CropInputsCondition.ItemID +
                                " นี้ใน AGM inventory โปรดติดต่อผู้ดูแลระบบ");

                        ///เก็บรายการที่ผ่านการตรวจสอบแล้วไว้ใน List
                        itemCheckedList.Add(inventoryItem);
                    }
                }

                ///********************************************************************************
                ///เช็คจำนวนสินค้าในคลังก่อนตัดสต๊อกออกจากระบบ AGM Inventory System
                ///********************************************************************************
                ///
                foreach (var item in invoiceDetailList)
                {
                    if (itemCheckedList
                        .Where(x => x.itemid == item.CropInputsCondition.ItemID)
                        .Count() > 0)
                    {
                        var balanceStock = AGMInventorySystemBL
                            .BLServices
                            .material_transactionBL()
                            .GetByItem(item.CropInputsCondition.ItemID)
                            .Where(x => x.locationid == location);

                        if (balanceStock.Sum(x => x.quantity) - item.Quantity < 0)
                            throw new ArgumentException("item id " +
                                item.CropInputsCondition.ItemID + " นี้ไม่พอจ่ายให้กับชาวไร่.");
                    }
                }

                ///********************************************************************************
                /// Create account invoice no.
                ///********************************************************************************
                var accountInvoice = "";
                if (invoice.IsIncludeVAT == true)
                {
                    if (invoiceDetailList.Where(x => x.IsCash == true).Count() > 0)
                    {
                        accountInvoice = BuyingFacade.AccountInvoiceBL()
                            .GenerateInvoiceNo(invoiceNo, areaCode.Substring(0, 1) + "OV",
                            user, accountInvoiceDate);

                        foreach (var item in invoiceDetailList.Where(x => x.IsCash == true))
                            item.AccountInvoiceNo = accountInvoice;
                    }

                    if (invoiceDetailList.Where(x => x.IsCash == false).Count() > 0)
                    {
                        accountInvoice = BuyingFacade.AccountInvoiceBL()
                            .GenerateInvoiceNo(invoiceNo, areaCode.Substring(0, 1) + "IV",
                            user, accountInvoiceDate);

                        foreach (var item in invoiceDetailList.Where(x => x.IsCash == false))
                            item.AccountInvoiceNo = accountInvoice;
                    }
                }
                else
                {
                    if (invoiceDetailList.Where(x => x.IsCash == true).Count() > 0)
                    {
                        accountInvoice = BuyingFacade.AccountInvoiceBL()
                            .GenerateInvoiceNo(invoiceNo, areaCode.Substring(0, 1) + "ON",
                            user, accountInvoiceDate);

                        foreach (var item in invoiceDetailList.Where(x => x.IsCash == true))
                            item.AccountInvoiceNo = accountInvoice;
                    }

                    if (invoiceDetailList.Where(x => x.IsCash == false).Count() > 0)
                    {
                        accountInvoice = BuyingFacade.AccountInvoiceBL()
                            .GenerateInvoiceNo(invoiceNo, areaCode.Substring(0, 1) + "IN",
                            user, accountInvoiceDate);

                        foreach (var item in invoiceDetailList.Where(x => x.IsCash == false))
                            item.AccountInvoiceNo = accountInvoice;
                    }
                }

                /// Mark account invoiceno to each item.
                foreach (var item in invoiceDetailList)
                    uow.InvoiceDetailRepository.Update(item);

                uow.Save();

                ///********************************************************************************
                ///ตัดสต๊อกออกจากระบบ AGM Inventory System
                ///********************************************************************************
                foreach (var item in invoiceDetailList)
                {
                    var inventoryItem = itemCheckedList.SingleOrDefault(x => x.itemid == item.CropInputsCondition.ItemID);
                    if (item.CropInputsCondition.IsReferToInventory == true)
                        AGMInventorySystemBL.BLServices
                            .material_transactionBL()
                            .Out(new material_transaction
                            {
                                crop = item.CropInputsCondition.Crop,
                                refid = item.AccountInvoiceNo,
                                itemid = inventoryItem.itemid,
                                unitid = inventoryItem.unitid,
                                quantity = item.Quantity,
                                unitprice = item.CropInputsCondition.UnitPrice,
                                locationid = location,
                                transactiontype = "Out",
                                modifieduser = user,
                                modifieddate = DateTime.Now
                            });
                }

                ///********************************************************************************
                /// Set invoice status to finish.
                ///********************************************************************************
                invoice.IsFinish = true;
                invoice.FinishDate = DateTime.Now;
                invoice.ModifiedDate = DateTime.Now;
                invoice.ModifiedUser = user;

                uow.InvoiceRepository.Update(invoice);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
