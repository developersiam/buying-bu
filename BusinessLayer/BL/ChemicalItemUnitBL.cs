﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IChemicalItemUnitBL
    {
        List<ChemicalItemUnit> GetAll();
    }

    public class ChemicalItemUnitBL : IChemicalItemUnitBL
    {
        UnitOfWork uow;
        public ChemicalItemUnitBL()
        {
            uow = new UnitOfWork();
        }

        public List<ChemicalItemUnit> GetAll()
        {
            return uow.ChemicalItemUnitRepository.Get().ToList();
        }
    }
}
