﻿using AGMInventorySystemEntities;
using DataAccessLayer;
using DomainModel;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace BusinessLayer.BL
{
    public interface ICPAContainerReturnBL
    {
        void Add(Guid receiveID, int quantity, bool isEmpty, string user);
        void Edit(Guid returnID, int quantity, bool isEmpty, string user);
        void Delete(Guid returnID);
        List<CPAContainerReturn> GetByFarmerCode(short crop, string farmerCode);
        List<CPAContainerReturn> GetByCitizenID(short crop, string citizenID);
        CPAContainerReturn GetSingle(Guid returnID);
    }

    public class CPAContainerReturnBL : ICPAContainerReturnBL
    {
        UnitOfWork uow;
        public CPAContainerReturnBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(Guid receiveID, int quantity, bool isEmpty, string user)
        {
            try
            {
                if (quantity < 1)
                    throw new ArgumentException("จำนวนที่รับคืนจะต้องไม่น้อยกว่า 1");

                if (string.IsNullOrEmpty(user))
                    throw new ArgumentException("user cannot be empty.");

                uow.CPAContainerReturnRepository
                    .Add(new CPAContainerReturn
                    {
                        ReturnID = Guid.NewGuid(),
                        ReceiveID = receiveID,
                        Quantity = quantity,
                        IsEmpty = isEmpty,
                        ReturnDate = DateTime.Now,
                        ModifiedBy = user,
                        ModifiedDate = DateTime.Now
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid returnID)
        {
            try
            {
                var model = GetSingle(returnID);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                uow.CPAContainerReturnRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(Guid returnID, int quantity, bool isEmpty, string user)
        {
            try
            {
                var model = GetSingle(returnID);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลนี้ในระบบ");

                model.Quantity = quantity;
                model.IsEmpty = isEmpty;
                model.ModifiedBy = user;
                model.ModifiedDate = DateTime.Now;

                uow.CPAContainerReturnRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CPAContainerReturn> GetByCitizenID(short crop, string citizenID)
        {
            return uow.CPAContainerReturnRepository
                .Get(x => x.CPAContainerReceive.Crop == crop &&
                x.CPAContainerReceive.RegistrationFarmer.Farmer.CitizenID == citizenID
                , null
                , x => x.CPAContainerReceive
                , x => x.CPAContainerReceive.CPAContainerItem
                , x => x.CPAContainerReceive.RegistrationFarmer
                , x => x.CPAContainerReceive.RegistrationFarmer.Farmer);
        }

        public List<CPAContainerReturn> GetByFarmerCode(short crop, string farmerCode)
        {
            return uow.CPAContainerReturnRepository
                .Get(x => x.CPAContainerReceive.Crop == crop &&
                x.CPAContainerReceive.FarmerCode == farmerCode
                , null
                , x => x.CPAContainerReceive
                , x => x.CPAContainerReceive.CPAContainerItem);
        }

        public CPAContainerReturn GetSingle(Guid returnID)
        {
            return uow.CPAContainerReturnRepository.GetSingle(x => x.ReturnID == returnID);
        }
    }
}
