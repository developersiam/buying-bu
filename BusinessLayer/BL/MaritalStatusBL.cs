﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IMaritalStatusBL
    {
        List<MaritalStatu> GetAllMaitalStatus();
    }
    public class MaritalStatusBL : IMaritalStatusBL
    {
        UnitOfWork uow;
        public MaritalStatusBL()
        {
            uow = new UnitOfWork();
        }

        public List<MaritalStatu> GetAllMaitalStatus()
        {
            return uow.MaritalStatuRepository.Get().ToList();
        }
    }
}
