﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;
using System.Windows.Media;
using System.Xml.Schema;

namespace BusinessLayer.BL
{
    public interface IExtraBonusBL
    {
        void Add(short crop, string stationCode, string farmerCode, short documentNumber);
        void Calculate(short crop);
        void ConfirmToTransfer(string[] farmerCodeList, string transferredBy);
        void ConfirmToPrint(List<int> receiptOrder, string printedBy);
        void CancelTransferred(List<int> receiptOrder);
        void CancelPrinted(List<int> receiptOrder);
        List<ExtraBonu> GetByStatus(string extraBonusStatus);
        ExtraBonu GetSingle(short crop, string stationCode, string farmerCode, short documentNumber);

    }

    public class ExtraBonusBL : IExtraBonusBL
    {
        UnitOfWork uow;
        public ExtraBonusBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(short crop, string stationCode, string farmerCode, short documentNumber)
        {
            try
            {
                var voucher = uow.BuyingDocumentRepository
                    .GetSingle(x => x.Crop == crop &&
                    x.BuyingStationCode == stationCode &&
                    x.FarmerCode == farmerCode &&
                    x.BuyingDocumentNumber == documentNumber,
                    x => x.Buyings);

                if (voucher == null)
                    throw new Exception("ไม่พบ voucher นี้ในระบบ");

                var buyingList = voucher.Buyings.ToList();
                if (buyingList.Sum(x => x.Weight) <= 0)
                    throw new Exception("Total weight is empty.");

                var quota = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == crop &&
                    x.FarmerCode == farmerCode,
                    x => x.Farmer);
                var kgPerRai = uow.KilogramsPerRaiConfigurationRepository
                    .GetSingle(x => x.Crop == crop &&
                    x.SupplierCode == quota.Farmer.Supplier);
                var sold = uow.BuyingRepository
                    .Get(x => x.Crop == crop &&
                    x.FarmerCode == farmerCode);
                bool isFullQuota = sold.Sum(x => x.Weight) >= quota.QuotaFromSignContract * kgPerRai.KilogramsPerRai
                    ? true : false;

                string[] str = { "1", "2", "3" };
                var bonusGrades = uow.BuyingGradeRepository
                    .Get(x => x.PricingCrop == crop && x.Grade.Length == 3)
                    .Where(y => str.Contains(y.Grade.Substring(2, 1)))
                    .ToList();



                var extraBonus = uow.ExtraBonusRepository
                    .GetSingle(x => x.Crop == crop &&
                    x.BuyingStationCode == stationCode &&
                    x.FarmerCode == farmerCode &&
                    x.BuyingDocumentNumber == documentNumber);
                if (extraBonus == null)
                {
                    // First sold in extra quota.

                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Calculate(short crop)
        {
            try
            {
                //Extra bonus grading criteria.
                string[] criteria = { "X5V", "X5Y", "XK", "XKY", "C5V", "C5Y", "CK", "CKY", "B5V", "B5Y", "BK", "BKY", "T3", "T4", "T5", "TK", "B3K", "ND" };
                var gradeList = uow.BuyingGradeRepository
                    .Get(x => x.PricingCrop == crop
                    && x.PricingNumber == 1)
                    .ToList();
                var bonusGrades = gradeList
                    .Where(x => !criteria.Contains(x.Grade))
                    .ToList();

                //string[] criteria = { "1", "2", "3" };
                //var gradeList = uow.BuyingGradeRepository
                //    .Get(x => x.PricingCrop == crop
                //    && x.PricingNumber == 1
                //    && x.Grade.Length == 3)
                //    .ToList();
                //var bonusGrades = gradeList
                //    .Where(x => criteria.Contains(x.Grade.Substring(2, 1)))
                //    .ToList();

                //var bf4 = gradeList.SingleOrDefault(x => x.Grade == "BF4");
                //bonusGrades.Add(bf4);

                //Farmer full quota.
                var reg = uow.RegistrationFarmerRepository
                    .Get(x => x.Crop == crop,
                    null,
                    x => x.Farmer);
                var kgPerRai = uow.KilogramsPerRaiConfigurationRepository
                    .Get(x => x.Crop == crop);
                var quotaList = (from a in reg
                                 from b in kgPerRai
                                 where a.Crop == b.Crop && a.Farmer.Supplier == b.SupplierCode
                                 select new
                                 {
                                     a.Crop,
                                     a.FarmerCode,
                                     QuotaRai = a.QuotaFromSignContract,
                                     QuotaKg = a.QuotaFromSignContract * b.KilogramsPerRai
                                 })
                                 .ToList();
                var sold = uow.BuyingRepository
                    .Get(x => x.Crop == crop)
                    .GroupBy(x => new { x.Crop, x.FarmerCode })
                    .Select(x => new
                    {
                        x.Key.Crop,
                        x.Key.FarmerCode,
                        SoldKg = x.Sum(y => y.Weight)
                    })
                    .Where(x => x.SoldKg > 0)
                    .ToList();

                var fullQuota = (from a in quotaList
                                 from b in sold
                                 where a.Crop == b.Crop && a.FarmerCode == b.FarmerCode
                                 select new
                                 {
                                     a.Crop,
                                     b.FarmerCode,
                                     a.QuotaRai,
                                     a.QuotaKg,
                                     b.SoldKg
                                 })
                                .Where(x => x.SoldKg >= x.QuotaKg)
                                .Select(x => new
                                {
                                    x.Crop,
                                    x.FarmerCode,
                                    x.QuotaRai,
                                    x.QuotaKg,
                                    x.SoldKg
                                })
                                .ToList();

                var existsExtraBonus = uow.ExtraBonusRepository
                    .Get(x => x.Crop == crop);

                var existsFarmers = existsExtraBonus
                    .GroupBy(x => x.FarmerCode)
                    .Select(x => x.Key)
                    .ToList();

                if (existsExtraBonus.Count() >= 0)
                {
                    //ยังไม่เคยประมวลผล Extra Bonus
                    var loop1 = fullQuota
                        .Where(x => !existsFarmers.Contains(x.FarmerCode))
                        .ToList();
                    //เคยมีการประมวลผล Extra Bonus ไปแล้ว
                    var loop2 = fullQuota
                        .Where(x => existsFarmers.Contains(x.FarmerCode))
                        .ToList();

                    foreach (var fq in loop1)
                    {
                        //*********************************
                        //Use for unit testing.
                        //if (fq.FarmerCode != "A000714")
                        //    continue;
                        //*********************************

                        //Get all voucher of ull quota farmer.
                        var vouchers = uow.BuyingDocumentRepository
                        .Get(x => x.Crop == fq.Crop &&
                        x.FarmerCode == fq.FarmerCode,
                        null,
                        x => x.Buyings)
                        .Select(x => new
                        {
                            x.Crop,
                            x.FarmerCode,
                            x.BuyingStationCode,
                            x.BuyingDocumentNumber,
                            Sold = x.Buyings.Sum(y => y.Weight)
                        })
                        .Where(x => x.Sold > 0)
                        .OrderBy(x => x.BuyingDocumentNumber)
                        .ToList();

                        decimal accSold = (decimal)0.0;
                        foreach (var vc in vouchers)
                        {
                            //หาเวาเชอร์ที่ขายเกินโควต้า
                            var extraWeight = (decimal)0.0;
                            accSold = accSold + (decimal)vc.Sold;
                            if (accSold >= fq.QuotaKg)
                            {
                                //หาห่อที่ขายเกินโควต้าโดยเรียงตามเวลาในการบันทึกน้ำหนัก
                                var buyings = uow.BuyingRepository
                                    .Get(x => x.Crop == vc.Crop &&
                                    x.FarmerCode == vc.FarmerCode &&
                                    x.BuyingStationCode == vc.BuyingStationCode &&
                                    x.BuyingDocumentNumber == vc.BuyingDocumentNumber &&
                                    x.Weight != null)
                                    .OrderBy(x => x.WeightDate)
                                    .ToList();
                                var bonus1 = uow.ExtraBonusRepository
                                    .Get(x => x.Crop == fq.Crop &&
                                    x.FarmerCode == fq.FarmerCode)
                                    .ToList();
                                if (bonus1.Count() <= 0)
                                {
                                    var accWeight = accSold - vc.Sold;
                                    foreach (var aw in buyings)
                                    {
                                        accWeight = accWeight + (decimal)aw.Weight;
                                        var weight = (decimal)aw.Weight;
                                        var accExt = accWeight - fq.QuotaKg;
                                        var docNo = aw.BuyingDocumentNumber;
                                        var grade = aw.Grade;
                                        if (accWeight > fq.QuotaKg)
                                        {
                                            if (bonusGrades.Contains(aw.BuyingGrade))
                                            {
                                                if (aw.Weight > accWeight - fq.QuotaKg)
                                                    extraWeight = extraWeight + (decimal)(accWeight - fq.QuotaKg);
                                                else
                                                    extraWeight = extraWeight + (decimal)aw.Weight;
                                            }
                                        }
                                    }
                                    if (extraWeight > 0)
                                    {
                                        uow.ExtraBonusRepository
                                            .Add(new ExtraBonu
                                            {
                                                Crop = vc.Crop,
                                                FarmerCode = vc.FarmerCode,
                                                BuyingStationCode = vc.BuyingStationCode,
                                                BuyingDocumentNumber = vc.BuyingDocumentNumber,
                                                ExtaBonusKg = extraWeight,
                                                ExtraBonusAmount = extraWeight * 2,
                                                ExtraBonusStatus = "Pending"
                                            });
                                        uow.Save();
                                    }
                                }
                                else
                                {
                                    extraWeight = (decimal)buyings
                                        .Where(x => bonusGrades.Contains(x.BuyingGrade))
                                        .Sum(y => y.Weight);

                                    if (extraWeight > 0)
                                    {
                                        uow.ExtraBonusRepository
                                            .Add(new ExtraBonu
                                            {
                                                Crop = vc.Crop,
                                                FarmerCode = vc.FarmerCode,
                                                BuyingStationCode = vc.BuyingStationCode,
                                                BuyingDocumentNumber = vc.BuyingDocumentNumber,
                                                ExtaBonusKg = extraWeight,
                                                ExtraBonusAmount = extraWeight * 2,
                                                ExtraBonusStatus = "Pending"
                                            });
                                        uow.Save();
                                    }
                                }
                            }
                        }
                    }

                    foreach (var fq in loop2)
                    {
                        var maxDocNo = existsExtraBonus
                            .Where(x => x.Crop == fq.Crop &&
                            x.FarmerCode == fq.FarmerCode)
                            .Max(y => y.BuyingDocumentNumber);
                        var vouchers = uow.BuyingDocumentRepository
                            .Get(x => x.Crop == fq.Crop &&
                            x.FarmerCode == fq.FarmerCode &&
                            x.BuyingDocumentNumber > maxDocNo)
                            .ToList();

                        foreach (var vc in vouchers)
                        {
                            var buyings = uow.BuyingRepository
                                    .Get(x => x.Crop == vc.Crop &&
                                    x.FarmerCode == vc.FarmerCode &&
                                    x.BuyingStationCode == vc.BuyingStationCode &&
                                    x.BuyingDocumentNumber == vc.BuyingDocumentNumber &&
                                    x.Weight != null);

                            decimal extraWeight = (decimal)buyings
                                .Where(x => bonusGrades.Contains(x.BuyingGrade))
                                .Sum(y => y.Weight);

                            if (extraWeight > 0)
                            {
                                uow.ExtraBonusRepository
                                    .Add(new ExtraBonu
                                    {
                                        Crop = vc.Crop,
                                        FarmerCode = vc.FarmerCode,
                                        BuyingStationCode = vc.BuyingStationCode,
                                        BuyingDocumentNumber = vc.BuyingDocumentNumber,
                                        ExtaBonusKg = extraWeight,
                                        ExtraBonusAmount = extraWeight * 2,
                                        ExtraBonusStatus = "Pending"
                                    });
                                uow.Save();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CancelPrinted(List<int> receiptOrder)
        {
            try
            {
                var farmerBonusList = uow.ExtraBonusRepository
                    .Get(x => x.ExtraBonusStatus == "Completed" &&
                    receiptOrder.Contains((int)x.ReceiptOrder))
                    .ToList();

                //Change transfer info.
                farmerBonusList.ForEach(x =>
                {
                    x.ExtraBonusStatus = "Transferred";
                    x.PrintedDate = null;
                });

                uow.ExtraBonusRepository.UpdateRange(farmerBonusList);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CancelTransferred(List<int> receiptOrder)
        {
            try
            {
                var farmerBonusList = uow.ExtraBonusRepository
                    .Get(x => x.ExtraBonusStatus == "Transferred" &&
                    receiptOrder.Contains((int)x.ReceiptOrder))
                    .ToList();

                //Change transfer info.
                farmerBonusList.ForEach(x =>
                {
                    x.ExtraBonusStatus = "Pending";
                    x.TransferredDate = null;
                });

                uow.ExtraBonusRepository.UpdateRange(farmerBonusList);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ConfirmToPrint(List<int> receiptOrder, string printedBy)
        {
            try
            {
                var farmerBonusList = uow.ExtraBonusRepository
                    .Get(x => x.ExtraBonusStatus == "Transferred" &&
                    receiptOrder.Contains((int)x.ReceiptOrder))
                    .ToList();

                //Change transfer info.
                farmerBonusList.ForEach(x =>
                {
                    x.ExtraBonusStatus = "Completed";
                    x.PrintedDate = DateTime.Now;
                    x.PrintedBy = printedBy;
                });

                uow.ExtraBonusRepository.UpdateRange(farmerBonusList);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ConfirmToTransfer(string[] farmerCodeList, string transferredBy)
        {
            try
            {
                var crop = uow.CropRepository
                    .GetSingle(x => x.DefaultStatus == true);
                if (crop == null)
                    throw new Exception("Crop default not declare.");

                var bonusList = uow.ExtraBonusRepository
                    .Get(x => x.Crop == crop.Crop1);

                var maxOrder = 0;
                if (bonusList
                    .Where(x => x.ReceiptOrder != null)
                    .Count() <= 0)
                    maxOrder = 0;
                else
                    maxOrder = (int)bonusList.Max(x => x.ReceiptOrder);

                var farmerBonusList = bonusList
                    .Where(x => x.ExtraBonusStatus == "Pending" &&
                    farmerCodeList.Contains(x.FarmerCode))
                    .ToList();

                //Change transfer info.
                farmerBonusList.ForEach(x =>
                {
                    x.ExtraBonusStatus = "Transferred";
                    x.TransferredDate = DateTime.Now;
                    x.TransferredBy = transferredBy;
                });

                //Generate receipt order number.
                var groupByFarmer = farmerBonusList
                    .Where(x => x.ReceiptOrder == null)
                    .GroupBy(x => x.FarmerCode)
                    .ToList();
                foreach (var item in groupByFarmer)
                {
                    maxOrder = maxOrder + 1;
                    foreach (var f in farmerBonusList
                        .Where(x => x.FarmerCode == item.FirstOrDefault().FarmerCode))
                    {
                        f.ReceiptOrder = maxOrder;
                    }
                }

                uow.ExtraBonusRepository.UpdateRange(farmerBonusList);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ExtraBonu> GetByStatus(string extraBonusStatus)
        {
            return uow.ExtraBonusRepository
                .Get(x => x.ExtraBonusStatus == extraBonusStatus,
                null,
                x => x.BuyingDocument);
        }

        public ExtraBonu GetSingle(short crop, string stationCode, string farmerCode, short documentNumber)
        {
            return uow.ExtraBonusRepository
                .GetSingle(x => x.Crop == crop &&
                x.BuyingStationCode == stationCode &&
                x.FarmerCode == farmerCode &&
                x.BuyingDocumentNumber == documentNumber,
                x => x.BuyingDocument);
        }
    }
}
