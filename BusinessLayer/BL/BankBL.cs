﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IBankBL
    {
        List<Bank> GetAll();
    }
    public class BankBL : IBankBL
    {
        UnitOfWork uow;
        public BankBL()
        {
            uow = new UnitOfWork();
        }

        public List<Bank> GetAll()
        {
            return uow.BankRepository.Get().ToList();
        }
    }
}
