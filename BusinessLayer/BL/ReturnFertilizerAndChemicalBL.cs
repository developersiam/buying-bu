﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IReturnFertilizerAndChemicalBL
    {
        List<ReturnFertilizerAndChemical> GetByCropAndFarmerCode(short crop, string farmerCode);
        ReturnFertilizerAndChemical GetById(Guid id);
        void Add(short crop, string farmerCode, Guid fertilizerAndChemicalID, bool returnType, short returnQuantity, string returnUser, DateTime returnDate);
        void Delete(Guid id);
    }

    public class ReturnFertilizerAndChemicalBL : IReturnFertilizerAndChemicalBL
    {
        UnitOfWork uow;
        public ReturnFertilizerAndChemicalBL()
        {
            uow = new UnitOfWork();
        }

        public List<ReturnFertilizerAndChemical> GetByCropAndFarmerCode(short crop, string farmerCode)
        {
            try
            {
                return uow.ReturnFertilizerAndChemicalRepository
                    .Query(r => r.Crop == crop &&
                    r.FarmerCode == farmerCode,
                    null,
                    r => r.ReceiveFertilizerAndChemical,
                    r => r.ReceiveFertilizerAndChemical.FertilizerAndChemical,
                    r => r.ReceiveFertilizerAndChemical.FertilizerAndChemical.FertilizerAndChemicalUnit,
                    r => r.ReceiveFertilizerAndChemical.FertilizerAndChemical.FertilizerAndChemicalType)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ReturnFertilizerAndChemical GetById(Guid id)
        {
            return uow.ReturnFertilizerAndChemicalRepository
                .GetSingle(r => r.ReturnFertilierAndChemicalID == id,
                    r => r.ReceiveFertilizerAndChemical,
                    r => r.ReceiveFertilizerAndChemical.FertilizerAndChemical);
        }
        public void Add(short crop, string farmerCode, Guid fertilizerAndChemicalID, bool isSurplus, short returnQuantity, string returnUser, DateTime returnDate)
        {
            try
            {
                /// เงื่อนไขที่ใช้ในการตรวจสอบการบันทึกการคืนสารเคมี
                /// 1.รายการที่นำมาคืนจะต้องมีอยู่ในรายการที่รับไป โดยเลือกจากตัวเลือกที่กำหนด
                /// 2.จำนวนที่คืนจะต้องไม่เกินจากจำนวนที่รับไป

                if (returnQuantity.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล returnQuantity");

                if (returnQuantity <= 0)
                    throw new ArgumentException("returnQuantity จะต้องมากกว่า 0");

                if (returnUser == "")
                    throw new ArgumentException("ไม่พบข้อมูล returnUser");

                if (returnDate.ToString() == "")
                    throw new ArgumentException("ไม่พบข้อมูล returnDate");

                /// 1.รายการที่นำมาคืนจะต้องมีอยู่ในรายการที่รับไป โดยเลือกจากตัวเลือกที่กำหนด ****************************************
                /// 
                var model = uow.ReceiveFertilizerAndChemicalRepository
                    .GetSingle(rc => rc.Crop == crop &&
                    rc.FarmerCode == farmerCode &&
                    rc.FertilizerAndChemicalID == fertilizerAndChemicalID,
                    rc => rc.FertilizerAndChemical,
                    rc => rc.FertilizerAndChemical.FertilizerAndChemicalType,
                    rc => rc.FertilizerAndChemical.FertilizerAndChemicalUnit);


                if (model == null)
                    throw new ArgumentException("ไม่พบรายการบรรจุภัณฑ์สารเคมีที่ท่านต้องการคืนนี้ ในรายการบรรจุภัณฑ์สารเคมีที่ชาวไร่ได้รับ");

                /// 2.จำนวนที่คืนจะต้องไม่เกินจากจำนวนที่รับไป
                /// 
                int totalReturn = uow.ReturnFertilizerAndChemicalRepository
                    .Query(rt => rt.Crop == crop
                    && rt.FarmerCode == farmerCode
                    && rt.FertilizerAndChemicalID == fertilizerAndChemicalID)
                    .Sum(rt => rt.ReturnQuantity);

                if ((totalReturn + returnQuantity) > model.Quantity)
                    throw new ArgumentException("จำนวนบรรจุภํณฑ์สารเคมีที่ท่านนำมาคืนทั้งหมด เกินกว่าจำนวนที่ท่านได้รับ ไม่สามารถบันทึกข้อมูลการคืนสารได้");

                uow.ReturnFertilizerAndChemicalRepository
                    .Add(new ReturnFertilizerAndChemical
                    {
                        Crop = crop,
                        FarmerCode = farmerCode,
                        ReturnQuantity = returnQuantity,
                        ReturnDate = returnDate,
                        FertilizerAndChemicalID = fertilizerAndChemicalID,
                        ReturnUser = returnUser,
                        IsSurplus = isSurplus,
                        ReturnFertilierAndChemicalID = Guid.NewGuid()
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(Guid id)
        {
            try
            {
                var model = GetById(id);
                if (model == null)
                    throw new ArgumentException("ไม่พบรายการบรรจุภัณฑ์สารเคมีที่ท่านต้องการลบ");

                uow.ReturnFertilizerAndChemicalRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
