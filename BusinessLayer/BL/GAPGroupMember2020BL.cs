﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IGAPGroupMember2020BL
    {
        GAPGroupMember2020 GetSingle(string gapGroupCode, string citizenID);
        GAPGroupMember2020 GetSingleByCitizenID(short crop, string citizenID);
        List<GAPGroupMember2020> GetByGAPGroupCode(string gapGroupCode);
    }

    public class GAPGroupMember2020BL : IGAPGroupMember2020BL
    {
        UnitOfWork uow;
        public GAPGroupMember2020BL()
        {
            uow = new UnitOfWork();
        }

        public List<GAPGroupMember2020> GetByGAPGroupCode(string gapGroupCode)
        {
            return uow.GAPGroupMember2020Repository
                .Query(x => x.GAPGroupCode == gapGroupCode,
                null,
                x => x.GAPGroup2020,
                x => x.Person)
                .ToList();
        }

        public GAPGroupMember2020 GetSingle(string gapGroupCode, string citizenID)
        {
            return uow.GAPGroupMember2020Repository
                .GetSingle(x => x.GAPGroupCode == gapGroupCode &&
                x.CitizenID == citizenID,
                x => x.GAPGroup2020);
        }

        public GAPGroupMember2020 GetSingleByCitizenID(short crop, string citizenID)
        {
            return uow.GAPGroupMember2020Repository
                .GetSingle(x => x.GAPGroup2020.Crop == crop &&
                x.CitizenID == citizenID,
                x => x.GAPGroup2020);
        }
    }
}
