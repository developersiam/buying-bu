﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IBuyingStationBL
    {
        List<BuyingStation> GetAll();
        List<BuyingStation> GetByArea(string areaCode);
    }

    public class BuyingStationBL : IBuyingStationBL
    {
        UnitOfWork uow;
        public BuyingStationBL()
        {
            uow = new UnitOfWork();
        }

        public List<BuyingStation> GetAll()
        {
            return uow.BuyingStationRepository.Get().ToList();
        }

        public List<BuyingStation> GetByArea(string areaCode)
        {
            return uow.BuyingStationRepository
                .Query(bs => bs.AreaCode == areaCode).ToList();
        }
    }
}
