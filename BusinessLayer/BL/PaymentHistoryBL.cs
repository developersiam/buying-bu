﻿using BusinessLayer.Model;
using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IPaymentHistoryBL
    {
        void Deduct(short crop, string stationCode, string farmerCode, short docNumber,
           DateTime paymentDate, short paymentPercentage, string deductator);
        void Finish(short crop, string stationCode, string farmerCode, short docNumber, string finishUser);
        void UnFinish(short crop, string stationCode, string farmerCode, short docNumber);
        void Delete(short crop, string stationCode, string farmerCode, short docNumber);
        List<PaymentHistory> GetByFarmer(short crop, string farmerCode);
        PaymentHistory GetSingle(short crop, string stationCode, string farmerCode, short docNumber);
    }

    public class PaymentHistoryBL : IPaymentHistoryBL
    {
        UnitOfWork _uow;
        public PaymentHistoryBL()
        {
            _uow = new UnitOfWork();
        }

        public void Deduct(short crop, string stationCode, string farmerCode, short docNumber,
            DateTime paymentDate, short paymentPercentage, string deductator)
        {
            try
            {
                if (crop != DateTime.Now.Year)
                    throw new ArgumentException("Crop ไม่ตรงกับปัจจุบัน ไม่สามารถบันทึกข้อมูลได้");

                if (string.IsNullOrEmpty(stationCode))
                    throw new ArgumentException("Station code cannot be null.");

                if (string.IsNullOrEmpty(farmerCode))
                    throw new ArgumentException("Farmer code cannot be null.");

                var document = BuyingFacade.BuyingDocumentBL()
                    .GetSingle(crop, stationCode, farmerCode, docNumber);
                if (document == null)
                    throw new ArgumentException("This buying document not found.");

                if (document.Crop != crop)
                    throw new ArgumentException("Crop ไม่ตรงกับในเอกสารการซื้อขาย ไม่สามารถบันทึกข้อมูลได้");

                if (document.IsAccountFinish == true)
                    throw new ArgumentException("แผนกบัญชีได้ยืนยันใบเวาเชอร์นี้แล้ว ไม่สามารถดำเนินการใดๆได้อีก");

                var paymentList = BuyingFacade.PaymentHistoryBL()
                    .GetByFarmer(crop, farmerCode)
                    .ToList();

                ///หากมีรายการใดที่ยังไม่ได้ Finish จากบัญชีจะไม่ให้หักเงิน 
                ///แต่หากรายการที่ยังไม่ได้ Finish นั้นคือรายการเดียวกันกับที่กำลังจะหักเงินนี้ ให้สามารถข้ามเงื่อนไขนี้ไปได้
                ///เพื่อแก้ไขยอดหัก กรณีที่มีการแบ็คยาจากทาง receiving
                if (paymentList
                    .Where(x => x.IsFinish == false)
                    .Count() > 0 &&
                    paymentList
                    .SingleOrDefault(x => x.Crop == crop &&
                    x.BuyingStationCode == stationCode &&
                    x.FarmerCode == farmerCode &&
                    x.BuyingDocumentNumber == docNumber) == null)
                    throw new ArgumentException("มียอดหักชำระบางรายการที่ทางแผนกบัญชียังไม่ได้ยืนยันข้อมูล โปรดตรวจสอบข้อมูลอีกครั้ง");

                var totalDebt = Helper.DebtorsHelper.GetTotalDebt(crop, farmerCode);
                var creditNote = BuyingFacade.AccountCreditNoteDetailBL()
                    .GetByFarmerCode(crop, farmerCode)
                    .Sum(x => x.Quantity * x.UnitPrice);
                var buyingPrice = Helper.BuyingHelper
                    .GetByBuyingDocument(crop, farmerCode, stationCode, docNumber)
                    .Sum(x => x.Price);

                var model = GetSingle(crop, stationCode, farmerCode, docNumber);
                if (model == null)
                {
                    var totalPayment = paymentList.Sum(x => x.PaymentAmount);
                    var remainDebt = totalDebt - creditNote - totalPayment;
                    var paymentAmount = remainDebt * Convert.ToDecimal(paymentPercentage / 100.0);

                    if (buyingPrice < paymentAmount)
                        throw new ArgumentException("ยอดเงินจากการขายในครั้งนี้ไม่พอหักชำระ โปรดแจ้งผู้เกี่ยวข้อง");

                    _uow.PaymentHistoryRepository
                        .Add(new PaymentHistory
                        {
                            Crop = crop,
                            BuyingStationCode = stationCode,
                            FarmerCode = farmerCode,
                            BuyingDocumentNumber = docNumber,
                            PaymentDate = paymentDate,
                            PaymentPercentage = paymentPercentage,
                            PaymentAmount = paymentAmount,
                            Deductator = deductator,
                            IsFinish = false
                        });
                }
                else
                {
                    var thisPayment = paymentList
                        .Where(x => x.Crop == model.Crop &&
                        x.BuyingStationCode == model.BuyingStationCode &&
                        x.FarmerCode == model.FarmerCode &&
                        x.BuyingDocumentNumber == model.BuyingDocumentNumber)
                        .Sum(x => x.PaymentAmount);
                    var totalPayment = paymentList.Sum(x => x.PaymentAmount) - thisPayment;
                    var remainDebt = totalDebt - creditNote - totalPayment;
                    var paymentAmount = remainDebt * Convert.ToDecimal(paymentPercentage / 100.0);

                    if (buyingPrice < paymentAmount)
                        throw new ArgumentException("ยอดเงินจากการขายในครั้งนี้ไม่พอหักชำระ โปรดแจ้งผู้เกี่ยวข้อง");

                    model.PaymentAmount = paymentAmount;
                    model.PaymentPercentage = paymentPercentage;
                    model.Deductator = deductator;
                    model.PaymentDate = paymentDate;

                    _uow.PaymentHistoryRepository.Update(model);
                }
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(short crop, string stationCode, string farmerCode, short docNumber)
        {
            try
            {
                var model = GetSingle(crop, stationCode, farmerCode, docNumber);
                if (model == null)
                    throw new ArgumentException("This payment not found.");

                if (model.Crop != crop)
                    throw new ArgumentException("Crop ไม่ตรงกับในเอกสารการซื้อขาย ไม่สามารถบันทึกข้อมูลได้");

                if (model.IsFinish == true)
                    throw new ArgumentException("สถานะของการหักหนี้นี้เป็น Finish ไม่สามารถลบรายการนี้ได้");

                _uow.PaymentHistoryRepository.Remove(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Finish(short crop, string stationCode, string farmerCode, short docNumber, string finishUser)
        {
            try
            {
                var model = GetSingle(crop, stationCode, farmerCode, docNumber);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลการหักหนี้นี้ในระบบ");
                
                model.IsFinish = true;
                model.FinishDate = DateTime.Now;
                model.FinishUser = finishUser;

                _uow.PaymentHistoryRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<PaymentHistory> GetByFarmer(short crop, string farmerCode)
        {
            return _uow.PaymentHistoryRepository
                .Get(x => x.Crop == crop && x.FarmerCode == farmerCode);
        }

        public PaymentHistory GetSingle(short crop, string stationCode, string farmerCode, short docNumber)
        {
            return _uow.PaymentHistoryRepository
                .GetSingle(x => x.Crop == crop &&
                x.BuyingStationCode == stationCode &&
                x.FarmerCode == farmerCode &&
                x.BuyingDocumentNumber == docNumber);
        }

        public void UnFinish(short crop, string stationCode, string farmerCode, short docNumber)
        {
            try
            {
                var model = GetSingle(crop, stationCode, farmerCode, docNumber);
                if (model == null)
                    throw new ArgumentException("This payment not found.");

                if (model.Crop != crop)
                    throw new ArgumentException("Crop ไม่ตรงกับในเอกสารการซื้อขาย ไม่สามารถบันทึกข้อมูลได้");

                var lastPayment = GetByFarmer(crop, farmerCode)
                    .ToList()
                    .Max(x => x.PaymentDate);

                if (lastPayment == null)
                    throw new ArgumentException("ไม่พบข้อมูลการหักรายการล่าสุด โปรดติดต่อผู้ดูแลระบบเพื่อตรวจสอบ");

                if (model.PaymentDate < lastPayment)
                    throw new ArgumentException("ไม่สามารถให้ทำรายการย้อนหลังเนื่องจากจะกระทบต่อข้อมูลการหักยอดหนี้รายการอื่นๆ");


                model.IsFinish = false;
                model.FinishDate = null;

                _uow.PaymentHistoryRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
