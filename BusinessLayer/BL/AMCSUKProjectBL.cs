﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IAMCSUKProjectBL
    {
        List<AMCSUKProject> GetByFarmer(short crop, string citizenID);
    }

    public class AMCSUKProjectBL : IAMCSUKProjectBL
    {
        UnitOfWork uow;
        public AMCSUKProjectBL()
        {
            uow = new UnitOfWork();
        }

        public List<AMCSUKProject> GetByFarmer(short crop, string citizenID)
        {
            return uow.AMCSUKProjectRepository.Get(x => x.Crop == crop && x.CitizenID == citizenID);
        }
    }
}
