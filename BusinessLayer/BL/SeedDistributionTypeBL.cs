﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface ISeedDistributionTypeBL
    {
        List<SeedDistributionType> GetAll();
    }

    public class SeedDistributionTypeBL : ISeedDistributionTypeBL
    {
        UnitOfWork uow;
        public SeedDistributionTypeBL()
        {
            uow = new UnitOfWork();
        }

        public List<SeedDistributionType> GetAll()
        {
            return uow.SeedDistributionTypeRepository.Get().ToList();
        }
    }
}
