﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;
using BusinessLayer.Model;
using BusinessLayer.Helper;

namespace BusinessLayer.BL
{
    public interface IBuyingDocumentBL
    {
        BuyingDocument Add(short crop, string stationCode, string farmerCode,
            string createUser, DateTime createDate);
        void Delete(short crop, string stationCode, string farmerCode, short documentNumber);
        void Finish(short crop, string stationCode, string farmerCode, short documentNumber, string buyingUser);
        void Unfinish(short crop, string stationCode, string farmerCode, short documentNumber, string buyingUser);
        void Printed(short crop, string stationCode, string farmerCode, short documentNumber,
            string buyingUser);
        void CencelPrinted(short crop, string stationCode, string farmerCode, short documentNumber,
            string buyingUser);
        void ReceivingFinish(short crop, string stationCode, string farmerCode, short documentNumber, string confirmUser);
        BuyingDocument GetSingle(short crop, string stationCode, string farmerCode, short documentNumber);
        List<BuyingDocument> GetByFarmer(short crop, string farmerCode);
    }

    public class BuyingDocumentBL : IBuyingDocumentBL
    {
        UnitOfWork uow;
        public BuyingDocumentBL()
        {
            uow = new UnitOfWork();
        }

        public BuyingDocument Add(short crop, string stationCode, string farmerCode,
            string createUser, DateTime createDate)
        {
            try
            {
                //เงื่อนไขที่ใช้ในการตรวจสอบก่อนเริ่มต้นการสร้างใบซื้อเพื่อจ่ายป้ายบาร์โค้ตให้แก่ชาวไร่มีดังนี้
                /*
                 * 1.ตรวจสอบจำนวนโควต้าคงเหลือ
                 * 2.ตรวจสอบสถานะสัญญา
                 * 3.ตรวจสอบสถานะการซื้อขาย
                 * 4.ตรวจสอบการกำหนดกิโลกรัมต่อไร่ในแต่ละซัพพลายเออร์ (KilogramPerRaiConfiguration table)
                 * 5.ตรวจสอบใบซื้อที่ยังไม่ได้ Finish (ต้อง Finish ครบทุกใบซื้อจึงจะสร้างใบซื้อใหม่ได้)
                 * 6.โควต้ารวมจะต้องไม่เป็น 0
                 * 7.ต้องมีการแบ่งโปรเจค
                 */

                if (createDate.Date < DateTime.Now.Date)
                    throw new ArgumentException("ไม่สามารถทำรายการย้อนหลังได้");

                if (string.IsNullOrEmpty(farmerCode))
                    throw new ArgumentException("ไม่พบข้อมูล Farmer Code");

                if (string.IsNullOrEmpty(stationCode))
                    throw new ArgumentException("ไม่พบข้อมูล Buying Station");

                if (string.IsNullOrEmpty(createUser))
                    throw new ArgumentException("ไม่พบข้อมูล Create User");

                if (createDate == null)
                    throw new ArgumentException("ไม่พบข้อมูล Create Document Date");

                var _regisModel = uow.RegistrationFarmerRepository
                    .GetSingle(x => x.Crop == crop &&
                    x.FarmerCode == farmerCode,
                    x => x.Farmer);

                if (_regisModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รหัส " + farmerCode + " ในปี " + crop);

                if (!_regisModel.ActiveStatus)
                    throw new ArgumentException("สถานะสัญญาอยู่ในสถานะ ยกเลิกสัญญา ไม่สามารถจ่ายป้ายได้");

                var _documentList = uow.BuyingDocumentRepository
                    .Get(x => x.Crop == crop &&
                    x.FarmerCode == farmerCode
                    , null
                    , x => x.DebtReceipts)
                    .ToList();

                // ตรวจสอบใบซื้อที่ยังไม่ได้ Finish (ต้อง Finish ครบทุกใบซื้อจึงจะสร้างใบซื้อใหม่ได้) 
                var unFinishDoc = _documentList.Where(bd => bd.IsFinish == false);
                if (unFinishDoc.Count() > 0)
                    throw new ArgumentException("มีเวาเชอร์ที่ยังไม่ได้ปิดการขายค้างอยู่จำนวน " + 
                        unFinishDoc.Count().ToString("N0") + " รายการ");

                //if (_documentList
                //    .Where(x => x.IsAccountFinish == false)
                //    .Count() > 0)
                //    throw new ArgumentException("มีเวาเชอร์ที่ยังไม่ได้รับการยืนยันจากทางแผนกบัญชีและแผนกใบยา " +
                //        "จึงไม่สามารถบันทึกการจ่ายป้ายรอบใหม่ได้");


                //var paymentList = uow.PaymentHistoryRepository
                //    .Get(x => x.Crop == crop && x.FarmerCode == farmerCode).ToList();
                //if (paymentList.Where(x => x.IsFinish == false).Count() > 0)
                //    throw new ArgumentException("ยังมีใบเวาเชอร์บางรายการที่รอการยืนยันการโอนเงินจากทางบัญชี " +
                //        "ไม่สามารถสร้างเวาเชอร์เพื่อจ่ายป้ายได้ โปรดติดต่อแผนกบัญชี");

                //var totalDebt = DebtorsHelper.GetTotalDebt(crop, farmerCode);
                //var totalPayment = DebtorsHelper.GetTotalPayment(crop, farmerCode);

                //if ((totalDebt - totalPayment) != 0)
                //    if (_documentList.Count() > 0 &&
                //        _documentList.Where(x => x.Crop == crop &&
                //        x.FarmerCode == farmerCode &&
                //        x.CreateDate.Date == createDate.Date)
                //        .Count() > 1)
                //        throw new ArgumentException("ชาวไร่ที่มียอดชำระหนี้ที่ยังชำระไม่ครบ จะไม่สามารถสร้างใบเวาเชอร์ได้มากกว่า 1 ใบ " +
                //            "โดยจะต้องรอทางแผนกบัญชีเคลียร์ยอดค้างชำระในแต่ละรอบให้เสร็จก่อน");

                var kgPerRai = uow.KilogramsPerRaiConfigurationRepository
                    .GetSingle(k => k.Crop == _regisModel.Crop &&
                    k.SupplierCode == _regisModel.Farmer.Supplier);
                if (kgPerRai == null)
                    throw new ArgumentException("ยังไม่มีการกำหนดจำนวนกิโลกรัมต่อไร่ โปรดแจ้งผู้ดูแลระบบ");

                //if (BuyingFacade.ChemicalDistributionBL()
                //    .GetByFarmer(crop, farmerCode)
                //    .Count() < 1)
                //    throw new ArgumentException("ไม่พบข้อมูลการรับปัจจัยการผลิต(ปุ๋ย, ยา)ของชาวไร่รายนี้ " +
                //        Environment.NewLine + "ไม่สามารถจ่ายป้ายบาร์โค้ตได้ กรุณาแจ้งผู้จัดการลานรับซื้อ");

                var farmerQuotaList = FarmerProjectHelper
                    .GetQuotaAndSoldByFarmerVersion2(crop, farmerCode);
                if (farmerQuotaList.Count < 1)
                    throw new ArgumentException("ชาวไร่รายนี้ยังไม่ได้แบ่งโควต้าออกตามโปรเจค โปรดติดต่อพนักงาน");

                if (farmerQuotaList
                    .Where(x => x.BalanceTag + x.BalanceExtraTag > 0)
                    .Count() <= 0)
                    throw new ArgumentException("จำนวนโควต้าคงเหลือไม่เพียงพอ ไม่สามารถจ่ายป้ายให้ได้ โปรดตรวจสอบข้อมูลอีกครั้ง");

                short newDocumentNumber = Convert.ToInt16(_documentList.Count() <= 0 ? 1 :
                    Convert.ToByte(_documentList.Max(m => m.BuyingDocumentNumber) + 1));

                BuyingDocument buyingDocument = new BuyingDocument
                {
                    Crop = crop,
                    BuyingStationCode = stationCode,
                    FarmerCode = farmerCode,
                    BuyingDocumentNumber = newDocumentNumber,
                    IsFinish = false,
                    CreateUser = createUser,
                    CreateDate = createDate,
                    ModifiedBy = createUser,
                    ModifiedDate = DateTime.Now,
                };

                uow.BuyingDocumentRepository.Add(buyingDocument);
                uow.Save();

                return buyingDocument;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(short crop, string stationCode, string farmerCode, short documentNumber)
        {
            try
            {
                var model = uow.BuyingDocumentRepository
                    .GetSingle(x => x.Crop == crop &&
                    x.BuyingStationCode == stationCode &&
                    x.FarmerCode == farmerCode &&
                    x.BuyingDocumentNumber == documentNumber,
                    x => x.Buyings,
                    x => x.HessianDistributions);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (model.IsFinish == true)
                    throw new ArgumentException("ใบซื้อนี้ถูก Finish แล้ว ไม่สามารถลบข้อมูลได้");

                if (model.Buyings.Count > 0)
                    throw new ArgumentException("ใบซื้อนี้มีข้อมูลเกี่ยวกับบาร์โค้ตอยู่จำนวน " + model.Buyings.Count + " รายการ จึงไม่สามารถลบข้อมูลได้");

                if (model.HessianDistributions.Count > 0)
                    throw new ArgumentException("ใบซื้อนี้มีข้อมูลการจ่ายกระสอบอยู่จำนวน " +
                        model.HessianDistributions.Sum(x => x.Quantity) + " รายการ จึงไม่สามารถลบข้อมูลได้" + Environment.NewLine +
                        "โปรดย้อนกลับไปยังหน้าพิมพ์เวาเชอร์เพื่อลบข้อมูลการจ่ายกระสอบออกก่อน");

                uow.BuyingDocumentRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Finish(short crop, string stationCode, string farmerCode, short documentNumber, string buyingUser)
        {
            try
            {
                if (buyingUser == "")
                    throw new ArgumentException("ไม่พบข้อมูล Buying User");

                var buyingDocument = GetSingle(crop, stationCode, farmerCode, documentNumber);

                if (buyingDocument == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (buyingDocument.Buyings.Where(b => b.Weight != null && b.TransportationDocumentCode == null).Count() >= 1)
                    throw new ArgumentException("มีห่อยาที่ชั่งน้ำหนักไปแล้ว " +
                        buyingDocument.Buyings.Where(b => b.Weight != null).Count() +
                        " ห่อ แต่ ถูกบันทึกข้อมูลการขนขึ้นรถบรรทุกเพียง " +
                        buyingDocument.Buyings.Where(b => b.Weight != null && b.TransportationDocumentCode != null).Count() +
                        " ห่อ โปรดตรวจสอบข้อมูลให้ถูกต้องอีกครั้ง");

                buyingDocument.IsFinish = true;
                buyingDocument.FinishDate = DateTime.Now;
                buyingDocument.ModifiedBy = buyingUser;
                buyingDocument.ModifiedDate = DateTime.Now;

                uow.BuyingDocumentRepository.Update(buyingDocument);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Unfinish(short crop, string stationCode, string farmerCode, short documentNumber, string buyingUser)
        {
            try
            {
                if (string.IsNullOrEmpty(buyingUser))
                    throw new ArgumentException("โปรดระบุ buying user");

                var document = GetSingle(crop, stationCode, farmerCode, documentNumber);
                if (document == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (document.IsAccountFinish)
                    throw new ArgumentException("วาเชอร์นี้แผนกบัญชียืนยันข้อมูลแล้ว ไม่สามารถยกเลิกสถานะปิดการขายได้");

                document.IsFinish = false;
                document.IsPrinted = false;
                document.ModifiedBy = buyingUser;
                document.ModifiedDate = DateTime.Now;

                uow.BuyingDocumentRepository.Update(document);
                uow.Save();

                // หากเวาเชอร์ใดมีการหักหนี้ จะต้องทำการเคลียร์ข้อมูลดังกล่าวออกด้วยทุกครั้ง
                // เพื่อให้ user หักหนี้และพิมพ์เวาเชอร์ใหม่ให้ชาวไร่
                foreach (var item in uow.DebtReceiptRepository.Get(x => x.Crop == crop &&
                x.BuyingStationCode == stationCode &&
                x.FarmerCode == farmerCode &&
                x.BuyingDocumentNumber == documentNumber))
                    uow.DebtReceiptRepository.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public BuyingDocument GetSingle(short crop, string stationCode, string farmerCode, short documentNumber)
        {
            return uow.BuyingDocumentRepository.GetSingle(bd => bd.Crop == crop
                && bd.BuyingStationCode == stationCode
                && bd.FarmerCode == farmerCode
                && bd.BuyingDocumentNumber == documentNumber,
                bd => bd.Buyings);
        }

        public List<BuyingDocument> GetByFarmer(short crop, string farmerCode)
        {
            return uow.BuyingDocumentRepository
                .Get(x => x.Crop == crop && x.FarmerCode == farmerCode);
        }

        public void Printed(short crop, string stationCode, string farmerCode, short documentNumber, string buyingUser)
        {
            try
            {
                var document = GetSingle(crop, stationCode, farmerCode, documentNumber);
                if (document == null)
                    throw new ArgumentException("ไม่พบข้อมูล buying document นี้ในระบบ");

                document.PrintDate = DateTime.Now;
                document.IsPrinted = true;
                document.ModifiedBy = buyingUser;
                document.ModifiedDate = DateTime.Now;
                uow.BuyingDocumentRepository.Update(document);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CencelPrinted(short crop, string stationCode, string farmerCode, short documentNumber, string buyingUser)
        {
            try
            {
                var document = GetSingle(crop, stationCode, farmerCode, documentNumber);
                if (document == null)
                    throw new ArgumentException("ไม่พบข้อมูล buying document นี้ในระบบ");

                document.IsPrinted = false;
                document.ModifiedBy = buyingUser;
                document.ModifiedDate = DateTime.Now;
                uow.BuyingDocumentRepository.Update(document);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ReceivingFinish(short crop, string stationCode, string farmerCode, short documentNumber, string confirmUser)
        {
            try
            {
                var buyingDocument = GetSingle(crop, stationCode, farmerCode, documentNumber);
                if (buyingDocument == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (buyingDocument.IsFinish == false)
                    throw new ArgumentException("ใบเวาเชอร์นี้อยู่ในสถานะ unfinish (ยังไม่ได้พิมพ์ใบเวาเชอร์ให้ชาวไร่) โปรดตรวจสอบไปยังลานรับซื้อ");

                buyingDocument.IsAccountFinish = true;
                buyingDocument.AccountFinishDate = DateTime.Now;
                buyingDocument.ModifiedBy = confirmUser;
                buyingDocument.ModifiedDate = DateTime.Now;

                uow.BuyingDocumentRepository.Update(buyingDocument);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
