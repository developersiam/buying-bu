﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface ISeedlingFromBL
    {
        void Add(string seedlingFrom, string modifiedBy);
        void Delete(Guid id);
        SeedlingFrom GetSingle(Guid id);
        List<SeedlingFrom> GetAll();
    }

    public class SeedlingFromBL : ISeedlingFromBL
    {
        UnitOfWork uow;
        public SeedlingFromBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(string seedlingFrom, string modifiedBy)
        {
            try
            {
                if (string.IsNullOrEmpty(seedlingFrom))
                    throw new ArgumentException("Seedling From cannot be empty.");

                if (string.IsNullOrEmpty(modifiedBy))
                    throw new ArgumentException("Modified By cannot be empty.");

                if (uow.SeedlingFromRepository
                    .Get(x => x.SeedlingFrom1 == seedlingFrom)
                    .Count() > 0)
                    throw new ArgumentException("มี Seedling Form '" + seedlingFrom + "' นี้แล้วในระบบ");

                uow.SeedlingFromRepository
                    .Add(new SeedlingFrom
                    {
                        ID = new Guid(),
                        SeedlingFrom1 = seedlingFrom,
                        ModifiedDate = DateTime.Now,
                        ModifiedBy = modifiedBy
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var model = GetSingle(id);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (uow.FarmerSeedlingFromRepository
                    .Get(x => x.SeedlingFromID == id)
                    .Count() > 0)
                    throw new ArgumentException(model.SeedlingFrom1 + " นี้ถูกนำไปใช้อ้างอิงในระบบแล้ว ไม่สามารถลบได้");

                uow.SeedlingFromRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SeedlingFrom> GetAll()
        {
            return uow.SeedlingFromRepository.Get();
        }

        public SeedlingFrom GetSingle(Guid id)
        {
            return uow.SeedlingFromRepository.GetSingle(x => x.ID == id);
        }
    }
}
