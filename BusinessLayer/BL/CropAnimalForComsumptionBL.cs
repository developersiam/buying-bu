﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface ICropAnimalForComsumptionBL
    {
        void Add(CropAnimalForConsumption model);
        void Update(CropAnimalForConsumption model);
        void Delete(CropAnimalForConsumption model);
        CropAnimalForConsumption GetByID(Guid id);
        List<CropAnimalForConsumption> GetByCitizenID(short crop, string citizenID);
    }

    public class CropAnimalForComsumptionBL : ICropAnimalForComsumptionBL
    {
        UnitOfWork uow;
        public CropAnimalForComsumptionBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(CropAnimalForConsumption model)
        {
            try
            {
                if (model.CitizenID == null)
                    throw new ArgumentException("You don't specify Citizen ID.");

                if (model.CropAnnimalType == null)
                    throw new ArgumentException("You don't specify CropAnnimalType.");

                if (model.Purpose == null)
                    throw new ArgumentException("You don't specify Purpose.");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("You don't specify Modified User.");

                model.ModifiedDate = DateTime.Now;
                uow.CropAnimalForConsumptionRepository.Add(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Update(CropAnimalForConsumption model)
        {
            try
            {
                if (model.CropAnimalForConsumptionID == null)
                    throw new ArgumentException("You don't specify CropAnimalForConsumptionID.");

                if (model.CitizenID == null)
                    throw new ArgumentException("You don't specify Citizen ID.");

                if (model.CropAnnimalType == null)
                    throw new ArgumentException("You don't specify CropAnnimalType.");

                if (model.Purpose == null)
                    throw new ArgumentException("You don't specify Purpose.");

                if (model.ModifiedUser == null)
                    throw new ArgumentException("You don't specify Modified User.");

                model.ModifiedDate = DateTime.Now;

                if (GetByID(model.CropAnimalForConsumptionID) == null)
                    throw new ArgumentException("Find by ID not found.");

                uow.CropAnimalForConsumptionRepository.Update(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(CropAnimalForConsumption model)
        {
            try
            {
                if (model.CropAnimalForConsumptionID == null)
                    throw new ArgumentException("You don't specify CropAnimalForConsumptionID.");

                model.ModifiedDate = DateTime.Now;

                if (GetByID(model.CropAnimalForConsumptionID) == null)
                    throw new ArgumentException("Find by ID not found.");

                uow.CropAnimalForConsumptionRepository.Remove(model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public CropAnimalForConsumption GetByID(Guid id)
        {
            try
            {
                return uow.CropAnimalForConsumptionRepository
                    .GetSingle(c => c.CropAnimalForConsumptionID == id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<CropAnimalForConsumption> GetByCitizenID(short crop, string citizenID)
        {
            try
            {
                return uow.CropAnimalForConsumptionRepository
                    .Query(c => c.Crop == crop &&
                    c.CitizenID == citizenID)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
