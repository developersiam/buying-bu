﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IIncidentCategoryBL
    {
        void Add(string categoryName, string createUser);
        void Update(Guid id, string categoryName, string modifiedUser);
        void Delete(Guid id);
        List<IncidentCategory> GetAll();
        IncidentCategory GetSingle(Guid id);
    }

    public class IncidentCategoryBL : IIncidentCategoryBL
    {
        UnitOfWork uow;

        public IncidentCategoryBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(string categoryName, string createUser)
        {
            try
            {
                if (uow.IncidentCategoryRepository
                    .Get(x => x.CategoryName == categoryName).Count() > 0)
                    throw new ArgumentException("มี Category นี้ซ้ำแล้วในระบบ");

                uow.IncidentCategoryRepository.Add(new IncidentCategory
                {
                    ID = Guid.NewGuid(),
                    CategoryName = categoryName,
                    CreateBy = createUser,
                    CreateDate = DateTime.Now,
                    ModifiedBy = createUser,
                    ModifiedDate = DateTime.Now
                });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var model = uow.IncidentCategoryRepository
                    .GetSingle(x => x.ID == id, x => x.IncidentTopics);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (model.IncidentTopics.Count() > 0)
                    throw new ArgumentException("มีข้อมูล Topic อยู่ภายใต้ Category นี้จำนวน " +
                        model.IncidentTopics.Count() + " รายการ ไม่สามารถลบข้อมูลนี้ได้");

                uow.IncidentCategoryRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<IncidentCategory> GetAll()
        {
            return uow.IncidentCategoryRepository.Get().ToList();
        }

        public IncidentCategory GetSingle(Guid id)
        {
            return uow.IncidentCategoryRepository
                .GetSingle(x => x.ID == id, x => x.IncidentTopics);
        }

        public void Update(Guid id, string categoryName, string modifiedUser)
        {
            try
            {
                if (uow.IncidentCategoryRepository
                    .Get(x => x.CategoryName == categoryName).Count() > 0)
                    throw new ArgumentException("มี Category นี้ซ้ำแล้วในระบบ");

                var model = uow.IncidentCategoryRepository.GetSingle(x => x.ID == id);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                model.CategoryName = categoryName;
                model.ModifiedBy = modifiedUser;
                model.ModifiedDate = DateTime.Now;

                uow.IncidentCategoryRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
