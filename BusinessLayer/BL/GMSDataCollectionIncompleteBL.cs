﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IGMSDataCollectionIncompleteBL
    {
        List<GMSDataCollectionIncomplete> GetByFarmer(short crop, string farmerCode);
    }

    public class GMSDataCollectionIncompleteBL : IGMSDataCollectionIncompleteBL
    {
        UnitOfWork _uow;
        public GMSDataCollectionIncompleteBL()
        {
            _uow = new UnitOfWork();
        }

        public List<GMSDataCollectionIncomplete> GetByFarmer(short crop, string farmerCode)
        {
            return _uow.GMSDataCollectionIncompleteRepository
                .Get(x => x.Crop == crop && x.FarmerCode == farmerCode,
                null,
                x => x.GMSDataCollectionField)
                .ToList();
        }
    }
}
