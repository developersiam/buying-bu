﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IRoleBL
    {
        void Add(Role role);
        void Update(Role role);
        void Delete(Guid roleID);
        List<Role> GetAll();
        Role GetByID(Guid roleID);
    }

    public class RoleBL : IRoleBL
    {
        UnitOfWork uow;
        public RoleBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(Role role)
        {
            try
            {
                if (role.RoleID == null)
                    throw new ArgumentException("ไม่พบข้อมูล RoleID");

                if (role.ModifiedByUser == null)
                    throw new ArgumentException("ไม่พบข้อมูล ModifiedByUser");

                if (role.CreateDate == null)
                    throw new ArgumentException("ไม่พบข้อมูล CreateDate");

                if (uow.RoleRepository.GetSingle(r => r.RoleID == role.RoleID) != null)
                    throw new ArgumentException("พบข้อมูลซ้ำในระบบ");

                role.LastModifiedDate = DateTime.Now;

                uow.RoleRepository.Add(role);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(Role role)
        {
            try
            {
                if (role.RoleID == null)
                    throw new ArgumentException("ไม่พบข้อมูล RoleID");

                if (role.ModifiedByUser == null)
                    throw new ArgumentException("ไม่พบข้อมูล ModifiedByUser");

                var roleFromDB = uow.RoleRepository.GetSingle(r => r.RoleID == role.RoleID);
                if (roleFromDB == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                role.LastModifiedDate = DateTime.Now;

                uow.RoleRepository.Update(role);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid roleID)
        {
            try
            {
                var role = uow.RoleRepository.GetSingle(r => r.RoleID == roleID);
                if (role == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.RoleRepository.Remove(role);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Role> GetAll()
        {
            return uow.RoleRepository.Get().ToList();
        }

        public Role GetByID(Guid roleID)
        {
            return uow.RoleRepository.GetSingle(r => r.RoleID == roleID);
        }

    }
}
