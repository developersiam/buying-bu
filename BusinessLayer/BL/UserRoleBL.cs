﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IUserRoleBL
    {
        void Add(string username, Guid roleId, string modifiedBy);
        void Delete(string username, Guid roleId);
        List<UserRole> GetByUser(string username);
    }

    public class UserRoleBL : IUserRoleBL
    {
        UnitOfWork uow;
        public UserRoleBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(string username, Guid roleId, string modifiedBy)
        {
            try
            {
                if (username == null)
                    throw new ArgumentException("โปรดระบุชื่อผู้ใช้ (Username)");

                if (roleId == null)
                    throw new ArgumentException("โปรดระบุบทบาทของผู้ใช้ (RoleID)");

                if (uow.UserRoleRepository
                    .Query(ur => ur.Username == username && ur.RoleID == roleId).Count() > 0)
                    throw new ArgumentException("มีการกำหนดสิทธิ์นี้ซ้ำอยู่แล้วในระบบ");

                UserRole userRole = new UserRole
                {
                    Username = username,
                    RoleID = roleId,
                    CreateDate = DateTime.Now,
                    ModifiedByUser = modifiedBy,
                    LastModifiedDate = DateTime.Now
                };

                uow.UserRoleRepository.Add(userRole);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string username, Guid roleId)
        {
            if (username == null)
                throw new ArgumentException("โปรดระบุชื่อผู้ใช้ (Username)");

            if (roleId == null)
                throw new ArgumentException("โปรดระบุบทบาทของผู้ใช้ (RoleID)");

            var userRole = uow.UserRoleRepository.GetSingle(ur => ur.Username == username && ur.RoleID == roleId);

            if (userRole == null)
                throw new ArgumentException("ไม่พบข้อมูลการกำหนดสิทธิ์นี้ในระบบ");

            uow.UserRoleRepository.Remove(userRole);
            uow.Save();
        }

        public List<UserRole> GetByUser(string username)
        {
            return uow.UserRoleRepository.Query(ur => ur.Username == username).ToList();
        }
    }
}
