﻿using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public interface IIncidentDescriptionBL
    {
        void Add(Guid topicID, string description, string createUser);
        void Update(Guid id, string description, Guid topicID, string modifiedUser);
        void Delete(Guid id);
        List<IncidentDescription> GetByTopic(Guid topicID);
        List<IncidentDescription> GetAll();
        IncidentDescription GetSingle(Guid id);
    }

    public class IncidentDescriptionBL : IIncidentDescriptionBL
    {
        UnitOfWork uow;

        public IncidentDescriptionBL()
        {
            uow = new UnitOfWork();
        }

        public void Add(Guid topicID, string description, string createUser)
        {
            try
            {
                if (uow.IncidentDescriptionRepository
                    .Get(x => x.Description == description &&
                    x.TopicID == topicID)
                    .Count() > 0)
                    throw new ArgumentException("มี description นี้ซ้ำแล้วในระบบ");

                uow.IncidentDescriptionRepository.Add(new IncidentDescription
                {
                    ID = Guid.NewGuid(),
                    Description = description,
                    TopicID = topicID,
                    CreateBy = createUser,
                    CreateDate = DateTime.Now,
                    ModifiedBy = createUser,
                    ModifiedDate = DateTime.Now
                });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var model = uow.IncidentDescriptionRepository
                    .GetSingle(x => x.ID == id, x => x.IncidentFarmers);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (model.IncidentFarmers.Count() > 0)
                    throw new ArgumentException("มีข้อมูล Incident Farmer อยู่ภายใต้ Description นี้จำนวน " +
                        model.IncidentFarmers.Count() + " รายการ ไม่สามารถลบข้อมูลนี้ได้");

                uow.IncidentDescriptionRepository.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<IncidentDescription> GetAll()
        {
            return uow.IncidentDescriptionRepository.Get(null, null, x => x.IncidentTopic, x => x.IncidentTopic.IncidentCategory);
        }

        public List<IncidentDescription> GetByTopic(Guid topicID)
        {
            return uow.IncidentDescriptionRepository
                .Get(x => x.TopicID == topicID,
                null,
                x => x.IncidentTopic,
                x => x.IncidentTopic.IncidentCategory);
        }

        public IncidentDescription GetSingle(Guid id)
        {
            return uow.IncidentDescriptionRepository
                .GetSingle(x => x.ID == id,
                x => x.IncidentTopic,
                x => x.IncidentTopic.IncidentCategory);
        }

        public void Update(Guid id, string description, Guid topicID, string modifiedUser)
        {
            try
            {
                if (uow.IncidentDescriptionRepository
                    .Get(x => x.Description == description &&
                    x.TopicID == topicID).Count() > 0)
                    throw new ArgumentException("มี description นี้ซ้ำแล้วในระบบ");

                var model = uow.IncidentDescriptionRepository.GetSingle(x => x.ID == id);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                model.Description = description;
                model.TopicID = topicID;
                model.ModifiedBy = modifiedUser;
                model.ModifiedDate = DateTime.Now;

                uow.IncidentDescriptionRepository.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
