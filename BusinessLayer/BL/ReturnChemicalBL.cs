﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DomainModel;
using DataAccessLayer;

namespace BusinessLayer.BL
{
    public interface IReturnChemicalBL
    {
        List<ReturnChemical> GetByCropAndFarmerCode(short crop, string farmerCode);
    }

    public class ReturnChemicalBL : IReturnChemicalBL
    {
        UnitOfWork uow;
        public ReturnChemicalBL()
        {
            uow = new UnitOfWork();
        }

        public List<ReturnChemical> GetByCropAndFarmerCode(short crop, string farmerCode)
        {
            try
            {
                return uow.ReturnChemicalRepository
                    .Query(rt => rt.Crop == crop && 
                    rt.FarmerCode == farmerCode,
                    null,
                    rt => rt,
                    rt => rt.ReceiveChemical.Chemical)
                    .ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
