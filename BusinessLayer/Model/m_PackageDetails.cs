﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Model
{
    public class m_PackageDetails : CropInputsPackageDetail
    {
        public string ItemName { get; set; }
        public string LotNo { get; set; }
        public string Category { get; set; }
        public string Brand { get; set; }
        public string Unit { get; set; }
        public string Description { get; set; }
    }
}
