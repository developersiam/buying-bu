﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace BusinessLayer.Model
{
    public class m_PaymentForInputFactor : PaymentForInputFactor
    {
        public decimal? Price { get; set; }
        public decimal UnitPrice { get; set; }
        public string InputFactorName { get; set; }
        public int Quantity { get; set; }
    }
}
