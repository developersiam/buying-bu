﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace BusinessLayer.Model
{
    public class m_FarmerProject : FarmerProject
    {
        public decimal? Sold { get; set; }
        public decimal? SoldInExtra { get; set; }
        public decimal? Balance { get; set; }
        public short BalanceTag { get; set; }
        public decimal? BalanceExtra { get; set; }
        public short BalanceExtraTag { get; set; }
        public decimal ActualKgPerRai { get; set; }
        public decimal TotalQuota { get; set; }
        //public decimal? SoldInExtra { get; set; }
        //public decimal? BalanceExtra { get; set; }
        //public short BalanceExtraTag { get; set; }
    }
}
