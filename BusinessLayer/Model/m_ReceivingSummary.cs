﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Model
{
    public class m_ReceivingSummary : sp_Receiving_SEL_SummaryBuyingAndReceiving_Result
    {
        public int DiffBale { get; set; }
        public decimal DiffWeight { get; set; }
        public string BuyingDocNo { get; set; }
    }
}
