﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Model
{
    public class m_PaymentHistory : PaymentHistory
    {
        public string BuyingDocumentCode { get; set; }
        public DateTime BuyingDate { get; set; }
    }
}
