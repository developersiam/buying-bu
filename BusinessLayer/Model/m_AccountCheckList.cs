﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Model
{
    public class m_AccountCheckList : sp_Receiving_SEL_ReceivingPerDayForAccountingSummary_Result
    {
        public int NotReceived { get; set; }
        public string Type { get; set; }
    }
}
