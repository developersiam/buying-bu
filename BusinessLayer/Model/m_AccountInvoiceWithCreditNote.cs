﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BusinessLayer.Model
{
    public class m_AccountInvoiceWithCreditNote
    {
        public string FarmerCode { get; set; }
        public string AccountInvoiceNo { get; set; }
        public string DebtTypeCode { get; set; }
        public string DebtTypeName { get; set; }
        public decimal AccountInvoiceAmount { get; set; }
        public decimal CreditNoteAmount { get; set; }
        public decimal BalanceAmount { get; set; }
        public List<m_InvoiceDetails> AccountInvoiceDetails { get; set; }
        public List<m_CreditNoteDetail> CreditNotes { get; set; }
    }
}