﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace BusinessLayer.Model
{
    public class m_ReceiveSeedAndMedia : ReceiveSeedAndMedia
    {
        public string SeedAndMediaName { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Price { get; set; }
    }
}
