﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Model
{
    public class m_FarmMaterialDistribution : CropInputDistribution
    {
        public decimal Price { get; set; }
        public string Name { get; set; }
    }
}
