﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Model
{
    public class m_SummaryBuyingByPosition
    {
        public string Position { get; set; }
        public decimal TotalWeight { get; set; }
        public decimal Percentages { get; set; }
    }
}
