﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Model
{
    public class m_CreditNoteHeader
    {
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerAddress1 { get; set; }
        public string TaxID { get; set; }
        public string CreditNoteCode { get; set; }
        public string CreateDate { get; set; }
        public string Description { get; set; }
        public string ReferenceAccountInvoice { get; set; }
        public decimal AccountInvoiceAmount { get; set; }
        public string AccountInvoiceNo { get; set; }
        public string ThaiBathText { get; set; }
    }
}
