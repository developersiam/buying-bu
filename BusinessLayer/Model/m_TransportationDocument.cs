﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace BusinessLayer.Model
{
    public class m_TransportationDocument : TransportationDocument
    {
        public int TotalBale { get; set; }
        public decimal? TotalWeight { get; set; }
    }
}
