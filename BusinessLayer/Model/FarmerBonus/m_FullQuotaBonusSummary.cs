﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Model.FarmerBonus
{
    public class m_FullQuotaBonusSummary
    {
        public int Crop { get; set; }
        public string AreaCode { get; set; }
        public string ExtenstionAgentCode { get; set; }
        public int TotalFarmer { get; set; }
        public int Pending { get; set; }
        public int Transferred { get; set; }
        public int Completed { get; set; }
    }
}
