﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Model.FarmerBonus
{
    public class m_FullQuotaBonusDetails
    {
        public short Crop { get; set; }
        public string AreaCode { get; set; }
        public string ExtensionAgentCode { get; set; }
        public string CitizenID { get; set; }
        public string FarmerCode { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Village { get; set; }
        public string HouseNumber { get; set; }
        public string Tumbon { get; set; }
        public string Amphur { get; set; }
        public string Province { get; set; }
        public string BankAccount { get; set; }
        public string BankName { get; set; }
        public string BankBranch { get; set; }
        public string BankBranchCode { get; set; }
        public int QuotaRai { get; set; }
        public int QuotaKg { get; set; }
        public decimal SoldKg { get; set; }
        public string FullQuotaStatus { get; set; }
        public int BonusAmount { get; set; }
        public string TransferredDate { get; set; }
        public int? ReceiptOrder { get; set; }
        public decimal? DebtAmount { get; set; }
        public decimal? DebtReceipt { get; set; }
        public decimal? DebtBalance { get; set; }
    }
}
