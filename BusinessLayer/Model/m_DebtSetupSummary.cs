﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Model
{
    public class m_DebtSetupSummary
    {
        public short Crop { get; set; }
        public string FarmerCode { get; set; }
        public string CitizenID { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ExtensionAgentCode { get; set; }
        public double AmountFromInvoice { get; set; }
        public double AmountFromDebtSetup { get; set; }
        public List<AccountInvoice> AccountInvoiceList { get; set; }
        public List<DebtSetup> DebtSetupList { get; set; }
    }
}
