﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Model
{
    public class m_FarmerCreditProject : FarmerCreditProject
    {
        public string CreditProjectName { get; set; }
    }
}
