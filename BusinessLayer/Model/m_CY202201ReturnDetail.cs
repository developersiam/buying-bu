﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Model
{
    public class m_CY202201ReturnDetail : CY202201ReturnDetail
    {
        public string ItemName { get; set; }
        public string ItemType { get; set; }
        public string Remark { get; set; }
    }
}
