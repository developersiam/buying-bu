﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace BusinessLayer.Model
{
    public class m_PaymentForSeedAndMedia : PaymentForSeedAndMedia
    {
        public decimal? Price { get; set; }
        public decimal UnitPrice { get; set; }
        public string SeedAndMediaName { get; set; }
        public int Quantity { get; set; }
    }
}
