﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Model
{
    public class m_DebtAndReceiptSummary : DebtSetup
    {
        public decimal ReceiptAmount { get; set; }
        public decimal BalanceAmount { get; set; }
    }
}
