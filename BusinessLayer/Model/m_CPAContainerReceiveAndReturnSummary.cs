﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Model
{
    public class m_CPAContainerReceiveAndReturnSummary : CPAContainerReceive
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string Description { get; set; }
        public string UnitName { get; set; }
        public int ReceiveQuantity { get; set; }
        public int ReturnQuantity { get; set; }
    }
}
