﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Model
{
    public class m_HessianReceiving : HessianReceiving
    {
        public int Issued { get; set; }
        public int OnHand { get; set; }
    }
}
