﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace BusinessLayer.Model
{
    public class m_ReceivingPerBuyingDocument : sp_Receiving_SEL_BUBuyingAndReceivedPerDay_Result
    {
        public int NotReceived { get; set; }
        public string Type { get; set; }
    }
}
