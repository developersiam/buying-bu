﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Model
{
    public class m_CY202201ReturnSummary
    {
        public int CPAContainerReceive { get; set; }
        public int BasketReceive { get; set; }
        public int CPAContainerReturn { get; set; }
        public int BasketReturn { get; set; }
        public int OtherReceive { get; set; }
        public decimal CPAConatainerReturnPercent { get; set; }
        public decimal BasketReturnPercent { get; set; }
        public List<m_CY202201ReturnDetail> ReceiveList { get; set; }
        public List<m_CY202201ReturnDetail> ReturnList { get; set; }
    }
}
