﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace BusinessLayer.Model
{
    public class m_BuyingDocument : BuyingDocument
    {
        public decimal TotalPrice { get; set; }
        public int TotalBale { get; set; }
        public int TotalGrade { get; set; }
        public decimal? TotalSold { get; set; }
        public int TotalReject { get; set; }
        public int TotalWeight { get; set; }
        public int TotalLoadTruck { get; set; }
        public string DocumentCode { get; set; }
        public string CitizenID { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
