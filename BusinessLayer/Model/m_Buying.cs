﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace BusinessLayer.Model
{
    public class m_Buying : Buying
    {
        public bool IsGrade { get; set; }
        public bool IsReject { get; set; }
        public bool IsWeight { get; set; }
        public bool IsTruck { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal? Price { get; set; }
        public string SupplierCode { get; set; }

    }
}
