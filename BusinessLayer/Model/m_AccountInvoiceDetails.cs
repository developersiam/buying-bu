﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Model
{
    public class m_AccountInvoiceDetails : InvoiceDetail
    {
        public DateTime PrintDate { get; set; }
        public DateTime CreateDate { get; set; }
        public decimal Price { get; set; }
        public int Items { get; set; }
    }
}
