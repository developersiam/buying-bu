﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Model
{
    public class m_SummaryBuyingByQuality
    {
        public string Quality { get; set; }
        public decimal TotalWeight { get; set; }
        public decimal Percentages { get; set; }
    }
}
