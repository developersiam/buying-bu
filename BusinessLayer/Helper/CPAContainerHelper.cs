﻿using BusinessLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public static class CPAContainerHelper
    {
        public static List<m_CPAContainerReceiveAndReturnSummary> GetByCitizenID(short crop, string citizenID)
        {
            var returnSummary = BuyingFacade.CPAContainerReturnBL()
                .GetByCitizenID(crop, citizenID)
                .GroupBy(x => x.ReceiveID)
                .Select(x => new m_CPAContainerReceiveAndReturnSummary
                {
                    ReturnQuantity = x.Sum(y => y.Quantity),
                    ReceiveID = x.Key
                })
                .ToList();

            var receiveSummary = BuyingFacade.CPAContainerReceiveBL()
                .GetByCitizenID(crop, citizenID);

            var resultList = (from a in receiveSummary
                              join b in returnSummary
                              on a.ReceiveID equals b.ReceiveID
                              into c
                              from d in c.DefaultIfEmpty()
                              select new m_CPAContainerReceiveAndReturnSummary
                              {
                                  FarmerCode = a.FarmerCode,
                                  ReceiveID = a.ReceiveID,
                                  ItemCode = a.Code,
                                  ItemName = a.CPAContainerItem.Name,
                                  Description = a.CPAContainerItem.Description,
                                  UnitName = a.CPAContainerItem.Unit,
                                  ReceiveQuantity = a.Quantity,
                                  ReturnQuantity = d == null ? 0 : d.ReturnQuantity,
                              })
                             .ToList();

            return resultList;
        }

        public static List<m_CPAContainerReceiveAndReturnSummary> GetByFarmerCode(short crop, string farmerCode)
        {
            var returnSummary = BuyingFacade.CPAContainerReturnBL()
                .GetByFarmerCode(crop, farmerCode)
                .Select(x => new m_CPAContainerReceiveAndReturnSummary
                {
                    ItemCode = x.CPAContainerReceive.Code,
                    ItemName = x.CPAContainerReceive.CPAContainerItem.Name,
                    Description = x.CPAContainerReceive.CPAContainerItem.Description,
                    UnitName = x.CPAContainerReceive.CPAContainerItem.Unit,
                    ReceiveQuantity = x.CPAContainerReceive.Quantity,
                    ReturnQuantity = x.Quantity,
                    ReceiveID = x.ReceiveID
                })
                .ToList();
            var receiveSummary = BuyingFacade.CPAContainerReceiveBL().GetByFarmerCode(crop, farmerCode);
            var resultList = (from a in receiveSummary
                              join b in returnSummary
                              on a.ReceiveID equals b.ReceiveID
                              into c
                              from d in c.DefaultIfEmpty()
                              select new m_CPAContainerReceiveAndReturnSummary
                              {
                                  ReceiveID = a.ReceiveID,
                                  ItemCode = a.Code,
                                  ItemName = a.CPAContainerItem.Name,
                                  Description = a.CPAContainerItem.Description,
                                  UnitName = a.CPAContainerItem.Unit,
                                  ReceiveQuantity = a.Quantity,
                                  ReturnQuantity = d == null ? 0 : d.ReturnQuantity,
                              })
                             .ToList();

            return resultList;
        }
    }
}
