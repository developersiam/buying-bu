﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BusinessLayer.Helper
{
    public static class SystemDeadlinesHelper
    {
        public static void FarmerContractDeadlinesValid(short crop)
        {
            if (DateTime.Now.Month > 3 && crop == DateTime.Now.Year)
                throw new ArgumentException("ระบบเปิดให้บันทึกข้อมูลได้ถึงสิ้นเดือนพฤษภาคมเท่านั้น");
        }

        public static void BeforeStartBuyingDeadLine(short crop,string farmerCode)
        {
            var documents = BuyingFacade.BuyingDocumentBL().GetByFarmer(crop, farmerCode);
            //if (documents.Count() > 0)
            if (documents.Count() > 100) //ใช้เงื่อนไขนี้สำหรับปลดล็อคชั่วคราวเพื่อทำลดหนี้หลังการขายในปี 2022
                throw new ArgumentException("มีการออกใบเวาเชอร์เพื่อจ่ายป้ายบาร์โค้ตให้กับชาวไร่ไปแล้ว ไม่สามารถทำรายการนี้ได้");
        }
    }
}