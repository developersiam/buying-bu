﻿using BusinessLayer.Model;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public static class DebtSetupHelper
    {
        public static List<m_DebtSetupSummary> GetByExtensionAgent(short crop, string extensionAgentCode)
        {
            /*
             * Account invoice ที่ถูกเอาไปทำลดหนี้แล้วจะไม่นำมาคิดในหน้าการตั้งหนี้
             * Account invoice ที่ถูกนำไปทำการตั้งหนี้แล้ว จะไม่สามารถทำลดหนี้ได้
             * Account invoice ที่ถูกนำไปทำการลดหนี้แล้ว จะไม่สามารถนำมาตั้งหนี้ได้
             */
            var accountInvoiceList = BuyingFacade.AccountInvoiceBL()
                .GetByExtensionAgentWithOutCreditNote(crop, extensionAgentCode)
                .Select(x => new AccountInvoice
                {
                    Invoice = x.Invoice,
                    InvoiceNo = x.InvoiceNo
                })
                .ToList();

            var invoiceDetailsList = BuyingFacade.InvoiceDetailsBL()
                .GetByExtensionAgent(crop, extensionAgentCode)
                .ToList();

            var summaryList = (from a in accountInvoiceList
                               from b in invoiceDetailsList
                               where a.InvoiceNo == b.AccountInvoiceNo
                               select b)
                               .GroupBy(x => new
                               {
                                   x.Invoice.Crop,
                                   x.Invoice.FarmerCode
                               })
                               .Select(x => new m_DebtSetupSummary
                               {
                                   Crop = x.Key.Crop,
                                   FarmerCode = x.Key.FarmerCode,
                                   AmountFromInvoice = (double)x.Sum(y => y.Quantity * y.UnitPrice)
                               })
                               .ToList();

            var debtSetupList = BuyingFacade.DebtSetupBL()
                .GetByExtensionAgentSetupCrop(crop, extensionAgentCode)
                .Where(x => x.DebtTypeCode != "L1")
                .GroupBy(x => new
                {
                    x.Crop,
                    x.FarmerCode
                })
                .Select(x => new m_DebtSetupSummary
                {
                    Crop = x.Key.Crop,
                    FarmerCode = x.Key.FarmerCode,
                    AmountFromDebtSetup = (double)x.Sum(y => y.Amount)
                })
                .ToList();

            var join1 = (from a in summaryList
                         join b in debtSetupList
                        on new { a.Crop, a.FarmerCode } equals new { b.Crop, b.FarmerCode }
                        into c
                         from d in c.DefaultIfEmpty()
                         select new m_DebtSetupSummary
                         {
                             Crop = a.Crop,
                             FarmerCode = a.FarmerCode,
                             AmountFromInvoice = a.AmountFromInvoice,
                             AmountFromDebtSetup = d?.AmountFromDebtSetup ?? 0
                         })
                         .ToList();

            // แก้ปัญหาการดึงข้อมูลช้า Eager Loading.
            // โดยการ select list มา join กันในภายหลังแทนการ load แบบ include ตารางที่เกี่ยวข้องมาในคราวเดียว
            var farmerProfileList = BuyingFacade.RegistrationBL()
                .GetBySupplier(crop, extensionAgentCode);

            var join2 = (from a in join1
                         from b in farmerProfileList
                         where a.Crop == b.Crop && a.FarmerCode == b.FarmerCode
                         select new m_DebtSetupSummary
                         {
                             Crop = a.Crop,
                             FarmerCode = a.FarmerCode,
                             CitizenID = b.Farmer.CitizenID,
                             Prefix = b.Farmer.Person.Prefix,
                             FirstName = b.Farmer.Person.FirstName,
                             LastName = b.Farmer.Person.LastName,
                             ExtensionAgentCode = b.Farmer.Supplier,
                             AmountFromInvoice = a.AmountFromInvoice,
                             AmountFromDebtSetup = a.AmountFromDebtSetup
                         })
                         .ToList();

            return join2;
        }

        public static List<m_DebtSetupSummary> GetByFarmer(short crop, string farmerCode)
        {
            var invoiceList = BuyingFacade.AccountInvoiceBL()
                .GetByFarmer(crop, farmerCode)
                .ToList();

            var debtList = invoiceList
                .GroupBy(x => new
                {
                    x.Invoice.Crop,
                    x.Invoice.FarmerCode,
                    x.Invoice.RegistrationFarmer.Farmer.Person.CitizenID,
                    x.Invoice.RegistrationFarmer.Farmer.Person.Prefix,
                    x.Invoice.RegistrationFarmer.Farmer.Person.FirstName,
                    x.Invoice.RegistrationFarmer.Farmer.Person.LastName,
                    x.Invoice.RegistrationFarmer.Farmer.Supplier
                })
                .Select(x => new m_DebtSetupSummary
                {
                    Crop = x.Key.Crop,
                    FarmerCode = x.Key.FarmerCode,
                    CitizenID = x.Key.CitizenID,
                    Prefix = x.Key.Prefix,
                    FirstName = x.Key.FirstName,
                    LastName = x.Key.LastName,
                    ExtensionAgentCode = x.Key.Supplier,
                    AmountFromInvoice = (double)x
                    .Sum(y => y.InvoiceDetails
                    .Sum(z => z.Quantity * z.UnitPrice))
                })
                .ToList();

            var debtSetupList = BuyingFacade.DebtSetupBL()
                .GetByFarmerDeductionCrop(crop, farmerCode)
                .GroupBy(x => new
                {
                    x.Crop,
                    x.FarmerCode
                })
                .Select(x => new m_DebtSetupSummary
                {
                    Crop = x.Key.Crop,
                    FarmerCode = x.Key.FarmerCode,
                    AmountFromDebtSetup = (double)x.Sum(y => y.Amount)
                })
                .ToList();

            var join = (from a in debtList
                        join b in debtSetupList on a.FarmerCode equals b.FarmerCode
                        into c
                        from d in c.DefaultIfEmpty()
                        select new m_DebtSetupSummary
                        {
                            Crop = a.Crop,
                            FarmerCode = a.FarmerCode,
                            CitizenID = a.CitizenID,
                            Prefix = a.Prefix,
                            FirstName = a.FirstName,
                            LastName = a.LastName,
                            ExtensionAgentCode = a.ExtensionAgentCode,
                            AmountFromInvoice = a.AmountFromInvoice,
                            AmountFromDebtSetup = d?.AmountFromDebtSetup ?? 0
                        })
                        .ToList();

            return join;
        }

        public static List<m_DebtAndReceiptSummary> GetByFarmerDeductionCrop(short deductionCrop, string farmerCode)
        {
            var debtSetupList = BuyingFacade.DebtSetupBL()
                .GetByFarmerDeductionCrop(deductionCrop, farmerCode);
            var receiptList = BuyingFacade.DebtReceiptBL()
                .GetByFarmerDeductionCrop(deductionCrop, farmerCode);

            var receiptGroupBy = receiptList
                .GroupBy(x => x.DebtSetupCode)
                .Select(x => new
                {
                    Code = x.Key,
                    TotalReceiptAmount = x.Sum(y => y.Amount)
                })
                .ToList();

            var join = (from a in debtSetupList
                        join b in receiptGroupBy
                        on a.DebtSetupCode equals b.Code
                        into c
                        from d in c.DefaultIfEmpty()
                        select new m_DebtAndReceiptSummary
                        {
                            DebtSetupCode = a.DebtSetupCode,
                            DebtTypeCode = a.DebtTypeCode,
                            DebtType = a.DebtType,
                            Crop = a.Crop,
                            FarmerCode = a.FarmerCode,
                            Amount = a.Amount,
                            DripProjectQuota = a.DripProjectQuota,
                            DeductPerKg = a.DeductPerKg,
                            DeductionCrop = a.DeductionCrop,
                            ReceiptAmount = d?.TotalReceiptAmount ?? 0,
                            BalanceAmount = a.Amount - (d?.TotalReceiptAmount ?? 0)
                        })
                        .ToList();

            return join;
        }

        public static List<m_DebtAndReceiptSummary> GetByFarmer(string farmerCode)
        {
            var debtSetupList = BuyingFacade.DebtSetupBL()
                .GetByFarmer(farmerCode)
                .Select(x => new m_DebtAndReceiptSummary
                {
                    DebtSetupCode = x.DebtSetupCode,
                    DebtTypeCode = x.DebtTypeCode,
                    DebtType = x.DebtType,
                    DeductionCrop = x.DeductionCrop,
                    Crop = x.Crop,
                    FarmerCode = x.FarmerCode,
                    Amount = x.Amount,
                    ReceiptAmount = x.DebtReceipts.Sum(y => y.Amount),
                    BalanceAmount = x.Amount - x.DebtReceipts.Sum(y => y.Amount)
                })
                .ToList();

            return debtSetupList;
        }
    }
}
