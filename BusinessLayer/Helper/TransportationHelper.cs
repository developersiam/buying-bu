﻿using BusinessLayer.Model;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public static class TransportationHelper
    {
        public static m_TransportationDocument GetByTransportationCode(string transportationCode)
        {
            UnitOfWork uow = new UnitOfWork();

            var model = uow.TransportationDocumentRepository
                    .GetSingle(td => td.TransportationDocumentCode == transportationCode,
                    td => td.Buyings);

            m_TransportationDocument returnModel = new m_TransportationDocument
            {
                Crop = model.Crop,
                BuyingStationCode = model.BuyingStationCode,
                SequenceNumber = model.SequenceNumber,
                TransportationDocumentCode = model.TransportationDocumentCode,
                TruckNumber = model.TruckNumber,
                CreateDate = model.CreateDate,
                CreateUser = model.CreateUser,
                MaximumWeight = model.MaximumWeight,
                IsFinish = model.IsFinish,
                IsTruckApprove = model.IsTruckApprove,
                ModifieBy = model.ModifieBy,
                ModifiedDate = model.ModifiedDate,
                TotalBale = model.Buyings.Count,
                TotalWeight = Convert.ToDecimal(model.Buyings.Sum(b => b.Weight)),
                Buyings = model.Buyings
            };

            return returnModel;

        }

        public static List<m_TransportationDocument> GetByCreateDate(short crop, string StationCode, DateTime createDate)
        {
            UnitOfWork uow = new UnitOfWork();

            return (from t in uow.TransportationDocumentRepository
                        .Query(td => td.Crop == crop &&
                        td.BuyingStationCode == StationCode &&
                        td.CreateDate.Day == createDate.Day &&
                        td.CreateDate.Month == createDate.Month &&
                        td.CreateDate.Year == createDate.Year,
                        null,
                        td => td.Buyings)
                    select new m_TransportationDocument
                    {
                        Crop = t.Crop,
                        BuyingStationCode = t.BuyingStationCode,
                        SequenceNumber = t.SequenceNumber,
                        TransportationDocumentCode = t.TransportationDocumentCode,
                        TruckNumber = t.TruckNumber,
                        CreateDate = t.CreateDate,
                        CreateUser = t.CreateUser,
                        MaximumWeight = t.MaximumWeight,
                        IsFinish = t.IsFinish,
                        IsTruckApprove = t.IsTruckApprove,
                        ModifieBy = t.ModifieBy,
                        ModifiedDate = t.ModifiedDate,
                        TotalBale = t.Buyings.Count,
                        TotalWeight = t.Buyings.Count <= 0 ? 0 : t.Buyings.Sum(b => b.Weight)
                    }).ToList();
        }
    }
}
