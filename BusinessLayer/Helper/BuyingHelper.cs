﻿using BusinessLayer.Model;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public static class BuyingHelper
    {
        public static List<m_Buying> GetMoistureResultByResultDate(DateTime inputResultDate)
        {
            UnitOfWork uow = new UnitOfWork();

            return (from buying in uow.BuyingRepository
                    .Query(b => b.MoistureResultDate.Value.Year == inputResultDate.Year &&
                    b.MoistureResultDate.Value.Month == inputResultDate.Month &&
                    b.MoistureResultDate.Value.Day == inputResultDate.Day)
                    select new m_Buying
                    {
                        Crop = buying.Crop,
                        BuyingStationCode = buying.BuyingStationCode,
                        FarmerCode = buying.FarmerCode,
                        BuyingDocumentNumber = buying.BuyingDocumentNumber,
                        BaleBarcode = buying.BaleBarcode,
                        RegisterBarcodeUser = buying.RegisterBarcodeUser,
                        RegisterBarcodeDate = buying.RegisterBarcodeDate,
                        ProjectType = buying.ProjectType,
                        PricingNumber = buying.PricingNumber,
                        Grade = buying.Grade,
                        GradeUser = buying.GradeUser,
                        GradeDate = buying.GradeDate,
                        ChangeGradeUser = buying.ChangeGradeUser,
                        BuyerCode = buying.BuyerCode,
                        Weight = buying.Weight,
                        WeightDate = buying.WeightDate,
                        WeightUser = buying.WeightUser,
                        RejectReason = buying.RejectReason,
                        TransportationDocumentCode = buying.TransportationDocumentCode,
                        LoadBaleToTruckDate = buying.LoadBaleToTruckDate,
                        LoadBaleToTruckUser = buying.LoadBaleToTruckUser,
                        MoistureResult = buying.MoistureResult,
                        MoistureResultDate = buying.MoistureResultDate,
                        MoistureResultUser = buying.MoistureResultUser
                    }).ToList();
        }

        public static m_Buying GetByBaleBarcode(string baleBarcode)
        {
            UnitOfWork uow = new UnitOfWork();

            var buying = uow.BuyingRepository
                .GetSingle(b => b.BaleBarcode == baleBarcode,
                b => b.BuyingGrade,
                b => b.TransportationDocument,
                b => b.BuyingDocument.RegistrationFarmer.Farmer);

            if (buying == null)
                throw new ArgumentException("ไม่พบข้อมูลห่อยาหมายเลข " + baleBarcode + " ในระบบ");

            m_Buying buyingInfo = new m_Buying
            {
                Crop = buying.Crop,
                BuyingStationCode = buying.BuyingStationCode,
                FarmerCode = buying.FarmerCode,
                BuyingDocumentNumber = buying.BuyingDocumentNumber,
                BaleBarcode = buying.BaleBarcode,
                RegisterBarcodeUser = buying.RegisterBarcodeUser,
                RegisterBarcodeDate = buying.RegisterBarcodeDate,
                ProjectType = buying.ProjectType,
                PricingNumber = buying.PricingNumber,
                Grade = buying.Grade,
                GradeUser = buying.GradeUser,
                GradeDate = buying.GradeDate,
                ChangeGradeUser = buying.ChangeGradeUser,
                BuyerCode = buying.BuyerCode,
                Weight = buying.Weight,
                WeightDate = buying.WeightDate,
                WeightUser = buying.WeightUser,
                RejectReason = buying.RejectReason,
                TransportationDocumentCode = buying.TransportationDocumentCode,
                LoadBaleToTruckDate = buying.LoadBaleToTruckDate,
                LoadBaleToTruckUser = buying.LoadBaleToTruckUser,
                MovingTransportationFrom = buying.MovingTransportationFrom,
                MovingTransportationDate = buying.MovingTransportationDate,
                MovingTransportationReason = buying.MovingTransportationReason,
                MovingBy = buying.MovingBy,
                CPAResult = buying.CPAResult,
                CPAResultDate = buying.CPAResultDate,
                CPAResultUser = buying.CPAResultUser,
                MoistureResult = buying.MoistureResult,
                MoistureResultDate = buying.MoistureResultDate,
                MoistureResultUser = buying.MoistureResultUser,
                IsGrade = buying.Grade == null ? false : true,
                IsReject = buying.RejectReason == null ? false : true,
                IsWeight = buying.Weight == null ? false : true,
                IsTruck = buying.TransportationDocumentCode == null ? false : true,
                IsExtraQuota = buying.IsExtraQuota,
                IsNTRMInspection = buying.IsNTRMInspection,
                UnitPrice = buying.Grade == null ? 0
                            : buying.Crop == 2020 && buying.IsExtraQuota == true && buying.ProjectType != "SC" && buying.MovingBy != "Project"
                                ? buying.BuyingGrade.UnitPrice - 2
                            //: buying.Crop == 2021 && buying.ProjectType == "NL"
                            //    ? buying.BuyingGrade.UnitPrice - 5
                            : buying.BuyingGrade.UnitPrice,
                Price = buying.Grade == null ? 0 : (
                            buying.Crop == 2020 && buying.IsExtraQuota == true && buying.ProjectType != "SC" && buying.MovingBy != "Project"
                                ? buying.BuyingGrade.UnitPrice - 2
                            //: buying.Crop == 2021 && buying.ProjectType == "NL"
                            //    ? buying.BuyingGrade.UnitPrice - 5
                            : buying.BuyingGrade.UnitPrice
                        ) * (buying.Weight == null ? 0 : buying.Weight),
                SupplierCode = buying.BuyingDocument.RegistrationFarmer.Farmer.Supplier,
                TransportationDocument = buying.TransportationDocument,
                BuyingDocument = buying.BuyingDocument
            };

            return buyingInfo;
        }

        public static List<m_Buying> GetByBuyingDocument(short crop, string farmerCode,
            string buyingStationCode, short buyingDocumentNumber)
        {
            UnitOfWork uow = new UnitOfWork();

            return (from buying in uow.BuyingRepository
                    .Get(b => b.Crop == crop &&
                    b.BuyingStationCode == buyingStationCode &&
                    b.FarmerCode == farmerCode &&
                    b.BuyingDocumentNumber == buyingDocumentNumber
                    , null
                    , b => b.BuyingGrade)
                    select new m_Buying
                    {
                        Crop = buying.Crop,
                        BuyingStationCode = buying.BuyingStationCode,
                        FarmerCode = buying.FarmerCode,
                        BuyingDocumentNumber = buying.BuyingDocumentNumber,
                        BaleBarcode = buying.BaleBarcode,
                        RegisterBarcodeUser = buying.RegisterBarcodeUser,
                        RegisterBarcodeDate = buying.RegisterBarcodeDate,
                        ProjectType = buying.ProjectType,
                        PricingNumber = buying.PricingNumber,
                        BuyingGrade = buying.BuyingGrade,
                        Grade = buying.Grade,
                        GradeUser = buying.GradeUser,
                        GradeDate = buying.GradeDate,
                        ChangeGradeUser = buying.ChangeGradeUser,
                        BuyerCode = buying.BuyerCode,
                        Weight = buying.Weight,
                        WeightDate = buying.WeightDate,
                        WeightUser = buying.WeightUser,
                        RejectReason = buying.RejectReason,
                        TransportationDocumentCode = buying.TransportationDocumentCode,
                        LoadBaleToTruckDate = buying.LoadBaleToTruckDate,
                        LoadBaleToTruckUser = buying.LoadBaleToTruckUser,
                        MovingTransportationFrom = buying.MovingTransportationFrom,
                        MovingTransportationDate = buying.MovingTransportationDate,
                        MovingTransportationReason = buying.MovingTransportationReason,
                        MovingBy = buying.MovingBy,
                        CPAResult = buying.CPAResult,
                        CPAResultDate = buying.CPAResultDate,
                        CPAResultUser = buying.CPAResultUser,
                        MoistureResult = buying.MoistureResult,
                        MoistureResultDate = buying.MoistureResultDate,
                        MoistureResultUser = buying.MoistureResultUser,
                        IsGrade = buying.Grade == null ? false : true,
                        IsReject = buying.RejectReason == null ? false : true,
                        IsWeight = buying.Weight == null ? false : true,
                        IsTruck = buying.TransportationDocumentCode == null ? false : true,
                        IsExtraQuota = buying.IsExtraQuota,
                        UnitPrice = buying.Grade == null ? 0
                            : buying.Crop == 2020 && buying.IsExtraQuota == true && buying.ProjectType != "SC" && buying.MovingBy != "Project"
                                ? buying.BuyingGrade.UnitPrice - 2
                            //: buying.Crop == 2021 && buying.ProjectType == "NL"
                            //    ? buying.BuyingGrade.UnitPrice - 5
                            : buying.BuyingGrade.UnitPrice,
                        Price = buying.Grade == null ? 0 : (
                            buying.Crop == 2020 && buying.IsExtraQuota == true && buying.ProjectType != "SC" && buying.MovingBy != "Project"
                                ? buying.BuyingGrade.UnitPrice - 2
                            //: buying.Crop == 2021 && buying.ProjectType == "NL"
                            //    ? buying.BuyingGrade.UnitPrice - 5
                            : buying.BuyingGrade.UnitPrice
                        ) * (buying.Weight == null ? 0 : buying.Weight)
                    }).ToList();
        }

        public static List<m_Buying> GetByTransportationCode(string transportationCode)
        {
            UnitOfWork uow = new UnitOfWork();

            return (from buying in GetByTransportationCode(transportationCode)
                    select new m_Buying
                    {
                        Crop = buying.Crop,
                        BuyingStationCode = buying.BuyingStationCode,
                        FarmerCode = buying.FarmerCode,
                        BuyingDocumentNumber = buying.BuyingDocumentNumber,
                        BaleBarcode = buying.BaleBarcode,
                        RegisterBarcodeUser = buying.RegisterBarcodeUser,
                        RegisterBarcodeDate = buying.RegisterBarcodeDate,
                        ProjectType = buying.ProjectType,
                        PricingNumber = buying.PricingNumber,
                        Grade = buying.Grade,
                        GradeUser = buying.GradeUser,
                        GradeDate = buying.GradeDate,
                        ChangeGradeUser = buying.ChangeGradeUser,
                        BuyerCode = buying.BuyerCode,
                        Weight = buying.Weight,
                        WeightDate = buying.WeightDate,
                        WeightUser = buying.WeightUser,
                        RejectReason = buying.RejectReason,
                        TransportationDocumentCode = buying.TransportationDocumentCode,
                        LoadBaleToTruckDate = buying.LoadBaleToTruckDate,
                        LoadBaleToTruckUser = buying.LoadBaleToTruckUser,
                        MovingTransportationFrom = buying.MovingTransportationFrom,
                        MovingTransportationDate = buying.MovingTransportationDate,
                        MovingTransportationReason = buying.MovingTransportationReason,
                        MovingBy = buying.MovingBy,
                        CPAResult = buying.CPAResult,
                        CPAResultDate = buying.CPAResultDate,
                        CPAResultUser = buying.CPAResultUser,
                        MoistureResult = buying.MoistureResult,
                        MoistureResultDate = buying.MoistureResultDate,
                        MoistureResultUser = buying.MoistureResultUser,
                        IsGrade = buying.Grade == null ? false : true,
                        IsReject = buying.RejectReason == null ? false : true,
                        IsWeight = buying.Weight == null ? false : true,
                        IsTruck = buying.TransportationDocumentCode == null ? false : true,
                        IsExtraQuota = buying.IsExtraQuota
                    }).ToList();
        }
    }
}
