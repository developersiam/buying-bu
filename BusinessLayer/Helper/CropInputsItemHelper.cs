﻿using BusinessLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public static class CropInputsItemHelper
    {
        public static m_CropInputsItem GetSingle(Guid conditionID)
        {
            var a = BuyingFacade.CropInputsConditionBL().GetSingle(conditionID);
            var b = AGMInventorySystemBL.BLServices.material_itemBL().GetSingle(a.ItemID);

            if (a == null)
                return new m_CropInputsItem();

            var result = new m_CropInputsItem
            {
                ConditionID = a.ConditionID,
                Crop = a.Crop,
                SupplierCode = a.SupplierCode,
                ItemID = a.ItemID,
                UnitPrice = a.UnitPrice,
                IsUsePerRai = a.IsUsePerRai,
                IsReferToInventory = a.IsReferToInventory,
                IsNotToInvoice = a.IsNotToInvoice,
                IsVat = a.IsVat,
                Description = a.Description,
                ModifiedDate = a.ModifiedDate,
                ModifiedUser = a.ModifiedUser,
                ItemName = b.item_name,
                LotNo = b.lotno,
                Unit = b.material_unit.unit_name,
                Brand = b.material_brand.brand_name,
                Category = b.material_category.category_name
            };

            return result;
        }

        public static List<m_CropInputsItem> GetBySupplierCode(short crop, string supplierCode)
        {
            var conditions = BuyingFacade.CropInputsConditionBL().GetBySupplierCode(crop, supplierCode);
            var items = AGMInventorySystemBL.BLServices.material_itemBL().GetAll();

            var result = (from a in conditions
                          from b in items
                          where a.ItemID == b.itemid
                          select new m_CropInputsItem
                          {
                              ConditionID = a.ConditionID,
                              Crop = a.Crop,
                              SupplierCode = a.SupplierCode,
                              ItemID = a.ItemID,
                              UnitPrice = a.UnitPrice,
                              IsUsePerRai = a.IsUsePerRai,
                              IsReferToInventory = a.IsReferToInventory,
                              IsNotToInvoice = a.IsNotToInvoice,
                              IsVat = a.IsVat,
                              Description = a.Description,
                              ModifiedDate = a.ModifiedDate,
                              ModifiedUser = a.ModifiedUser,
                              ItemName = b.item_name,
                              LotNo = b.lotno,
                              Unit = b.material_unit.unit_name,
                              Brand = b.material_brand.brand_name,
                              Category = b.material_category.category_name
                          }).ToList();

            return result;
        }

        public static List<m_CropInputsItem> GetByPackage(string packageCode)
        {
            var packageDetails = BuyingFacade.CropInputsPackageDetailsBL().GetByPackageCode(packageCode);
            var items = AGMInventorySystemBL.BLServices.material_itemBL().GetAll();

            var result = (from a in packageDetails
                          from b in items
                          where a.CropInputsCondition.ItemID == b.itemid
                          select new m_CropInputsItem
                          {
                              ConditionID = a.ConditionID,
                              Crop = a.CropInputsCondition.Crop,
                              SupplierCode = a.CropInputsCondition.SupplierCode,
                              ItemID = a.CropInputsCondition.ItemID,
                              Quantity = a.Quantity,
                              UnitPrice = a.CropInputsCondition.UnitPrice,
                              IsUsePerRai = a.CropInputsCondition.IsUsePerRai,
                              IsReferToInventory = a.CropInputsCondition.IsReferToInventory,
                              IsNotToInvoice = a.CropInputsCondition.IsNotToInvoice,
                              IsVat = a.CropInputsCondition.IsVat,
                              Description = a.CropInputsCondition.Description,
                              ModifiedDate = a.ModifiedDate,
                              ModifiedUser = a.ModifiedUser,
                              ItemName = b.item_name,
                              LotNo = b.lotno,
                              Unit = b.material_unit.unit_name,
                              Brand = b.material_brand.brand_name,
                              Category = b.material_category.category_name,
                              PackageCode = a.PackageCode
                          }).ToList();

            return result;
        }

        public static m_CropInputsItem GetByPackageDetails(string packageCode, Guid conditionID)
        {
            var a = BuyingFacade.CropInputsPackageDetailsBL()
                .GetSingle(packageCode, conditionID);
            var b = AGMInventorySystemBL.BLServices.material_itemBL()
                .GetSingle(a.CropInputsCondition.ItemID);

            if (a == null)
                return new m_CropInputsItem();

            var result = new m_CropInputsItem
            {
                ConditionID = a.ConditionID,
                Crop = a.CropInputsCondition.Crop,
                SupplierCode = a.CropInputsCondition.SupplierCode,
                ItemID = a.CropInputsCondition.ItemID,
                UnitPrice = a.CropInputsCondition.UnitPrice,
                IsUsePerRai = a.CropInputsCondition.IsUsePerRai,
                IsReferToInventory = a.CropInputsCondition.IsReferToInventory,
                IsVat = a.CropInputsCondition.IsVat,
                IsNotToInvoice = a.CropInputsCondition.IsNotToInvoice,
                Description = a.CropInputsCondition.Description,
                ModifiedDate = a.ModifiedDate,
                ModifiedUser = a.ModifiedUser,
                ItemName = b.item_name,
                LotNo = b.lotno,
                Unit = b.material_unit.unit_name,
                Brand = b.material_brand.brand_name,
                Category = b.material_category.category_name,
                PackageCode = a.PackageCode
            };

            return result;
        }
    }
}
