﻿using BusinessLayer.BL;
using BusinessLayer.Model;
using BusinessLayer.Model.FarmerBonus;
using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper.FarmerBonus
{
    public static class FullQuotaBonusHelper
    {
        public static List<m_FullQuotaBonusSummary> GetFullQuotaBonusSummary(short crop)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                ///******************************************************
                ///Update field CreditGroupCode to "WaitToTransfer" 
                ///when a farmer sold complete quota.
                ///******************************************************

                var regisList = uow.RegistrationFarmerRepository
                    .Get(x => x.Crop == crop &&
                    x.ActiveStatus == true,
                    null,
                    x => x.Farmer)
                    .ToList();
                var kgPerRaiList = uow.KilogramsPerRaiConfigurationRepository
                    .Get(x => x.Crop == crop)
                    .ToList();

                var quotaList = (from a in regisList
                                 from b in kgPerRaiList
                                 where a.Crop == b.Crop &&
                                 a.Farmer.Supplier == b.SupplierCode
                                 select new
                                 {
                                     a.Crop,
                                     a.FarmerCode,
                                     a.QuotaFromSignContract,
                                     a.CreditGroupCode,//Use this field for tracking transfer status for CY2024.
                                     b.KilogramsPerRai,
                                     QuotaKg = a.QuotaFromSignContract * b.KilogramsPerRai,
                                 })
                                 .ToList();

                var buyingList = uow.BuyingRepository
                    .Get(x => x.Crop == crop)
                    .GroupBy(x => new { x.Crop, x.FarmerCode })
                    .Select(x => new m_Buying
                    {
                        Crop = x.Key.Crop,
                        FarmerCode = x.Key.FarmerCode,
                        Weight = x.Sum(y => y.Weight)
                    })
                    .ToList();

                var quotaAndSoldList = (from a in quotaList
                                        join b in buyingList
                                        on new { a.Crop, a.FarmerCode } equals new { b.Crop, b.FarmerCode }
                                        into c
                                        from d in c.DefaultIfEmpty()
                                        select new m_FullQuotaBonusDetails
                                        {
                                            Crop = a.Crop,
                                            FarmerCode = a.FarmerCode,
                                            FullQuotaStatus = a.CreditGroupCode,
                                            QuotaRai = (int)a.QuotaFromSignContract,
                                            QuotaKg = (int)a.QuotaKg,
                                            SoldKg = (decimal)(c == null ? 0 : c.Sum(x => x.Weight))
                                        })
                                        .Where(x => x.SoldKg >= x.QuotaKg &&
                                        x.FullQuotaStatus == null)
                                        .ToList();
                string[] newFullQuotaList = quotaAndSoldList.Select(x => x.FarmerCode).ToArray();
                if (newFullQuotaList.Length > 0)
                {
                    var updateList = uow.RegistrationFarmerRepository
                        .Get(x => x.Crop == crop &&
                        newFullQuotaList.Contains(x.FarmerCode));
                    updateList.ForEach(x => x.CreditGroupCode = "Pending");
                    uow.RegistrationFarmerRepository.UpdateRange(updateList);
                    uow.Save();
                }

                ///******************************************************
                ///Return list by extension agent and area.
                ///******************************************************
                var returnList = uow.RegistrationFarmerRepository
                                .Get(x => x.Crop == crop &&
                                x.ActiveStatus == true,
                                null,
                                x => x.Farmer,
                                x => x.Farmer.Supplier1,
                                x => x.BuyingDocuments)
                                .GroupBy(x => new
                                {
                                    x.Crop,
                                    x.Farmer.Supplier,
                                    x.Farmer.Supplier1.SupplierArea
                                })
                                .Select(x => new m_FullQuotaBonusSummary
                                {
                                    Crop = x.Key.Crop,
                                    ExtenstionAgentCode = x.Key.Supplier,
                                    AreaCode = x.Key.SupplierArea,
                                    TotalFarmer = x.Count(),
                                    Pending = x.Count(y => y.CreditGroupCode == "Pending"),
                                    Transferred = x.Count(y => y.CreditGroupCode == "Transferred"),
                                    Completed = x.Count(y => y.CreditGroupCode == "Completed")
                                })
                                .ToList();

                return returnList;
            }
        }

        public static List<m_FullQuotaBonusDetails> GetFullQuotaBonusDetails(short crop, string status)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var contractList = uow.RegistrationFarmerRepository
                    .Get(x => x.Crop == crop &&
                    x.CreditGroupCode.Contains(status) &&
                    x.BankID != null,
                    null,
                    x => x.Bank,
                    x => x.Farmer,
                    x => x.Farmer.Supplier1,
                    x => x.Farmer.Person)
                    .ToList();

                var kgPerRaiList = uow.KilogramsPerRaiConfigurationRepository
                    .Get(x => x.Crop == crop)
                    .ToList();

                var quotaList = (from a in contractList
                                 from b in kgPerRaiList
                                 where a.Crop == b.Crop &&
                                 a.Farmer.Supplier == b.SupplierCode
                                 select new m_FullQuotaBonusDetails
                                 {
                                     Crop = a.Crop,
                                     AreaCode = a.Farmer.Supplier1.SupplierArea,
                                     ExtensionAgentCode = a.Farmer.Supplier,
                                     FarmerCode = a.FarmerCode,
                                     CitizenID = a.Farmer.CitizenID,
                                     FirstName = a.Farmer.Person.FirstName,
                                     LastName = a.Farmer.Person.LastName,
                                     BankAccount = a.BookBank,
                                     BankBranchCode = a.BankBranchCode,
                                     BankBranch = a.BankBranch,
                                     BankName = a.Bank.BankName,
                                     QuotaRai = (int)a.QuotaFromSignContract,
                                     QuotaKg = (int)a.QuotaFromSignContract * b.KilogramsPerRai,
                                     FullQuotaStatus = a.CreditGroupCode,
                                     ReceiptOrder = a.PreviousCropTotalVolume
                                 })
                                .ToList();

                var soldList = uow.BuyingRepository
                    .Get(x => x.Crop == crop)
                    .GroupBy(x => new
                    {
                        x.Crop,
                        x.FarmerCode
                    })
                    .Select(x => new
                    {
                        x.Key.Crop,
                        x.Key.FarmerCode,
                        SoldKg = x.Sum(y => y.Weight),
                    })
                    .ToList();

                var farmerCodeList = contractList
                    .GroupBy(x => x.FarmerCode)
                    .Select(x => x.Key)
                    .ToList();

                var debtSetupList = uow.DebtSetupRepository
                    .Get(x => farmerCodeList.Contains(x.FarmerCode)
                    && x.DeductionCrop <= crop)
                    .ToList();

                var debtAmountList = debtSetupList
                    .GroupBy(x => new { x.FarmerCode })
                    .Select(x => new
                    {
                        x.Key.FarmerCode,
                        DebtAmount = x.Sum(y => y.Amount)
                    })
                    .ToList();

                var debtSetupCodeList = debtSetupList
                    .GroupBy(x => x.DebtSetupCode)
                    .Select(x => x.Key)
                    .ToList();

                var debtReceiptList = uow.DebtReceiptRepository
                    .Get(x => debtSetupCodeList.Contains(x.DebtSetupCode))
                    .GroupBy(x => x.FarmerCode)
                    .Select(x => new
                    {
                        FarmerCode = x.Key,
                        ReceiptAmount = x.Sum(y => y.Amount)
                    })
                    .ToList();

                var debtBalanceList = (from a in debtAmountList
                                       from b in debtReceiptList
                                       where a.FarmerCode == b.FarmerCode
                                       select new
                                       {
                                           a.FarmerCode,
                                           a.DebtAmount,
                                           b.ReceiptAmount,
                                           DebtBalanceAmount = a.DebtAmount - b.ReceiptAmount
                                       })
                                      .ToList();

                var resultList = (from a in quotaList
                                  join b in soldList
                                  on new { a.Crop, a.FarmerCode } equals new { b.Crop, b.FarmerCode }
                                  into c
                                  from d in c.DefaultIfEmpty()
                                  select new m_FullQuotaBonusDetails
                                  {
                                      Crop = a.Crop,
                                      AreaCode = a.AreaCode,
                                      ExtensionAgentCode = a.ExtensionAgentCode,
                                      FarmerCode = a.FarmerCode,
                                      CitizenID = a.CitizenID,
                                      FirstName = a.FirstName,
                                      LastName = a.LastName,
                                      BankAccount = a.BankAccount,
                                      BankBranchCode = a.BankBranchCode,
                                      BankBranch = a.BankBranch,
                                      BankName = a.BankName,
                                      QuotaRai = a.QuotaRai,
                                      QuotaKg = a.QuotaKg,
                                      SoldKg = (decimal)(d == null ? 0 : d.SoldKg),
                                      FullQuotaStatus = a.FullQuotaStatus,
                                      BonusAmount = a.QuotaRai * 2000,
                                      ReceiptOrder = a.ReceiptOrder
                                  })
                                  .ToList();

                var returnList = (from a in resultList
                                  join b in debtBalanceList
                                  on a.FarmerCode equals b.FarmerCode
                                  into c
                                  from d in c.DefaultIfEmpty()
                                  select new m_FullQuotaBonusDetails
                                  {
                                      Crop = a.Crop,
                                      AreaCode = a.AreaCode,
                                      ExtensionAgentCode = a.ExtensionAgentCode,
                                      CitizenID = a.CitizenID,
                                      FarmerCode = a.FarmerCode,
                                      Prefix = a.Prefix,
                                      FirstName = a.FirstName,
                                      LastName = a.LastName,
                                      Village = a.Village,
                                      HouseNumber = a.HouseNumber,
                                      Tumbon = a.Tumbon,
                                      Amphur = a.Amphur,
                                      Province = a.Province,
                                      BankAccount = a.BankAccount,
                                      BankBranch = a.BankBranch,
                                      BankBranchCode = a.BankBranchCode,
                                      BankName = a.BankName,
                                      QuotaRai = a.QuotaRai,
                                      QuotaKg = a.QuotaKg,
                                      SoldKg = a.SoldKg,
                                      BonusAmount = a.BonusAmount,
                                      TransferredDate = a.TransferredDate,
                                      ReceiptOrder = a.ReceiptOrder,
                                      DebtAmount = d == null ? 0 : d.DebtAmount,
                                      DebtReceipt = d == null ? 0 : d.ReceiptAmount,
                                      DebtBalance = d == null ? 0 : d.DebtBalanceAmount,
                                  })
                                 .ToList();

                return returnList;
            }
        }

        public static List<m_FullQuotaBonusDetails> GetFullQuotaBonusDetailsBySelectedFarmer(short crop, string status, string[] farmerCodeList)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var contractList = uow.RegistrationFarmerRepository
                    .Get(x => x.Crop == crop &&
                    farmerCodeList.Contains(x.FarmerCode) &&
                    x.CreditGroupCode.Contains(status) &&
                    x.BankID != null,
                    null,
                    x => x.Bank,
                    x => x.Farmer,
                    x => x.Farmer.Supplier1,
                    x => x.Farmer.Person)
                    .ToList();

                var kgPerRaiList = uow.KilogramsPerRaiConfigurationRepository
                    .Get(x => x.Crop == crop)
                    .ToList();

                var quotaList = (from a in contractList
                                 from b in kgPerRaiList
                                 where a.Crop == b.Crop &&
                                 a.Farmer.Supplier == b.SupplierCode
                                 select new m_FullQuotaBonusDetails
                                 {
                                     Crop = a.Crop,
                                     AreaCode = a.Farmer.Supplier1.SupplierArea,
                                     ExtensionAgentCode = a.Farmer.Supplier,
                                     FarmerCode = a.FarmerCode,
                                     CitizenID = a.Farmer.CitizenID,
                                     Prefix = a.Farmer.Person.Prefix,
                                     FirstName = a.Farmer.Person.FirstName,
                                     LastName = a.Farmer.Person.LastName,
                                     HouseNumber = a.Farmer.Person.HouseNumber,
                                     Village = a.Farmer.Person.Village,
                                     Tumbon = a.Farmer.Person.Tumbon,
                                     Amphur = a.Farmer.Person.Amphur,
                                     Province = a.Farmer.Person.Province,
                                     BankAccount = a.BookBank,
                                     BankBranchCode = a.BankBranchCode,
                                     BankBranch = a.BankBranch,
                                     BankName = a.Bank.BankName,
                                     QuotaRai = (int)a.QuotaFromSignContract,
                                     QuotaKg = (int)a.QuotaFromSignContract * b.KilogramsPerRai,
                                     FullQuotaStatus = a.CreditGroupCode,
                                     TransferredDate = a.TTMQuotaStation,
                                     ReceiptOrder = a.PreviousCropTotalVolume
                                 })
                                .ToList();

                var soldList = uow.BuyingRepository
                    .Get(x => x.Crop == crop)
                    .GroupBy(x => new
                    {
                        x.Crop,
                        x.FarmerCode
                    })
                    .Select(x => new
                    {
                        x.Key.Crop,
                        x.Key.FarmerCode,
                        SoldKg = x.Sum(y => y.Weight),
                    })
                    .ToList();

                var debtSetupList = uow.DebtSetupRepository
                    .Get(x => farmerCodeList.Contains(x.FarmerCode)
                    && x.DeductionCrop <= crop)
                    .ToList();

                var debtAmountList = debtSetupList
                    .GroupBy(x => new { x.FarmerCode })
                    .Select(x => new
                    {
                        x.Key.FarmerCode,
                        DebtAmount = x.Sum(y => y.Amount)
                    })
                    .ToList();

                var debtSetupCodeList = debtSetupList
                    .GroupBy(x => x.DebtSetupCode)
                    .Select(x => x.Key)
                    .ToList();

                var debtReceiptList = uow.DebtReceiptRepository
                    .Get(x => debtSetupCodeList.Contains(x.DebtSetupCode))
                    .GroupBy(x => x.FarmerCode)
                    .Select(x => new
                    {
                        FarmerCode = x.Key,
                        ReceiptAmount = x.Sum(y => y.Amount)
                    })
                    .ToList();

                var debtBalanceList = (from a in debtAmountList
                                       from b in debtReceiptList
                                       where a.FarmerCode == b.FarmerCode
                                       select new
                                       {
                                           a.FarmerCode,
                                           a.DebtAmount,
                                           b.ReceiptAmount,
                                           DebtBalanceAmount = a.DebtAmount - b.ReceiptAmount
                                       })
                                      .ToList();

                var resultList = (from a in quotaList
                                  join b in soldList
                                  on new { a.Crop, a.FarmerCode } equals new { b.Crop, b.FarmerCode }
                                  into c
                                  from d in c.DefaultIfEmpty()
                                  select new m_FullQuotaBonusDetails
                                  {
                                      Crop = a.Crop,
                                      AreaCode = a.AreaCode,
                                      ExtensionAgentCode = a.ExtensionAgentCode,
                                      FarmerCode = a.FarmerCode,
                                      CitizenID = a.CitizenID,
                                      Prefix = a.Prefix,
                                      FirstName = a.FirstName,
                                      LastName = a.LastName,
                                      Village = a.Village,
                                      HouseNumber = a.HouseNumber,
                                      Tumbon = a.Tumbon,
                                      Amphur = a.Amphur,
                                      Province = a.Province,
                                      BankAccount = a.BankAccount,
                                      BankBranchCode = a.BankBranchCode,
                                      BankBranch = a.BankBranch,
                                      BankName = a.BankName,
                                      QuotaRai = a.QuotaRai,
                                      QuotaKg = a.QuotaKg,
                                      SoldKg = (decimal)(d == null ? 0 : d.SoldKg),
                                      FullQuotaStatus = a.FullQuotaStatus,
                                      BonusAmount = a.QuotaRai * 2000,
                                      TransferredDate = a.TransferredDate,
                                      ReceiptOrder = a.ReceiptOrder
                                  })
                                  .ToList();

                var returnList = (from a in resultList
                                  join b in debtBalanceList
                                  on a.FarmerCode equals b.FarmerCode
                                  into c
                                  from d in c.DefaultIfEmpty()
                                  select new m_FullQuotaBonusDetails
                                  {
                                      Crop = a.Crop,
                                      AreaCode = a.AreaCode,
                                      ExtensionAgentCode = a.ExtensionAgentCode,
                                      CitizenID = a.CitizenID,
                                      FarmerCode = a.FarmerCode,
                                      Prefix = a.Prefix,
                                      FirstName = a.FirstName,
                                      LastName = a.LastName,
                                      Village = a.Village,
                                      HouseNumber = a.HouseNumber,
                                      Tumbon = a.Tumbon,
                                      Amphur = a.Amphur,
                                      Province = a.Province,
                                      BankAccount = a.BankAccount,
                                      BankBranch = a.BankBranch,
                                      BankBranchCode = a.BankBranchCode,
                                      BankName = a.BankName,
                                      QuotaRai = a.QuotaRai,
                                      QuotaKg = a.QuotaKg,
                                      SoldKg = a.SoldKg,
                                      BonusAmount = a.BonusAmount,
                                      TransferredDate = a.TransferredDate,
                                      ReceiptOrder = a.ReceiptOrder,
                                      DebtAmount = d == null ? 0 : d.DebtAmount,
                                      DebtReceipt = d == null ? 0 : d.ReceiptAmount,
                                      DebtBalance = d == null ? 0 : d.DebtBalanceAmount,
                                  })
                                 .ToList();

                return returnList;
            }
        }
    }
}
