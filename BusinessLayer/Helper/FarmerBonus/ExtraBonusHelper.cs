﻿using BusinessLayer.Model.FarmerBonus;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper.FarmerBonus
{
    public static class ExtraBonusHelper
    {
        public static List<m_ExtraBonusSummary> GetExtraBonusSummaries(short crop)
        {
            try
            {
                using (var uow = new UnitOfWork())
                {
                    var bonusList = uow.ExtraBonusRepository
                        .Get(x => x.Crop == crop)
                        .GroupBy(x => new
                        {
                            x.Crop,
                            x.FarmerCode,
                            x.ExtraBonusStatus
                        })
                        .Select(x => new
                        {
                            x.Key.Crop,
                            x.Key.FarmerCode,
                            x.Key.ExtraBonusStatus,
                            BonusAmount = x.Sum(y => y.ExtraBonusAmount),
                            ExtraSoldKg = x.Sum(y => y.ExtaBonusKg)
                        })
                        .ToList();

                    var farmerCodeList = bonusList
                        .Select(x => x.FarmerCode);
                    var farmerList = uow.FarmerRepository
                        .Get(x => farmerCodeList.Contains(x.FarmerCode)
                        , null
                        , x => x.Supplier1)
                        .ToList();
                    var returnList = (from a in bonusList
                                      from b in farmerList
                                      where a.FarmerCode == b.FarmerCode
                                      select new
                                      {
                                          a.Crop,
                                          a.FarmerCode,
                                          a.ExtraBonusStatus,
                                          a.ExtraSoldKg,
                                          a.BonusAmount,
                                          b.Supplier,
                                          b.Supplier1.SupplierArea
                                      })
                                      .GroupBy(x => new
                                      {
                                          x.Crop,
                                          x.SupplierArea,
                                          x.Supplier
                                      })
                                      .Select(x => new m_ExtraBonusSummary
                                      {
                                          Crop = x.Key.Crop,
                                          AreaCode = x.Key.SupplierArea,
                                          ExtenstionAgentCode = x.Key.Supplier,
                                          TotalFarmer = x.Count(),
                                          Pending = x.Where(y => y.ExtraBonusStatus == "Pending").Count(),
                                          Transferred = x.Where(y => y.ExtraBonusStatus == "Transferred").Count(),
                                          Completed = x.Where(y => y.ExtraBonusStatus == "Completed").Count(),
                                      })
                                      .ToList();

                    return returnList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<m_ExtraBonusDetails> GetExtraBonusDetails(short crop, string status)
        {
            try
            {
                using (var uow = new UnitOfWork())
                {
                    var bonusList = uow.ExtraBonusRepository
                        .Get(x => x.Crop == crop &&
                        x.ExtraBonusStatus == status)
                        .GroupBy(x => new
                        {
                            x.Crop,
                            x.FarmerCode,
                            x.ExtraBonusStatus,
                            x.ReceiptOrder,
                            x.TransferredDate
                        })
                        .Select(x => new
                        {
                            x.Key.Crop,
                            x.Key.FarmerCode,
                            x.Key.ExtraBonusStatus,
                            x.Key.ReceiptOrder,
                            x.Key.TransferredDate,
                            BonusAmount = x.Sum(y => y.ExtraBonusAmount),
                            ExtraSoldKg = x.Sum(y => y.ExtaBonusKg)
                        })
                        .ToList();

                    var farmerCodeList = bonusList
                        .Select(x => x.FarmerCode);
                    var contractList = uow.RegistrationFarmerRepository
                        .Get(x => x.Crop == crop &&
                        farmerCodeList.Contains(x.FarmerCode) &&
                        x.BankID != null,
                        null,
                        x => x.Bank,
                        x => x.Farmer,
                        x => x.Farmer.Supplier1,
                        x => x.Farmer.Person)
                        .ToList();

                    var kgPerRaiList = uow.KilogramsPerRaiConfigurationRepository
                        .Get(x => x.Crop == crop)
                        .ToList();

                    var quotaList = (from a in contractList
                                     from b in kgPerRaiList
                                     where a.Crop == b.Crop &&
                                     a.Farmer.Supplier == b.SupplierCode
                                     select new
                                     {
                                         a.Crop,
                                         a.FarmerCode,
                                         QuotaRai = (int)a.QuotaFromSignContract,
                                         QuotaKg = (int)a.QuotaFromSignContract * b.KilogramsPerRai
                                     })
                                .ToList();

                    var soldList = uow.BuyingRepository
                        .Get(x => x.Crop == crop &&
                        farmerCodeList.Contains(x.FarmerCode) &&
                        x.Weight != null)
                        .GroupBy(x => new
                        {
                            x.Crop,
                            x.FarmerCode
                        })
                        .Select(x => new
                        {
                            x.Key.Crop,
                            x.Key.FarmerCode,
                            SoldKg = x.Sum(y => y.Weight)
                        })
                        .ToList();

                    var debtSetupList = uow.DebtSetupRepository
                        .Get(x => farmerCodeList.Contains(x.FarmerCode)
                        && x.DeductionCrop <= crop)
                        .ToList();

                    var debtAmountList = debtSetupList
                        .GroupBy(x => new { x.FarmerCode })
                        .Select(x => new
                        {
                            x.Key.FarmerCode,
                            DebtAmount = x.Sum(y => y.Amount)
                        })
                        .ToList();

                    var debtSetupCodeList = debtSetupList
                        .GroupBy(x => x.DebtSetupCode)
                        .Select(x => x.Key)
                        .ToList();

                    var debtReceiptList = uow.DebtReceiptRepository
                        .Get(x => debtSetupCodeList.Contains(x.DebtSetupCode))
                        .GroupBy(x => x.FarmerCode)
                        .Select(x => new
                        {
                            FarmerCode = x.Key,
                            ReceiptAmount = x.Sum(y => y.Amount)
                        })
                        .ToList();
                    var debtBalanceList = (from a in debtAmountList
                                           from b in debtReceiptList
                                           where a.FarmerCode == b.FarmerCode
                                           select new
                                           {
                                               a.FarmerCode,
                                               a.DebtAmount,
                                               b.ReceiptAmount,
                                               DebtBalanceAmount = a.DebtAmount - b.ReceiptAmount
                                           })
                                          .ToList();

                    var fixAndSoldList = (
                         from a in quotaList
                         join b in soldList
                         on new { a.Crop, a.FarmerCode } equals new { b.Crop, b.FarmerCode }
                         into c
                         from d in c.DefaultIfEmpty()
                         select new
                         {
                             a.Crop,
                             a.FarmerCode,
                             a.QuotaRai,
                             a.QuotaKg,
                             SoldKg = c == null ? 0 : d.SoldKg,
                         }
                        ).ToList();

                    var farmerSummaryList = (
                        from a in fixAndSoldList
                        from b in contractList
                        where a.Crop == b.Crop &&
                        a.FarmerCode == b.FarmerCode
                        select new m_ExtraBonusDetails
                        {
                            Crop = a.Crop,
                            AreaCode = b.Farmer.Supplier1.SupplierArea,
                            ExtensionAgentCode = b.Farmer.Supplier,
                            CitizenID = b.Farmer.CitizenID,
                            FarmerCode = b.FarmerCode,
                            Prefix = b.Farmer.Person.Prefix,
                            FirstName = b.Farmer.Person.FirstName,
                            LastName = b.Farmer.Person.LastName,
                            Village = b.Farmer.Person.Village,
                            HouseNumber = b.Farmer.Person.HouseNumber,
                            Tumbon = b.Farmer.Person.Tumbon,
                            Amphur = b.Farmer.Person.Amphur,
                            Province = b.Farmer.Person.Province,
                            BankAccount = b.BookBank,
                            BankBranch = b.BankBranch,
                            BankBranchCode = b.BankBranchCode,
                            BankName = b.Bank.BankName,
                            QuotaRai = a.QuotaRai,
                            QuotaKg = a.QuotaKg,
                            SoldKg = (decimal)a.SoldKg
                        })
                        .ToList();

                    var joinBonusList = (from a in farmerSummaryList
                                         from b in bonusList
                                         where a.Crop == b.Crop
                                         && a.FarmerCode == b.FarmerCode
                                         select new m_ExtraBonusDetails
                                         {
                                             Crop = a.Crop,
                                             AreaCode = a.AreaCode,
                                             ExtensionAgentCode = a.ExtensionAgentCode,
                                             CitizenID = a.CitizenID,
                                             FarmerCode = a.FarmerCode,
                                             Prefix = a.Prefix,
                                             FirstName = a.FirstName,
                                             LastName = a.LastName,
                                             Village = a.Village,
                                             HouseNumber = a.HouseNumber,
                                             Tumbon = a.Tumbon,
                                             Amphur = a.Amphur,
                                             Province = a.Province,
                                             BankAccount = a.BankAccount,
                                             BankBranch = a.BankBranch,
                                             BankBranchCode = a.BankBranchCode,
                                             BankName = a.BankName,
                                             QuotaRai = a.QuotaRai,
                                             QuotaKg = a.QuotaKg,
                                             SoldKg = a.SoldKg,
                                             ExtraSoldKg = b.ExtraSoldKg,
                                             ExtraQuotaStatus = b.ExtraBonusStatus,
                                             BonusAmount = b.BonusAmount,
                                             TransferredDate = b.TransferredDate,
                                             ReceiptOrder = b.ReceiptOrder
                                         })
                                         .ToList();

                    var returnList = (from a in joinBonusList
                                      join b in debtBalanceList
                                      on a.FarmerCode equals b.FarmerCode
                                      into c
                                      from d in c.DefaultIfEmpty()
                                      select new m_ExtraBonusDetails
                                      {
                                          Crop = a.Crop,
                                          AreaCode = a.AreaCode,
                                          ExtensionAgentCode = a.ExtensionAgentCode,
                                          CitizenID = a.CitizenID,
                                          FarmerCode = a.FarmerCode,
                                          Prefix = a.Prefix,
                                          FirstName = a.FirstName,
                                          LastName = a.LastName,
                                          Village = a.Village,
                                          HouseNumber = a.HouseNumber,
                                          Tumbon = a.Tumbon,
                                          Amphur = a.Amphur,
                                          Province = a.Province,
                                          BankAccount = a.BankAccount,
                                          BankBranch = a.BankBranch,
                                          BankBranchCode = a.BankBranchCode,
                                          BankName = a.BankName,
                                          QuotaRai = a.QuotaRai,
                                          QuotaKg = a.QuotaKg,
                                          SoldKg = a.SoldKg,
                                          ExtraSoldKg = a.ExtraSoldKg,
                                          ExtraQuotaStatus = a.ExtraQuotaStatus,
                                          BonusAmount = a.BonusAmount,
                                          TransferredDate = a.TransferredDate,
                                          ReceiptOrder = a.ReceiptOrder,
                                          DebtAmount = d == null ? 0 : d.DebtAmount,
                                          DebtReceipt = d == null ? 0 : d.ReceiptAmount,
                                          DebtBalance = d == null ? 0 : d.DebtBalanceAmount,
                                      })
                                     .ToList();

                    return returnList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<m_ExtraBonusDetails> GetExtraBonusDetailsBySelectedReceiptOrder(short crop, string status, List<int> receiptOrderList)
        {
            try
            {
                using (var uow = new UnitOfWork())
                {
                    var bonusList = uow.ExtraBonusRepository
                        .Get(x => x.Crop == crop &&
                        x.ExtraBonusStatus == status &&
                        receiptOrderList.Contains((int)x.ReceiptOrder))
                        .GroupBy(x => new
                        {
                            x.Crop,
                            x.FarmerCode,
                            x.ExtraBonusStatus,
                            x.ReceiptOrder,
                            x.TransferredDate
                        })
                        .Select(x => new
                        {
                            x.Key.Crop,
                            x.Key.FarmerCode,
                            x.Key.ExtraBonusStatus,
                            x.Key.ReceiptOrder,
                            x.Key.TransferredDate,
                            BonusAmount = x.Sum(y => y.ExtraBonusAmount),
                            ExtraSoldKg = x.Sum(y => y.ExtaBonusKg)
                        })
                        .ToList();

                    var farmerCodeList = bonusList
                        .Select(x => x.FarmerCode)
                        .ToList();

                    var contractList = uow.RegistrationFarmerRepository
                        .Get(x => x.Crop == crop &&
                        farmerCodeList.Contains(x.FarmerCode) &&
                        x.BankID != null,
                        null,
                        x => x.Bank,
                        x => x.Farmer,
                        x => x.Farmer.Supplier1,
                        x => x.Farmer.Person)
                        .ToList();

                    var kgPerRaiList = uow.KilogramsPerRaiConfigurationRepository
                        .Get(x => x.Crop == crop)
                        .ToList();

                    var quotaList = (from a in contractList
                                     from b in kgPerRaiList
                                     where a.Crop == b.Crop &&
                                     a.Farmer.Supplier == b.SupplierCode
                                     select new
                                     {
                                         a.Crop,
                                         a.FarmerCode,
                                         QuotaRai = (int)a.QuotaFromSignContract,
                                         QuotaKg = (int)a.QuotaFromSignContract * b.KilogramsPerRai
                                     })
                                .ToList();

                    var soldList = uow.BuyingRepository
                        .Get(x => x.Crop == crop &&
                        farmerCodeList.Contains(x.FarmerCode) &&
                        x.Weight != null)
                        .GroupBy(x => new
                        {
                            x.Crop,
                            x.FarmerCode
                        })
                        .Select(x => new
                        {
                            x.Key.Crop,
                            x.Key.FarmerCode,
                            SoldKg = x.Sum(y => y.Weight)
                        })
                        .ToList();

                    var debtSetupList = uow.DebtSetupRepository
                        .Get(x => farmerCodeList.Contains(x.FarmerCode)
                        && x.DeductionCrop <= crop
                        )
                        .ToList();

                    var debtAmountList = debtSetupList
                        .GroupBy(x => new { x.FarmerCode })
                        .Select(x => new
                        {
                            x.Key.FarmerCode,
                            DebtAmount = x.Sum(y => y.Amount)
                        })
                        .ToList();

                    var debtSetupCodeList = debtSetupList
                        .GroupBy(x => x.DebtSetupCode)
                        .Select(x => x.Key)
                        .ToList();

                    var debtReceiptList = uow.DebtReceiptRepository
                        .Get(x => debtSetupCodeList.Contains(x.DebtSetupCode))
                        .GroupBy(x => x.FarmerCode)
                        .Select(x => new
                        {
                            FarmerCode = x.Key,
                            ReceiptAmount = x.Sum(y => y.Amount)
                        })
                        .ToList();
                    var debtBalanceList = (from a in debtAmountList
                                           from b in debtReceiptList
                                           where a.FarmerCode == b.FarmerCode
                                           select new
                                           {
                                               a.FarmerCode,
                                               a.DebtAmount,
                                               b.ReceiptAmount,
                                               DebtBalanceAmount = a.DebtAmount - b.ReceiptAmount
                                           })
                                          .ToList();

                    var abcList = (from a in quotaList
                                   from b in soldList
                                   from c in contractList
                                   where a.Crop == b.Crop &&
                                   a.FarmerCode == b.FarmerCode &&
                                   a.Crop == c.Crop &&
                                   a.FarmerCode == c.FarmerCode
                                   select new m_ExtraBonusDetails
                                   {
                                       Crop = a.Crop,
                                       AreaCode = c.Farmer.Supplier1.SupplierArea,
                                       ExtensionAgentCode = c.Farmer.Supplier,
                                       CitizenID = c.Farmer.CitizenID,
                                       FarmerCode = c.FarmerCode,
                                       Prefix = c.Farmer.Person.Prefix,
                                       FirstName = c.Farmer.Person.FirstName,
                                       LastName = c.Farmer.Person.LastName,
                                       Village = c.Farmer.Person.Village,
                                       HouseNumber = c.Farmer.Person.HouseNumber,
                                       Tumbon = c.Farmer.Person.Tumbon,
                                       Amphur = c.Farmer.Person.Amphur,
                                       Province = c.Farmer.Person.Province,
                                       BankAccount = c.BookBank,
                                       BankBranch = c.BankBranch,
                                       BankBranchCode = c.BankBranchCode,
                                       BankName = c.Bank.BankName,
                                       QuotaRai = a.QuotaRai,
                                       QuotaKg = a.QuotaKg,
                                       SoldKg = (decimal)b.SoldKg
                                   })
                                   .ToList();

                    var joinBonusList = (from a in abcList
                                         from b in bonusList
                                         where a.Crop == b.Crop
                                         && a.FarmerCode == b.FarmerCode
                                         select new m_ExtraBonusDetails
                                         {
                                             Crop = a.Crop,
                                             AreaCode = a.AreaCode,
                                             ExtensionAgentCode = a.ExtensionAgentCode,
                                             CitizenID = a.CitizenID,
                                             FarmerCode = a.FarmerCode,
                                             Prefix = a.Prefix,
                                             FirstName = a.FirstName,
                                             LastName = a.LastName,
                                             Village = a.Village,
                                             HouseNumber = a.HouseNumber,
                                             Tumbon = a.Tumbon,
                                             Amphur = a.Amphur,
                                             Province = a.Province,
                                             BankAccount = a.BankAccount,
                                             BankBranch = a.BankBranch,
                                             BankBranchCode = a.BankBranchCode,
                                             BankName = a.BankName,
                                             QuotaRai = a.QuotaRai,
                                             QuotaKg = a.QuotaKg,
                                             SoldKg = a.SoldKg,
                                             ExtraSoldKg = b.ExtraSoldKg,
                                             ExtraQuotaStatus = b.ExtraBonusStatus,
                                             BonusAmount = b.BonusAmount,
                                             TransferredDate = b.TransferredDate,
                                             ReceiptOrder = b.ReceiptOrder
                                         })
                                         .ToList();

                    //var resultList = (from a in quotaList
                    //                  from b in soldList
                    //                  from c in contractList
                    //                  from d in bonusList
                    //                  where a.Crop == b.Crop
                    //                  && a.FarmerCode == b.FarmerCode
                    //                  && a.Crop == c.Crop
                    //                  && a.FarmerCode == c.FarmerCode
                    //                  && a.Crop == d.Crop
                    //                  && a.FarmerCode == d.FarmerCode
                    //                  select new m_ExtraBonusDetails
                    //                  {
                    //                      Crop = a.Crop,
                    //                      AreaCode = c.Farmer.Supplier1.SupplierArea,
                    //                      ExtensionAgentCode = c.Farmer.Supplier,
                    //                      CitizenID = c.Farmer.CitizenID,
                    //                      FarmerCode = a.FarmerCode,
                    //                      Prefix = c.Farmer.Person.Prefix,
                    //                      FirstName = c.Farmer.Person.FirstName,
                    //                      LastName = c.Farmer.Person.LastName,
                    //                      Village = c.Farmer.Person.Village,
                    //                      HouseNumber = c.Farmer.Person.HouseNumber,
                    //                      Tumbon = c.Farmer.Person.Tumbon,
                    //                      Amphur = c.Farmer.Person.Amphur,
                    //                      Province = c.Farmer.Person.Province,
                    //                      BankAccount = c.BookBank,
                    //                      BankBranch = c.BankBranch,
                    //                      BankBranchCode = c.BankBranchCode,
                    //                      BankName = c.Bank.BankName,
                    //                      QuotaRai = a.QuotaRai,
                    //                      QuotaKg = a.QuotaKg,
                    //                      SoldKg = (decimal)b.SoldKg,
                    //                      ExtraSoldKg = d.ExtraSoldKg,
                    //                      ExtraQuotaStatus = d.ExtraBonusStatus,
                    //                      BonusAmount = d.BonusAmount,
                    //                      TransferredDate = d.TransferredDate,
                    //                      ReceiptOrder = d.ReceiptOrder
                    //                  })
                    //                  .ToList();

                    var returnList = (from a in joinBonusList
                                      join b in debtBalanceList
                                      on a.FarmerCode equals b.FarmerCode
                                      into c
                                      from d in c.DefaultIfEmpty()
                                      select new m_ExtraBonusDetails
                                      {
                                          Crop = a.Crop,
                                          AreaCode = a.AreaCode,
                                          ExtensionAgentCode = a.ExtensionAgentCode,
                                          CitizenID = a.CitizenID,
                                          FarmerCode = a.FarmerCode,
                                          Prefix = a.Prefix,
                                          FirstName = a.FirstName,
                                          LastName = a.LastName,
                                          Village = a.Village,
                                          HouseNumber = a.HouseNumber,
                                          Tumbon = a.Tumbon,
                                          Amphur = a.Amphur,
                                          Province = a.Province,
                                          BankAccount = a.BankAccount,
                                          BankBranch = a.BankBranch,
                                          BankBranchCode = a.BankBranchCode,
                                          BankName = a.BankName,
                                          QuotaRai = a.QuotaRai,
                                          QuotaKg = a.QuotaKg,
                                          SoldKg = a.SoldKg,
                                          ExtraSoldKg = a.ExtraSoldKg,
                                          ExtraQuotaStatus = a.ExtraQuotaStatus,
                                          BonusAmount = a.BonusAmount,
                                          TransferredDate = a.TransferredDate,
                                          ReceiptOrder = a.ReceiptOrder,
                                          DebtAmount = d == null ? 0 : d.DebtAmount,
                                          DebtReceipt = d == null ? 0 : d.ReceiptAmount,
                                          DebtBalance = d == null ? 0 : d.DebtBalanceAmount,
                                      })
                                     .ToList();

                    return returnList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
