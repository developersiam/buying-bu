﻿using BusinessLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public static class CY202201ReturnDetailHelper
    {
        public static List<m_CY202201ReturnDetail> GetByReturnNo(string returnNo)
        {
            var agmItemList = AGMInventorySystemBL.BLServices.material_itemBL().GetAll();
            var returnList = (from a in BuyingFacade.CY202201ReturnDetailBL().GetByReturnNo(returnNo)
                              from b in agmItemList
                              where a.ReturnItemID == b.itemid
                              select new m_CY202201ReturnDetail
                              {
                                  ReturnDetailID = a.ReturnDetailID,
                                  ReturnNo = a.ReturnNo,
                                  ReturnItemID = a.ReturnItemID,
                                  ItemName = b.item_name,
                                  ItemType = a.CY202201ReturnItem.ItemType,
                                  Remark = a.CY202201ReturnItem.Remark,
                                  Quantity = a.Quantity,
                                  RecordDate = a.RecordDate,
                                  RecordUser = a.RecordUser,
                                  ModifiedDate = a.ModifiedDate,
                                  ModifiedUser = a.ModifiedUser
                              }).ToList();
            return returnList;
        }

        public static List<m_CY202201ReturnDetail> GetByFarmer(short crop, string farmerCode)
        {
            var agmItemList = AGMInventorySystemBL.BLServices.material_itemBL().GetAll();
            var returnList = (from a in BuyingFacade.CY202201ReturnDetailBL().GetByFarmer(crop, farmerCode)
                              from b in agmItemList
                              where a.ReturnItemID == b.itemid
                              select new m_CY202201ReturnDetail
                              {
                                  ReturnDetailID = a.ReturnDetailID,
                                  ReturnNo = a.ReturnNo,
                                  ReturnItemID = a.ReturnItemID,
                                  ItemName = b.item_name,
                                  ItemType = a.CY202201ReturnItem.ItemType,
                                  Remark = a.CY202201ReturnItem.Remark,
                                  Quantity = a.Quantity,
                                  RecordDate = a.RecordDate,
                                  RecordUser = a.RecordUser,
                                  ModifiedDate = a.ModifiedDate,
                                  ModifiedUser = a.ModifiedUser
                              }).ToList();
            return returnList;
        }

        public static List<m_CY202201ReturnDetail> GetSummaryByFarmer(short crop, string farmerCode)
        {
            var agmItemList = AGMInventorySystemBL.BLServices.material_itemBL().GetAll();
            var returnList = GetByFarmer(crop, farmerCode)
                .GroupBy(x => new { x.ReturnItemID, x.ItemType })
                .Select(x => new m_CY202201ReturnDetail
                {
                    ReturnItemID = x.Key.ReturnItemID,
                    ItemType = x.Key.ItemType,
                    Quantity = x.Sum(y => y.Quantity)
                })
                .ToList();

            return (from a in returnList
                    from b in agmItemList
                    where a.ReturnItemID == b.itemid
                    select new m_CY202201ReturnDetail
                    {
                        ReturnItemID = a.ReturnItemID,
                        ItemName = b.item_name,
                        ItemType = a.ItemType,
                        Quantity = a.Quantity
                    }).ToList();
        }
    }
}
