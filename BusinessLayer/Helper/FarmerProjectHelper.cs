﻿using BusinessLayer.Model;
using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public class FarmerProjectHelper
    {
        public static List<m_FarmerProject> GetQuotaAndSoldByFarmer(short crop, string farmerCode)
        {
            try
            {
                UnitOfWork uow = new UnitOfWork();

                var buyingList = uow.BuyingRepository
                    .Query(b => b.Crop == crop && b.FarmerCode == farmerCode).ToList();

                var soldInNormal = from b in buyingList
                                   where b.IsExtraQuota == false
                                   group b by new { b.Crop, b.FarmerCode, b.ProjectType } into g
                                   select new
                                   {
                                       g.Key.Crop,
                                       g.Key.FarmerCode,
                                       g.Key.ProjectType,
                                       WeightSold = g.Sum(b => b.Weight)
                                   };

                var soldInExtra = from b in buyingList
                                  where b.IsExtraQuota == true
                                  group b by new { b.Crop, b.FarmerCode, b.ProjectType } into g
                                  select new
                                  {
                                      g.Key.Crop,
                                      g.Key.FarmerCode,
                                      g.Key.ProjectType,
                                      WeightSold = g.Sum(b => b.Weight)
                                  };

                var regisModel = uow.RegistrationFarmerRepository.GetSingle(rg => rg.Crop == crop
                                        && rg.FarmerCode == farmerCode,
                                        rg => rg.Farmer);

                if (regisModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รหัส " + farmerCode + " ในปี " + crop);

                var kgPerRai = uow.KilogramsPerRaiConfigurationRepository
                    .GetSingle(k => k.Crop == regisModel.Crop &&
                    k.SupplierCode == regisModel.Farmer.Supplier);


                //4.ตรวจสอบการกำหนดกิโลกรัมต่อไร่ในแต่ละซัพพลายเออร์ (KilogramPerRaiConfiguration table)
                if (kgPerRai == null) throw new ArgumentException("ยังไม่มีการกำหนดจำนวนกิโลกรัมต่อไร่ โปรดแจ้งผู้ดูแลระบบเพื่อดำเนินการโดยด่วน");

                decimal extraPercentage = kgPerRai.ExtraPercentage == 0 ? 0 : Convert.ToDecimal(kgPerRai.ExtraPercentage) / Convert.ToDecimal(100);

                List<m_FarmerProject> farmerProjectAndSold = new List<m_FarmerProject>();

                //var inNormalQuota = (from fp in uow.FarmerProjectRepository.Get(fp => fp.Crop == crop && fp.FarmerCode == farmerCode).ToList()
                //                     join buy in soldInNormal on new { Crop = fp.Crop, FarmerCode = fp.FarmerCode, ProjectType = fp.ProjectType }
                //                     equals new { Crop = buy.Crop, FarmerCode = buy.FarmerCode, ProjectType = buy.ProjectType }
                //                     into resultJoin
                //                     from b in resultJoin.DefaultIfEmpty()
                //                     select new m_FarmerProject
                //                     {
                //                         Crop = fp.Crop,
                //                         FarmerCode = fp.FarmerCode,
                //                         ProjectType = fp.ProjectType,
                //                         ActualRai = fp.ActualRai,
                //                         ExtraQuota = fp.ExtraQuota,
                //                         ModifiedByUser = fp.ModifiedByUser,
                //                         LastModified = fp.LastModified,
                //                         ActualKgPerRai = (fp.ActualRai * kgPerRai.KilogramsPerRai) + ((fp.ActualRai * kgPerRai.KilogramsPerRai) * extraPercentage),
                //                         Sold = b == null ? 0 : b.WeightSold,
                //                         Balance = ((fp.ActualRai * kgPerRai.KilogramsPerRai) +
                //                                    ((fp.ActualRai * kgPerRai.KilogramsPerRai) * extraPercentage))
                //                                    - (b == null ? 0 : b.WeightSold),
                //                         BalanceTag = Convert.ToInt16(
                //                                            Math.Ceiling(
                //                                                Convert.ToDecimal(
                //                                                    (((fp.ActualRai * kgPerRai.KilogramsPerRai) +
                //                                                     ((fp.ActualRai * kgPerRai.KilogramsPerRai) *
                //                                                     extraPercentage)) - (b == null ? 0 : b.WeightSold)) / 45
                //                                                 )
                //                                             )
                //                                     )
                //                     }).ToList();

                //farmerProjectAndSold = (from nm in inNormalQuota
                //                        from b in soldInExtra.DefaultIfEmpty()
                //                        where b == null || (nm.Crop == b.Crop && nm.FarmerCode == b.FarmerCode)
                //                        select new m_FarmerProject
                //                        {
                //                            Crop = nm.Crop,
                //                            FarmerCode = nm.FarmerCode,
                //                            ProjectType = nm.ProjectType,
                //                            ActualRai = nm.ActualRai,
                //                            ExtraQuota = nm.ExtraQuota,
                //                            ActualKgPerRai = nm.ActualKgPerRai,
                //                            Sold = nm.Sold,
                //                            Balance = nm.Balance,
                //                            BalanceTag = nm.BalanceTag,
                //                            SoldInExtra = b == null ? 0 : b.WeightSold,
                //                            BalanceExtra = nm.ExtraQuota - (b == null ? 0 : b.WeightSold),
                //                            BalanceExtraTag = Convert.ToInt16((nm.ExtraQuota - (b == null ? 0 : b.WeightSold)) / Convert.ToInt16(45)),
                //                            ModifiedByUser = nm.ModifiedByUser,
                //                            LastModified = nm.LastModified
                //                        }).ToList();

                return farmerProjectAndSold;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<m_FarmerProject> GetQuotaAndSoldByFarmerVersion2(short crop, string farmerCode)
        {
            try
            {
                var regisModel = BuyingFacade.RegistrationBL()
                    .GetSingle(crop, farmerCode);
                if (regisModel == null)
                    throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รหัส " + farmerCode + " ในปี " + crop);

                var kgPerRai = BuyingFacade.KilogramsPerRaiConfigurationBL()
                    .GetSingle(crop, regisModel.Farmer.Supplier);
                if (kgPerRai == null)
                    throw new ArgumentException("ยังไม่มีการกำหนดจำนวนกิโลกรัมต่อไร่ โปรดแจ้งผู้ดูแลระบบ");

                var projectList = BuyingFacade.FarmerProjectBL()
                    .GetByFarmer(crop, farmerCode)
                    .Select(fp => new m_FarmerProject
                    {
                        Crop = fp.Crop,
                        FarmerCode = fp.FarmerCode,
                        ProjectType = fp.ProjectType,
                        ActualRai = fp.ActualRai,
                        ExtraQuota = fp.ExtraQuota,
                        ActualKgPerRai = (fp.ActualRai * kgPerRai.KilogramsPerRai),
                        TotalQuota = fp.ActualRai * kgPerRai.KilogramsPerRai + fp.ExtraQuota
                    }).ToList();


                var buyingList = BuyingFacade.BuyingBL().GetByFarmer(crop, farmerCode);
                var totalSold = buyingList.Sum(x => x.Weight);

                var soldInProjectList = buyingList.GroupBy(x => new { x.Crop, x.FarmerCode, x.ProjectType })
                    .Select(x => new m_FarmerProject
                    {
                        Crop = x.Key.Crop,
                        FarmerCode = x.Key.FarmerCode,
                        ProjectType = x.Key.ProjectType,
                        Sold = x.Where(y => y.IsExtraQuota == false).Sum(z => z.Weight),
                        SoldInExtra = x.Where(y => y.IsExtraQuota == true).Sum(z => z.Weight)
                    });

                var returnList = (from a in projectList
                                  join b in soldInProjectList
                                  on new { a.Crop, a.FarmerCode, a.ProjectType }
                                  equals new { b.Crop, b.FarmerCode, b.ProjectType }
                                  into ps
                                  from b in ps.DefaultIfEmpty()
                                  select new m_FarmerProject
                                  {
                                      Crop = a.Crop,
                                      FarmerCode = a.FarmerCode,
                                      ProjectType = a.ProjectType,
                                      ActualRai = a.ActualRai,
                                      ExtraQuota = a.ExtraQuota,
                                      ActualKgPerRai = a.ActualKgPerRai,
                                      TotalQuota = a.TotalQuota,
                                      Sold = b == null ? 0 : b.Sold,
                                      Balance = b == null ? a.ActualKgPerRai : a.ActualKgPerRai - b.Sold,
                                      BalanceTag = Convert.ToInt16(Math.Ceiling(b == null ? Convert.ToDecimal(a.ActualKgPerRai) / 45
                                      : Convert.ToDecimal(a.ActualKgPerRai - b.Sold) / 45)),
                                      SoldInExtra = b == null ? 0 : b.SoldInExtra,
                                      BalanceExtra = b == null ? a.ExtraQuota : a.ExtraQuota - b.SoldInExtra,
                                      BalanceExtraTag = Convert.ToInt16(Math.Ceiling(b == null ? Convert.ToDecimal(a.ExtraQuota) / 45
                                      : Convert.ToDecimal(a.ExtraQuota - b.SoldInExtra) / 45))
                                  }).ToList();

                return returnList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<m_FarmerProject> GetQuotaAndSoldBySupplierVersion2(short crop, string extensionAgentCode)
        {
            try
            {
                var kgPerRai = BuyingFacade.KilogramsPerRaiConfigurationBL()
                    .GetSingle(crop, extensionAgentCode);
                if (kgPerRai == null)
                    throw new ArgumentException("ยังไม่มีการกำหนดจำนวนกิโลกรัมต่อไร่ โปรดแจ้งผู้ดูแลระบบ");

                var projectList = BuyingFacade.FarmerProjectBL()
                    .GetByExtensionAgentCode(extensionAgentCode, crop)
                    .Select(fp => new m_FarmerProject
                    {
                        Crop = fp.Crop,
                        FarmerCode = fp.FarmerCode,
                        ProjectType = fp.ProjectType,
                        ActualRai = fp.ActualRai,
                        ExtraQuota = fp.ExtraQuota,
                        ActualKgPerRai = (fp.ActualRai * kgPerRai.KilogramsPerRai),
                        TotalQuota = fp.ActualRai * kgPerRai.KilogramsPerRai + fp.ExtraQuota
                    }).ToList();


                var buyingList = BuyingFacade.BuyingBL().GetByExtensionAgent(crop, extensionAgentCode);
                var totalSold = buyingList.Sum(x => x.Weight);

                var soldInProjectList = buyingList
                    .GroupBy(x => new { x.Crop, x.FarmerCode, x.ProjectType })
                    .Select(x => new m_FarmerProject
                    {
                        Crop = x.Key.Crop,
                        FarmerCode = x.Key.FarmerCode,
                        ProjectType = x.Key.ProjectType,
                        Sold = x.Where(y => y.IsExtraQuota == false).Sum(z => z.Weight),
                        SoldInExtra = x.Where(y => y.IsExtraQuota == true).Sum(z => z.Weight)
                    });

                var returnList = (from a in projectList
                                  join b in soldInProjectList
                                  on new { a.Crop, a.FarmerCode, a.ProjectType }
                                  equals new { b.Crop, b.FarmerCode, b.ProjectType }
                                  into ps
                                  from b in ps.DefaultIfEmpty()
                                  select new m_FarmerProject
                                  {
                                      Crop = a.Crop,
                                      FarmerCode = a.FarmerCode,
                                      ProjectType = a.ProjectType,
                                      ActualRai = a.ActualRai,
                                      ExtraQuota = a.ExtraQuota,
                                      ActualKgPerRai = a.ActualKgPerRai,
                                      TotalQuota = a.TotalQuota,
                                      Sold = b == null ? 0 : b.Sold,
                                      Balance = b == null ? a.ActualKgPerRai : a.ActualKgPerRai - b.Sold,
                                      BalanceTag = Convert.ToInt16(Math.Ceiling(b == null ? Convert.ToDecimal(a.ActualKgPerRai) / 45
                                      : Convert.ToDecimal(a.ActualKgPerRai - b.Sold) / 45)),
                                      SoldInExtra = b == null ? 0 : b.SoldInExtra,
                                      BalanceExtra = b == null ? a.ExtraQuota : a.ExtraQuota - b.SoldInExtra,
                                      BalanceExtraTag = Convert.ToInt16(Math.Ceiling(b == null ? Convert.ToDecimal(a.ExtraQuota) / 45
                                      : Convert.ToDecimal(a.ExtraQuota - b.SoldInExtra) / 45))
                                  }).ToList();

                return returnList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<m_FarmerProject> GetBalanceQuotaForRegisterBarcode(short crop, string farmerCode, string buyingStationCode, short documentNumber)
        {
            try
            {
                UnitOfWork uow = new UnitOfWork();

                var sold = from b in uow.BuyingRepository.Query(b => b.Crop == crop &&
                           b.FarmerCode == farmerCode)
                           .ToList()
                           group b by new { b.Crop, b.FarmerCode, b.ProjectType } into g
                           select new
                           {
                               g.Key.Crop,
                               g.Key.FarmerCode,
                               g.Key.ProjectType,
                               WeightSold = g.Sum(b => b.Weight)
                           };

                List<m_FarmerProject> farmerBalanceQuota = new List<m_FarmerProject>();

                farmerBalanceQuota = (from fp in uow.FarmerProjectRepository.Query(fp => fp.Crop == crop && fp.FarmerCode == farmerCode).ToList()
                                      from b in sold.DefaultIfEmpty()
                                      where b == null || (b.Crop == fp.Crop && b.FarmerCode == fp.FarmerCode)
                                      select new m_FarmerProject
                                      {
                                          Crop = fp.Crop,
                                          FarmerCode = fp.FarmerCode,
                                          ProjectType = fp.ProjectType,
                                          ActualRai = fp.ActualRai,
                                          ExtraQuota = fp.ExtraQuota,
                                          ModifiedByUser = fp.ModifiedByUser,
                                          LastModified = fp.LastModified,
                                          Sold = b == null ? 0 : b.WeightSold,
                                          Balance = (fp.ActualRai * 400) - (b == null ? 0 : b.WeightSold),
                                          BalanceTag = Convert.ToInt16(Math.Ceiling(Convert.ToDouble((fp.ActualRai * 400) - (b == null ? 0 : b.WeightSold))))
                                      }).ToList();

                return farmerBalanceQuota;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static m_FarmerProject GetQuotaAndSoldByProject(short crop, string farmerCode, string projectType)
        {
            try
            {
                UnitOfWork uow = new UnitOfWork();

                var _supplierCode = uow.FarmerRepository.GetSingle(x => x.FarmerCode == farmerCode).Supplier;

                var kgPerRai = uow.KilogramsPerRaiConfigurationRepository
                    .GetSingle(k => k.Crop == crop && k.SupplierCode == _supplierCode);

                //4.ตรวจสอบการกำหนดกิโลกรัมต่อไร่ในแต่ละซัพพลายเออร์ (KilogramPerRaiConfiguration table)
                if (kgPerRai == null)
                    throw new ArgumentException("ยังไม่มีการกำหนดจำนวนกิโลกรัมต่อไร่ โปรดแจ้งผู้ดูแลระบบเพื่อดำเนินการโดยด่วน");

                var _farmerProject = uow.FarmerProjectRepository
                    .GetSingle(x => x.Crop == crop &&
                    x.FarmerCode == farmerCode &&
                    x.ProjectType == projectType);

                var _sold = uow.BuyingRepository.Query(x => x.Crop == crop &&
                                     x.FarmerCode == farmerCode &&
                                     x.ProjectType == projectType)
                                     .ToList();

                return new m_FarmerProject
                {
                    Crop = crop,
                    FarmerCode = farmerCode,
                    ProjectType = projectType,
                    ActualRai = _farmerProject.ActualRai,
                    ExtraQuota = _farmerProject.ExtraQuota,
                    ActualKgPerRai = _farmerProject.ActualRai * kgPerRai.KilogramsPerRai + _farmerProject.ExtraQuota,
                    Sold = _sold.Sum(x => x.Weight),
                    Balance = (_farmerProject.ActualRai * kgPerRai.KilogramsPerRai + _farmerProject.ExtraQuota) - _sold.Sum(x => x.Weight),
                    BalanceTag = Convert.ToInt16((_farmerProject.ActualRai * kgPerRai.KilogramsPerRai + _farmerProject.ExtraQuota) - _sold.Sum(x => x.Weight) / 45)
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
