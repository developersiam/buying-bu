﻿using BusinessLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public static class InvoiceDetailsHelper
    {
        public static List<m_InvoiceDetails> GetByInvoiceNo(string invoiceNo)
        {
            var invoiceDetails = BuyingFacade.InvoiceDetailsBL().GetByInvoiceNo(invoiceNo);
            var items = AGMInventorySystemBL.BLServices.material_itemBL().GetAll();

            var result = (from a in invoiceDetails
                          from b in items
                          where a.CropInputsCondition.ItemID == b.itemid
                          select new m_InvoiceDetails
                          {
                              InvoiceNo = a.InvoiceNo,
                              ConditionID = a.ConditionID,
                              ItemID = a.CropInputsCondition.ItemID,
                              ItemName = b.item_name,
                              Quantity = a.Quantity,
                              UnitPrice = a.UnitPrice,
                              IsCash = a.IsCash,
                              RecordDate = a.RecordDate,
                              RecordUser = a.RecordUser,
                              ModifiedDate = a.ModifiedDate,
                              ModifiedUser = a.ModifiedUser,
                              PackageCode = a.PackageCode,
                              AccountInvoiceNo = a.AccountInvoiceNo,
                              LotNo = b.lotno,
                              Unit = b.material_unit.unit_name,
                              Brand = b.material_brand.brand_name,
                              Category = b.material_category.category_name,
                              CropInputsCondition = a.CropInputsCondition
                          }).ToList();

            return result;
        }

        public static List<m_InvoiceDetails> GetByFarmer(short crop, string farmerCode)
        {
            var invoiceDetails = BuyingFacade.InvoiceDetailsBL().GetByFarmer(crop, farmerCode);
            var items = AGMInventorySystemBL.BLServices.material_itemBL().GetAll();

            var result = (from a in invoiceDetails
                          from b in items
                          where a.CropInputsCondition.ItemID == b.itemid
                          select new m_InvoiceDetails
                          {
                              InvoiceNo = a.InvoiceNo,
                              ConditionID = a.ConditionID,
                              ItemID = a.CropInputsCondition.ItemID,
                              ItemName = b.item_name,
                              Quantity = a.Quantity,
                              UnitPrice = a.UnitPrice,
                              IsCash = a.IsCash,
                              RecordDate = a.RecordDate,
                              RecordUser = a.RecordUser,
                              ModifiedDate = a.ModifiedDate,
                              ModifiedUser = a.ModifiedUser,
                              PackageCode = a.PackageCode,
                              AccountInvoiceNo = a.AccountInvoiceNo,
                              LotNo = b.lotno,
                              Unit = b.material_unit.unit_name,
                              Brand = b.material_brand.brand_name,
                              Category = b.material_category.category_name,
                              CropInputsCondition = a.CropInputsCondition
                          }).ToList();

            return result;
        }

        public static List<m_InvoiceDetails> GetByAccountInvoiceNo(string accountInvoiceNo)
        {
            var invoiceDetails = BuyingFacade.InvoiceDetailsBL().GetByAccountInvoiceNo(accountInvoiceNo);
            var items = AGMInventorySystemBL.BLServices.material_itemBL().GetAll();
            var accountInvoice = BuyingFacade.AccountInvoiceBL().GetSingle(accountInvoiceNo);
            if (accountInvoice == null)
                throw new ArgumentException("Account invoice cannot be null.");

            var result = (from a in invoiceDetails
                          from b in items
                          where a.CropInputsCondition.ItemID == b.itemid
                          select new m_InvoiceDetails
                          {
                              InvoiceNo = a.InvoiceNo,
                              ConditionID = a.ConditionID,
                              ItemID = a.CropInputsCondition.ItemID,
                              ItemName = b.item_name,
                              Quantity = a.Quantity,
                              UnitPrice = a.UnitPrice,
                              IsCash = a.IsCash,
                              RecordDate = a.RecordDate,
                              RecordUser = a.RecordUser,
                              ModifiedDate = a.ModifiedDate,
                              ModifiedUser = a.ModifiedUser,
                              PackageCode = a.PackageCode,
                              AccountInvoiceNo = a.AccountInvoiceNo,
                              LotNo = b.lotno,
                              Unit = b.material_unit.unit_name,
                              Brand = b.material_brand.brand_name,
                              Category = b.material_category.category_name,
                              CropInputsCondition = a.CropInputsCondition,
                              Price = a.Quantity * a.UnitPrice
                          }).ToList();

            return result;
        }

        public static List<m_InvoiceDetails> GetSummaryByFarmer(short crop, string farmerCode)
        {
            return GetByFarmer(crop, farmerCode)
                .GroupBy(x => new { x.ItemID, x.ItemName })
                .Select(x => new m_InvoiceDetails
                {
                    ItemID = x.Key.ItemID,
                    ItemName = x.Key.ItemName,
                    Quantity = x.Sum(y => y.Quantity)
                })
                .ToList();
        }
    }
}
