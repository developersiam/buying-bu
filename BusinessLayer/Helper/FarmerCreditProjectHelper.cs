﻿using BusinessLayer.Model;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public class FarmerCreditProjectHelper
    {
        public static List<m_FarmerCreditProject> GetByFarmer(short crop, string farmerCode)
        {
            try
            {
                UnitOfWork uow = new UnitOfWork();

                return uow.FarmerCreditProjectRepository
                    .Query(x => x.Crop == crop &&
                    x.FarmerCode == farmerCode)
                    .Select(x => new m_FarmerCreditProject
                    {
                        Crop = x.Crop,
                        FarmerCode = x.FarmerCode,
                        Rai = x.Rai,
                        IsPayment = x.IsPayment,
                        CreditProjectName = x.CreditProject.CreditProjectName
                    }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
