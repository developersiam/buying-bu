﻿using BusinessLayer.Model;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public static class AccountInvoiceHelper
    {
        //////////public static List<m_AccountInvoiceWithCreditNote> GetAccountInvoiceSummaryWithCreditNote(short crop, string extensionAgentCode)
        //////////{
        //////////    var returnList = new List<m_AccountInvoiceWithCreditNote>();
        //////////    var creditNoteList = CreditNoteDetailHelper.GetByExtensionAgent(crop, extensionAgentCode);
        //////////    var accountInvoiceList = BuyingFacade.AccountInvoiceBL()
        //////////        .GetByExtensionAgent(crop, extensionAgentCode);

        //////////    foreach (var item in accountInvoiceList)
        //////////    {
        //////////        returnList
        //////////            .Add(new m_AccountInvoiceWithCreditNote
        //////////            {
        //////////                FarmerCode = item.Invoice.FarmerCode,
        //////////                AccountInvoiceNo = item.InvoiceNo,
        //////////                AccountInvoiceAmount = item.InvoiceDetails
        //////////                .Sum(x => x.UnitPrice * x.Quantity),
        //////////                DebtTypeCode = item.Invoice.DebtTypeCode,
        //////////                DebtTypeName = item.Invoice.DebtType.DebtTypeName,
        //////////                CreditNoteAmount = accountInvoiceList.Count(x => x.AccountCreditNotes) <= 0 ? 0 : creditNoteList
        //////////                .Where(x => x.AccountCreditNote.AccountInvoiceNo == item.InvoiceNo)
        //////////                .Sum(z => z.Quantity * z.UnitPrice),
        //////////                BalanceAmount = item.InvoiceDetails
        //////////                .Sum(x => x.UnitPrice * x.Quantity) -
        //////////                creditNoteList
        //////////                .Where(x => x.AccountCreditNote.AccountInvoiceNo == item.InvoiceNo)
        //////////                .Sum(z => z.Quantity * z.UnitPrice),
        //////////                CreditNotes = creditNoteList
        //////////                .Where(x => x.AccountCreditNote.AccountInvoiceNo == item.InvoiceNo)
        //////////                .ToList()
        //////////            });
        //////////    }
        //////////    return returnList;
        //////////}

        public static List<m_AccountInvoiceWithCreditNote> GetAccountInvoiceSummaryWithCreditNoteByFarmer(short crop, string farmerCode)
        {
            var returnList = new List<m_AccountInvoiceWithCreditNote>();
            var creditNoteList = CreditNoteDetailHelper
                .GetByFarmer(crop, farmerCode);
            var accountInvoiceList = BuyingFacade.AccountInvoiceBL()
                .GetByFarmer(crop, farmerCode);
            var accountInvoiceDetailsList = AccountInvoiceHelper
                .GetAccountInvoiceDetailsByFarmer(crop, farmerCode);

            foreach (var item in accountInvoiceList)
            {
                returnList
                    .Add(new m_AccountInvoiceWithCreditNote
                    {
                        FarmerCode = item.Invoice.FarmerCode,
                        DebtTypeCode = item.Invoice.DebtTypeCode,
                        DebtTypeName = item.Invoice.DebtType.DebtTypeName,
                        AccountInvoiceNo = item.InvoiceNo,
                        AccountInvoiceAmount = item.InvoiceDetails
                        .Sum(x => x.UnitPrice * x.Quantity),
                        CreditNoteAmount = creditNoteList
                        .Where(x => x.AccountCreditNote.AccountInvoiceNo == item.InvoiceNo)
                        .Sum(z => z.Quantity * z.UnitPrice),
                        BalanceAmount = item.InvoiceDetails
                        .Sum(x => x.UnitPrice * x.Quantity) -
                        creditNoteList
                        .Where(x => x.AccountCreditNote.AccountInvoiceNo == item.InvoiceNo)
                        .Sum(z => z.Quantity * z.UnitPrice),
                        CreditNotes = creditNoteList
                        .Where(x => x.AccountCreditNote.AccountInvoiceNo == item.InvoiceNo)
                        .ToList(),
                        AccountInvoiceDetails = accountInvoiceDetailsList
                        .Where(x => x.AccountInvoiceNo == item.InvoiceNo)
                        .ToList(),
                    });
            }
            return returnList;
        }

        public static List<m_AccountInvoiceSummary> GetByFarmer(short crop, string farmerCode)
        {
            var invoiceList = BuyingFacade.AccountInvoiceBL().GetByFarmer(crop, farmerCode);
            var resultList = invoiceList
                .GroupBy(x => new { x.InvoiceNo, x.PrintDate })
                .Select(x => new m_AccountInvoiceSummary
                {
                    InvoiceNo = x.Key.InvoiceNo,
                    PrintDate = (DateTime)x.Key.PrintDate,
                    Price = (double)x.Sum(y => y.InvoiceDetails.Sum(z => z.Quantity * z.UnitPrice)),
                    Items = x.Sum(y => y.InvoiceDetails.Count())
                })
                .ToList();
            return resultList;
        }

        public static List<m_InvoiceDetails> GetAccountInvoiceDetailsByFarmer(short crop, string farmerCode)
        {
            var accountInvoiceList = BuyingFacade.AccountInvoiceBL().GetByFarmer(crop, farmerCode);
            var invoiceDetailsList = BuyingFacade.InvoiceDetailsBL().GetByFarmer(crop, farmerCode);
            var join1 = from a in accountInvoiceList
                        from i in invoiceDetailsList
                        where a.InvoiceNo == i.AccountInvoiceNo
                        select new InvoiceDetail
                        {
                            AccountInvoiceNo = a.InvoiceNo,
                            InvoiceNo = a.RefInvoiceNo,
                            ConditionID = i.ConditionID,
                            CropInputsCondition = i.CropInputsCondition,
                            UnitPrice = i.UnitPrice,
                            Quantity = i.Quantity,
                            AccountInvoice = a,
                            Invoice = i.Invoice,
                            IsCash = i.IsCash
                        };
            var itemList = AGMInventorySystemBL.BLServices.material_itemBL().GetAll();
            var join2 = (from a in join1
                         from b in itemList
                         where a.CropInputsCondition.ItemID == b.itemid
                         select new m_InvoiceDetails
                         {
                             InvoiceNo = a.InvoiceNo,
                             ConditionID = a.ConditionID,
                             ItemID = a.CropInputsCondition.ItemID,
                             ItemName = b.item_name,
                             Quantity = a.Quantity,
                             UnitPrice = a.UnitPrice,
                             IsCash = a.IsCash,
                             RecordDate = a.RecordDate,
                             RecordUser = a.RecordUser,
                             ModifiedDate = a.ModifiedDate,
                             ModifiedUser = a.ModifiedUser,
                             PackageCode = a.PackageCode,
                             AccountInvoiceNo = a.AccountInvoiceNo,
                             LotNo = b.lotno,
                             Unit = b.material_unit.unit_name,
                             Brand = b.material_brand.brand_name,
                             Category = b.material_category.category_name,
                             CropInputsCondition = a.CropInputsCondition,
                             Invoice = a.Invoice
                         })
                        .ToList();

            return join2;
        }

        public static List<m_InvoiceDetails> GetAccountInvoiceDetailsByInvoice(string accountInvoiceNo)
        {
            var accountInvoice = BuyingFacade.AccountInvoiceBL().
                GetSingle(accountInvoiceNo);
            if (accountInvoice == null)
                throw new ArgumentException("ไม่พบ account invoice no " + accountInvoiceNo + " นี้ในระบบ");

            var invoiceDetailsList = BuyingFacade.InvoiceDetailsBL().GetByAccountInvoiceNo(accountInvoiceNo);
            var itemList = AGMInventorySystemBL.BLServices.material_itemBL().GetAll();
            var join = (from a in invoiceDetailsList
                        from b in itemList
                        where a.CropInputsCondition.ItemID == b.itemid
                        select new m_InvoiceDetails
                        {
                            InvoiceNo = a.InvoiceNo,
                            ConditionID = a.ConditionID,
                            ItemID = a.CropInputsCondition.ItemID,
                            ItemName = b.item_name,
                            Quantity = a.Quantity,
                            UnitPrice = a.UnitPrice,
                            IsCash = a.IsCash,
                            RecordDate = a.RecordDate,
                            RecordUser = a.RecordUser,
                            ModifiedDate = a.ModifiedDate,
                            ModifiedUser = a.ModifiedUser,
                            PackageCode = a.PackageCode,
                            AccountInvoiceNo = a.AccountInvoiceNo,
                            LotNo = b.lotno,
                            Unit = b.material_unit.unit_name,
                            Brand = b.material_brand.brand_name,
                            Category = b.material_category.category_name,
                            CropInputsCondition = a.CropInputsCondition
                        })
                        .ToList();

            return join;
        }
    }
}
