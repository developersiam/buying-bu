﻿using BusinessLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public class CY202201CreditNoteHelper
    {
        public static string MonthText(int month)
        {
            var result = "";
            switch (month)
            {
                case 1:
                    result = " มกราคม ";
                    break;
                case 2:
                    result = " กุมภาพันธุ์ ";
                    break;
                case 3:
                    result = " มีนาคม ";
                    break;
                case 4:
                    result = " เมษายน ";
                    break;
                case 5:
                    result = " พฤษภาคม ";
                    break;
                case 6:
                    result = " มิถุนายน ";
                    break;
                case 7:
                    result = " กรกฎาคม ";
                    break;
                case 8:
                    result = " สิงหาคม ";
                    break;
                case 9:
                    result = " กันยายน ";
                    break;
                case 10:
                    result = " ตุลาคม ";
                    break;
                case 11:
                    result = " พฤศจิกายน ";
                    break;
                case 12:
                    result = " ธันวาคม ";
                    break;
                default:
                    break;
            }

            return result;
        }

        public static List<m_CY202201CreditNoteDetail> GetDetail(short crop, string farmerCode)
        {
            var receiveList = InvoiceDetailsHelper.GetSummaryByFarmer(crop, farmerCode);
            var returnItemList = BuyingFacade.CY202201ReturnItemBL().GetAll();
            var result = (from a in receiveList
                          from b in returnItemList
                          where a.ItemID == b.ReturnItemID && b.ItemType == "ตะกร้า"
                          select new m_CY202201CreditNoteDetail
                          {
                              ItemName = "ตะกร้าพลาสติก " + a.ItemName,
                              Quantity = a.Quantity,
                              UnitPrice = 10,
                              Price = a.Quantity * 10,
                              RefInvoiceNo = a.AccountInvoiceNo
                          })
                          .ToList();

            return result;
        }

        public static List<m_CY202201CreditNoteHeader> GetHeader(string creditNoteCode)
        {
            var creditNote = BuyingFacade.CY202201CreditNoteCodeBL().GetSingle(creditNoteCode);
            var person = BuyingFacade.FarmerBL().GetByFarmerCode(creditNote.FarmerCode).Person;
            var model = new m_CY202201CreditNoteHeader();
            var receiveList = GetDetail(creditNote.Crop, creditNote.FarmerCode);

            model.CustomerName = person.Prefix + person.FirstName + " " + person.LastName;
            model.CustomerAddress = person.HouseNumber + " หมู่ " + person.Village + " ตำบล" + person.Tumbon + " อำเภอ" + person.Amphur;
            model.CustomerAddress1 = " จังหวัด" + person.Province;
            model.TaxID = person.CitizenID;
            model.CreditNoteCode = "เลขที่ " + creditNoteCode;
            model.Description = creditNote.Description;
            model.ThaiBathText = BuyingFacade.StoreProcetureBL()
                .sp_ConvertCurrencyToThaiBath(receiveList
                .Sum(x => x.Quantity * x.UnitPrice));
            model.CreateDate = "วันที่ " + creditNote.CreateDate.Day + " " +
                MonthText(creditNote.CreateDate.Month) + " " +
                (creditNote.CreateDate.Year + 543);

            var refInvoiceNoList = "";
            foreach (var item in receiveList)
            {
                refInvoiceNoList = refInvoiceNoList + item.RefInvoiceNo + ", ";
            }
            model.ReferenceAccountInvoice = "อ้างถึง ใบส่งปัจจัยการผลิต เลขที่ " + refInvoiceNoList;

            var resultList = new List<m_CY202201CreditNoteHeader>();
            resultList.Add(model);
            return resultList;
        }
    }
}
