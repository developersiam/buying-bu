﻿using BusinessLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public static class MaterialIssuedItemHelper
    {
        public static List<m_MaterialIssuedDetails> GetByIssuedCode(string issuedCode)
        {
            var issuedDetailsList = BuyingFacade.MaterialIssuedDetailsBL().GetByIssuedCode(issuedCode);
            var materialItemList = AGMInventorySystemBL.BLServices.material_itemBL().GetAll();

            var result = (from a in issuedDetailsList
                          from b in materialItemList
                          where a.ItemID == b.itemid
                          select new m_MaterialIssuedDetails
                          {
                              IssuedCode = a.IssuedCode,
                              ItemID = a.ItemID,
                              Quantity = a.Quantity,
                              UnitPrice = a.UnitPrice,
                              RecordDate = a.RecordDate,
                              RecordUser = a.RecordUser,
                              ModifiedDate = a.ModifiedDate,
                              ModifiedUser = a.ModifiedUser,
                              ItemName = b.item_name,
                              LotNo = b.lotno,
                              Unit = b.material_unit.unit_name,
                              Brand = b.material_brand.brand_name,
                              Category = b.material_category.category_name
                          }).ToList();

            return result;
        }

        public static m_MaterialIssuedDetails GetSingle(string issuedCode, int itemID)
        {
            var a = BuyingFacade.MaterialIssuedDetailsBL().GetSingle(issuedCode, itemID);
            var b = AGMInventorySystemBL.BLServices.material_itemBL().GetSingle(itemID);

            if (a == null)
                return new m_MaterialIssuedDetails();

            var result = new m_MaterialIssuedDetails
            {
                IssuedCode = a.IssuedCode,
                ItemID = a.ItemID,
                Quantity = a.Quantity,
                UnitPrice = a.UnitPrice,
                RecordDate = a.RecordDate,
                RecordUser = a.RecordUser,
                ModifiedDate = a.ModifiedDate,
                ModifiedUser = a.ModifiedUser,
                ItemName = b.item_name,
                LotNo = b.lotno,
                Unit = b.material_unit.unit_name,
                Brand = b.material_brand.brand_name,
                Category = b.material_category.category_name
            };
            return result;
        }
    }
}
