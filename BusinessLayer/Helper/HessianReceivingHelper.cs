﻿using BusinessLayer.Model;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public static class HessianReceivingHelper
    {
        public static List<m_HessianReceiving> GetByWarehouse(short crop, Guid warehouseID)
        {
            return BuyingFacade.HessianReceivingBL()
                .GetByWarehouse(crop, warehouseID)
                .Select(x=>new m_HessianReceiving {
                    ReceivingCode = x.ReceivingCode,
                    ReceivedBy = x.ReceivedBy,
                    ReceivedDate = x.ReceivedDate,
                    Crop = x.Crop,
                    WarehouseID = x.WarehouseID,
                    TruckNumber = x.TruckNumber,
                    Quantity = x.Quantity,
                    ModifiedBy = x.ModifiedBy,
                    ModifiedDate = x.ModifiedDate,
                    Issued = x.HessianDistributions.Sum(y=>y.Quantity),
                    OnHand = x.Quantity - x.HessianDistributions.Sum(y => y.Quantity)
                })
                .ToList();
        }
    }
}
