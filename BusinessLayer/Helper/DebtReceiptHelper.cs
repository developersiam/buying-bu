﻿using BusinessLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public static class DebtReceiptHelper
    {
        public static List<m_DebtReceipt> GetByDebtSetupCode(string debtSetupCode)
        {
            var list = BuyingFacade.DebtReceiptBL()
                 .GetByDebtSetupCode(debtSetupCode)
                 .Select(x => new m_DebtReceipt
                 {
                     RefCode = x.Crop + "-" +
                     x.BuyingStationCode + "-" +
                     x.FarmerCode + "-" +
                     x.BuyingDocumentNumber,
                     ReceiptCode = x.ReceiptCode,
                     Amount = x.Amount,
                     ReceiptTypeCode = x.ReceiptTypeCode,
                     ReceiptDate = x.ReceiptDate,
                     ReceiptBy = x.ReceiptBy,
                     DebtSetupCode = x.DebtSetupCode,
                     DebtSetup = x.DebtSetup
                 })
                 .ToList();

            return list;
        }

        public static List<m_DebtReceipt> GetByBuyingDocumentCode(short crop,
            string stationCode,
            string farmerCode,
            short documentNumber)
        {
            var list = BuyingFacade.DebtReceiptBL()
                 .GetByBuyingDocument(crop, stationCode, farmerCode, documentNumber)
                 .Select(x => new m_DebtReceipt
                 {
                     Crop = x.Crop,
                     BuyingStationCode = x.BuyingStationCode,
                     FarmerCode = x.FarmerCode,
                     BuyingDocumentNumber = x.BuyingDocumentNumber,
                     RefCode = x.BuyingDocument.Crop + "-" +
                     x.BuyingDocument.BuyingStationCode + "-" +
                     x.BuyingDocument.FarmerCode + "-" +
                     x.BuyingDocument.BuyingDocumentNumber,
                     ReceiptCode = x.ReceiptCode,
                     Amount = x.Amount,
                     ReceiptTypeCode = x.ReceiptTypeCode,
                     ReceiptDate = x.ReceiptDate,
                     ReceiptBy = x.ReceiptBy,
                     DebtSetupCode = x.DebtSetupCode,
                     DebtSetup = x.DebtSetup
                 })
                 .ToList();

            return list;
        }
    }
}
