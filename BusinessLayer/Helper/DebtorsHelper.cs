﻿using BusinessLayer.Model;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public static class DebtorsHelper
    {
        public static decimal GetTotalDebt(short crop, string farmerCode)
        {
            using (var uow = new UnitOfWork())
            {
                return GetDebtByFarmer(crop, farmerCode).Sum(x => x.Price);
            }
        }

        public static decimal GetTotalPayment(short crop, string farmerCode)
        {
            using (var uow = new UnitOfWork())
            {
                return uow.PaymentHistoryRepository
                    .Get(x => x.Crop == crop && x.FarmerCode == farmerCode)
                    .Sum(x => x.PaymentAmount);
            }
        }

        public static List<m_AccountInvoiceDetails> GetDebtByFarmer(short crop, string farmerCode)
        {
            using (var uow = new UnitOfWork())
            {
                return uow.InvoiceDetailRepository
                    .Get(x => x.Invoice.Crop == crop &&
                    x.Invoice.FarmerCode == farmerCode &&
                    (x.AccountInvoice.InvoiceNo.Contains("IN") || x.AccountInvoice.InvoiceNo.Contains("IV")),
                    null,
                    x => x.Invoice,
                    x => x.AccountInvoice)
                    .GroupBy(x => new
                    {
                        x.InvoiceNo,
                        x.AccountInvoiceNo,
                        x.AccountInvoice.PrintDate,
                        x.AccountInvoice.CreateDate,
                    })
                    .Select(y => new m_AccountInvoiceDetails
                    {
                        InvoiceNo = y.Key.InvoiceNo,
                        AccountInvoiceNo = y.Key.AccountInvoiceNo,
                        PrintDate = Convert.ToDateTime(y.Key.PrintDate),
                        CreateDate = Convert.ToDateTime(y.Key.CreateDate),
                        Price = y.Sum(z => z.Quantity * z.UnitPrice)
                    })
                    .ToList();
            }
        }

        public static List<m_PaymentHistory> GetPaymentByFarmer(short crop, string farmerCode)
        {
            using (var uow = new UnitOfWork())
            {
                var history = uow.PaymentHistoryRepository
                .Get(x => x.Crop == crop &&
                x.FarmerCode == farmerCode)
                .ToList();
                var document = BuyingDocumentHelper.GetByFarmer(crop, farmerCode);

                var returnList = (from a in history
                                  from b in document
                                  where a.Crop == b.Crop &&
                                  a.BuyingStationCode == b.BuyingStationCode &&
                                  a.FarmerCode == b.FarmerCode &&
                                  a.BuyingDocumentNumber == b.BuyingDocumentNumber
                                  select new m_PaymentHistory
                                  {
                                      BuyingDocumentCode = a.Crop + "-" +
                                      a.BuyingStationCode + "-" +
                                      a.FarmerCode + "-" +
                                      a.BuyingDocumentNumber,
                                      BuyingDate = Convert.ToDateTime(b.BuyingDate),
                                      PaymentAmount = a.PaymentAmount,
                                      PaymentDate = a.PaymentDate,
                                      PaymentPercentage = a.PaymentPercentage,
                                      IsFinish = a.IsFinish,
                                      FinishAmount = a.FinishAmount,
                                      FinishPercentage = a.FinishPercentage,
                                      FinishDate = Convert.ToDateTime(a.FinishDate)
                                  })
                                 .ToList();

                return returnList;
            }
        }
    }
}
