﻿using BusinessLayer.Model;
using DomainModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public class CreditNoteDetailHelper
    {
        public static string MonthText(int month)
        {
            var result = "";
            switch (month)
            {
                case 1:
                    result = " มกราคม ";
                    break;
                case 2:
                    result = " กุมภาพันธุ์ ";
                    break;
                case 3:
                    result = " มีนาคม ";
                    break;
                case 4:
                    result = " เมษายน ";
                    break;
                case 5:
                    result = " พฤษภาคม ";
                    break;
                case 6:
                    result = " มิถุนายน ";
                    break;
                case 7:
                    result = " กรกฎาคม ";
                    break;
                case 8:
                    result = " สิงหาคม ";
                    break;
                case 9:
                    result = " กันยายน ";
                    break;
                case 10:
                    result = " ตุลาคม ";
                    break;
                case 11:
                    result = " พฤศจิกายน ";
                    break;
                case 12:
                    result = " ธันวาคม ";
                    break;
                default:
                    break;
            }

            return result;
        }

        public static List<m_CreditNoteDetail> GetByCreditNoteCode(string creditNoteCode)
        {

            var creditNote = BuyingFacade.AccountCreditNoteBL().GetSingle(creditNoteCode);
            var list = BuyingFacade.AccountCreditNoteDetailBL().GetByCreditNoteCode(creditNoteCode);
            var list1 = BuyingFacade.InvoiceDetailsBL().GetByAccountInvoiceNo(creditNote.AccountInvoiceNo);
            var list2 = (from a in list
                         from b in list1
                         where a.ConditionID == b.ConditionID
                         select new m_CreditNoteDetail
                         {
                             ConditionID = a.ConditionID,
                             ItemID = b.CropInputsCondition.ItemID,
                             Quantity = a.Quantity,
                             UnitPrice = a.UnitPrice
                         })
                         .ToList();
            var items = AGMInventorySystemBL.BLServices.material_itemBL().GetAll();
            var result = (from a in list2
                          from b in items
                          where a.ItemID == b.itemid
                          select new m_CreditNoteDetail
                          {
                              CreditNoteCode = a.CreditNoteCode,
                              ConditionID = a.ConditionID,
                              ItemID = a.ItemID,
                              ItemName = b.item_name,
                              Quantity = a.Quantity,
                              UnitPrice = a.UnitPrice,
                              RecordDate = a.RecordDate,
                              RecordUser = a.RecordUser,
                              ModifiedDate = a.ModifiedDate,
                              ModifiedUser = a.ModifiedUser,
                              LotNo = b.lotno,
                              Unit = b.material_unit.unit_name,
                              Brand = b.material_brand.brand_name,
                              Category = b.material_category.category_name,
                              Price = a.UnitPrice * a.Quantity
                          }).ToList();

            return result;
        }

        public static List<m_CreditNoteDetail> GetByAccountInvoice(string accountInvoiceNo)
        {
            var list = BuyingFacade.AccountCreditNoteDetailBL()
                .GetByAccountInvoiceNo(accountInvoiceNo)
                .GroupBy(x => new
                {
                    ConditionID = x.ConditionID,
                    ItemID = x.CropInputsCondition.ItemID,
                    UnitPrice = x.UnitPrice
                })
                .Select(x => new m_CreditNoteDetail
                {
                    ConditionID = x.Key.ConditionID,
                    ItemID = x.Key.ItemID,
                    UnitPrice = x.Key.UnitPrice,
                    ReturnQuantity = x.Sum(y => y.Quantity),
                    ReturnPrice = x.Sum(y => y.Quantity * y.UnitPrice)
                }).ToList();

            var list1 = BuyingFacade.InvoiceDetailsBL().GetByAccountInvoiceNo(accountInvoiceNo);
            var list2 = (from a in list
                         from b in list1
                         where a.ConditionID == b.ConditionID
                         select new m_CreditNoteDetail
                         {
                             ConditionID = a.ConditionID,
                             ItemID = b.CropInputsCondition.ItemID,
                             Quantity = b.Quantity,
                             UnitPrice = b.UnitPrice,
                             CreditNoteCode = a.CreditNoteCode,
                             AccountCreditNote = a.AccountCreditNote,
                             ReturnQuantity = a.ReturnQuantity,
                             ReturnPrice = a.ReturnPrice
                         })
                         .ToList();

            var items = AGMInventorySystemBL.BLServices.material_itemBL().GetAll();
            var result = (from a in list2
                          from b in items
                          where a.ItemID == b.itemid
                          select new m_CreditNoteDetail
                          {
                              CreditNoteCode = a.CreditNoteCode,
                              AccountCreditNote = a.AccountCreditNote,
                              ConditionID = a.ConditionID,
                              ItemID = a.ItemID,
                              ItemName = b.item_name,
                              Quantity = a.Quantity,
                              UnitPrice = a.UnitPrice,
                              ReturnQuantity = a.ReturnQuantity,
                              ReturnPrice = a.ReturnPrice,
                              LotNo = b.lotno,
                              Unit = b.material_unit.unit_name,
                              Brand = b.material_brand.brand_name,
                              Category = b.material_category.category_name,
                              Price = a.UnitPrice * a.Quantity
                          }).ToList();

            return result;
        }

        public static List<m_CreditNoteDetail> GetByFarmer(short crop, string farmerCode)
        {
            var list = BuyingFacade.AccountCreditNoteDetailBL()
                .GetByFarmerCode(crop, farmerCode)
                .GroupBy(x => new
                {
                    x.CreditNoteCode,
                    x.ConditionID,
                    x.CropInputsCondition.ItemID,
                    x.UnitPrice
                })
                .Select(x => new m_CreditNoteDetail
                {
                    CreditNoteCode = x.Key.CreditNoteCode,
                    ConditionID = x.Key.ConditionID,
                    ItemID = x.Key.ItemID,
                    UnitPrice = x.Key.UnitPrice,
                    ReturnQuantity = x.Sum(y => y.Quantity),
                    ReturnPrice = x.Sum(y => y.Quantity * y.UnitPrice)
                }).ToList();

            var list1 = BuyingFacade.InvoiceDetailsBL().GetByFarmer(crop, farmerCode);
            var list2 = (from a in list
                         from b in list1
                         where a.ConditionID == b.ConditionID
                         select new m_CreditNoteDetail
                         {
                             ConditionID = a.ConditionID,
                             ItemID = b.CropInputsCondition.ItemID,
                             Quantity = b.Quantity,
                             UnitPrice = b.UnitPrice,
                             CreditNoteCode = a.CreditNoteCode,
                             AccountCreditNote = a.AccountCreditNote,
                             ReturnQuantity = a.ReturnQuantity,
                             ReturnPrice = a.ReturnPrice
                         })
                         .ToList();

            var items = AGMInventorySystemBL.BLServices.material_itemBL().GetAll();
            var result = (from a in list2
                          from b in items
                          where a.ItemID == b.itemid
                          select new m_CreditNoteDetail
                          {
                              CreditNoteCode = a.CreditNoteCode,
                              AccountCreditNote = a.AccountCreditNote,
                              ConditionID = a.ConditionID,
                              ItemID = a.ItemID,
                              ItemName = b.item_name,
                              Quantity = a.Quantity,
                              UnitPrice = a.UnitPrice,
                              ReturnQuantity = a.ReturnQuantity,
                              ReturnPrice = a.ReturnPrice,
                              LotNo = b.lotno,
                              Unit = b.material_unit.unit_name,
                              Brand = b.material_brand.brand_name,
                              Category = b.material_category.category_name,
                              Price = a.UnitPrice * a.Quantity
                          }).ToList();

            return result;
        }

        public static List<m_CreditNoteDetail> GetByFarmerCode(short crop, string farmerCode)
        {
            var items = AGMInventorySystemBL.BLServices.material_itemBL().GetAll();
            var result = (from a in BuyingFacade.AccountCreditNoteDetailBL().GetByFarmerCode(crop, farmerCode)
                          from b in items
                          where a.CropInputsCondition.ItemID == b.itemid
                          select new m_CreditNoteDetail
                          {
                              CreditNoteCode = a.CreditNoteCode,
                              AccountCreditNote = a.AccountCreditNote,
                              ConditionID = a.ConditionID,
                              ItemID = a.CropInputsCondition.ItemID,
                              ItemName = b.item_name,
                              Quantity = a.Quantity,
                              UnitPrice = a.UnitPrice,
                              LotNo = b.lotno,
                              Unit = b.material_unit.unit_name,
                              Brand = b.material_brand.brand_name,
                              Category = b.material_category.category_name,
                              Price = a.UnitPrice * a.Quantity,
                              DebtTypeCode = a.AccountCreditNote
                              .AccountInvoice
                              .Invoice
                              .DebtTypeCode
                          }).ToList();

            return result;
        }

        public static List<m_CreditNoteDetail> GetByExtensionAgent(short crop, string extensionAgentCode)
        {
            var list = BuyingFacade.AccountCreditNoteDetailBL()
                .GetByExtensionAgent(crop, extensionAgentCode)
                .GroupBy(x => new
                {
                    ConditionID = x.ConditionID,
                    ItemID = x.CropInputsCondition.ItemID,
                    UnitPrice = x.UnitPrice
                })
                .Select(x => new m_CreditNoteDetail
                {
                    ConditionID = x.Key.ConditionID,
                    ItemID = x.Key.ItemID,
                    UnitPrice = x.Key.UnitPrice,
                    ReturnQuantity = x.Sum(y => y.Quantity),
                    ReturnPrice = x.Sum(y => y.Quantity * y.UnitPrice)
                }).ToList();

            var list1 = BuyingFacade.InvoiceDetailsBL().GetByExtensionAgent(crop, extensionAgentCode);
            var list2 = (from a in list
                         from b in list1
                         where a.ConditionID == b.ConditionID
                         select new m_CreditNoteDetail
                         {
                             ConditionID = a.ConditionID,
                             ItemID = b.CropInputsCondition.ItemID,
                             Quantity = b.Quantity,
                             UnitPrice = b.UnitPrice,
                             CreditNoteCode = a.CreditNoteCode,
                             AccountCreditNote = a.AccountCreditNote,
                             ReturnQuantity = a.ReturnQuantity,
                             ReturnPrice = a.ReturnPrice
                         })
                         .ToList();

            var items = AGMInventorySystemBL.BLServices.material_itemBL().GetAll();
            var result = (from a in list2
                          from b in items
                          where a.ItemID == b.itemid
                          select new m_CreditNoteDetail
                          {
                              CreditNoteCode = a.CreditNoteCode,
                              AccountCreditNote = a.AccountCreditNote,
                              ConditionID = a.ConditionID,
                              ItemID = a.ItemID,
                              ItemName = b.item_name,
                              Quantity = a.Quantity,
                              UnitPrice = a.UnitPrice,
                              ReturnQuantity = a.ReturnQuantity,
                              ReturnPrice = a.ReturnPrice,
                              LotNo = b.lotno,
                              Unit = b.material_unit.unit_name,
                              Brand = b.material_brand.brand_name,
                              Category = b.material_category.category_name,
                              Price = a.UnitPrice * a.Quantity
                          }).ToList();

            return result;
        }

        public static List<m_CreditNoteHeader> GetHeader(string creditNoteCode)
        {
            var creditNote = BuyingFacade.AccountCreditNoteBL().GetSingle(creditNoteCode);
            var person = BuyingFacade.FarmerBL().GetByFarmerCode(creditNote.AccountInvoice.Invoice.FarmerCode).Person;
            var model = new m_CreditNoteHeader();
            var returnList = new List<m_CreditNoteHeader>();

            /// รายการของที่ลดหนี้ทั้งหมดใน acount invoice นี้
            var creditNoteByAccountInvoiceList = BuyingFacade.AccountCreditNoteBL().GetByAccountInvoice(creditNote.AccountInvoiceNo);
            /// รายการของที่ลดหนี้ใน creditnote นี้
            var creditNoteDetailList = creditNote.AccountCreditNoteDetails;
            /// รายการของก่อนทำการลดหนี้ใน creditnote นี้
            var accumulativeList = creditNoteByAccountInvoiceList.Where(x => x.PrintDate < creditNote.PrintDate).ToList();
            /// ราคารวมของการลดหนี้ ไม่รวมกับใน creditnote นี้เพื่อหามูลค่าตามเอกสารเดิม
            var accumulativePrice = accumulativeList.Sum(x => x.AccountCreditNoteDetails.Sum(y => y.Quantity * y.UnitPrice));
            /// มูลค่าของใน account invoice ทั้งหมด
            var accountInvoicePrice = BuyingFacade.InvoiceDetailsBL().GetByAccountInvoiceNo(creditNote.AccountInvoiceNo)
                .Sum(x => x.Quantity * x.UnitPrice);
            /// มูลค่าของใน creditnote นี้
            var currentDocPrice = creditNoteDetailList.Sum(x => x.Quantity * x.UnitPrice);

            model.CustomerName = person.Prefix + person.FirstName + " " + person.LastName;
            model.CustomerAddress = person.HouseNumber + " หมู่ " + person.Village + " ตำบล" + person.Tumbon + " อำเภอ" + person.Amphur;
            model.CustomerAddress1 = " จังหวัด" + person.Province;
            model.TaxID = person.CitizenID;
            model.CreditNoteCode = "เลขที่ " + creditNoteCode;
            model.Description = creditNote.Description;
            model.AccountInvoiceNo = creditNote.AccountInvoiceNo;
            model.AccountInvoiceAmount = accountInvoicePrice - accumulativePrice; //มูลค่าตามเอกสารเดิม
            model.ThaiBathText = BuyingFacade.StoreProcetureBL().sp_ConvertCurrencyToThaiBath(currentDocPrice); //มูลค่าในเอกสารนี้
            model.CreateDate = "วันที่ " + creditNote.CreateDate.Day + " " +
                MonthText(creditNote.CreateDate.Month) + " " +
                (creditNote.CreateDate.Year + 543);
            model.ReferenceAccountInvoice = "อ้างถึง ใบส่งปัจจัยการผลิต เลขที่ " + creditNote.AccountInvoiceNo +
                " ลงวันที่ " + creditNote.AccountInvoice.CreateDate.Day + " " +
                MonthText(creditNote.AccountInvoice.CreateDate.Month) + " "
                + (creditNote.AccountInvoice.CreateDate.Year + 543);

            returnList.Add(model);
            return returnList;
        }
    }
}
