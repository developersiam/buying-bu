﻿using BusinessLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public static class CY202201ReturnItemHelper
    {
        public static List<m_CY202201ReturnItem> GetAll()
        {
            var returnItemList = BuyingFacade.CY202201ReturnItemBL().GetAll();
            var agmItemList = AGMInventorySystemBL.BLServices.material_itemBL().GetAll();
            return (from a in returnItemList
                    from b in agmItemList
                    where a.ReturnItemID == b.itemid
                    && a.IsActive == true
                    select new m_CY202201ReturnItem
                    {
                        ReturnItemID = a.ReturnItemID,
                        ItemName = b.item_name,
                        ItemType = a.ItemType,
                        IsActive = a.IsActive,
                        Remark = a.Remark
                    })
                    .ToList();
        }
    }
}
