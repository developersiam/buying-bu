﻿using BusinessLayer.Model;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public static class ReceiveSeedAndMediaHelper
    {
        public static List<m_ReceiveSeedAndMedia> GetByFarmerAndCrop(short crop, string farmerCode)
        {
            UnitOfWork uow = new UnitOfWork();

            return (from rs in uow.ReceiveSeedAndMediaRepository
                    .Query(rs => rs.Crop == crop &&
                    rs.FarmerCode == farmerCode,
                    null,
                    rs => rs.SeedVariety,
                    rs => rs.SeedAndMedia)
                    select new m_ReceiveSeedAndMedia
                    {
                        Crop = rs.Crop,
                        FarmerCode = rs.FarmerCode,
                        SeedAndMediaID = rs.SeedAndMediaID,
                        SeedAndMediaName = rs.SeedAndMedia.Name,
                        SeedCode = rs.SeedCode,
                        SeedVariety = rs.SeedVariety,
                        UnitPrice = rs.SeedAndMedia.UnitPrice,
                        Quantity = rs.Quantity,
                        Price = rs.Quantity * rs.SeedAndMedia.UnitPrice
                    }).ToList();
        }
    }
}
