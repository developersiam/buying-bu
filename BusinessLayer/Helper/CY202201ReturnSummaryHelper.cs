﻿using BusinessLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public static class CY202201ReturnSummaryHelper
    {
        public static m_CY202201ReturnSummary GetByFarmer(short crop, string farmerCode)
        {
            var receiveList = (from a in InvoiceDetailsHelper.GetSummaryByFarmer(crop, farmerCode)
                               join b in BuyingFacade.CY202201ReturnItemBL().GetAll()
                               on a.ItemID equals b.ReturnItemID into ab
                               from c in ab.DefaultIfEmpty()
                               select new m_CY202201ReturnDetail
                               {
                                   ReturnItemID = a.ItemID,
                                   ItemName = a.ItemName,
                                   ItemType = c == null ? "ไม่ระบุ" : c.ItemType,
                                   Quantity = a.Quantity,
                               })
                               .ToList();
            var returnList = CY202201ReturnDetailHelper.GetSummaryByFarmer(crop, farmerCode);
            var cpaContainerReceive = receiveList.Where(x => x.ItemType == "ขยะสารเคมี").Sum(y => y.Quantity);
            var cpaContainerReturn = returnList.Where(x => x.ItemType == "ขยะสารเคมี").Sum(y => y.Quantity);
            var basketReceive = receiveList.Where(x => x.ItemType == "ตะกร้า").Sum(y => y.Quantity);
            var basketReturn = returnList.Where(x => x.ItemType == "ตะกร้า").Sum(y => y.Quantity);
            var otherReceive = receiveList.Sum(y => y.Quantity) - cpaContainerReceive - basketReceive;
            var cpaReturnPercent = cpaContainerReturn == 0 || cpaContainerReceive == 0 ? 0 : (decimal)cpaContainerReturn / (decimal)cpaContainerReceive * 100;
            var basketReturnPercent = basketReturn == 0 || basketReceive == 0 ? 0 : (decimal)basketReturn / (decimal)basketReceive * 100;

            return new m_CY202201ReturnSummary
            {
                CPAContainerReceive = cpaContainerReceive,
                CPAContainerReturn = cpaContainerReturn,
                OtherReceive = otherReceive,
                BasketReceive = basketReceive,
                BasketReturn = basketReturn,
                CPAConatainerReturnPercent = cpaReturnPercent,
                BasketReturnPercent = basketReturnPercent,
                ReceiveList = receiveList,
                ReturnList = returnList
            };
        }
    }
}
