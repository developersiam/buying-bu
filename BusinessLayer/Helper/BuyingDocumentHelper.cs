﻿using BusinessLayer.Model;
using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public class BuyingDocumentHelper
    {
        public static m_BuyingDocument GetSingle(short crop, string stationCode, string farmerCode, short documentNumber)
        {
            try
            {
                UnitOfWork uow = new UnitOfWork();

                var model = uow.BuyingDocumentRepository
                    .GetSingle(bd => bd.Crop == crop &&
                    bd.FarmerCode == farmerCode &&
                    bd.BuyingStationCode == stationCode &&
                    bd.BuyingDocumentNumber == documentNumber,
                    bd => bd.Buyings);

                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลใบซื้อหมายเลข " + crop + "-" +
                        stationCode + "-" + farmerCode + "-" + documentNumber + " ในระบบ");

                return new m_BuyingDocument
                {
                    Crop = model.Crop,
                    FarmerCode = model.FarmerCode,
                    BuyingStationCode = model.BuyingStationCode,
                    BuyingDocumentNumber = model.BuyingDocumentNumber,
                    CreateDate = model.CreateDate,
                    CreateUser = model.CreateUser,
                    BuyingDate = model.BuyingDate,
                    IsFinish = model.IsFinish,
                    FinishDate = model.FinishDate,
                    IsAccountFinish = model.IsAccountFinish,
                    IsPrinted = model.IsPrinted,
                    PrintDate = model.PrintDate,
                    ModifiedBy = model.ModifiedBy,
                    ModifiedDate = model.ModifiedDate,
                    TotalBale = model.Buyings.Count,
                    TotalGrade = model.Buyings.Count(b => b.Grade != null),
                    TotalSold = model.Buyings.Sum(b => b.Weight),
                    TotalWeight = model.Buyings.Where(b => b.Weight != null).Count(),
                    TotalReject = model.Buyings.Where(b => b.RejectReason != null).Count(),
                    TotalLoadTruck = model.Buyings.Where(b => b.TransportationDocumentCode != null).Count(),
                    DocumentCode = model.Crop + "-" + model.BuyingStationCode + "-" + model.FarmerCode + "-" + model.BuyingDocumentNumber,
                    Buyings = model.Buyings
                };
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<m_BuyingDocument> GetByStationAndBuyingDate(DateTime buyingDate, string buyingStationCode)
        {
            try
            {
                UnitOfWork uow = new UnitOfWork();

                var list = uow.BuyingRepository
                    .Query(x => x.Crop == buyingDate.Year &&
                    x.BuyingStationCode == buyingStationCode &&
                    x.BuyingDocument.BuyingDate.Value.Day == buyingDate.Day &&
                    x.BuyingDocument.BuyingDate.Value.Month == buyingDate.Month &&
                    x.BuyingDocument.BuyingDate.Value.Year == buyingDate.Year,
                    null,
                    x => x.BuyingDocument,
                    x => x.BuyingGrade,
                    x => x.BuyingDocument.RegistrationFarmer,
                    x => x.BuyingDocument.RegistrationFarmer.Farmer,
                    x => x.BuyingDocument.RegistrationFarmer.Farmer.Person)
                    .GroupBy(x => new
                    {
                        x.Crop,
                        x.FarmerCode,
                        x.BuyingStationCode,
                        x.BuyingDocumentNumber,
                        x.BuyingDocument.CreateDate,
                        x.BuyingDocument.BuyingDate,
                        x.BuyingDocument.IsFinish,
                        x.BuyingDocument.FinishDate,
                        x.BuyingDocument.IsAccountFinish,
                        x.BuyingDocument.IsPrinted,
                        x.BuyingDocument.PrintDate,
                        x.BuyingDocument.RegistrationFarmer.Farmer.Person.CitizenID,
                        x.BuyingDocument.RegistrationFarmer.Farmer.Person.Prefix,
                        x.BuyingDocument.RegistrationFarmer.Farmer.Person.FirstName,
                        x.BuyingDocument.RegistrationFarmer.Farmer.Person.LastName,
                    }).Select(y => new m_BuyingDocument
                    {
                        Crop = y.Key.Crop,
                        FarmerCode = y.Key.FarmerCode,
                        BuyingStationCode = y.Key.BuyingStationCode,
                        BuyingDocumentNumber = y.Key.BuyingDocumentNumber,
                        DocumentCode = y.Key.Crop + "-" +
                        y.Key.BuyingStationCode + "-" +
                        y.Key.FarmerCode + "-" +
                        y.Key.BuyingDocumentNumber,
                        CreateDate = y.Key.CreateDate,
                        BuyingDate = y.Key.BuyingDate,
                        IsFinish = y.Key.IsFinish,
                        FinishDate = y.Key.FinishDate,
                        IsAccountFinish = y.Key.IsAccountFinish,
                        IsPrinted = y.Key.IsPrinted,
                        PrintDate = y.Key.PrintDate,
                        CitizenID = y.Key.CitizenID,
                        Prefix = y.Key.Prefix,
                        FirstName = y.Key.FirstName,
                        LastName = y.Key.LastName,
                        TotalBale = y.Count(),
                        TotalGrade = y.Where(b => b.Grade != null).Count(),
                        TotalSold = y.Sum(b => b.Weight),
                        TotalWeight = y.Where(b => b.Weight != null).Count(),
                        TotalPrice = (decimal)(y.Sum(b => b.Grade == null ? 0 : b.BuyingGrade.UnitPrice * b.Weight)),
                        TotalReject = y.Where(b => b.RejectReason != null).Count(),
                        TotalLoadTruck = y.Where(b => b.TransportationDocumentCode != null).Count()
                    })
                    .ToList();

                return list;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<m_BuyingDocument> GetByStationAndCreateDate(DateTime createDate, string buyingStationCode)
        {
            try
            {
                UnitOfWork uow = new UnitOfWork();

                var list = uow.BuyingDocumentRepository
                        .Query(x => x.Crop == createDate.Year &&
                        x.BuyingStationCode == buyingStationCode &&
                        x.CreateDate != null,
                        null,
                        bd => bd.RegistrationFarmer,
                        bd => bd.RegistrationFarmer.Farmer,
                        bd => bd.RegistrationFarmer.Farmer.Person)
                        .ToList();

                var list2 = uow.BuyingRepository
                    .Query(x => x.Crop == createDate.Year &&
                    x.BuyingStationCode == buyingStationCode)
                    .GroupBy(x => new
                    {
                        x.Crop,
                        x.FarmerCode,
                        x.BuyingStationCode,
                        x.BuyingDocumentNumber
                    }).Select(y => new m_BuyingDocument
                    {
                        Crop = y.Key.Crop,
                        FarmerCode = y.Key.FarmerCode,
                        BuyingStationCode = y.Key.BuyingStationCode,
                        BuyingDocumentNumber = y.Key.BuyingDocumentNumber,
                        TotalBale = y.Count(),
                        TotalGrade = y.Where(b => b.Grade != null).Count(),
                        TotalSold = y.Sum(b => b.Weight),
                        TotalWeight = y.Where(b => b.Weight != null).Count(),
                        TotalReject = y.Where(b => b.RejectReason != null).Count(),
                        TotalLoadTruck = y.Where(b => b.TransportationDocumentCode != null).Count(),
                    }).ToList();

                var list3 = (from bd in list
                             from b in list2
                             where bd.Crop == b.Crop &&
                             bd.FarmerCode == b.FarmerCode &&
                             bd.BuyingStationCode == b.BuyingStationCode &&
                             bd.BuyingDocumentNumber == b.BuyingDocumentNumber &&
                             Convert.ToDateTime(bd.CreateDate).Day == createDate.Day &&
                             Convert.ToDateTime(bd.CreateDate).Month == createDate.Month &&
                             Convert.ToDateTime(bd.CreateDate).Year == createDate.Year
                             select new m_BuyingDocument
                             {
                                 Crop = bd.Crop,
                                 FarmerCode = bd.FarmerCode,
                                 BuyingStationCode = bd.BuyingStationCode,
                                 BuyingDocumentNumber = bd.BuyingDocumentNumber,
                                 CreateDate = bd.CreateDate,
                                 CreateUser = bd.CreateUser,
                                 BuyingDate = bd.BuyingDate,
                                 IsFinish = bd.IsFinish,
                                 FinishDate = bd.FinishDate,
                                 IsAccountFinish = bd.IsAccountFinish,
                                 IsPrinted = bd.IsPrinted,
                                 PrintDate = bd.PrintDate,
                                 ModifiedBy = bd.ModifiedBy,
                                 ModifiedDate = bd.ModifiedDate,
                                 TotalBale = b.TotalBale,
                                 TotalGrade = b.TotalGrade,
                                 TotalSold = b.TotalSold,
                                 TotalWeight = b.TotalWeight,
                                 TotalReject = b.TotalReject,
                                 TotalLoadTruck = b.TotalLoadTruck,
                                 DocumentCode = bd.Crop + "-" + bd.BuyingStationCode + "-" + bd.FarmerCode + "-" + bd.BuyingDocumentNumber,
                                 CitizenID = bd.RegistrationFarmer.Farmer.CitizenID,
                                 Prefix = bd.RegistrationFarmer.Farmer.Person.Prefix,
                                 FirstName = bd.RegistrationFarmer.Farmer.Person.FirstName,
                                 LastName = bd.RegistrationFarmer.Farmer.Person.LastName,
                                 RegistrationFarmer = bd.RegistrationFarmer
                             }).ToList();

                return list3.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<m_BuyingDocument> GetByFarmer(short crop, string farmerCode)
        {
            UnitOfWork uow = new UnitOfWork();
            return (from bd in uow.BuyingDocumentRepository
                    .Get(bd => bd.Crop == crop &&
                    bd.FarmerCode == farmerCode
                    , null
                    , bd => bd.RegistrationFarmer
                    , bd => bd.RegistrationFarmer.Farmer
                    , bd => bd.RegistrationFarmer.Farmer.Person
                    , bd => bd.Buyings)
                    select new m_BuyingDocument
                    {
                        Crop = bd.Crop,
                        FarmerCode = bd.FarmerCode,
                        BuyingStationCode = bd.BuyingStationCode,
                        BuyingDocumentNumber = bd.BuyingDocumentNumber,
                        CreateDate = bd.CreateDate,
                        CreateUser = bd.CreateUser,
                        BuyingDate = bd.BuyingDate,
                        IsFinish = bd.IsFinish,
                        FinishDate = bd.FinishDate,
                        IsAccountFinish = bd.IsAccountFinish,
                        IsPrinted = bd.IsPrinted,
                        PrintDate = bd.PrintDate,
                        ModifiedBy = bd.ModifiedBy,
                        ModifiedDate = bd.ModifiedDate,
                        TotalBale = bd.Buyings.Count,
                        TotalGrade = bd.Buyings.Count(b => b.Grade != null),
                        TotalSold = bd.Buyings.Sum(b => b.Weight),
                        TotalWeight = bd.Buyings.Where(b => b.Weight != null).Count(),
                        TotalReject = bd.Buyings.Where(b => b.RejectReason != null).Count(),
                        DocumentCode = bd.Crop + "-" + bd.BuyingStationCode + "-" + bd.FarmerCode + "-" + bd.BuyingDocumentNumber,
                        CitizenID = bd.RegistrationFarmer.Farmer.CitizenID,
                        Prefix = bd.RegistrationFarmer.Farmer.Person.Prefix,
                        FirstName = bd.RegistrationFarmer.Farmer.Person.FirstName,
                        LastName = bd.RegistrationFarmer.Farmer.Person.LastName,
                        RegistrationFarmer = bd.RegistrationFarmer,
                        Buyings = bd.Buyings
                    }).ToList();

        }
    }
}
