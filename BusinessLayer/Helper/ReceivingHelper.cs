﻿using BusinessLayer.Model;
using DataAccessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Helper
{
    public static class ReceivingHelper
    {
        public static List<m_ReceivingSummary> GetBySupplierCode(DateTime buyingDate, string supplierCode, bool confirmStatus)
        {
            return StoreProcedureRepository.sp_Receiving_SEL_SummaryBuyingAndReceiving(buyingDate, supplierCode, confirmStatus)
                .Select(x => new m_ReceivingSummary
                {
                    Crop = x.Crop,
                    BuyingStationCode = x.BuyingStationCode,
                    FarmerCode = x.FarmerCode,
                    BuyingDocumentNumber = x.BuyingDocumentNumber,
                    Supplier = x.Supplier,
                    CitizenID = x.CitizenID,
                    IsAccountFinish = x.IsAccountFinish,
                    CreateDate = x.CreateDate,
                    BuyingDate = x.BuyingDate,
                    BuyingBale = x.BuyingBale,
                    ReceivingBale = x.ReceivingBale,
                    BuyingWeight = x.BuyingWeight,
                    ReceivingWeight = x.ReceivingWeight,
                    Prefix = x.Prefix,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    DiffBale = (int)(x.BuyingBale - x.ReceivingBale),
                    DiffWeight = (decimal)(x.BuyingWeight - x.ReceivingWeight),
                    BuyingDocNo = x.Crop + "-" + x.BuyingStationCode + "-" + x.FarmerCode + "-" + x.BuyingDocumentNumber
                })
                .ToList();
        }
    }
}
