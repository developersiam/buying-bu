﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace DataAccessLayer
{
    public interface IAreaRepository : IGenericDataRepository<Area> { }
    public interface IAspNetRoleRepository : IGenericDataRepository<AspNetRole> { }
    public interface IAspNetUserRepository : IGenericDataRepository<AspNetUser> { }
    public interface IBuyerRepository : IGenericDataRepository<Buyer> { }
    public interface IBuyingDocumentRepository : IGenericDataRepository<BuyingDocument> { }
    public interface IBuyingGradeRepository : IGenericDataRepository<BuyingGrade> { }
    public interface IBuyingReporsitory : IGenericDataRepository<Buying> { }
    public interface IBuyingStationRepository : IGenericDataRepository<BuyingStation> { }
    public interface IChemicalCategoryRepository : IGenericDataRepository<ChemicalCategory> { }
    public interface IChemicalDistributionRepository : IGenericDataRepository<ChemicalDistribution> { }
    public interface IChemicalItemRepository : IGenericDataRepository<ChemicalItem> { }
    public interface IChemicalItemTypeRepository : IGenericDataRepository<ChemicalItemType> { }
    public interface IChemicalItemUnitRepository : IGenericDataRepository<ChemicalItemUnit> { }
    public interface ICropAnimalForConsumtionRepository : IGenericDataRepository<CropAnimalForConsumption> { }
    public interface ICropInputCategoryRepository : IGenericDataRepository<CropInputCategory> { }
    public interface ICropInputDistributionRepository : IGenericDataRepository<CropInputDistribution> { }
    public interface ICropInputItemRepository : IGenericDataRepository<CropInputItem> { }
    public interface ICropRepository : IGenericDataRepository<Crop> { }
    public interface IMasterGradeRepository : IGenericDataRepository<MasterGrade> { }
    public interface ICuringBarnInformationRepository : IGenericDataRepository<CuringBarnInformation> { }
    public interface IDebtorPaymentRepository : IGenericDataRepository<DebtorPayment> { }
    public interface IDistrictRepository : IGenericDataRepository<District> { }
    public interface IEducationLevelRepository : IGenericDataRepository<EducationLevel> { }
    public interface IFarmerChildInformationRepository : IGenericDataRepository<FarmerChildInformation> { }
    public interface IFarmerMachineAndFarmEquipmentRepository : IGenericDataRepository<FarmerMachineAndFarmEquipment> { }
    public interface IFarmerProjectRepository : IGenericDataRepository<FarmerProject> { }
    public interface IFarmerRepository : IGenericDataRepository<Farmer> { }
    public interface IFarmLandInfomationRepository : IGenericDataRepository<FarmLandInformation> { }
    public interface IFertilizerAndChemicalRepository : IGenericDataRepository<FertilizerAndChemical> { }
    public interface IFertilizerAndChemicalTypeRepository : IGenericDataRepository<FertilizerAndChemicalType> { }
    public interface IFertilizerAndChemicalUnitRepository : IGenericDataRepository<FertilizerAndChemicalUnit> { }
    public interface IGAPGroupLeaderByCropRepository : IGenericDataRepository<GAPGroupLeaderByCrop> { }
    public interface IGapGroupLeaderRepository : IGenericDataRepository<GAPGroupLeader> { }
    public interface IGapGroupRepository : IGenericDataRepository<GAPGroup> { }
    public interface IInputFacorRepository : IGenericDataRepository<InputFactor> { }
    public interface IKilogramsPerRaiConfigurationRepository : IGenericDataRepository<KilogramsPerRaiConfiguration> { }
    public interface IMachineAndFarmEquipmentRepository : IGenericDataRepository<MachineAndFarmEquipment> { }
    public interface IMaritalStatusRepository : IGenericDataRepository<MaritalStatu> { }
    public interface INTRMInspectionRepository : IGenericDataRepository<NTRMInspection> { }
    public interface INTRMTypeRepository : IGenericDataRepository<NTRMType> { }
    public interface IOtherCropProductionRepository : IGenericDataRepository<OtherCropProduction> { }
    public interface IPaymentForInputFactorRepository : IGenericDataRepository<PaymentForInputFactor> { }
    public interface IPaymentForSeedAndMediaRepository : IGenericDataRepository<PaymentForSeedAndMedia> { }
    public interface IPersonRepository : IGenericDataRepository<Person> { }
    public interface IPersonSpouseRepository : IGenericDataRepository<PersonSpouse> { }
    public interface IPricingSetRepository : IGenericDataRepository<PricingSet> { }
    public interface IProjectTypeRepository : IGenericDataRepository<ProjectType> { }
    public interface IProvinceRepository : IGenericDataRepository<Province> { }
    public interface IReceiveFertilizerAndChemicalRepository : IGenericDataRepository<ReceiveFertilizerAndChemical> { }
    public interface IReceiveInputFactorRepository : IGenericDataRepository<ReceiveInputFactor> { }
    public interface IReceiveSeedAndMediaRepository : IGenericDataRepository<ReceiveSeedAndMedia> { }
    public interface IRegistrationRepository : IGenericDataRepository<RegistrationFarmer> { }
    public interface IReturnChemicalRepository : IGenericDataRepository<ReturnChemical> { }
    public interface IReturnCPAContainerRepository : IGenericDataRepository<ReturnCPAContainer> { }
    public interface IReturnFertilizerAndChemicalRepository : IGenericDataRepository<ReturnFertilizerAndChemical> { }
    public interface IRoleRepository : IGenericDataRepository<Role> { }
    public interface ISeedAndMediaRepository : IGenericDataRepository<SeedAndMedia> { }
    public interface ISeedAndMediaTypeRepository : IGenericDataRepository<SeedAndMediaType> { }
    public interface ISeedAndMedieUnitRepository : IGenericDataRepository<SeedAndMediaUnit> { }
    public interface ISeedDistributionRepository : IGenericDataRepository<SeedDistribution> { }
    public interface ISeedDistributionTypeRepository : IGenericDataRepository<SeedDistributionType> { }
    public interface ISeedForDistributionRepository : IGenericDataRepository<SeedForDistribution> { }
    public interface ISeedVarietyRepository : IGenericDataRepository<SeedVariety> { }
    public interface ISubDistrictRepository : IGenericDataRepository<SubDistrict> { }
    public interface ISupervisorUserRepository : IGenericDataRepository<SupervisorUser> { }
    public interface ISupplierRepository : IGenericDataRepository<Supplier> { }
    public interface ISupplierUserRepository : IGenericDataRepository<SupplierUser> { }
    public interface ISystemDeadlineSettingRepository : IGenericDataRepository<SystemDeadlineSetting> { }
    public interface ITransportationDocumentRepository : IGenericDataRepository<TransportationDocument> { }
    public interface ITreeSeedingTypeRepository : IGenericDataRepository<TreeSeedingType> { }
    public interface ITreeSeedlingWantedRepository : IGenericDataRepository<TreeSeedlingWanted> { }
    public interface IUserRepository : IGenericDataRepository<UserAccount> { }
    public interface IUserRoleRepository : IGenericDataRepository<UserRole> { }
    public interface IIrrigationWaterRepository : IGenericDataRepository<IrrigationWater> { }
    public interface IIrrigationWaterTypeRepository : IGenericDataRepository<IrrigationWaterType> { }
    public interface IEstimateTransplantingPeriodRepository : IGenericDataRepository<EstimateTransplantingPeriod> { }
    public interface IFarmerEstimateTransplantingPeriodRepository : IGenericDataRepository<FarmerEstimateTransplantingPeriod> { }
    public interface IBankRepository : IGenericDataRepository<Bank> { }
    public interface ICPASampleCollectionRepository : IGenericDataRepository<CPASampleCollection> { }
    public interface ICreditProjectRepository : IGenericDataRepository<CreditProject> { }
    public interface IFarmerCreditProjectRepository : IGenericDataRepository<FarmerCreditProject> { }


    public class AreaRepository : GenericDataRepository<Area>, IAreaRepository { }
    public class AspNetRoleRepository : GenericDataRepository<AspNetRole>, IAspNetRoleRepository { }
    public class AspNetUserRepository : GenericDataRepository<AspNetUser>, IAspNetUserRepository { }
    public class BuyerRepository : GenericDataRepository<Buyer>, IBuyerRepository { }
    public class BuyingDocumentRepository : GenericDataRepository<BuyingDocument>, IBuyingDocumentRepository { }
    public class BuyingGradeRepository : GenericDataRepository<BuyingGrade>, IBuyingGradeRepository { }
    public class BuyingRepository : GenericDataRepository<Buying>, IBuyingReporsitory { }
    public class BuyingStationRepository : GenericDataRepository<BuyingStation>, IBuyingStationRepository { }
    public class ChemicalCategoryRepository : GenericDataRepository<ChemicalCategory>, IChemicalCategoryRepository { }
    public class ChemicalDistributionRepository : GenericDataRepository<ChemicalDistribution>, IChemicalDistributionRepository { }
    public class ChemicalItemRepository : GenericDataRepository<ChemicalItem>, IChemicalItemRepository { }
    public class ChemicalItemTypeRepository : GenericDataRepository<ChemicalItemType>, IChemicalItemTypeRepository { }
    public class ChemicalItemUnitRepository : GenericDataRepository<ChemicalItemUnit>, IChemicalItemUnitRepository { }
    public class CropAnimalForConsumtionRepository : GenericDataRepository<CropAnimalForConsumption>, ICropAnimalForConsumtionRepository { }
    public class CropInputCategoryRepository : GenericDataRepository<CropInputCategory>, ICropInputCategoryRepository { }
    public class CropInputDistributionRepository : GenericDataRepository<CropInputDistribution>, ICropInputDistributionRepository { }
    public class CropInputItemRepository : GenericDataRepository<CropInputItem>, ICropInputItemRepository { }
    public class CropRepository : GenericDataRepository<Crop>, ICropRepository { }
    public class CuringBarnInformationRepository : GenericDataRepository<CuringBarnInformation>, ICuringBarnInformationRepository { }
    public class DebtorPaymentRepository : GenericDataRepository<DebtorPayment>, IDebtorPaymentRepository { }
    public class DistrictRepository : GenericDataRepository<District>, IDistrictRepository { }
    public class EducationLevelRepository : GenericDataRepository<EducationLevel>, IEducationLevelRepository { }
    public class FarmerChildInformationRepository : GenericDataRepository<FarmerChildInformation>, IFarmerChildInformationRepository { }
    public class FarmerMachineAndFarmEquipmentRepository : GenericDataRepository<FarmerMachineAndFarmEquipment>, IFarmerMachineAndFarmEquipmentRepository { }
    public class FarmerProjectRepository : GenericDataRepository<FarmerProject>, IFarmerProjectRepository { }
    public class FarmerRepository : GenericDataRepository<Farmer>, IFarmerRepository { }
    public class FarmLandInfomationRepository : GenericDataRepository<FarmLandInformation>, IFarmLandInfomationRepository { }
    public class FertilizerAndChemicalRepository : GenericDataRepository<FertilizerAndChemical>, IFertilizerAndChemicalRepository { }
    public class FertilizerAndChemicalTypeRepository : GenericDataRepository<FertilizerAndChemicalType>, IFertilizerAndChemicalTypeRepository { }
    public class FertilizerAndChemicalUnitRepository : GenericDataRepository<FertilizerAndChemicalUnit>, IFertilizerAndChemicalUnitRepository { }
    public class GAPGroupLeaderByCropRepository : GenericDataRepository<GAPGroupLeaderByCrop>, IGAPGroupLeaderByCropRepository { }
    public class GapGroupLeaderRepository : GenericDataRepository<GAPGroupLeader>, IGapGroupLeaderRepository { }
    public class GapGroupRepository : GenericDataRepository<GAPGroup>, IGapGroupRepository { }
    public class InputFactorRepository : GenericDataRepository<InputFactor>, IInputFacorRepository { }
    public class KilogramsPerRaiConfigurationRepository : GenericDataRepository<KilogramsPerRaiConfiguration>, IKilogramsPerRaiConfigurationRepository { }
    public class MachineAndFarmEquipmentRepository : GenericDataRepository<MachineAndFarmEquipment>, IMachineAndFarmEquipmentRepository { }
    public class MaritalStatusRepository : GenericDataRepository<MaritalStatu>, IMaritalStatusRepository { }
    public class MasterGradeRepository : GenericDataRepository<MasterGrade>, IMasterGradeRepository { }
    public class NTRMInspectionRepository : GenericDataRepository<NTRMInspection>, INTRMInspectionRepository { }
    public class NTRMTypeRepository : GenericDataRepository<NTRMType>, INTRMTypeRepository { }
    public class OtherCropProductionRepository : GenericDataRepository<OtherCropProduction>, IOtherCropProductionRepository { }
    public class PaymentForInputFactorRepository : GenericDataRepository<PaymentForInputFactor>, IPaymentForInputFactorRepository { }
    public class PaymentForSeedAndMediaRepository : GenericDataRepository<PaymentForSeedAndMedia>, IPaymentForSeedAndMediaRepository { }
    public class PersonRepository : GenericDataRepository<Person>, IPersonRepository { }
    public class PersonSpouseRepository : GenericDataRepository<PersonSpouse>, IPersonSpouseRepository { }
    public class PricingSetRepository : GenericDataRepository<PricingSet>, IPricingSetRepository { }
    public class ProjectTypeRepository : GenericDataRepository<ProjectType>, IProjectTypeRepository { }
    public class ProvinceRepository : GenericDataRepository<Province>, IProvinceRepository { }
    public class ReceiveFertilizerAndChemicalRepository : GenericDataRepository<ReceiveFertilizerAndChemical>, IReceiveFertilizerAndChemicalRepository { }
    public class ReceiveInputFactorRepository : GenericDataRepository<ReceiveInputFactor>, IReceiveInputFactorRepository { }
    public class RecevieSeedAndMediaRepository : GenericDataRepository<ReceiveSeedAndMedia>, IReceiveSeedAndMediaRepository { }
    public class RegistrationRepository : GenericDataRepository<RegistrationFarmer>, IRegistrationRepository { }
    public class ReturnChemicalRepository : GenericDataRepository<ReturnChemical>, IReturnChemicalRepository { }
    public class ReturnCPAContainerRepository : GenericDataRepository<ReturnCPAContainer>, IReturnCPAContainerRepository { }
    public class ReturnFertilizerAndChemicalRepository : GenericDataRepository<ReturnFertilizerAndChemical>, IReturnFertilizerAndChemicalRepository { }
    public class RoleRepository : GenericDataRepository<Role>, IRoleRepository { }
    public class SeedAndMediaRepository : GenericDataRepository<SeedAndMedia>, ISeedAndMediaRepository { }
    public class SeedAndMediaTypeRepository : GenericDataRepository<SeedAndMediaType>, ISeedAndMediaTypeRepository { }
    public class SeedAndMediaUnitRepository : GenericDataRepository<SeedAndMediaUnit>, ISeedAndMedieUnitRepository { }
    public class SeedDistributionRepository : GenericDataRepository<SeedDistribution>, ISeedDistributionRepository { }
    public class SeedDistributionTypeRepository : GenericDataRepository<SeedDistributionType>, ISeedDistributionTypeRepository { }
    public class SeedForDistributionRepository : GenericDataRepository<SeedForDistribution>, ISeedForDistributionRepository { }
    public class SeedVarietyRepository : GenericDataRepository<SeedVariety>, ISeedVarietyRepository { }
    public class SubDistrictRepository : GenericDataRepository<SubDistrict>, ISubDistrictRepository { }
    public class SupervisorUserRepository : GenericDataRepository<SupervisorUser>, ISupervisorUserRepository { }
    public class SupplierRepository : GenericDataRepository<Supplier>, ISupplierRepository { }
    public class SupplierUserRepository : GenericDataRepository<SupplierUser>, ISupplierUserRepository { }
    public class SystemDeadlineSettingRepository : GenericDataRepository<SystemDeadlineSetting>, ISystemDeadlineSettingRepository { }
    public class TransportationDocumentRepository : GenericDataRepository<TransportationDocument>, ITransportationDocumentRepository { }
    public class TreeSeedingTypeRepository : GenericDataRepository<TreeSeedingType>, ITreeSeedingTypeRepository { }
    public class TreeSeedlingWantedRepository : GenericDataRepository<TreeSeedlingWanted>, ITreeSeedlingWantedRepository { }
    public class UserRepository : GenericDataRepository<UserAccount>, IUserRepository { }
    public class UserRoleRepositiry : GenericDataRepository<UserRole>, IUserRoleRepository { }
    public class IrrigationWaterRepository : GenericDataRepository<IrrigationWater>, IIrrigationWaterRepository { }
    public class IrrigationWaterTypeRepository : GenericDataRepository<IrrigationWaterType>, IIrrigationWaterTypeRepository { }
    public class EstimateTransplantingPeriodRepository : GenericDataRepository<EstimateTransplantingPeriod>, IEstimateTransplantingPeriodRepository { }
    public class FarmerEstimateTransplantingPeriodRepository : GenericDataRepository<FarmerEstimateTransplantingPeriod>, IFarmerEstimateTransplantingPeriodRepository { }
    public class BankRepository : GenericDataRepository<Bank>, IBankRepository { }
    public class CPASampleCollectionRepository : GenericDataRepository<CPASampleCollection>, ICPASampleCollectionRepository { }
    public class CreditProjectRepository : GenericDataRepository<CreditProject>, ICreditProjectRepository { }
    public class FarmerCreditProjectRepository : GenericDataRepository<FarmerCreditProject>, IFarmerCreditProjectRepository { }
}