﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using System.Data.Entity;
using DataAccessLayer.EDMX;

namespace DataAccessLayer
{
    public class UnitOfWork : IUnitOfWork, System.IDisposable
    {
        private readonly BuyingSystemEntities _context;
        private IGenericDataRepository<Area> _areaRepository;
        private IGenericDataRepository<AspNetRole> _aspNetRole;
        private IGenericDataRepository<AspNetUserClaim> _aspNetUserClaim;
        private IGenericDataRepository<AspNetUserLogin> _aspNetUserLogin;
        private IGenericDataRepository<AspNetUser> _aspNetUser;
        private IGenericDataRepository<Bank> _bank;
        private IGenericDataRepository<Buyer> _buyer;
        private IGenericDataRepository<Buying> _buying;
        private IGenericDataRepository<BuyingDocument> _buyingDocument;
        private IGenericDataRepository<BuyingGrade> _buyingGrade;
        private IGenericDataRepository<BuyingStation> _buyingStation;
        private IGenericDataRepository<Chemical> _chemical;
        private IGenericDataRepository<ChemicalCategory> _chemicalCategory;
        private IGenericDataRepository<ChemicalDistribution> _chemicalDistribution;
        private IGenericDataRepository<ChemicalItem> _chemicalItem;
        private IGenericDataRepository<ChemicalItemType> _chemicalItemType;
        private IGenericDataRepository<ChemicalItemUnit> _chemicalItemUnit;
        private IGenericDataRepository<ChemicalType> _chemicalType;
        private IGenericDataRepository<Crop> _crop;
        private IGenericDataRepository<CropAnimalForConsumption> _cropAnimalForConsumption;
        private IGenericDataRepository<CropInputCategory> _cropInputCategory;
        private IGenericDataRepository<CropInputDistribution> _cropInputDistribution;
        private IGenericDataRepository<CropInputItem> _cropInputItem;
        private IGenericDataRepository<CuringBarnInformation> _curingBarnInformation;
        private IGenericDataRepository<CPASampleCollection> _cpaSampleCollection;
        private IGenericDataRepository<CPASampleCollection2020> _cpaSampleCollection2020;
        private IGenericDataRepository<CY202201CreditNoteCode> _cy202201CreditNoteCode;
        private IGenericDataRepository<CY202201ReturnItem> _cy202201ReturnItem;
        private IGenericDataRepository<CY202201Return> _cy202201Return;
        private IGenericDataRepository<CY202201ReturnDetail> _cy202201ReturnDetail;
        private IGenericDataRepository<DebtorPayment> _debtorPayment;
        private IGenericDataRepository<District> _district;
        private IGenericDataRepository<EducationLevel> _educationLevel;
        private IGenericDataRepository<EstimateTransplantingPeriod> _estimateTransplantingPeriod;
        private IGenericDataRepository<Farmer> _farmer;
        private IGenericDataRepository<FarmerChildInformation> _farmerChildInformation;
        private IGenericDataRepository<FarmerEstimateTransplantingPeriod> _farmerEstimateTransplantingPeriod;
        private IGenericDataRepository<FarmerMachineAndFarmEquipment> _farmerMachineAndFarmEquipment;
        private IGenericDataRepository<FarmerProject> _farmerProject;
        private IGenericDataRepository<FarmLandInformation> _farmLandInformation;
        private IGenericDataRepository<FarmProfile> _farmProfile;
        private IGenericDataRepository<FertilizerAndChemical> _fertilizerAndChemical;
        private IGenericDataRepository<FertilizerAndChemicalType> _fertilizerAndChemicalType;
        private IGenericDataRepository<FertilizerAndChemicalUnit> _fertilizerAndChemicalUnit;
        private IGenericDataRepository<GMSFarmerInfoCollectionCriteria> _gmsFarmerInfoCollectionCriteriap;
        private IGenericDataRepository<GAPGroup2020> _gapGroup2020;
        private IGenericDataRepository<GAPGroupMember2020> _gapGroupMember2020;
        private IGenericDataRepository<GAPGroup> _gapGroup;
        private IGenericDataRepository<GAPGroupLeader> _gapGroupLeader;
        private IGenericDataRepository<GAPGroupLeaderByCrop> _gapGroupLeaderByCrop;
        private IGenericDataRepository<HistoryBackBuying> _historyBackBuying;
        private IGenericDataRepository<HistoryBuying> _historyBuying;
        private IGenericDataRepository<InputFactor> _inputFactor;
        private IGenericDataRepository<IrrigationWater> _irrigationWater;
        private IGenericDataRepository<IrrigationWaterType> _irrigationWaterType;
        private IGenericDataRepository<KilogramsPerRaiConfiguration> _kilogramsPerRaiConfiguration;
        private IGenericDataRepository<MachineAndFarmEquipment> _machineAndFarmEquipment;
        private IGenericDataRepository<MaritalStatu> _maritalStatu;
        private IGenericDataRepository<MasterGrade> _masterGrade;
        //private IGenericDataRepository<Material> _material;
        private IGenericDataRepository<NTRMInspection> _ntrmInspection;
        private IGenericDataRepository<NTRMSample> _ntrmSample;
        private IGenericDataRepository<NTRMType> _ntrmType;
        private IGenericDataRepository<OtherCropProduction> _otherCropProduction;
        private IGenericDataRepository<PaymentForInputFactor> _paymentForInputFactor;
        private IGenericDataRepository<PaymentForSeedAndMedia> _paymentForSeedAndMedia;
        private IGenericDataRepository<Person> _person;
        private IGenericDataRepository<PersonSpouse> _personSpouse;
        private IGenericDataRepository<PricingSet> _pricingSet;
        private IGenericDataRepository<ProductionFactor> _productionFactor;
        private IGenericDataRepository<ProductionFactorDetail> _productionFactorDetail;
        private IGenericDataRepository<ProjectType> _projectType;
        private IGenericDataRepository<Province> _province;
        private IGenericDataRepository<ReceiveChemical> _receiveChemical;
        private IGenericDataRepository<ReceiveInputFactor> _receiveInputFactor;
        private IGenericDataRepository<ReceiveProductionFactor> _receiveProductionFactor;
        private IGenericDataRepository<ReceiveFertilizerAndChemical> _receiveFertilizerAndChemical;
        private IGenericDataRepository<ReceiveSeedAndMedia> _receiveSeedAndMedia;
        private IGenericDataRepository<RegistrationFarmer> _registrationFarmer;
        private IGenericDataRepository<ReturnChemical> _returnChemical;
        private IGenericDataRepository<ReturnCPAContainer> _returnCPAContainer;
        private IGenericDataRepository<ReturnFertilizerAndChemical> _returnFertilizerAndChemical;
        private IGenericDataRepository<Role> _role;
        private IGenericDataRepository<SeedAndMedia> _seedAndMedia;
        private IGenericDataRepository<SeedAndMediaType> _seedAndMediaType;
        private IGenericDataRepository<SeedAndMediaUnit> _seedAndMediaUnit;
        private IGenericDataRepository<SeedDistribution> _seedDistribution;
        private IGenericDataRepository<SeedDistributionType> _seedDistributionType;
        private IGenericDataRepository<SeedForDistribution> _seedForDistribution;
        private IGenericDataRepository<SeedVariety> _seedVariety;
        private IGenericDataRepository<StaffUser> _staffUser;
        private IGenericDataRepository<SubDistrict> _subDistrict;
        private IGenericDataRepository<SupervisorUser> _supervisorUser;
        private IGenericDataRepository<Supplier> _supplier;
        private IGenericDataRepository<SupplierUser> _supplierUser;
        private IGenericDataRepository<SystemDeadlineSetting> _systemDeadlineSetting;
        private IGenericDataRepository<TransportationDocument> _transportationDocument;
        private IGenericDataRepository<TreeSeedingType> _treeSeedingType;
        private IGenericDataRepository<TreeSeedlingWanted> _treeSeedlingWanted;
        private IGenericDataRepository<UserAccount> _userAccount;
        private IGenericDataRepository<UserRole> _userRole;
        private IGenericDataRepository<CreditProject> _creditProject;
        private IGenericDataRepository<FarmerCreditProject> _farmerCreditProject;
        private IGenericDataRepository<Warehouse> _warehouse;
        private IGenericDataRepository<HessianReceiving> _hessianReceiving;
        private IGenericDataRepository<HessianDistribution> _hessianDistribution;
        private IGenericDataRepository<IncidentCategory> _incidentCategory;
        private IGenericDataRepository<IncidentTopic> _incidentTopic;
        private IGenericDataRepository<IncidentDescription> _incidentDescription;
        private IGenericDataRepository<IncidentFarmer> _incidentFarmer;
        private IGenericDataRepository<IncidentFollowUp> _incidentFollowUp;
        private IGenericDataRepository<Invoice> _invoice;
        private IGenericDataRepository<InvoiceDetail> _invoiceDetail;
        private IGenericDataRepository<CropInputsPackage> _cropInputsPackage;
        private IGenericDataRepository<CropInputsPackageDetail> _cropInputsPackageDetail;
        private IGenericDataRepository<CropInputsCondition> _cropInputsCondition;
        private IGenericDataRepository<AccountInvoice> _accountInvoice;
        private IGenericDataRepository<AccountCreditNote> _accountCreditNote;
        private IGenericDataRepository<AccountCreditNoteDetail> _accountCreditNoteDetail;
        private IGenericDataRepository<AppPolicyApplication> _appPolicyApplication;
        private IGenericDataRepository<AppPolicyRole> _appPolicyRole;
        private IGenericDataRepository<AppPolicyUser> _appPolicyUser;
        private IGenericDataRepository<AppPolicyUserRole> _appPolicyUserRole;
        private IGenericDataRepository<PaymentHistory> _paymentHistory;
        private IGenericDataRepository<SampleResult> _sampleResult;
        private IGenericDataRepository<ReturnChemicalContainer> _returnChemicalContainer;
        private IGenericDataRepository<GMSDataCollectionField> _gmsDataCollectionField;
        private IGenericDataRepository<GMSDataCollectionIncomplete> _gmsDataCollectionIncomplete;
        private IGenericDataRepository<SeedlingFrom> _seedlingFrom;
        private IGenericDataRepository<FarmerSeedlingFrom> _farmerSeedlingFrom;
        private IGenericDataRepository<FarmerSuccessor> _farmerSuccessor;
        private IGenericDataRepository<DebtSetup> _debtSetup;
        private IGenericDataRepository<DebtType> _debtType;
        private IGenericDataRepository<DebtSetupAccountInvoice> _debtSetupAccountInvoice;
        private IGenericDataRepository<DebtReceipt> _debtReceipt;
        private IGenericDataRepository<AMCSUKProject> _AMCSUKProjectReceipt;
        private IGenericDataRepository<CPAContainerItem> _cpaContainerItem;
        private IGenericDataRepository<CPAContainerReceive> _cpaContainerReceive;
        private IGenericDataRepository<CPAContainerReturn> _cpaContainerReturn;
        private IGenericDataRepository<ExtraBonu> _extraBonus;

        public UnitOfWork()
        {
            _context = new BuyingSystemEntities();
        }

        public IGenericDataRepository<Area> AreaRepository
        {
            get
            {
                return _areaRepository ?? (_areaRepository = new GenericDataRepository<Area>(_context));
            }
        }

        public IGenericDataRepository<AspNetRole> AspNetRoleRepository
        {
            get
            {
                return _aspNetRole ?? (_aspNetRole = new GenericDataRepository<AspNetRole>(_context));
            }
        }

        public IGenericDataRepository<AspNetUserClaim> AspNetUserClaimRepository
        {
            get
            {
                return _aspNetUserClaim ?? (_aspNetUserClaim = new GenericDataRepository<AspNetUserClaim>(_context));
            }
        }

        public IGenericDataRepository<AspNetUserLogin> AspNetUserLoginRepository
        {
            get
            {
                return _aspNetUserLogin ?? (_aspNetUserLogin = new GenericDataRepository<AspNetUserLogin>(_context));
            }
        }

        public IGenericDataRepository<AspNetUser> AspNetUserRepository
        {
            get
            {
                return _aspNetUser ?? (_aspNetUser = new GenericDataRepository<AspNetUser>(_context));
            }
        }

        public IGenericDataRepository<Bank> BankRepository
        {
            get
            {
                return _bank ?? (_bank = new GenericDataRepository<Bank>(_context));
            }
        }

        public IGenericDataRepository<Buyer> BuyerRepository
        {
            get
            {
                return _buyer ?? (_buyer = new GenericDataRepository<Buyer>(_context));
            }
        }

        public IGenericDataRepository<Buying> BuyingRepository
        {
            get
            {
                return _buying ?? (_buying = new GenericDataRepository<Buying>(_context));
            }
        }

        public IGenericDataRepository<BuyingDocument> BuyingDocumentRepository
        {
            get
            {
                return _buyingDocument ?? (_buyingDocument = new GenericDataRepository<BuyingDocument>(_context));
            }
        }

        public IGenericDataRepository<BuyingGrade> BuyingGradeRepository
        {
            get
            {
                return _buyingGrade ?? (_buyingGrade = new GenericDataRepository<BuyingGrade>(_context));
            }
        }

        public IGenericDataRepository<BuyingStation> BuyingStationRepository
        {
            get
            {
                return _buyingStation ?? (_buyingStation = new GenericDataRepository<BuyingStation>(_context));
            }
        }

        public IGenericDataRepository<Chemical> ChemicalRepository
        {
            get
            {
                return _chemical ?? (_chemical = new GenericDataRepository<Chemical>(_context));
            }
        }

        public IGenericDataRepository<ChemicalCategory> ChemicalCategoryRepository
        {
            get
            {
                return _chemicalCategory ?? (_chemicalCategory = new GenericDataRepository<ChemicalCategory>(_context));
            }
        }

        public IGenericDataRepository<ChemicalDistribution> ChemicalDistributionRepository
        {
            get
            {
                return _chemicalDistribution ?? (_chemicalDistribution = new GenericDataRepository<ChemicalDistribution>(_context));
            }
        }

        public IGenericDataRepository<ChemicalItem> ChemicalItemRepository
        {
            get
            {
                return _chemicalItem ?? (_chemicalItem = new GenericDataRepository<ChemicalItem>(_context));
            }
        }

        public IGenericDataRepository<ChemicalItemType> ChemicalItemTypeRepository
        {
            get
            {
                return _chemicalItemType ?? (_chemicalItemType = new GenericDataRepository<ChemicalItemType>(_context));
            }
        }

        public IGenericDataRepository<ChemicalItemUnit> ChemicalItemUnitRepository
        {
            get
            {
                return _chemicalItemUnit ?? (_chemicalItemUnit = new GenericDataRepository<ChemicalItemUnit>(_context));
            }
        }

        public IGenericDataRepository<ChemicalType> ChemicalTypeRepository
        {
            get
            {
                return _chemicalType ?? (_chemicalType = new GenericDataRepository<ChemicalType>(_context));
            }
        }

        public IGenericDataRepository<Crop> CropRepository
        {
            get
            {
                return _crop ?? (_crop = new GenericDataRepository<Crop>(_context));
            }
        }

        public IGenericDataRepository<CropAnimalForConsumption> CropAnimalForConsumptionRepository
        {
            get
            {
                return _cropAnimalForConsumption ?? (_cropAnimalForConsumption = new GenericDataRepository<CropAnimalForConsumption>(_context));
            }
        }

        public IGenericDataRepository<CropInputCategory> CropInputCategoryRepository
        {
            get
            {
                return _cropInputCategory ?? (_cropInputCategory = new GenericDataRepository<CropInputCategory>(_context));
            }
        }

        public IGenericDataRepository<CropInputDistribution> CropInputDistributionRepository
        {
            get
            {
                return _cropInputDistribution ?? (_cropInputDistribution = new GenericDataRepository<CropInputDistribution>(_context));
            }
        }

        public IGenericDataRepository<CropInputItem> CropInputItemRepository
        {
            get
            {
                return _cropInputItem ?? (_cropInputItem = new GenericDataRepository<CropInputItem>(_context));
            }
        }

        public IGenericDataRepository<CuringBarnInformation> CuringBarnInformationRepository
        {
            get
            {
                return _curingBarnInformation ?? (_curingBarnInformation = new GenericDataRepository<CuringBarnInformation>(_context));
            }
        }

        public IGenericDataRepository<DebtorPayment> DebtorPaymentRepository
        {
            get
            {
                return _debtorPayment ?? (_debtorPayment = new GenericDataRepository<DebtorPayment>(_context));
            }
        }

        public IGenericDataRepository<District> DistrictRepository
        {
            get
            {
                return _district ?? (_district = new GenericDataRepository<District>(_context));
            }
        }

        public IGenericDataRepository<EducationLevel> EducationLevelRepository
        {
            get
            {
                return _educationLevel ?? (_educationLevel = new GenericDataRepository<EducationLevel>(_context));
            }
        }

        public IGenericDataRepository<EstimateTransplantingPeriod> EstimateTransplantingPeriodRepository
        {
            get
            {
                return _estimateTransplantingPeriod ?? (_estimateTransplantingPeriod = new GenericDataRepository<EstimateTransplantingPeriod>(_context));
            }
        }

        public IGenericDataRepository<Farmer> FarmerRepository
        {
            get
            {
                return _farmer ?? (_farmer = new GenericDataRepository<Farmer>(_context));
            }
        }

        public IGenericDataRepository<FarmerChildInformation> FarmerChildInformationRepository
        {
            get
            {
                return _farmerChildInformation ?? (_farmerChildInformation = new GenericDataRepository<FarmerChildInformation>(_context));
            }
        }

        public IGenericDataRepository<FarmerEstimateTransplantingPeriod> FarmerEstimateTransplantingPeriodRepository
        {
            get
            {
                return _farmerEstimateTransplantingPeriod ?? (_farmerEstimateTransplantingPeriod = new GenericDataRepository<FarmerEstimateTransplantingPeriod>(_context));
            }
        }

        public IGenericDataRepository<FarmerMachineAndFarmEquipment> FarmerMachineAndFarmEquipmentRepository
        {
            get
            {
                return _farmerMachineAndFarmEquipment ?? (_farmerMachineAndFarmEquipment = new GenericDataRepository<FarmerMachineAndFarmEquipment>(_context));
            }
        }

        public IGenericDataRepository<FarmerProject> FarmerProjectRepository
        {
            get
            {
                return _farmerProject ?? (_farmerProject = new GenericDataRepository<FarmerProject>(_context));
            }
        }

        public IGenericDataRepository<FarmLandInformation> FarmLandInformationRepository
        {
            get
            {
                return _farmLandInformation ?? (_farmLandInformation = new GenericDataRepository<FarmLandInformation>(_context));
            }
        }

        public IGenericDataRepository<FarmProfile> FarmProfileRepository
        {
            get
            {
                return _farmProfile ?? (_farmProfile = new GenericDataRepository<FarmProfile>(_context));
            }
        }

        public IGenericDataRepository<FertilizerAndChemical> FertilizerAndChemicalRepository
        {
            get
            {
                return _fertilizerAndChemical ?? (_fertilizerAndChemical = new GenericDataRepository<FertilizerAndChemical>(_context));
            }
        }

        public IGenericDataRepository<FertilizerAndChemicalType> FertilizerAndChemicalTypeRepository
        {
            get
            {
                return _fertilizerAndChemicalType ?? (_fertilizerAndChemicalType = new GenericDataRepository<FertilizerAndChemicalType>(_context));
            }
        }

        public IGenericDataRepository<FertilizerAndChemicalUnit> FertilizerAndChemicalUnitRepository
        {
            get
            {
                return _fertilizerAndChemicalUnit ?? (_fertilizerAndChemicalUnit = new GenericDataRepository<FertilizerAndChemicalUnit>(_context));
            }
        }

        public IGenericDataRepository<GAPGroup> GAPGroupRepository
        {
            get
            {
                return _gapGroup ?? (_gapGroup = new GenericDataRepository<GAPGroup>(_context));
            }
        }

        public IGenericDataRepository<GAPGroupLeader> GAPGroupLeaderRepository
        {
            get
            {
                return _gapGroupLeader ?? (_gapGroupLeader = new GenericDataRepository<GAPGroupLeader>(_context));
            }
        }

        public IGenericDataRepository<GAPGroupLeaderByCrop> GAPGroupLeaderByCropRepository
        {
            get
            {
                return _gapGroupLeaderByCrop ?? (_gapGroupLeaderByCrop = new GenericDataRepository<GAPGroupLeaderByCrop>(_context));
            }
        }

        public IGenericDataRepository<HistoryBackBuying> HistoryBackBuyingRepository
        {
            get
            {
                return _historyBackBuying ?? (_historyBackBuying = new GenericDataRepository<HistoryBackBuying>(_context));
            }
        }

        public IGenericDataRepository<HistoryBuying> HistoryBuyingRepository
        {
            get
            {
                return _historyBuying ?? (_historyBuying = new GenericDataRepository<HistoryBuying>(_context));
            }
        }

        public IGenericDataRepository<InputFactor> InputFactorRepository
        {
            get
            {
                return _inputFactor ?? (_inputFactor = new GenericDataRepository<InputFactor>(_context));
            }
        }

        public IGenericDataRepository<IrrigationWater> IrrigationWaterRepository
        {
            get
            {
                return _irrigationWater ?? (_irrigationWater = new GenericDataRepository<IrrigationWater>(_context));
            }
        }

        public IGenericDataRepository<IrrigationWaterType> IrrigationWaterTypeRepository
        {
            get
            {
                return _irrigationWaterType ?? (_irrigationWaterType = new GenericDataRepository<IrrigationWaterType>(_context));
            }
        }

        public IGenericDataRepository<KilogramsPerRaiConfiguration> KilogramsPerRaiConfigurationRepository
        {
            get
            {
                return _kilogramsPerRaiConfiguration ?? (_kilogramsPerRaiConfiguration = new GenericDataRepository<KilogramsPerRaiConfiguration>(_context));
            }
        }

        public IGenericDataRepository<MachineAndFarmEquipment> MachineAndFarmEquipmentRepository
        {
            get
            {
                return _machineAndFarmEquipment ?? (_machineAndFarmEquipment = new GenericDataRepository<MachineAndFarmEquipment>(_context));
            }
        }

        public IGenericDataRepository<MaritalStatu> MaritalStatuRepository
        {
            get
            {
                return _maritalStatu ?? (_maritalStatu = new GenericDataRepository<MaritalStatu>(_context));
            }
        }

        public IGenericDataRepository<MasterGrade> MasterGradeRepository
        {
            get
            {
                return _masterGrade ?? (_masterGrade = new GenericDataRepository<MasterGrade>(_context));
            }
        }

        //public IGenericDataRepository<Material> MaterialRepository
        //{
        //    get
        //    {
        //        return _material ?? (_material = new GenericDataRepository<Material>(_context));
        //    }
        //}

        public IGenericDataRepository<NTRMInspection> NTRMInspectionRepository
        {
            get
            {
                return _ntrmInspection ?? (_ntrmInspection = new GenericDataRepository<NTRMInspection>(_context));
            }
        }

        public IGenericDataRepository<NTRMSample> NTRMSampleRepository
        {
            get
            {
                return _ntrmSample ?? (_ntrmSample = new GenericDataRepository<NTRMSample>(_context));
            }
        }

        public IGenericDataRepository<NTRMType> NTRMTypeRepository
        {
            get
            {
                return _ntrmType ?? (_ntrmType = new GenericDataRepository<NTRMType>(_context));
            }
        }

        public IGenericDataRepository<OtherCropProduction> OtherCropProductionRepository
        {
            get
            {
                return _otherCropProduction ?? (_otherCropProduction = new GenericDataRepository<OtherCropProduction>(_context));
            }
        }

        public IGenericDataRepository<PaymentForInputFactor> PaymentForInputFactorRepository
        {
            get
            {
                return _paymentForInputFactor ?? (_paymentForInputFactor = new GenericDataRepository<PaymentForInputFactor>(_context));
            }
        }

        public IGenericDataRepository<PaymentForSeedAndMedia> PaymentForSeedAndMediaRepository
        {
            get
            {
                return _paymentForSeedAndMedia ?? (_paymentForSeedAndMedia = new GenericDataRepository<PaymentForSeedAndMedia>(_context));
            }
        }

        public IGenericDataRepository<Person> PersonRepository
        {
            get
            {
                return _person ?? (_person = new GenericDataRepository<Person>(_context));
            }
        }

        public IGenericDataRepository<PersonSpouse> PersonSpouseRepository
        {
            get
            {
                return _personSpouse ?? (_personSpouse = new GenericDataRepository<PersonSpouse>(_context));
            }
        }

        public IGenericDataRepository<PricingSet> PricingSetRepository
        {
            get
            {
                return _pricingSet ?? (_pricingSet = new GenericDataRepository<PricingSet>(_context));
            }
        }

        public IGenericDataRepository<ProductionFactor> ProductionFactorRepository
        {
            get
            {
                return _productionFactor ?? (_productionFactor = new GenericDataRepository<ProductionFactor>(_context));
            }
        }

        public IGenericDataRepository<ProductionFactorDetail> ProductionFactorDetailRepository
        {
            get
            {
                return _productionFactorDetail ?? (_productionFactorDetail = new GenericDataRepository<ProductionFactorDetail>(_context));
            }
        }

        public IGenericDataRepository<ProjectType> ProjectTypeRepository
        {
            get
            {
                return _projectType ?? (_projectType = new GenericDataRepository<ProjectType>(_context));
            }
        }

        public IGenericDataRepository<Province> ProvinceRepository
        {
            get
            {
                return _province ?? (_province = new GenericDataRepository<Province>(_context));
            }
        }

        public IGenericDataRepository<ReceiveChemical> ReceiveChemicalRepository
        {
            get
            {
                return _receiveChemical ?? (_receiveChemical = new GenericDataRepository<ReceiveChemical>(_context));
            }
        }

        public IGenericDataRepository<ReceiveFertilizerAndChemical> ReceiveFertilizerAndChemicalRepository
        {
            get
            {
                return _receiveFertilizerAndChemical ?? (_receiveFertilizerAndChemical = new GenericDataRepository<ReceiveFertilizerAndChemical>(_context));
            }
        }

        public IGenericDataRepository<ReceiveInputFactor> ReceiveInputFactorRepository
        {
            get
            {
                return _receiveInputFactor ?? (_receiveInputFactor = new GenericDataRepository<ReceiveInputFactor>(_context));
            }
        }

        public IGenericDataRepository<ReceiveProductionFactor> ReceiveProductionFactorRepository
        {
            get
            {
                return _receiveProductionFactor ?? (_receiveProductionFactor = new GenericDataRepository<ReceiveProductionFactor>(_context));
            }
        }

        public IGenericDataRepository<ReceiveSeedAndMedia> ReceiveSeedAndMediaRepository
        {
            get
            {
                return _receiveSeedAndMedia ?? (_receiveSeedAndMedia = new GenericDataRepository<ReceiveSeedAndMedia>(_context));
            }
        }

        public IGenericDataRepository<RegistrationFarmer> RegistrationFarmerRepository
        {
            get
            {
                return _registrationFarmer ?? (_registrationFarmer = new GenericDataRepository<RegistrationFarmer>(_context));
            }
        }

        public IGenericDataRepository<ReturnChemical> ReturnChemicalRepository
        {
            get
            {
                return _returnChemical ?? (_returnChemical = new GenericDataRepository<ReturnChemical>(_context));
            }
        }

        public IGenericDataRepository<ReturnCPAContainer> ReturnCPAContainerRepository
        {
            get
            {
                return _returnCPAContainer ?? (_returnCPAContainer = new GenericDataRepository<ReturnCPAContainer>(_context));
            }
        }

        public IGenericDataRepository<ReturnFertilizerAndChemical> ReturnFertilizerAndChemicalRepository
        {
            get
            {
                return _returnFertilizerAndChemical ?? (_returnFertilizerAndChemical = new GenericDataRepository<ReturnFertilizerAndChemical>(_context));
            }
        }

        public IGenericDataRepository<Role> RoleRepository
        {
            get
            {
                return _role ?? (_role = new GenericDataRepository<Role>(_context));
            }
        }

        public IGenericDataRepository<SeedAndMedia> SeedAndMediaRepository
        {
            get
            {
                return _seedAndMedia ?? (_seedAndMedia = new GenericDataRepository<SeedAndMedia>(_context));
            }
        }

        public IGenericDataRepository<SeedAndMediaType> SeedAndMediaTypeRepository
        {
            get
            {
                return _seedAndMediaType ?? (_seedAndMediaType = new GenericDataRepository<SeedAndMediaType>(_context));
            }
        }

        public IGenericDataRepository<SeedAndMediaUnit> SeedAndMediaUnitRepository
        {
            get
            {
                return _seedAndMediaUnit ?? (_seedAndMediaUnit = new GenericDataRepository<SeedAndMediaUnit>(_context));
            }
        }

        public IGenericDataRepository<SeedDistribution> SeedDistributionRepository
        {
            get
            {
                return _seedDistribution ?? (_seedDistribution = new GenericDataRepository<SeedDistribution>(_context));
            }
        }

        public IGenericDataRepository<SeedDistributionType> SeedDistributionTypeRepository
        {
            get
            {
                return _seedDistributionType ?? (_seedDistributionType = new GenericDataRepository<SeedDistributionType>(_context));
            }
        }

        public IGenericDataRepository<SeedForDistribution> SeedForDistributionRepository
        {
            get
            {
                return _seedForDistribution ?? (_seedForDistribution = new GenericDataRepository<SeedForDistribution>(_context));
            }
        }

        public IGenericDataRepository<SeedVariety> SeedVarietyRepository
        {
            get
            {
                return _seedVariety ?? (_seedVariety = new GenericDataRepository<SeedVariety>(_context));
            }
        }

        public IGenericDataRepository<StaffUser> StaffUserRepository
        {
            get
            {
                return _staffUser ?? (_staffUser = new GenericDataRepository<StaffUser>(_context));
            }
        }

        public IGenericDataRepository<SubDistrict> SubDistrictRepository
        {
            get
            {
                return _subDistrict ?? (_subDistrict = new GenericDataRepository<SubDistrict>(_context));
            }
        }

        public IGenericDataRepository<SupervisorUser> SupervisorUserRepository
        {
            get
            {
                return _supervisorUser ?? (_supervisorUser = new GenericDataRepository<SupervisorUser>(_context));
            }
        }

        public IGenericDataRepository<Supplier> SupplierRepository
        {
            get
            {
                return _supplier ?? (_supplier = new GenericDataRepository<Supplier>(_context));
            }
        }

        public IGenericDataRepository<SupplierUser> SupplierUserRepository
        {
            get
            {
                return _supplierUser ?? (_supplierUser = new GenericDataRepository<SupplierUser>(_context));
            }
        }

        public IGenericDataRepository<SystemDeadlineSetting> SystemDeadlineSettingRepository
        {
            get
            {
                return _systemDeadlineSetting ?? (_systemDeadlineSetting = new GenericDataRepository<SystemDeadlineSetting>(_context));
            }
        }

        public IGenericDataRepository<TransportationDocument> TransportationDocumentRepository
        {
            get
            {
                return _transportationDocument ?? (_transportationDocument = new GenericDataRepository<TransportationDocument>(_context));
            }
        }

        public IGenericDataRepository<TreeSeedingType> TreeSeedingTypeRepository
        {
            get
            {
                return _treeSeedingType ?? (_treeSeedingType = new GenericDataRepository<TreeSeedingType>(_context));
            }
        }

        public IGenericDataRepository<TreeSeedlingWanted> TreeSeedlingWantedRepository
        {
            get
            {
                return _treeSeedlingWanted ?? (_treeSeedlingWanted = new GenericDataRepository<TreeSeedlingWanted>(_context));
            }
        }

        public IGenericDataRepository<UserAccount> UserAccountRepository
        {
            get
            {
                return _userAccount ?? (_userAccount = new GenericDataRepository<UserAccount>(_context));
            }
        }

        public IGenericDataRepository<UserRole> UserRoleRepository
        {
            get
            {
                return _userRole ?? (_userRole = new GenericDataRepository<UserRole>(_context));
            }
        }

        public IGenericDataRepository<CPASampleCollection> CPASampleCollectionRepository
        {
            get
            {
                return _cpaSampleCollection ?? (_cpaSampleCollection = new GenericDataRepository<CPASampleCollection>(_context));
            }
        }

        public IGenericDataRepository<CreditProject> CreditProjectRepository
        {
            get
            {
                return _creditProject ?? (_creditProject = new GenericDataRepository<CreditProject>(_context));
            }
        }

        public IGenericDataRepository<FarmerCreditProject> FarmerCreditProjectRepository
        {
            get
            {
                return _farmerCreditProject ?? (_farmerCreditProject = new GenericDataRepository<FarmerCreditProject>(_context));
            }
        }

        public IGenericDataRepository<GMSFarmerInfoCollectionCriteria> GMSFarmerInfoCollectionCriteriaRepository
        {
            get
            {
                return _gmsFarmerInfoCollectionCriteriap ?? (_gmsFarmerInfoCollectionCriteriap = new GenericDataRepository<GMSFarmerInfoCollectionCriteria>(_context));
            }
        }

        public IGenericDataRepository<CPASampleCollection2020> CPASampleCollection2020Repository
        {
            get
            {
                return _cpaSampleCollection2020 ?? (_cpaSampleCollection2020 = new GenericDataRepository<CPASampleCollection2020>(_context));
            }
        }

        public IGenericDataRepository<GAPGroup2020> GAPGroup2020Repository
        {
            get
            {
                return _gapGroup2020 ?? (_gapGroup2020 = new GenericDataRepository<GAPGroup2020>(_context));
            }
        }

        public IGenericDataRepository<GAPGroupMember2020> GAPGroupMember2020Repository
        {
            get
            {
                return _gapGroupMember2020 ?? (_gapGroupMember2020 = new GenericDataRepository<GAPGroupMember2020>(_context));
            }
        }

        public IGenericDataRepository<Warehouse> WarehouseRepository
        {
            get
            {
                return _warehouse ?? (_warehouse = new GenericDataRepository<Warehouse>(_context));
            }
        }

        public IGenericDataRepository<HessianReceiving> HessianReceivingRepository
        {
            get
            {
                return _hessianReceiving ?? (_hessianReceiving = new GenericDataRepository<HessianReceiving>(_context));
            }
        }

        public IGenericDataRepository<HessianDistribution> HessianDistributionRepository
        {
            get
            {
                return _hessianDistribution ?? (_hessianDistribution = new GenericDataRepository<HessianDistribution>(_context));
            }
        }

        public IGenericDataRepository<IncidentCategory> IncidentCategoryRepository
        {
            get
            {
                return _incidentCategory ?? (_incidentCategory = new GenericDataRepository<IncidentCategory>(_context));
            }
        }

        public IGenericDataRepository<IncidentTopic> IncidentTopicRepository
        {
            get
            {
                return _incidentTopic ?? (_incidentTopic = new GenericDataRepository<IncidentTopic>(_context));
            }
        }

        public IGenericDataRepository<IncidentDescription> IncidentDescriptionRepository
        {
            get
            {
                return _incidentDescription ?? (_incidentDescription = new GenericDataRepository<IncidentDescription>(_context));
            }
        }

        public IGenericDataRepository<IncidentFarmer> IncidentFarmerRepository
        {
            get
            {
                return _incidentFarmer ?? (_incidentFarmer = new GenericDataRepository<IncidentFarmer>(_context));
            }
        }

        public IGenericDataRepository<IncidentFollowUp> IncidentFollowUpRepository
        {
            get
            {
                return _incidentFollowUp ?? (_incidentFollowUp = new GenericDataRepository<IncidentFollowUp>(_context));
            }
        }

        public IGenericDataRepository<Invoice> InvoiceRepository
        {
            get
            {
                return _invoice ?? (_invoice = new GenericDataRepository<Invoice>(_context));
            }
        }

        public IGenericDataRepository<InvoiceDetail> InvoiceDetailRepository
        {
            get
            {
                return _invoiceDetail ?? (_invoiceDetail = new GenericDataRepository<InvoiceDetail>(_context));
            }
        }

        public IGenericDataRepository<CropInputsPackage> CropInputsPackageRepository
        {
            get
            {
                return _cropInputsPackage ?? (_cropInputsPackage = new GenericDataRepository<CropInputsPackage>(_context));
            }
        }

        public IGenericDataRepository<CropInputsPackageDetail> CropInputsPackageDetailsRepository
        {
            get
            {
                return _cropInputsPackageDetail ?? (_cropInputsPackageDetail = new GenericDataRepository<CropInputsPackageDetail>(_context));
            }
        }

        public IGenericDataRepository<CropInputsCondition> CropInputsConditionRepository
        {
            get
            {
                return _cropInputsCondition ?? (_cropInputsCondition = new GenericDataRepository<CropInputsCondition>(_context));
            }
        }

        public IGenericDataRepository<AccountInvoice> AccountInvoiceRepository
        {
            get
            {
                return _accountInvoice ?? (_accountInvoice = new GenericDataRepository<AccountInvoice>(_context));
            }
        }

        public IGenericDataRepository<AppPolicyApplication> AppPolicyApplicationRepository
        {
            get
            {
                return _appPolicyApplication ?? (_appPolicyApplication = new GenericDataRepository<AppPolicyApplication>(_context));
            }
        }

        public IGenericDataRepository<AppPolicyRole> AppPolicyRoleRepository
        {
            get
            {
                return _appPolicyRole ?? (_appPolicyRole = new GenericDataRepository<AppPolicyRole>(_context));
            }
        }

        public IGenericDataRepository<AppPolicyUser> AppPolicyUserRepository
        {
            get
            {
                return _appPolicyUser ?? (_appPolicyUser = new GenericDataRepository<AppPolicyUser>(_context));
            }
        }

        public IGenericDataRepository<AppPolicyUserRole> AppPolicyUserRoleRepository
        {
            get
            {
                return _appPolicyUserRole ?? (_appPolicyUserRole = new GenericDataRepository<AppPolicyUserRole>(_context));
            }
        }

        public IGenericDataRepository<PaymentHistory> PaymentHistoryRepository
        {
            get
            {
                return _paymentHistory ?? (_paymentHistory = new GenericDataRepository<PaymentHistory>(_context));
            }
        }

        public IGenericDataRepository<SampleResult> SampleResultRepository
        {
            get
            {
                return _sampleResult ?? (_sampleResult = new GenericDataRepository<SampleResult>(_context));
            }
        }

        public IGenericDataRepository<ReturnChemicalContainer> ReturnChemicalContainerRepository
        {
            get
            {
                return _returnChemicalContainer ?? (_returnChemicalContainer = new GenericDataRepository<ReturnChemicalContainer>(_context));
            }
        }

        public IGenericDataRepository<GMSDataCollectionField> GMSDataCollectionFieldRepository
        {
            get
            {
                return _gmsDataCollectionField ?? (_gmsDataCollectionField = new GenericDataRepository<GMSDataCollectionField>(_context));
            }
        }

        public IGenericDataRepository<GMSDataCollectionIncomplete> GMSDataCollectionIncompleteRepository
        {
            get
            {
                return _gmsDataCollectionIncomplete ?? (_gmsDataCollectionIncomplete = new GenericDataRepository<GMSDataCollectionIncomplete>(_context));
            }
        }

        public IGenericDataRepository<AccountCreditNote> AccountCreditNoteRepository
        {
            get
            {
                return _accountCreditNote ?? (_accountCreditNote = new GenericDataRepository<AccountCreditNote>(_context));
            }
        }

        public IGenericDataRepository<AccountCreditNoteDetail> AccountCreditNoteDetailRepository
        {
            get
            {
                return _accountCreditNoteDetail ?? (_accountCreditNoteDetail = new GenericDataRepository<AccountCreditNoteDetail>(_context));
            }
        }

        public IGenericDataRepository<CY202201ReturnItem> CY202201ReturnItemRepository
        {
            get
            {
                return _cy202201ReturnItem ?? (_cy202201ReturnItem = new GenericDataRepository<CY202201ReturnItem>(_context));
            }
        }

        public IGenericDataRepository<CY202201Return> CY202201ReturnRepository
        {
            get
            {
                return _cy202201Return ?? (_cy202201Return = new GenericDataRepository<CY202201Return>(_context));
            }
        }

        public IGenericDataRepository<CY202201ReturnDetail> CY202201ReturnDetailRepository
        {
            get
            {
                return _cy202201ReturnDetail ?? (_cy202201ReturnDetail = new GenericDataRepository<CY202201ReturnDetail>(_context));
            }
        }

        public IGenericDataRepository<CY202201CreditNoteCode> CY202201CreditNoteCodeRepository
        {
            get
            {
                return _cy202201CreditNoteCode ?? (_cy202201CreditNoteCode = new GenericDataRepository<CY202201CreditNoteCode>(_context));
            }
        }

        public IGenericDataRepository<SeedlingFrom> SeedlingFromRepository
        {
            get
            {
                return _seedlingFrom ?? (_seedlingFrom = new GenericDataRepository<SeedlingFrom>(_context));
            }
        }

        public IGenericDataRepository<FarmerSeedlingFrom> FarmerSeedlingFromRepository
        {
            get
            {
                return _farmerSeedlingFrom ?? (_farmerSeedlingFrom = new GenericDataRepository<FarmerSeedlingFrom>(_context));
            }
        }

        public IGenericDataRepository<FarmerSuccessor> FarmerSuccessorRepository
        {
            get
            {
                return _farmerSuccessor ?? (_farmerSuccessor = new GenericDataRepository<FarmerSuccessor>(_context));
            }
        }

        public IGenericDataRepository<DebtSetup> DebtSetupRepository
        {
            get
            {
                return _debtSetup ?? (_debtSetup = new GenericDataRepository<DebtSetup>(_context));
            }
        }

        public IGenericDataRepository<DebtSetupAccountInvoice> DebtSetupAccountInvoiceRepository
        {
            get
            {
                return _debtSetupAccountInvoice ?? (_debtSetupAccountInvoice = new GenericDataRepository<DebtSetupAccountInvoice>(_context));
            }
        }

        public IGenericDataRepository<DebtType> DebtTypeRepository
        {
            get
            {
                return _debtType ?? (_debtType = new GenericDataRepository<DebtType>(_context));
            }
        }

        public IGenericDataRepository<DebtReceipt> DebtReceiptRepository
        {
            get
            {
                return _debtReceipt ?? (_debtReceipt = new GenericDataRepository<DebtReceipt>(_context));
            }
        }

        public IGenericDataRepository<AMCSUKProject> AMCSUKProjectRepository
        {
            get
            {
                return _AMCSUKProjectReceipt ?? (_AMCSUKProjectReceipt = new GenericDataRepository<AMCSUKProject>(_context));
            }
        }

        public IGenericDataRepository<CPAContainerItem> CPAContainerItemRepository
        {
            get
            {
                return _cpaContainerItem ?? (_cpaContainerItem = new GenericDataRepository<CPAContainerItem>(_context));
            }
        }

        public IGenericDataRepository<CPAContainerReceive> CPAContainerReceiveRepository
        {
            get
            {
                return _cpaContainerReceive ?? (_cpaContainerReceive = new GenericDataRepository<CPAContainerReceive>(_context));
            }
        }

        public IGenericDataRepository<CPAContainerReturn> CPAContainerReturnRepository
        {
            get
            {
                return _cpaContainerReturn ?? (_cpaContainerReturn = new GenericDataRepository<CPAContainerReturn>(_context));
            }
        }

        public IGenericDataRepository<ExtraBonu> ExtraBonusRepository
        {
            get
            {
                return _extraBonus ?? (_extraBonus = new GenericDataRepository<ExtraBonu>(_context));
            }
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }
    }
}