﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;
using DataAccessLayer.EDMX;
using System.Security;

namespace DataAccessLayer
{
    public static class StoreProcedureRepository
    {
        public static List<sp_GetCPASampleCollectionByGapGroup_Result> GetGetCPASampleCollectionByGapGroup(string gapGroupCode)
        {
            try
            {
                using (BuyingSystemEntities _context = new BuyingSystemEntities())
                {
                    return _context.sp_GetCPASampleCollectionByGapGroup(gapGroupCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<sp_CY2018GetDebtorPaymentByFarmer_Result> sp_CY2018GetDebtorPaymentByFarmer(short crop, string farmerCode)
        {
            try
            {
                using (BuyingSystemEntities _context = new BuyingSystemEntities())
                {
                    return _context.sp_CY2018GetDebtorPaymentByFarmer(crop, farmerCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<sp_CY2018GetDebtorPaymentByVoucher_Result> sp_CY2018GetDebtorPaymentByVoucher(short voucherCrop,
            string voucherFarmerCode,
            string voucherBuyingStationCode,
            short voucherBuyingDocumentNumber)
        {
            try
            {
                using (BuyingSystemEntities _context = new BuyingSystemEntities())
                {
                    return _context.sp_CY2018GetDebtorPaymentByVoucher(voucherCrop,
                        voucherFarmerCode,
                        voucherBuyingStationCode,
                        voucherBuyingDocumentNumber).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<sp_CY2018GetPPEKitsDistributionByFarmer_Result> sp_CY2018GetPPEKitsDistributionByFarmer(short crop, string farmerCode)
        {
            try
            {
                using (BuyingSystemEntities _context = new BuyingSystemEntities())
                {
                    return _context.sp_CY2018GetPPEKitsDistributionByFarmer(crop, farmerCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<sp_CY2018GetSeedDistributionByFarmer_Result> sp_CY2018GetSeedDistributionByFarmer(short crop, string farmerCode)
        {
            try
            {
                using (BuyingSystemEntities _context = new BuyingSystemEntities())
                {
                    return _context.sp_CY2018GetSeedDistributionByFarmer(crop, farmerCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<sp_GetBuyingDetailsByBuyingDocument_Result> sp_GetBuyingDetailsByBuyingDocument(short crop,
            string farmerCode,
            string stationCode,
            short documentNumber)
        {
            try
            {
                using (BuyingSystemEntities _context = new BuyingSystemEntities())
                {
                    return _context.sp_GetBuyingDetailsByBuyingDocument(crop,
                        stationCode,
                        farmerCode,
                        documentNumber).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<sp_GetBuyingDetailsByFarmer_Result> sp_GetBuyingDetailsByFarmer(short crop, string farmerCode)
        {
            try
            {
                using (BuyingSystemEntities _context = new BuyingSystemEntities())
                {
                    return _context.sp_GetBuyingDetailsByFarmer(crop, farmerCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<sp_GetBuyingDocumentByBuyingDocument_Result> sp_GetBuyingDocumentByBuyingDocument(short crop,
            string buyingStationCode,
            string farmerCode,
            short buyingDocumentNumber)
        {
            try
            {
                using (BuyingSystemEntities _context = new BuyingSystemEntities())
                {
                    return _context.sp_GetBuyingDocumentByBuyingDocument(crop,
                        buyingStationCode,
                        farmerCode,
                        buyingDocumentNumber).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<sp_GetCalculationPercentOfSold_Result> sp_GetCalculationPercentOfSold(short crop, string farmerCode)
        {
            try
            {
                using (BuyingSystemEntities _context = new BuyingSystemEntities())
                {
                    return _context.sp_GetCalculationPercentOfSold(crop, farmerCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<sp_GetFarmerQuotaFixedAndSold_Result> sp_GetFarmerQuotaFixedAndSold(short crop, string farmerCode)
        {
            try
            {
                using (BuyingSystemEntities _context = new BuyingSystemEntities())
                {
                    return _context.sp_GetFarmerQuotaFixedAndSold(crop, farmerCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<sp_GetTransportationDocumentByTransportationCode_Result> sp_GetTransportationDocumentByTransportationCode(string transportationDocumentCode)
        {
            try
            {
                using (BuyingSystemEntities _context = new BuyingSystemEntities())
                {
                    return _context.sp_GetTransportationDocumentByTransportationCode(transportationDocumentCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<sp_GetTransportationDetailsByTransportationCode_Result> sp_GetTransportationDetailsByTransportationCode(string transportationDocumentCode)
        {
            try
            {
                using (BuyingSystemEntities _context = new BuyingSystemEntities())
                {
                    return _context.sp_GetTransportationDetailsByTransportationCode(transportationDocumentCode).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<sp_GetMoistureResultDetails_Result> sp_GetMoistureResultDetails()
        {
            try
            {
                using (BuyingSystemEntities _context = new BuyingSystemEntities())
                {
                    return _context.sp_GetMoistureResultDetails().ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string sp_ConvertCurrencyToThaiBath(decimal currency)
        {
            try
            {
                using (var context = new BuyingSystemEntities())
                {
                    return context.sp_ConvertCurrencyToThaiBath(currency).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<sp_Receiving_SEL_SummaryBuyingAndReceiving_Result> sp_Receiving_SEL_SummaryBuyingAndReceiving(DateTime buyingDate, string supplierCode, bool confirmStatus)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_Receiving_SEL_SummaryBuyingAndReceiving(buyingDate, supplierCode, confirmStatus).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
