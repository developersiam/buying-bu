﻿/// <reference path="modernizr-2.8.3.js" />
/// <reference path="jquery-3.6.3.js" />
/// <autosync enabled="true" />
/// <reference path="../plugins/datatables/js/datatables.foundation.min.js" />
/// <reference path="../plugins/datatables/js/datatables.jqueryui.min.js" />
/// <reference path="../plugins/datatables/js/datatables.semanticui.min.js" />
/// <reference path="../plugins/datatables/js/jquery.datatables.min.js" />
/// <reference path="bootstrap.bundle.min.js" />
/// <reference path="bootstrap.min.js" />
/// <reference path="esm/popper.min.js" />
/// <reference path="esm/popper-utils.min.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-3.0.0.min.js" />
/// <reference path="jquery-3.0.0.slim.min.js" />
/// <reference path="jquery-3.2.1.min.js" />
/// <reference path="jquery-3.2.1.slim.min.js" />
/// <reference path="popper.min.js" />
/// <reference path="popper-utils.min.js" />
/// <reference path="respond.js" />
/// <reference path="scanner/decoderworker.js" />
/// <reference path="scanner/qrcodelib.js" />
/// <reference path="scanner/quagga.min.js" />
/// <reference path="scanner/webcodecamjquery.js" />
/// <reference path="scanner/webcodecamjs.js" />
/// <reference path="tether.min.js" />
/// <reference path="umd/popper.min.js" />
/// <reference path="umd/popper-utils.min.js" />
