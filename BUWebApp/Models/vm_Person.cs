﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BUWebApp.Models
{
    public class vm_Person
    {
        [Required(ErrorMessage = "CitizenID required")]

        [RegularExpression("^[0-9]{13}$", ErrorMessage = "The value must be 13 digits numeric")]
        public string CitizenID { get; set; }
        [Required(ErrorMessage = "Prefix required")]
        public string Prefix { get; set; }
        [Required(ErrorMessage = "First name required")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Last name required")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Birth date required")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime BirthDate { get; set; }
        [Required(ErrorMessage = "House number required")]
        public string HouseNumber { get; set; }
        [Required(ErrorMessage = "Village required")]
        public string Village { get; set; }
        [Required(ErrorMessage = "Tumbon required")]
        public string Tumbon { get; set; }
        [Required(ErrorMessage = "Amphur required")]
        public string Amphur { get; set; }
        [Required(ErrorMessage = "Province required")]
        public string Province { get; set; }
        [RegularExpression("^[0-9]{10}$", ErrorMessage = "The value must be 10 digits numeric")]
        public string TelephoneNumber { get; set; }
        [Required(ErrorMessage = "Marital status required")]
        public string MaritalStatusCode { get; set; }
        public string SpouseCitizenID { get; set; }
        public string SpousePrefix { get; set; }
        public string SpouseFirstName { get; set; }
        public string SpouseLastName { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> SpouseBirthDate { get; set; }
        [Range(0, short.MaxValue, ErrorMessage = "The value must be start from 0")]

        [RegularExpression("^[0-9]+$", ErrorMessage = "The value must be numeric")]
        public Nullable<short> YearsInAgriculture { get; set; }
        [Range(0, short.MaxValue, ErrorMessage = "The value must be start from 0")]

        [RegularExpression("^[0-9]+$", ErrorMessage = "The value must be numeric")]
        public Nullable<short> YearsGrowingTobacco { get; set; }
        public short StartAgriculture { get; set; }
        public short StartGrowingTobacco { get; set; }
        public string ExtensionAgentCode { get; set; }
    }
}