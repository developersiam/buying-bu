﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DomainModel;

namespace BUWebApp.Models
{
    public class vm_RegistrationFarmer
    {
        [Required(ErrorMessage = "Crop required")]
        public short Crop { get; set; }
        [Required(ErrorMessage = "Farmer code required")]
        [StringLength(7)]
        public string FarmerCode { get; set; }
        public string GAPGroupCode { get; set; }
        [Required(ErrorMessage = "Register date required")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime RegistrationDate { get; set; }
        public bool ActiveStatus { get; set; }
        public bool SellingStatus { get; set; }
        public byte EstimatRai { get; set; }
        public bool ReceiveCPABox { get; set; }
        public System.DateTime ReceiveCPABoxDate { get; set; }
        public string GAPContractCode { get; set; }
        public bool GAPContract { get; set; }
        public System.DateTime GAPContractDate { get; set; }
        public bool ReceiveSeed { get; set; }
        public System.DateTime ReceiveSeedDate { get; set; }
        public Nullable<int> BankID { get; set; }
        public string BookBank { get; set; }
        public string BankBranch { get; set; }
        public string BankBranchCode { get; set; }
        public string UnRegisterReason { get; set; }
        //[Required(ErrorMessage = "PreviousCropTotalRai required")]
        public Nullable<short> PreviousCropTotalRai { get; set; }
        //[Required(ErrorMessage = "PreviousCropTotalVolume required")]
        public Nullable<int> PreviousCropTotalVolume { get; set; }
        //[Required(ErrorMessage = "TTMQuotaStatus required")]
        public bool TTMQuotaStatus { get; set; }
        //[Required(ErrorMessage = "TTMQuotaKg required")]
        public Nullable<int> TTMQuotaKg { get; set; }
        public string TTMQuotaStation { get; set; }
        //[Required(ErrorMessage = "QuotaFromFarmerRequest required")]
        public Nullable<short> QuotaFromFarmerRequest { get; set; }
        public Nullable<short> QuotaFromSupplierApprove { get; set; }
        public Nullable<short> QuotaFromSTECApprove { get; set; }
        public Nullable<short> QuotaFromSignContract { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public Nullable<DateTime> SignContractDate { get; set; }
        public bool ApproveFromSupplierStatus { get; set; }
        public bool ApproveFromSTECStatus { get; set; }
        public System.DateTime LastModified { get; set; }
        public string ModifiedByUser { get; set; }
        public Person Person { get; set; }
        public string SupplierCode { get; set; }
        public bool HasSuccessor { get; set; }
    }
}