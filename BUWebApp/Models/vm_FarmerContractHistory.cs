﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainModel;

namespace BUWebApp.Models
{
    public class vm_FarmerContractHistory : RegistrationFarmer
    {
        public int Fixed { get; set; }
        public decimal Sold { get; set; }
    }
}
