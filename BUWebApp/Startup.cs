﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BUWebApp.Startup))]
namespace BUWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
