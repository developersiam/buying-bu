﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.AccountReceivable.Data
{
    public class m_FarmerDebtSetupSummary
    {
        public string FarmerCode { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Invoice Invoice { get; set; }
        public decimal InvoiceAmount { get; set; }
        public decimal CreditNoteAmount { get; set; }
        public decimal InvoiceBalanceAmount { get; set; }
        public decimal DebtSetupAmount { get; set; }
        public decimal WaitForSetupAmount { get; set; }
    }
}