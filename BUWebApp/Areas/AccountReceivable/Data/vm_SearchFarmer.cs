﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.AccountReceivable.Data
{
    public class vm_SearchFarmer
    {
        [Required(ErrorMessage = "โปรดระบุรายชื่อชาวไร่ที่ต้องการค้นหา")]
        public string FarmerName { get; set; }
        [Required(ErrorMessage = "โปรดระบุตัวแทน")]
        public string ExtensionAgentCode { get; set; }
    }
}