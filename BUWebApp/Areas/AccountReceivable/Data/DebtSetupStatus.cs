﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.AccountReceivable.Data
{
    public class DebtSetupStatus
    {
        public string StatusNameEN { get; set; }
        public string StatusNameTH { get; set; }
    }
}