﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.AccountReceivable.Data
{
    public class vm_AddDebtSetup
    {
        public short Crop { get; set; }

        [Required(ErrorMessage = "โปรดระบุรหัสชาวไร่เพื่อตรวจสอบข้อมูลก่อนบันทึกการตั้งหนี้")]
        public string FarmerCode { get; set; }

        public double Amount { get; set; }

        [Required(ErrorMessage = "โปรดระบุประเภทของการตั้งหนี้")]
        public string DebtTypeCode { get; set; }

        public int DripProjectQuota { get; set; }

        public int NumberOfDeductionCrop { get; set; }

        public decimal DeductionPercentOfQuota { get; set; }

        [Required(ErrorMessage = "โปรดระบุวันที่บันทึกข้อมูล")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreateDate { get; set; }

        public string CreateBy { get; set; }
    }
}