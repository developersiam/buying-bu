﻿using BusinessLayer.Model;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.AccountReceivable.Data
{
    public class m_InvoiceDebtSummary
    {
        public List<AccountInvoice> AccountInvoices { get; set; }
        public List<m_InvoiceDetails> AccountInvoiceDetails { get; set; }
        public List<AccountCreditNote> CreditNotes { get; set; }
        public List<m_CreditNoteDetail> CreditNoteDetails { get; set; }
    }
}