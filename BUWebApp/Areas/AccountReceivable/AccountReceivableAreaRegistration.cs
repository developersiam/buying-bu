﻿using System.Web.Mvc;

namespace BUWebApp.Areas.AccountReceivable
{
    public class AccountReceivableAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "AccountReceivable";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "AccountReceivable_default",
                "AccountReceivable/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}