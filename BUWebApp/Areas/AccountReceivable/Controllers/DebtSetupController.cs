﻿using BusinessLayer;
using BusinessLayer.Helper;
using BusinessLayer.Model;
using BUWebApp.Areas.AccountReceivable.Data;
using BUWebApp.Helper;
using BUWebApp.Models;
using DomainModel;
using Microsoft.AspNet.Identity;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Net;
using System.Runtime.Remoting.Lifetime;
using System.Security.Permissions;
using System.Security.Principal;
using System.Web;
using System.Web.ModelBinding;
using System.Web.Mvc;
using System.Windows;

namespace BUWebApp.Areas.AccountReceivable.Controllers
{
    [Authorize(Roles = "Supervisor")]
    public class DebtSetupController : Controller
    {
        [HttpGet]
        public ActionResult DebtSetupSummary(string setupStatus, string extensionAgentCode)
        {
            var extentionAgentList = new List<Supplier>();
            extentionAgentList = BuyingFacade.SupplierBL().GetAll();

            if (HttpContext.User.IsInRole("Accounting") == false)
            {
                AppSetting.Username = HttpContext.User.Identity.Name;
                var supplierArea = AppSetting.SupervisorUser.AreaCode;
                extentionAgentList = extentionAgentList
                    .Where(x => x.SupplierArea == supplierArea)
                    .ToList();
            }

            ViewBag.ExtensionAgentList = extentionAgentList;
            ViewBag.ExtensionAgentCode = extensionAgentCode;

            var crop = AppSetting.Crop.Crop1;
            ViewBag.Crop = crop;
            ViewBag.SetupStatusList = new List<DebtSetupStatus>
            {
                new DebtSetupStatus { StatusNameEN = "Complete", StatusNameTH ="ตั้งหนี้ครบแล้ว"},
                new DebtSetupStatus { StatusNameEN = "Incomplete",StatusNameTH = "ค้างที่ยังไม่ได้ตั้งหนี้"}
            };

            if (string.IsNullOrEmpty(setupStatus))
                return View(new List<m_FarmerDebtSetupSummary>());

            if (string.IsNullOrEmpty(extensionAgentCode))
                return View(new List<m_FarmerDebtSetupSummary>());

            var accountInvoiceList = BuyingFacade.AccountInvoiceBL()
                .GetByExtensionAgent(crop, extensionAgentCode)
                .Where(x => x.InvoiceNo.Contains("I"))
                .GroupBy(x => new { x.InvoiceNo, x.Invoice })
                .Select(x => new
                {
                    x.Key.InvoiceNo,
                    x.Key.Invoice,
                    InvoiceAmount = x.Sum(y =>
                    y.InvoiceDetails.Sum(z =>
                    z.Quantity * z.UnitPrice))
                })
                .ToList();

            var creditNotes = BuyingFacade.AccountCreditNoteDetailBL()
                .GetByExtensionAgent(crop, extensionAgentCode)
                .GroupBy(x => x.AccountCreditNote.AccountInvoiceNo)
                .Select(x => new
                {
                    AccountInvoicveNo = x.Key,
                    CreditNoteAmount = x.Sum(y => y.Quantity * y.UnitPrice)
                })
                .ToList();

            var invoiceCreditNotes = (from a in accountInvoiceList
                                      join b in creditNotes
                                      on a.InvoiceNo equals b.AccountInvoicveNo
                                      into c
                                      from d in c.DefaultIfEmpty()
                                      select new
                                      {
                                          a.InvoiceNo,
                                          a.Invoice,
                                          a.InvoiceAmount,
                                          CreditNoteAmount = d == null ? 0 : d.CreditNoteAmount,
                                          InvoiceBalanceAmount = a.InvoiceAmount -
                                          (d == null ? 0 : d.CreditNoteAmount)
                                      })
                                      .ToList();

            var famers = BuyingFacade.FarmerBL()
                .GetBySupplierCode(extensionAgentCode);

            var farmerInvoices = (from a in invoiceCreditNotes
                                  from b in famers
                                  where a.Invoice.FarmerCode == b.FarmerCode
                                  select new
                                  {
                                      AccountInvoice = a,
                                      b.Person.FirstName,
                                      b.Person.LastName,
                                      b.FarmerCode
                                  })
                                  .GroupBy(x => new
                                  {
                                      x.FarmerCode,
                                      x.FirstName,
                                      x.LastName
                                  })
                                  .Select(x => new
                                  {
                                      x.Key.FarmerCode,
                                      x.Key.FirstName,
                                      x.Key.LastName,
                                      InvoiceAmount = x.Sum(y => y.AccountInvoice.InvoiceAmount),
                                      CreditNoteAmount = x.Sum(y => y.AccountInvoice.CreditNoteAmount),
                                      InvoiceBalanceAmount = x.Sum(y => y.AccountInvoice.InvoiceAmount) -
                                      x.Sum(y => y.AccountInvoice.CreditNoteAmount)
                                  }).ToList();

            var debtSetupList = BuyingFacade.DebtSetupBL()
                .GetByExtensionAgentSetupCrop(crop, extensionAgentCode)
                .ToList();
            var farmerDebSetupList = debtSetupList
                .GroupBy(x => x.FarmerCode)
                .Select(x => new
                {
                    FarmerCode = x.Key,
                    DebtSetupAmount = x.Sum(y => y.Amount)
                })
                .ToList();

            var join = (from a in farmerInvoices
                        join b in farmerDebSetupList
                        on a.FarmerCode equals b.FarmerCode
                        into c
                        from d in c.DefaultIfEmpty()
                        select new m_FarmerDebtSetupSummary
                        {
                            FarmerCode = a.FarmerCode,
                            FirstName = a.FirstName,
                            LastName = a.LastName,
                            InvoiceAmount = a.InvoiceAmount,
                            InvoiceBalanceAmount = a.InvoiceBalanceAmount,
                            CreditNoteAmount = a.CreditNoteAmount,
                            DebtSetupAmount = d == null ? 0 : d.DebtSetupAmount,
                            WaitForSetupAmount = a.InvoiceBalanceAmount - (d == null ? 0 : d.DebtSetupAmount)
                        })
                       .ToList();

            var resultList = new List<m_FarmerDebtSetupSummary>();
            if (setupStatus == "Complete")
                resultList = join.Where(x => Math.Round(x.WaitForSetupAmount, 2) == 0).ToList();
            else
                resultList = join.Where(x => Math.Round(x.WaitForSetupAmount, 2) != 0).ToList();

            var dpInvoices = invoiceCreditNotes
                .Where(x => x.Invoice.DebtTypeCode == "DP")
                .ToList();
            var dpInvoiceAmt = dpInvoices.Sum(x => x.InvoiceAmount);
            var dpCreditNoteAmt = dpInvoices.Sum(x => x.CreditNoteAmount);
            var dpInvoiceBalanceAmt = dpInvoices.Sum(x => x.InvoiceBalanceAmount);

            var ciInvoices = invoiceCreditNotes
                .Where(x => x.Invoice.DebtTypeCode == "CI")
                .ToList();
            var ciInvoiceAmt = ciInvoices.Sum(x => x.InvoiceAmount);
            var ciCreditNoteAmt = ciInvoices.Sum(x => x.CreditNoteAmount);
            var ciInvoiceBalanceAmt = ciInvoices.Sum(x => x.InvoiceBalanceAmount);

            var dpDebtSetupAmt = debtSetupList
                .Where(x => x.DebtTypeCode == "DP")
                .Sum(x => x.Amount);
            var ciDebtSetupAmt = debtSetupList
                .Where(x => x.DebtTypeCode == "CI")
                .Sum(x => x.Amount);

            ViewBag.dpInvoiceAmt = dpInvoiceAmt;
            ViewBag.dpCreditNoteAmt = dpCreditNoteAmt;
            ViewBag.dpInvoiceBalanceAmt = dpInvoiceBalanceAmt;
            ViewBag.ciInvoiceAmt = ciInvoiceAmt;
            ViewBag.ciCreditNoteAmt = ciCreditNoteAmt;
            ViewBag.ciInvoiceBalanceAmt = ciInvoiceBalanceAmt;
            ViewBag.dpDebtSetupAmt = dpDebtSetupAmt;
            ViewBag.ciDebtSetupAmt = ciDebtSetupAmt;
            ViewBag.setupStatus = setupStatus;

            return View(resultList);
        }

        [HttpGet]
        public ActionResult InvoiceAndCreditNoteSummary(string farmerCode)
        {
            if (string.IsNullOrEmpty(farmerCode))
                return View();
            var crop = AppSetting.Crop.Crop1;
            var accInvoices = BuyingFacade.AccountInvoiceBL()
                .GetByFarmer(crop, farmerCode)
                .Where(x => x.InvoiceNo.Contains("I"))
                .ToList();
            var accInvoiceDetails = AccountInvoiceHelper
                .GetAccountInvoiceDetailsByFarmer(crop, farmerCode)
                .Where(x => x.AccountInvoiceNo.Contains("I"))
                .ToList();
            var creditNotes = BuyingFacade.AccountCreditNoteBL()
                .GetByFarmer(crop, farmerCode)
                .Where(x => x.AccountInvoiceNo.Contains("I"))
                .ToList();
            var creditNoteDetails = CreditNoteDetailHelper
                .GetByFarmerCode(crop, farmerCode)
                .Where(x => x.AccountCreditNote.AccountInvoiceNo.Contains("I"))
                .ToList();

            var returnList = new m_InvoiceDebtSummary
            {
                AccountInvoices = accInvoices,
                AccountInvoiceDetails = accInvoiceDetails.ToList(),
                CreditNotes = creditNotes,
                CreditNoteDetails = creditNoteDetails
            };

            var list2 = BuyingFacade.DebtSetupBL().GetByFarmerSetupCrop(crop, farmerCode);

            ViewBag.Farmer = BuyingFacade.FarmerBL()
                .GetByFarmerCode(farmerCode);
            ViewBag.DebtSetupList = list2;
            return View(returnList);
        }

        [HttpGet]
        public ActionResult Setup(string farmerCode)
        {
            if (string.IsNullOrEmpty(farmerCode))
                return View();
            var crop = AppSetting.Crop.Crop1;
            var accInvoices = BuyingFacade.AccountInvoiceBL()
                .GetByFarmer(crop, farmerCode)
                .Where(x => x.InvoiceNo.Contains("I"))
                .ToList();
            var accInvoiceDetails = AccountInvoiceHelper
                .GetAccountInvoiceDetailsByFarmer(crop, farmerCode)
                .Where(x => x.AccountInvoiceNo.Contains("I"))
                .ToList();
            var creditNotes = BuyingFacade.AccountCreditNoteBL()
                .GetByFarmer(crop, farmerCode)
                .Where(x => x.AccountInvoiceNo.Contains("I"))
                .ToList();
            var creditNoteDetails = CreditNoteDetailHelper
                .GetByFarmerCode(crop, farmerCode)
                .Where(x => x.AccountCreditNote.AccountInvoiceNo.Contains("I"))
                .ToList();

            var returnList = new m_InvoiceDebtSummary
            {
                AccountInvoices = accInvoices,
                AccountInvoiceDetails = accInvoiceDetails,
                CreditNotes = creditNotes,
                CreditNoteDetails = creditNoteDetails
            };

            ViewBag.Registration = BuyingFacade.RegistrationBL()
                .GetSingle(crop, farmerCode);
            ViewBag.DebtSetupList = BuyingFacade.DebtSetupBL()
                .GetByFarmer(farmerCode);

            return View(returnList);
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Main(string setupStatus, string extensionAgentCode)
        {
            // Show extension agent by user role.
            // Show all extension agent for accounting.
            // Show by area for supervisor.
            var extentionAgentList = new List<Supplier>();
            extentionAgentList = BuyingFacade.SupplierBL().GetAll();
            if (HttpContext.User.IsInRole("Accounting") == false)
            {
                AppSetting.Username = HttpContext.User.Identity.Name;
                var supplierArea = AppSetting.SupervisorUser.AreaCode;
                extentionAgentList = extentionAgentList
                    .Where(x => x.SupplierArea == supplierArea)
                    .ToList();
            }
            ViewBag.ExtensionAgentList = extentionAgentList;
            ViewBag.ExtensionAgentCode = extensionAgentCode;

            var crop = AppSetting.Crop.Crop1;
            ViewBag.Crop = crop;

            var list = new List<m_DebtSetupSummary>();
            double totalDebtAmount = 0.0;
            double totalDebtSetupAmount = 0.0;
            if (!string.IsNullOrEmpty(extensionAgentCode))
            {
                list = DebtSetupHelper.GetByExtensionAgent(crop, extensionAgentCode);
                totalDebtAmount = list.Sum(x => x.AmountFromInvoice);
                totalDebtSetupAmount = list.Sum(x => x.AmountFromDebtSetup);
                if (setupStatus == "Complete")
                    list = list
                        .Where(x => x.AmountFromInvoice == Math.Round(x.AmountFromDebtSetup, 2))
                        .ToList();
                else
                    list = list
                        .Where(x => x.AmountFromInvoice > Math.Round(x.AmountFromDebtSetup, 2))
                        .ToList();
            }
            ViewBag.TotalDebtAmount = totalDebtAmount;
            ViewBag.TotalDebtSetupAmount = totalDebtSetupAmount;
            ViewBag.setupStatus = setupStatus;
            ViewBag.SetupStatusList = new List<DebtSetupStatus>
            {
                new DebtSetupStatus { StatusNameEN = "Complete", StatusNameTH ="ตั้งหนี้ครบแล้ว"},
                new DebtSetupStatus { StatusNameEN = "Incomplete",StatusNameTH = "ค้างที่ยังไม่ได้ตั้งหนี้"}
            };

            return View(list);
        }

        [HttpGet]
        public ActionResult SetupDebt(short crop, string farmerCode)
        {
            var register = BuyingFacade.RegistrationBL()
                .GetSingle(crop, farmerCode);
            ViewBag.Register = register;

            // ในหน้า SetupDebt จะมีเฉพาะหนี้ประเภท CI (Crop Inputs) และ DP (Drip Project) เท่านั้น
            var debtTypes = new List<DebtType>();
            debtTypes = BuyingFacade.DebtTypeBL()
                .GetAll()
                .Where(x => x.DebtTypeCode != "L1")
                .OrderBy(x => x.DebtTypeCode)
                .ToList();
            ViewBag.DebtTypeList = debtTypes;

            // show debt setup of selected crop year.
            var debtSetupList = new List<DebtSetup>();
            debtSetupList = BuyingFacade.DebtSetupBL()
                .GetByFarmerSetupCrop(crop, farmerCode)
                .OrderBy(x => x.CreateDate)
                .ToList();
            ViewBag.DebtSetupList = debtSetupList;

            var accInvoiceList = new List<m_AccountInvoiceSummary>();
            var specifiedList = new List<m_AccountInvoiceSummary>();
            var debSetupAccInvoiceList = new List<DebtSetupAccountInvoice>();

            // show all invoice list of selected crop year.
            // disabled checkbox when that invoice to used already.

            // account invoice ที่ถูกนำไปทำลดหนี้แล้ว จะไม่สามารถเอามาทำตั้งหนี้ได้
            // account invoice ที่ถูกนำไปตั้งหนี้แล้ว จะไม่สามารถเอามาทำลดหนี้ได้
            accInvoiceList = BuyingFacade.AccountInvoiceBL()
                .GetByFarmerWithOutCreditNote(crop, farmerCode)
                .Select(x => new m_AccountInvoiceSummary
                {
                    InvoiceNo = x.InvoiceNo,
                    IsPrinted = x.IsPrinted,
                    PrintBy = x.PrintBy,
                    PrintDate = x.PrintDate,
                    CreateDate = x.CreateDate,
                    CreateBy = x.CreateBy,
                    RefInvoiceNo = x.RefInvoiceNo,
                    Price = (double)x.InvoiceDetails.Sum(y => y.UnitPrice * y.Quantity)
                })
                .ToList();

            // debSetupAccInvoiceList is association of debt setup and acccount invoice.
            debSetupAccInvoiceList = BuyingFacade.DebtSetupAccountInvoiceBL()
                .GetByFarmer(crop, farmerCode);

            // SpecifiedList is a list of account invoice used already.
            // Use IsPrinted field for checkbox disabled or enabled.
            specifiedList = accInvoiceList
                .Where(x => !debSetupAccInvoiceList.All(y => y.AccountInvoiceNo != x.InvoiceNo))
                .ToList();
            foreach (var item in accInvoiceList)
            {
                item.IsPrinted = specifiedList
                    .Where(x => x.InvoiceNo == item.InvoiceNo)
                    .Count() > 0 ? true : false;
            }
            ViewBag.AccountInvoiceList = accInvoiceList;

            var accountInvoiceDetails = new List<m_InvoiceDetails>();
            accountInvoiceDetails = AccountInvoiceHelper.GetAccountInvoiceDetailsByFarmer(crop, farmerCode);
            ViewBag.AccountInvoiceDetails = accountInvoiceDetails;

            var associationList = BuyingFacade.DebtSetupAccountInvoiceBL().GetByFarmer(crop, farmerCode);
            ViewBag.AssociationList = associationList;

            var model = new vm_AddDebtSetup();
            model.Crop = crop;
            model.FarmerCode = farmerCode;
            model.Amount = 0;
            model.DebtTypeCode = "";
            model.DripProjectQuota = 0;
            model.CreateBy = HttpContext.User.Identity.Name;
            model.CreateDate = DateTime.Now.Date;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SetupDebt(vm_AddDebtSetup collation, params string[] listOfInvoiceNo)
        {

            //if (!ModelState.IsValid)
            //{
            //    var register = BuyingFacade.RegistrationBL()
            //        .GetSingle(crop, farmerCode);
            //    ViewBag.Register = register;

            //    var debtTypes = new List<DebtType>();
            //    debtTypes = BuyingFacade.DebtTypeBL()
            //        .GetAll()
            //        .OrderBy(x => x.DebtTypeCode)
            //        .ToList();
            //    ViewBag.DebtTypeList = debtTypes;

            //    // Show debt setup of selected crop year.
            //    var debtSetupList = new List<DebtSetup>();
            //    debtSetupList = BuyingFacade.DebtSetupBL()
            //        .GetByFarmerDeductionCrop(collation.Crop, collation.FarmerCode)
            //        .OrderBy(x => x.CreateDate)
            //        .ToList();
            //    ViewBag.DebtSetupList = debtSetupList;

            //    var accInvoiceList = new List<m_AccountInvoiceSummary>();
            //    var specifiedList = new List<m_AccountInvoiceSummary>();
            //    var debSetupAccInvoiceList = new List<DebtSetupAccountInvoice>();

            //    // Show all invoice list of selected crop year.
            //    // Disabled checkbox when that invoice to used already.
            //    accInvoiceList = BuyingFacade.AccountInvoiceBL()
            //        .GetByFarmer(crop, farmerCode)
            //        .Select(x => new m_AccountInvoiceSummary
            //        {
            //            InvoiceNo = x.InvoiceNo,
            //            IsPrinted = x.IsPrinted,
            //            PrintBy = x.PrintBy,
            //            PrintDate = x.PrintDate,
            //            CreateDate = x.CreateDate,
            //            CreateBy = x.CreateBy,
            //            RefInvoiceNo = x.RefInvoiceNo,
            //            Price = (double)x.InvoiceDetails.Sum(y => y.UnitPrice * y.Quantity)
            //        })
            //        .ToList();

            //    // DebSetupAccInvoiceList is association of debt setup and acccount invoice.
            //    debSetupAccInvoiceList = BuyingFacade.DebtSetupAccountInvoiceBL()
            //        .GetByFarmer(crop, farmerCode);

            //    // SpecifiedList is a list of account invoice used already.
            //    // Use IsPrinted field for checkbox disabled or enabled.
            //    specifiedList = accInvoiceList
            //        .Where(x => !debSetupAccInvoiceList.All(y => y.AccountInvoiceNo != x.InvoiceNo))
            //        .ToList();
            //    foreach (var item in accInvoiceList)
            //    {
            //        item.IsPrinted = specifiedList
            //            .Where(x => x.InvoiceNo == item.InvoiceNo)
            //            .Count() > 0 ? true : false;
            //    }
            //    ViewBag.AccountInvoiceList = accInvoiceList;

            //    var accountInvoiceDetails = new List<m_InvoiceDetails>();
            //    accountInvoiceDetails = AccountInvoiceHelper.GetAccountInvoiceDetailsByFarmer(crop, farmerCode);
            //    ViewBag.AccountInvoiceDetails = accountInvoiceDetails;

            //    var associationList = BuyingFacade.DebtSetupAccountInvoiceBL().GetByFarmer(crop, farmerCode);
            //    ViewBag.AssociationList = associationList;
            //    return View(collation);
            //}

            BuyingFacade.DebtSetupBL()
                .Add(collation.Crop,
                collation.FarmerCode,
                listOfInvoiceNo,
                collation.DebtTypeCode,
                collation.DripProjectQuota,
                HttpContext.User.Identity.Name,
                DateTime.Now);
            return RedirectToAction("Setup", new { farmerCode = collation.FarmerCode });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SetupCropInputsDebt(string extensionAgentCode, string setupStatus)
        {
            var crop = AppSetting.Crop.Crop1;
            BuyingFacade.DebtSetupBL()
                .SetupCropInputsDebt(crop,
                extensionAgentCode,
                User.Identity.Name);
            return RedirectToAction("DebtSetupSummary", new { setupStatus = setupStatus, extensionAgentCode = extensionAgentCode });
        }

        [HttpGet]
        public ActionResult Delete(string debtSetupCode, short crop, string farmerCode, string actionReturn)
        {
            BuyingFacade.DebtSetupBL().Delete(debtSetupCode);
            return RedirectToAction(actionReturn, new { crop = crop, farmerCode = farmerCode });
        }

        [HttpGet]
        public ActionResult LoanSetup(short crop, string farmerCode)
        {
            crop = AppSetting.Crop.Crop1;
            var model = new vm_AddDebtSetup();
            var debtSetupList = new List<DebtSetup>();
            var register = new RegistrationFarmer();

            if (string.IsNullOrEmpty(farmerCode))
            {
                ModelState.AddModelError("FarmerCode", "โปรดระบุรหัสชาวไร่เพื่อตรวจสอบข้อมูลก่อนบันทึกการตั้งหนี้");
                return View(new vm_AddDebtSetup());
            }
            register = BuyingFacade.RegistrationBL()
                .GetSingle(crop, farmerCode);
            ViewBag.Register = register;

            // show debt setup of selected crop year.
            debtSetupList = BuyingFacade.DebtSetupBL()
                .GetByFarmerSetupCrop(crop, farmerCode)
                .Where(x => x.DebtTypeCode == "L1")
                .OrderBy(x => x.CreateDate)
                .ToList();
            ViewBag.DebtSetupList = debtSetupList;
            ViewBag.DebtTypeName = BuyingFacade.DebtTypeBL()
                .GetAll()
                .Single(x => x.DebtTypeCode == "L1")
                .DebtTypeName;

            model.Crop = crop;
            model.FarmerCode = farmerCode;
            model.Amount = 0;
            model.DebtTypeCode = "L1";
            model.DripProjectQuota = 0;
            model.NumberOfDeductionCrop = 5;
            model.CreateBy = HttpContext.User.Identity.Name;
            model.CreateDate = DateTime.Now.Date;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LoanSetupDebt(vm_AddDebtSetup collation)
        {
            var crop = collation.Crop;
            var farmerCode = collation.FarmerCode;

            if (!ModelState.IsValid)
            {
                var register = BuyingFacade.RegistrationBL()
                    .GetSingle(crop, farmerCode);
                ViewBag.Register = register;

                var debtTypes = new List<DebtType>();
                debtTypes = BuyingFacade.DebtTypeBL()
                    .GetAll()
                    .OrderBy(x => x.DebtTypeCode)
                    .ToList();
                ViewBag.DebtTypeList = debtTypes;

                // Show debt setup of selected crop year.
                var debtSetupList = new List<DebtSetup>();
                debtSetupList = BuyingFacade.DebtSetupBL()
                    .GetByFarmerDeductionCrop(collation.Crop, collation.FarmerCode)
                    .OrderBy(x => x.CreateDate)
                    .ToList();
                ViewBag.DebtSetupList = debtSetupList;
                ViewBag.DebtTypeName = BuyingFacade.DebtTypeBL()
                    .GetAll()
                    .Single(x => x.DebtTypeCode == "L1")
                    .DebtTypeName;
                return View(collation);
            }

            BuyingFacade.DebtSetupBL()
                .AddLoan(crop,
                farmerCode,
                collation.DripProjectQuota,
                collation.NumberOfDeductionCrop,
                collation.Amount,
                HttpContext.User.Identity.Name,
                DateTime.Now);

            return RedirectToAction("LoanSetup", new { crop = crop, farmerCode = farmerCode });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDeductionPerKg(string debtSetupCode, short crop, string farmerCode, int newQuota, string actionReturn)
        {
            BuyingFacade.DebtSetupBL()
                .EditDeductionPerKg(debtSetupCode, newQuota, HttpContext.User.Identity.Name);
            return RedirectToAction(actionReturn,
                new { crop = crop, farmerCode = farmerCode });
        }
    }
}