﻿using BusinessLayer;
using BusinessLayer.BL;
using BUWebApp.Areas.AccountReceivable.Data;
using BUWebApp.Helper;
using DomainModel;
using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.AccountReceivable.Controllers
{
    [Authorize(Roles = "Supervisor")]
    public class DebtReceiptController : Controller
    {
        // GET: AccountReceivable/DebtReceipt
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult SearchFarmer(string farmerName, string extensionAgentCode)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var farmers = new List<Farmer>();
            if (!string.IsNullOrEmpty(farmerName) && !string.IsNullOrEmpty(extensionAgentCode))
            {
                farmers = BuyingFacade.FarmerBL()
                .GetByFarmerName(farmerName)
                .Where(x => x.Supplier == extensionAgentCode)
                .ToList();
            }

            ViewBag.FarmerName = farmerName;
            ViewBag.ExtentionAgentCode = extensionAgentCode;
            ViewBag.Farmers = farmers;
            ViewBag.ExtensionAgenCodeList = BuyingFacade.SupplierBL()
                .GetByArea(AppSetting.SupervisorUser.AreaCode);
            return View();
        }

        [HttpGet]
        public ActionResult DebtSetupDetails(string farmerCode)
        {
            var farmer = BuyingFacade.FarmerBL()
                .GetByFarmerCode(farmerCode);
            ViewBag.Farmer = farmer;

            var debtSetupList = new List<DebtSetup>();
            if (!string.IsNullOrEmpty(farmerCode))
                debtSetupList = BuyingFacade.DebtSetupBL().GetByFarmer(farmerCode);

            return View(debtSetupList);
        }

        [HttpGet]
        public ActionResult Delete(string receiptCode,string farmerCode)
        {
            BuyingFacade.DebtReceiptBL().Delete(receiptCode);
            return RedirectToAction("DebtSetupDetails", new { farmerCode = farmerCode });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(string debtSetupCode, decimal receiptAmount,string farmerCode)
        {
            var debtSetup = BuyingFacade.DebtSetupBL().GetSingle(debtSetupCode);
            if (debtSetup == null)
                throw new Exception("ไม่พบข้อมูล debt setup code " + debtSetupCode + " นี้ในระบบ");

            BuyingFacade.DebtReceiptBL()
                .Add(debtSetup.Crop,
                null, 
                debtSetup.FarmerCode,
                0, 
                "RC",
                receiptAmount,
                DateTime.Now,
                HttpContext.User.Identity.Name, 
                debtSetupCode);
            return RedirectToAction("DebtSetupDetails", new { farmerCode = farmerCode });
        }
    }
}