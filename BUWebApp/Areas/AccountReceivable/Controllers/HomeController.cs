﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.AccountReceivable.Controllers
{
    [Authorize(Roles = "Supervisor")]
    public class HomeController : Controller
    {
        // GET: AccountReceivable/Home
        public ActionResult Index()
        {
            return View();
        }
    }
}