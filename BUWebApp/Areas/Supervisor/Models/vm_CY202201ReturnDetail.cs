﻿using BusinessLayer.Model;
using DomainModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.Supervisor.Models
{
    public class vm_CY202201ReturnDetail
    {
        [Required(ErrorMessage = "*ReturnItem required")]
        public int ReturnItemID { get; set; }

        [Required(ErrorMessage = "*Quantity required")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Quantity is not valid.")]
        [Range(1, 100,
        ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int Quantity { get; set; }

        public string ReturnNo { get; set; }

        public CY202201Return ReturnMaster { get; set; }

        public m_CY202201ReturnSummary Summary { get; set; }
    }
}