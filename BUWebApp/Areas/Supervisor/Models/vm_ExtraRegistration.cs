﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.Supervisor.Models
{
    public class vm_ExtraRegistration
    {
        [Required(ErrorMessage = "Crop required")]
        public short Crop { get; set; }

        [Required(ErrorMessage = "Quota from sign contract required")]
        public short QuotaFromSignContract { get; set; }

        [Required(ErrorMessage = "Extension agent required")]
        public string SupplierCode { get; set; }
        
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime SignContractDate { get; set; }

        [Required(ErrorMessage = "CitizenID required")]
        public string CitizenID { get; set; }
    }
}