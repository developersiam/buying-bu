﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.Supervisor.Models
{
    public class vm_RegistraionHistory
    {
        public short Crop { get; set; }
        public string FarmerCode { get; set; }
        public short RaiOfContract { get; set; }
        public decimal TotalSold { get; set; }
        public decimal AveragePrice { get; set; }
    }
}