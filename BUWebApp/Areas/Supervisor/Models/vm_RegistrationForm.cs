﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DomainModel;

namespace BUWebApp.Areas.Supervisor.Models
{
    public class vm_RegistrationForm
    {
        [Required(ErrorMessage = "Crop required")]
        [Range(1, short.MaxValue, ErrorMessage = "The value must be greater than 0")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "The value must be numeric")]
        public short Crop { get; set; }

        [Required(ErrorMessage = "Farmer Code required")]
        public string FarmerCode { get; set; }

        [Required(ErrorMessage = "First name required")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "last name required")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "CitizenID required")]
        [RegularExpression("^[0-9]{13}$", ErrorMessage = "The value must be 13 digits numeric.")]
        public string CitizenID { get; set; }

        [Required(ErrorMessage = "Telephone number required")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "The value must be numeric")]
        public string TelephoneNumber { get; set; }

        [Required(ErrorMessage = "Extension agent code required")]
        public string ExtensionAgentCode { get; set; }
        public Person Person { get; set; }
        public Farmer Farmer { get; set; }
        public RegistrationFarmer Registration { get; set; }

        [Required(ErrorMessage = "Sign contract date required")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime SignContractDate { get; set; }

        [Required(ErrorMessage = "Sign Contract Quota required")]
        [Range(1, short.MaxValue, ErrorMessage = "The value must be greater than 0")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "The value must be numeric")]
        public Nullable<short> QuotaFromSignContract { get; set; }

        #region SeedlingFrom
        [Required(ErrorMessage = "How did farmer get the seedlings from required")]
        public Guid SeedlingFromID { get; set; }
        public List<FarmerSeedlingFrom> SelectedSeedlingFromList { get; set; }
        #endregion

        #region IrrigationWater
        [Required(ErrorMessage = "Irrigation water type required")]
        public System.Guid IrrigationWaterTypeID { get; set; }
        public Nullable<decimal> IrrigationWaterDeep { get; set; }
        public string RiverName { get; set; }
        public bool GroundWaterLicence { get; set; }
        public string GroundWaterLicenceRemark { get; set; }
        public List<IrrigationWater> IrrigationWaterList { get; set; }
        #endregion

        #region TransplantingPeriod
        [Required(ErrorMessage = "Estimate transplanting period required")]
        public System.Guid EstimateTransplantingPeriodID { get; set; }
        public List<FarmerEstimateTransplantingPeriod> SelectedPeriodList { get; set; }
        #endregion

        #region Successor
        [Required(ErrorMessage = "Have designated a successor required")]
        public string HasSuccessor { get; set; }
        #endregion

        public string UnRegisterReason { get; set; }
        public bool ActiveStatus { get; set; }
    }
}