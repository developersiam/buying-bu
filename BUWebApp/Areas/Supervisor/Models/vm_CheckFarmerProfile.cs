﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using BUWebApp.Models;
using DomainModel;

namespace BUWebApp.Areas.Supervisor.Models
{
    public class vm_CheckFarmerProfile
    {
        [Required(ErrorMessage = "CitizenID cannot be empty.")]
        [RegularExpression("^[0-9]{13}$", ErrorMessage = "The value must be 13 digits numeric.")]
        public string CitizenID { get; set; }
        [Required(ErrorMessage = "ExtensionAgentCode cannot be empty.")]
        public string ExtensionAgentCode { get; set; }
        public Person Person { get; set; }
        public Farmer Farmer { get; set; }
        public RegistrationFarmer RegistrationFarmer { get; set; }
        public List<Farmer> FarmerList { get; set; }
        public List<vm_FarmerContractHistory> CropSummaryList { get; set; }
    }
}