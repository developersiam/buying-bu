﻿using BusinessLayer.Model;
using DomainModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.Supervisor.Models
{
    public class vm_CreditNoteDetail
    {
        [Required(ErrorMessage = "Credit note code cannot be empty.")]
        public string CreditNoteCode { get; set; }
        [Required(ErrorMessage = "Account invoice no cannot be empty.")]
        public string AccountInvoiceNo { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime AccountInvoiceDate { get; set; }
        [Required(ErrorMessage = "Create credit note date cannot be empty.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CreateDate { get; set; }
        public decimal DistibutionValue { get; set; }
        public bool IsPrinted { get; set; }
        public DateTime? PrintDate { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string TaxID { get; set; }
        [Required(ErrorMessage = "Condition ID cannot be empty.")]
        public Guid ConditionID { get; set; }
        [Required(ErrorMessage = "Quantity cannot be empty.")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Quantity should be numeric.")]
        public int Quantity { get; set; }
        [DataType(DataType.Currency)]
        public decimal UnitPrice { get; set; }
        public decimal Amount { get; set; }
        public decimal Vat { get; set; }
        public decimal NetAmount { get; set; }
        public string Description { get; set; }
        public bool IsPayForCash { get; set; }
        public List<m_CreditNoteDetail> CreditNoteDetailList { get; set; }
        public List<m_CreditNoteDetail> InvoiceDetailList { get; set; }
    }
}