﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.Supervisor.Models
{
    public class vm_FarmerProject
    {
        [Required(ErrorMessage = "Crop required")]
        public short Crop { get; set; }
        [Required(ErrorMessage = "Farmer code required")]
        public string FarmerCode { get; set; }
        [Required(ErrorMessage = "project type required")]
        public string ProjectType { get; set; }
        [Required(ErrorMessage = "Project quota (rai) required")]
        [Range(1, short.MaxValue, ErrorMessage = "The value must be greater than 0")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "The value must be numeric")]
        public byte ActualRai { get; set; }
        [Required(ErrorMessage = "Extra quota Kgs) required")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "The value must be numeric")]
        public short ExtraQuota { get; set; }
        public string ExtensionAgentCode { get; set; }
    }
}