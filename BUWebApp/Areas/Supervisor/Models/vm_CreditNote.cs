﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.Supervisor.Models
{
    public class vm_CreditNote
    {
        [Required(ErrorMessage = "Account invoice no cannot be empty.")]
        public AccountInvoice AccountInvoice { get; set; }
        public Farmer Farmer { get; set; }
        public string AccountInvoiceNo { get; set; }
        public DateTime CreateDate { get; set; }
        public bool IsPayForCash { get; set; }
        public string Description { get; set; }
        public List<AccountCreditNote> CreditNoteList { get; set; }
    }
}