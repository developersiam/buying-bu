﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.Supervisor.Models
{
    public class vm_CPAContainerReturn
    {
        [Required(ErrorMessage = "Required")]
        public string FarmerCode { get; set; }
        [Required(ErrorMessage = "Required")]
        public Guid ReceiveID { get; set; }
        [Required(ErrorMessage = "Required")]
        [Range(1, short.MaxValue, ErrorMessage = "The value must be greater than 0")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "The value must be numeric")]
        public int ReturnQuantity { get; set; }
        [Required(ErrorMessage = "Required")]
        public bool IsEmpty { get; set; }
        public string CitizenID { get; set; }
    }
}