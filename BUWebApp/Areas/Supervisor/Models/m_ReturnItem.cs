﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.Supervisor.Models
{
    public class m_ReturnItem : ReturnChemicalContainer
    {
        public string ItemName { get; set; }
        public string UnitName { get; set; }
        public string BrandName { get; set; }
        public string CategoryName { get; set; }
    }
}