﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.Supervisor.Models
{
    public class m_UseIrrigationLicense
    {
        public bool Value { get; set; }
        public string Name { get; set; }
    }
}