﻿using AGMInventorySystemEntities;
using DomainModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.Supervisor.Models
{
    public class vm_ReturnChemicalContainer
    {
        #region Propperties
        [Required(ErrorMessage = "Crop cannot by empty.")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Crop should be numeric.")]
        public short Crop { get; set; }

        [Required(ErrorMessage = "Farmer code canoot be empty.")]
        public string FarmerCode { get; set; }

        [Required(ErrorMessage = "Item ID cannot be empty.")]
        [RegularExpression("(.*[1-9].*)|(.*[.].*[1-9].*)",ErrorMessage ="Item ID cannot be zero.")]
        public int ItemID { get; set; }

        [Required(ErrorMessage = "Quantity cannot be empty.")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Quantity should be numeric.")]
        public short Quantity { get; set; }

        [Required(ErrorMessage = "Return date cannot be empty.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ReturnDate { get; set; }

        public string Description { get; set; }

        public int CategoryID { get; set; }

        public RegistrationFarmer Registration { get; set; }
        #endregion



        #region ListProperties
        public List<material_category> Categories { get; set; }
        public List<material_item> Items { get; set; }
        public List<m_ReturnItem> ReturnList { get; set; }
        #endregion
    }
}