﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BUWebApp.Areas.Supervisor.Models
{
    public class vm_BankAccount
    {
        [Required(ErrorMessage = "Bank ID required")]
        public int BankID { get; set; }
        [Required(ErrorMessage = "Bank account ID required")]
        public string BookBank { get; set; }
        [Required(ErrorMessage = "Branch required")]
        public string BankBranch { get; set; }
        [Required(ErrorMessage = "Branch code required")]
        public string BankBranchCode { get; set; }
        [Required(ErrorMessage = "Crop required")]
        public short Crop { get; set; }
        [Required(ErrorMessage = "Farmer code required")]
        [StringLength(7)]
        public string FarmerCode { get; set; }
        public string CitizenID { get; set; }
        public string ExtensionAgentCode { get; set; }
    }
}