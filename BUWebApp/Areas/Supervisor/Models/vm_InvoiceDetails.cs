﻿using AGMInventorySystemEntities;
using BusinessLayer.Model;
using DomainModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.Supervisor.Models
{
    public class vm_InvoiceDetails
    {
        /// <summary>
        /// ใช้ในกรณีบันทึกแบบ Additional Item.
        /// </summary>
        [Required(ErrorMessage = "Please select item condition")]
        [Display(Name = "Condition ID")]
        public Guid ConditionID { get; set; }
        [Required(ErrorMessage = "Please enter quantity")]
        [Display(Name = "Quantity")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Quantity is not valid.")]
        [Range(1, 20000,
        ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int Quantity { get; set; }
        public string InvoiceNo { get; set; }

        /// <summary>
        /// ใช้กรณีบันทึกแบบ Package.
        /// </summary>
        public string PackageCode { get; set; }

        /// <summary>
        /// Use for binding data to input from and table.
        /// </summary>
        public Invoice Invoice { get; set; }
        public CropInputsPackage Package { get; set; }
        public List<CropInputsPackage> Packages { get; set; }
        public List<material_item> Items { get; set; }
        public List<m_InvoiceDetails> InvoiceDetails { get; set; }
        public RegistrationFarmer RegistrationFarmer { get; set; }
        public Guid ID { get; set; }
    }
}