﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.Supervisor.Models
{
    public class m_FarmerAccountInvoice
    {
        public string CitizenID { get; set; }
        public string Prefix { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<string> AccountInvoiceNoList { get; set; }
    }
}