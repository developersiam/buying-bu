﻿using BusinessLayer;
using BUWebApp.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Supervisor.Controllers
{
    [Authorize(Roles = "Supervisor")]
    public class CPAContainerReceiveController : Controller
    {
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(string farmerCode, string itemCode, int quantity)
        {
            var crop = AppSetting.Crop.Crop1;
            BuyingFacade.CPAContainerReceiveBL()
                .Add(crop, farmerCode, itemCode, quantity, HttpContext.User.Identity.Name);
            return RedirectToAction("Return", "CPAContainerReturn", new { farmerCode = farmerCode });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Guid receiveID, int receiveQuantity, string farmerCode)
        {
            BuyingFacade.CPAContainerReceiveBL()
                .Edit(receiveID, receiveQuantity, farmerCode);
            return RedirectToAction("Return", "CPAContainerReturn", new { farmerCode = farmerCode });
        }

        [HttpGet]
        public ActionResult Delete(Guid receiveID, string farmerCode)
        {
            BuyingFacade.CPAContainerReceiveBL().Delete(receiveID);
            return RedirectToAction("Return", "CPAContainerReturn", new { farmerCode = farmerCode });
        }
    }
}