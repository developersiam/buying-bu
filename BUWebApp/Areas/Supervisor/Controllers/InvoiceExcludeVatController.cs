﻿using BusinessLayer;
using BUWebApp.Helper;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Supervisor.Controllers
{
    [Authorize(Roles = "Supervisor")]
    public class InvoiceExcludeVatController : Controller
    {
        [HttpGet]
        public ActionResult Index(string farmerCode)
        {
            var _crop = AppSetting.Crop.Crop1;
            AppSetting.Username = HttpContext.User.Identity.Name;
            ViewBag.Farmer = BuyingFacade.FarmerBL().GetByFarmerCode(farmerCode);
            ViewBag.QuotaFromSignContract = BuyingFacade.RegistrationBL()
                .GetSingle(_crop, farmerCode).QuotaFromSignContract;
            ViewBag.InvoiceList = BuyingFacade.InvoiceBL()
                .GetByFarmer(_crop, farmerCode)
                .Where(x => x.IsIncludeVAT == false &&
                x.InvoiceNo.Substring(0, 2) == "IV")
                .ToList();

            return View(new Invoice
            {
                Crop = _crop,
                FarmerCode = farmerCode
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Invoice collation)
        {
            BuyingFacade.InvoiceBL()
                .Add(collation.Crop, 
                collation.FarmerCode,
                "invoice", 
                false,
                HttpContext.User.Identity.Name);
            return RedirectToAction("Index", new { farmerCode = collation.FarmerCode });
        }

        [HttpGet]
        public ActionResult Delete(string invoiceNo)
        {
            var invoice = BuyingFacade.InvoiceBL().GetSingle(invoiceNo);
            if (invoice == null)
                throw new ArgumentException("ไม่พบข้อมูล invoice นี้ในระบบ");

            BuyingFacade.InvoiceBL().Delete(invoiceNo);
            return RedirectToAction("Index", new { farmerCode = invoice.FarmerCode });
        }
    }
}