﻿using BusinessLayer;
using BUWebApp.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Supervisor.Controllers
{
    [Authorize(Roles = "Supervisor")]
    public class CY202201SearchFarmerController : Controller
    {
        // GET: Supervisor/CY202201SearFarmer
        [HttpGet]
        public ActionResult Index(string supplierCode)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supervisor = AppSetting.SupervisorUser;
            var _crop = AppSetting.Crop;

            List<SelectListItem> supplierList = new List<SelectListItem>();
            foreach (var item in BuyingFacade.SupplierBL().GetByArea(_supervisor.AreaCode))
            {
                supplierList.Add(new SelectListItem
                {
                    Text = item.SupplierCode,
                    Value = item.SupplierCode
                });
            }
            ViewBag.SupplierList = supplierList;
            ViewBag.SupplierCode = supplierCode;

            return View(BuyingFacade.RegistrationBL()
                .GetBySupplier(_crop.Crop1, supplierCode));
        }
    }
}