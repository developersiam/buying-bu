﻿using BusinessLayer;
using BUWebApp.Areas.Supervisor.Models;
using BUWebApp.Helper;
using BUWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Supervisor.Controllers
{
    [Authorize(Roles = "Supervisor")]
    public class BankAccountController : Controller
    {
        [HttpGet]
        public ActionResult UpdateBankAccount(short crop, string farmerCode)
        {
            var registration = BuyingFacade.RegistrationBL().GetSingle(crop, farmerCode);
            if (registration == null)
                throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รายนี้! (farmer code : " + farmerCode + ")");

            ViewBag.BankList = BuyingFacade.BankBL().GetAll().OrderBy(b => b.BankName);
            ViewBag.Registration = registration;
            return View(new vm_BankAccount
            {
                Crop = crop,
                FarmerCode = farmerCode,
                CitizenID = registration.Farmer.CitizenID,
                ExtensionAgentCode = registration.Farmer.Supplier,
                BankBranch = registration.BankBranch,
                BankBranchCode = registration.BankBranchCode,
                BankID = registration.BankID == null ? 0 : (int)registration.BankID,
                BookBank = registration.BookBank
            });
        }

        [HttpPost]
        public ActionResult UpdateBankAccount(vm_BankAccount model)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.BankList = BuyingFacade.BankBL().GetAll().OrderBy(b => b.BankName);
                return View(model);
            }

            BuyingFacade.RegistrationBL()
                .UpdateBookBank(model.Crop,
                model.FarmerCode,
                Convert.ToUInt16(model.BankID),
                model.BookBank,
                model.BankBranchCode,
                model.BankBranch,
                HttpContext.User.Identity.Name);

            return RedirectToAction("CheckProfile", "Farmer",
                new { citizenID = model.CitizenID, extensionAgentCode = model.ExtensionAgentCode });
        }
    }
}