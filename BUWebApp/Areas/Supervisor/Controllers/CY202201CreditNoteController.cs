﻿using BusinessLayer;
using BusinessLayer.Helper;
using BUWebApp.Areas.Supervisor.Models;
using BUWebApp.Helper;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Supervisor.Controllers
{
    public class CY202201CreditNoteController : Controller
    {
        // GET: Supervisor/CY202201CreditNote
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }


        [HttpGet]
        public ActionResult RenderPDF(string creditNoteCode)
        {
            try
            {
                var model = new vm_CreditNoteDetail();
                var creditNote = BuyingFacade.CY202201CreditNoteCodeBL().GetSingle(creditNoteCode);
                if (creditNote == null)
                    throw new ArgumentException("ไม่พบข้อมูล creditnote " + creditNoteCode + " นี้ในระบบ");

                if (creditNote.IsPrinted == true)
                    throw new ArgumentException("ใบลดหนี้นี้ถูกปริ้นท์ไปแล้วเมื่อ " + creditNote.PrintDate + " โดย " + creditNote.PrintBy);

                var farmer = BuyingFacade.FarmerBL()
                    .GetByFarmerCode(creditNote.FarmerCode);
                if (farmer == null)
                    throw new ArgumentException("ไม่พบข้อมูล farmer นี้ในระบบ");

                ReportDataSource headerDS = new ReportDataSource();
                ReportDataSource DetailDS = new ReportDataSource();

                headerDS.Name = "CreditNoteHeaderDataSet";
                DetailDS.Name = "CreditNoteDetailDataSet";

                headerDS.Value = CY202201CreditNoteHelper.GetHeader(creditNoteCode);
                DetailDS.Value = CY202201CreditNoteHelper.GetDetail(creditNote.Crop, creditNote.FarmerCode);

                LocalReport report = new LocalReport();
                report.DataSources.Add(headerDS);
                report.DataSources.Add(DetailDS);

                report.ReportPath = Server.MapPath("~/Areas/Supervisor/RDLC/CY202201CreditNote.rdlc");

                string filePath = Path.GetTempFileName();
                Export(report, filePath);

                //Clost Report object.           
                report.Dispose();

                //Change credit note  status for just can print only one time.
                BuyingFacade.AccountCreditNoteBL().ReturnItemToAGMInventory(creditNoteCode, AppSetting.Username);
                return File(filePath, "application/pdf");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Export(LocalReport report, string filePath)
        {
            string ack = "";
            try
            {
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;

                byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);
                using (FileStream stream = System.IO.File.OpenWrite(filePath))
                {
                    stream.Write(bytes, 0, bytes.Length);
                }
                return ack;
            }
            catch (Exception ex)
            {
                ack = ex.InnerException.Message;
                return ack;
            }
        }
    }
}