﻿using BusinessLayer;
using BusinessLayer.Helper;
using BUWebApp.Areas.Supervisor.Models;
using BUWebApp.Helper;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Supervisor.Controllers
{
    [Authorize(Roles = "Supervisor")]
    public class FarmerProjectController : Controller
    {
        [HttpGet]
        public ActionResult Index(string extensionAgentCode)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var supervisor = AppSetting.SupervisorUser;
            ViewBag.ExtensionAgentCode = extensionAgentCode;
            ViewBag.ExtensionAgentList = BuyingFacade.SupplierBL().GetByArea(supervisor.AreaCode);
            if (extensionAgentCode == null)
                return View(new List<FarmerProject>());

            var cropDefault = AppSetting.Crop;
            ViewBag.FarmerProjectList = FarmerProjectHelper
                .GetQuotaAndSoldBySupplierVersion2(cropDefault.Crop1, extensionAgentCode);
            return View(BuyingFacade.FarmerProjectBL()
                .GetByExtensionAgentCode(extensionAgentCode, cropDefault.Crop1));
        }

        [HttpGet]
        public ActionResult Add(string farmerCode)
        {
            ViewBag.RegistrationFarmer = null;
            ViewBag.Farmer = null;

            if (string.IsNullOrEmpty(farmerCode))
            {
                ModelState.AddModelError("", "โปรดกรอกรหัสชาวไร่ (farmer code) เพื่อตรวจสอบข้อมูลชาวไร่ในระบบก่อน");
                return View(new vm_FarmerProject());
            }

            var farmer = BuyingFacade.FarmerBL().GetByFarmerCode(farmerCode);
            if (farmer == null)
            {
                ModelState.AddModelError("", "ไม่พบประวัติชาวไร่รหัส " + farmerCode + " ในระบบ");
                return View(new vm_FarmerProject());
            }

            var cropDefault = AppSetting.Crop.Crop1;
            var register = BuyingFacade.RegistrationBL().GetSingle(cropDefault, farmerCode);
            if (register == null)
            {
                ModelState.AddModelError("", "ไม่พบข้อมูลการทำสัญญาของชาวไร่รายนี้ในระบบ");
                return View(new vm_FarmerProject());
            }

            ViewBag.RegistrationFarmer = register;
            ViewBag.Farmer = register.Farmer;
            ViewBag.FarmerProjectList = BusinessLayer.Helper.FarmerProjectHelper
                .GetQuotaAndSoldByFarmerVersion2(cropDefault, farmerCode);
            ViewBag.ProjectTypes = new List<SelectListItem> {
            new SelectListItem{ Text = "HB" ,Value = "HB" },
            new SelectListItem{ Text = "HPP" ,Value = "HPP"},
            new SelectListItem{ Text = "NL" ,Value = "NL"},
            new SelectListItem{ Text = "SC" ,Value = "SC"}
            };

            return View(new vm_FarmerProject
            {
                Crop = cropDefault,
                FarmerCode = farmerCode,
                ActualRai = 0,
                ExtraQuota = 0,
                ExtensionAgentCode = farmer.Supplier
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(vm_FarmerProject model)
        {
            var farmer = BuyingFacade.FarmerBL().GetByFarmerCode(model.FarmerCode);
            if (farmer == null)
                throw new ArgumentException("ไม่พบข้อมูลชาวไร่รหัส " + model.FarmerCode + " นี้ในระบบ");

            var register = BuyingFacade.RegistrationBL().GetSingle(model.Crop, model.FarmerCode);
            ViewBag.RegistrationFarmer = register;
            ViewBag.Farmer = register.Farmer;
            ViewBag.ProjectTypes = new List<SelectListItem> {
            new SelectListItem{ Text = "HB" ,Value = "HB" },
            new SelectListItem{ Text = "HPP" ,Value = "HPP"},
            new SelectListItem{ Text = "NL" ,Value = "NL"},
            new SelectListItem{ Text = "SC" ,Value = "SC"}
            };

            if (!ModelState.IsValid)
                return View();

            BuyingFacade.FarmerProjectBL()
                .Add(new FarmerProject
                {
                    Crop = model.Crop,
                    FarmerCode = model.FarmerCode,
                    ProjectType = model.ProjectType,
                    ActualRai = model.ActualRai,
                    ExtraQuota = model.ExtraQuota,
                    ModifiedByUser = HttpContext.User.Identity.Name,
                    LastModified = DateTime.Now
                });
            return RedirectToAction("Index", "FarmerProject", new { extensionAgentCode = model.ExtensionAgentCode });
        }

        [HttpGet]
        public ActionResult Edit(short crop, string farmerCode, string projectType)
        {
            var farmerProject = BuyingFacade.FarmerProjectBL().GetSingle(crop, farmerCode, projectType);
            if (farmerProject == null)
                throw new ArgumentException("ไม่พบข้อมูล farmer project นี้ในระบบ");

            ViewBag.RegistrationFarmer = farmerProject.RegistrationFarmer;
            ViewBag.Farmer = farmerProject.RegistrationFarmer.Farmer;
            ViewBag.FarmerProjectList = BusinessLayer.Helper.FarmerProjectHelper
                .GetQuotaAndSoldByFarmerVersion2(crop, farmerCode)
                .Where(x => x.ProjectType == projectType)
                .ToList();
            ViewBag.ProjectTypes = new List<SelectListItem> {
            new SelectListItem{ Text = "HB" ,Value = "HB", Selected = farmerProject.ProjectType.Contains("HB") },
            new SelectListItem{ Text = "HPP" ,Value = "HPP", Selected = farmerProject.ProjectType.Contains("HPP") },
            new SelectListItem{ Text = "NL" ,Value = "NL", Selected = farmerProject.ProjectType.Contains("NL") },
            new SelectListItem{ Text = "SC" ,Value = "SC", Selected = farmerProject.ProjectType.Contains("SC")}
            };

            return View(new vm_FarmerProject
            {
                Crop = farmerProject.Crop,
                FarmerCode = farmerProject.FarmerCode,
                ProjectType = farmerProject.ProjectType,
                ActualRai = farmerProject.ActualRai,
                ExtraQuota = farmerProject.ExtraQuota,
                ExtensionAgentCode = farmerProject.RegistrationFarmer.Farmer.Supplier
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(vm_FarmerProject model)
        {
            ViewBag.ProjectTypes = new List<SelectListItem> {
            new SelectListItem{ Text = "HB" ,Value = "HB", Selected = model.ProjectType.Contains("HB") },
            new SelectListItem{ Text = "HPP" ,Value = "HPP", Selected = model.ProjectType.Contains("HPP") },
            new SelectListItem{ Text = "NL" ,Value = "NL", Selected = model.ProjectType.Contains("NL") },
            new SelectListItem{ Text = "SC" ,Value = "SC", Selected = model.ProjectType.Contains("SC")}
            };

            if (!ModelState.IsValid)
                return View(model);

            var username = HttpContext.User.Identity.Name;
            BuyingFacade.FarmerProjectBL()
                .UpdateAll(model.Crop,
                model.FarmerCode,
                model.ProjectType,
                model.ActualRai,
                model.ExtraQuota,
                username);
            return RedirectToAction("Index", "FarmerProject",
                new { extensionAgentCode = model.ExtensionAgentCode });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditFromIndex(short crop, string farmerCode, string projectType, byte actualRai, short extraQuota, string extensionAgentCode)
        {
            BuyingFacade.FarmerProjectBL()
                .UpdateAll(crop,
                farmerCode.ToUpper(),
                projectType.ToUpper(),
                actualRai,
                extraQuota,
                HttpContext.User.Identity.Name);
            return RedirectToAction("Index", "FarmerProject",
                new { extensionAgentCode = extensionAgentCode });
        }

        [HttpGet]
        public ActionResult Delete(short crop, string farmerCode, string projectType, string extensionAgentCode)
        {
            BuyingFacade.FarmerProjectBL().Delete(crop, farmerCode, projectType);
            return RedirectToAction("Edit", "FarmerProject",
                new
                {
                    crop = crop,
                    farmerCode = farmerCode,
                    projectType = projectType
                });
        }
    }
}