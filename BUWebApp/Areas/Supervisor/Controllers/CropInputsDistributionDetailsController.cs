﻿using AGMInventorySystemEntities;
using BusinessLayer;
using BusinessLayer.Helper;
using BusinessLayer.Model;
using BUWebApp.Areas.Supervisor.Models;
using BUWebApp.Helper;
using DomainModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Supervisor.Controllers
{
    [Authorize(Roles = "Supervisor")]
    public class CropInputsDistributionDetailsController : Controller
    {
        [HttpGet]
        public ActionResult Index(string invoiceNo)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            ViewBag.SupervisorArea = AppSetting.SupervisorUser.AreaCode;
            ViewBag.CreateDate = DateTime.Now.Date;

            /// แสดง invoice header
            var invoice = BuyingFacade.InvoiceBL().GetSingle(invoiceNo);

            /// ใช้แสดง invoice details
            var invoiceDetails = InvoiceDetailsHelper
                .GetByInvoiceNo(invoiceNo)
                .ToList();

            /// แสดงประวัติการทำสัญญา
            var register = BuyingFacade.RegistrationBL()
                .GetSingle(invoice.Crop, invoice.FarmerCode);

            ViewBag.Categories = ViewBag.Categories = BusinessLayer.Helper
                .CropInputsItemHelper
                .GetBySupplierCode(register.Crop, register.Farmer.Supplier)
                .GroupBy(x => x.Category)
                .OrderBy(x => x.Key)
                .Select(x => new m_CropInputsItem
                {
                    Category = x.Key
                }).ToList();

            var returnModel = new vm_InvoiceDetails
            {
                InvoiceNo = invoice.InvoiceNo,
                Invoice = invoice,
                Items = new List<material_item>(),
                InvoiceDetails = invoiceDetails,
                RegistrationFarmer = register
            };

            return View(returnModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(InvoiceDetail collation)
        {
            BuyingFacade.InvoiceDetailsBL()
                .Add(collation.InvoiceNo, 
                collation.ConditionID, 
                collation.Quantity, 
                HttpContext.User.Identity.Name);
            return RedirectToAction("Index", new { invoiceNo = collation.InvoiceNo });
        }

        [HttpGet]
        public ActionResult Delete(string invoiceNo, Guid conditionID)
        {
            BuyingFacade.InvoiceDetailsBL().Delete(invoiceNo, conditionID);
            return RedirectToAction("Index", new { invoiceNo = invoiceNo });
        }

        [HttpPost]
        public ActionResult Finish(string invoiceNo, DateTime createDate, params Guid[] isCashes)
        {
            if (createDate == null)
                throw new ArgumentException("โปรดระบุวันที่ออกใบส่่งปัจจัยการผลิต");

            var invoice = BuyingFacade.InvoiceBL().GetSingle(invoiceNo);
            if (invoice == null)
                throw new ArgumentException("ไม่พบข้อมูล invoice : " + invoiceNo + " นี้ในระบบ");

            if (invoice.IsFinish == true)
                throw new ArgumentException("Invoice นี้ถูกเปลี่ยนสถานะเป็น finish แล้ว " +
                    "โปรดติดต่อแผนกไอทีหากต้องการตรวจสอบข้อมูลหรือปลดล็อค");

            var invoiceItems = BuyingFacade.InvoiceDetailsBL().GetByInvoiceNo(invoiceNo);
            if (isCashes != null)
            {
                foreach (var item in invoiceItems)
                {
                    if (isCashes.Where(x => x == item.ConditionID).Count() > 0)
                        item.IsCash = true;
                    else
                        item.IsCash = false;
                }
            }

            foreach (var item in invoiceItems)
                BuyingFacade.InvoiceDetailsBL().ChangeIsCashStatus(item);

            BuyingFacade.InvoiceBL().Finish(invoiceNo, HttpContext.User.Identity.Name, createDate);
            return RedirectToAction("Index", new { invoiceNo = invoiceNo });
        }
    }
}