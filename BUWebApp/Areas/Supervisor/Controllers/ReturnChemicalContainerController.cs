﻿using AGMInventorySystemEntities;
using BusinessLayer;
using BUWebApp.Areas.Supervisor.Models;
using BUWebApp.Helper;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Supervisor.Controllers
{
    public class ReturnChemicalContainerController : Controller
    {
        // GET: Supervisor/ReturnChemicalContainer
        [HttpGet]
        public ActionResult Add(string farmerCode)
        {
            var registration = BuyingFacade.RegistrationBL().GetSingle(AppSetting.Crop.Crop1, farmerCode);
            var itemList = AGMInventorySystemBL.BLServices.material_itemBL().GetAll();
            var returnList = BuyingFacade.ReturnChemicalContainerBL().GetByFarmer(AppSetting.Crop.Crop1, farmerCode);
            var resultList = (from a in itemList
                              from b in returnList
                              where a.itemid == b.ItemID
                              select new m_ReturnItem
                              {
                                  Crop = b.Crop,
                                  FarmerCode = b.FarmerCode,
                                  ReturnDate = b.ReturnDate,
                                  Quantity = b.Quantity,
                                  ItemID = b.ItemID,
                                  ID = b.ID,
                                  ItemName = a.item_name,
                                  UnitName = a.material_unit.unit_name,
                                  BrandName = a.material_brand.brand_name,
                                  CategoryName = a.material_category.category_name
                              }).ToList();

            return View(new vm_ReturnChemicalContainer
            {
                Crop = AppSetting.Crop.Crop1,
                FarmerCode = farmerCode,
                Quantity = 1,
                ReturnDate = DateTime.Now,
                Registration = registration,
                Categories = AGMInventorySystemBL.BLServices.material_categoryBL().GetAll(),
                Items = null,
                ReturnList = resultList
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(vm_ReturnChemicalContainer model)
        {
            var registration = BuyingFacade.RegistrationBL().GetSingle(model.Crop, model.FarmerCode);
            var itemList = AGMInventorySystemBL.BLServices.material_itemBL().GetAll();
            var returnList = BuyingFacade.ReturnChemicalContainerBL().GetByFarmer(model.Crop, model.FarmerCode);
            var resultList = (from a in itemList
                              from b in returnList
                              where a.itemid == b.ItemID
                              select new m_ReturnItem
                              {
                                  Crop = b.Crop,
                                  FarmerCode = b.FarmerCode,
                                  ReturnDate = b.ReturnDate,
                                  Quantity = b.Quantity,
                                  ItemID = b.ItemID,
                                  ID = b.ID,
                                  ItemName = a.item_name,
                                  UnitName = a.material_unit.unit_name,
                                  BrandName = a.material_brand.brand_name,
                                  CategoryName = a.material_category.category_name
                              }).ToList();

            model.Registration = registration;
            model.ReturnList = resultList;
            model.Categories = AGMInventorySystemBL.BLServices.material_categoryBL().GetAll();

            if (!ModelState.IsValid)
                return View(model);

            BuyingFacade.ReturnChemicalContainerBL()
                .Add(model.Crop,
                model.FarmerCode,
                model.ItemID,
                model.Quantity,
                model.ReturnDate,
                model.Description,
                HttpContext.User.Identity.Name);

            return RedirectToAction("Add", new { FarmerCode = model.FarmerCode });
        }

        [HttpGet]
        public ActionResult Delete(Guid id)
        {
            var model = BuyingFacade.ReturnChemicalContainerBL().GetSingle(id);
            if (model == null)
                throw new ArgumentException("ไม่พบข้อมูลที่ท่านต้องการลบ");

            BuyingFacade.ReturnChemicalContainerBL().Delete(id);

            return RedirectToAction("Add", new { crop = model.Crop, FarmerCode = model.FarmerCode });
        }
    }
}