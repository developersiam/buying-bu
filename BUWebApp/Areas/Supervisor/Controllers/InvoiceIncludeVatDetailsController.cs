﻿using AGMInventorySystemEntities;
using BusinessLayer;
using BusinessLayer.Helper;
using BusinessLayer.Model;
using BUWebApp.Areas.Supervisor.Models;
using BUWebApp.Helper;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Supervisor.Controllers
{
    [Authorize(Roles = "Supervisor")]
    public class InvoiceIncludeVatDetailsController : Controller
    {
        [HttpGet]
        public ActionResult Index(string invoiceNo)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            ViewBag.SupervisorArea = AppSetting.SupervisorUser.AreaCode;

            /// แสดง invoice header
            var invoice = BuyingFacade.InvoiceBL().GetSingle(invoiceNo);

            /// ใช้แสดง invoice details
            var invoiceDetails = InvoiceDetailsHelper
                .GetByInvoiceNo(invoiceNo)
                .ToList();

            /// แสดงประวัติการทำสัญญา
            var register = BuyingFacade.RegistrationBL()
                .GetSingle(invoice.Crop, invoice.FarmerCode);

            /// เมื่อมีการบันทึกรายการที่อยู่ใน minimum package ไปแล้ว ให้แสดงรายละเอียดของ issued code
            var package = new CropInputsPackage();
            if (invoiceDetails.Where(x => x.PackageCode != null).Count() > 0)
            {
                package = BuyingFacade.CropInputsPackageBL()
                   .GetSingle(invoiceDetails
                   .Where(x => x.PackageCode != null)
                   .FirstOrDefault()
                   .PackageCode);
            }

            /// ตัวเลือกแพ็คเกจ
            var packages = BuyingFacade.CropInputsPackageBL()
                .GetBySupplier(invoice.Crop, register.Farmer.Supplier)
                .Select(x => new CropInputsPackage
                {
                    PackageCode = x.PackageCode,
                    Description = x.PackageCode + " (" + x.Description + ")",
                    SupplierCode = x.SupplierCode
                }).ToList();

            ViewBag.Categories = ViewBag.Categories = CropInputsItemHelper
                .GetBySupplierCode(register.Crop, register.Farmer.Supplier)
                .GroupBy(x => x.Category)
                .OrderBy(x => x.Key)
                .Select(x => new m_CropInputsItem
                {
                    Category = x.Key
                }).ToList();

            ViewBag.SupplierCode = register.Farmer.Supplier;

            var returnModel = new vm_InvoiceDetails
            {
                InvoiceNo = invoice.InvoiceNo,
                PackageCode = package == null ? null : package.PackageCode,
                Invoice = invoice,
                Package = package,
                Packages = packages,
                Items = new List<material_item>(),
                InvoiceDetails = invoiceDetails,
                RegistrationFarmer = register
            };

            return View(returnModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(InvoiceDetail collation)
        {
            BuyingFacade.InvoiceDetailsBL()
                .Add(collation.InvoiceNo,
                collation.ConditionID,
                collation.Quantity,
                HttpContext.User.Identity.Name);
            return RedirectToAction("Index", new { invoiceNo = collation.InvoiceNo });
        }

        [HttpGet]
        public ActionResult Delete(string invoiceNo, Guid conditionID)
        {
            BuyingFacade.InvoiceDetailsBL().Delete(invoiceNo, conditionID);
            return RedirectToAction("Index", new { invoiceNo = invoiceNo });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddByPackage(vm_InvoiceDetails collation)
        {
            BuyingFacade.InvoiceDetailsBL()
                .AddByPackage(collation.PackageCode,
                collation.InvoiceNo,
                HttpContext.User.Identity.Name);
            return RedirectToAction("Index", new { invoiceNo = collation.InvoiceNo });
        }

        [HttpGet]
        public ActionResult DeleteByPackage(vm_InvoiceDetails collation)
        {
            BuyingFacade.InvoiceDetailsBL()
                .DeleteByPackage(collation.PackageCode, collation.InvoiceNo);
            return RedirectToAction("Index", new { invoiceNo = collation.InvoiceNo });
        }

        [HttpPost]
        public ActionResult Finish(string invoiceNo, DateTime createDate, params Guid[] isCashes)
        {
            if (createDate == null)
                throw new ArgumentException("โปรดระบุวันที่ออกใบส่่งปัจจัยการผลิต");

            var invoiceItems = BuyingFacade.InvoiceDetailsBL().GetByInvoiceNo(invoiceNo);
            if (isCashes != null)
            {
                foreach (var item in invoiceItems)
                {
                    if (isCashes.Where(x => x == item.ConditionID).Count() > 0)
                        item.IsCash = true;
                    else
                        item.IsCash = false;
                }
            }

            ///ใช้สำหรับกรณี 1 invoice มีทั้งรายการที่จ่ายเป็นเงินสด และเอาของไปก่อนจ่ายทีหลัง
            ///ระบบจะต้องแยกใบ account invoice ออกเป็น 2 แบบ
            ///code ส่วนนี้เอาไว้ mark ว่ารายการใดบ้างที่จ่ายเงินสด 
            ///เพื่อจะได้นำไปออกรหัส account invoice ได้ตามประเภทที่ถูกต้อง
            foreach (var item in invoiceItems)
                BuyingFacade.InvoiceDetailsBL().ChangeIsCashStatus(item);

            BuyingFacade.InvoiceBL().Finish(invoiceNo, HttpContext.User.Identity.Name, createDate);
            return RedirectToAction("Index", new { invoiceNo = invoiceNo });
        }
    }
}