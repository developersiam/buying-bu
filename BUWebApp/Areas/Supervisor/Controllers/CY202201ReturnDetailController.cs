﻿using BusinessLayer;
using BusinessLayer.Model;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using BUWebApp.Areas.Supervisor.Models;
using BusinessLayer.Helper;

namespace BUWebApp.Areas.Supervisor.Controllers
{
    [Authorize(Roles = "Supervisor")]
    public class CY202201ReturnDetailController : Controller
    {
        // GET: Supervisor/CY202201ReturnDetail
        [HttpGet]
        public ActionResult Index(string returnNo)
        {
            ViewBag.ReturnItemList = BusinessLayer.Helper.CY202201ReturnItemHelper.GetAll()
                .OrderBy(x => x.ItemType)
                .ThenBy(x => x.ItemName)
                .ToList();

            ViewBag.ItemTypeList = BuyingFacade.CY202201ReturnItemBL().GetItemType();
            ViewBag.ReturnList = BusinessLayer.Helper.CY202201ReturnDetailHelper.GetByReturnNo(returnNo);

            var returnDoc = BuyingFacade.CY202201ReturnBL().GetSingle(returnNo);
            ViewBag.SummaryReceiveList = BusinessLayer.Helper.InvoiceDetailsHelper
                .GetSummaryByFarmer(returnDoc.Crop, returnDoc.FarmerCode);
            ViewBag.SummaryReturnList = BusinessLayer.Helper.CY202201ReturnDetailHelper
                .GetSummaryByFarmer(returnDoc.Crop, returnDoc.FarmerCode);

            var forPatialView = CY202201ReturnSummaryHelper.GetByFarmer(returnDoc.Crop, returnDoc.FarmerCode);

            return View(new vm_CY202201ReturnDetail
            {
                ReturnMaster = returnDoc,
                ReturnNo = returnNo,
                Summary = forPatialView
            });
        }

        [HttpPost]
        public ActionResult Add(CY202201ReturnDetail collection)
        {
            var user = HttpContext.User.Identity.Name;
            collection.ModifiedUser = user;
            collection.RecordUser = user;
            BuyingFacade.CY202201ReturnDetailBL().Add(collection);
            return RedirectToAction("Index", new { returnNo = collection.ReturnNo });
        }

        [HttpGet]
        public ActionResult Delete(Guid returnDetailID, string returnNo)
        {
            BuyingFacade.CY202201ReturnDetailBL().Delete(returnDetailID);
            return RedirectToAction("Index", new { returnNo = returnNo });
        }

        [HttpPost]
        public ActionResult Edit(CY202201ReturnDetail collection)
        {
            BuyingFacade.CY202201ReturnDetailBL()
                .Update(collection.ReturnDetailID, collection.Quantity, HttpContext.User.Identity.Name);
            return RedirectToAction("Index", new { returnNo = collection.ReturnNo });
        }
    }
}