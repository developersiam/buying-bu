﻿using AGMInventorySystemEntities;
using BusinessLayer.Helper;
using BusinessLayer.Model;
using BusinessLayer;
using BUWebApp.Areas.Supervisor.Models;
using BUWebApp.Helper;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Linq.Expressions;

namespace BUWebApp.Areas.Supervisor.Controllers
{
    [Authorize(Roles = "Supervisor")]
    public class InvoiceDetailsController : Controller
    {
        // GET: Supervisor/InvoiceDetails
        public ActionResult Index(string invoiceNo)
        {
            var invoice = BuyingFacade.InvoiceBL().GetSingle(invoiceNo);
            if (invoice == null)
                throw new Exception("ไม่พบ invoice นี้ในระบบ");

            //if (invoice.IsFinish == true)
            //    throw new Exception("invoice นี้อยู่ในสถานะ finished");

            AppSetting.Username = HttpContext.User.Identity.Name;
            ViewBag.CreateDate = DateTime.Now.Date;
            ViewBag.Invoice = invoice;
            ViewBag.InvoiceDetails = InvoiceDetailsHelper
                .GetByInvoiceNo(invoiceNo)
                .ToList();

            var register = BuyingFacade.RegistrationBL()
                .GetSingle(invoice.Crop, invoice.FarmerCode);
            if (register == null)
                throw new Exception("ไม่พบข้อมูลการทำสัญญาชาวไร่รายนี้ในระบบ");

            if (register.ActiveStatus == false)
                throw new Exception("ชาวไร่รายนี้ทำการยกเลิกสัญญาแล้ว");

            ViewBag.Registration = register;
            ViewBag.PackageList = BuyingFacade.CropInputsPackageBL()
                .GetBySupplier(invoice.Crop, register.Farmer.Supplier)
                .Select(x => new CropInputsPackage
                {
                    PackageCode = x.PackageCode,
                    Description = x.PackageCode + " (" + x.Description + ")"
                })
                .ToList();
            ViewBag.Categories = CropInputsItemHelper
                .GetBySupplierCode(register.Crop, register.Farmer.Supplier)
                .GroupBy(x => x.Category)
                .OrderBy(x => x.Key)
                .Select(x => new m_CropInputsItem
                {
                    Category = x.Key
                })
                .ToList();
            ViewBag.AccountInvoice = BuyingFacade.AccountInvoiceBL()
                .GetByRefInvoice(invoiceNo)
                .ToList();
            ViewBag.DebtTypes = BuyingFacade.DebtTypeBL().GetAll();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(string invoiceNo, string conditionIDs)
        {
            var json = JsonConvert.DeserializeObject<List<InvoiceDetail>>(conditionIDs);
            List<InvoiceDetail> list = new List<InvoiceDetail>();
            list = json;
            foreach (var item in json)
            {
                BuyingFacade.InvoiceDetailsBL()
                    .Add(invoiceNo,
                    item.ConditionID,
                    item.Quantity,
                    HttpContext.User.Identity.Name);
            }
            return RedirectToAction("Index", new { invoiceNo = invoiceNo });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddByPackage(string invoiceNo, string packageCode)
        {
            BuyingFacade.InvoiceDetailsBL()
                .AddByPackage(packageCode,
                invoiceNo,
                HttpContext.User.Identity.Name);
            return RedirectToAction("Index", new { invoiceNo = invoiceNo });
        }

        [HttpGet]
        public ActionResult DeleteByItem(string invoiceNo, Guid conditionID)
        {
            BuyingFacade.InvoiceDetailsBL().Delete(invoiceNo, conditionID);
            return RedirectToAction("Index", new { invoiceNo = invoiceNo });
        }

        [HttpGet]
        public ActionResult DeleteByPackage(string invoiceNo, string packageCode)
        {
            BuyingFacade.InvoiceDetailsBL().DeleteByPackage(invoiceNo, packageCode);
            return RedirectToAction("Index", new { invoiceNo = invoiceNo, packageCode = packageCode });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Finish(string invoiceNo, DateTime createDate, params Guid[] isCashes)
        {
            if (createDate == null)
                throw new ArgumentException("โปรดระบุวันที่ออกใบส่่งปัจจัยการผลิต");

            var invoiceItems = BuyingFacade.InvoiceDetailsBL().GetByInvoiceNo(invoiceNo);
            if (isCashes != null)
            {
                foreach (var item in invoiceItems)
                {
                    if (isCashes.Where(x => x == item.ConditionID).Count() > 0)
                        item.IsCash = true;
                    else
                        item.IsCash = false;
                }
            }

            foreach (var item in invoiceItems)
                BuyingFacade.InvoiceDetailsBL().ChangeIsCashStatus(item);

            BuyingFacade.InvoiceBL().Finish(invoiceNo, HttpContext.User.Identity.Name, createDate);
            return RedirectToAction("Index", new { invoiceNo = invoiceNo });
        }
    }
}