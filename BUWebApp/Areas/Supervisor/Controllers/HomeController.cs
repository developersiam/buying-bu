﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Supervisor.Controllers
{
    [Authorize(Roles = "Supervisor")]
    public class HomeController : Controller
    {
        // GET: Supervisor/Home
        public ActionResult Index()
        {
            //return RedirectToAction("Index","Farmer",new { extensionAgentCode = "" });
            return View();
        }
    }
}