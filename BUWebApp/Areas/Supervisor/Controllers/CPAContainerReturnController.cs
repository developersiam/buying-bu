﻿using BusinessLayer.Helper;
using BusinessLayer;
using BUWebApp.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using BUWebApp.Areas.Supervisor.Models;
using System.Runtime.InteropServices;
using BusinessLayer.Model;
using DomainModel;
using Microsoft.Ajax.Utilities;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Data;
using System.Drawing.Drawing2D;

namespace BUWebApp.Areas.Supervisor.Controllers
{
    [Authorize(Roles = "Supervisor")]
    public class CPAContainerReturnController : Controller
    {
        // GET: Admin/CPAContainerReturn
        public ActionResult Index(string citizenID)
        {
            if (string.IsNullOrEmpty(citizenID))
            {
                ModelState.AddModelError("CitizenID", "โปรดระบุรหัสประจำตัวประชาชน 13 หลักของชาวไร่");
                return View(new vm_CPAContainerReturn());
            }

            var crop = AppSetting.Crop.Crop1;
            var registerList = BuyingFacade.RegistrationBL().GetByCropAndCitizenID(crop, citizenID);
            if (registerList.Count() <= 0)
            {
                ModelState.AddModelError("CitizenID", "ไม่พบข้อมูลการลงทะเบียนกับตัวแทนใดๆ");
                return View(new vm_CPAContainerReturn());
            }

            var summaryList = CPAContainerHelper.GetByCitizenID(crop, citizenID)
                .GroupBy(x => new
                {
                    x.FarmerCode
                })
                .Select(x => new m_CPAContainerReceiveAndReturnSummary
                {
                    FarmerCode = x.Key.FarmerCode,
                    ReceiveQuantity = x.Sum(y => y.ReceiveQuantity),
                    ReturnQuantity = x.Sum(y => y.ReturnQuantity)
                })
                .ToList();

            ViewBag.ReturnSummary = summaryList;
            ViewBag.PersonProfile = BuyingFacade.PersonBL().GetSingle(citizenID);

            return View(new vm_CPAContainerReturn { CitizenID = citizenID });
        }

        [HttpGet]
        public ActionResult Return(string farmerCode)
        {
            if (string.IsNullOrEmpty(farmerCode))
                return RedirectToAction("Index");

            var crop = AppSetting.Crop.Crop1;
            var register = BuyingFacade.RegistrationBL()
                .GetSingle(crop, farmerCode);
            ViewBag.Register = register;

            var summaryList = CPAContainerHelper.GetByFarmerCode(crop, farmerCode)
                .GroupBy(x => new
                {
                    x.ReceiveID,
                    x.ItemCode,
                    x.ItemName,
                    x.Description,
                    x.UnitName,
                    x.ReceiveQuantity
                })
                .Select(x => new m_CPAContainerReceiveAndReturnSummary
                {
                    ReceiveID = x.Key.ReceiveID,
                    ItemCode = x.Key.ItemCode,
                    ItemName = x.Key.ItemName,
                    Description = x.Key.Description,
                    UnitName = x.Key.UnitName,
                    ReceiveQuantity = x.Key.ReceiveQuantity,
                    ReturnQuantity = x.Sum(y => y.ReturnQuantity)
                })
                .ToList();
            ViewBag.ReturnSummary = summaryList;

            var returnDetailsList = BuyingFacade.CPAContainerReturnBL()
                .GetByFarmerCode(crop, farmerCode);
            ViewBag.ReturnDetailsList = returnDetailsList;

            var returnItemList = BuyingFacade.CPAContainerItemBL()
                .GetAll()
                .Select(x => new CPAContainerItem
                {
                    Code = x.Code,
                    Name = x.Code + " / " + x.Name + " / " + x.Unit + " / " + x.Description
                })
                .OrderBy(x => x.Name)
                .ToList();
            ViewBag.ReturnItemList = returnItemList;

            return View(new vm_CPAContainerReturn
            {
                CitizenID = register.Farmer.CitizenID,
                FarmerCode = register.FarmerCode,
                IsEmpty = true
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(vm_CPAContainerReturn collation)
        {
            var crop = AppSetting.Crop.Crop1;
            BuyingFacade.CPAContainerReturnBL()
                .Add(collation.ReceiveID,
                collation.ReturnQuantity,
                collation.IsEmpty,
                HttpContext.User.Identity.Name);

            return RedirectToAction("Return", new { farmerCode = collation.FarmerCode });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Guid returnID, int quantity, bool isEmpty, string farmerCode)
        {
            BuyingFacade.CPAContainerReturnBL()
                .Edit(returnID, quantity, isEmpty, HttpContext.User.Identity.Name);
            return RedirectToAction("Return", new { farmerCode = farmerCode });
        }

        [HttpGet]
        public ActionResult Delete(Guid returnID, string farmerCode)
        {
            BuyingFacade.CPAContainerReturnBL().Delete(returnID);
            return RedirectToAction("Return", new { farmerCode = farmerCode });
        }

        [HttpGet]
        public ActionResult Print(string citizenID)
        {
            return RedirectToAction("RenderPDF", "CPAContainerReturn",
                new { citizenID = citizenID });
        }

        [HttpGet]
        public ActionResult RenderPDF(string citizenID)
        {
            try
            {
                if (string.IsNullOrEmpty(citizenID))
                    throw new ArgumentException("CitizenID cannot be empty.");

                var crop = AppSetting.Crop.Crop1;

                BuyingSystemDataSet.PersonDataTableDataTable personDT = new BuyingSystemDataSet.PersonDataTableDataTable();
                BuyingSystemDataSet.CPAContainerReturnDataTableDataTable returnDT = new BuyingSystemDataSet.CPAContainerReturnDataTableDataTable();
                BuyingSystemDataSetTableAdapters.PersonDataTableTableAdapter personTA = new BuyingSystemDataSetTableAdapters.PersonDataTableTableAdapter();
                BuyingSystemDataSetTableAdapters.CPAContainerReturnTableAdapter returnTA = new BuyingSystemDataSetTableAdapters.CPAContainerReturnTableAdapter();
                personTA.Fill(personDT, citizenID, crop);
                returnTA.Fill(returnDT, crop, citizenID);

                ReportDataSource personDS = new ReportDataSource();
                ReportDataSource returnDS = new ReportDataSource();
                personDS.Name = "PersonDataSet";
                returnDS.Name = "CPAContainerReturnDataSet";
                personDS.Value = personDT;
                returnDS.Value = returnDT;

                LocalReport report = new LocalReport();
                report.DataSources.Add(personDS);
                report.DataSources.Add(returnDS);
                report.ReportPath = Server.MapPath("~/Areas/Supervisor/RDLC/CPAContainerReturn.rdlc");

                string filePath = Path.GetTempFileName();
                Export(report, filePath);

                //Clost Report object.           
                report.Dispose();
                return File(filePath, "application/pdf");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Export(LocalReport report, string filePath)
        {
            string ack = "";
            try
            {
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;

                byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);
                using (FileStream stream = System.IO.File.OpenWrite(filePath))
                {
                    stream.Write(bytes, 0, bytes.Length);
                }
                return ack;
            }
            catch (Exception ex)
            {
                ack = ex.InnerException.Message;
                return ack;
            }
        }
    }
}