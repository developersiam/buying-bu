﻿using BusinessLayer;
using BusinessLayer.Helper;
using BUWebApp.Areas.Supervisor.Models;
using BUWebApp.Helper;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Supervisor.Controllers
{
    [Authorize(Roles = "Supervisor")]
    public class CY202201ReturnController : Controller
    {
        // GET: Supervisor/CY202201Return
        [HttpGet]
        public ActionResult Index(short crop, string farmerCode)
        {
            ViewBag.Crop = crop;
            ViewBag.Farmer = BuyingFacade.FarmerBL().GetByFarmerCode(farmerCode);
            ViewBag.ReturnNoList = BuyingFacade.CY202201ReturnBL().GetByFarmerCode(crop, farmerCode);
            return View(CY202201ReturnSummaryHelper.GetByFarmer(crop, farmerCode));
        }

        [HttpGet]
        public ActionResult Add(short crop, string farmerCode)
        {
            BuyingFacade.CY202201ReturnBL()
                .Add(crop, farmerCode, HttpContext.User.Identity.Name);
            return RedirectToAction("Index", new { crop = crop, farmerCode = farmerCode });
        }

        [HttpGet]
        public ActionResult Delete(short crop, string farmerCode, string returnNo)
        {
            BuyingFacade.CY202201ReturnBL().Delete(returnNo);
            return RedirectToAction("Index", new { crop = crop, farmerCode = farmerCode });
        }

        [HttpGet]
        public ActionResult Finish(string returnNo)
        {
            BuyingFacade.CY202201ReturnBL().Finish(returnNo, HttpContext.User.Identity.Name);
            return RedirectToAction("Index", "CY202201ReturnDetail", new { returnNo = returnNo });
        }

        [HttpGet]
        public ActionResult UnFinish(string returnNo)
        {
            BuyingFacade.CY202201ReturnBL().UnFinish(returnNo, HttpContext.User.Identity.Name);
            return RedirectToAction("Index","CY202201ReturnDetail", new { returnNo = returnNo });
        }
    }
}