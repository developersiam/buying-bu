﻿using BusinessLayer;
using BUWebApp.Areas.Supervisor.Models;
using BUWebApp.Helper;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLayer.Model;
using BusinessLayer.Helper;
using DomainModel;

namespace BUWebApp.Areas.Supervisor.Controllers
{
    [Authorize(Roles = "Supervisor")]
    public class CreditNoteController : Controller
    {
        [HttpGet]
        public ActionResult Index(string accountInvoiceNo)
        {
            var accountInvoice = BuyingFacade.AccountInvoiceBL().GetSingle(accountInvoiceNo);
            var invoice = BuyingFacade.InvoiceBL().GetSingle(accountInvoice.RefInvoiceNo);

            ViewBag.CreditNoteList = BuyingFacade.AccountCreditNoteBL().GetByAccountInvoice(accountInvoiceNo);
            ViewBag.InvoiceDetailList = BusinessLayer.Helper.InvoiceDetailsHelper.GetByAccountInvoiceNo(accountInvoiceNo);
            ViewBag.AccountInvoice = accountInvoice;
            ViewBag.Invoice = invoice;

            var model = new vm_CreditNote();

            model.AccountInvoice = accountInvoice;
            model.AccountInvoiceNo = accountInvoiceNo;
            model.CreateDate = DateTime.Now;
            model.CreditNoteList = BuyingFacade.AccountCreditNoteBL().GetByAccountInvoice(accountInvoiceNo);
            model.Farmer = BuyingFacade.FarmerBL().GetByFarmerCode(accountInvoice.Invoice.FarmerCode);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(vm_CreditNote collation)
        {
            var model = BuyingFacade.AccountInvoiceBL()
                .GetSingle(collation.AccountInvoiceNo);
            if (model == null)
                throw new ArgumentException("ไม่พบข้อมูล account invoice no #" + collation.AccountInvoiceNo + " ในระบบ");

            var creditNoteCode = BuyingFacade.AccountCreditNoteBL()
                .Add(collation.CreateDate,
                collation.AccountInvoiceNo,
                collation.IsPayForCash,
                collation.Description,
                HttpContext.User.Identity.Name);

            return RedirectToAction("Detail", "CreditNoteDetail",
                new { creditNoteCode = creditNoteCode, accountInvoiceNo = collation.AccountInvoiceNo });
        }

        [HttpGet]
        public ActionResult LockPrint(string creditNoteCode)
        {
            var model = BuyingFacade.AccountCreditNoteBL().GetSingle(creditNoteCode);
            if (model == null)
                throw new ArgumentException("ไม่พบข้อมูล credit note code #" + creditNoteCode + " ในระบบ");

            if (model.IsPayForCash == false)
                BuyingFacade.AccountCreditNoteBL()
                    .ReturnItemToAGMInventory(creditNoteCode, HttpContext.User.Identity.Name);

            BuyingFacade.AccountCreditNoteBL()
                .Lock(creditNoteCode, HttpContext.User.Identity.Name);
            return RedirectToAction("RenderPDF", "CreditNote",
                new { creditNoteCode = creditNoteCode });
        }

        [HttpGet]
        public ActionResult Delete(string creditNoteCode)
        {
            var model = BuyingFacade.AccountCreditNoteBL().GetSingle(creditNoteCode);
            if (model == null)
                throw new ArgumentException("ไม่พบข้อมูล credit note code #" + creditNoteCode + " ในระบบ");

            BuyingFacade.AccountCreditNoteBL().Delete(creditNoteCode);
            return RedirectToAction("Index", "CreditNote", new { accountInvoiceNo = model.AccountInvoiceNo });
        }

        [HttpGet]
        public ActionResult RenderPDF(string creditNoteCode)
        {
            try
            {
                var creditNote = BuyingFacade.AccountCreditNoteBL().GetSingle(creditNoteCode);
                if (creditNote == null)
                    throw new ArgumentException("ไม่พบข้อมูล creditnote " + creditNoteCode + " นี้ในระบบ");

                if ((DateTime.Now.DayOfYear - Convert.ToDateTime(creditNote.PrintDate).DayOfYear) > 1)
                    throw new ArgumentException("สามารถพิมพ์ใบลดหนี้ได้ภายในวันที่ยืนยันใบลดหนี้เท่านั้น");

                var farmer = BuyingFacade.FarmerBL()
                    .GetByFarmerCode(creditNote.AccountInvoice.Invoice.FarmerCode);
                if (farmer == null)
                    throw new ArgumentException("ไม่พบข้อมูล farmer นี้ในระบบ");

                ReportDataSource headerDS = new ReportDataSource();
                ReportDataSource DetailDS = new ReportDataSource();

                headerDS.Name = "CreditNoteHeaderDataSet";
                DetailDS.Name = "CreditNoteDetailDataSet";

                //Modified Report Data source (replace class with dataset).
                var dataTable = new BuyingSystemDataSet.CreditNoteDetailsByCreditNoteCodeDataTableDataTable();
                var tableAdapter = new BuyingSystemDataSetTableAdapters.CreditNoteDetailsByCreditNoteCodeTableAdapter();
                tableAdapter.Fill(dataTable, creditNoteCode);

                headerDS.Value = CreditNoteDetailHelper.GetHeader(creditNoteCode);
                DetailDS.Value = dataTable;
                //DetailDS.Value = CreditNoteDetailHelper.GetByCreditNoteCode(creditNoteCode);

                LocalReport report = new LocalReport();
                report.DataSources.Add(headerDS);
                report.DataSources.Add(DetailDS);

                if (creditNote.IsPayForCash == false)
                    report.ReportPath = Server.MapPath("~/Areas/Supervisor/RDLC/CreditNote.rdlc");
                else
                    report.ReportPath = Server.MapPath("~/Areas/Supervisor/RDLC/Receipt.rdlc");

                string filePath = Path.GetTempFileName();
                Export(report, filePath);

                //Clost Report object.           
                report.Dispose();
                return File(filePath, "application/pdf");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Export(LocalReport report, string filePath)
        {
            string ack = "";
            try
            {
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;

                byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);
                using (FileStream stream = System.IO.File.OpenWrite(filePath))
                {
                    stream.Write(bytes, 0, bytes.Length);
                }
                return ack;
            }
            catch (Exception ex)
            {
                ack = ex.InnerException.Message;
                return ack;
            }
        }

        [HttpGet]
        public ActionResult SearchFarmer(string farmerName)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var crop = AppSetting.Crop.Crop1;
            var areaCode = AppSetting.SupervisorUser.AreaCode;
            var farmerAccountInvoices = new List<m_FarmerAccountInvoice>();
            ViewBag.FarmerName = farmerName;
            ViewBag.FarmerAccountInvoices = farmerAccountInvoices;
            if (string.IsNullOrEmpty(farmerName))
                return View();

            var list = BuyingFacade.AccountInvoiceBL()
                .GetByFarmerName(crop, areaCode, farmerName);
            var farmers = BuyingFacade.FarmerBL()
                .GetByFarmerName(farmerName)
                .Where(x=>x.Supplier1.SupplierArea == areaCode)
                .ToList();

            var list1 = farmers
                .GroupBy(x => new
                {
                    x.CitizenID,
                    x.Person.Prefix,
                    x.Person.FirstName,
                    x.Person.LastName,
                })
                .Select(x => new m_FarmerAccountInvoice
                {
                    CitizenID = x.Key.CitizenID,
                    Prefix = x.Key.Prefix,
                    FirstName = x.Key.FirstName,
                    LastName = x.Key.LastName
                })
                .ToList();

            var list2 = new List<m_FarmerAccountInvoice>();
            foreach (var item in list1)
            {
                var str = list
                    .Where(x => x.Invoice.RegistrationFarmer.Farmer.CitizenID == item.CitizenID)
                    .Select(x => x.InvoiceNo)
                    .ToList();
                list2.Add(new m_FarmerAccountInvoice
                {
                    CitizenID = item.CitizenID,
                    Prefix = item.Prefix,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    AccountInvoiceNoList = str
                });
            }

            ViewBag.FarmerAccountInvoices = list2.ToList();
            return View();
        }
    }
}