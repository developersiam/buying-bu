﻿using BusinessLayer;
using BUWebApp.Helper;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BUWebApp.Areas.Supervisor.Models;
using BUWebApp.Models;

namespace BUWebApp.Areas.Supervisor.Controllers
{
    [Authorize(Roles = "Supervisor")]
    public class FarmerController : Controller
    {
        // GET: Supervisor/Farmer
        [HttpGet]
        public ActionResult Index(string extensionAgentCode)
        {
            var farmerList = new List<Farmer>();
            var extensionAgentList = new List<Supplier>();
            if (!string.IsNullOrEmpty(extensionAgentCode))
                farmerList = BuyingFacade.FarmerBL().GetBySupplierCode(extensionAgentCode);

            AppSetting.Username = HttpContext.User.Identity.Name;
            var supervisor = AppSetting.SupervisorUser;
            extensionAgentList = BuyingFacade.SupplierBL()
                .GetByArea(supervisor.AreaCode)
                .OrderBy(x => x.SupplierCode)
                .ToList();

            ViewBag.FarmerList = farmerList;
            ViewBag.ExtensionAgentList = extensionAgentList;
            ViewBag.ExtensionAgentCode = extensionAgentCode;

            return View();
        }

        [HttpGet]
        public ActionResult CheckProfile(string extensionAgentCode, string citizenID)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            ViewBag.ExtensionAgentList = BuyingFacade.SupplierBL()
                .GetByArea(AppSetting.SupervisorUser.AreaCode)
                .ToList();

            var model = new vm_CheckFarmerProfile
            {
                CitizenID = citizenID,
                ExtensionAgentCode = extensionAgentCode,
                Person = null,
                Farmer = null,
                RegistrationFarmer = null,
                FarmerList = new List<Farmer>(),
                CropSummaryList = new List<vm_FarmerContractHistory>()
            };

            if (!string.IsNullOrEmpty(citizenID))
            {
                var person = new Person();
                var farmer = new Farmer();
                var registration = new RegistrationFarmer();
                var farmerList = new List<Farmer>();
                var cropSummaryList = new List<vm_FarmerContractHistory>();

                person = BuyingFacade.PersonBL().GetSingle(model.CitizenID);
                farmerList = BuyingFacade.FarmerBL().GetByCitizenID(model.CitizenID);
                farmer = farmerList.SingleOrDefault(x => x.Supplier == model.ExtensionAgentCode);
                registration = new RegistrationFarmer();
                cropSummaryList = new List<vm_FarmerContractHistory>();
                if (farmer != null)
                    registration = BuyingFacade.RegistrationBL().GetSingle(AppSetting.Crop.Crop1, farmer.FarmerCode);

                if (farmerList.Count() > 0)
                    cropSummaryList = RegistrationFarmerHelper.GetContractHistoryByCitizenID(model.CitizenID);

                model.Person = person;
                model.Farmer = farmer;
                model.RegistrationFarmer = registration;
                model.FarmerList = farmerList;
                model.CropSummaryList = cropSummaryList;
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CheckProfile(vm_CheckFarmerProfile model)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            ViewBag.ExtensionAgentList = BuyingFacade.SupplierBL()
                .GetByArea(AppSetting.SupervisorUser.AreaCode)
                .ToList();

            var person = new Person();
            var farmer = new Farmer();
            var registration = new RegistrationFarmer();
            var farmerList = new List<Farmer>();
            var cropSummaryList = new List<vm_FarmerContractHistory>();
            model.Person = person;
            model.Farmer = farmer;
            model.RegistrationFarmer = registration;
            model.FarmerList = farmerList;
            model.CropSummaryList = cropSummaryList;

            if (!ModelState.IsValid)
                return View(model);

            if (!PersonHelper.CitizenIDIsValid(model.CitizenID))
            {
                ModelState.AddModelError("CitizenID", "รูปแบบรหัสประจำตัวประชาชนไม่ถูกต้อง");
                return View(model);
            }

            person = BuyingFacade.PersonBL().GetSingle(model.CitizenID);
            farmerList = BuyingFacade.FarmerBL().GetByCitizenID(model.CitizenID);
            farmer = farmerList.SingleOrDefault(x => x.Supplier == model.ExtensionAgentCode);
            cropSummaryList = new List<vm_FarmerContractHistory>();
            if (farmer != null)
                registration = BuyingFacade.RegistrationBL().GetSingle(AppSetting.Crop.Crop1, farmer.FarmerCode);

            if (farmerList.Count() > 0)
                cropSummaryList = RegistrationFarmerHelper.GetContractHistoryByCitizenID(model.CitizenID);

            model.Person = person;
            model.FarmerList = farmerList;
            model.Farmer = farmer;
            model.FarmerList = farmerList;
            model.RegistrationFarmer = registration;
            model.CropSummaryList = cropSummaryList;

            return View(model);
        }

        [HttpGet]
        public ActionResult AddNewProfile(string citizenID, string extensionAgentCode)
        {
            if (!PersonHelper.CitizenIDIsValid(citizenID))
                throw new ArgumentException("รูปแบบรหัสประจำตัวประชาชนไม่ถูกต้อง");

            ViewBag.MaritalStatusList = BuyingFacade.MaritalStatusBL().GetAllMaitalStatus();
            ViewBag.Provinces = BuyingFacade.ThailandGeoBL()
                .GetProvinces()
                .Where(x => x.ProvinceCode == "54" ||
                x.ProvinceCode == "51" ||
                x.ProvinceCode == "38")
                .Select(x => new SelectListItem
                {
                    Text = x.ProvinceNameThai.Replace(" ", ""),
                    Value = x.ProvinceNameThai.Replace(" ", "")
                });

            return View(new vm_Person
            {
                CitizenID = citizenID,
                BirthDate = DateTime.Now.Date,
                SpouseBirthDate = DateTime.Now.Date,
                ExtensionAgentCode = extensionAgentCode
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddNewProfile(vm_Person person)
        {
            ViewBag.MaritalStatusList = BuyingFacade.MaritalStatusBL().GetAllMaitalStatus();
            ViewBag.Provinces = BuyingFacade.ThailandGeoBL()
                .GetProvinces()
                .Where(x => x.ProvinceCode == "54" ||
                x.ProvinceCode == "51" ||
                x.ProvinceCode == "38")
                .Select(x => new SelectListItem
                {
                    Text = x.ProvinceNameThai.Replace(" ", ""),
                    Value = x.ProvinceNameThai.Replace(" ", "")
                });

            if (!ModelState.IsValid)
                return View(person);

            BuyingFacade.PersonBL()
                .Add(new Person
                {
                    CitizenID = person.CitizenID,
                    Prefix = person.Prefix,
                    FirstName = person.FirstName,
                    LastName = person.LastName,
                    BirthDate = person.BirthDate,
                    Village = person.Village,
                    HouseNumber = person.HouseNumber,
                    Tumbon = person.Tumbon,
                    Amphur = person.Amphur,
                    Province = person.Province,
                    TelephoneNumber = person.TelephoneNumber,
                    MaritalStatusCode = person.MaritalStatusCode,
                    SpousePrefix = person.SpousePrefix,
                    SpouseCitizenID = person.SpouseCitizenID,
                    SpouseFirstName = person.SpouseFirstName,
                    SpouseLastName = person.SpouseLastName,
                    SpouseBirthDate = person.SpouseBirthDate,

                    /// A new requirements from CY 2022.
                    /// 
                    StartAgriculture = person.StartAgriculture,
                    StartGrowingTobacco = person.StartGrowingTobacco,
                    YearsGrowingTobacco = person.YearsGrowingTobacco,
                    YearsInAgriculture = person.YearsInAgriculture,
                    ModifiedByUser = HttpContext.User.Identity.Name
                });
            return RedirectToAction("CheckProfile",
                new
                {
                    citizenID = person.CitizenID,
                    extensionAgentCode = person.ExtensionAgentCode
                });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddNewFarmer(string citizenID, string extensionAgentCode)
        {
            BuyingFacade.FarmerBL().Add(citizenID, extensionAgentCode);
            return RedirectToAction("CheckProfile", new { extensionAgentCode = extensionAgentCode, citizenID = citizenID });
        }

        [HttpGet]
        public ActionResult RegistrationForm(string farmerCode)
        {
            var model = new vm_RegistrationForm();
            if (string.IsNullOrEmpty(farmerCode))
                throw new ArgumentException("Farmer code cannot be empty.");

            var farmer = BuyingFacade.FarmerBL().GetByFarmerCode(farmerCode);
            if (farmer == null)
                throw new ArgumentException("ไม่พบข้อมูลชาวไร่รายนี้ในระบบ โปรดตรวจสอบ farmer code ในระบบ");

            var cropDef = AppSetting.Crop;
            if (cropDef == null)
                throw new ArgumentException("ไม่พบ crop default ในระบบ โปรดตรวจสอบการตั้งค่า crop กับ admin");

            var registration = BuyingFacade.RegistrationBL().GetSingle(cropDef.Crop1, farmerCode);
            var seedlingFromList = BuyingFacade.SeedlingFromBL().GetAll();
            var farmerSeedlingFromList = BuyingFacade.FarmerSeedlingFromBL().GetByCitizenID(farmer.CitizenID, cropDef.Crop1);
            var irrigationWaterTypeList = BuyingFacade.IIrrigationWaterBL().GetIrrigationWaterTypes();
            var irrigationWaterList = BuyingFacade.IIrrigationWaterBL().GetByCitizenID(farmer.CitizenID, cropDef.Crop1);
            var transplantingPeriodList = BuyingFacade.EstimateTransplantingPeriodBL().GetAll();
            var farmerTransplantingPeriodList = BuyingFacade.FarmerEstimateTransplantingPeriodBL().GetByCitizenID(cropDef.Crop1, farmer.CitizenID);
            var farmerSuccessor = BuyingFacade.FarmerSuccessorBL().GetSingle(farmer.CitizenID, cropDef.Crop1);

            IEnumerable<m_HasSuccessor> hassuccessorList = new List<m_HasSuccessor> {
                new m_HasSuccessor {
                    Value = false,
                    Name = "ไม่มี",
                },
                new m_HasSuccessor {
                    Value = true,
                    Name = "มี"
                }};

            IEnumerable<m_UseIrrigationLicense> irrigationLicenses = new List<m_UseIrrigationLicense> {
                new m_UseIrrigationLicense {
                    Value = false,
                    Name = "ไม่มี"
                },
                new m_UseIrrigationLicense {
                    Value = true,
                    Name = "มี"
                }};

            IEnumerable<IrrigationWaterType> irrigrationWaterTypes = new List<IrrigationWaterType>();
            irrigrationWaterTypes = irrigationWaterTypeList;

            ViewBag.IrrigationWaterTypeList = irrigrationWaterTypes;
            ViewBag.HasSuccessorList = hassuccessorList;
            ViewBag.UseIrrigationLicenseList = irrigationLicenses;

            AppSetting.Username = HttpContext.User.Identity.Name;
            ViewBag.ExtensionAgentList = BuyingFacade.SupplierBL()
                .GetByArea(AppSetting.SupervisorUser.AreaCode)
                .ToList();

            ViewBag.EstimateTransplantingList = transplantingPeriodList
                .Select(x => new SelectListItem()
                {
                    Selected = farmerTransplantingPeriodList
                    .Where(y => y.EstimateTransplantingPeriodID == x.EstimateTransplantingPeriodID)
                    .Count() > 0 ? true : false,
                    Text = x.EstimateTransplantingPeriodName,
                    Value = x.EstimateTransplantingPeriodID.ToString()
                }); ;

            ViewBag.SeedlingFormList = seedlingFromList
                .Select(x => new SelectListItem()
                {
                    Selected = farmerSeedlingFromList
                    .Where(y => y.SeedlingFromID == x.ID)
                    .Count() > 0 ? true : false,
                    Text = x.SeedlingFrom1,
                    Value = x.ID.ToString()
                });

            IEnumerable<SelectListItem> unRegisterReasonList =
                new List<SelectListItem> {
                    new SelectListItem{ Value = "มีปัญหาสุขภาพ",Text="มีปัญหาสุขภาพ" },
                    new SelectListItem{ Value = "ไม่มีพื้นที่เพาะปลูก",Text="ไม่มีพื้นที่เพาะปลูก" },
                    new SelectListItem{ Value = "ต้องการปลูกพืชอื่น",Text="ต้องการปลูกพืชอื่น" },
                    new SelectListItem{ Value = "ขาดแคลนแรงงาน",Text="ขาดแคลนแรงงาน" },
                    new SelectListItem{ Value = "ขาดทุนทรัพย์",Text="ขาดทุนทรัพย์" },
                    new SelectListItem{ Value = "กลัวภัยธรรมชาติ (ฝนตก น้ำท่วม ภัยแล้ง)",Text="กลัวภัยธรรมชาติ (ฝนตก น้ำท่วม ภัยแล้ง)" },
                    new SelectListItem{ Value = "ไม่มั่นใจนโยบายของบริษัทฯ",Text="ไม่มั่นใจนโยบายของบริษัทฯ" },
                    new SelectListItem{ Value = "อื่นๆ",Text="อื่นๆ" }
                };

            ViewBag.UnRegisterReasonList = unRegisterReasonList
                .Select(x => new SelectListItem()
                {
                    Value = x.Value,
                    Text = x.Text,
                    Selected = x.Value == (registration == null ? "" : registration.UnRegisterReason)
                });

            model.Person = farmer.Person;
            model.Crop = cropDef.Crop1;
            model.FarmerCode = farmerCode;
            model.FirstName = farmer.Person.FirstName;
            model.LastName = farmer.Person.LastName;
            model.TelephoneNumber = farmer.Person.TelephoneNumber;
            model.CitizenID = farmer.CitizenID;
            model.Farmer = farmer;
            model.Registration = registration;
            model.ExtensionAgentCode = farmer.Supplier;
            model.IrrigationWaterList = irrigationWaterList;
            model.SignContractDate = registration == null ? DateTime.Now : (DateTime)registration.SignContractDate;
            model.QuotaFromSignContract = registration == null ? 0 : registration.QuotaFromSignContract;
            model.HasSuccessor = farmerSuccessor == null ? "ไม่มี" : farmerSuccessor.Successor == true ? "มี" : "ไม่มี";
            model.SelectedSeedlingFromList = farmerSeedlingFromList;
            model.SelectedPeriodList = farmerTransplantingPeriodList;
            model.UnRegisterReason = registration == null ? "" : registration.UnRegisterReason;
            model.ActiveStatus = registration == null ? false : registration.ActiveStatus;
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration(vm_RegistrationForm model, Guid[] SelectedPeriodList, Guid[] SelectedSeedlingFromList)
        {
            var person = BuyingFacade.PersonBL().GetSingle(model.CitizenID);
            var cropDefault = AppSetting.Crop.Crop1;
            var user = HttpContext.User.Identity.Name;

            person.FirstName = model.FirstName;
            person.LastName = model.LastName;
            person.TelephoneNumber = model.TelephoneNumber;
            person.ModifiedByUser = HttpContext.User.Identity.Name;
            person.LastModified = DateTime.Now;
            BuyingFacade.PersonBL().Update(person);

            var registration = BuyingFacade.RegistrationBL().GetSingle(cropDefault, model.FarmerCode);
            if (registration == null)
            {
                BuyingFacade.RegistrationBL()
                    .SignContractV2023(model.FarmerCode,
                    (short)model.QuotaFromSignContract,
                    model.SignContractDate, user);
            }
            else
            {
                if (registration.QuotaFromSignContract != model.QuotaFromSignContract ||
                    registration.SignContractDate != model.SignContractDate)
                    BuyingFacade.RegistrationBL()
                        .UpdateSignContract(cropDefault,
                        model.FarmerCode,
                        (short)model.QuotaFromSignContract,
                        model.SignContractDate,                        
                        "Recommender",//ใส่เพื่อให้ฟังก์ชันสามารถรันผ่านได้เพื่อให้รองรับการขยายในปี 2024
                        user);
            }

            var successor = BuyingFacade.FarmerSuccessorBL().GetSingle(model.CitizenID, cropDefault);
            var successorFlag = model.HasSuccessor.Contains("ไม่") ? false : true;
            if (successor == null)
            {
                BuyingFacade.FarmerSuccessorBL()
                    .Add(model.CitizenID, successorFlag, cropDefault, user);
            }
            else
            {
                if (successor.Successor != successorFlag)
                    BuyingFacade.FarmerSuccessorBL()
                        .Update(model.CitizenID, successorFlag, cropDefault, user);
            }

            var periodList = BuyingFacade.FarmerEstimateTransplantingPeriodBL().GetByCitizenID(cropDefault, model.CitizenID);
            var seedlingList = BuyingFacade.FarmerSeedlingFromBL().GetByCitizenID(model.CitizenID, cropDefault);

            foreach (var item in periodList)
                BuyingFacade.FarmerEstimateTransplantingPeriodBL()
                    .Delete(cropDefault, model.CitizenID, item.EstimateTransplantingPeriodID);

            foreach (var item in seedlingList)
                BuyingFacade.FarmerSeedlingFromBL()
                    .Delete(model.CitizenID, item.SeedlingFromID, cropDefault);

            foreach (var item in SelectedSeedlingFromList)
                BuyingFacade.FarmerSeedlingFromBL().Add(model.CitizenID, item, cropDefault, user);

            foreach (var item in SelectedPeriodList)
                BuyingFacade.FarmerEstimateTransplantingPeriodBL()
                    .Add(new FarmerEstimateTransplantingPeriod
                    {
                        CitizenID = model.CitizenID,
                        EstimateTransplantingPeriodID = item,
                        Crop = cropDefault,
                        ModifiedUser = user,
                        ModifiedDate = DateTime.Now,
                    });

            return RedirectToAction("CheckProfile",
                new
                {
                    citizenID = model.CitizenID,
                    extensionAgentCode = model.ExtensionAgentCode
                });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UnRegister(string farmerCode, string unRegisterReason)
        {
            BuyingFacade.RegistrationBL().UnRegister(farmerCode, unRegisterReason, HttpContext.User.Identity.Name);
            return RedirectToAction("RegistrationForm", "Farmer", new { farmerCode = farmerCode });
        }

        [HttpGet]
        public ActionResult CancelUnRegister(string farmerCode)
        {
            BuyingFacade.RegistrationBL().CancelUnRegister(farmerCode, HttpContext.User.Identity.Name);
            return RedirectToAction("RegistrationForm", "Farmer", new { farmerCode = farmerCode });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddIrrigationWater(vm_RegistrationForm model)
        {
            BuyingFacade.IIrrigationWaterBL()
                .Add(new IrrigationWater
                {
                    FarmerIrrigationWaterID = Guid.NewGuid(),
                    CitizenID = model.CitizenID,
                    Crop = model.Crop,
                    IrrigationWaterTypeID = model.IrrigationWaterTypeID,
                    IrrigationWaterDeep = model.IrrigationWaterDeep,
                    RiverName = model.RiverName,
                    ModifiedBy = HttpContext.User.Identity.Name,
                    ModifiedDate = DateTime.Now,
                    GroundWaterLicence = model.GroundWaterLicence,
                    GroundWaterLicenceRemark = model.GroundWaterLicenceRemark
                });
            return RedirectToAction("RegistrationForm", new { farmerCode = model.FarmerCode });
        }

        [HttpGet]
        public ActionResult DeleteIrrigrationWater(Guid id, string farmerCode)
        {
            BuyingFacade.IIrrigationWaterBL().Delete(id);
            return RedirectToAction("RegistrationForm", new { farmerCode = farmerCode });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddEstimateTransplanting(Guid estimateTransplantingPeriodID, string citizenID, short crop, string farmerCode)
        {
            BuyingFacade.FarmerEstimateTransplantingPeriodBL()
                .Add(new FarmerEstimateTransplantingPeriod
                {
                    EstimateTransplantingPeriodID = estimateTransplantingPeriodID,
                    CitizenID = citizenID,
                    Crop = crop,
                    ModifiedUser = HttpContext.User.Identity.Name,
                    ModifiedDate = DateTime.Now
                });
            return RedirectToAction("RegistrationForm", new { farmerCode = farmerCode });
        }

        [HttpGet]
        public ActionResult DeleteEstimateTransplanting(Guid estimateTransplantingPeriodID, string citizenID, short crop, string farmerCode)
        {
            BuyingFacade.FarmerEstimateTransplantingPeriodBL().Delete(crop, citizenID, estimateTransplantingPeriodID);
            return RedirectToAction("RegistrationForm", new { farmerCode = farmerCode });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddSeedlingForm(Guid SeedlingFromID, string citizenID, short crop, string farmerCode)
        {
            BuyingFacade.FarmerSeedlingFromBL().Add(citizenID, SeedlingFromID, crop, farmerCode);
            return RedirectToAction("RegistrationForm", new { farmerCode = farmerCode });
        }

        [HttpGet]
        public ActionResult DeleteSeedlingForm(Guid seedlingFormID, string citizenID, short crop, string farmerCode)
        {
            BuyingFacade.FarmerSeedlingFromBL().Delete(citizenID, seedlingFormID, crop);
            return RedirectToAction("RegistrationForm", new { farmerCode = farmerCode });
        }

        [HttpGet]
        public ActionResult UpdateProfile(string citizenID, string extensionAgentCode, string farmerCode)
        {
            var person = BuyingFacade.PersonBL().GetSingle(citizenID);
            if (person == null)
                throw new ArgumentException("ไม่พบข้อมูลชาวไร่ รหัสประจำตัวประชาชน " + citizenID + " นี้ในระบบ");

            AppSetting.Username = HttpContext.User.Identity.Name;
            ViewBag.MaritalStatusList = BuyingFacade.MaritalStatusBL().GetAllMaitalStatus();
            ViewBag.Provinces = BuyingFacade.ThailandGeoBL()
                .GetProvinces()
                .Where(x => x.ProvinceCode == "54" ||
                x.ProvinceCode == "51" ||
                x.ProvinceCode == "38")
                .Select(x => new SelectListItem
                {
                    Text = x.ProvinceNameThai.Replace(" ", ""),
                    Value = x.ProvinceNameThai.Replace(" ", ""),
                    Selected = x.ProvinceNameThai.Contains(person.Province) ? true : false
                });

            ViewBag.Amphurs = BuyingFacade.ThailandGeoBL()
                .GetDistrictByProvinceName(person.Province)
                .Select(x => new SelectListItem
                {
                    Text = x.DistrictNameThai.Replace(" ", ""),
                    Value = x.DistrictNameThai.Replace(" ", ""),
                    Selected = x.DistrictNameThai.Contains(person.Amphur) ? true : false
                }).ToList();

            ViewBag.Tumbons = BuyingFacade.ThailandGeoBL()
                .GetSubDistrictByDistrictName(person.Amphur)
                .Select(x => new SelectListItem
                {
                    Text = x.SubDistrictNameThai.Replace(" ", ""),
                    Value = x.SubDistrictNameThai.Replace(" ", ""),
                    Selected = x.SubDistrictNameThai.Contains(person.Tumbon) ? true : false
                }).ToList();

            ViewBag.FarmerCode = farmerCode;

            var birthYear = person.BirthDate.Year;
            return View(new vm_Person
            {
                CitizenID = person.CitizenID,
                Prefix = person.Prefix,
                FirstName = person.FirstName,
                LastName = person.LastName,
                BirthDate = person.BirthDate,
                Village = person.Village,
                HouseNumber = person.HouseNumber,
                Tumbon = person.Tumbon,
                Amphur = person.Amphur,
                Province = person.Province,
                TelephoneNumber = person.TelephoneNumber,
                MaritalStatusCode = person.MaritalStatusCode,
                SpousePrefix = person.SpousePrefix,
                SpouseCitizenID = person.SpouseCitizenID,
                SpouseFirstName = person.SpouseFirstName,
                SpouseLastName = person.SpouseLastName,
                SpouseBirthDate = person.SpouseBirthDate,
                StartAgriculture = person.StartAgriculture == null ? Convert.ToInt16(birthYear + 18) : Convert.ToInt16(person.StartAgriculture),
                StartGrowingTobacco = person.StartGrowingTobacco == null ? Convert.ToInt16(birthYear + 18) : Convert.ToInt16(person.StartGrowingTobacco),
                YearsGrowingTobacco = person.YearsGrowingTobacco,
                YearsInAgriculture = person.YearsInAgriculture,
                ExtensionAgentCode = extensionAgentCode
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateProfile(vm_Person model, string farmerCode)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            ViewBag.MaritalStatusList = BuyingFacade.MaritalStatusBL().GetAllMaitalStatus();
            ViewBag.Provinces = BuyingFacade.ThailandGeoBL()
                .GetProvinces()
                .Where(x => x.ProvinceCode == "54" ||
                x.ProvinceCode == "51" ||
                x.ProvinceCode == "38")
                .Select(x => new SelectListItem
                {
                    Text = x.ProvinceNameThai.Replace(" ", ""),
                    Value = x.ProvinceNameThai.Replace(" ", "")
                });

            ViewBag.Amphur = BuyingFacade.ThailandGeoBL()
                .GetDistrictByProvinceName(model.Province)
                .Select(x => new SelectListItem
                {
                    Text = x.DistrictNameThai.Replace(" ", ""),
                    Value = x.DistrictNameThai.Replace(" ", ""),
                    Selected = x.DistrictNameThai.Contains(model.Amphur) ? true : false
                });

            ViewBag.Tumbon = BuyingFacade.ThailandGeoBL()
                .GetSubDistrictByDistrictName(model.Amphur)
                .Select(x => new SelectListItem
                {
                    Text = x.SubDistrictNameThai.Replace(" ", ""),
                    Value = x.SubDistrictNameThai.Replace(" ", ""),
                    Selected = x.SubDistrictNameThai.Contains(model.Tumbon) ? true : false
                });

            ViewBag.FarmerCode = farmerCode;

            if (!ModelState.IsValid)
                return View(model);

            //Update farmer profile.
            BuyingFacade.PersonBL()
                .Update(new Person
                {
                    CitizenID = model.CitizenID,
                    Prefix = model.Prefix,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    BirthDate = model.BirthDate,
                    Village = model.Village,
                    HouseNumber = model.HouseNumber,
                    Tumbon = model.Tumbon,
                    Amphur = model.Amphur,
                    Province = model.Province,
                    TelephoneNumber = model.TelephoneNumber,
                    MaritalStatusCode = model.MaritalStatusCode,
                    SpousePrefix = model.SpousePrefix,
                    SpouseFirstName = model.SpouseFirstName,
                    SpouseLastName = model.SpouseLastName,
                    SpouseCitizenID = model.SpouseCitizenID,
                    SpouseBirthDate = model.SpouseBirthDate,

                    /// A new requirements from CY 2022.
                    /// 
                    StartAgriculture = model.StartAgriculture,
                    StartGrowingTobacco = model.StartGrowingTobacco,

                    YearsGrowingTobacco = model.YearsGrowingTobacco,
                    YearsInAgriculture = model.YearsInAgriculture,
                    ModifiedByUser = AppSetting.Username
                });

            return RedirectToAction("Index", "Farmer", new { extensionAgentCode = model.ExtensionAgentCode });
        }
    }
}
