﻿using BusinessLayer;
using BusinessLayer.Helper;
using BusinessLayer.Model;
using BUWebApp.Areas.Supervisor.Models;
using BUWebApp.Helper;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Supervisor.Controllers
{
    [Authorize(Roles = "Supervisor")]
    public class CreditNoteDetailController : Controller
    {
        [HttpGet]
        public ActionResult Detail(string creditNoteCode, string accountInvoiceNo)
        {
            var model = new vm_CreditNoteDetail();
            var creditNoteDetailList = new List<m_CreditNoteDetail>();

            var accountInvoice = BuyingFacade.AccountInvoiceBL().GetSingle(accountInvoiceNo);
            if (accountInvoice == null)
                throw new ArgumentException("ไม่พบข้อมูล account invoice นี้ในระบบ");

            model.AccountInvoiceNo = accountInvoiceNo;
            model.AccountInvoiceDate = accountInvoice.CreateDate;

            var farmer = BuyingFacade.FarmerBL().GetByFarmerCode(accountInvoice.Invoice.FarmerCode);
            model.CustomerName = farmer.Person.Prefix + farmer.Person.FirstName + "  " + farmer.Person.LastName;
            model.CustomerAddress = farmer.Person.HouseNumber + " หมู่ " +
                farmer.Person.Village + " ต." +
                farmer.Person.Tumbon + " อ." +
                farmer.Person.Amphur + " จ." +
                farmer.Person.Province;
            model.TaxID = farmer.CitizenID;

            var creditNote = BuyingFacade.AccountCreditNoteBL().GetSingle(creditNoteCode);
            if (creditNote == null)
                throw new ArgumentException("ไม่พบข้อมูล credit note นี้ในระบบ");

            model.CreditNoteCode = creditNote.CreditNoteCode;
            model.CreateDate = creditNote.CreateDate;
            model.IsPayForCash = creditNote.IsPayForCash;
            model.IsPrinted = creditNote.IsPrinted;
            model.PrintDate = creditNote.PrintDate;
            model.CreditNoteDetailList = CreditNoteDetailHelper.GetByCreditNoteCode(creditNoteCode);
            model.Description = creditNote.Description;

            var a1 = InvoiceDetailsHelper.GetByAccountInvoiceNo(accountInvoiceNo);
            var b1 = CreditNoteDetailHelper.GetByAccountInvoice(model.AccountInvoiceNo);
            var join1 = (from a in a1
                         join b in b1
                         on a.ConditionID equals b.ConditionID
                         into c
                         from result in c.DefaultIfEmpty()
                         select new m_CreditNoteDetail
                         {
                             ConditionID = a.ConditionID,
                             ItemID = a.ItemID,
                             ItemName = a.ItemName,
                             Brand = a.Brand,
                             Unit = a.Unit,
                             UnitPrice = a.UnitPrice,
                             Quantity = a.Quantity,
                             Price = a.Price,
                             ReturnQuantity = result == null ? 0 : result.ReturnQuantity,
                             ReturnPrice = result == null ? 0 : result.ReturnPrice
                         })
                         .GroupBy(x => new
                         {
                             ConditionID = x.ConditionID,
                             CreditNoteCode = x.CreditNoteCode,
                             ItemID = x.ItemID,
                             ItemName = x.ItemName,
                             Brand = x.Brand,
                             Unit = x.Unit,
                             UnitPrice = x.UnitPrice,
                             Quantity = x.Quantity,
                             ReturnQuantity = x.ReturnQuantity,
                             Price = x.Price,
                             ReturnPrice = x.ReturnPrice
                         })
                         .Select(x => new m_CreditNoteDetail
                         {
                             ConditionID = x.Key.ConditionID,
                             CreditNoteCode = x.Key.CreditNoteCode,
                             ItemID = x.Key.ItemID,
                             ItemName = x.Key.ItemName,
                             Brand = x.Key.Brand,
                             Unit = x.Key.Unit,
                             UnitPrice = x.Key.UnitPrice,
                             Quantity = x.Key.Quantity,
                             ReturnQuantity = x.Key.ReturnQuantity,
                             Price = x.Key.Price,
                             ReturnPrice = x.Key.ReturnPrice
                         }).ToList();

            var join2 = (from a in join1
                         join b in BuyingFacade.AccountCreditNoteDetailBL()
                         .GetByCreditNoteCode(creditNoteCode)
                         on a.ConditionID equals b.ConditionID
                         into c
                         from result in c.DefaultIfEmpty()
                         select new m_CreditNoteDetail
                         {
                             ConditionID = a.ConditionID,
                             CreditNoteCode = result == null ? "NotIn" : "In",
                             ItemID = a.ItemID,
                             ItemName = a.ItemName,
                             Brand = a.Brand,
                             Unit = a.Unit,
                             UnitPrice = a.UnitPrice,
                             Quantity = a.Quantity,
                             ReturnQuantity = a.ReturnQuantity,
                             Price = a.Price,
                             ReturnPrice = a.ReturnPrice,
                             Description = result != null ? "Can Delete" : "Cannot Delete"
                         }).ToList();

            model.InvoiceDetailList = join2;

            /// รายการของที่ลดหนี้ทั้งหมดใน acount invoice นี้
            var creditNoteByAccountInvoiceList = BuyingFacade.AccountCreditNoteBL().GetByAccountInvoice(creditNote.AccountInvoiceNo);

            /// วันที่พิมพ์เอกสารล่าสุด หากไม่มีให้ใช้วันที่ปัจจุบันแทนเพื่อเลือกข้อมูลใบลดหนี้ที่ผ่านมา
            var printDate = creditNote.PrintDate == null ? DateTime.Now : creditNote.PrintDate;
            /// รายการของก่อนทำการลดหนี้ใน creditnote นี้
            var accumulativeList = creditNoteByAccountInvoiceList.Where(x => x.PrintDate < printDate).ToList();
            /// ราคารวมของการลดหนี้ ไม่รวมกับใน creditnote นี้เพื่อหามูลค่าตามเอกสารเดิม
            var accumulativePrice = accumulativeList.Sum(x => x.AccountCreditNoteDetails.Sum(y => y.Quantity * y.UnitPrice));
            /// มูลค่าของใน account invoice ทั้งหมด
            var accountInvoicePrice = BuyingFacade.InvoiceDetailsBL().GetByAccountInvoiceNo(creditNote.AccountInvoiceNo)
                .Sum(x => x.Quantity * x.UnitPrice);
            /// มูลค่าของใน creditnote นี้
            var currentDocPrice = model.CreditNoteDetailList.Sum(x => x.Quantity * x.UnitPrice);

            //ใช้แสดงมูลค่ายอดค้างชำระยกมา
            model.DistibutionValue = accountInvoicePrice - accumulativePrice;

            ///กรณีไม่มีข้อมูลใน List ไม่ต้องคำนวณ Amount เพื่อไม่ให้ error
            if (model.CreditNoteDetailList != null)
            {
                model.Amount = currentDocPrice;
                model.Vat = accountInvoiceNo.Contains("V") ? model.Amount * Convert.ToDecimal(0.07) : 0;
                model.NetAmount = model.Amount;
            }

            ViewBag.AccountInvoice = accountInvoice;
            ViewBag.CreditNoteDetailByAccountInvoice = CreditNoteDetailHelper.GetByAccountInvoice(accountInvoiceNo);

            /// Basket return promotion.
            /// 
            //var basketItemList = BuyingFacade.CY202201ReturnItemBL().GetAll()
            //    .Where(x => x.ItemType == "ตะกร้า");
            //ViewBag.BasketItemList = (from a in basketItemList
            //                          from b in a1
            //                          where a.ReturnItemID == b.ItemID
            //                          select new m_InvoiceDetails
            //                          {
            //                              ConditionID = b.ConditionID,
            //                              ItemID = a.ReturnItemID
            //                          })
            //                          .ToList();
            //ViewBag.PromotionCalculate = CY202201ReturnSummaryHelper
            //    .GetByFarmer(AppSetting.Crop.Crop1, farmer.FarmerCode);

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(vm_CreditNoteDetail collation)
        {
            BuyingFacade.AccountCreditNoteDetailBL()
                    .Add(collation.CreditNoteCode,
                    collation.ConditionID,
                    collation.Quantity,
                    DateTime.Now,
                    HttpContext.User.Identity.Name);

            return RedirectToAction("Detail",
                new
                {
                    creditNoteCode = collation.CreditNoteCode,
                    accountInvoiceNo = collation.AccountInvoiceNo
                });
        }

        [HttpGet]
        public ActionResult Delete(string creditNoteCode, Guid conditionID, string accountInvoiceNo)
        {
            var deleteItem = BuyingFacade.AccountCreditNoteDetailBL().GetSingleByItem(creditNoteCode, conditionID);
            BuyingFacade.AccountCreditNoteDetailBL().Delete(deleteItem.ID);
            return RedirectToAction("Detail",
                new
                {
                    creditNoteCode = creditNoteCode,
                    accountInvoiceNo = accountInvoiceNo
                });
        }
    }
}