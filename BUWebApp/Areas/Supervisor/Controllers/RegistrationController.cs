﻿using BUWebApp.Areas.Supervisor.Models;
using BusinessLayer;
using BUWebApp.Helper;
using BUWebApp.Models;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLayer.BL;
using BusinessLayer.Helper;
using Microsoft.Reporting.WebForms;
using System.IO;
using Antlr.Runtime;
using BUWebApp.Areas.Supervisor.Data;

namespace BUWebApp.Areas.Supervisor.Controllers
{
    [Authorize(Roles = "Supervisor")]
    public class RegistrationController : Controller
    {
        [HttpGet]
        public ActionResult Index(string supplierCode)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supervisor = AppSetting.SupervisorUser;
            var _crop = AppSetting.Crop;

            ViewBag.SupervisorArea = _supervisor.AreaCode;
            ViewBag.SupplierCode = supplierCode;
            ViewBag.SupplierList = BuyingFacade.SupplierBL().GetByArea(_supervisor.AreaCode);

            if (supplierCode == "" || supplierCode == null)
            {
                return View(new List<vm_RegistrationFarmer>());
            }

            return View(RegistrationFarmerHelper
                .GetBySupplierCode(_crop.Crop1, supplierCode));
        }

        [HttpGet]
        public ActionResult Approve(short crop, string farmerCode)
        {
            var model = BuyingFacade.RegistrationBL().GetSingle(crop, farmerCode);
            if (model == null)
                throw new ArgumentNullException("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รายนี้! (farmer code : " + farmerCode + ")");

            var regisList = BuyingFacade.RegistrationBL()
                                    .GetByCropAndCitizenID(crop, model.Farmer.CitizenID);

            /// หากมีการลงทะเบียนซ้ำตั้งแต่ 2 ตัวแทนขึ้นไปให้โชว์ไปที่หน้ารายละเอียดการลงทะเบียนซ้ำมากกว่า 2 ตัวแทน
            /// 
            //if (regisList.Where(x => x.ApproveFromSTECStatus == true).Count() >= 2)
            //    return RedirectToAction("DupplicateDetails",
            //        "Registration", new { citizenID = model.Farmer.CitizenID });

            BuyingFacade.RegistrationBL()
                .ApproveSTECQuota(crop,
                farmerCode,
                Convert.ToInt16(model.QuotaFromSupplierApprove),
                HttpContext.User.Identity.Name);

            return RedirectToAction("Index", new { supplierCode = model.Farmer.Supplier });
        }

        [HttpPost]
        public ActionResult ApproveAll(string supplierCode)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supervisor = AppSetting.SupervisorUser;
            var _crop = AppSetting.Crop;

            var dupplicateList = BuyingFacade.RegistrationBL()
                .GetMoreThanTwoSupplierByArea(
                _crop.Crop1,
                _supervisor.AreaCode);

            ///เอามาเฉพาะในรายที่ผ่านการอนุมัติโควต้าจาก Supplier แล้วเท่านั้น 
            ///อันไหนที่ยังไม่อนุมัติให้รอจนกว่า Supplier จะอนุมัติ (ApproveFromSupplierStatus = true)
            ///
            foreach (RegistrationFarmer item in BuyingFacade.RegistrationBL()
                .GetBySupplier(_crop.Crop1, supplierCode)
                .Where(reg => reg.ApproveFromSupplierStatus == true))
            {
                ///ชาวไร่ที่ลงทะเบียนกับแทนมากกว่า 2 ตัวแทนขึ้นไปให้ข้ามการอนุมัติไป
                ///
                if (dupplicateList.Where(x => x.CitizenID == item.Farmer.CitizenID).Count() > 0)
                    continue;

                if (item.ApproveFromSTECStatus == false && item.QuotaFromSupplierApprove > 0)
                    BuyingFacade.RegistrationBL()
                        .ApproveSTECQuota(item.Crop,
                        item.FarmerCode,
                        Convert.ToInt16(item.QuotaFromSupplierApprove),
                        HttpContext.User.Identity.Name);
            }
            return RedirectToAction("Index", new { supplierCode = supplierCode });
        }

        [HttpGet]
        public ActionResult CancelApprove(short crop, string farmerCode)
        {
            var model = BuyingFacade.RegistrationBL().GetSingle(crop, farmerCode);
            if (model == null)
                throw new ArgumentNullException("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รายนี้! (farmer code : " + farmerCode + ")");
            var regisList = BuyingFacade.RegistrationBL()
                                    .GetByCropAndCitizenID(crop, model.Farmer.CitizenID);

            BuyingFacade.RegistrationBL()
                .CancelSTECQuota(crop,
                farmerCode,
                HttpContext.User.Identity.Name);

            return RedirectToAction("Index", new { supplierCode = model.Farmer.Supplier });
        }

        [HttpPost]
        public ActionResult CancelApproveAll(string supplierCode)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supervisor = AppSetting.SupervisorUser;
            var _crop = AppSetting.Crop;

            /// เอามาเฉพาะในรายที่ผ่านการอนุมัติโควต้าจาก STEC แล้วเท่านั้น 
            /// อันไหนที่ยังไม่อนุมัติให้เว้นไป (ApproveFromSTECStatus = true)
            /// 
            foreach (RegistrationFarmer item in BuyingFacade.RegistrationBL()
                .GetBySupplier(_crop.Crop1, supplierCode)
                .Where(reg => reg.ApproveFromSTECStatus == true && reg.SignContractDate == null))
            {
                BuyingFacade.RegistrationBL()
                    .CancelSTECQuota(item.Crop, item.FarmerCode, AppSetting.Username);
            }

            return RedirectToAction("Index", new { supplierCode = supplierCode });
        }

        [HttpGet]
        public ActionResult Edit(short crop, string farmerCode)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supervisor = AppSetting.SupervisorUser;
            var _crop = AppSetting.Crop;

            var farmer = RegistrationFarmerHelper.GetByFarmerCode(crop, farmerCode);
            var registerList = BuyingFacade.RegistrationBL()
                .GetByCropAndCitizenID(crop, farmer.Person.CitizenID);

            ViewBag.ReturnToAction = "Index";// registerList.Count() < 3 ? "ApproveSTECQuota" : "ShowDupplicateRegistration";
            ViewBag.SupervisorArea = _supervisor.AreaCode;

            return View(farmer);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(vm_RegistrationFarmer collation)
        {
            BuyingFacade.RegistrationBL()
                .EditSTECQuota(collation.Crop,
                collation.FarmerCode,
                Convert.ToInt16(collation.QuotaFromSTECApprove),
                HttpContext.User.Identity.Name);

            return RedirectToAction("Index", "Registration", new { supplierCode = collation.SupplierCode });

        }

        [HttpGet]
        public ActionResult SignContract(string farmerCode)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supervisorUser = AppSetting.SupervisorUser;
            var _crop = AppSetting.Crop;

            //Get registration farmer information.
            var model = RegistrationFarmerHelper.GetByFarmerCode(_crop.Crop1, farmerCode);
            if (model == null)
                throw new ArgumentException("ชาวไร่รายนี้ยังไม่ผ่านขั้นตอนการบันทึกคำร้อง จึงไม่สามารถทำสัญญาได้");

            if (model.ApproveFromSTECStatus == false)
                throw new ArgumentException("ชาวไร่รายนี้ยังไม่ผ่านขั้นตอนการยืนยันโควต้าโดย STEC");

            if (model.SignContractDate == null)
                model.SignContractDate = DateTime.Now;

            ViewBag.SupervisorArea = _supervisorUser.AreaCode;
            ViewBag.RegistrationFarmer = BuyingFacade.RegistrationBL().GetSingle(_crop.Crop1, farmerCode);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignContract(vm_RegistrationFarmer collation)
        {
            BuyingFacade.RegistrationBL()
                .SignContract(collation.Crop,
                collation.FarmerCode,
                Convert.ToInt16(collation.QuotaFromSignContract),
                Convert.ToDateTime(collation.SignContractDate),
                HttpContext.User.Identity.Name);

            return RedirectToAction("SignContract",
                new { farmerCode = collation.FarmerCode });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CancelContract(vm_RegistrationFarmer model)
        {
            BuyingFacade.RegistrationBL().CancelSignContract(model.Crop,
                model.FarmerCode,
                HttpContext.User.Identity.Name);
            return RedirectToAction("SignContract", new { farmerCode = model.FarmerCode });
        }

        [HttpGet]
        public ActionResult Dupplicate()
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supervisor = AppSetting.SupervisorUser;
            var _crop = AppSetting.Crop;

            var list = BuyingFacade.RegistrationBL()
                .GetMoreThanTwoSupplierByArea(_crop.Crop1,
                _supervisor.AreaCode);

            ViewBag.SupervisorArea = _supervisor.AreaCode;

            return View(list);
        }

        [HttpGet]
        public ActionResult DupplicateDetails(string citizenID)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supervisor = AppSetting.SupervisorUser;
            var _crop = AppSetting.Crop;

            ViewBag.SupervisorArea = _supervisor.AreaCode;

            return View(BuyingFacade.RegistrationBL()
                .GetByCropAndCitizenID(_crop.Crop1, citizenID));
        }

        [HttpGet]
        public ActionResult RegistrationDetails(short crop, string farmerCode)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supervisor = AppSetting.SupervisorUser;
            var _crop = AppSetting.Crop;

            //Get registration farmer information.
            var model = RegistrationFarmerHelper.GetByFarmerCode(crop, farmerCode);
            if (model == null)
                throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รายนี้! (farmer code : " + farmerCode + ")");

            //var registrationList = BuyingFacade.RegistrationBL()
            //    .GetByCitizenID(_crop.Crop1, model.Person.CitizenID);

            //ViewBag.ReturnToAction = registrationList.Count() < 3 ?
            //    "Index" : "DupplicateRegistration";
            ViewBag.SupervisorArea = _supervisor.AreaCode;
            return View(model);
        }

        [HttpGet]
        public ActionResult ExtraRegistration(string citizenID)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supervisor = AppSetting.SupervisorUser;
            var _crop = AppSetting.Crop;

            ViewBag.SupervisorArea = _supervisor.AreaCode;

            var person = BuyingFacade.PersonBL().GetSingle(citizenID);
            if (person == null && citizenID != null)
                throw new ArgumentException("ไม่พบข้อมูลประวัติส่วนตัวในระบบ หรือรหัสประจำตัวประชาชนไม่ถูกต้อง");

            //var farmers = BuyingFacade.FarmerBL()
            //    .GetByCitizenID(citizenID);

            //if (farmers.Where(x => x.Supplier == "STEC/SK").Count() <= 0)
            //    throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนไว้กับ STEC/SK โปรดย้อนกลับไปยังขั้นตอนการบันทึกใบสมัครใหม่อีกครั้ง");

            var registrations = RegistrationFarmerHelper
                .GetByCitizenID(_crop.Crop1, citizenID);

            //if (registrations.Where(x => x.SupplierCode == "STEC/SK").Count() <= 0)
            //    throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนไว้กับ STEC/SK โปรดย้อนกลับไปยังขั้นตอนการบันทึกใบสมัครใหม่อีกครั้ง");

            var extensionAgents = BuyingFacade.SupplierBL()
                .GetByArea(_supervisor.AreaCode);

            extensionAgents
                .Remove(extensionAgents
                .SingleOrDefault(x => x.SupplierCode == "STEC/SK"));

            ViewBag.Person = person == null ? new Person() : person;
            //ViewBag.Farmers = farmers;
            ViewBag.Registrations = registrations;
            ViewBag.ExtensionAgents = extensionAgents;
            ViewBag.CitizenID = citizenID;
            ViewBag.SupervisorArea = _supervisor.AreaCode;

            /// Registration history
            /// 
            var list = BuyingFacade.BuyingBL()
                .GetByCitizenID(citizenID)
                .GroupBy(x => new
                {
                    x.Crop,
                    x.FarmerCode,
                    x.BuyingDocument.RegistrationFarmer.QuotaFromSignContract
                })
                .Select(x => new vm_RegistraionHistory
                {
                    Crop = x.Key.Crop,
                    FarmerCode = x.Key.FarmerCode,
                    RaiOfContract = Convert.ToInt16(x.Key.QuotaFromSignContract),
                    TotalSold = Convert.ToDecimal(x.Sum(y => y.Weight)),
                    AveragePrice = Convert.ToDecimal(x.Sum(y => y.Weight * y.BuyingGrade.UnitPrice) / x.Sum(y => y.Weight))
                }).ToList();

            ViewBag.RegistrationHistory = list;

            return View(new vm_ExtraRegistration
            {
                Crop = _crop.Crop1,
                CitizenID = citizenID,
                SignContractDate = DateTime.Now
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ExtraRegistration(vm_ExtraRegistration collation)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supervisor = AppSetting.SupervisorUser;
            var _crop = AppSetting.Crop;

            ViewBag.SupervisorArea = _supervisor.AreaCode;

            /// ตรวจสอบข้อมูลประวัติส่วนตัวในระยย
            /// 
            var person = BuyingFacade.PersonBL().GetSingle(collation.CitizenID);
            if (person == null)
                throw new ArgumentException("ไม่พบข้อมูลประวัติส่วนตัวของชาวไร่รายนี้");

            /// ตรวจสอบการลงทะเบียนซ้ำซ้อน
            /// 
            var registrations = BuyingFacade.RegistrationBL()
                .GetByCropAndCitizenID(_crop.Crop1, collation.CitizenID);
            if (registrations
                .Where(x => x.Farmer.Supplier == collation.SupplierCode)
                .Count() > 0)
                throw new ArgumentException("ชาวไร่รายนี้เคยลงทะเบียนไว้กับ " +
                        collation.SupplierCode + " แล้วในปีนี้");

            var stecSKRegis = registrations.SingleOrDefault(x => x.Farmer.Supplier == "STEC/SK");
            if (stecSKRegis == null)
                throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนไว้กับ STEC/SK โปรดย้อนกลับไปยังขั้นตอนการบันทึกใบสมัครใหม่อีกครั้ง");

            if (stecSKRegis.ApproveFromSTECStatus == false)
                throw new ArgumentException("โควต้าของชาวไร่รายนี้ยังไม่ได้รับการอนุมัติโควต้าจาก STEC");

            var totalSignContract = registrations.Sum(x => x.QuotaFromSignContract);
            if (totalSignContract + collation.QuotaFromSignContract > stecSKRegis.QuotaFromSTECApprove)
                throw new ArgumentException("จำนวนไร่ที่จะลงทะเบียน เกินจำนวนโควต้าที่ลงไว้กับ STEC/SK (" +
                    stecSKRegis.QuotaFromSTECApprove +
                    " ไร่) supervisor จะต้องย้อนกลับไปปรับจำนวนไร่ในหน้า ยืนยันโควก้าก่อน");

            /// ตรวจสอบการลงทะเบียนเป็นสมาชิกของตัวแทนที่เลือก
            /// ถ้ายังไม่เคยลงทะเบียนไว้กับตัวแทนที่เลือกมาก่อน ให้ลงทะเบียนเพื่อสร้าง FarmerCode ก่อน
            /// 
            var farmers = BuyingFacade.FarmerBL()
                .GetByCitizenID(collation.CitizenID);
            if (farmers.SingleOrDefault(x => x.Supplier == collation.SupplierCode) == null)
                BuyingFacade.FarmerBL()
                    .Add(person.CitizenID, collation.SupplierCode);

            var farmer = BuyingFacade.FarmerBL()
                .GetByCitizenID(collation.CitizenID)
                .SingleOrDefault(x => x.Supplier == collation.SupplierCode);
            if (farmer == null)
                throw new ArgumentException("ลงทะเบียนกับตัวแทน " + collation.SupplierCode +
                    " ไม่สำเร็จ โปรดแจ้งแผนกไอทีเพื่อตรวจสอบข้อมูล");

            BuyingFacade.RegistrationBL().Add(new RegistrationFarmer
            {
                Crop = _crop.Crop1,
                FarmerCode = farmer.FarmerCode,
                RegistrationDate = DateTime.Now,

                ActiveStatus = stecSKRegis.ActiveStatus,
                BankBranch = stecSKRegis.BankBranch,
                BankBranchCode = stecSKRegis.BankBranchCode,
                BankID = stecSKRegis.BankID,
                BookBank = stecSKRegis.BookBank,
                PreviousCropTotalRai = stecSKRegis.PreviousCropTotalRai,
                PreviousCropTotalVolume = stecSKRegis.PreviousCropTotalVolume,
                TTMQuotaKg = stecSKRegis.TTMQuotaKg,
                TTMQuotaStation = stecSKRegis.TTMQuotaStation,
                TTMQuotaStatus = stecSKRegis.TTMQuotaStatus,

                ApproveFromSupplierStatus = stecSKRegis.ApproveFromSupplierStatus,
                ApproveFromSTECStatus = stecSKRegis.ApproveFromSTECStatus,

                QuotaFromFarmerRequest = collation.QuotaFromSignContract,
                QuotaFromSupplierApprove = collation.QuotaFromSignContract,
                QuotaFromSTECApprove = collation.QuotaFromSignContract,
                QuotaFromSignContract = collation.QuotaFromSignContract,
                SignContractDate = collation.SignContractDate,

                LastModified = DateTime.Now,
                ModifiedByUser = AppSetting.Username
            });

            return RedirectToAction("ExtraRegistration", new { citizenID = collation.CitizenID });
        }

        [HttpGet]
        public ActionResult SearchFarmer(string farmerName, string extensionAgentCode)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var extensionAgentFarmerList = new List<Farmer>();
            if (!string.IsNullOrEmpty(farmerName) && !string.IsNullOrEmpty(extensionAgentCode))
            {
                extensionAgentFarmerList = BuyingFacade.FarmerBL()
                .GetByFarmerName(farmerName)
                .Where(x => x.Supplier == extensionAgentCode)
                .ToList();
            }

            ViewBag.FarmerName = farmerName;
            ViewBag.ExtentionAgentCode = extensionAgentCode;
            ViewBag.ExtensionAgentFarmerList = extensionAgentFarmerList;
            ViewBag.ExtensionAgenCodeList = BuyingFacade.SupplierBL()
                .GetByArea(AppSetting.SupervisorUser.AreaCode);
            return View();
        }

        [HttpGet]
        public ActionResult CheckProfile(string firstName)
        {
            var persons = new List<Person>();
            persons = BuyingFacade.PersonBL().GetByName(firstName).ToList();
            ViewBag.Persons = persons;
            ViewBag.FirstName = firstName;

            AppSetting.Username = HttpContext.User.Identity.Name;
            ViewBag.ExtensionAgenCodeList = BuyingFacade.SupplierBL()
                .GetByArea(AppSetting.SupervisorUser.AreaCode);

            ViewBag.MaritalStatusList = BuyingFacade.MaritalStatusBL().GetAllMaitalStatus();
            ViewBag.Provinces = BuyingFacade.ThailandGeoBL()
                .GetProvinces()
                .Where(x => x.ProvinceCode == "54" ||
                x.ProvinceCode == "51" ||
                x.ProvinceCode == "38")
                .Select(x => new SelectListItem
                {
                    Text = x.ProvinceNameThai.Replace(" ", ""),
                    Value = x.ProvinceNameThai.Replace(" ", "")
                });

            return View(new vm_Person
            {
                CitizenID = "",
                BirthDate = DateTime.Now.Date,
                SpouseBirthDate = DateTime.Now.Date
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddPersonProfile(vm_Person person)
        {
            BuyingFacade.PersonBL()
                .Add(new Person
                {
                    CitizenID = person.CitizenID,
                    Prefix = person.Prefix,
                    FirstName = person.FirstName,
                    LastName = person.LastName,
                    BirthDate = person.BirthDate,
                    Village = person.Village,
                    HouseNumber = person.HouseNumber,
                    Tumbon = person.Tumbon,
                    Amphur = person.Amphur,
                    Province = person.Province,
                    TelephoneNumber = person.TelephoneNumber,
                    MaritalStatusCode = person.MaritalStatusCode,
                    SpousePrefix = person.SpousePrefix,
                    SpouseCitizenID = person.SpouseCitizenID,
                    SpouseFirstName = person.SpouseFirstName,
                    SpouseLastName = person.SpouseLastName,
                    SpouseBirthDate = person.SpouseBirthDate,

                    /// A new requirements from CY 2022.
                    /// 
                    StartAgriculture = person.StartAgriculture,
                    StartGrowingTobacco = person.StartGrowingTobacco,
                    YearsGrowingTobacco = person.YearsGrowingTobacco,
                    YearsInAgriculture = person.YearsInAgriculture,
                    ModifiedByUser = HttpContext.User.Identity.Name
                });

            return RedirectToAction("CheckProfile", new { firstName = person.FirstName });
        }

        [HttpGet]
        public ActionResult UpdateProfile(string citizenID)
        {
            var person = BuyingFacade.PersonBL().GetSingle(citizenID);
            if (person == null)
                throw new ArgumentException("ไม่พบข้อมูลชาวไร่ รหัสประจำตัวประชาชน " + citizenID + " นี้ในระบบ");

            AppSetting.Username = HttpContext.User.Identity.Name;
            ViewBag.MaritalStatusList = BuyingFacade.MaritalStatusBL().GetAllMaitalStatus();
            ViewBag.Provinces = BuyingFacade.ThailandGeoBL()
                .GetProvinces()
                .Where(x => x.ProvinceCode == "54" ||
                x.ProvinceCode == "51" ||
                x.ProvinceCode == "38")
                .Select(x => new SelectListItem
                {
                    Text = x.ProvinceNameThai.Replace(" ", ""),
                    Value = x.ProvinceNameThai.Replace(" ", ""),
                    Selected = x.ProvinceNameThai.Contains(person.Province) ? true : false
                });

            ViewBag.Amphurs = BuyingFacade.ThailandGeoBL()
                .GetDistrictByProvinceName(person.Province)
                .Select(x => new SelectListItem
                {
                    Text = x.DistrictNameThai.Replace(" ", ""),
                    Value = x.DistrictNameThai.Replace(" ", ""),
                    Selected = x.DistrictNameThai.Contains(person.Amphur) ? true : false
                }).ToList();

            ViewBag.Tumbons = BuyingFacade.ThailandGeoBL()
                .GetSubDistrictByDistrictName(person.Amphur)
                .Select(x => new SelectListItem
                {
                    Text = x.SubDistrictNameThai.Replace(" ", ""),
                    Value = x.SubDistrictNameThai.Replace(" ", ""),
                    Selected = x.SubDistrictNameThai.Contains(person.Tumbon) ? true : false
                }).ToList();

            var birthYear = person.BirthDate.Year;
            return View(new vm_Person
            {
                CitizenID = person.CitizenID,
                Prefix = person.Prefix,
                FirstName = person.FirstName,
                LastName = person.LastName,
                BirthDate = person.BirthDate,
                Village = person.Village,
                HouseNumber = person.HouseNumber,
                Tumbon = person.Tumbon,
                Amphur = person.Amphur,
                Province = person.Province,
                TelephoneNumber = person.TelephoneNumber,
                MaritalStatusCode = person.MaritalStatusCode,
                SpousePrefix = person.SpousePrefix,
                SpouseCitizenID = person.SpouseCitizenID,
                SpouseFirstName = person.SpouseFirstName,
                SpouseLastName = person.SpouseLastName,
                SpouseBirthDate = person.SpouseBirthDate,
                StartAgriculture = person.StartAgriculture == null ? Convert.ToInt16(birthYear + 18) : Convert.ToInt16(person.StartAgriculture),
                StartGrowingTobacco = person.StartGrowingTobacco == null ? Convert.ToInt16(birthYear + 18) : Convert.ToInt16(person.StartGrowingTobacco),
                YearsGrowingTobacco = person.YearsGrowingTobacco,
                YearsInAgriculture = person.YearsInAgriculture
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateProfile(vm_Person model)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            ViewBag.MaritalStatusList = BuyingFacade.MaritalStatusBL().GetAllMaitalStatus();
            ViewBag.Provinces = BuyingFacade.ThailandGeoBL()
                .GetProvinces()
                .Where(x => x.ProvinceCode == "54" ||
                x.ProvinceCode == "51" ||
                x.ProvinceCode == "38")
                .Select(x => new SelectListItem
                {
                    Text = x.ProvinceNameThai.Replace(" ", ""),
                    Value = x.ProvinceNameThai.Replace(" ", "")
                });

            ViewBag.Amphur = BuyingFacade.ThailandGeoBL()
                .GetDistrictByProvinceName(model.Province)
                .Select(x => new SelectListItem
                {
                    Text = x.DistrictNameThai.Replace(" ", ""),
                    Value = x.DistrictNameThai.Replace(" ", ""),
                    Selected = x.DistrictNameThai.Contains(model.Amphur) ? true : false
                });

            ViewBag.Tumbon = BuyingFacade.ThailandGeoBL()
                .GetSubDistrictByDistrictName(model.Amphur)
                .Select(x => new SelectListItem
                {
                    Text = x.SubDistrictNameThai.Replace(" ", ""),
                    Value = x.SubDistrictNameThai.Replace(" ", ""),
                    Selected = x.SubDistrictNameThai.Contains(model.Tumbon) ? true : false
                });

            if (!ModelState.IsValid)
                return View(model);

            //Update farmer profile.
            BuyingFacade.PersonBL()
                .Update(new Person
                {
                    CitizenID = model.CitizenID,
                    Prefix = model.Prefix,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    BirthDate = model.BirthDate,
                    Village = model.Village,
                    HouseNumber = model.HouseNumber,
                    Tumbon = model.Tumbon,
                    Amphur = model.Amphur,
                    Province = model.Province,
                    TelephoneNumber = model.TelephoneNumber,
                    MaritalStatusCode = model.MaritalStatusCode,
                    SpousePrefix = model.SpousePrefix,
                    SpouseFirstName = model.SpouseFirstName,
                    SpouseLastName = model.SpouseLastName,
                    SpouseCitizenID = model.SpouseCitizenID,
                    SpouseBirthDate = model.SpouseBirthDate,

                    /// A new requirements from CY 2022.
                    /// 
                    StartAgriculture = model.StartAgriculture,
                    StartGrowingTobacco = model.StartGrowingTobacco,

                    YearsGrowingTobacco = model.YearsGrowingTobacco,
                    YearsInAgriculture = model.YearsInAgriculture,
                    ModifiedByUser = AppSetting.Username
                });

            return RedirectToAction("CheckProfile", new { firstName = model.FirstName });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateNewFarmer(string citizenID, string extensionAgentCode)
        {
            var farmerCode = BuyingFacade.FarmerBL().Add(citizenID, extensionAgentCode);
            var farmer = BuyingFacade.FarmerBL().GetByFarmerCode(farmerCode);
            if (farmer == null)
                throw new Exception("ไม่พบชาวไร่ที่ลงทะเบียนเป็นสมาชิกกับตัวแทน " + extensionAgentCode + " ในระบบ");

            return RedirectToAction("CheckProfile", new { firstName = farmer.Person.FirstName });
        }

        [HttpGet]
        public ActionResult SignContractV2(string farmerCode)
        {
            var farmer = BuyingFacade.FarmerBL().GetByFarmerCode(farmerCode);
            if (farmer == null)
                throw new Exception("ไม่พบข้อมูลชาวไร่รหัส farmercode " + farmerCode + " นี้ในระบบ");

            AppSetting.Username = HttpContext.User.Identity.Name;
            var supervisorArea = AppSetting.SupervisorUser.AreaCode;
            if (supervisorArea == null)
                throw new Exception("Supervisor area not found. Please contact IT department.");

            if (farmer.Supplier1.SupplierArea != supervisorArea)
                throw new Exception("ท่านสามารถบันทึกข้อมูลได้เฉพาะกลุ่มของตัวแทนที่อยู่ในจังหวัดของท่านเท่านั้น " +
                    "ไม่สามารถบันทึกข้อมูลข้ามจังหวัดได้");

            var registration = new RegistrationFarmer();
            registration = BuyingFacade.RegistrationBL()
                .GetSingle(AppSetting.Crop.Crop1, farmerCode);

            ViewBag.Farmer = farmer;
            ViewBag.RegistrationFarmer = registration;
            ViewBag.BankList = BuyingFacade.BankBL().GetAll().OrderBy(b => b.BankName);

            IEnumerable<SelectListItem> unRegisterReasonList =
                new List<SelectListItem> {
                    new SelectListItem{ Value = "มีปัญหาสุขภาพ",Text="มีปัญหาสุขภาพ" },
                    new SelectListItem{ Value = "ไม่มีพื้นที่เพาะปลูก",Text="ไม่มีพื้นที่เพาะปลูก" },
                    new SelectListItem{ Value = "ต้องการปลูกพืชอื่น",Text="ต้องการปลูกพืชอื่น" },
                    new SelectListItem{ Value = "ขาดแคลนแรงงาน",Text="ขาดแคลนแรงงาน" },
                    new SelectListItem{ Value = "ขาดทุนทรัพย์",Text="ขาดทุนทรัพย์" },
                    new SelectListItem{ Value = "กลัวภัยธรรมชาติ (ฝนตก น้ำท่วม ภัยแล้ง)",Text="กลัวภัยธรรมชาติ (ฝนตก น้ำท่วม ภัยแล้ง)" },
                    new SelectListItem{ Value = "ไม่มั่นใจนโยบายของบริษัทฯ",Text="ไม่มั่นใจนโยบายของบริษัทฯ" },
                    new SelectListItem{ Value = "อื่นๆ",Text="อื่นๆ" }
                };

            ViewBag.UnRegisterReasonList = unRegisterReasonList
                .Select(x => new SelectListItem()
                {
                    Value = x.Value,
                    Text = x.Text,
                    Selected = x.Value == (registration == null ? "" : registration.UnRegisterReason)
                });

            var vm = new vm_BankAccount();
            if (registration != null)
            {
                vm.Crop = registration.Crop;
                vm.FarmerCode = registration.FarmerCode;
                vm.BankBranch = registration.BankBranch;
                vm.BankBranchCode = registration.BankBranchCode;
                vm.BankID = registration.BankID == null ? 0 : (int)registration.BankID;
                vm.BookBank = registration.BookBank;
            }

            return View(vm);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignContractV2(string farmerCode, short signContractRai, DateTime signContractDate, string recommender)
        {
            BuyingFacade.RegistrationBL()
                .Add(new RegistrationFarmer
                {
                    Crop = AppSetting.Crop.Crop1,
                    FarmerCode = farmerCode,
                    QuotaFromFarmerRequest = signContractRai,
                    QuotaFromSupplierApprove = signContractRai,
                    QuotaFromSTECApprove = signContractRai,
                    ApproveFromSupplierStatus = true,
                    ApproveFromSTECStatus = true,
                    QuotaFromSignContract = signContractRai,
                    SignContractDate = signContractDate,
                    RegistrationDate = DateTime.Now,
                    ModifiedByUser = HttpContext.User.Identity.Name,

                    //CY24 ใช้ช่องนี้สำหรับเก็บข้อมูลผู้แนะนำให้ชาวไร่มาปลูกยาสูบกับทางบริษัทให้ไร่ละ 300 บาท
                    TTMQuotaStation = recommender
                });
            return RedirectToAction("SignContractV2", new { farmerCode = farmerCode });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangeSignContractRai(short crop, string farmerCode, short newSignContractRai, DateTime signContractDate, string recommender)
        {
            BuyingFacade.RegistrationBL()
                .UpdateSignContract(crop, farmerCode, newSignContractRai, signContractDate, recommender, HttpContext.User.Identity.Name);
            return RedirectToAction("SignContractV2", new { farmerCode = farmerCode });
        }

        [HttpGet]
        public ActionResult CancelUnContract(string farmerCode)
        {
            BuyingFacade.RegistrationBL()
                .CancelUnRegister(farmerCode, HttpContext.User.Identity.Name);
            return RedirectToAction("SignContractV2", new { farmerCode = farmerCode });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UnContract(string farmerCode, string unContractReason)
        {
            BuyingFacade.RegistrationBL()
                .UnRegister(farmerCode, unContractReason,
                HttpContext.User.Identity.Name);
            return RedirectToAction("SignContractV2", new { farmerCode = farmerCode });
        }


        [HttpGet]
        public ActionResult PrintContract(short crop, string farmerCode)
        {
            try
            {
                var reg = BuyingFacade.RegistrationBL().GetSingle(crop, farmerCode);
                if (reg == null)
                    throw new Exception("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รหัส farmer code " + farmerCode);

                if (reg.QuotaFromSignContract == null)
                    throw new Exception("ชาวไร่รหัส farmer code " + farmerCode + " นี้ยังไม่ได้ระบุจำนวนไร่ในระบบ");

                if (!string.IsNullOrEmpty(reg.UnRegisterReason))
                    throw new Exception("ชาวไร่รหัส farmer code " + farmerCode + " นี้ทำการยกเลิกสัญญาแล้วเนื่องจาก " + reg.UnRegisterReason);

                var dataTable = new SupervisorDataSet.FarmerContractDataTableDataTable();
                var dataRow = dataTable.NewRow();
                var person = reg.Farmer.Person;

                dataRow["Prefix"] = person.Prefix;
                dataRow["FirstName"] = person.FirstName;
                dataRow["LastName"] = person.LastName;
                dataRow["CitizenID"] = person.CitizenID;
                dataRow["BirthDate"] = person.BirthDate;
                dataRow["HouseNumber"] = person.HouseNumber;
                dataRow["Tumbon"] = person.Tumbon;
                dataRow["Amphur"] = person.Amphur;
                dataRow["Province"] = person.Province;
                dataRow["TelephoneNumber"] = person.TelephoneNumber;
                dataRow["FarmerCode"] = reg.FarmerCode;
                dataRow["QuotaFromSignContract"] = reg.QuotaFromSignContract;
                dataRow["SignContractDate"] = reg.SignContractDate;
                dataRow["GAPContractCode"] = reg.GAPContractCode;
                dataRow["SupplierArea"] = reg.Farmer.Supplier1.SupplierArea;
                dataRow["Village"] = person.Village;
                dataRow["Crop"] = reg.Crop;
                dataTable.Rows.Add(dataRow);

                ReportDataSource reportDataSource = new ReportDataSource();
                reportDataSource.Value = dataTable;
                reportDataSource.Name = "FarmerContractDataset";

                LocalReport report = new LocalReport();
                report.DataSources.Add(reportDataSource);
                report.ReportPath = Server.MapPath("~/Areas/Supervisor/RDLC/FarmerContract.rdlc");

                string filePath = Path.GetTempFileName();
                Export(report, filePath);

                report.Dispose();
                return File(filePath, "application/pdf");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string Export(LocalReport report, string filePath)
        {
            string ack = "";
            try
            {
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;

                byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);
                using (FileStream stream = System.IO.File.OpenWrite(filePath))
                {
                    stream.Write(bytes, 0, bytes.Length);
                }
                return ack;
            }
            catch (Exception ex)
            {
                ack = ex.InnerException.Message;
                return ack;
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UpdateBankAccount(vm_BankAccount model)
        {
            BuyingFacade.RegistrationBL()
                .UpdateBookBank(model.Crop,
                model.FarmerCode,
                Convert.ToUInt16(model.BankID),
                model.BookBank,
                model.BankBranchCode,
                model.BankBranch,
                HttpContext.User.Identity.Name);

            return RedirectToAction("SignContractV2", new { farmerCode = model.FarmerCode });
        }
    }
}