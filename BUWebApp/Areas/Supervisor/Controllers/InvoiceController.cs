﻿using BusinessLayer;
using BUWebApp.Helper;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Supervisor.Controllers
{
    public struct InvoiceType
    {
        public string TypeCode { get; set; }
        public string TypeName { get; set; }
    }

    [Authorize(Roles = "Supervisor")]
    public class InvoiceController : Controller
    {
        [HttpGet]
        public ActionResult Index(string farmerCode)
        {
            ViewBag.FarmerCode = farmerCode;
            return View();
        }

        [HttpGet]
        public ActionResult SearchFarmer(string farmerName, string extensionAgentCode)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var farmers = new List<Farmer>();
            var crop = AppSetting.Crop.Crop1;
            if (!string.IsNullOrEmpty(farmerName) && !string.IsNullOrEmpty(extensionAgentCode))
            {
                farmers = BuyingFacade.FarmerBL()
                .GetByFarmerName(farmerName)
                .Where(x => x.Supplier == extensionAgentCode)
                .ToList();
                var registrationList = BuyingFacade.RegistrationBL().GetBySupplier(crop, extensionAgentCode);
                foreach (var item in farmers)
                {
                    item.RegistrationFarmers = registrationList
                        .Where(x => x.FarmerCode == item.FarmerCode)
                        .ToList();
                }
            }

            ViewBag.FarmerName = farmerName;
            ViewBag.ExtentionAgentCode = extensionAgentCode;
            ViewBag.Crop = crop;
            ViewBag.Farmers = farmers;
            ViewBag.ExtensionAgenCodeList = BuyingFacade.SupplierBL()
                .GetByArea(AppSetting.SupervisorUser.AreaCode);
            return View();
        }

        [HttpGet]
        public ActionResult Index2(string farmerCode)
        {
            var crop = AppSetting.Crop.Crop1;
            var registration = BuyingFacade.RegistrationBL().GetSingle(crop, farmerCode);
            if (registration == null)
                throw new Exception("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รายนี้ในระบบ");

            if (registration.ActiveStatus == false)
                throw new Exception("ชาวไร่รายนี้ได้ทำการยกเลิกสัญญาแล้ว");

            var invoiceList = BuyingFacade.InvoiceBL()
                .GetByFarmer(crop, farmerCode);

            var typeList = new List<InvoiceType>();
            typeList.Add(new InvoiceType { TypeCode = "IV", TypeName = "Include VAT" });
            typeList.Add(new InvoiceType { TypeCode = "IN", TypeName = "Not Include VAT" });

            ViewBag.InvoiceList = invoiceList;
            ViewBag.FarmerCode = farmerCode;
            ViewBag.Farmer = registration.Farmer;
            ViewBag.RegistrationFarmer = registration;
            ViewBag.InvoiceTypeList = typeList;
            ViewBag.DebtTypeList = BuyingFacade.DebtTypeBL().GetAll();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(string farmerCode, string invoiceType, string debtTypeCode)
        {
            var isVat = invoiceType == "IV" ? true : false;
            var crop = AppSetting.Crop.Crop1;
            var user = HttpContext.User.Identity.Name;
            BuyingFacade.InvoiceBL()
                .Add2(crop, farmerCode, isVat, debtTypeCode, user);

            return RedirectToAction("Index2", new { farmerCode = farmerCode });
        }

        [HttpGet]
        public ActionResult Delete2(string invoiceNo, string farmerCode)
        {
            BuyingFacade.InvoiceBL().Delete(invoiceNo);
            return RedirectToAction("Index2", new { farmerCode = farmerCode });
        }

        [HttpGet]
        public ActionResult Main(string farmerCode, string invoiceType)
        {
            /// Invoice types.
            /// CD : return crop input distibution.
            /// IV : invoice include vat (prefix IV + isVat = true).
            /// IN : invoice not include vat (prefix IV + isVat = false).
            /// 

            var crop = AppSetting.Crop.Crop1;
            var user = HttpContext.User.Identity.Name;
            var invoiceList = BuyingFacade.InvoiceBL()
                .GetByFarmer(crop, farmerCode);

            var retrunInvoiceList = new List<Invoice>();
            switch (invoiceType)
            {
                case "CD":
                    retrunInvoiceList = invoiceList
                        .Where(x => x.InvoiceNo.Substring(0, 2) == "CD")
                        .ToList();
                    break;
                case "IV":
                    retrunInvoiceList = invoiceList
                        .Where(x => x.InvoiceNo.Substring(0, 2) == "IV" &&
                        x.IsIncludeVAT == true)
                        .ToList();
                    break;
                case "IN":
                    retrunInvoiceList = invoiceList
                        .Where(x => x.InvoiceNo.Substring(0, 2) == "IV" &&
                        x.IsIncludeVAT == false)
                        .ToList();
                    break;
                default:
                    break;
            }

            var typeList = new List<InvoiceType>();
            //typeList.Add(new InvoiceType { TypeCode = "CD", TypeName = "คืนบรรจุภัณฑ์" });
            typeList.Add(new InvoiceType { TypeCode = "IV", TypeName = "Include VAT" });
            typeList.Add(new InvoiceType { TypeCode = "IN", TypeName = "Not Include VAT" });

            ViewBag.InvoiceList = retrunInvoiceList;
            ViewBag.InvoiceTypeList = typeList;
            ViewBag.InvoiceType = invoiceType;
            ViewBag.FarmerCode = farmerCode;

            var registration = BuyingFacade.RegistrationBL().GetSingle(crop, farmerCode);
            ViewBag.Farmer = registration.Farmer;
            ViewBag.RegistrationFarmer = registration;

            return View();
        }

        [HttpGet]
        public ActionResult Create(string farmerCode, string invoiceType)
        {
            if (invoiceType != "CD" && invoiceType != "IV" && invoiceType != "IN")
                throw new ArgumentException("ประเภทของ invoice ไม่ถูกต้อง โปรดเลือกจากตัวเลือกที่กำหนดให้");

            var isVat = invoiceType == "IV" ? true : false;
            var crop = AppSetting.Crop.Crop1;

            BuyingFacade.InvoiceBL().Add(crop, farmerCode, invoiceType, isVat, HttpContext.User.Identity.Name);

            if (invoiceType == "CD")
                return RedirectToAction("ReturnCPAContainer", new { farmerCode = farmerCode });
            else
                return RedirectToAction("Main", new { farmerCode = farmerCode, invoiceType = invoiceType });
        }

        [HttpGet]
        public ActionResult Delete(string farmerCode, string invoiceType, string invoiceNo)
        {
            BuyingFacade.InvoiceBL().Delete(invoiceNo);

            if (invoiceType == "CD")
                return RedirectToAction("ReturnCPAContainer", new { farmerCode = farmerCode });
            else
                return RedirectToAction("Main", new { farmerCode = farmerCode, invoiceType = invoiceType });
        }

        [HttpGet]
        public ActionResult ReturnCPAContainer(string farmerCode)
        {
            throw new ArgumentException("อยู่ในระหว่างปรับปรุงระบบ");

            var crop = AppSetting.Crop.Crop1;
            var user = HttpContext.User.Identity.Name;
            var registration = BuyingFacade.RegistrationBL().GetSingle(crop, farmerCode);

            ViewBag.FarmerCode = farmerCode;
            ViewBag.Farmer = registration.Farmer;
            ViewBag.RegistrationFarmer = registration;
            ViewBag.InvoiceList = BuyingFacade.InvoiceBL().GetByFarmer(crop, farmerCode)
                        .Where(x => x.InvoiceNo.Substring(0, 2) == "CD")
                        .ToList();

            return View();
        }
    }
}