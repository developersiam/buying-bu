﻿using BUWebApp.Helper;
using BUWebApp.Models;
using BusinessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CropInputsController : Controller
    {
        [HttpGet]
        // GET: CropInputsConditions
        public ActionResult Index()
        {
            return View(BuyingFacade.RegistrationBL()
                .GetByCrop(AppSetting.Crop.Crop1)
                .GroupBy(x => new
                {
                    x.Crop,
                    x.Farmer.Supplier,
                    x.Farmer.Supplier1.SupplierArea
                })
                .Select(x => new Supplier
                {
                    SupplierArea = x.Key.SupplierArea,
                    SupplierCode = x.Key.Supplier
                })
                .OrderBy(x => x.SupplierCode)
                .ToList());

        }
    }
}