﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainModel;
using BusinessLayer;
using BUWebApp.Areas.Admin.Models;
using BUWebApp.Models;
using BUWebApp.Helper;

namespace BUWebApp.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CropController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            List<vm_Crop> cropAdminList = new List<vm_Crop>();
            foreach (Crop crop in BuyingFacade.CropBL().GetAll())
            {
                cropAdminList.Add(new vm_Crop
                {
                    Crop = crop.Crop1,
                    CropName = crop.CropName,
                    StartCrop = crop.StartCrop,
                    EndCrop = crop.EndCrop,
                    DefaultStatus = crop.DefaultStatus,
                    LastModifed = crop.LastModifed,
                    ModifiedByUser = crop.ModifiedByUser
                });
            }

            return View(cropAdminList);
        }
        
        [HttpGet]
        public ActionResult Create()
        {
            vm_Crop cropViewModel = new vm_Crop();
            cropViewModel.Crop = Convert.ToInt16(DateTime.Now.Year);
            return View(cropViewModel);
        }

        [HttpPost]
        public ActionResult Create(vm_Crop collection)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(string.Empty, "Model State is Error!");
                return View(collection);
            }

            if (BuyingFacade.CropBL().GetSingle(collection.Crop) != null)
                throw new ArgumentException("Dupplicate data!");

            Crop crop = new Crop
            {
                Crop1 = collection.Crop,
                CropName = collection.CropName,
                StartCrop = collection.StartCrop,
                EndCrop = collection.EndCrop,
                DefaultStatus = collection.DefaultStatus,
                ModifiedByUser = AppSetting.Username,
                LastModifed = DateTime.Now
            };

            BuyingFacade.CropBL().Add(crop);

            //ถ้ามีการระบุ crop นี้ให้เป็น default crop ให้ทำการเคลียร์ default crop ของทุกๆ รายการให้เป็น false ก่อน
            if (collection.DefaultStatus == true)
                BuyingFacade.CropBL().SetDefault(collection.Crop, AppSetting.Username);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(short id)
        {
            var crop = BuyingFacade.CropBL().GetSingle(id);
            if (crop == null)
                throw new ArgumentNullException("Find data not found!");

            vm_Crop cropAdmin = new vm_Crop
            {
                Crop = crop.Crop1,
                CropName = crop.CropName,
                StartCrop = crop.StartCrop,
                EndCrop = crop.EndCrop,
                DefaultStatus = crop.DefaultStatus,
                ModifiedByUser = crop.ModifiedByUser,
                LastModifed = crop.LastModifed
            };
            return View(cropAdmin);
        }

        [HttpPost]
        public ActionResult Edit(short id, vm_Crop collection)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError(string.Empty, "Model State is not valid!");
                return View(collection);
            }

            var crop = BuyingFacade.CropBL().GetSingle(id);
            if (crop == null)
                throw new ArgumentNullException("Find data not found!");

            crop.CropName = collection.CropName;
            crop.StartCrop = collection.StartCrop;
            crop.EndCrop = collection.EndCrop;
            crop.DefaultStatus = collection.DefaultStatus;
            crop.ModifiedByUser = AppSetting.Username;
            crop.LastModifed = DateTime.Now;

            BuyingFacade.CropBL().Update(crop);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(short crop)
        {
            var model = BuyingFacade.CropBL().GetSingle(crop);
            if (model == null)
                throw new ArgumentNullException("Find data not found!");
            BuyingFacade.CropBL().Delete(crop);
            return RedirectToAction("Index");
        }
    }
}