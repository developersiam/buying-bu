﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BusinessLayer;
using DomainModel;
using System.Web.Mvc;
using BUWebApp.Areas.Admin.Models;
using BUWebApp.Helper;

namespace BUWebApp.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class FarmEquipmentController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.ItemList = BuyingFacade.MachineAndFarmEquipmentBL().GetAll();
            return View(new vm_FarmEquipment());
        }

        [HttpPost]
        public ActionResult Create(vm_FarmEquipment collection)
        {
            if (!ModelState.IsValid)
            {
                ViewBag.ItemList = BuyingFacade.MachineAndFarmEquipmentBL().GetAll();
                return View("Index", collection);
            }

            BuyingFacade.MachineAndFarmEquipmentBL()
                .Add(new MachineAndFarmEquipment
                {
                    MachineAndFarmEquipmentID = Guid.NewGuid(),
                    MachineAndFarmEquipmentName = collection.MachineAndFarmEquipmentName,
                    IsActive = collection.IsActive,
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = HttpContext.User.Identity.Name,
                });

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            var model = BuyingFacade.MachineAndFarmEquipmentBL().GetByID(id);
            if (model == null)
                throw new ArgumentException("ไม่พบข้อมูล");

            return View(new vm_FarmEquipment
            {
                MachineAndFarmEquipmentID = model.MachineAndFarmEquipmentID,
                MachineAndFarmEquipmentName = model.MachineAndFarmEquipmentName,
                IsActive = model.IsActive,
                ModifiedDate = model.ModifiedDate,
                ModifiedUser = model.ModifiedUser
            });
        }

        [HttpPost]
        public ActionResult Edit(vm_FarmEquipment collection)
        {
            if (!ModelState.IsValid)
                return View(collection);

            BuyingFacade.MachineAndFarmEquipmentBL()
                .Update(new MachineAndFarmEquipment
                {
                    MachineAndFarmEquipmentID = collection.MachineAndFarmEquipmentID,
                    MachineAndFarmEquipmentName = collection.MachineAndFarmEquipmentName,
                    IsActive = collection.IsActive,
                    ModifiedUser = HttpContext.User.Identity.Name,
                    ModifiedDate = DateTime.Now
                });

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(Guid id)
        {
            BuyingFacade.MachineAndFarmEquipmentBL().Delete(id);
            return RedirectToAction("Index");
        }
    }
}