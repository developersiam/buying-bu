﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainModel;
using BusinessLayer;
using BUWebApp.Areas.Admin.Models;
using BUWebApp.Models;

namespace BUWebApp.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SupplierUserController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            vm_SupplierUser supplierUserViewModel = new vm_SupplierUser();

            List<AspNetUser> supplierUserList1 = new List<AspNetUser>();//Supplier User ที่บันทึกไปแล้วก่อนหน้านี้
            List<AspNetUser> supplierUserList2 = new List<AspNetUser>();//User ทั้งหมดที่มี Role = Supplier

            supplierUserList1 = BuyingFacade.UserManagementBL().GetSupplierUsers().Select(u => u.AspNetUser).ToList();
            supplierUserList2 = BuyingFacade.UserManagementBL().GetAspNetUserByRole("ExtensionAgent");

            //ต้องการหาข้อมูลว่าใครที่ Role = Supplier ที่ยังไม่ได้บันทึกลงใน Supplier User
            HashSet<string> diffids = new HashSet<string>(supplierUserList1.Select(s => s.Id));

            var aspNetUsersNotAdded = supplierUserList2.Where(s => !diffids.Contains(s.Id)).ToList();

            supplierUserViewModel.SupplierUserNotMember = aspNetUsersNotAdded;
            supplierUserViewModel.SupplierUserList = BuyingFacade.UserManagementBL().GetSupplierUsers();
            supplierUserViewModel.SupplierList = BuyingFacade.SupplierBL().GetAll();

            return View(supplierUserViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(vm_SupplierUser collection)
        {
            // TODO: Add insert logic here
            if (ModelState.IsValid)
            {
                SupplierUser supplierUser = new SupplierUser
                {
                    SupplierCode = collection.SupplierCode,
                    UserId = collection.UserId,
                    ModifiedByUser = "Eakkaluck",
                    LastModifiedDate = DateTime.Now
                };

                if (BuyingFacade.UserManagementBL()
                    .GetSupplierUser(collection.SupplierCode, collection.UserId) != null)
                {
                    ModelState.AddModelError("", "Dupplicated data.");
                    return View(collection);
                }

                BuyingFacade.UserManagementBL().AddSupplierUser(supplierUser);
                return RedirectToAction("Index");
            }

            vm_SupplierUser supplierUserViewModel = new vm_SupplierUser();
            List<AspNetUser> supplierUserMemberList = new List<AspNetUser>();
            List<AspNetUser> supplierUserByRoleList = new List<AspNetUser>();

            supplierUserMemberList = BuyingFacade.UserManagementBL().GetSupplierUsers().Select(u => u.AspNetUser).ToList();
            supplierUserByRoleList = BuyingFacade.UserManagementBL().GetAspNetUserByRole("Supplier");

            var aspNetUsersNotMember = supplierUserByRoleList.Except(supplierUserMemberList).ToList();

            supplierUserViewModel.SupplierUserNotMember = aspNetUsersNotMember;
            supplierUserViewModel.SupplierUserList = BuyingFacade.UserManagementBL().GetSupplierUsers();
            supplierUserViewModel.SupplierList = BuyingFacade.SupplierBL().GetAll();

            return View(supplierUserViewModel);
        }

        [HttpGet]
        public ActionResult Delete(string supplierCode, string userId)
        {
            var supplierUser = BuyingFacade.UserManagementBL()
                .GetSupplierUser(supplierCode, userId);
            if (supplierUser == null)
                throw new ArgumentException("Find data not found.");

            return View(supplierUser);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(SupplierUser collection)
        {
            var supplierUser = BuyingFacade.UserManagementBL()
                .GetSupplierUser(collection.SupplierCode, collection.UserId);
            if (supplierUser == null)
                throw new ArgumentException("Find data not found.");

            BuyingFacade.UserManagementBL().DeleteSupplierUser(supplierUser);
            return RedirectToAction("Index");
        }
    }
}
