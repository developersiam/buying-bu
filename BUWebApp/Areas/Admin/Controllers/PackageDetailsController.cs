﻿using BUWebApp.Areas.Admin.Models;
using BUWebApp.Models;
using BusinessLayer;
using BusinessLayer.Model;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PackageDetailsController : Controller
    {
        // GET: MaterialIssuedDatails
        [HttpGet]
        public ActionResult Index(string packageCode)
        {
            var package = BuyingFacade.CropInputsPackageBL()
                .GetSingle(packageCode);

            if (package == null)
                throw new ArgumentException("ไม่พบข้อมูล package: " + packageCode + " ในระบบ");

            ViewBag.Package = package;
            ViewBag.PackageDetails = BusinessLayer.Helper
                .CropInputsItemHelper.GetByPackage(packageCode);

            ViewBag.CropInputsConditions = BusinessLayer.Helper
                .CropInputsItemHelper
                .GetBySupplierCode(package.Crop, package.SupplierCode)
                .ToList();

            return View(new vm_PackageDetails
            {
                PackageCode = package.PackageCode
            });
        }

        [HttpGet]
        public ActionResult Edit(string packageCode, Guid conditionID)
        {
            ViewBag.Package = BuyingFacade.CropInputsPackageBL()
                .GetSingle(packageCode);

            var model = BusinessLayer.Helper.CropInputsItemHelper
                .GetByPackageDetails(packageCode, conditionID);

            ViewBag.PackageDetail = model;

            return View(new m_CropInputsItem
            {
                PackageCode = model.PackageCode,
                ConditionID = model.ConditionID
            });
        }

        [HttpGet]
        public ActionResult DeleteConfirmation(string packageCode, Guid conditionID)
        {
            return View(BusinessLayer.Helper.CropInputsItemHelper
                .GetByPackageDetails(packageCode, conditionID));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(vm_PackageDetails collation)
        {
            BuyingFacade.CropInputsPackageDetailsBL()
                .Add(new CropInputsPackageDetail
                {
                    PackageCode = collation.PackageCode,
                    ConditionID = collation.ConditionID,
                    Quantity = collation.Quantity,
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = HttpContext.User.Identity.Name
                });
            return RedirectToAction("Index", new { packageCode = collation.PackageCode });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(vm_PackageDetails collation)
        {
            ViewBag.Package = BuyingFacade.CropInputsPackageBL()
                .GetSingle(collation.PackageCode);

            ViewBag.MaterialIssuedDetails = BusinessLayer.Helper
                .CropInputsItemHelper
                .GetByPackage(collation.PackageCode);

            ViewBag.Items = AGMInventorySystemBL.BLServices
                .material_itemBL().GetAll();

            if (!ModelState.IsValid)
                return RedirectToAction("Index", "MaterialIssuedDetails", collation);

            BuyingFacade.CropInputsPackageDetailsBL()
                .Update(new CropInputsPackageDetail
                {
                    PackageCode = collation.PackageCode,
                    ConditionID = Guid.NewGuid(),
                    Quantity = collation.Quantity,
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = HttpContext.User.Identity.Name
                });

            return RedirectToAction("Index", new { packageCode = collation.PackageCode });
        }

        [HttpGet]
        public ActionResult Delete(string packageCode, Guid conditionID)
        {
            BuyingFacade.CropInputsPackageDetailsBL()
                .Delete(packageCode, conditionID);

            return RedirectToAction("Index", new { packageCode = packageCode });
        }
    }
}