﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Threading.Tasks;
using BUWebApp.Models;
using System.Security.Claims;
using System.Data.Entity;
using System.Net;
using BUWebApp.Areas.Admin.Models;

namespace BUWebApp.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserController : Controller
    {
        public UserController()
        {
        }

        public UserController(ApplicationUserManager userManager, ApplicationRoleManager roleManager)
        {
            UserManager = userManager;
            RoleManager = roleManager;
        }

        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        private ApplicationRoleManager _roleManager;

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        public async Task<ActionResult> Index()
        {
            return View(await UserManager.Users.ToListAsync());
        }

        public async Task<ActionResult> Create()
        {
            ViewBag.RoleId = new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name");
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Create(RegisterViewModel userViewModel, params string[] selectedRoles)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Helper.ActiveDirectoryHelper.ActiveDirectoryAuthenticate(userViewModel.Username, userViewModel.Password);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                    ViewBag.RoleId = new SelectList(RoleManager.Roles, "Name", "Name");
                    return View(userViewModel);
                }

                var user = new ApplicationUser { UserName = userViewModel.Username, Email = userViewModel.Username + "@siamtobacco.com" };
                var adminresult = await UserManager.CreateAsync(user, userViewModel.Password);

                //Add User to the selected Roles 
                if (adminresult.Succeeded)
                {
                    if (selectedRoles != null)
                    {
                        var result = await UserManager.AddToRolesAsync(user.Id, selectedRoles);
                        if (!result.Succeeded)
                        {
                            ModelState.AddModelError("", result.Errors.First());
                            ViewBag.RoleId = new SelectList(await RoleManager.Roles.ToListAsync(), "Name", "Name");
                            return View();
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("", adminresult.Errors.First());
                    ViewBag.RoleId = new SelectList(RoleManager.Roles, "Name", "Name");
                    return View();

                }
                return RedirectToAction("Index");
            }
            ViewBag.RoleId = new SelectList(RoleManager.Roles, "Name", "Name");
            return View();
        }

        public async Task<ActionResult> Edit(string id)
        {
            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
                throw new ArgumentException("ไม่พบบัญชีผู้ใช้นี้ในระบบ");

            var userRoles = await UserManager.GetRolesAsync(user.Id);

            return View(new vm_User()
            {
                Id = user.Id,
                Username = user.UserName,
                RolesList = RoleManager.Roles.ToList().Select(x => new SelectListItem()
                {
                    Selected = userRoles.Contains(x.Name),
                    Text = x.Name,
                    Value = x.Name
                })
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Username,Id")] vm_User editUser, params string[] selectedRole)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Something failed.");
                editUser.RolesList = RoleManager.Roles.ToList().Select(x => new SelectListItem()
                {
                    Selected = selectedRole.Contains(x.Name),
                    Text = x.Name,
                    Value = x.Name
                });

                return View(editUser);
            }

            var user = await UserManager.FindByIdAsync(editUser.Id);
            if (user == null)
                throw new ArgumentException("ไม่พบบัญชีผู้ใช้นี้ในระบบ");

            var userRoles = await UserManager.GetRolesAsync(user.Id);
            selectedRole = selectedRole ?? new string[] { };

            /// add a new roles.
            var result = await UserManager.AddToRolesAsync(user.Id, selectedRole.Except(userRoles).ToArray<string>());
            if (!result.Succeeded)
            {
                ModelState.AddModelError("", result.Errors.First());
                return View(editUser);
            }

            /// remove an old roles.
            result = await UserManager.RemoveFromRolesAsync(user.Id, userRoles.Except(selectedRole).ToArray<string>());
            if (!result.Succeeded)
            {
                ModelState.AddModelError("", result.Errors.First());
                return View(editUser);
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<ActionResult> Delete(string id)
        {

            var user = await UserManager.FindByIdAsync(id);
            if (user == null)
                throw new ArgumentException("ไม่พบข้อมูลในระบบ");

            var result = await UserManager.DeleteAsync(user);
            return RedirectToAction("Index");
        }
    }
}