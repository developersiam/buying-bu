﻿using BusinessLayer;
using BusinessLayer.Helper;
using BUWebApp.Areas.Admin.Models;
using BUWebApp.Helper;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CPAContainerItemController : Controller
    {
        // GET: Admin/CPAContainerItem
        public ActionResult Index()
        {
            ViewBag.CPAContainerItemList = BuyingFacade.CPAContainerItemBL()
                .GetAll()
                .OrderBy(x => x.Code)
                .ToList();

            return View(new vm_CPAContianerItem());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(vm_CPAContianerItem collaction)
        {
            if (!ModelState.IsValid)
                return View(collaction);

            BuyingFacade.CPAContainerItemBL()
                .Add(collaction.Code,
                collaction.Name,
                collaction.Description,
                collaction.Unit,
                HttpContext.User.Identity.Name);

            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(vm_CPAContianerItem collaction)
        {
            if (!ModelState.IsValid)
                return View(collaction);

            BuyingFacade.CPAContainerItemBL()
                .Edit(collaction.Code,
                collaction.Name,
                collaction.Description,
                collaction.Unit,
                HttpContext.User.Identity.Name);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(string code)
        {
            BuyingFacade.CPAContainerItemBL().Delete(code);
            return RedirectToAction("Index");
        }
    }
}