﻿using AGMInventorySystemEntities;
using BUWebApp.Areas.Admin.Models;
using BUWebApp.Helper;
using BUWebApp.Models;
using BusinessLayer;
using BusinessLayer.Model;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CropInputsConditionController : Controller
    {
        [HttpGet]
        // GET: CropInputsConditions
        public ActionResult Index(string supplierCode)
        {
            var crop = AppSetting.Crop;
            ViewBag.Categories = AGMInventorySystemBL.BLServices
                .material_categoryBL()
                .GetAll()
                .OrderBy(x => x.category_name)
                .Select(x => new material_category
                {
                    categoryid = x.categoryid,
                    category_name = x.category_name
                })
                .ToList();

            ViewBag.Items = new List<material_item>();
            ViewBag.Brands = new List<material_brand>();

            var cropInputsItems = BusinessLayer.Helper
                .CropInputsItemHelper
                .GetBySupplierCode(crop.Crop1, supplierCode);

            var returnModel = new vm_CropItemCondition
            {
                Crop = crop.Crop1,
                SupplierCode = supplierCode,
                IsNotToInvoice = false,
                IsReferToInventory = false,
                IsUsePerRai = false,
                IsVat = false,
                CropInputsItems = cropInputsItems
            };

            return View(returnModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(vm_CropItemCondition collation)
        {
            BuyingFacade.CropInputsConditionBL()
                .Add(new CropInputsCondition
                {
                    ConditionID = Guid.NewGuid(),
                    Crop = collation.Crop,
                    SupplierCode = collation.SupplierCode,
                    ItemID = collation.ItemID,
                    UnitPrice = collation.UnitPrice,
                    IsUsePerRai = collation.IsUsePerRai,
                    IsNotToInvoice = collation.IsNotToInvoice,
                    IsReferToInventory = collation.IsReferToInventory,
                    IsVat = collation.IsVat,
                    Description = collation.Description,
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = HttpContext.User.Identity.Name
                });
            return RedirectToAction("Index", new { supplierCode = collation.SupplierCode });
        }

        [HttpGet]
        public ActionResult Delete(Guid conditionID, string supplierCode)
        {
            BuyingFacade.CropInputsConditionBL().Delete(conditionID);
            return RedirectToAction("Index", new { supplierCode = supplierCode });
        }

        [HttpGet]
        public ActionResult Edit(Guid conditionID)
        {
            var model = BuyingFacade.CropInputsConditionBL().GetSingle(conditionID);
            if (model == null)
                throw new ArgumentException("ไม่พบข้อมูลในระบบ");

            ViewBag.Categories = ViewBag.Categories = BusinessLayer.Helper
                    .CropInputsItemHelper
                    .GetBySupplierCode(model.Crop, model.SupplierCode)
                    .GroupBy(x => x.Category)
                    .OrderBy(x => x.Key)
                    .Select(x => new m_CropInputsItem
                    {
                        Category = x.Key
                    }).ToList();

            var returnModel = new vm_CropItemCondition
            {
                Crop = model.Crop,
                SupplierCode = model.SupplierCode,
                IsNotToInvoice = model.IsNotToInvoice,
                IsReferToInventory = model.IsReferToInventory,
                IsUsePerRai = model.IsUsePerRai,
                IsVat = model.IsVat
            };

            return View(returnModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(vm_CropItemCondition collation)
        {
            BuyingFacade.CropInputsConditionBL()
                .Edit(new CropInputsCondition
                {
                    ConditionID = collation.ConditionID,
                    Crop = collation.Crop,
                    SupplierCode = collation.SupplierCode,
                    ItemID = collation.ItemID,
                    UnitPrice = collation.UnitPrice,
                    IsUsePerRai = collation.IsUsePerRai,
                    IsNotToInvoice = collation.IsNotToInvoice,
                    IsReferToInventory = collation.IsReferToInventory,
                    IsVat = collation.IsVat,
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = HttpContext.User.Identity.Name
                });
            return RedirectToAction("Index", new { supplierCode = collation.SupplierCode });
        }
    }
}