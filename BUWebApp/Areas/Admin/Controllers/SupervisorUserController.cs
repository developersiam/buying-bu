﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DomainModel;
using BusinessLayer;
using BUWebApp.Areas.Admin.Models;
using BUWebApp.Models;

namespace BUWebApp.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SupervisorUserController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            vm_SupervisorUser supervisorUserViewModel = new vm_SupervisorUser();

            List<AspNetUser> supervisorUserList1 = new List<AspNetUser>();//Supervisor User ที่บันทึกไปแล้วก่อนหน้านี้
            List<AspNetUser> supervisorUserList2 = new List<AspNetUser>();//User ทั้งหมดที่มี Role = Supervisor

            supervisorUserList1 = BuyingFacade.UserManagementBL().GetSupervisorUsers().Select(u => u.AspNetUser).ToList();
            supervisorUserList2 = BuyingFacade.UserManagementBL().GetAspNetUserByRole("Supervisor");

            //ต้องการหาข้อมูลว่าใครที่ Role = Supervisor ที่ยังไม่ได้บันทึกลงใน Supervisor User
            HashSet<string> diffids = new HashSet<string>(supervisorUserList1.Select(s => s.Id));

            var aspNetUsersNotAdded = supervisorUserList2.Where(s => !diffids.Contains(s.Id)).ToList();

            supervisorUserViewModel.SupervisorUserNotMemberList = aspNetUsersNotAdded;
            supervisorUserViewModel.SupervisorList = BuyingFacade.UserManagementBL().GetSupervisorUsers();
            supervisorUserViewModel.AreaList = BuyingFacade.AreaBL().GetAll();

            return View(supervisorUserViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(vm_SupervisorUser collection)
        {
            // TODO: Add insert logic here
            if (ModelState.IsValid)
            {
                SupervisorUser supervisorUser = new SupervisorUser
                {
                    AreaCode = collection.AreaCode,
                    UserId = collection.UserId,
                    ModifiedUser = User.Identity.Name,
                    ModifiedDate = DateTime.Now
                };

                if (BuyingFacade.UserManagementBL().GetSupervisorUser(collection.AreaCode, collection.UserId) != null)
                {
                    ModelState.AddModelError("", "Dupplicate Data.");
                    return View(collection);
                }

                BuyingFacade.UserManagementBL().AddSupervisorUser(supervisorUser);
                return RedirectToAction("Index");
            }

            vm_SupervisorUser model = new vm_SupervisorUser();
            List<AspNetUser> supervisorUserList1 = new List<AspNetUser>();//Supervisor User ที่บันทึกไปแล้วก่อนหน้านี้
            List<AspNetUser> supervisorUserList2 = new List<AspNetUser>();//User ทั้งหมดที่มี Role = Supervisor

            supervisorUserList1 = BuyingFacade.UserManagementBL().GetSupervisorUsers().Select(u => u.AspNetUser).ToList();
            supervisorUserList2 = BuyingFacade.UserManagementBL().GetAspNetUserByRole("Supervisor");

            //ต้องการหาข้อมูลว่าใครที่ Role = Supervisor ที่ยังไม่ได้บันทึกลงใน Supervisor User
            HashSet<string> diffids = new HashSet<string>(supervisorUserList1.Select(s => s.Id));
            var aspNetUsersNotAdded = supervisorUserList2.Where(s => !diffids.Contains(s.Id)).ToList();

            model.SupervisorUserNotMemberList = aspNetUsersNotAdded;
            model.SupervisorList = BuyingFacade.UserManagementBL().GetSupervisorUsers();
            model.AreaList = BuyingFacade.AreaBL().GetAll();

            return View(model);
        }

        [HttpGet]
        public ActionResult Delete(string areaCode, string userId)
        {
            var supervisorUser = BuyingFacade.UserManagementBL()
                .GetSupervisorUser(areaCode, userId);
            if (supervisorUser == null)
                throw new ArgumentException("Find data not found.");

            BuyingFacade.UserManagementBL()
                .DeleteSupervisorUser(supervisorUser);
            return RedirectToAction("Index");
            //return View(supervisorUser);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(SupervisorUser collection)
        {
            var _delete = BuyingFacade.UserManagementBL()
                .GetSupervisorUser(collection.AreaCode, collection.UserId);
            if (_delete == null)
                throw new ArgumentException("Find data not found.");

            BuyingFacade.UserManagementBL().DeleteSupervisorUser(_delete);

            return RedirectToAction("Index");
        }
    }
}