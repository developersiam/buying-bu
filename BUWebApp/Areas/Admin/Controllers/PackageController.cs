﻿using BUWebApp.Areas.Admin.Models;
using BUWebApp.Helper;
using BUWebApp.Models;
using BusinessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PackageController : Controller
    {
        [HttpGet]
        // GET: Packages
        public ActionResult SupplierPackages(string supplierCode)
        {
            var crop = AppSetting.Crop;
            var list = BuyingFacade
                .CropInputsPackageBL()
                .GetBySupplier(crop.Crop1, supplierCode);
            ViewBag.Packages = list;

            return View(new vm_Package
            {
                Crop = crop.Crop1,
                SupplierCode = supplierCode
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        // GET: MaterialIssueds
        public ActionResult Add(vm_Package collation)
        {
            BuyingFacade.CropInputsPackageBL()
                .Add(new CropInputsPackage
                {
                    PackageCode = collation.PackageCode,
                    Crop = collation.Crop,
                    SupplierCode = collation.SupplierCode,
                    Description = collation.Description,
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = HttpContext.User.Identity.Name
                });

            return RedirectToAction("SupplierPackages", new { supplierCode = collation.SupplierCode });
        }

        [HttpGet]
        // GET: MaterialIssueds
        public ActionResult Delete(string packageCode)
        {
            var model = BuyingFacade.CropInputsPackageBL().GetSingle(packageCode);
            if (model == null)
                throw new ArgumentException("ไม่พบข้อมูลในระบบ");

            BuyingFacade.CropInputsPackageBL().Delete(model.PackageCode);
            return RedirectToAction("SupplierPackages", new { supplierCode = model.SupplierCode });
        }
    }
}