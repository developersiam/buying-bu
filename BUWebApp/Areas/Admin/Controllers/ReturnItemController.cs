﻿using AGMInventorySystemEntities;
using BusinessLayer;
using BusinessLayer.Model;
using BUWebApp.Areas.Admin.Models;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ReturnItemController : Controller
    {
        // GET: Admin/ReturnItem
        public ActionResult Index()
        {
            ViewBag.Items = new List<material_item>();
            ViewBag.Brands = new List<material_brand>();

            ViewBag.Categories = AGMInventorySystemBL.BLServices
                .material_categoryBL()
                .GetAll()
                .OrderBy(x => x.category_name)
                .Select(x => new material_category
                {
                    categoryid = x.categoryid,
                    category_name = x.category_name
                })
                .ToList();

            ViewBag.ItemTypes = BuyingFacade.CY202201ReturnItemBL().GetItemType();

            var returnItemList = BuyingFacade.CY202201ReturnItemBL().GetAll();
            var agmInventoryItemList = AGMInventorySystemBL.BLServices.material_itemBL().GetAll();

            ViewBag.ReturnItemList = (from a in returnItemList
                                      from b in agmInventoryItemList
                                      where a.ReturnItemID == b.itemid
                                      select new m_CY202201ReturnItem
                                      {
                                          ReturnItemID = a.ReturnItemID,
                                          ItemName = b.item_name,
                                          ItemType = a.ItemType,
                                          IsActive = a.IsActive,
                                          Remark = a.Remark
                                      })
                               .OrderBy(x => x.ItemType)
                               .ThenBy(x => x.ItemName)
                               .ToList();

            return View(new vm_ReturnItem
            {
                ItemType = "ตะกร้า",
                IsActive = true
            });
        }

        [HttpPost]
        public ActionResult Add(vm_ReturnItem collection)
        {
            BuyingFacade.CY202201ReturnItemBL()
                .Add(new CY202201ReturnItem
                {
                    ReturnItemID = collection.ItemID,
                    ItemType = collection.ItemType,
                    IsActive = collection.IsActive,
                    Remark = collection.Remark,
                    RecordUser = HttpContext.User.Identity.Name,
                    RecordDate = DateTime.Now,
                    ModifiedUser = HttpContext.User.Identity.Name,
                    ModifiedDate = DateTime.Now
                });
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int itemID)
        {
            var item = BuyingFacade.CY202201ReturnItemBL().GetSingle(itemID);
            if (item == null)
                throw new ArgumentException("ไม่พบข้อมูล ReturnItem นี้ในระบบ");

            var agmInventoryItem = AGMInventorySystemBL.BLServices.material_itemBL().GetSingle(itemID);
            if (agmInventoryItem == null)
                throw new ArgumentException("ไม่พบข้อมูล ReturnItem นี้ในระบบ AGM Inventory โปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

            ViewBag.ItemTypes = BuyingFacade.CY202201ReturnItemBL().GetItemType();

            return View(new m_CY202201ReturnItem
            {
                ReturnItemID = item.ReturnItemID,
                ItemType = item.ItemType,
                IsActive = item.IsActive,
                Remark = item.Remark,
                RecordDate = item.RecordDate,
                RecordUser = item.RecordUser,
                ModifiedDate = item.ModifiedDate,
                ModifiedUser = item.ModifiedUser,
                ItemName = agmInventoryItem.item_name
            });
        }

        [HttpPost]
        public ActionResult Edit(m_CY202201ReturnItem collection)
        {
            BuyingFacade.CY202201ReturnItemBL()
                .Update(new CY202201ReturnItem
                {
                    ReturnItemID = collection.ReturnItemID,
                    ItemType = collection.ItemType,
                    IsActive = collection.IsActive,
                    Remark = collection.Remark,
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = HttpContext.User.Identity.Name
                });
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int itemID)
        {
            BuyingFacade.CY202201ReturnItemBL().Delete(itemID);
            return RedirectToAction("Index");
        }
    }
}