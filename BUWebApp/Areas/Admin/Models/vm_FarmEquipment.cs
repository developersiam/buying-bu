﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.Admin.Models
{
    public class vm_FarmEquipment
    {
        public System.Guid MachineAndFarmEquipmentID { get; set; }
        [Required(ErrorMessage = "*Farm equipment name required")]
        public string MachineAndFarmEquipmentName { get; set; }
        public bool IsActive { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedUser { get; set; }
    }
}