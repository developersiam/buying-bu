﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DomainModel;

namespace BUWebApp.Areas.Admin.Models
{
    public class vm_Crop
    {
        [Required(ErrorMessage = "Crop required")]
        [Range(2000, short.MaxValue, ErrorMessage = "The value must be greater than 2000")]
        public short Crop { get; set; }
        [Required(ErrorMessage = "CropName required")]
        public string CropName { get; set; }
        [Required(ErrorMessage = "StartCrop required")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime StartCrop { get; set; }
        [Required(ErrorMessage = "EndCrop required")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime EndCrop { get; set; }
        [Required(ErrorMessage = "DefaultStatus required")]
        public bool DefaultStatus { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public System.DateTime LastModifed { get; set; }       
        public string ModifiedByUser { get; set; }
        public List<Crop> CropList { get; set; }
    }
}