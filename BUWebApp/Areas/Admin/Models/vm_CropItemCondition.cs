﻿using BusinessLayer.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.Admin.Models
{
    public class vm_CropItemCondition
    {
        public System.Guid ConditionID { get; set; }

        [Required(ErrorMessage = "Please specify crop")]
        [Display(Name = "Crop")]
        public short Crop { get; set; }

        [Required(ErrorMessage = "Please specify supplier code")]
        [Display(Name = "SupplierCode")]
        public string SupplierCode { get; set; }

        [Required(ErrorMessage = "Please select item")]
        [Display(Name = "ItemID")]
        public int ItemID { get; set; }
        [Required(ErrorMessage = "Please enter unit price")]
        [Display(Name = "UnitPrice")]
        [RegularExpression(@"^[0-9]+(\.[0-9]{1,2})$", ErrorMessage = "Unit price is not valid.")]
        [Range(0, 5000,
        ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public decimal UnitPrice { get; set; }

        [Required(ErrorMessage = "Please specify IsUsePerRai")]
        [Display(Name = "IsUsePerRai")]
        public bool IsUsePerRai { get; set; }
        [Required(ErrorMessage = "Please specify IsDeduction")]
        [Display(Name = "IsNotToInvoice")]
        public bool IsNotToInvoice { get; set; }
        [Required(ErrorMessage = "Please specify IsReferToInventory")]
        [Display(Name = "IsReferToInventory")]
        public bool IsReferToInventory { get; set; }
        [Required(ErrorMessage = "Please specify IsVat")]
        [Display(Name = "IsVat")]
        public bool IsVat { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }

        public string Category { get; set; }
        public string ItemName { get; set; }
        public string Brand { get; set; }

        public List<m_CropInputsItem> CropInputsItems { get; set; }
    }
}