﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AGMInventorySystemEntities;
using System.ComponentModel.DataAnnotations;

namespace BUWebApp.Areas.Admin.Models
{
    public class vm_ReturnItem
    {
        [Required(ErrorMessage = "required")]
        public int ItemID { get; set; }
        [Required(ErrorMessage = "required")]
        public string ItemType { get; set; }
        public bool IsActive { get; set; }
        public string Remark { get; set; }

        public string Category { get; set; }
        public string ItemName { get; set; }
        public string Brand { get; set; }
    }
}