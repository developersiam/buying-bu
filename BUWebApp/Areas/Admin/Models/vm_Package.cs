﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.Admin.Models
{
    public class vm_Package
    {
        [Required(ErrorMessage = "Please enter package code")]
        [Display(Name = "Package Code")]
        public string PackageCode { get; set; }
        [Required(ErrorMessage = "Please enter crop")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Crop is not valid.")]
        [Display(Name = "Crop")]
        public short Crop { get; set; }
        [Required(ErrorMessage = "Please enter extension agent")]
        [Display(Name = "Supplier Code")]
        public string SupplierCode { get; set; }
        [Required(ErrorMessage = "Please enter description")]
        [Display(Name = "Description")]
        public string Description { get; set; }
    }
}