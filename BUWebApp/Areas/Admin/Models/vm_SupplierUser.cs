﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DomainModel;
using System.ComponentModel.DataAnnotations;

namespace BUWebApp.Areas.Admin.Models
{
    public class vm_SupplierUser
    {
        [Required(ErrorMessage = "UseId required")]
        public string UserId { get; set; }
        [Required(ErrorMessage = "SupplierCode required")]
        public string SupplierCode { get; set; }
        public string ModifiedByUser { get; set; }
        public System.DateTime LastModifiedDate { get; set; }
        public List<AspNetUser> SupplierUserNotMember { get; set; }
        public List<SupplierUser> SupplierUserList { get; set; }
        public List<Supplier> SupplierList { get; set; }
    }
}