﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DomainModel;
using System.ComponentModel.DataAnnotations;

namespace BUWebApp.Areas.Admin.Models
{
    public class vm_SupervisorUser
    {
        [Required(ErrorMessage = "AreaCode required")]
        public string AreaCode { get; set; }
        [Required(ErrorMessage = "UserId required")]
        public string UserId { get; set; }
        public string ModifiedUser { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }

        public List<Area> AreaList { get; set; }
        public List<AspNetUser> SupervisorUserNotMemberList { get; set; }
        public List<SupervisorUser> SupervisorList { get; set; }
    }
}