﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DomainModel;
using System.ComponentModel.DataAnnotations;

namespace BUWebApp.Areas.Admin.Models
{
    public class vm_CropInputItem
    {
        public System.Guid ID { get; set; }
        [Required(ErrorMessage = "Product required")]
        public System.Guid CropInputCategoryID { get; set; }
        [Required(ErrorMessage = "Unit Price required")]
        public decimal UnitPrice { get; set; }
        public short Crop { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedUser { get; set; }

        public virtual CropInputCategory CropInputCategory { get; set; }
    }
}