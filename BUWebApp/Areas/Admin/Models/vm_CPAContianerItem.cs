﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.Admin.Models
{
    public class vm_CPAContianerItem
    {

        [Required(ErrorMessage = "required")]
        public string Code { get; set; }

        [Required(ErrorMessage = "required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "required")]
        public string Description { get; set; }

        [Required(ErrorMessage = "required")]
        public string Unit { get; set; }
    }
}