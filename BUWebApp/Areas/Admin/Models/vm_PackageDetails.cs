﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.Admin.Models
{
    public class vm_PackageDetails
    {
        [Required(ErrorMessage = "Please enter package code")]
        [Display(Name = "Package Code")]
        public string PackageCode { get; set; }
        [Required(ErrorMessage = "Please select item")]
        [Display(Name = "Item ID")]
        public Guid ConditionID { get; set; }
        [Required(ErrorMessage = "Please enter quantity")]
        [Display(Name = "Quantity")]
        [RegularExpression("([0-9]+)", ErrorMessage = "Quantity is not valid.")]
        [Range(1, 100,
        ErrorMessage = "Value for {0} must be between {1} and {2}.")]
        public int Quantity { get; set; }
    }
}