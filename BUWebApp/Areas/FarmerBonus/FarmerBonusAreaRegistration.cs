﻿using System.Web.Mvc;

namespace BUWebApp.Areas.FarmerBonus
{
    public class FarmerBonusAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "FarmerBonus";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "FarmerBonus_default",
                "FarmerBonus/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}