﻿using BusinessLayer.Helper;
using BusinessLayer;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;
using System.Data;
using BUWebApp.Areas.FarmerBonus.Data;
using BusinessLayer.Helper.FarmerBonus;
using BUWebApp.Helper;
using BusinessLayer.Model.FarmerBonus;

namespace BUWebApp.Areas.FarmerBonus.Controllers
{
    public class FullQuotaBonusController : Controller
    {
        [Authorize(Roles = "Accounting,Supervisor")]

        // GET: Accounting/FullQuotaBonus
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.FullQuotaBonusSummary = FullQuotaBonusHelper
                .GetFullQuotaBonusSummary(AppSetting.Crop.Crop1);

            return View();
        }

        [HttpGet]
        public ActionResult Details(string status)
        {
            var detailsList = new List<m_FullQuotaBonusDetails>();
            var resultList = new List<m_FullQuotaBonusDetails>();
            detailsList = FullQuotaBonusHelper
                .GetFullQuotaBonusDetails(
                AppSetting.Crop.Crop1, status);

            var suppervisorUser = AppSetting.SupervisorUser;
            if (suppervisorUser != null)
                resultList = detailsList
                    .Where(x => x.AreaCode == suppervisorUser.AreaCode)
                    .ToList();
            else
                resultList = detailsList;

            ViewBag.FullQuotaBonusDetails = resultList;
            ViewBag.Status = status;
            return View();
        }

        [HttpGet]
        public ActionResult Export(string status)
        {
            try
            {
                var list = BuyingFacade.FullQuotaBonusBL()
                        .GetByStatus(AppSetting.Crop.Crop1, status);
                var fileDownloadName = "D:\\FullQuotaBonus.xlsx";
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                FileInfo info = new FileInfo(fileDownloadName);
                if (info.Exists)
                    System.IO.File.Delete(fileDownloadName);

                ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
                using (ExcelPackage pck = new ExcelPackage(fileDownloadName))
                {
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add(fileDownloadName);
                    ws.Cells["A1"].LoadFromCollection(list, true);
                    ws.Protection.IsProtected = false;

                    for (int i = 1; i <= 16; i++)
                        ws.Column(i).AutoFit();

                    var fileStream = new MemoryStream();
                    pck.SaveAs(fileStream);
                    fileStream.Position = 0;

                    var fsr = new FileStreamResult(fileStream, contentType);
                    fsr.FileDownloadName = fileDownloadName;

                    return fsr;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult Operation(string status, string operation, params string[] farmerCodeList)
        {
            var crop = AppSetting.Crop.Crop1;
            switch (operation)
            {
                case "ConfirmTransfer":
                    BuyingFacade.FullQuotaBonusBL()
                        .TransferConfirm(crop, farmerCodeList);
                    break;
                case "ConfirmPrint":
                    BuyingFacade.FullQuotaBonusBL()
                        .PrintConfirm(crop, farmerCodeList);
                    TempData["FarmerCodeList"] = farmerCodeList;
                    return RedirectToAction("Print", new { status = status });
                case "CancelTransferred":
                    BuyingFacade.FullQuotaBonusBL()
                        .CencelTransferred(crop, farmerCodeList);
                    break;
                case "CancelForPrint":
                    BuyingFacade.FullQuotaBonusBL()
                        .CencelPrinted(crop, farmerCodeList);
                    break;
                default:
                    break;
            }
            return RedirectToAction("Details", new { status = status });
        }

        [HttpGet]
        public ActionResult Print(string status)
        {
            //var farmers = BuyingFacade.FarmerBL().GetBySupplierCode("ABC/S");
            //var farmerResult = farmers.Where(x => farmerCodeList.Contains(x.FarmerCode)).ToList();
            //ReportDataSource personDataSource = new ReportDataSource();
            //BuyingSystemDataSet.PersonDataTableDataTable personDataTable = new BuyingSystemDataSet.PersonDataTableDataTable();

            var farmerCodeList = TempData["FarmerCodeList"] as string[];
            //string[] farmerCodeList = { "S005701", "S005706", "S005706", "S005741", "S001306" };
            var farmerBonusList = FullQuotaBonusHelper
                .GetFullQuotaBonusDetailsBySelectedFarmer(AppSetting.Crop.Crop1, "Completed", farmerCodeList);
            var fullQuotaBonusDataTable = new FarmerQuotaBonusDataSet.FullQuotaBonusDataTableDataTable();
            ReportDataSource fullQuotaBonusDataSource = new ReportDataSource();

            foreach (var item in farmerBonusList
                .OrderBy(x => x.ReceiptOrder))
            {
                var thaiBathText = BuyingFacade.StoreProcetureBL()
                    .sp_ConvertCurrencyToThaiBath(item.BonusAmount);
                var str = item.TransferredDate.Split('-');
                var day = str[2];
                var month = str[1];
                var year = str[0];

                switch (month)
                {
                    case "1":
                        month = "มกราคม";
                        break;
                    case "2":
                        month = "กุมภาพันธ์";
                        break;
                    case "3":
                        month = "มีนาคม";
                        break;
                    case "4":
                        month = "เมษายน";
                        break;
                    case "5":
                        month = "พฤษภาคม";
                        break;
                    case "6":
                        month = "มิถุนายน";
                        break;
                    case "7":
                        month = "กรกฎาคม";
                        break;
                    case "8":
                        month = "สิงหาคม";
                        break;
                    case "9":
                        month = "กันยายน";
                        break;
                    case "10":
                        month = "ตุลาคม";
                        break;
                    case "11":
                        month = "พฤศจิกายน";
                        break;
                    case "12":
                        month = "ธันวาคม";
                        break;
                    default:
                        break;
                }

                DataRow row = fullQuotaBonusDataTable.NewRow();
                row["Prefix"] = item.Prefix;
                row["FirstName"] = item.FirstName;
                row["LastName"] = item.LastName;
                row["CitizenID"] = item.CitizenID;
                row["HouseNumber"] = item.HouseNumber;
                row["Village"] = item.Village;
                row["Tumbon"] = item.Tumbon;
                row["Amphur"] = item.Amphur;
                row["Province"] = item.Province;
                row["BankAccount"] = item.BankAccount;
                row["BankName"] = item.BankName;
                row["QuotaRai"] = item.QuotaRai;
                row["QuotaKg"] = item.QuotaKg;
                row["SoldKg"] = item.SoldKg;
                row["Supplier"] = item.ExtensionAgentCode;
                row["FarmerCode"] = item.FarmerCode;
                row["BonusAmount"] = item.BonusAmount;
                row["TransferredDate"] = day;
                row["TransferredMonth"] = month;
                row["TransferredYear"] = year;
                row["ThaiBathText"] = thaiBathText;
                row["ReceiptOrder"] = item.ReceiptOrder;
                fullQuotaBonusDataTable.Rows.Add(row);
            }

            fullQuotaBonusDataSource.Name = "DataSet1";
            fullQuotaBonusDataSource.Value = fullQuotaBonusDataTable;
            string reportPath = Server.MapPath("~/Areas/FarmerBonus/RDLC/FullQuotaBonusReceipt.rdlc");
            LocalReport report = new LocalReport();
            report.DataSources.Add(fullQuotaBonusDataSource);
            report.ReportPath = reportPath;

            string filePath = Path.GetTempFileName();
            //throw new Exception(filePath);

            Export(report, filePath);

            //Clost Report object.           
            report.Dispose();
            return File(filePath, "application/pdf");
        }

        public string Export(LocalReport report, string filePath)
        {
            string ack = "";
            try
            {
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;

                byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);
                using (FileStream stream = System.IO.File.OpenWrite(filePath))
                {
                    stream.Write(bytes, 0, bytes.Length);
                }
                return ack;
            }
            catch (Exception ex)
            {
                ack = ex.InnerException.Message;
                return ack;
            }
        }
    }
}