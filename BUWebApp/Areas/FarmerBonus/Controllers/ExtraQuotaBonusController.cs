﻿using BusinessLayer;
using BusinessLayer.Helper.FarmerBonus;
using BusinessLayer.Model.FarmerBonus;
using BUWebApp.Areas.FarmerBonus.Data;
using BUWebApp.Helper;
using Microsoft.Reporting.WebForms;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.FarmerBonus.Controllers
{
    [Authorize(Roles = "Accounting,Supervisor")]
    public class ExtraQuotaBonusController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            //BuyingFacade.ExtraBonusBL().Calculate(AppSetting.Crop.Crop1);
            var bonusList = ExtraBonusHelper
                .GetExtraBonusSummaries(AppSetting.Crop.Crop1);
            ViewBag.BonusSummaryList = bonusList;
            return View();
        }

        [HttpGet]
        public ActionResult Details(string status)
        {
            var detailsList = new List<m_ExtraBonusDetails>();
            var resultList = new List<m_ExtraBonusDetails>();
            detailsList = ExtraBonusHelper
                .GetExtraBonusDetails(AppSetting.Crop.Crop1, status);

            var suppervisorUser = AppSetting.SupervisorUser;
            if (suppervisorUser != null)
                resultList = detailsList
                    .Where(x => x.AreaCode == suppervisorUser.AreaCode)
                    .ToList();
            else
                resultList = detailsList;

            ViewBag.ExtraBonusDetailsList = resultList;
            ViewBag.Status = status;
            return View();
        }

        [HttpGet]
        public ActionResult Calculate()
        {
            BuyingFacade.ExtraBonusBL().Calculate(AppSetting.Crop.Crop1);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Export(string status)
        {
            try
            {
                var list = BuyingFacade.FullQuotaBonusBL()
                        .GetByStatus(AppSetting.Crop.Crop1, status);
                var fileDownloadName = "D:\\FullQuotaBonus.xlsx";
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                FileInfo info = new FileInfo(fileDownloadName);
                if (info.Exists)
                    System.IO.File.Delete(fileDownloadName);

                ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
                using (ExcelPackage pck = new ExcelPackage(fileDownloadName))
                {
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add(fileDownloadName);
                    ws.Cells["A1"].LoadFromCollection(list, true);
                    ws.Protection.IsProtected = false;

                    for (int i = 1; i <= 16; i++)
                        ws.Column(i).AutoFit();

                    var fileStream = new MemoryStream();
                    pck.SaveAs(fileStream);
                    fileStream.Position = 0;

                    var fsr = new FileStreamResult(fileStream, contentType);
                    fsr.FileDownloadName = fileDownloadName;

                    return fsr;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult Operation(string status, string operation, string[] farmerCodeList, string[] receiptOrderList)
        {
            var crop = AppSetting.Crop.Crop1;
            var username = HttpContext.User.Identity.Name;
            List<int> numberArray = null;
            if (receiptOrderList != null)
                numberArray = receiptOrderList
                    .Select(x => Convert.ToInt32(x))
                    .ToList();

            switch (operation)
            {
                case "ConfirmTransfer":
                    BuyingFacade.ExtraBonusBL()
                        .ConfirmToTransfer(farmerCodeList, username);
                    break;
                case "ConfirmPrint":
                    BuyingFacade.ExtraBonusBL()
                        .ConfirmToPrint(numberArray, username);
                    TempData["ReceiptOrderList"] = numberArray;
                    return RedirectToAction("Print", new { status = status });
                case "CancelTransferred":
                    BuyingFacade.ExtraBonusBL()
                        .CancelTransferred(numberArray);
                    break;
                case "CancelForPrint":
                    BuyingFacade.ExtraBonusBL()
                        .CancelPrinted(numberArray);
                    break;
                default:
                    break;
            }
            return RedirectToAction("Details", new { status = status });
        }

        [HttpGet]
        public ActionResult Print(string status)
        {
            var receiptOrderList = TempData["ReceiptOrderList"] as List<int>;
            //List<int> numberArray;
            //numberArray = receiptOrderList
            //    .Select(x => Convert.ToInt32(x))
            //    .ToList();
            var farmerBonusList = ExtraBonusHelper
                .GetExtraBonusDetailsBySelectedReceiptOrder(AppSetting.Crop.Crop1, "Completed", receiptOrderList);
            var extraQuotaBonusDataTable = new FarmerQuotaBonusDataSet.ExtraQuotaBonusDataTableDataTable();
            ReportDataSource extraQuotaBonusDataSource = new ReportDataSource();

            foreach (var item in farmerBonusList
                .OrderBy(x => x.ReceiptOrder))
            {
                var thaiBathText = BuyingFacade.StoreProcetureBL()
                    .sp_ConvertCurrencyToThaiBath(item.BonusAmount);
                var transDate = (DateTime)item.TransferredDate;
                var day = transDate.Day.ToString();
                var month = transDate.Month.ToString();
                var year = transDate.Year.ToString();

                switch (month)
                {
                    case "1":
                        month = "มกราคม";
                        break;
                    case "2":
                        month = "กุมภาพันธ์";
                        break;
                    case "3":
                        month = "มีนาคม";
                        break;
                    case "4":
                        month = "เมษายน";
                        break;
                    case "5":
                        month = "พฤษภาคม";
                        break;
                    case "6":
                        month = "มิถุนายน";
                        break;
                    case "7":
                        month = "กรกฎาคม";
                        break;
                    case "8":
                        month = "สิงหาคม";
                        break;
                    case "9":
                        month = "กันยายน";
                        break;
                    case "10":
                        month = "ตุลาคม";
                        break;
                    case "11":
                        month = "พฤศจิกายน";
                        break;
                    case "12":
                        month = "ธันวาคม";
                        break;
                    default:
                        break;
                }

                DataRow row = extraQuotaBonusDataTable.NewRow();
                row["Prefix"] = item.Prefix;
                row["FirstName"] = item.FirstName;
                row["LastName"] = item.LastName;
                row["CitizenID"] = item.CitizenID;
                row["HouseNumber"] = item.HouseNumber;
                row["Village"] = item.Village;
                row["Tumbon"] = item.Tumbon;
                row["Amphur"] = item.Amphur;
                row["Province"] = item.Province;
                row["BankAccount"] = item.BankAccount;
                row["BankName"] = item.BankName;
                row["QuotaRai"] = item.QuotaRai;
                row["QuotaKg"] = item.QuotaKg;
                row["SoldKg"] = item.SoldKg;
                row["ExtraSoldKg"] = item.ExtraSoldKg;
                row["Supplier"] = item.ExtensionAgentCode;
                row["FarmerCode"] = item.FarmerCode;
                row["BonusAmount"] = item.BonusAmount;
                row["TransferredDate"] = day;
                row["TransferredMonth"] = month;
                row["TransferredYear"] = year;
                row["ThaiBathText"] = thaiBathText;
                row["ReceiptOrder"] = item.ReceiptOrder;
                extraQuotaBonusDataTable.Rows.Add(row);
            }

            extraQuotaBonusDataSource.Name = "DataSet1";
            extraQuotaBonusDataSource.Value = extraQuotaBonusDataTable;
            string reportPath = Server.MapPath("~/Areas/FarmerBonus/RDLC/ExtraQuotaBonusReceipt.rdlc");
            LocalReport report = new LocalReport();
            report.DataSources.Add(extraQuotaBonusDataSource);
            report.ReportPath = reportPath;

            string filePath = Path.GetTempFileName();
            //throw new Exception(filePath);

            Export(report, filePath);

            //Clost Report object.           
            report.Dispose();
            return File(filePath, "application/pdf");
        }

        public string Export(LocalReport report, string filePath)
        {
            string ack = "";
            try
            {
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;

                byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);
                using (FileStream stream = System.IO.File.OpenWrite(filePath))
                {
                    stream.Write(bytes, 0, bytes.Length);
                }
                return ack;
            }
            catch (Exception ex)
            {
                ack = ex.InnerException.Message;
                return ack;
            }
        }
    }
}