﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.ALP.Models
{
    public class vm_IncidentFollowUp
    {
        public Guid ID { get; set; }
        public Guid IncidentFarmerID { get; set; }
        [Display(Name = "Follow up date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Follow up date is required")]
        public System.DateTime FollowUpDate { get; set; }
        [Display(Name = "Description")]
        [Required(ErrorMessage = "Follow up description is required")]
        public string FollowUpDescription { get; set; }
        public System.DateTime RecordDate { get; set; }
        public string RecordBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }

        public vm_IncidentFarmer IncidentFarmer { get; set; }
    }
}