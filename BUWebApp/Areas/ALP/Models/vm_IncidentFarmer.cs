﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.ALP.Models
{
    public class vm_IncidentFarmer
    {
        public Guid ID { get; set; }
        [Required(ErrorMessage = "Crop is required")]
        [Display(Name = "Crop")]
        public short Crop { get; set; }
        [Required(ErrorMessage = "CitizenID is required")]
        [Display(Name = "CitizenID")]
        public string CitizenID { get; set; }
        [Required(ErrorMessage = "Description is required")]
        [Display(Name = "Description")]
        public System.Guid DescriptionID { get; set; }

        [Display(Name = "Incident Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Incident Date is required")]
        public System.DateTime IncidentDate { get; set; }

        [Required(ErrorMessage = "Incident Status is required")]
        [Display(Name = "Incident Status")]
        public string IncidentStatus { get; set; }
        [Display(Name = "Follow Up Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MM-yyyy}", ApplyFormatInEditMode = true)]
        public Nullable<System.DateTime> FollowUpDate { get; set; }
        public System.DateTime RecordDate { get; set; }
        public string RecordBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }

        public Guid CategoryID { get; set; }
        public Guid TopicID { get; set; }
        public Person Person { get; set; }
        public IEnumerable<SelectListItem> IncidentStatusDropDownList { get; set; }
        public IEnumerable<SelectListItem> IncidentCategoryDropDownList { get; set; }
        public IEnumerable<SelectListItem> IncidentTopicDropDownList { get; set; }
        public IEnumerable<SelectListItem> IncidentDescriptionDropDownList { get; set; }
        public List<IncidentFarmer> IncidentFarmers { get; set; }

        public string CategoryName { get; set; }
        public string TopicName { get; set; }
        public string DescriptionName { get; set; }
        public int TotalCases { get; set; }
    }
}