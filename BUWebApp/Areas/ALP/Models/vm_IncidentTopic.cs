﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.ALP.Models
{
    public class vm_IncidentTopic
    {
        public Guid ID { get; set; }
        [Required(ErrorMessage = "Topic title is required")]
        [Display(Name = "Topic Title")]
        public string TopicTitle { get; set; }
        [Required(ErrorMessage = "Category is required")]
        [Display(Name = "Category")]
        public System.Guid CategoryID { get; set; }
        [Display(Name = "Category Name")]
        public string CategoryName { get; set; }
        [Display(Name = "Categories")]
        public IEnumerable<SelectListItem> Categories { get; set; }
        [Display(Name = "Create Date")]
        public System.DateTime CreateDate { get; set; }
        [Display(Name = "Create By")]
        public string CreateBy { get; set; }
        [Display(Name = "Modified Date")]
        public System.DateTime ModifiedDate { get; set; }
        [Display(Name = "Modified By")]
        public string ModifiedBy { get; set; }
    }
}