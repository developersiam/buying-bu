﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.ALP.Models
{
    public class vm_IncidentDescription
    {
        public System.Guid ID { get; set; }
        [Required(ErrorMessage = "Decription is required")]
        [Display(Name = "Decription")]
        public string Description { get; set; }
        [Required(ErrorMessage = "Topic is required")]
        public System.Guid TopicID { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Guid CategoryID { get; set; }
        [Display(Name = "Category")]
        public string CategoryName { get; set; }
        [Display(Name = "Topic")]
        public string TopicTitle { get; set; }
        public IEnumerable<SelectListItem> Categories { get; set; }
        public IEnumerable<SelectListItem> Topics { get; set; }
    }
}