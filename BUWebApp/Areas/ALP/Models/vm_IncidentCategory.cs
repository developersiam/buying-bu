﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.ALP.Models
{
    public class vm_IncidentCategory
    {
        public System.Guid ID { get; set; }
        [Required(ErrorMessage = "Name is required")]
        [Display(Name = "Category Name")]
        public string CategoryName { get; set; }
        [Display(Name = "Create Date")]
        public System.DateTime CreateDate { get; set; }
        [Display(Name = "Create By")]
        public string CreateBy { get; set; }
        [Display(Name = "Modified Date")]
        public System.DateTime ModifiedDate { get; set; }
        [Display(Name = "Modified By")]
        public string ModifiedBy { get; set; }
    }
}