﻿using BusinessLayer;
using BUWebApp.Areas.ALP.Models;
using BUWebApp.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.ALP.Controllers
{
    public struct IncidentStatus
    {
        public string Status { get; set; }
    }

    [Authorize(Roles = "ALP")]
    public class IncidentFarmerController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            var _crop = AppSetting.Crop;
            ViewBag.Crop = _crop.Crop1;

            return View(BuyingFacade.IncidentFarmerBL()
                .GetByCrop(_crop.Crop1)
                .GroupBy(x => new
                {
                    x.IncidentDescription.IncidentTopic.IncidentCategory.CategoryName,
                    x.IncidentDescription.IncidentTopic.TopicTitle,
                    x.IncidentDescription.Description,
                    x.DescriptionID,
                    x.Crop
                })
                .Select(x => new vm_IncidentFarmer
                {
                    CategoryName = x.Key.CategoryName,
                    TopicName = x.Key.TopicTitle,
                    DescriptionName = x.Key.Description,
                    DescriptionID = x.Key.DescriptionID,
                    Crop = x.Key.Crop,
                    TotalCases = x.Count()
                }));
        }

        // GET: IncidentFarmer
        [HttpGet]
        public ActionResult Add(short crop, string citizenID)
        {
            var registrationList = BuyingFacade.RegistrationBL().GetByCropAndCitizenID(crop, citizenID);
            if (registrationList.Count() <= 0)
                throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รหัสประจำตัวประชาชนหมายเลข " + citizenID + " ในปี " + crop);

            var incidentCategories = BuyingFacade.IncidentCategoryBL()
                .GetAll()
                .OrderBy(x => x.CategoryName)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.CategoryName
                });

            var incidentDescriptions = BuyingFacade.IncidentDescriptionBL()
                .GetByTopic(Guid.NewGuid())
                .OrderBy(x => x.IncidentTopic.IncidentCategory.CategoryName)
                .ThenBy(x => x.IncidentTopic.TopicTitle)
                .ThenBy(x => x.Description)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.Description
                });

            var incidentTopics = BuyingFacade.IncidentTopicBL()
                .GetByCategory(Guid.NewGuid())
                .OrderBy(x => x.TopicTitle)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.TopicTitle
                });

            var incidentFarmers = BuyingFacade.IncidentFarmerBL()
                .GetByFarmer(crop, citizenID)
                .OrderBy(x => x.IncidentDescription.IncidentTopic.IncidentCategory.CategoryName)
                .ThenBy(x => x.IncidentDescription.IncidentTopic.TopicTitle)
                .ThenBy(x => x.IncidentDescription.Description)
                .ToList();

            List<IncidentStatus> list = new List<IncidentStatus>();
            list.Add(new IncidentStatus { Status = "Ongoing" });
            list.Add(new IncidentStatus { Status = "Closed" });

            var incidentStatus = list
                .Select(x => new SelectListItem
                {
                    Value = x.Status,
                    Text = x.Status
                });

            /// Show a farmer quota from sign contract.
            /// 
            var registration = BuyingFacade.RegistrationBL()
                .GetByCropAndCitizenID(crop, citizenID);

            string regisInfo = "{ ";

            foreach (var row in registration)
                regisInfo = regisInfo + row.FarmerCode + " : " + row.QuotaFromSignContract + ", ";

            regisInfo = regisInfo + " }";
            ViewBag.QuotaDetails = regisInfo;

            return View(new vm_IncidentFarmer
            {
                Crop = crop,
                Person = BuyingFacade.PersonBL().GetSingle(citizenID),
                CitizenID = citizenID,
                IncidentDescriptionDropDownList = incidentDescriptions,
                IncidentCategoryDropDownList = incidentCategories,
                IncidentTopicDropDownList = incidentTopics,
                IncidentStatusDropDownList = incidentStatus,
                IncidentFarmers = incidentFarmers
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(vm_IncidentFarmer collation)
        {
            var incidentDescriptions = BuyingFacade.IncidentDescriptionBL()
                .GetAll()
                .OrderBy(x => x.IncidentTopic.IncidentCategory.CategoryName)
                .ThenBy(x => x.IncidentTopic.TopicTitle)
                .ThenBy(x => x.Description)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.Description
                });

            var incidentCategories = BuyingFacade.IncidentCategoryBL()
                .GetAll()
                .OrderBy(x => x.CategoryName)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.CategoryName
                });

            var incidentTopics = BuyingFacade.IncidentTopicBL()
                .GetByCategory(Guid.NewGuid())
                .OrderBy(x => x.TopicTitle)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.TopicTitle
                });

            var incidentFarmers = BuyingFacade.IncidentFarmerBL()
                .GetByFarmer(collation.Crop, collation.CitizenID)
                .OrderBy(x => x.IncidentDescription.IncidentTopic.IncidentCategory.CategoryName)
                .ThenBy(x => x.IncidentDescription.IncidentTopic.TopicTitle)
                .ThenBy(x => x.IncidentDescription.Description)
                .ToList();

            List<IncidentStatus> list = new List<IncidentStatus>();
            list.Add(new IncidentStatus { Status = "Ongoing" });
            list.Add(new IncidentStatus { Status = "Closed" });

            var incidentStatus = list
                .Select(x => new SelectListItem
                {
                    Value = x.Status,
                    Text = x.Status
                });

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Model state is error.");
                collation.Person = BuyingFacade.PersonBL().GetSingle(collation.CitizenID);
                collation.CitizenID = collation.CitizenID;
                collation.IncidentCategoryDropDownList = incidentCategories;
                collation.IncidentTopicDropDownList = incidentTopics;
                collation.IncidentDescriptionDropDownList = incidentDescriptions;
                collation.IncidentStatusDropDownList = incidentStatus;
                collation.IncidentFarmers = incidentFarmers;

                return View(collation);
            }

            BuyingFacade.IncidentFarmerBL()
                .Add(collation.Crop,
                collation.CitizenID,
                collation.DescriptionID,
                collation.IncidentDate,
                collation.IncidentStatus,
                DateTime.Now,
                HttpContext.User.Identity.Name);

            return RedirectToAction("Add", new { crop = collation.Crop, citizenID = collation.CitizenID });
        }

        // GET: IncidentFarmer
        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            var model = BuyingFacade.IncidentFarmerBL().GetSingle(id);
            if (model == null)
                throw new ArgumentException("ไม่พบข้อมูลในระบบ");

            var incidentCategories = BuyingFacade.IncidentCategoryBL()
                .GetAll()
                .OrderBy(x => x.CategoryName)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.CategoryName
                });

            var incidentTopics = BuyingFacade.IncidentTopicBL()
                .GetByCategory(model.IncidentDescription.IncidentTopic.CategoryID)
                .OrderBy(x => x.TopicTitle)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.TopicTitle
                });

            var incidentDescriptions = BuyingFacade.IncidentDescriptionBL()
                .GetByTopic(model.IncidentDescription.TopicID)
                .OrderBy(x => x.IncidentTopic.IncidentCategory.CategoryName)
                .ThenBy(x => x.IncidentTopic.TopicTitle)
                .ThenBy(x => x.Description)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.Description
                });

            var incidentFarmers = BuyingFacade.IncidentFarmerBL()
                .GetByFarmer(model.Crop, model.CitizenID)
                .OrderBy(x => x.IncidentDescription.IncidentTopic.IncidentCategory.CategoryName)
                .ThenBy(x => x.IncidentDescription.IncidentTopic.TopicTitle)
                .ThenBy(x => x.IncidentDescription.Description)
                .ToList();

            List<IncidentStatus> list = new List<IncidentStatus>();
            list.Add(new IncidentStatus { Status = "Ongoing" });
            list.Add(new IncidentStatus { Status = "Closed" });

            var incidentStatus = list
                .Select(x => new SelectListItem
                {
                    Value = x.Status,
                    Text = x.Status
                });

            /// Show a farmer quota from sign contract.
            /// 
            var registration = BuyingFacade.RegistrationBL()
                .GetByCropAndCitizenID(model.Crop, model.CitizenID);

            string regisInfo = "{ ";

            foreach (var row in registration)
                regisInfo = regisInfo + row.FarmerCode + " : " + row.QuotaFromSignContract + ", ";

            regisInfo = regisInfo + " }";
            ViewBag.QuotaDetails = regisInfo;

            return View(new vm_IncidentFarmer
            {
                Crop = model.Crop,
                CitizenID = model.CitizenID,
                DescriptionID = model.DescriptionID,
                TopicID = model.IncidentDescription.TopicID,
                CategoryID = model.IncidentDescription.IncidentTopic.CategoryID,
                IncidentDate = model.IncidentDate,
                IncidentStatus = model.IncidentStatus,
                FollowUpDate = model.FollowUpDate,
                RecordBy = model.RecordBy,
                RecordDate = model.RecordDate,
                ModifiedBy = model.ModifiedBy,
                ModifiedDate = model.ModifiedDate,
                IncidentDescriptionDropDownList = incidentDescriptions,
                IncidentCategoryDropDownList = incidentCategories,
                IncidentTopicDropDownList = incidentTopics,
                IncidentStatusDropDownList = incidentStatus,
                IncidentFarmers = incidentFarmers,
                Person = model.Person
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(vm_IncidentFarmer collation)
        {
            var model = BuyingFacade.IncidentFarmerBL().GetSingle(collation.ID);
            if (model == null)
                throw new ArgumentException("ไม่พบข้อมูลในระบบ");

            collation.Crop = model.Crop;
            collation.Person = model.Person;
            collation.CitizenID = collation.CitizenID;
            collation.DescriptionID = collation.DescriptionID;
            collation.TopicID = collation.TopicID;
            collation.CategoryID = collation.CategoryID;

            var incidentCategories = BuyingFacade.IncidentCategoryBL()
                .GetAll()
                .OrderBy(x => x.CategoryName)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.CategoryName
                });

            var incidentTopics = BuyingFacade.IncidentTopicBL()
                .GetByCategory(collation.CategoryID)
                .OrderBy(x => x.TopicTitle)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.TopicTitle
                });

            var incidentDescriptions = BuyingFacade.IncidentDescriptionBL()
                .GetByTopic(collation.TopicID)
                .OrderBy(x => x.IncidentTopic.IncidentCategory.CategoryName)
                .ThenBy(x => x.IncidentTopic.TopicTitle)
                .ThenBy(x => x.Description)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.Description
                });

            var incidentFarmers = BuyingFacade.IncidentFarmerBL()
                .GetByFarmer(collation.Crop, collation.CitizenID)
                .OrderBy(x => x.IncidentDescription.IncidentTopic.IncidentCategory.CategoryName)
                .ThenBy(x => x.IncidentDescription.IncidentTopic.TopicTitle)
                .ThenBy(x => x.IncidentDescription.Description)
                .ToList();

            List<IncidentStatus> list = new List<IncidentStatus>();
            list.Add(new IncidentStatus { Status = "Ongoing" });
            list.Add(new IncidentStatus { Status = "Closed" });

            var incidentStatus = list
                .Select(x => new SelectListItem
                {
                    Value = x.Status,
                    Text = x.Status
                });

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Model state is error.");

                collation.IncidentCategoryDropDownList = incidentCategories;
                collation.IncidentTopicDropDownList = incidentTopics;
                collation.IncidentDescriptionDropDownList = incidentDescriptions;
                collation.IncidentStatusDropDownList = incidentStatus;
                collation.IncidentFarmers = incidentFarmers;

                return View(collation);
            }

            BuyingFacade.IncidentFarmerBL()
                .Update(collation.ID,
                collation.Crop,
                collation.CitizenID,
                collation.DescriptionID,
                collation.IncidentDate,
                collation.IncidentStatus,
                Convert.ToDateTime(collation.FollowUpDate).Year < 2000 ? null : collation.FollowUpDate,
                HttpContext.User.Identity.Name);

            return RedirectToAction("Add", new { crop = model.Crop, citizenID = model.CitizenID });
        }

        [HttpGet]
        public ActionResult Delete(Guid id)
        {
            var model = BuyingFacade.IncidentFarmerBL().GetSingle(id);
            BuyingFacade.IncidentFarmerBL().Delete(id);
            return RedirectToAction("Add", new { crop = model.Crop, citizenID = model.CitizenID });
        }

        [HttpGet]
        public ActionResult CasesDetails(short crop, Guid descriptionID)
        {
            ViewBag.Crop = crop;

            var list = BuyingFacade.IncidentFarmerBL()
                .GetByCropAndDescription(crop, descriptionID)
                .Select(x => new vm_IncidentFarmer
                {
                    Crop = x.Crop,
                    CitizenID = x.CitizenID,
                    Person = x.Person,
                    CategoryName = x.IncidentDescription.IncidentTopic.IncidentCategory.CategoryName,
                    TopicName = x.IncidentDescription.IncidentTopic.TopicTitle,
                    DescriptionName = x.IncidentDescription.Description,
                    IncidentStatus = x.IncidentStatus,
                    IncidentDate = x.IncidentDate,
                    FollowUpDate = x.FollowUpDate,
                    ID = x.ID
                }).ToList();

            return View(list);
        }
    }
}