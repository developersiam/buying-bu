﻿using BusinessLayer;
using BUWebApp.Areas.ALP.Models;
using BUWebApp.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.ALP.Controllers
{
    [Authorize(Roles = "ALP")]
    public class IncidentDescriptionController : Controller
    {
        // GET: IncidentCategory
        [HttpGet]
        public ActionResult Index()
        {
            return View(BuyingFacade.IncidentDescriptionBL()
                .GetAll()
                .OrderBy(x => x.IncidentTopic.IncidentCategory.CategoryName)
                .ThenBy(x => x.IncidentTopic.TopicTitle)
                .ToList()
                .Select(x => new vm_IncidentDescription
                {
                    ID = x.ID,
                    Description = x.Description,
                    CategoryID = x.IncidentTopic.CategoryID,
                    CategoryName = x.IncidentTopic.IncidentCategory.CategoryName,
                    TopicID = x.TopicID,
                    TopicTitle = x.IncidentTopic.TopicTitle,
                    CreateBy = x.CreateBy,
                    CreateDate = x.CreateDate,
                    ModifiedBy = x.ModifiedBy,
                    ModifiedDate = x.ModifiedDate
                }));
        }

        //
        // GET: /IncidentCategory/Create
        [HttpGet]
        public ActionResult Create()
        {
            vm_IncidentDescription model = new vm_IncidentDescription();
            model.Categories = BuyingFacade.IncidentCategoryBL()
                .GetAll()
                .OrderBy(x => x.CategoryName)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.CategoryName
                });

            model.Topics = BuyingFacade.IncidentTopicBL()
                .GetAll()
                .Where(x => x.CreateBy == "")
                .OrderBy(x => x.IncidentCategory.CategoryName)
                .ThenBy(x => x.TopicTitle)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.TopicTitle
                });

            return View(model);
        }

        //
        // POST: /IncidentCategory/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(vm_IncidentDescription collation)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Please select category name.");
                collation.Categories = BuyingFacade.IncidentCategoryBL()
                .GetAll()
                .OrderBy(x => x.CategoryName)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.CategoryName
                });
                return View(collation);
            }

            BuyingFacade.IncidentDescriptionBL()
                .Add(collation.TopicID, collation.Description, AppSetting.Username);

            return RedirectToAction("Index");
        }

        //
        // GET: /IncidentCategory/Edit/1
        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            var model = BuyingFacade.IncidentDescriptionBL().GetSingle(id);
            if (model == null)
                throw new ArgumentException("ไม่พบข้อมูลในระบบ");

            return View(new vm_IncidentDescription
            {
                ID = model.ID,
                Description = model.Description,
                CreateBy = model.CreateBy,
                CreateDate = model.CreateDate,
                ModifiedBy = model.ModifiedBy,
                ModifiedDate = model.ModifiedDate,
                CategoryID = model.IncidentTopic.CategoryID,
                TopicID = model.TopicID,
                Topics = BuyingFacade.IncidentTopicBL()
                .GetByCategory(model.IncidentTopic.CategoryID)
                .OrderBy(x => x.TopicTitle)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.TopicTitle
                }),
                TopicTitle = model.IncidentTopic.TopicTitle,
                CategoryName = model.IncidentTopic.IncidentCategory.CategoryName,
                Categories = BuyingFacade.IncidentCategoryBL()
                .GetAll()
                .OrderBy(x => x.CategoryName)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.CategoryName
                })
            });
        }

        //
        // POST: /IncidentCategory/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(vm_IncidentDescription collation)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Model state error.");
                return View(collation);
            }

            BuyingFacade.IncidentDescriptionBL()
                .Update(collation.ID,
                collation.Description,
                collation.TopicID,
                AppSetting.Username);

            return RedirectToAction("Index");
        }

        //
        // GET: /IncidentCategory/Delete/1
        [HttpGet]
        public ActionResult Delete(Guid id)
        {
            BuyingFacade.IncidentDescriptionBL().Delete(id);
            return RedirectToAction("Index", "IncidentDescription");
        }

        //
        // GET: /IncidentCategory/Details/1
        [HttpGet]
        public ActionResult Details(Guid id)
        {
            var model = BuyingFacade.IncidentDescriptionBL().GetSingle(id);
            if (model == null)
                throw new ArgumentException("ไม่พบข้อมูลในระบบ");

            return View(new vm_IncidentDescription
            {
                ID = model.ID,
                Description = model.Description,
                CreateBy = model.CreateBy,
                CreateDate = model.CreateDate,
                ModifiedBy = model.ModifiedBy,
                ModifiedDate = model.ModifiedDate,
                CategoryID = model.IncidentTopic.CategoryID,
                TopicID = model.TopicID,
                TopicTitle = model.IncidentTopic.TopicTitle,
                CategoryName = model.IncidentTopic.IncidentCategory.CategoryName
            });
        }

        [HttpGet]
        public JsonResult GetTopics(Guid categoryID)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list = BuyingFacade.IncidentTopicBL()
                .GetByCategory(categoryID)
                .OrderBy(x => x.TopicTitle)
                .Select(x => new SelectListItem
                {
                    Text = x.TopicTitle,
                    Value = x.ID.ToString()
                }).ToList();
            return Json(new SelectList(list, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetDescriptions(Guid topicID)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list = BuyingFacade.IncidentDescriptionBL()
                .GetByTopic(topicID)
                .OrderBy(x => x.Description)
                .Select(x => new SelectListItem
                {
                    Text = x.Description,
                    Value = x.ID.ToString()
                }).ToList();
            return Json(new SelectList(list, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }
    }
}