﻿using BusinessLayer;
using BUWebApp.Areas.ALP.Models;
using BUWebApp.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.ALP.Controllers
{
    [Authorize(Roles = "ALP")]
    public class IncidentFollowUpController : Controller
    {
        // GET: IncidentFollowUps
        public ActionResult Index(Guid incidentFarmerID)
        {
            var model = BuyingFacade.IncidentFarmerBL().GetSingle(incidentFarmerID);
            if (model == null)
                throw new ArgumentException("ไม่พบหัวข้อหลักที่ต้องการติดตาม");

            var list = BuyingFacade.IncidentFollowUpBL()
                .GetByIncidentFarmer(incidentFarmerID);

            ViewBag.IncidentFollowUpList = list;

            /// Show a farmer quota from sign contract.
            /// 
            var registration = BuyingFacade.RegistrationBL()
                .GetByCropAndCitizenID(model.Crop, model.CitizenID);

            string regisInfo = "{ ";

            foreach (var row in registration)
                regisInfo = regisInfo + row.FarmerCode + " : " + row.QuotaFromSignContract + ", ";

            regisInfo = regisInfo + " }";
            ViewBag.QuotaDetails = regisInfo;

            var item = new vm_IncidentFollowUp
            {
                IncidentFarmer = new vm_IncidentFarmer
                {
                    Crop = model.Crop,
                    CitizenID = model.CitizenID,
                    DescriptionID = model.DescriptionID,
                    TopicID = model.IncidentDescription.TopicID,
                    CategoryID = model.IncidentDescription.IncidentTopic.CategoryID,
                    DescriptionName = model.IncidentDescription.Description,
                    TopicName = model.IncidentDescription.IncidentTopic.TopicTitle,
                    CategoryName = model.IncidentDescription.IncidentTopic.IncidentCategory.CategoryName,
                    IncidentDate = model.IncidentDate,
                    IncidentStatus = model.IncidentStatus,
                    FollowUpDate = DateTime.Now,
                    RecordBy = model.RecordBy,
                    RecordDate = model.RecordDate,
                    ModifiedBy = model.ModifiedBy,
                    ModifiedDate = model.ModifiedDate,
                    Person = model.Person
                },
                FollowUpDate = DateTime.Now,
                IncidentFarmerID = incidentFarmerID
            };

            return View(item);
        }

        [HttpGet]
        public ActionResult Delete(Guid id)
        {
            var model = BuyingFacade.IncidentFollowUpBL().GetSingle(id);
            if (model == null)
                throw new ArgumentException("ไม่พบหัวข้อที่ต้องการลบ");

            BuyingFacade.IncidentFollowUpBL().Delete(id);
            return RedirectToAction("Index", new { incidentFarmerID = model.IncidentFarmerID });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(vm_IncidentFollowUp collation)
        {
            var model = BuyingFacade.IncidentFarmerBL()
                .GetSingle(collation.IncidentFarmerID);
            if (model == null)
                throw new ArgumentException("ไม่พบหัวข้อหลักที่ต้องการติดตาม");

            if (!ModelState.IsValid)
            {
                ViewBag.IncidentFollowUpList = BuyingFacade.IncidentFollowUpBL()
                    .GetByIncidentFarmer(collation.IncidentFarmerID);

                return View(new vm_IncidentFollowUp
                {
                    IncidentFarmer = new vm_IncidentFarmer
                    {
                        Crop = model.Crop,
                        CitizenID = model.CitizenID,
                        DescriptionID = model.DescriptionID,
                        TopicID = model.IncidentDescription.TopicID,
                        CategoryID = model.IncidentDescription.IncidentTopic.CategoryID,
                        DescriptionName = model.IncidentDescription.Description,
                        TopicName = model.IncidentDescription.IncidentTopic.TopicTitle,
                        CategoryName = model.IncidentDescription.IncidentTopic.IncidentCategory.CategoryName,
                        IncidentDate = model.IncidentDate,
                        IncidentStatus = model.IncidentStatus,
                        FollowUpDate = DateTime.Now,
                        RecordBy = model.RecordBy,
                        RecordDate = model.RecordDate,
                        ModifiedBy = model.ModifiedBy,
                        ModifiedDate = model.ModifiedDate,
                        Person = model.Person
                    },
                    FollowUpDate = collation.FollowUpDate,
                    IncidentFarmerID = collation.IncidentFarmerID
                });
            }

            BuyingFacade.IncidentFollowUpBL()
                .Add(collation.IncidentFarmerID,
                collation.FollowUpDate,
                collation.FollowUpDescription,
                AppSetting.Username);

            return RedirectToAction("Index", new { incidentFarmerID = collation.IncidentFarmerID });
        }
    }
}