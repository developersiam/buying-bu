﻿using BusinessLayer;
using BUWebApp.Areas.ALP.Models;
using BUWebApp.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.ALP.Controllers
{
    [Authorize(Roles = "ALP")]
    public class IncidentTopicController : Controller
    {
        // GET: IncidentCategory
        [HttpGet]
        public ActionResult Index()
        {
            return View(BuyingFacade.IncidentTopicBL()
                .GetAll()
                .OrderBy(x => x.IncidentCategory.CategoryName)
                .ThenBy(x => x.TopicTitle)
                .ToList()
                .Select(x => new vm_IncidentTopic
                {
                    ID = x.ID,
                    TopicTitle = x.TopicTitle,
                    CategoryID = x.CategoryID,
                    CategoryName = x.IncidentCategory.CategoryName,
                    CreateBy = x.CreateBy,
                    CreateDate = x.CreateDate,
                    ModifiedBy = x.ModifiedBy,
                    ModifiedDate = x.ModifiedDate
                }));
        }

        //
        // GET: /IncidentCategory/Create
        [HttpGet]
        public ActionResult Create()
        {
            vm_IncidentTopic model = new vm_IncidentTopic();

            model.Categories = BuyingFacade.IncidentCategoryBL()
                .GetAll()
                .OrderBy(x => x.CategoryName)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.CategoryName
                });

            return View(model);
        }

        //
        // POST: /IncidentCategory/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(vm_IncidentTopic collation)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Please select category name.");
                collation.Categories = BuyingFacade.IncidentCategoryBL()
                .GetAll()
                .OrderBy(x => x.CategoryName)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.CategoryName
                });
                return View(collation);
            }

            BuyingFacade.IncidentTopicBL()
                .Add(collation.CategoryID, collation.TopicTitle, AppSetting.Username);

            return RedirectToAction("Index");
        }

        //
        // GET: /IncidentCategory/Edit/1
        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            var model = BuyingFacade.IncidentTopicBL().GetSingle(id);
            if (model == null)
                throw new ArgumentException("ไม่พบข้อมูลในระบบ");

            return View(new vm_IncidentTopic
            {
                ID = model.ID,
                TopicTitle = model.TopicTitle,
                CreateBy = model.CreateBy,
                CreateDate = model.CreateDate,
                ModifiedBy = model.ModifiedBy,
                ModifiedDate = model.ModifiedDate,
                CategoryID = model.CategoryID,
                CategoryName = model.IncidentCategory.CategoryName,
                Categories = BuyingFacade.IncidentCategoryBL()
                .GetAll()
                .OrderBy(x => x.CategoryName)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.CategoryName
                })
            });
        }

        //
        // POST: /IncidentCategory/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(vm_IncidentTopic collation)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Model state error.");
                return View(collation);
            }

            BuyingFacade.IncidentTopicBL()
                .Update(collation.ID,
                collation.TopicTitle,
                collation.CategoryID,
                AppSetting.Username);

            return RedirectToAction("Index");
        }

        //
        // GET: /IncidentCategory/Delete/1
        [HttpGet]
        public ActionResult Delete(Guid id)
        {
            BuyingFacade.IncidentTopicBL().Delete(id);
            return RedirectToAction("Index", "IncidentTopic");
        }


        //
        // GET: /IncidentCategory/Details/1
        [HttpGet]
        public ActionResult Details(Guid id)
        {
            var model = BuyingFacade.IncidentTopicBL().GetSingle(id);
            if (model == null)
                throw new ArgumentException("ไม่พบข้อมูลในระบบ");

            return View(new vm_IncidentTopic
            {
                ID = model.ID,
                TopicTitle = model.TopicTitle,
                CreateBy = model.CreateBy,
                CreateDate = model.CreateDate,
                ModifiedBy = model.ModifiedBy,
                ModifiedDate = model.ModifiedDate,
                CategoryID = model.CategoryID,
                CategoryName = model.IncidentCategory.CategoryName,
                Categories = BuyingFacade.IncidentCategoryBL()
                .GetAll()
                .OrderBy(x => x.CategoryName)
                .Select(x => new SelectListItem
                {
                    Value = x.ID.ToString(),
                    Text = x.CategoryName
                })
            });
        }
    }
}