﻿using BusinessLayer;
using BUWebApp.Areas.ALP.Models;
using BUWebApp.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.ALP.Controllers
{
    [Authorize(Roles = "ALP")]
    public class IncidentCategoryController : Controller
    {
        // GET: IncidentCategory
        [HttpGet]
        public ActionResult Index()
        {
            return View(BuyingFacade.IncidentCategoryBL()
                .GetAll()
                .OrderBy(x => x.CategoryName));
        }

        //
        // GET: /IncidentCategory/Create
        [HttpGet]
        public ActionResult Create()
        {
            return View(new vm_IncidentCategory());
        }

        //
        // POST: /IncidentCategory/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(vm_IncidentCategory collation)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Please input category name.");
                return View(collation);
            }

            BuyingFacade.IncidentCategoryBL()
                .Add(collation.CategoryName, AppSetting.Username);

            return RedirectToAction("Index");
        }

        //
        // GET: /IncidentCategory/Edit/1
        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            var model = BuyingFacade.IncidentCategoryBL().GetSingle(id);
            if (model == null)
                throw new ArgumentException("ไม่พบข้อมูลในระบบ");

            return View(new vm_IncidentCategory
            {
                ID = model.ID,
                CategoryName = model.CategoryName,
                CreateBy = model.CreateBy,
                CreateDate = model.CreateDate,
                ModifiedBy = model.ModifiedBy,
                ModifiedDate = model.ModifiedDate
            });
        }

        //
        // POST: /IncidentCategory/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(vm_IncidentCategory collation)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Please input category name.");
                return View(collation);
            }

            BuyingFacade.IncidentCategoryBL()
                .Update(collation.ID, collation.CategoryName, AppSetting.Username);

            return RedirectToAction("Index");
        }

        //
        // GET: /IncidentCategory/Delete/1
        [HttpGet]
        public ActionResult Delete(Guid id)
        {
            BuyingFacade.IncidentCategoryBL().Delete(id);
            return RedirectToAction("Index", "IncidentCategory");
        }
    }
}