﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Accounting.Controllers
{
    [Authorize(Roles = "Accounting,Supervisor")]
    public class HomeController : Controller
    {
        // GET: Accounting/Home
        public ActionResult Index()
        {
            return View();
        }
    }
}