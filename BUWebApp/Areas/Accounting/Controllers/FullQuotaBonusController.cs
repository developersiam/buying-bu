﻿using BusinessLayer;
using BusinessLayer.Helper;
using BusinessLayer.Helper.FarmerBonus;
using BusinessLayer.Model;
using BUWebApp.Areas.Supervisor.Models;
using BUWebApp.Helper;
using DomainModel;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.Identity;
using Microsoft.Reporting.WebForms;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IdentityModel.Protocols.WSTrust;
using System.IO;
using System.IO.Packaging;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace BUWebApp.Areas.Accounting.Controllers
{
    [Authorize(Roles = "Accounting,Supervisor")]
    public class FullQuotaBonusController : Controller
    {
        // GET: Accounting/FullQuotaBonus
        [HttpGet]
        public ActionResult Index()
        {
            ViewBag.FullQuotaBonusSummary = FullQuotaBonusHelper
                .GetFullQuotaBonusSummary(AppSetting.Crop.Crop1);

            return View();
        }

        [HttpGet]
        public ActionResult Details(string status)
        {
            ViewBag.FullQuotaBonusDetails = FullQuotaBonusHelper
                .GetFullQuotaBonusDetails(
                AppSetting.Crop.Crop1, status);
            ViewBag.Status = status;

            return View();
        }

        [HttpGet]
        public ActionResult Export(string status)
        {
            try
            {
                var list = BuyingFacade.FullQuotaBonusBL()
                        .GetByStatus(AppSetting.Crop.Crop1, status);

                var fileDownloadName = "FullQuotaBonus" + DateTime.Now + ".xlsx";
                var contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
                using (ExcelPackage pck = new ExcelPackage(fileDownloadName))
                {
                    ExcelWorksheet ws = pck.Workbook.Worksheets.Add("FullQuotaBonus");
                    ws.Cells["A1"].Value = "Crop";
                    ws.Cells["B1"].Value = "AreaCode";
                    ws.Cells["C1"].Value = "ExtensionAgentCode";
                    ws.Cells["D1"].Value = "CitizenID";
                    ws.Cells["E1"].Value = "FarmerCode";
                    ws.Cells["F1"].Value = "FirstName";
                    ws.Cells["G1"].Value = "LastName";
                    ws.Cells["H1"].Value = "BankAccount";
                    ws.Cells["I1"].Value = "BankName";
                    ws.Cells["J1"].Value = "BankBranch";
                    ws.Cells["K1"].Value = "BankBranchCode";
                    ws.Cells["L1"].Value = "QuotaRai";
                    ws.Cells["N1"].Value = "QuotaKg";
                    ws.Cells["M1"].Value = "SoldKg";
                    ws.Cells["O1"].Value = "FullQuotaStatus";
                    ws.Cells["P1"].Value = "BonusAmount";

                    ws.Cells["A2"].LoadFromCollection(list, true);
                    ws.Protection.IsProtected = false;

                    for (int i = 1; i <= 16; i++)
                        ws.Column(i).AutoFit();

                    pck.Save();

                    var fileStream = new MemoryStream();
                    pck.SaveAs(fileStream);
                    fileStream.Position = 0;

                    var fsr = new FileStreamResult(fileStream, contentType);
                    fsr.FileDownloadName = fileDownloadName;

                    return fsr;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult Operation(string status, string operation, params string[] farmerCodeList)
        {
            var crop = AppSetting.Crop.Crop1;
            switch (operation)
            {
                case "ConfirmTransfer":
                    BuyingFacade.FullQuotaBonusBL()
                        .TransferConfirm(crop, farmerCodeList);
                    break;
                case "ConfirmPrint":
                    //BuyingFacade.FullQuotaBonusBL()
                    //    .PrintConfirm(crop, farmerCodeList);
                    RedirectToAction("Print", new { farmerCodeList = farmerCodeList });
                    break;
                case "CancelTransferred":
                    BuyingFacade.FullQuotaBonusBL()
                        .CencelTransferred(crop, farmerCodeList);
                    break;
                case "CancelForPrint":
                    BuyingFacade.FullQuotaBonusBL()
                        .CencelPrinted(crop, farmerCodeList);
                    break;
                default:
                    break;
            }

            return RedirectToAction("Details", new { status = status });
        }


        [HttpGet]
        public ActionResult Print(params string[] farmerCodeList)
        {
            var farmers = BuyingFacade.FarmerBL().GetBySupplierCode("ABC/S");
            var farmerResult = farmers.Where(x => farmerCodeList.Contains(x.FarmerCode)).ToList();
            ReportDataSource personDataSource = new ReportDataSource();
            BuyingSystemDataSet.PersonDataTableDataTable personDataTable = new BuyingSystemDataSet.PersonDataTableDataTable();

            foreach (var item in farmerResult)
            {
                DataRow row = personDataTable.NewRow();
                row["CitizenID"] = item.CitizenID;
                row["Prefix"] = item.Person.Prefix;
                row["FirstName"] = item.Person.FirstName;
                row["LastName"] = item.Person.LastName;
                row["Village"] = item.Person.Village;
                row["HouseNumber"] = item.Person.HouseNumber;
                row["Tumbon"] = item.Person.Tumbon;
                row["Amphur"] = item.Person.Amphur;
                row["Province"] = item.Person.Province;
                personDataTable.Rows.Add(row);
            }

            personDataSource.Name = "Dataset1";
            personDataSource.Value = personDataTable;
            string reportPath = Server.MapPath("~/Areas/FarmerBonus/RDLC/FullQuotaBonusReceipt.rdlc");
            LocalReport report = new LocalReport();
            report.DataSources.Add(personDataSource);
            report.ReportPath = reportPath;

            string filePath = Path.GetTempFileName();
            //throw new Exception(filePath);

            Export(report, filePath);

            //Clost Report object.           
            report.Dispose();
            return File(filePath, "application/pdf");
        }

        public string Export(LocalReport report, string filePath)
        {
            string ack = "";
            try
            {
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;

                byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);
                using (FileStream stream = System.IO.File.OpenWrite(filePath))
                {
                    stream.Write(bytes, 0, bytes.Length);
                }
                return ack;
            }
            catch (Exception ex)
            {
                ack = ex.InnerException.Message;
                return ack;
            }
        }
    }
}