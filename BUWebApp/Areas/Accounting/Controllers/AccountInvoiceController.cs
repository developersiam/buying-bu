﻿using BusinessLayer;
using BusinessLayer.Helper;
using BusinessLayer.Model;
using BUWebApp.Areas.Supervisor.Models;
using DomainModel;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.Accounting.Controllers
{
    [Authorize(Roles = "Accounting")]
    public class AccountInvoiceController : Controller
    {
        [HttpGet]
        public ActionResult Search(string accountInvoiceNo)
        {
            ViewBag.AccountInvoiceNo = accountInvoiceNo;
            if (string.IsNullOrEmpty(accountInvoiceNo))
            {
                ViewBag.AccountInvoices = new List<AccountInvoice>();
                return View();
            }
            ViewBag.AccountInvoices = BuyingFacade.AccountInvoiceBL()
                .SearchByInvoiceNo(accountInvoiceNo);
            return View();
        }

        [HttpGet]
        public ActionResult Details(string accountInvoiceNo)
        {
            var accountInvoice = BuyingFacade.AccountInvoiceBL().GetSingle(accountInvoiceNo);
            var invoice = BuyingFacade.InvoiceBL().GetSingle(accountInvoice.RefInvoiceNo);

            ViewBag.AccountInvoice = accountInvoice;
            ViewBag.Invoice = invoice;
            ViewBag.AccountInvoiceDetails = InvoiceDetailsHelper
                .GetByAccountInvoiceNo(accountInvoiceNo);
            ViewBag.Registration = BuyingFacade.RegistrationBL()
                .GetSingle(invoice.Crop, invoice.FarmerCode);
            return View();
        }

        [HttpGet]
        public ActionResult Print(string accountInvoiceNo)
        {
            var accountInvoice = BuyingFacade.AccountInvoiceBL()
                .GetSingle(accountInvoiceNo);
            if (accountInvoice.IsPrinted == true)
                throw new Exception("Invoice No. " + accountInvoiceNo +
                    " นี้ถูกพิมพ์ไปแล้วเมื่อ " + accountInvoice.PrintDate +
                    " โดย " + accountInvoice.PrintBy +
                    " หากต้องการพิมพ์ใบเสร็จใหม่ โปรดติดต่อไปยังแผนกบัญชีเพื่อทำการปลดล็อคให้พิมพ์ใหม่");

            var invoiceDetails = BusinessLayer.Helper.InvoiceDetailsHelper.GetByAccountInvoiceNo(accountInvoiceNo);
            if (accountInvoice == null)
                throw new Exception("ไม่พบข้อมูล invoice นี้ในระบบ");

            if (accountInvoice.Invoice == null)
                throw new Exception("ไม่พบข้อมูล invoice นี้ในระบบ");

            ReportDataSource farmerProfileDataSource = new ReportDataSource();
            ReportDataSource cropInputsDetailsDataSource = new ReportDataSource();
            ReportDataSource currencyToThaiBathDataSource = new ReportDataSource();

            BuyingSystemDataSet.FarmerProfileDataTable farmerProfileDataTable = new BuyingSystemDataSet.FarmerProfileDataTable();
            BuyingSystemDataSet.sp_CropInputsInvoiceDetailsDataTable sp_CropInputsInvoiceDetailsDataTable = new BuyingSystemDataSet.sp_CropInputsInvoiceDetailsDataTable();
            BuyingSystemDataSet.CurrencyToThaiBathDataTable currencyToThaiBathDataTable = new BuyingSystemDataSet.CurrencyToThaiBathDataTable();

            BUWebApp.BuyingSystemDataSetTableAdapters.FarmerProfileTableAdapter farmerProfileTableAdapter = new BuyingSystemDataSetTableAdapters.FarmerProfileTableAdapter();
            BUWebApp.BuyingSystemDataSetTableAdapters.sp_CropInputsInvoiceDetailsTableAdapter sp_CropInputsInvoiceDetailsTableAdapter = new BuyingSystemDataSetTableAdapters.sp_CropInputsInvoiceDetailsTableAdapter();
            BUWebApp.BuyingSystemDataSetTableAdapters.CurrencyToThaiBathTableAdapter currencyToThaiBathTableAdapter = new BuyingSystemDataSetTableAdapters.CurrencyToThaiBathTableAdapter();

            farmerProfileTableAdapter.Fill(farmerProfileDataTable, accountInvoice.Invoice.Crop, accountInvoice.Invoice.FarmerCode);
            sp_CropInputsInvoiceDetailsTableAdapter.Fill(sp_CropInputsInvoiceDetailsDataTable, accountInvoiceNo);

            var currency = invoiceDetails.Sum(x => x.UnitPrice * x.Quantity);
            currencyToThaiBathTableAdapter.Fill(currencyToThaiBathDataTable, accountInvoiceNo);

            farmerProfileDataSource.Name = "FarmerProfile";
            farmerProfileDataSource.Value = farmerProfileDataTable;

            cropInputsDetailsDataSource.Name = "CropInputsInvoiceDetails";
            cropInputsDetailsDataSource.Value = sp_CropInputsInvoiceDetailsDataTable;

            currencyToThaiBathDataSource.Name = "CurrencyToThaiBath";
            currencyToThaiBathDataSource.Value = currencyToThaiBathDataTable;

            var farmer = BuyingFacade.FarmerBL().GetByFarmerCode(accountInvoice.Invoice.FarmerCode);
            string area = area = farmer.Supplier1.SupplierArea;
            string reportPath = "";

            if (area == "SUK" && accountInvoiceNo.Substring(1, 2) == "IV")
            {
                reportPath = Server.MapPath("~/RDLC/PIV-1.rdlc");
            }
            else
            {
                switch (accountInvoiceNo.Substring(1, 2))
                {
                    case "ON":
                        reportPath = Server.MapPath("~/RDLC/PON.rdlc");
                        break;
                    case "IN":
                        reportPath = Server.MapPath("~/RDLC/PIN.rdlc");
                        break;
                    case "OV":
                        reportPath = Server.MapPath("~/RDLC/POV.rdlc");
                        break;
                    case "IV":
                        reportPath = Server.MapPath("~/RDLC/PIV.rdlc");
                        break;
                    default:
                        break;
                }
            }

            LocalReport report = new LocalReport();

            report.DataSources.Add(farmerProfileDataSource);
            report.DataSources.Add(cropInputsDetailsDataSource);
            report.DataSources.Add(currencyToThaiBathDataSource);
            report.ReportPath = reportPath;

            string filePath = Path.GetTempFileName();
            Export(report, filePath);

            //Clost Report object.           
            report.Dispose();
            BuyingFacade.AccountInvoiceBL()
                .LockPrint(accountInvoice.InvoiceNo, HttpContext.User.Identity.Name);

            return File(filePath, "application/pdf");
        }

        public string Export(LocalReport report, string filePath)
        {
            string ack = "";
            try
            {
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;

                byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);
                using (FileStream stream = System.IO.File.OpenWrite(filePath))
                {
                    stream.Write(bytes, 0, bytes.Length);
                }
                return ack;
            }
            catch (Exception ex)
            {
                ack = ex.InnerException.Message;
                return ack;
            }
        }

        [HttpGet]
        public ActionResult UnlockPrint(string accountInvoiceNo)
        {
            BuyingFacade.AccountInvoiceBL().UnLockPrint(accountInvoiceNo);
            return RedirectToAction("Details", new { accountInvoiceNo = accountInvoiceNo });
        }
    }
}