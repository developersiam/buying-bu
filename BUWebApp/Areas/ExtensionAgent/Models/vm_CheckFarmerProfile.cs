﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.ExtensionAgent.Models
{
    public class vm_CheckFarmerProfile
    {
        [Required(ErrorMessage = "CitizenID required")]
        [RegularExpression("^[0-9]{13}$", ErrorMessage = "The value must be 13 digits numeric")]
        public string CitizenID { get; set; }
        public Person Person { get; set; }
        public List<Farmer> Farmers { get; set; }

    }
}