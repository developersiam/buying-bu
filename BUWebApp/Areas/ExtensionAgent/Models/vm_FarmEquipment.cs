﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DomainModel;
using System.ComponentModel.DataAnnotations;

namespace BUWebApp.Areas.ExtensionAgent.Models
{
    public class vm_FarmEquipment
    {
        [Required(ErrorMessage = "CitizenID required")]
        [StringLength(13, MinimumLength = 13)]
        public string CitizenID { get; set; }
        [Required(ErrorMessage = "MachineAndFarmEquipmentID required")]        
        public System.Guid MachineAndFarmEquipmentID { get; set; }
        public short Crop { get; set; }
        [Required(ErrorMessage = "Quantity required")]
        [Range(1, short.MaxValue, ErrorMessage = "The value must be greater than 0")]
        public short Quantity { get; set; }
        [Required(ErrorMessage = "LifeTime required")]
        [Range(0, short.MaxValue, ErrorMessage = "The minimum value is 0")]
        public short LifeTime { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public bool CanUse { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedUser { get; set; }

        public List<FarmerMachineAndFarmEquipment> FarmEquipmentList { get; set; }
        public DomainModel.Farmer Farmer { get; set; }
    }
}