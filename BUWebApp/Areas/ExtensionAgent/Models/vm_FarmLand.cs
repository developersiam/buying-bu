﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DomainModel;
using System.ComponentModel.DataAnnotations;

namespace BUWebApp.Areas.ExtensionAgent.Models
{
    public class vm_FarmLand
    {
        [Required(ErrorMessage = "โปรดระบุรหัสประจำตัวประชาชน")]
        [StringLength(13, MinimumLength = 13)]
        public string CitizenID { get; set; }
        [Required(ErrorMessage = "โปรดระบุตำบล")]
        public string SubDistrictCode { get; set; }
        [Range(1, short.MaxValue, ErrorMessage = "ขนาดพื้นที่จะต้องไม่เป็น 0")]
        [Required(ErrorMessage = "LandSize required")]
        public decimal LandSize { get; set; }
        [Required(ErrorMessage = "OwnerShip required")]
        public bool OwnerShip { get; set; }
        [Required(ErrorMessage = "UseForGrowingTobacco required")]
        public bool UseForGrowingTobacco { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedUser { get; set; }
        public short Crop { get; set; }
        public System.Guid FarmLandInfoID { get; set; }
        [Range(1, short.MaxValue, ErrorMessage = "หมายเลขของแปลงที่ดินจะต้องไม่เป็น 0")]
        public short FieldNo { get; set; }

        public List<FarmLandInformation> FarmLandList { get; set; }
        public DomainModel.Farmer Farmer { get; set; }
    }
}