﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DomainModel;
using System.ComponentModel.DataAnnotations;

namespace BUWebApp.Areas.ExtensionAgent.Models
{
    public class vm_PreRegistrationForm
    {
        public short Crop { get; set; }
        public string FarmerCode { get; set; }
        [Required(ErrorMessage = "Registration date required")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime RegistrationDate { get; set; }
        [Required(ErrorMessage = "Previous crop total rai required")]
        [Range(0, short.MaxValue, ErrorMessage = "The value must be start from 0")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "The value must be numeric")]
        public Nullable<short> PreviousCropTotalRai { get; set; }
        [Required(ErrorMessage = "Previous crop total volume required")]
        [Range(0, short.MaxValue, ErrorMessage = "The value must be start from 0")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "The value must be numeric")]
        public Nullable<int> PreviousCropTotalVolume { get; set; }
        public bool TTMQuotaStatus { get; set; }
        [Range(0, short.MaxValue, ErrorMessage = "The value must be start from 0")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "The value must be numeric")]
        public Nullable<int> TTMQuotaKg { get; set; }
        public string TTMQuotaStation { get; set; }
        [Required(ErrorMessage = "* Request quota required")]
        [Range(1, short.MaxValue, ErrorMessage = "The value must be greater than 0")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "The value must be numeric")]
        public Nullable<short> QuotaFromFarmerRequest { get; set; }
        public bool HasSuccessor { get; set; }
        public DomainModel.Farmer Farmer { get; set; }
        public List<CuringBarnInformation> CuringBarnInformationList { get; set; }
        public List<FarmLandInformation> FarmLandList { get; set; }
        public List<IrrigationWater> IrrigationWaterList { get; set; }
        public List<FarmerMachineAndFarmEquipment> FarmerAndMachineFarmEqipmentList { get; set; }
        public List<FarmerEstimateTransplantingPeriod> FarmerEstimateTransplantingPeriodList  { get; set; }
        public Nullable<int> BankID { get; set; }
        public string BookBank { get; set; }
        public string BankBranch { get; set; }
        public string BankBranchCode { get; set; }
        public string CitizenID { get; set; }
    }
}