﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BUWebApp.Areas.ExtensionAgent.Models
{
    public class vm_EstimateTransplantingPeriod
    {

        [Required(ErrorMessage = "Estimate Transplanting Period ID required")]
        public System.Guid EstimateTransplantingPeriodID { get; set; }
        [Required(ErrorMessage = "CitizenID required")]
        public string CitizenID { get; set; }
        public short Crop { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedUser { get; set; }

        public DomainModel.Farmer Farmer { get; set; }
        public List<FarmerEstimateTransplantingPeriod> EstimateTransplantingPeriodList { get; set; }
    }
}