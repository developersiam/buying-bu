﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DomainModel;
using System.ComponentModel.DataAnnotations;

namespace BUWebApp.Areas.ExtensionAgent.Models
{
    public class vm_IrrigationWater
    {
        public System.Guid FarmerIrrigationWaterID { get; set; }
        [Required(ErrorMessage = "CitizenID required")]
        public string CitizenID { get; set; }
        [Required(ErrorMessage = "Irrigation Water Type required")]
        public System.Guid IrrigationWaterTypeID { get; set; }
        public short Crop { get; set; }
        public Nullable<decimal> IrrigationWaterDeep { get; set; }
        public string RiverName { get; set; }
        public bool GroundWaterLicence { get; set; }
        public string GroundWaterLicenceRemark { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }

        public DomainModel.Farmer Farmer { get; set; }
        public List<IrrigationWater> IrrigationWaterList { get; set; }
    }
}