﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DomainModel;
using System.ComponentModel.DataAnnotations;

namespace BUWebApp.Areas.ExtensionAgent.Models
{
    public class vm_CuringBarn
    {
        public System.Guid BarnID { get; set; }
        [Required(ErrorMessage = "Width required")]
        [Range(1.0, double.MaxValue, ErrorMessage = "The value must be greater than 0")]
        public decimal Width { get; set; }
        [Required(ErrorMessage = "Length required")]
        [Range(1.0, double.MaxValue, ErrorMessage = "The value must be greater than 0")]
        public decimal Length { get; set; }
        [Required(ErrorMessage = "Tier required")]
        [Range(1, short.MaxValue, ErrorMessage = "The value must be greater than 0")]
        public short Tier { get; set; }
        [Required(ErrorMessage = "Age required")]
        [Range(1, short.MaxValue, ErrorMessage = "The value must be greater than 0")]
        public short Age { get; set; }
        [Required(ErrorMessage = "Pole material required")]
        public string PoleMaterial { get; set; }
        [Required(ErrorMessage = "Rack material required")]
        public string RackMaterial { get; set; }
        [Required(ErrorMessage = "CitizenID required")]
        public string CitizenID { get; set; }
        public bool IsOwner { get; set; }
        public short Crop { get; set; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedUser { get; set; }

        public List<CuringBarnInformation> CuringBarnList { get; set; }
        public DomainModel.Farmer Farmer { get; set; }
    }
}