﻿using System.Web.Mvc;

namespace BUWebApp.Areas.ExtensionAgent
{
    public class ExtensionAgentAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ExtensionAgent";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ExtensionAgent_default",
                "ExtensionAgent/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}