﻿using BusinessLayer;
using BUWebApp.Areas.ExtensionAgent.Models;
using BUWebApp.Helper;
using BUWebApp.Models;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.ExtensionAgent.Controllers
{
    [Authorize(Roles = "ExtensionAgent")]
    public class RegistrationController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            var _crop = AppSetting.Crop;
            ViewBag.SupplierCode = _supplierUser.SupplierCode;

            return View(RegistrationFarmerHelper
                .GetBySupplierCode(_crop.Crop1, _supplierUser.SupplierCode));
        }

        [HttpGet]
        public ActionResult Approve(short crop, string farmerCode)
        {
            var model = BuyingFacade.RegistrationBL().GetSingle(crop, farmerCode);
            if (model == null)
                throw new ArgumentNullException("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รายนี้! (farmer code : " + farmerCode + ")");

            BuyingFacade.RegistrationBL()
                .ApproveSupplierQuota(crop,
                farmerCode,
                Convert.ToInt16(model.QuotaFromFarmerRequest),
                HttpContext.User.Identity.Name);

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult CancelApprove(short crop, string farmerCode)
        {
            var model = BuyingFacade.RegistrationBL().GetSingle(crop, farmerCode);
            if (model == null)
                throw new ArgumentNullException("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รายนี้! (farmer code : " + farmerCode + ")");

            BuyingFacade.RegistrationBL()
                    .CancelSupplierQuota(crop, farmerCode, HttpContext.User.Identity.Name);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult ApproveAll()
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            var _crop = AppSetting.Crop;

            ///เอามาเฉพาะในรายที่ยังไม่ผ่านการอนุมัติโควต้าจาก STEC (ApproveFromSTECStatus = false)
            ///
            foreach (var item in BuyingFacade.RegistrationBL()
                .GetBySupplier(_crop.Crop1, _supplierUser.SupplierCode)
                .Where(reg => reg.ApproveFromSupplierStatus == false))
            {
                if (item.ApproveFromSupplierStatus == false)
                    BuyingFacade.RegistrationBL()
                        .ApproveSupplierQuota(item.Crop,
                        item.FarmerCode,
                        Convert.ToInt16(item.QuotaFromFarmerRequest),
                        AppSetting.Username);
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult CancelAll()
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            var _crop = AppSetting.Crop;

            ///เอามาเฉพาะในรายที่ยังไม่ผ่านการอนุมัติโควต้าจาก STEC (ApproveFromSTECStatus = false)
            ///
            foreach (var item in BuyingFacade.RegistrationBL()
                .GetBySupplier(_crop.Crop1, _supplierUser.SupplierCode)
                .Where(reg => reg.ApproveFromSTECStatus == false))
            {
                BuyingFacade.RegistrationBL()
                    .CancelSupplierQuota(item.Crop, item.FarmerCode, AppSetting.Username);
            }

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult RegistrationDetails(short crop, string farmerCode)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supervisor = AppSetting.SupervisorUser;
            var _crop = AppSetting.Crop;

            var model = RegistrationFarmerHelper.GetByFarmerCode(crop, farmerCode);
            if (model == null)
                throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รายนี้! (farmer code : " + farmerCode + ")");

            ViewBag.SupervisorArea = _supervisor.AreaCode;
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(short crop, string farmerCode)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            var _crop = AppSetting.Crop;

            var model = RegistrationFarmerHelper.GetByFarmerCode(crop, farmerCode);
            if (model == null)
                throw new ArgumentNullException("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รายนี้! (farmer code : " + farmerCode + ")");

            if (model.ApproveFromSTECStatus == true)
                throw new ArgumentException("ข้อมูลการลงทะเบียนของชาวไร่รายนี้ ได้รับการอนุมัติโดย STEC แล้ว ไม่สามารถแก้ไขข้อมูลการลงทะเบียนได้");

            ViewBag.SupplierCode = _supplierUser.SupplierCode;
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(vm_RegistrationFarmer collation)
        {
            BuyingFacade.RegistrationBL()
                .EditSupplierQuota(collation.Crop,
                collation.FarmerCode,
                Convert.ToInt16(collation.QuotaFromSupplierApprove),
                HttpContext.User.Identity.Name);

            return RedirectToAction("Index");
        }
    }
}