﻿using BusinessLayer;
using BUWebApp.Areas.ExtensionAgent.Models;
using BUWebApp.Helper;
using DomainModel;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace BUWebApp.Areas.ExtensionAgent.Controllers
{
    [Authorize(Roles = "ExtensionAgent")]
    public class FarmLandController : Controller
    {
        [HttpGet]
        public ActionResult Index(string id)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            var _crop = AppSetting.Crop;

            var farmerModel = BuyingFacade.FarmerBL().GetByCitizenID(id)
                .Single(f => f.Supplier == _supplierUser.SupplierCode);
            if (farmerModel == null)
                throw new ArgumentNullException("ไม่พบข้อมูลการลงทะเบียนเป็นสมาชิกกับตัวแทน " + _supplierUser.SupplierCode + " ในระบบ");

            ViewBag.SupplierCode = _supplierUser.SupplierCode;
            ViewData["province"] = BuyingFacade.ThailandGeoBL()
                .GetProvinces()
                .Where(x => x.ProvinceCode == "54" || x.ProvinceCode == "51")
                .Select(x => new SelectListItem
                {
                    Text = x.ProvinceNameThai.Replace(" ", ""),
                    Value = x.ProvinceCode
                });

            var list = BuyingFacade.FarmLandBL().GetByCitizenID(farmerModel.CitizenID, _crop.Crop1);

            return View(new vm_FarmLand
            {
                CitizenID = farmerModel.CitizenID,
                Crop = _crop.Crop1,
                FarmLandList = list,
                Farmer = farmerModel
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(vm_FarmLand collection)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            var _crop = AppSetting.Crop;

            if (!ModelState.IsValid)
            {
                collection.FarmLandList = BuyingFacade.FarmLandBL()
                    .GetByCitizenID(collection.CitizenID, _crop.Crop1);

                ViewBag.SupplierCode = _supplierUser.SupplierCode;
                ViewData["province"] = BuyingFacade.ThailandGeoBL()
                .GetProvinces().Select(x => new SelectListItem
                {
                    Text = x.ProvinceNameThai.Replace(" ", ""),
                    Value = x.ProvinceCode
                });
                return View("Index", collection);
            }

            BuyingFacade.FarmLandBL().Add(new FarmLandInformation
            {
                CitizenID = collection.CitizenID,
                Crop = _crop.Crop1,
                FieldNo = collection.FieldNo,
                LandSize = collection.LandSize,
                OwnerShip = collection.OwnerShip,
                UseForGrowingTobacco = collection.UseForGrowingTobacco,
                SubDistrictCode = collection.SubDistrictCode,
                ModifiedDate = DateTime.Now,
                ModifiedUser = AppSetting.Username
            });

            return RedirectToAction("Index", new { id = collection.CitizenID });
        }

        [HttpGet]
        public ActionResult Delete(Guid id, string citizenID)
        {
            BuyingFacade.FarmLandBL().Delete(id);
            return RedirectToAction("Index", new { id = citizenID });
        }
    }
}
