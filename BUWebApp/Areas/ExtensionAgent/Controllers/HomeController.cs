﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.ExtensionAgent.Controllers
{
    [Authorize(Roles = "ExtensionAgent")]
    public class HomeController : Controller
    {
        // GET: ExtensionAgent/Home
        public ActionResult Index()
        {
            return View();
        }
    }
}