﻿using BusinessLayer;
using BUWebApp.Areas.ExtensionAgent.Models;
using BUWebApp.Helper;
using BUWebApp.Models;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.ExtensionAgent.Controllers
{
    [Authorize(Roles = "ExtensionAgent")]
    public class FarmerController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CheckFarmerProfile(string citizenID)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            ViewBag.SupplierCode = _supplierUser.SupplierCode;
            var transaction = new List<vm_FarmerContractHistory>();
            ViewBag.RegisterAndSoldHistory = transaction;

            if (citizenID == null)
            {
                return View(new vm_CheckFarmerProfile
                {
                    CitizenID = null,
                    Person = new Person(),
                    Farmers = new List<Farmer>()
                });
            }

            var person = BuyingFacade.PersonBL().GetSingle(citizenID);
            if (person == null)
                return View(new vm_CheckFarmerProfile
                {
                    CitizenID = citizenID,
                    Person = new Person(),
                    Farmers = new List<Farmer>()
                });

            /// Populate farmer transaction history.
            /// 
            foreach (var item in person.Farmers)
                transaction.AddRange(RegistrationFarmerHelper
                    .GetContractHistoryByFarmer(item.FarmerCode));

            ViewBag.RegisterAndSoldHistory = transaction;
            return View(new vm_CheckFarmerProfile
            {
                CitizenID = citizenID,
                Person = person,
                Farmers = person.Farmers.ToList()
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CheckFarmerProfile(vm_CheckFarmerProfile collation)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            ViewBag.SupplierCode = _supplierUser.SupplierCode;

            collation.Person = null;
            collation.Farmers = new List<Farmer>();

            if (!ModelState.IsValid)
                return View(collation);

            if (!PersonHelper.CitizenIDIsValid(collation.CitizenID))
            {
                ModelState.AddModelError("", "รูปแบบรหัสประจำตัวประชาชนไม่ถูกต้อง");
                return View(collation);
            }

            /// ตรวจสอบประวัติชาวไร่ในตาราง Person
            /// 
            //var person = BuyingFacade.PersonBL().GetSingle(collation.CitizenID);
            //if (person == null)
            //{
            //    ModelState.AddModelError("", "ชาวไร่รายนี้ยังไม่ได้บันทึกข้อมูลประวัติตามบัตรประจำตัวประชาชนในระบบ");
            //    return View(collation);
            //}

            return RedirectToAction("CheckFarmerProfile", new { citizenID = collation.CitizenID });
        }

        [HttpGet]
        public ActionResult Create(string citizenID, string supplierCode)
        {
            if (!PersonHelper.CitizenIDIsValid(citizenID))
                throw new ArgumentException("รูปแบบรหัสประจำตัวประชาชนไม่ถูกต้อง");

            BuyingFacade.FarmerBL().Add(citizenID, supplierCode);
            return RedirectToAction("CheckFarmerProfile", new { citizenID = citizenID });
        }
    }
}