﻿using BusinessLayer;
using BUWebApp.Models;
using BUWebApp.Helper;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.ExtensionAgent.Controllers
{
    [Authorize(Roles = "ExtensionAgent")]
    public class PersonController : Controller
    {
        // GET: ExtensionAgent/Person
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create(string id)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            ViewBag.MaritalStatusList = BuyingFacade.MaritalStatusBL().GetAllMaitalStatus();
            ViewBag.Provinces = BuyingFacade.ThailandGeoBL().GetProvinces()
                .Where(x => x.ProvinceCode == "54" || x.ProvinceCode == "51")
                .ToList();

            return View(new vm_Person
            {
                CitizenID = id,
                BirthDate = DateTime.Now.Date,
                SpouseBirthDate = DateTime.Now.Date
            });
        }

        [HttpPost]
        public ActionResult Create(vm_Person collation)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;

            ViewBag.MaritalStatusList = BuyingFacade.MaritalStatusBL().GetAllMaitalStatus();
            ViewBag.Provinces = BuyingFacade.ThailandGeoBL().GetProvinces()
                .Where(x => x.ProvinceCode == "54" || x.ProvinceCode == "51")
                .ToList();

            BuyingFacade.PersonBL()
                .Add(new Person
                {
                    CitizenID = collation.CitizenID,
                    Prefix = collation.Prefix,
                    FirstName = collation.FirstName,
                    LastName = collation.LastName,
                    BirthDate = collation.BirthDate,
                    Village = collation.Village,
                    HouseNumber = collation.HouseNumber,
                    Tumbon = collation.Tumbon,
                    Amphur = collation.Amphur,
                    Province = collation.Province,
                    TelephoneNumber = collation.TelephoneNumber,
                    MaritalStatusCode = collation.MaritalStatusCode,
                    SpousePrefix = collation.SpousePrefix,
                    SpouseCitizenID = collation.SpouseCitizenID,
                    SpouseFirstName = collation.SpouseFirstName,
                    SpouseLastName = collation.SpouseLastName,
                    SpouseBirthDate = collation.SpouseBirthDate,

                    /// A new requirements from CY 2022.
                    /// 
                    StartAgriculture = collation.StartAgriculture,
                    StartGrowingTobacco = collation.StartGrowingTobacco,
                    YearsGrowingTobacco = collation.YearsGrowingTobacco,
                    YearsInAgriculture = collation.YearsInAgriculture,
                    ModifiedByUser = AppSetting.Username
                });

            return RedirectToAction("CheckFarmerProfile", "Farmer", new { citizenID = collation.CitizenID });
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;

            var person = BuyingFacade.PersonBL().GetSingle(id);
            if (person == null)
                throw new ArgumentException("ไม่พบข้อมูลชาวไร่ รหัสประจำตัวประชาชนหมายเลข " + id + " ในระบบ");

            ViewBag.MaritalStatusList = BuyingFacade.MaritalStatusBL().GetAllMaitalStatus();
            ViewBag.Provinces = BuyingFacade.ThailandGeoBL()
                .GetProvinces()
                .Where(x => x.ProvinceCode == "54" || x.ProvinceCode == "51" || x.ProvinceCode == "38")
                .Select(x => new SelectListItem
                {
                    Text = x.ProvinceNameThai.Replace(" ", ""),
                    Value = x.ProvinceNameThai.Replace(" ", ""),
                    Selected = x.ProvinceNameThai.Contains(person.Province) ? true : false
                });

            ViewBag.Amphurs = BuyingFacade.ThailandGeoBL()
                .GetDistrictByProvinceName(person.Province)
                .Select(x => new SelectListItem
                {
                    Text = x.DistrictNameThai.Replace(" ", ""),
                    Value = x.DistrictNameThai.Replace(" ", ""),
                    Selected = x.DistrictNameThai.Contains(person.Amphur) ? true : false
                }).ToList();

            ViewBag.Tumbons = BuyingFacade.ThailandGeoBL()
                .GetSubDistrictByDistrictName(person.Amphur)
                .Select(x => new SelectListItem
                {
                    Text = x.SubDistrictNameThai.Replace(" ", ""),
                    Value = x.SubDistrictNameThai.Replace(" ", ""),
                    Selected = x.SubDistrictNameThai.Contains(person.Tumbon) ? true : false
                }).ToList();
            
            var birthYear = person.BirthDate.Year;
            return View(new vm_Person
            {
                CitizenID = person.CitizenID,
                Prefix = person.Prefix,
                FirstName = person.FirstName,
                LastName = person.LastName,
                BirthDate = person.BirthDate,
                Village = person.Village,
                HouseNumber = person.HouseNumber,
                Tumbon = person.Tumbon,
                Amphur = person.Amphur,
                Province = person.Province,
                TelephoneNumber = person.TelephoneNumber,
                MaritalStatusCode = person.MaritalStatusCode,
                SpousePrefix = person.SpousePrefix,
                SpouseCitizenID = person.SpouseCitizenID,
                SpouseFirstName = person.SpouseFirstName,
                SpouseLastName = person.SpouseLastName,
                SpouseBirthDate = person.SpouseBirthDate,
                StartAgriculture = person.StartAgriculture == null ? Convert.ToInt16(birthYear +18) : Convert.ToInt16(person.StartAgriculture),
                StartGrowingTobacco = person.StartGrowingTobacco == null ? Convert.ToInt16(birthYear + 18) : Convert.ToInt16(person.StartGrowingTobacco),
                YearsGrowingTobacco = person.YearsGrowingTobacco,
                YearsInAgriculture = person.YearsInAgriculture
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(vm_Person collation)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;

            ViewBag.MaritalStatusList = BuyingFacade.MaritalStatusBL().GetAllMaitalStatus();
            ViewBag.Provinces = BuyingFacade.ThailandGeoBL()
                .GetProvinces()
                .Where(x => x.ProvinceCode == "54" || x.ProvinceCode == "51")
                .Select(x => new SelectListItem
                {
                    Text = x.ProvinceNameThai.Replace(" ", ""),
                    Value = x.ProvinceNameThai.Replace(" ", "")
                });

            ViewBag.Amphur = BuyingFacade.ThailandGeoBL()
                .GetDistrictByProvinceName(collation.Province)
                .Select(x => new SelectListItem
                {
                    Text = x.DistrictNameThai.Replace(" ", ""),
                    Value = x.DistrictNameThai.Replace(" ", ""),
                    Selected = x.DistrictNameThai.Contains(collation.Amphur) ? true : false
                });

            ViewBag.Tumbon = BuyingFacade.ThailandGeoBL()
                .GetSubDistrictByDistrictName(collation.Amphur)
                .Select(x => new SelectListItem
                {
                    Text = x.SubDistrictNameThai.Replace(" ", ""),
                    Value = x.SubDistrictNameThai.Replace(" ", ""),
                    Selected = x.SubDistrictNameThai.Contains(collation.Tumbon) ? true : false
                });

            if (!ModelState.IsValid)
                return View(collation);

            //Update farmer profile.
            BuyingFacade.PersonBL()
                .Update(new Person
                {
                    CitizenID = collation.CitizenID,
                    Prefix = collation.Prefix,
                    FirstName = collation.FirstName,
                    LastName = collation.LastName,
                    BirthDate = collation.BirthDate,
                    Village = collation.Village,
                    HouseNumber = collation.HouseNumber,
                    Tumbon = collation.Tumbon,
                    Amphur = collation.Amphur,
                    Province = collation.Province,
                    TelephoneNumber = collation.TelephoneNumber,
                    MaritalStatusCode = collation.MaritalStatusCode,
                    SpousePrefix = collation.SpousePrefix,
                    SpouseFirstName = collation.SpouseFirstName,
                    SpouseLastName = collation.SpouseLastName,
                    SpouseCitizenID = collation.SpouseCitizenID,
                    SpouseBirthDate = collation.SpouseBirthDate,

                    /// A new requirements from CY 2022.
                    /// 
                    StartAgriculture = collation.StartAgriculture,
                    StartGrowingTobacco = collation.StartGrowingTobacco,

                    YearsGrowingTobacco = collation.YearsGrowingTobacco,
                    YearsInAgriculture = collation.YearsInAgriculture,
                    ModifiedByUser = AppSetting.Username
                });

            return RedirectToAction("CheckFarmerProfile", "Farmer", new { citizenID = collation.CitizenID });
        }
    }
}