﻿using BusinessLayer;
using BUWebApp.Areas.ExtensionAgent.Models;
using BUWebApp.Helper;
using DomainModel;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace BUWebApp.Areas.ExtensionAgent.Controllers
{
    [Authorize(Roles = "ExtensionAgent")]
    public class EstimateTransplantingPeriodController : Controller
    {
        // GET: FarmerEstimateTransplantingPeriod
        public ActionResult Index(string id)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            var _crop = AppSetting.Crop;

            //ดึงข้อมูลชาวไร่ในระบบเพื่อทำการแสดงบนหน้าจอ
            var farmerModel = BuyingFacade.FarmerBL()
                .GetByCitizenID(id)
                .Single(f => f.Supplier == _supplierUser.SupplierCode);

            if (farmerModel == null)
                throw new ArgumentNullException("ไม่พบข้อมูลการลงทะเบียนเป็นสมาชิกกับตัวแทน "
                + _supplierUser.SupplierCode + " ในระบบ");

            ViewBag.EstimateTransplantingPeriodList = BuyingFacade.EstimateTransplantingPeriodBL().GetAll();
            ViewBag.SupplierCode = _supplierUser.SupplierCode;

            var list = BuyingFacade.FarmerEstimateTransplantingPeriodBL()
                    .GetByCitizenID(_crop.Crop1, id);

            return View(new vm_EstimateTransplantingPeriod
            {
                CitizenID = farmerModel.CitizenID,
                Crop = _crop.Crop1,
                Farmer = farmerModel,
                EstimateTransplantingPeriodList = list,
                ModifiedUser = AppSetting.Username,
                ModifiedDate = DateTime.Now
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(vm_EstimateTransplantingPeriod collection)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            var _crop = AppSetting.Crop;

            if (!ModelState.IsValid)
            {
                ViewBag.EstimateTransplantingPeriodList = BuyingFacade.EstimateTransplantingPeriodBL().GetAll();
                ViewBag.SupplierCode = _supplierUser.SupplierCode;
                return View("Index", collection);
            }

            BuyingFacade.FarmerEstimateTransplantingPeriodBL()
                .Add(new FarmerEstimateTransplantingPeriod
                {
                    CitizenID = collection.CitizenID,
                    Crop = _crop.Crop1,
                    EstimateTransplantingPeriodID = collection.EstimateTransplantingPeriodID,
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = AppSetting.Username
                });

            return RedirectToAction("Index", new { id = collection.CitizenID });
        }

        [HttpGet]
        public ActionResult Delete(Guid id, string citizenId)
        {
            BuyingFacade.FarmerEstimateTransplantingPeriodBL()
                .Delete(AppSetting.Crop.Crop1, citizenId, id);
            return RedirectToAction("Index", new { id = citizenId });
        }
    }
}