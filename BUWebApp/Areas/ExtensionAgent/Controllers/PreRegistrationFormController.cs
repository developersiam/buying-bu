﻿using BusinessLayer;
using BUWebApp.Areas.ExtensionAgent.Models;
using BUWebApp.Helper;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Areas.ExtensionAgent.Controllers
{
    [Authorize(Roles = "ExtensionAgent")]
    public class PreRegistrationFormController : Controller
    {
        // GET: ExtensionAgent/PreRegistrationForm
        [HttpGet]
        public ActionResult Index(string id)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            var _crop = AppSetting.Crop;

            //ตรวจสอบการเป็นสมาชิกชาวไร่กับซัพพลายเออร์ดังกล่าว
            var farmer = BuyingFacade.FarmerBL()
                .GetByCitizenID(id)
                .Single(f => f.Supplier == _supplierUser.SupplierCode);
            if (farmer == null)
                throw new ArgumentNullException("ชาวไร่รายนี้ยังไม่ได้สมัครสมาชิกกับ " + _supplierUser.SupplierCode +
                    " โปรดย้อนกลับไปยังหน้าตรวจสอบข้อมูลชาวไร่เพื่อลงทะเบียนเป็นสมาชิกกับตัวแทนก่อน");

            //หากยังไม่เคยมีการบันทึกข้อมูลใบสมัคร ให้เด้งไปยังหน้าบันทึกใบสมัคร
            var register = BuyingFacade.RegistrationBL()
                .GetSingle(_crop.Crop1, farmer.FarmerCode);
            if (register == null)
                return RedirectToAction("Create", new { id = id });

            vm_PreRegistrationForm model = new vm_PreRegistrationForm
            {
                Crop = _crop.Crop1,
                FarmerCode = farmer.FarmerCode,
                CitizenID = id,
                RegistrationDate = register.RegistrationDate,
                Farmer = farmer,

                TTMQuotaKg = register.TTMQuotaKg,
                TTMQuotaStation = register.TTMQuotaStation,
                TTMQuotaStatus = Convert.ToBoolean(register.TTMQuotaStatus),
                PreviousCropTotalRai = register.PreviousCropTotalRai,
                PreviousCropTotalVolume = register.PreviousCropTotalVolume,
                QuotaFromFarmerRequest = register.QuotaFromFarmerRequest,
                HasSuccessor = Convert.ToBoolean(register.HasSuccessor),

                CuringBarnInformationList = BuyingFacade.CuringBarnBL().GetByCitizenID(id, _crop.Crop1),
                FarmLandList = BuyingFacade.FarmLandBL().GetByCitizenID(id, _crop.Crop1),
                IrrigationWaterList = BuyingFacade.IIrrigationWaterBL().GetByCitizenID(id, _crop.Crop1),
                FarmerAndMachineFarmEqipmentList = BuyingFacade.FarmerMachineAndFarmEquipmentBL().GetByCitizenID(_crop.Crop1, id),
                FarmerEstimateTransplantingPeriodList = BuyingFacade.FarmerEstimateTransplantingPeriodBL().GetByCitizenID(_crop.Crop1, id),

                BankID = register.BankID,
                BookBank = register.BookBank,
                BankBranch = register.BankBranch,
                BankBranchCode = register.BankBranchCode
            };

            ViewBag.TTMStation = PreRegistrationFormHelper.GetTTMStation(_supplierUser.Supplier.SupplierArea);
            ViewBag.MaritalStatusList = BuyingFacade.MaritalStatusBL().GetAllMaitalStatus().ToList();
            ViewBag.BankList = BuyingFacade.BankBL().GetAll().OrderBy(b => b.BankName);
            ViewBag.SupplierCode = _supplierUser.SupplierCode;

            return View(model);
        }

        [HttpGet]
        public ActionResult Create(string id)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            var _crop = AppSetting.Crop;

            if (BuyingFacade.RegistrationBL()
                .IsValidMaxNumberOfContract(_crop.Crop1, id) == false)
                throw new ArgumentException("มีการสมัครเข้าร่วมโครงการครบตามจำนวนแล้ว (1 รายสมัครได้ 2 ตัวแทน)");

            //ตรวจสอบการเป็นสมาชิกชาวไร่กับซัพพลายเออร์ดังกล่าว
            var farmer = BuyingFacade.FarmerBL()
                .GetByCitizenID(id)
                .Single(f => f.Supplier == _supplierUser.SupplierCode);
            if (farmer == null)
                throw new ArgumentNullException("ชาวไร่รายนี้ยังไม่ได้สมัครสมาชิกกับ " + _supplierUser.SupplierCode +
                    " โปรดย้อนกลับไปยังหน้าตรวจสอบข้อมูลชาวไร่เพื่อลงทะเบียนเป็นสมาชิกกับตัวแทนก่อน");

            //หากมีการบันทึกข้อมูลแล้วก่อนหน้านี้ให้เด้งไปยังหน้าการอัพเดทข้อมูลแทน
            var register = BuyingFacade.RegistrationBL()
                .GetSingle(_crop.Crop1, farmer.FarmerCode);
            if (register != null)
                return RedirectToAction("Edit", new { id = id });

            vm_PreRegistrationForm model = new vm_PreRegistrationForm
            {
                Crop = _crop.Crop1,
                FarmerCode = farmer.FarmerCode,
                CitizenID = id,
                RegistrationDate = DateTime.Now,
                Farmer = farmer,

                TTMQuotaKg = null,
                TTMQuotaStation = null,
                TTMQuotaStatus = false,
                PreviousCropTotalRai = null,
                PreviousCropTotalVolume = null,
                QuotaFromFarmerRequest = null,
                HasSuccessor = true,

                CuringBarnInformationList = BuyingFacade.CuringBarnBL().GetByCitizenID(id, _crop.Crop1),
                FarmLandList = BuyingFacade.FarmLandBL().GetByCitizenID(id, _crop.Crop1),
                IrrigationWaterList = BuyingFacade.IIrrigationWaterBL().GetByCitizenID(id, _crop.Crop1),
                FarmerAndMachineFarmEqipmentList = BuyingFacade.FarmerMachineAndFarmEquipmentBL().GetByCitizenID(_crop.Crop1, id),
                FarmerEstimateTransplantingPeriodList = BuyingFacade.FarmerEstimateTransplantingPeriodBL().GetByCitizenID(_crop.Crop1, id),

                BankID = null,
                BookBank = null,
                BankBranch = null,
                BankBranchCode = null
            };

            ViewBag.TTMStation = PreRegistrationFormHelper.GetTTMStation(_supplierUser.Supplier.SupplierArea);
            ViewBag.MaritalStatusList = BuyingFacade.MaritalStatusBL().GetAllMaitalStatus().ToList();
            ViewBag.BankList = BuyingFacade.BankBL().GetAll().OrderBy(b => b.BankName);
            ViewBag.SupplierCode = _supplierUser.SupplierCode;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(vm_PreRegistrationForm collation)
        {
            if (BuyingFacade.RegistrationBL()
                .IsValidMaxNumberOfContract(collation.Crop, collation.CitizenID) == false)
                throw new ArgumentException("มีการสมัครเข้าร่วมโครงการครบตามจำนวนแล้ว (1 รายสมัครได้ 2 ตัวแทน)");

            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            var _crop = AppSetting.Crop;

            ViewBag.TTMStation = PreRegistrationFormHelper.GetTTMStation(_supplierUser.SupplierCode);
            ViewBag.MaritalStatusList = BuyingFacade.MaritalStatusBL().GetAllMaitalStatus().ToList();
            ViewBag.BankList = BuyingFacade.BankBL().GetAll().OrderBy(b => b.BankName);
            ViewBag.MaritalStatusList = BuyingFacade.MaritalStatusBL().GetAllMaitalStatus().ToList();
            ViewBag.BankList = BuyingFacade.BankBL().GetAll().OrderBy(b => b.BankName);
            ViewBag.SupplierCode = _supplierUser.SupplierCode;

            collation.CuringBarnInformationList = BuyingFacade.CuringBarnBL().GetByCitizenID(collation.CitizenID, _crop.Crop1);
            collation.FarmLandList = BuyingFacade.FarmLandBL().GetByCitizenID(collation.CitizenID, _crop.Crop1);
            collation.IrrigationWaterList = BuyingFacade.IIrrigationWaterBL().GetByCitizenID(collation.CitizenID, _crop.Crop1);
            collation.FarmerAndMachineFarmEqipmentList = BuyingFacade.FarmerMachineAndFarmEquipmentBL().GetByCitizenID(_crop.Crop1, collation.CitizenID);
            collation.FarmerEstimateTransplantingPeriodList = BuyingFacade.FarmerEstimateTransplantingPeriodBL().GetByCitizenID(_crop.Crop1, collation.CitizenID);

            if (!ModelState.IsValid)
                return View(collation);

            /*
                 *(2)ระบบจะต้องตรวจสอบการบันทึกข้อมูลได้แก่ ที่ดินเพื่อทำการเกษตร โรงบ่มใบยาสูบ 
                 * แหล่งน้ำเพื่อทำการปลูกยาสูบ เครื่องทุ่นแรงและอุปกรณ์ทางการเกษตร ช่วงเวลาที่คาดว่าจะปลูกต้นยาสูบ 
                 * โดยจะต้องมีการบันทึกข้อมูลในหัวข้อที่ได้กล่าวมาอย่างน้อยหัวข้อละ 1 รายการ 
                 * หากไม่บันทึกข้อมูลดังกล่าวจะถือว่าสามารถไปยังขั้นตอนการอนุมัติโควต้าโดยตัวแทนได้ 
                 */

            if (collation.FarmLandList.Count() < 1)
            {
                ModelState.AddModelError("", "โปรดบันทึกข้อมูลที่ดินเพื่อทำการเกษตรก่อน");
                return View(collation);
            }

            if (collation.CuringBarnInformationList.Count() < 1)
            {
                ModelState.AddModelError("", "โปรดบันทึกข้อมูลโรงบ่มใบยาสูบก่อน");
                return View(collation);
            }

            if (collation.IrrigationWaterList.Count() < 1)
            {
                ModelState.AddModelError("", "โปรดบันทึกข้อมูลแหล่งน้ำที่ใช้ทำการเกษตรก่อน");
                return View(collation);
            }

            if (collation.FarmerAndMachineFarmEqipmentList.Count() < 1)
            {
                ModelState.AddModelError("", "โปรดบันทึกข้อมูลเครื่องทุ่นแรงและอุปกรณ์ทางการเกษตรก่อน");
                return View(collation);
            }

            if (collation.FarmerEstimateTransplantingPeriodList.Count() < 1)
            {
                ModelState.AddModelError("", "โปรดบันทึกข้อมูลช่วงเวลาที่คาดว่าจะทำการเพาะปลูกก่อน");
                return View(collation);
            }

            //กรณีที่ชาวไร่ยังไม่ได้ลงทะเบียนเพาะปลูกใน default crop กล่าว ให้ทำการบันทึกข้อมูลการลงทะเบียนก่อน
            BuyingFacade.RegistrationBL()
                .Add(new RegistrationFarmer
                {
                    Crop = _crop.Crop1,
                    FarmerCode = collation.FarmerCode,
                    RegistrationDate = collation.RegistrationDate,
                    BankID = collation.BankID,
                    BookBank = collation.BookBank,
                    BankBranch = collation.BankBranch,
                    BankBranchCode = collation.BankBranchCode,
                    HasSuccessor = collation.HasSuccessor,
                    PreviousCropTotalRai = collation.PreviousCropTotalRai,
                    PreviousCropTotalVolume = collation.PreviousCropTotalVolume,
                    TTMQuotaStatus = collation.TTMQuotaStatus,
                    TTMQuotaKg = collation.TTMQuotaKg,
                    TTMQuotaStation = collation.TTMQuotaStation,
                    QuotaFromFarmerRequest = collation.QuotaFromFarmerRequest,
                    ModifiedByUser = AppSetting.Username
                });

            return RedirectToAction("Index", "PreRegistrationForm", new { id = collation.CitizenID });
        }

        [HttpGet]
        public ActionResult Edit(string id)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            var _crop = AppSetting.Crop;

            //ตรวจสอบการเป็นสมาชิกชาวไร่กับซัพพลายเออร์ดังกล่าว
            var farmer = BuyingFacade.FarmerBL()
                .GetByCitizenID(id)
                .Single(f => f.Supplier == _supplierUser.SupplierCode);
            if (farmer == null)
                throw new ArgumentNullException("ชาวไร่รายนี้ยังไม่ได้สมัครสมาชิกกับ " +
                    _supplierUser.SupplierCode +
                    " โปรดย้อนกลับไปยังหน้าตรวจสอบข้อมูลชาวไร่เพื่อลงทะเบียนเป็นสมาชิกกับตัวแทนก่อน");

            //หากมีการบันทึกข้อมูลแล้วก่อนหน้านี้ให้เด้งไปยังหน้าการอัพเดทข้อมูลแทน
            var register = BuyingFacade.RegistrationBL()
                .GetSingle(_crop.Crop1, farmer.FarmerCode);
            if (register == null)
                throw new ArgumentNullException("ไม่พบข้อมูลใบสมัครเข้าร่วมโครงการกับตัวแทน " +
                    _supplierUser.SupplierCode + " ในระบบ โปรดตรวจสอบอีกครั้ง");

            vm_PreRegistrationForm model = new vm_PreRegistrationForm
            {
                Crop = register.Crop,
                FarmerCode = register.FarmerCode,
                CitizenID = farmer.CitizenID,
                RegistrationDate = register.RegistrationDate,
                Farmer = farmer,

                TTMQuotaKg = register.TTMQuotaKg,
                TTMQuotaStation = register.TTMQuotaStation,
                TTMQuotaStatus = Convert.ToBoolean(register.TTMQuotaStatus),
                PreviousCropTotalRai = register.PreviousCropTotalRai,
                PreviousCropTotalVolume = register.PreviousCropTotalVolume,
                QuotaFromFarmerRequest = register.QuotaFromFarmerRequest,
                HasSuccessor = Convert.ToBoolean(register.HasSuccessor),

                CuringBarnInformationList = BuyingFacade.CuringBarnBL().GetByCitizenID(id, _crop.Crop1),
                FarmLandList = BuyingFacade.FarmLandBL().GetByCitizenID(id, _crop.Crop1),
                IrrigationWaterList = BuyingFacade.IIrrigationWaterBL().GetByCitizenID(id, _crop.Crop1),
                FarmerAndMachineFarmEqipmentList = BuyingFacade.FarmerMachineAndFarmEquipmentBL().GetByCitizenID(_crop.Crop1, id),
                FarmerEstimateTransplantingPeriodList = BuyingFacade.FarmerEstimateTransplantingPeriodBL().GetByCitizenID(_crop.Crop1, id),

                BankID = register.BankID,
                BookBank = register.BookBank,
                BankBranch = register.BankBranch,
                BankBranchCode = register.BankBranchCode
            };

            ViewBag.TTMStation = Helper.PreRegistrationFormHelper.GetTTMStation(_supplierUser.Supplier.SupplierArea);
            ViewBag.MaritalStatusList = BuyingFacade.MaritalStatusBL().GetAllMaitalStatus().ToList();
            ViewBag.BankList = BuyingFacade.BankBL().GetAll().OrderBy(b => b.BankName);
            ViewBag.SupplierCode = _supplierUser.SupplierCode;

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(vm_PreRegistrationForm collation)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            var _crop = AppSetting.Crop;

            ViewBag.TTMStation = PreRegistrationFormHelper.GetTTMStation(_supplierUser.SupplierCode);
            ViewBag.MaritalStatusList = BuyingFacade.MaritalStatusBL().GetAllMaitalStatus().ToList();
            ViewBag.BankList = BuyingFacade.BankBL().GetAll().OrderBy(b => b.BankName);
            ViewBag.MaritalStatusList = BuyingFacade.MaritalStatusBL().GetAllMaitalStatus().ToList();
            ViewBag.BankList = BuyingFacade.BankBL().GetAll().OrderBy(b => b.BankName);
            ViewBag.SupplierCode = _supplierUser.SupplierCode;

            collation.CuringBarnInformationList = BuyingFacade.CuringBarnBL().GetByCitizenID(collation.CitizenID, _crop.Crop1);
            collation.FarmLandList = BuyingFacade.FarmLandBL().GetByCitizenID(collation.CitizenID, _crop.Crop1);
            collation.IrrigationWaterList = BuyingFacade.IIrrigationWaterBL().GetByCitizenID(collation.CitizenID, _crop.Crop1);
            collation.FarmerAndMachineFarmEqipmentList = BuyingFacade.FarmerMachineAndFarmEquipmentBL().GetByCitizenID(_crop.Crop1, collation.CitizenID);
            collation.FarmerEstimateTransplantingPeriodList = BuyingFacade.FarmerEstimateTransplantingPeriodBL().GetByCitizenID(_crop.Crop1, collation.CitizenID);

            if (!ModelState.IsValid)
                return View(collation);

            //กรณีที่ชาวไร่ยังไม่ได้ลงทะเบียนเพาะปลูกใน default crop กล่าว ให้ทำการบันทึกข้อมูลการลงทะเบียนก่อน
            BuyingFacade.RegistrationBL()
                .UpdatePreRegisterProfile(new RegistrationFarmer
                {
                    Crop = _crop.Crop1,
                    FarmerCode = collation.FarmerCode,
                    RegistrationDate = collation.RegistrationDate,
                    BankID = collation.BankID,
                    BookBank = collation.BookBank,
                    BankBranch = collation.BankBranch,
                    BankBranchCode = collation.BankBranchCode,
                    HasSuccessor = collation.HasSuccessor,
                    PreviousCropTotalRai = collation.PreviousCropTotalRai,
                    PreviousCropTotalVolume = collation.PreviousCropTotalVolume,
                    TTMQuotaStatus = collation.TTMQuotaStatus,
                    TTMQuotaKg = collation.TTMQuotaKg,
                    TTMQuotaStation = collation.TTMQuotaStation,
                    QuotaFromFarmerRequest = collation.QuotaFromFarmerRequest,
                    ModifiedByUser = AppSetting.Username
                });

            return RedirectToAction("Edit", "PreRegistrationForm", new { id = collation.CitizenID });
        }
    }
}