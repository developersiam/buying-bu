﻿using BusinessLayer;
using BUWebApp.Helper;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace BUWebApp.Areas.ExtensionAgent.Controllers
{
    [Authorize(Roles = "ExtensionAgent")]
    public class MachineAndFarmEqipmentController : Controller
    {
        [HttpGet]
        public ActionResult Index(string id)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            var _crop = AppSetting.Crop;
            ViewBag.SupplierCode = _supplierUser.SupplierCode;

            var farmer = BuyingFacade.FarmerBL()
                    .GetByCitizenID(id)
                    .Single(f => f.Supplier == _supplierUser.SupplierCode);
            if (farmer == null)
                throw new ArgumentNullException("ไม่พบข้อมูลการลงทะเบียนเป็นสมาชิกกับตัวแทน " +
                    _supplierUser.SupplierCode + " ในระบบ");

            ViewBag.MachineFarmAndEqipments = BuyingFacade.MachineAndFarmEquipmentBL()
                .GetAll()
                .Where(x => x.IsActive == true)
                .OrderBy(x => x.MachineAndFarmEquipmentName);

            var list = BuyingFacade.FarmerMachineAndFarmEquipmentBL().GetByCitizenID(_crop.Crop1, id);
            return View(new Models.vm_FarmEquipment
            {
                CitizenID = farmer.CitizenID,
                Crop = _crop.Crop1,
                FarmEquipmentList = list,
                Farmer = farmer
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Models.vm_FarmEquipment collection)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            var _crop = AppSetting.Crop;

            ViewBag.SupplierCode = _supplierUser.SupplierCode;
            ViewBag.MachineFarmAndEqipments = BuyingFacade.MachineAndFarmEquipmentBL()
                .GetAll()
                .OrderBy(x => x.MachineAndFarmEquipmentName);

            if (!ModelState.IsValid)
            {
                collection.FarmEquipmentList = BuyingFacade.FarmerMachineAndFarmEquipmentBL()
                    .GetByCitizenID(_crop.Crop1, collection.CitizenID);
                return View("Index", collection);
            }

            BuyingFacade.FarmerMachineAndFarmEquipmentBL().Add(
                new FarmerMachineAndFarmEquipment
                {
                    CitizenID = collection.CitizenID,
                    Crop = _crop.Crop1,
                    MachineAndFarmEquipmentID = collection.MachineAndFarmEquipmentID,
                    LifeTime = collection.LifeTime,
                    Quantity = collection.Quantity,
                    CanUse = collection.CanUse,
                    ModifiedDate = DateTime.Now,
                    ModifiedUser = AppSetting.Username
                });

            return RedirectToAction("Index", new { id = collection.CitizenID });
        }

        [HttpGet]
        public ActionResult Delete(short crop, string citizenID, Guid machineID)
        {
            BuyingFacade.FarmerMachineAndFarmEquipmentBL().Delete(crop, citizenID, machineID);
            return RedirectToAction("Index", new { id = citizenID });

        }
    }
}
