﻿using BusinessLayer;
using BUWebApp.Areas.ExtensionAgent.Models;
using BUWebApp.Helper;
using DomainModel;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace BUWebApp.Areas.ExtensionAgent.Controllers
{
    [Authorize(Roles = "ExtensionAgent")]
    public class IrrigationWaterController : Controller
    {
        [HttpGet]
        public ActionResult Index(string id)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            var _crop = AppSetting.Crop;

            var farmerModel = BuyingFacade.FarmerBL()
                .GetByCitizenID(id)
                .Single(f => f.Supplier == _supplierUser.SupplierCode);

            if (farmerModel == null)
                throw new ArgumentNullException("ไม่พบข้อมูลการลงทะเบียนเป็นสมาชิกกับตัวแทน " +
                    _supplierUser.SupplierCode + " ในระบบ");

            ViewBag.IrrigationWaterTypeList = BuyingFacade.IIrrigationWaterBL().GetIrrigationWaterTypes();
            ViewBag.SupplierCode = _supplierUser.SupplierCode;

            return View(new vm_IrrigationWater
            {
                CitizenID = farmerModel.CitizenID,
                Crop = _crop.Crop1,
                IrrigationWaterList = BuyingFacade.IIrrigationWaterBL()
                .GetByCitizenID(id, _crop.Crop1),
                Farmer = farmerModel
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(vm_IrrigationWater collection)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            var _crop = AppSetting.Crop;

            if (!ModelState.IsValid)
            {
                ViewBag.IrrigationWaterTypeList = BuyingFacade.IIrrigationWaterBL().GetIrrigationWaterTypes();
                ViewBag.SupplierCode = _supplierUser.SupplierCode;
                //collection.IrrigationWaterList = BuyingFacade.IIrrigationWaterBL()
                //    .GetByCitizenID(collection.CitizenID, _crop.Crop1);
                return View("Index", collection);
            }

            BuyingFacade.IIrrigationWaterBL()
                .Add(new IrrigationWater
                {
                    CitizenID = collection.CitizenID,
                    Crop = _crop.Crop1,
                    IrrigationWaterTypeID = collection.IrrigationWaterTypeID,
                    IrrigationWaterDeep = collection.IrrigationWaterDeep,
                    RiverName = collection.RiverName,
                    GroundWaterLicence = collection.GroundWaterLicence,
                    GroundWaterLicenceRemark = collection.GroundWaterLicenceRemark,
                    ModifiedDate = DateTime.Now,
                    ModifiedBy = AppSetting.Username
                });

            return RedirectToAction("Index", new { id = collection.CitizenID });
        }

        [HttpGet]
        public ActionResult Delete(Guid id, string citizenID)
        {
            BuyingFacade.IIrrigationWaterBL().Delete(id);
            return RedirectToAction("Index", new { id = citizenID });
        }
    }
}
