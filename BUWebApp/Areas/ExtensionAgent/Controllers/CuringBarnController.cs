﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLayer;
using DomainModel;
using BUWebApp.Helper;
using BUWebApp.Areas.ExtensionAgent.Models;

namespace BUWebApp.Areas.ExtensionAgent.Controllers
{
    [Authorize(Roles = "ExtensionAgent")]
    public class CuringBarnController : Controller
    {
        [HttpGet]
        public ActionResult Index(string id)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            var _crop = AppSetting.Crop;

            var farmerModel = BuyingFacade.FarmerBL()
                .GetByCitizenID(id)
                .Single(f => f.Supplier == _supplierUser.SupplierCode);
            if (farmerModel == null)
                throw new ArgumentNullException("ไม่พบข้อมูลการลงทะเบียนเป็นสมาชิกกับตัวแทน " +
                    _supplierUser.SupplierCode + " ในระบบ");

            ViewBag.SupplierCode = _supplierUser.SupplierCode;
            return View(new vm_CuringBarn
            {
                CitizenID = farmerModel.CitizenID,
                Crop = _crop.Crop1,
                CuringBarnList = BuyingFacade.CuringBarnBL()
                .GetByCitizenID(id, _crop.Crop1),
                Farmer = farmerModel
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(vm_CuringBarn collection)
        {
            AppSetting.Username = HttpContext.User.Identity.Name;
            var _supplierUser = AppSetting.SupplierUser;
            var _crop = AppSetting.Crop;

            ViewBag.SupplierCode = _supplierUser.SupplierCode;

            if (!ModelState.IsValid)
            {
                collection.CuringBarnList = BuyingFacade.CuringBarnBL()
                    .GetByCitizenID(collection.CitizenID, _crop.Crop1);
                return View("Index", collection);
            }

            BuyingFacade.CuringBarnBL()
                .Add(new CuringBarnInformation
                {
                    CitizenID = collection.CitizenID,
                    Crop = _crop.Crop1,
                    BarnID = Guid.NewGuid(),
                    Length = collection.Length,
                    Width = collection.Width,
                    Tier = collection.Tier,
                    PoleMaterial = collection.PoleMaterial,
                    RackMaterial = collection.RackMaterial,
                    Age = collection.Age,
                    IsOwner = collection.IsOwner,
                    ModifiedUser = AppSetting.Username,
                    ModifiedDate = DateTime.Now
                });

            return RedirectToAction("Index", new { id = collection.CitizenID });
        }

        [HttpGet]
        public ActionResult Delete(Guid id, string citizenID)
        {
            BuyingFacade.CuringBarnBL().Delete(id);
            return RedirectToAction("Index", new { id = citizenID });
        }
    }
}
