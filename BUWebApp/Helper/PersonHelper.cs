﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Helper
{
    public static class PersonHelper
    {
        public static bool CitizenIDIsValid(string citizenID)
        {
            //ตรวจสอบว่าทุก ๆ ตัวอักษรเป็นตัวเลข
            if (citizenID.ToCharArray().All(c => char.IsNumber(c)) == false)
                return false;

            //ตรวจสอบว่าข้อมูลมีทั้งหมด 13 ตัวอักษร
            if (citizenID.Trim().Length != 13)
                return false;

            int sumValue = 0;
            for (int i = 0; i < citizenID.Length - 1; i++)
                sumValue += int.Parse(citizenID[i].ToString()) * (13 - i);

            int v = 11 - (sumValue % 11);
            //return PID[12].ToString() == v.ToString();

            if (v.ToString().Length > 1)
            {
                if (v.ToString()[1] == citizenID[12])
                    return true;
                else
                    return false;
            }
            else
            {
                if (v.ToString() == citizenID[12].ToString())
                    return true;
                else
                    return false;
            }
        }
    }
}