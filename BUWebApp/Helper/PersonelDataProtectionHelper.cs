﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BUWebApp.Helper
{
    public static class PersonelDataProtectionHelper
    {
        public static string CitizenIDProtection(string citizenID)
        {
            try
            {
                if (string.IsNullOrEmpty(citizenID))
                    throw new Exception("CitizenID cannot be empty.");

                var result = "";
                var digit = 1;
                foreach (var item in citizenID)
                {
                    if (digit <= 10)
                        result += item;
                    else
                        result = result + "x";
                    digit++;
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string PhoneNumberProtection(string phoneNumber)
        {
            try
            {
                if (string.IsNullOrEmpty(phoneNumber))
                    return "";

                var result = "";
                var digit = 1;
                foreach (var item in phoneNumber)
                {
                    if (digit <= 6)
                        result += item;
                    else
                        result = result + "x";
                    digit++;
                }
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}