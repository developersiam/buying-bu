﻿using BusinessLayer;
using BUWebApp.Models;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BUWebApp.Helper
{
    public static class RegistrationFarmerHelper
    {
        public static vm_RegistrationFarmer GetByFarmerCode(short crop, string farmerCode)
        {
            try
            {
                vm_RegistrationFarmer model = new vm_RegistrationFarmer();
                RegistrationFarmer item = new RegistrationFarmer();
                item = BuyingFacade.RegistrationBL().GetSingle(crop, farmerCode);

                if (item == null)
                    return null;

                model.Crop = item.Crop;
                model.FarmerCode = item.FarmerCode;
                model.GAPGroupCode = item.GAPGroupCode;
                model.RegistrationDate = item.RegistrationDate;
                model.ActiveStatus = item.ActiveStatus;
                model.GAPContractCode = item.GAPContractCode;
                model.BankID = item.BankID;
                model.BookBank = item.BookBank;
                model.BankBranch = item.BankBranch;
                model.BankBranchCode = item.BankBranchCode;
                model.UnRegisterReason = item.UnRegisterReason;
                model.PreviousCropTotalRai = item.PreviousCropTotalRai;
                model.PreviousCropTotalVolume = item.PreviousCropTotalVolume;
                model.TTMQuotaStatus = item.TTMQuotaStatus == null ? false : (bool)item.TTMQuotaStatus;
                model.TTMQuotaKg = item.TTMQuotaKg;
                model.TTMQuotaStation = item.TTMQuotaStation;
                model.QuotaFromFarmerRequest = item.QuotaFromFarmerRequest;
                model.QuotaFromSupplierApprove = item.QuotaFromSupplierApprove;
                model.QuotaFromSTECApprove = item.QuotaFromSTECApprove;
                model.QuotaFromSignContract = item.QuotaFromSignContract;
                model.ApproveFromSupplierStatus = item.ApproveFromSupplierStatus;
                model.ApproveFromSTECStatus = item.ApproveFromSTECStatus;
                model.SignContractDate = item.SignContractDate;
                model.LastModified = item.LastModified;
                model.ModifiedByUser = item.ModifiedByUser;
                model.Person = item.Farmer.Person;
                model.SupplierCode = item.Farmer.Supplier;

                return model;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public static List<vm_RegistrationFarmer> GetBySupplierCode(short crop, string supplierCode)
        {
            try
            {
                List<vm_RegistrationFarmer> registrationvm_Farmer = new List<vm_RegistrationFarmer>();
                foreach (RegistrationFarmer item in BuyingFacade.RegistrationBL().GetBySupplier(crop, supplierCode))
                {
                    registrationvm_Farmer.Add(new vm_RegistrationFarmer
                    {
                        Crop = item.Crop,
                        FarmerCode = item.FarmerCode,
                        GAPGroupCode = item.GAPGroupCode,
                        RegistrationDate = item.RegistrationDate,
                        ActiveStatus = item.ActiveStatus,
                        GAPContractCode = item.GAPContractCode,
                        BankID = item.BankID,
                        BookBank = item.BookBank,
                        BankBranch = item.BankBranch,
                        BankBranchCode = item.BankBranchCode,
                        UnRegisterReason = item.UnRegisterReason,
                        PreviousCropTotalRai = item.PreviousCropTotalRai,
                        PreviousCropTotalVolume = item.PreviousCropTotalVolume,
                        TTMQuotaStatus = item.TTMQuotaStatus == null ? false : (bool)item.TTMQuotaStatus,
                        TTMQuotaKg = item.TTMQuotaKg,
                        TTMQuotaStation = item.TTMQuotaStation,
                        QuotaFromFarmerRequest = item.QuotaFromFarmerRequest,
                        QuotaFromSupplierApprove = item.QuotaFromSupplierApprove,
                        QuotaFromSTECApprove = item.QuotaFromSTECApprove,
                        QuotaFromSignContract = item.QuotaFromSignContract,
                        ApproveFromSupplierStatus = item.ApproveFromSupplierStatus,
                        ApproveFromSTECStatus = item.ApproveFromSTECStatus,
                        SignContractDate = item.SignContractDate,
                        LastModified = item.LastModified,
                        ModifiedByUser = item.ModifiedByUser,
                        Person = item.Farmer.Person,
                        SupplierCode = item.Farmer.Supplier
                    });
                }
                return registrationvm_Farmer;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public static List<vm_RegistrationFarmer> GetByCitizenID(short crop, string citizenID)
        {
            return BuyingFacade.RegistrationBL().GetByCropAndCitizenID(crop, citizenID)
                .Select(item => new vm_RegistrationFarmer
                {
                    Crop = item.Crop,
                    FarmerCode = item.FarmerCode,
                    GAPGroupCode = item.GAPGroupCode,
                    RegistrationDate = item.RegistrationDate,
                    ActiveStatus = item.ActiveStatus,
                    GAPContractCode = item.GAPContractCode,
                    BankID = item.BankID,
                    BookBank = item.BookBank,
                    BankBranch = item.BankBranch,
                    BankBranchCode = item.BankBranchCode,
                    UnRegisterReason = item.UnRegisterReason,
                    PreviousCropTotalRai = item.PreviousCropTotalRai,
                    PreviousCropTotalVolume = item.PreviousCropTotalVolume,
                    TTMQuotaStatus = item.TTMQuotaStatus == null ? false : (bool)item.TTMQuotaStatus,
                    TTMQuotaKg = item.TTMQuotaKg,
                    TTMQuotaStation = item.TTMQuotaStation,
                    QuotaFromFarmerRequest = item.QuotaFromFarmerRequest,
                    QuotaFromSupplierApprove = item.QuotaFromSupplierApprove,
                    QuotaFromSTECApprove = item.QuotaFromSTECApprove,
                    QuotaFromSignContract = item.QuotaFromSignContract,
                    ApproveFromSupplierStatus = item.ApproveFromSupplierStatus,
                    ApproveFromSTECStatus = item.ApproveFromSTECStatus,
                    SignContractDate = item.SignContractDate,
                    LastModified = item.LastModified,
                    ModifiedByUser = item.ModifiedByUser,
                    Person = item.Farmer.Person,
                    SupplierCode = item.Farmer.Supplier
                }).ToList();
        }

        public static List<vm_FarmerContractHistory> GetContractHistoryByFarmer(string farmerCode)
        {
            var registerList = BuyingFacade.RegistrationBL().GetByFarmer(farmerCode);
            if (registerList.Count() <= 0)
                return new List<vm_FarmerContractHistory>();

            var extensionAgent = registerList.FirstOrDefault().Farmer.Supplier;
            if (extensionAgent == null)
                return new List<vm_FarmerContractHistory>();

            var kgPerRai = BuyingFacade.KilogramsPerRaiConfigurationBL().GetAll()
                .Where(x => x.SupplierCode == extensionAgent)
                .ToList();
            if (kgPerRai.Count() <= 0)
                return new List<vm_FarmerContractHistory>();

            var soldList = BuyingFacade.BuyingBL().GetSoldHistoryByFarmer(farmerCode);
            var join1 = (from a in registerList
                         join b in kgPerRai
                         on a.Crop equals b.Crop into j
                         from subJ in j.DefaultIfEmpty()
                         select new vm_FarmerContractHistory
                         {
                             Crop = a.Crop,
                             FarmerCode = a.FarmerCode,
                             SignContractDate = a.SignContractDate,
                             QuotaFromFarmerRequest = a.QuotaFromFarmerRequest,
                             QuotaFromSignContract = a.QuotaFromSignContract,
                             Fixed = Convert.ToInt16(a.QuotaFromSignContract * (subJ == null ? 0 : subJ.KilogramsPerRai))
                         })
                        .ToList();

            var join2 = (from a in join1
                         join b in soldList
                         on a.Crop equals b.Crop into j
                         from subj in j.DefaultIfEmpty()
                         select new vm_FarmerContractHistory
                         {
                             Crop = a.Crop,
                             FarmerCode = a.FarmerCode,
                             SignContractDate = a.SignContractDate,
                             QuotaFromFarmerRequest = a.QuotaFromFarmerRequest,
                             QuotaFromSignContract = a.QuotaFromSignContract,
                             Fixed = a.Fixed,
                             Sold = subj == null ? 0 : Convert.ToDecimal(subj.Weight)
                         })
                         .ToList();

            return join2;
        }

        public static List<vm_FarmerContractHistory> GetContractHistoryByCitizenID(string citizenID)
        {
            var registerList = BuyingFacade.RegistrationBL().GetByCitizenID(citizenID);
            if (registerList.Count() <= 0)
                return new List<vm_FarmerContractHistory>();

            var agentList = registerList
                .GroupBy(x => new { x.Crop, x.Farmer.Supplier })
                .Select(x => new
                {
                    Crop = x.Key.Crop,
                    SupplierCode = x.Key.Supplier
                })
                .ToList();

            var kgPerRaiList = BuyingFacade
                .KilogramsPerRaiConfigurationBL()
                .GetAll()
                .ToList();

            var agentKgPerRaiList = (from a in kgPerRaiList
                                     from b in agentList
                                     where b.Crop == a.Crop && b.SupplierCode == a.SupplierCode
                                     select new
                                     {
                                         Crop = a.Crop,
                                         SupplierCode = b.SupplierCode,
                                         Kg = a.KilogramsPerRai
                                     })
                                     .ToList();

            var soldList = BuyingFacade.BuyingBL().GetSoldHistoryByCitizenID(citizenID);
            var join1 = (from a in registerList
                         from b in agentKgPerRaiList
                         where a.Crop == b.Crop && a.Farmer.Supplier == b.SupplierCode
                         select new vm_FarmerContractHistory
                         {
                             Crop = a.Crop,
                             FarmerCode = a.FarmerCode,
                             SignContractDate = a.SignContractDate,
                             QuotaFromFarmerRequest = a.QuotaFromFarmerRequest,
                             QuotaFromSignContract = a.QuotaFromSignContract,
                             Fixed = (int)(a.QuotaFromSignContract <= 0 || a.QuotaFromSignContract == null ? 0 : a.QuotaFromSignContract * b.Kg)
                         })
                        .ToList();

            var join2 = (from a in join1
                         join b in soldList
                         on new { a.Crop, a.FarmerCode } equals new { b.Crop, b.FarmerCode } into j
                         from subj in j.DefaultIfEmpty()
                         select new vm_FarmerContractHistory
                         {
                             Crop = a.Crop,
                             FarmerCode = a.FarmerCode,
                             SignContractDate = a.SignContractDate,
                             QuotaFromFarmerRequest = a.QuotaFromFarmerRequest,
                             QuotaFromSignContract = a.QuotaFromSignContract,
                             Fixed = a.Fixed,
                             Sold = subj == null ? 0 : Convert.ToDecimal(subj.Weight)
                         })
                         .ToList();

            return join2;
        }

        public static bool IsNewFarmer(short currentCrop, string farmerCode)
        {
            try
            {
                RegistrationFarmer model = new RegistrationFarmer();
                model = BuyingFacade.RegistrationBL().GetSingle(currentCrop, farmerCode);

                if (model == null)
                    throw new ArgumentNullException("ไม่พบข้อมูลการลงทะเบียนชาวไร่รหัส " + farmerCode + " ในปี " + currentCrop);

                //ไม่เคยบันทึกข้อมูลการทำสัญญาใน table RegistrationFarmer มาก่อนเลย
                if (BuyingFacade.RegistrationBL().GetByFarmer(farmerCode).Where(r => r.Crop < currentCrop).Count() <= 0)
                    return true;//new farmer.
                else
                    return false;//old farmer.
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}