﻿using BusinessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BUWebApp.Helper
{
    public static class AppSetting
    {
        public static string Username { get { return HttpContext.Current.User.Identity.Name; } set { } }
        public static Crop Crop { get { return BuyingFacade.CropBL().GetDefault(); } }
        public static AspNetUser AspNetUser { get { return BuyingFacade.UserManagementBL().GetAspNetUserByUsername(Username); } }
        public static UserAccount UserAccount { get { return BuyingFacade.UserBL().GetByUsername(Username); } }
        public static SupplierUser SupplierUser { get { return BuyingFacade.UserManagementBL().GetSupplierUserByUserName(Username); } }
        public static SupervisorUser SupervisorUser { get { return BuyingFacade.UserManagementBL().GetSupervisorUserByUserName(Username); } }
    }
}