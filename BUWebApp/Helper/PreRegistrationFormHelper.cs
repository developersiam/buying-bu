﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Helper
{
    public static class PreRegistrationFormHelper
    {
        public static List<SelectListItem> GetTTMStation(string area)
        {
            var list = new List<SelectListItem>();
            if (area == "SUK")
            {
                list.Add(new SelectListItem
                {
                    Text = "สถานีใบยาสูบศรีสำโรง",
                    Value = "สถานีใบยาสูบศรีสำโรง"
                });
                list.Add(new SelectListItem
                {
                    Text = "สถานีใบยาสูบหนองยาว",
                    Value = "สถานีใบยาสูบหนองยาว"
                });
            }
            else
            {
                list.Add(new SelectListItem
                {
                    Text = "สถานีใบยาสูบนางั่ว",
                    Value = "สถานีใบยาสูบนางั่ว"
                });
                list.Add(new SelectListItem
                {
                    Text = "สถานีใบยาสูบท่าพล",
                    Value = "สถานีใบยาสูบท่าพล"
                });
            }
            return list;
        }
    }
}