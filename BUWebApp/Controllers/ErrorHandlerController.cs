﻿using BUWebApp.Models;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Controllers
{
    public class ErrorHandlerController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            vm_ErrorHandler error = (vm_ErrorHandler)TempData["Error"];
            return View(error);
        }

        public ActionResult NotFound()
        {
            return View();
        }

        public ActionResult ErrorInfo(string message)
        {
            ViewBag.message = message;
            return View();
        }
    }
}