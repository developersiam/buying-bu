﻿using BusinessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Controllers
{
    public class ThailandGEOsController : Controller
    {
        [HttpGet]
        public JsonResult GetProvinceWithValue()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list = BuyingFacade.ThailandGeoBL().GetProvinces()
                .Where(x => x.ProvinceCode == "54" || x.ProvinceCode == "51")
                .Select(x => new SelectListItem
                {
                    Text = x.ProvinceNameThai.Replace(" ", ""),
                    Value = x.ProvinceCode
                }).ToList();
            return Json(new SelectList(list, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetDistrictWithValue(string provinceCode)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list = BuyingFacade.ThailandGeoBL().GetDistrictByProvince(provinceCode)
                .Select(x => new SelectListItem
                {
                    Text = x.DistrictNameThai.Replace(" ", ""),
                    Value = x.DistrictCode
                }).ToList();
            return Json(new SelectList(list, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSubDistrictWithValue(string districtCode)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list = BuyingFacade.ThailandGeoBL().GetSubDistrictByDistrict(districtCode)
                .Select(x => new SelectListItem
                {
                    Text = x.SubDistrictNameThai.Replace(" ", ""),
                    Value = x.SubDistrictCode
                }).ToList();
            return Json(new SelectList(list, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetProvinces()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list = BuyingFacade.ThailandGeoBL().GetProvinces()
                .Where(x => x.ProvinceCode == "54" || x.ProvinceCode == "51")
                .Select(x => new SelectListItem
                {
                    Text = x.ProvinceNameThai.Replace(" ", ""),
                    Value = x.ProvinceNameThai.Replace(" ", "")
                }).ToList();
            return Json(new SelectList(list, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetDistricts(string provinceName)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list = BuyingFacade.ThailandGeoBL()
                .GetDistrictByProvinceName(provinceName)
                .Select(x => new SelectListItem
                {
                    Text = x.DistrictNameThai.Replace(" ", ""),
                    Value = x.DistrictNameThai.Replace(" ", "")
                }).ToList();
            return Json(new SelectList(list, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetSubDistricts(string districtName)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            list = BuyingFacade.ThailandGeoBL()
                .GetSubDistrictByDistrictName(districtName).Select(x => new SelectListItem
                {
                    Text = x.SubDistrictNameThai.Replace(" ", ""),
                    Value = x.SubDistrictNameThai.Replace(" ", "")
                }).ToList();
            return Json(new SelectList(list, "Value", "Text"), JsonRequestBehavior.AllowGet);
        }
    }
}