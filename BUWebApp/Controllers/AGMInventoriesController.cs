﻿using BusinessLayer;
using BusinessLayer.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BUWebApp.Controllers
{
    public class AGMInventoriesController : Controller
    {
        [HttpGet]
        public JsonResult Items(int categoryid, int brandid)
        {
            try
            {
                List<SelectListItem> list = new List<SelectListItem>();
                list = AGMInventorySystemBL.BLServices.material_itemBL()
                    .GetByCategory(categoryid)
                    .Where(x => x.brandid == brandid)
                    .OrderBy(x => x.item_name)
                    .Select(x => new SelectListItem
                    {
                        Text = x.item_name,
                        Value = x.itemid.ToString()
                    }).ToList();

                return Json(new SelectList(list, "Value", "Text"), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public JsonResult ItemsByCategory(int categoryid)
        {
            try
            {
                List<SelectListItem> list = new List<SelectListItem>();
                list = AGMInventorySystemBL.BLServices.material_itemBL()
                    .GetByCategory(categoryid)
                    .OrderBy(x => x.item_name)
                    .Select(x => new SelectListItem
                    {
                        Text = x.item_name + " (" + x.material_unit.unit_name + ")",
                        Value = x.itemid.ToString()
                    }).ToList();

                return Json(new SelectList(list, "Value", "Text"), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public JsonResult Brands(int categoryid)
        {
            try
            {
                List<SelectListItem> list = new List<SelectListItem>();
                list = AGMInventorySystemBL.BLServices
                    .material_brandBL()
                    .GetAll()
                    .Where(x => x.categoryid == categoryid)
                    .OrderBy(x => x.brand_name)
                    .Select(x => new SelectListItem
                    {
                        Text = x.brand_name,
                        Value = x.brandid.ToString()
                    }).ToList();

                return Json(new SelectList(list, "Value", "Text"), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public JsonResult Categories()
        {
            try
            {
                List<SelectListItem> list = new List<SelectListItem>();
                list = AGMInventorySystemBL.BLServices.material_categoryBL()
                    .GetAll()
                    .OrderBy(x => x.category_name)
                    .Select(x => new SelectListItem
                    {
                        Text = x.category_name,
                        Value = x.categoryid.ToString()
                    }).ToList();
                return Json(new SelectList(list, "Value", "Text"), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public JsonResult CategoriesFromItemCondition(short crop, string supplierCode)
        {
            try
            {
                List<SelectListItem> list = new List<SelectListItem>();
                list = BusinessLayer.Helper.CropInputsItemHelper
                    .GetBySupplierCode(crop, supplierCode)
                    .GroupBy(x => new { x.Category })
                    .OrderBy(x => x.Key.Category)
                    .Select(x => new SelectListItem
                    {
                        Text = x.Key.Category,
                        Value = x.Key.Category.ToString()
                    }).ToList();
                return Json(new SelectList(list, "Value", "Text"), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public JsonResult ItemsFromItemCondition(string category, string supplierCode, string filter)
        {
            try
            {
                List<SelectListItem> resultList = new List<SelectListItem>();

                var list = BusinessLayer.Helper.CropInputsItemHelper
                    .GetBySupplierCode(BuyingFacade.CropBL().GetDefault().Crop1, supplierCode)
                    .Where(x => x.Category == category && x.IsNotToInvoice == false)
                    .ToList();

                if (filter != "all")
                    resultList = list.Where(x => x.IsVat == (filter == "IncludeVAT" ? true : false))
                    .OrderBy(x => x.ItemName)
                    .Select(x => new SelectListItem
                    {
                        Text = x.ItemName + " (" + x.Brand + ")",
                        Value = x.ConditionID.ToString()
                    }).ToList();
                else
                    resultList = list
                    .OrderBy(x => x.ItemName)
                    .Select(x => new SelectListItem
                    {
                        Text = x.ItemName + " (" + x.Brand + ")",
                        Value = x.ConditionID.ToString()
                    }).ToList();

                return Json(new SelectList(resultList, "Value", "Text"), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public JsonResult ItemsWithCondition(string category, string supplierCode)
        {
            try
            {
                var crop = Helper.AppSetting.Crop.Crop1;
                var list = CropInputsItemHelper
                    .GetBySupplierCode(crop, supplierCode)
                    .Where(x => x.Category == category && x.IsNotToInvoice == false)
                    .OrderBy(x => x.ItemName)
                    .ToList();

                return Json(list, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public JsonResult NotToInvoiceItemsFromItemCondition(string category, string supplierCode)
        {
            try
            {
                List<SelectListItem> resultList = new List<SelectListItem>();
                resultList = BusinessLayer.Helper.CropInputsItemHelper
                    .GetBySupplierCode(BuyingFacade.CropBL().GetDefault().Crop1, supplierCode)
                    .Where(x => x.Category == category && x.IsNotToInvoice == true)
                    .OrderBy(x => x.ItemName)
                    .Select(x => new SelectListItem
                    {
                        Text = x.ItemName + " (" + x.Brand + ")",
                        Value = x.ConditionID.ToString()
                    }).ToList();

                return Json(new SelectList(resultList, "Value", "Text"), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public JsonResult ItemSingleFromItemCondition(Guid conditionID)
        {
            try
            {
                return Json(BusinessLayer.Helper.CropInputsItemHelper.GetSingle(conditionID), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public JsonResult Package(short crop, string supplierCode)
        {
            try
            {
                List<SelectListItem> list = new List<SelectListItem>();
                list = BuyingFacade.CropInputsPackageBL()
                    .GetBySupplier(crop, supplierCode)
                    .OrderBy(x => x.PackageCode)
                    .Select(x => new SelectListItem
                    {
                        Text = x.PackageCode + " (" + x.Description + ")",
                        Value = x.PackageCode
                    }).ToList();

                return Json(new SelectList(list, "Value", "Text"), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpGet]
        public JsonResult PackageDetails(string packageCode)
        {
            try
            {
                var list1 = BusinessLayer.Helper.CropInputsItemHelper
                     .GetByPackage(packageCode)
                     .OrderBy(x => x.Category)
                     .ThenBy(x => x.ItemName)
                     .ThenBy(x => x.Brand)
                     .ToList();

                return Json(list1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}