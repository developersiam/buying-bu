//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Buying
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Buying()
        {
            this.NTRMInspections = new HashSet<NTRMInspection>();
        }
    
        public short Crop { get; set; }
        public string BuyingStationCode { get; set; }
        public string FarmerCode { get; set; }
        public short BuyingDocumentNumber { get; set; }
        public string BaleBarcode { get; set; }
        public string ProjectType { get; set; }
        public bool IsExtraQuota { get; set; }
        public bool IsNTRMInspection { get; set; }
        public System.DateTime RegisterBarcodeDate { get; set; }
        public string RegisterBarcodeUser { get; set; }
        public string Grade { get; set; }
        public Nullable<short> PricingCrop { get; set; }
        public Nullable<byte> PricingNumber { get; set; }
        public string RejectReason { get; set; }
        public Nullable<System.DateTime> GradeDate { get; set; }
        public string GradeUser { get; set; }
        public string ChangeGradeUser { get; set; }
        public string BuyerCode { get; set; }
        public Nullable<decimal> Weight { get; set; }
        public Nullable<System.DateTime> WeightDate { get; set; }
        public string WeightUser { get; set; }
        public string TransportationDocumentCode { get; set; }
        public Nullable<System.DateTime> LoadBaleToTruckDate { get; set; }
        public string LoadBaleToTruckUser { get; set; }
        public string MovingTransportationFrom { get; set; }
        public string MovingTransportationReason { get; set; }
        public Nullable<System.DateTime> MovingTransportationDate { get; set; }
        public string MovingBy { get; set; }
        public Nullable<decimal> CPAResult { get; set; }
        public Nullable<System.DateTime> CPAResultDate { get; set; }
        public string CPAResultUser { get; set; }
        public Nullable<decimal> MoistureResult { get; set; }
        public Nullable<System.DateTime> MoistureResultDate { get; set; }
        public string MoistureResultUser { get; set; }
    
        public virtual Buyer Buyer { get; set; }
        public virtual BuyingDocument BuyingDocument { get; set; }
        public virtual BuyingGrade BuyingGrade { get; set; }
        public virtual TransportationDocument TransportationDocument { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<NTRMInspection> NTRMInspections { get; set; }
        public virtual NTRMSample NTRMSample { get; set; }
    }
}
