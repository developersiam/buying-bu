//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class ChemicalDistribution
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ChemicalDistribution()
        {
            this.ReturnCPAContainers = new HashSet<ReturnCPAContainer>();
        }
    
        public short Crop { get; set; }
        public string FarmerCode { get; set; }
        public System.Guid ChemicalItemID { get; set; }
        public System.DateTime DistributionDate { get; set; }
        public string DistributionUser { get; set; }
        public short Quantity { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedUser { get; set; }
    
        public virtual ChemicalItem ChemicalItem { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReturnCPAContainer> ReturnCPAContainers { get; set; }
        public virtual RegistrationFarmer RegistrationFarmer { get; set; }
    }
}
