//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class HistoryBackBuying
    {
        public int HistBackID { get; set; }
        public string BaleBarcode { get; set; }
        public string BuyingStationCode { get; set; }
        public string FarmerCode { get; set; }
        public string Grade { get; set; }
        public string GradeBy { get; set; }
        public Nullable<System.DateTime> GradeDate { get; set; }
        public Nullable<int> PRICINGNUMBER { get; set; }
        public string RejectReason { get; set; }
        public Nullable<decimal> Weight { get; set; }
        public Nullable<System.DateTime> WeightDate { get; set; }
        public string WeightBy { get; set; }
        public string TransportationDocumentCode { get; set; }
        public Nullable<int> PRICINGCROP { get; set; }
        public Nullable<System.DateTime> RegisterBarcodeDate { get; set; }
        public string Buyercode { get; set; }
        public Nullable<System.DateTime> BackDate { get; set; }
        public string CauseBy { get; set; }
        public string modifiedBy { get; set; }
        public Nullable<System.DateTime> modifiedDate { get; set; }
    }
}
