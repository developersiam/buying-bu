//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class ReturnFertilizerAndChemical
    {
        public short Crop { get; set; }
        public string FarmerCode { get; set; }
        public short ReturnQuantity { get; set; }
        public System.DateTime ReturnDate { get; set; }
        public System.Guid FertilizerAndChemicalID { get; set; }
        public string ReturnUser { get; set; }
        public System.Guid ReturnFertilierAndChemicalID { get; set; }
        public bool IsSurplus { get; set; }
    
        public virtual ReceiveFertilizerAndChemical ReceiveFertilizerAndChemical { get; set; }
    }
}
