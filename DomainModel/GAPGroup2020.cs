//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class GAPGroup2020
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GAPGroup2020()
        {
            this.GAPGroupMember2020 = new HashSet<GAPGroupMember2020>();
        }
    
        public short Crop { get; set; }
        public string LeaderCitizenID { get; set; }
        public string AreaCode { get; set; }
        public string GAPGroupCode { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
    
        public virtual Area Area { get; set; }
        public virtual Crop Crop1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GAPGroupMember2020> GAPGroupMember2020 { get; set; }
        public virtual Person Person { get; set; }
    }
}
