//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class GMSFarmerInfoCollectionCriteria
    {
        public short Crop { get; set; }
        public string CitizenID { get; set; }
        public bool CropInputQty { get; set; }
        public bool GPSHouseLatLong { get; set; }
        public bool GPSFieldLatLong { get; set; }
        public bool EstOfTransplanting { get; set; }
        public bool EstOfTopping { get; set; }
        public bool TransplantingCompletionDate { get; set; }
    }
}
