//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DomainModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class IrrigationWater
    {
        public System.Guid FarmerIrrigationWaterID { get; set; }
        public string CitizenID { get; set; }
        public short Crop { get; set; }
        public System.Guid IrrigationWaterTypeID { get; set; }
        public Nullable<decimal> IrrigationWaterDeep { get; set; }
        public string RiverName { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<bool> GroundWaterLicence { get; set; }
        public string GroundWaterLicenceRemark { get; set; }
    
        public virtual Crop Crop1 { get; set; }
        public virtual IrrigationWaterType IrrigationWaterType { get; set; }
        public virtual Person Person { get; set; }
    }
}
