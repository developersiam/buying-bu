﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUWinApp.Model.BuyingVoucher
{
    public class m_DebtorInfo
    {
        public decimal TotalDebt { get; set; }
        public decimal TotalPayment { get; set; }
        public decimal DebtBalance { get; set; }
        public decimal CurrentPayment { get; set; }
        public string CreditNoteStatus { get; set; }
    }
}
