﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUWinApp.Model.BuyingVoucher
{
    public class m_DocumentInfo
    {
        public string Station { get; set; }
        public string DocumentCode { get; set; }
        public string DocumentOwner { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime PrintDate { get; set; }
        public DateTime FinishBuyingDate { get; set; }
        public int TotalBale { get; set; }
        public int TotalReject { get; set; }
        public decimal TotalWeight { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal NetPrice { get; set; }
        public decimal AvgPrice { get; set; }
        public decimal AccumulativeAvgPrice { get; set; }
        public decimal AccumulativeHighGradeAvgPrice { get; set; }
        public decimal AccumulativeLowGradeAvgPrice { get; set; }
    }
}
