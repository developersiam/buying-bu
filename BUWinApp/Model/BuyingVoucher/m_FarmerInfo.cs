﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUWinApp.Model.BuyingVoucher
{
    public class m_FarmerInfo
    {
        public string FarmerCode { get; set; }
        public string CitizenID { get; set; }
        public string FarmerName { get; set; }
        public string Address { get; set; }
        public string GAPGroupCode { get; set; }
        public string RegisterStatus { get; set; }
        public string BankCode { get; set; }
        public string BankBranch { get; set; }
    }
}
