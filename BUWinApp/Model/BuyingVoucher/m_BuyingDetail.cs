﻿using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUWinApp.Model.BuyingVoucher
{
    public class m_BuyingDetail : Buying
    {
        public string Quality { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal Price { get; set; }
    }
}
