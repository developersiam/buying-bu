﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUWinApp.Model.BuyingVoucher
{
    public class m_FarmerQuota
    {
        public string ProjectType { get; set; }
        public decimal Quota { get; set; }
        public decimal Extra { get; set; }
        public decimal Sold { get; set; }
    }
}
