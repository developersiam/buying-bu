﻿using BusinessLayer;
using BUWinApp.Forms.BuyingStaff;
using BUWinApp.Helper;
using BUWinApp.View.BuyingStaff;
using BUWinApp.View.Hessian;
using BUWinApp.View.Receiving;
using BUWinApp.ViewModel.BuyingStaff;
using BUWinApp.ViewModel.Receiving;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BUWinApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            //user_setting.User = BuyingFacade.UserBL().GetByUsername("eakkaluck");
            //MainFrame.NavigationService.Navigate(new Confirm());

            //var window = new Buying();
            //var vm = new vm_Buying();

            //window.DataContext = vm;
            //window.ShowDialog();
            //var window = new View.BuyingStaff.DebtAndReceipt.Voucher();
            //var vm = new ViewModel.BuyingStaff.DebtAndReceipt.vm_Voucher();
            //window.DataContext = vm;
            //window.ShowDialog();
        }
        private void UIElement_OnPreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void MenuPopupButton_OnClick(object sender, RoutedEventArgs e)
        {

        }

        private void OnCopy(object sender, ExecutedRoutedEventArgs e)
        {

        }

        private void HomeMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new View.Login());
                    return;
                }

                if (string.IsNullOrEmpty(user_setting.StactionCode))
                    throw new ArgumentException("ท่านยังไม่ได้ระบุลานรับซื้อ หลังการล็อคอิน");

                MainFrame.NavigationService.Navigate(new View.Home());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void LogoffMenu_Click(object sender, RoutedEventArgs e)
        {
            user_setting.User = null;
            user_setting.StactionCode = null;
            user_setting.UserRoles = null;
            MainFrame.NavigationService.Navigate(new View.Login());
        }

        private void RegisterBarcodeMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new View.Login());
                    return;
                }

                if (string.IsNullOrEmpty(user_setting.StactionCode))
                    throw new ArgumentException("ท่านยังไม่ได้ระบุลานรับซื้อ หลังการล็อคอิน");

                if (user_setting.UserRoles
                    .Where(x => x.RoleName == "Admin" ||
                    x.RoleName == "Staff" ||
                    x.RoleName == "Supervisor")
                    .Count() < 1)
                    throw new ArgumentException("เมนูนี้อนุญาตเฉพาะ Admin และ Staff เท่านั้น");

                MainFrame.NavigationService.Navigate(new View.BuyingStaff.BuyingDocument());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CaptureWeightMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new View.Login());
                    return;
                }

                if (string.IsNullOrEmpty(user_setting.StactionCode))
                    throw new ArgumentException("ท่านยังไม่ได้ระบุลานรับซื้อ หลังการล็อคอิน");

                if (user_setting.UserRoles
                    .Where(x => x.RoleName == "Admin" ||
                    x.RoleName == "Staff" ||
                    x.RoleName == "Supervisor")
                    .Count() < 1)
                    throw new ArgumentException("เมนูนี้อนุญาตเฉพาะ Admin และ Staff เท่านั้น");

                Buying window = new Buying();
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PrintVoucherMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new View.Login());
                    return;
                }

                if (string.IsNullOrEmpty(user_setting.StactionCode))
                    throw new ArgumentException("ท่านยังไม่ได้ระบุลานรับซื้อ หลังการล็อคอิน");

                if (user_setting.UserRoles
                    .Where(x => x.RoleName == "Admin" ||
                    x.RoleName == "Staff" ||
                    x.RoleName == "Supervisor")
                    .Count() < 1)
                    throw new ArgumentException("เมนูนี้อนุญาตเฉพาะ Admin และ Staff เท่านั้น");

                MainFrame.NavigationService.Navigate(new View.BuyingStaff.Voucher());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void TruckMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new View.Login());
                    return;
                }

                if (string.IsNullOrEmpty(user_setting.StactionCode))
                    throw new ArgumentException("ท่านยังไม่ได้ระบุลานรับซื้อ หลังการล็อคอิน");

                if (user_setting.UserRoles
                    .Where(x => x.RoleName == "Admin" ||
                    x.RoleName == "Staff" ||
                    x.RoleName == "Supervisor")
                    .Count() < 1)
                    throw new ArgumentException("เมนูนี้อนุญาตเฉพาะ Admin และ Staff เท่านั้น");

                MainFrame.NavigationService.Navigate(new View.BuyingStaff.TransportationDocument());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CPASampleCollectionMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new View.Login());
                    return;
                }

                if (string.IsNullOrEmpty(user_setting.StactionCode))
                    throw new ArgumentException("ท่านยังไม่ได้ระบุลานรับซื้อ หลังการล็อคอิน");

                if (user_setting.UserRoles
                    .Where(x => x.RoleName == "Admin" ||
                    x.RoleName == "Staff" ||
                    x.RoleName == "Supervisor")
                    .Count() < 1)
                    throw new ArgumentException("เมนูนี้อนุญาตเฉพาะ Admin และ Staff เท่านั้น");

                SampleCollection window = new SampleCollection();
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void NTRMInspectionMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new View.Login());
                    return;
                }

                if (string.IsNullOrEmpty(user_setting.StactionCode))
                    throw new ArgumentException("ท่านยังไม่ได้ระบุลานรับซื้อ หลังการล็อคอิน");

                if (user_setting.UserRoles
                    .Where(x => x.RoleName == "Admin" ||
                    x.RoleName == "Staff" ||
                    x.RoleName == "Supervisor")
                    .Count() < 1)
                    throw new ArgumentException("เมนูนี้อนุญาตเฉพาะ Admin และ Staff เท่านั้น");

                NTRMInspection window = new NTRMInspection();
                window.DataContext = new vm_NTRMInspection();
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void SetupDigitalScaleMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new View.Login());
                    return;
                }

                if (string.IsNullOrEmpty(user_setting.StactionCode))
                    throw new ArgumentException("ท่านยังไม่ได้ระบุลานรับซื้อ หลังการล็อคอิน");

                if (user_setting.UserRoles
                    .Where(x => x.RoleName == "Admin" ||
                    x.RoleName == "Staff" ||
                    x.RoleName == "Supervisor")
                    .Count() < 1)
                    throw new ArgumentException("เมนูนี้อนุญาตเฉพาะ Admin และ Staff เท่านั้น");

                SetupDigitalScale window = new SetupDigitalScale();
                window.DataContext = new vm_SetupDigitalScale(window);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void MoistureResultMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new View.Login());
                    return;
                }

                if (user_setting.UserRoles
                    .Where(x => x.RoleName == "Admin" ||
                    x.RoleName == "QC")
                    .Count() < 1)
                    throw new ArgumentException("เมนูนี้อนุญาตเฉพาะ Admin และ QC เท่านั้น");

                MainFrame.NavigationService.Navigate(new View.QC.MoistureResult());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void MoistureReportMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new View.Login());
                    return;
                }

                if (string.IsNullOrEmpty(user_setting.StactionCode))
                    throw new ArgumentException("ท่านยังไม่ได้ระบุลานรับซื้อ หลังการล็อคอิน");

                if (user_setting.UserRoles
                    .Where(x => x.RoleName == "Admin" ||
                    x.RoleName == "QC")
                    .Count() < 1)
                    throw new ArgumentException("เมนูนี้อนุญาตเฉพาะ Admin และ Staff เท่านั้น");

                Forms.QC.MoistureResultReport window = new Forms.QC.MoistureResultReport(DateTime.Now);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ManageFarmerQuotaMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new View.Login());
                    return;
                }

                if (string.IsNullOrEmpty(user_setting.StactionCode))
                    throw new ArgumentException("ท่านยังไม่ได้ระบุลานรับซื้อ หลังการล็อคอิน");

                if (user_setting.UserRoles
                    .Where(x => x.RoleName == "Admin" ||
                    x.RoleName == "Supervisor")
                    .Count() < 1)
                    throw new ArgumentException("เมนูนี้อนุญาตเฉพาะ Admin และ Supervisor เท่านั้น");

                Forms.Supervisor.FarmerProjects window = new Forms.Supervisor.FarmerProjects();
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ReceivingHessianMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new View.Login());
                    return;
                }

                if (string.IsNullOrEmpty(user_setting.StactionCode))
                    throw new ArgumentException("ท่านยังไม่ได้ระบุลานรับซื้อ หลังการล็อคอิน");

                if (user_setting.UserRoles
                    .Where(x => x.RoleName == "Admin" ||
                    x.RoleName == "Staff" ||
                    x.RoleName == "Supervisor")
                    .Count() < 1)
                    throw new ArgumentException("เมนูนี้อนุญาตเฉพาะ Admin Staff และ Supervisor เท่านั้น");

                //MainFrame.NavigationService.Navigate(new Forms.Hessian.HessianReceivings());
                MainFrame.NavigationService.Navigate(new View.Hessian.Receiving());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DistributionHessianMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new View.Login());
                    return;
                }

                if (string.IsNullOrEmpty(user_setting.StactionCode))
                    throw new ArgumentException("ท่านยังไม่ได้ระบุลานรับซื้อ หลังการล็อคอิน");

                if (user_setting.UserRoles
                    .Where(x => x.RoleName == "Admin" ||
                    x.RoleName == "Staff" ||
                    x.RoleName == "Supervisor")
                    .Count() < 1)
                    throw new ArgumentException("เมนูนี้อนุญาตเฉพาะ Admin Staff และ Supervisor เท่านั้น");

                MainFrame.NavigationService.Navigate(new Forms.Hessian.HessianDistributions());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void EditBaleInfoMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new View.Login());
                    return;
                }

                if (string.IsNullOrEmpty(user_setting.StactionCode))
                    throw new ArgumentException("ท่านยังไม่ได้ระบุลานรับซื้อ หลังการล็อคอิน");

                if (user_setting.UserRoles
                    .Where(x => x.RoleName == "Admin" ||
                    x.RoleName == "Supervisor")
                    .Count() < 1)
                    throw new ArgumentException("เมนูนี้อนุญาตเฉพาะ Admin และ Supervisor เท่านั้น");

                MainFrame.NavigationService.Navigate(new BarcodeInfo());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void BarcodeInfoMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new View.Login());
                    return;
                }

                if (string.IsNullOrEmpty(user_setting.StactionCode))
                    throw new ArgumentException("ท่านยังไม่ได้ระบุลานรับซื้อ หลังการล็อคอิน");

                if (user_setting.UserRoles
                    .Where(x => x.RoleName == "Admin" ||
                    x.RoleName == "Staff" ||
                    x.RoleName == "Supervisor")
                    .Count() < 1)
                    throw new ArgumentException("เมนูนี้อนุญาตเฉพาะ Admin และ Supervisor เท่านั้น");

                MainFrame.NavigationService.Navigate(new BarcodeInfo());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ManageUserAccountMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new View.Login());
                    return;
                }

                if (user_setting.UserRoles.Where(x => x.RoleName == "Admin").Count() < 1)
                    throw new ArgumentException("เมนูนี้อนุญาตเฉพาะ Admin เท่านั้น");

                MainFrame.NavigationService.Navigate(new View.Admin.UserAccount());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ViewAreaSummaryMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new View.Login());
                    return;
                }

                if (string.IsNullOrEmpty(user_setting.StactionCode))
                    throw new ArgumentException("ท่านยังไม่ได้ระบุลานรับซื้อ หลังการล็อคอิน");

                if (user_setting.UserRoles
                    .Where(x => x.RoleName == "Admin" ||
                    x.RoleName == "Supervisor")
                    .Count() < 1)
                    throw new ArgumentException("เมนูนี้อนุญาตเฉพาะ Admin และ Supervisor เท่านั้น");

                MainFrame.NavigationService.Navigate(new AreaSummary());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void TruckMenu2_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new View.Login());
                    return;
                }

                if (user_setting.UserRoles
                    .Where(x => x.RoleName == "Admin" ||
                    x.RoleName == "Staff" ||
                    x.RoleName == "Supervisor")
                    .Count() < 1)
                    throw new ArgumentException("เมนูนี้อนุญาตเฉพาะ Admin Staff และ Supervisor เท่านั้น");

                MainFrame.NavigationService.Navigate(new View.BuyingStaff.MoveLoadingBale());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ReceivingConfirmMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.User == null)
                {
                    MessageBoxHelper.Warning("โปรดทำการล็อคอินเข้าใช้งานก่อน");
                    MainFrame.NavigationService.Navigate(new View.Login());
                    return;
                }

                if (user_setting.UserRoles
                    .Where(x => x.RoleName == "Admin" ||
                    x.RoleName == "Receiving")
                    .Count() < 1)
                    throw new ArgumentException("เมนูนี้อนุญาตเฉพาะ Admin และ Receiving เท่านั้น");

                MainFrame.NavigationService.Navigate(new Confirm());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
