﻿using BUWinApp.Helper;
using BUWinApp.MVVM;
using BusinessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Navigation;

namespace BUWinApp.ViewModel
{
    public class vm_Login : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_Login()
        {
            _loginVisible = Visibility.Visible;
            _goHomeVisible = Visibility.Collapsed;
            //_username = "eakkaluck";
            //_password = "1234";

            //RaisePropertyChangedEvent(nameof(Username));
            //RaisePropertyChangedEvent(nameof(Password));
        }



        #region Properties
        private string _username;
        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        private string _stationCode;

        public string StationCode
        {
            get { return _stationCode; }
            set
            {
                _stationCode = value;
                if (!string.IsNullOrEmpty(_stationCode))
                    _isContinueButtonEnable = true;
                else
                    _isContinueButtonEnable = false;

                RaisePropertyChangedEvent(nameof(IsContinueButtonEnable));
            }
        }

        private Visibility _goHomeVisible;
        public Visibility GoHomeVisible
        {
            get { return _goHomeVisible; }
            set { _goHomeVisible = value; }
        }

        private Visibility _loginVisible;
        public Visibility LoginVisible
        {
            get { return _loginVisible; }
            set { _loginVisible = value; }
        }

        private List<BuyingStation> _stationList;

        public List<BuyingStation> StationList
        {
            get { return _stationList; }
            set { _stationList = value; }
        }

        private bool _isContinueButtonEnable;

        public bool IsContinueButtonEnable
        {
            get { return _isContinueButtonEnable; }
            set { _isContinueButtonEnable = value; }
        }

        #endregion



        #region Command
        private ICommand _onPasswordChangedCommand;

        public ICommand OnPasswordChangedCommand
        {
            get { return _onPasswordChangedCommand ?? (_onPasswordChangedCommand = new RelayCommand(OnPasswordChanged)); }
            set { _onPasswordChangedCommand = value; }
        }

        private void OnPasswordChanged(object e)
        {
            _password = ((System.Windows.Controls.PasswordBox)e).Password;
        }

        private ICommand _onLoginCommand;

        public ICommand OnLoginCommand
        {
            get { return _onLoginCommand ?? (_onLoginCommand = new RelayCommand(OnLogin)); }
            set { _onLoginCommand = value; }
        }

        private void OnLogin(object e)
        {
            try
            {
                if (string.IsNullOrEmpty(_username))
                {
                    MessageBoxHelper.Warning("โปรดกรอกชื่อผู้ใช้");
                    OnFocusRequested(nameof(Username));
                    return;
                }

                if (string.IsNullOrEmpty(_password))
                {
                    MessageBoxHelper.Warning("โปรดกรอกรหัสผ่าน");
                    OnFocusRequested(nameof(Password));
                    return;
                }

                /// Active directory service.
                /// 
                //if (ActiveDirectoryHelper.ValidateUserNameInActiveDirectory(_username) == false)
                //    throw new ArgumentException("ไม่พบชื่อผู้ใช้นี้ในระบบ โปรดตรวจสอบกับทางแผนกไอที");

                //if (ActiveDirectoryHelper.ActiveDirectoryAuthenticate(_username, _password) == false)
                //    throw new ArgumentException("รหัสผ่านไม่ถูกต้อง");

                //var user = BuyingFacade.PolicyUserRoleBL().GetByUsername(_username);
                //if (user.Count() <= 0)
                //    throw new ArgumentException("บัญชีผู้ใช้งานของท่านยังไม่ได้ลงทะเบียนเข้าใช้งานระบบ โปรดติดต่อแผนกไอทีเพื่อทำการเพิ่มสิทธิ์การใช้งานระบบ");

                ///// store login information.
                ///// 
                //Helper.user_setting.User.Username = _username;
                //Helper.user_setting.userRoles = BuyingFacade.PolicyUserRoleBL()
                //    .GetByUsername(_username)
                //    .Select(x => new AppPolicyRole
                //    {
                //        RoleID = x.RoleID,
                //        RoleName = x.AppPolicyRole.RoleName
                //    }).ToList();


                var user = BuyingFacade.UserBL().GetByUsername(_username);
                if (user == null)
                    throw new ArgumentException("ไม่พบชื่อผู้ใช้นี้ในระบบ");

                if (user.Password != _password)
                    throw new ArgumentException("รหัสผ่านไม่ถูกต้อง");

                user_setting.User = user;
                user_setting.UserRoles = (from ur in BuyingFacade.UserRoleBL().GetByUser(user.Username)
                                          from r in BuyingFacade.RoleBL().GetAll()
                                          where ur.RoleID == r.RoleID
                                          select new Role
                                          {
                                              RoleID = ur.RoleID,
                                              RoleName = r.RoleName
                                          }).ToList();

                /// Go to Home page.
                /// 
                if (user.StaffUser != null)
                    _stationList = BuyingFacade.BuyingStationBL()
                        .GetByArea(user_setting.User.StaffUser.AreaCode);

                _loginVisible = Visibility.Collapsed;
                _goHomeVisible = Visibility.Visible;

                RaisePropertyChangedEvent(nameof(LoginVisible));
                RaisePropertyChangedEvent(nameof(GoHomeVisible));
                RaisePropertyChangedEvent(nameof(StationList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(OnClear)); }
            set { _onClearCommand = value; }
        }

        private void OnClear(object e)
        {
            _username = "";
            _password = "";

            RaisePropertyChangedEvent(nameof(Username));
            RaisePropertyChangedEvent(nameof(Password));
            OnFocusRequested(nameof(Username));
            OnFocusRequested(nameof(Password));
        }
        #endregion
    }
}
