﻿using BUWinApp.Helper;
using BUWinApp.MVVM;
using BusinessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace BUWinApp.ViewModel.Admin
{
    public class vm_Role : ObservableObject, IRequestFocus
    {
        public vm_Role()
        {
            Refresh();
        }

        #region Properties
        private string _roleName;
        public string RoleName
        {
            get { return _roleName; }
            set { _roleName = value; }
        }

        private bool _canEdit;
        public bool CanEdit
        {
            get { return _canEdit; }
            set { _canEdit = value; }
        }

        private int _totalRecord;
        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private AppPolicyRole _selectedRow;
        public AppPolicyRole SelectedRow
        {
            get { return _selectedRow; }
            set { _selectedRow = value; }
        }
        #endregion


        #region ListProperties
        private List<AppPolicyRole> _roleList;

        public List<AppPolicyRole> RoleList
        {
            get { return _roleList; }
            set { _roleList = value; }
        }

        #endregion


        #region Command
        private ICommand _onCreateCommand;
        public ICommand OnCreateCommand
        {
            get { return _onCreateCommand ?? (_onCreateCommand = new RelayCommand(OnCreate)); }
            set { _onCreateCommand = value; }
        }

        private ICommand _onEditCommand;
        public ICommand OnEditCommand
        {
            get { return _onEditCommand ?? (_onEditCommand = new RelayCommand(OnEdit)); }
            set { _onEditCommand = value; }
        }

        private ICommand _onDeleteCommand;
        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private ICommand _onRefreshCommand;
        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(OnRefresh)); }
            set { _onRefreshCommand = value; }
        }

        private ICommand _onRowSelectedCommand;
        public ICommand OnRowSelectedCommand
        {
            get { return _onRowSelectedCommand ?? (_onRowSelectedCommand = new RelayCommand(OnSelectedRow)); }
            set { _onRowSelectedCommand = value; }
        }
        #endregion


        #region Function
        private void OnCreate(object e)
        {
            try
            {
                if (string.IsNullOrEmpty(_roleName))
                {
                    MessageBoxHelper.Warning("Role name cannot be empty.");
                    OnFocusRequested(nameof(RoleName));
                    return;
                }

                BuyingFacade.PolicyRoleBL().Add(new AppPolicyRole
                {
                    RoleID = Guid.NewGuid(),
                    RoleName = _roleName,
                    ModifiedBy = Helper.user_setting.User.Username,
                    ModifiedDate = DateTime.Now,
                    CreateBy = Helper.user_setting.User.Username,
                    CreateDate = DateTime.Now
                });

                Refresh();
                MessageBoxHelper.Info("Added succesfully!");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void OnEdit(object e)
        {
            try
            {
                if (string.IsNullOrEmpty(_roleName))
                {
                    MessageBoxHelper.Warning("Role name cannot be empty.");
                    OnFocusRequested(nameof(RoleName));
                    return;
                }

                BuyingFacade.PolicyRoleBL()
                    .Edit(_selectedRow.RoleID, _roleName, user_setting.User.Username);
                MessageBoxHelper.Info("Edit succesfully!");
                Refresh();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void OnDelete(object e)
        {
            try
            {
                if (MessageBoxHelper.Question("Do you want to delete this record?") == MessageBoxResult.No)
                    return;

                BuyingFacade.PolicyRoleBL().Delete(Guid.Parse(e.ToString()));
                MessageBoxHelper.Info("Deleted succesfully!");
                Refresh();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void OnRefresh(object e)
        {
            Refresh();
        }

        private void Refresh()
        {
            _roleName = "";
            _canEdit = false;
            _selectedRow = null;

            RaisePropertyChangedEvent(nameof(RoleName));
            RaisePropertyChangedEvent(nameof(CanEdit));
            OnFocusRequested(nameof(RoleName));
            RoleListDataBinding();
        }

        private void RoleListDataBinding()
        {
            _roleList = BuyingFacade.PolicyRoleBL()
                .GetAll()
                .OrderBy(x => x.RoleName)
                .ToList();

            _totalRecord = _roleList.Count();
            RaisePropertyChangedEvent(nameof(TotalRecord));
            RaisePropertyChangedEvent(nameof(RoleList));
        }

        private void OnSelectedRow(object e)
        {
            _selectedRow = BuyingFacade.PolicyRoleBL()
                .GetSingle(Guid.Parse(e.ToString()));

            _canEdit = true;
            _roleName = _selectedRow.RoleName;
            RaisePropertyChangedEvent(nameof(RoleName));
            RaisePropertyChangedEvent(nameof(CanEdit));
            OnFocusRequested(nameof(RoleName));
        }
        #endregion

        #region Event
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        private void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }
        #endregion
    }
}
