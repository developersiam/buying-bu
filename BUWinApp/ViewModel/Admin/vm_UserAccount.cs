﻿using BUWinApp.Helper;
using BUWinApp.MVVM;
using BusinessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace BUWinApp.ViewModel.Admin
{
    public class vm_UserAccount : ObservableObject, IRequestFocus
    {
        public vm_UserAccount()
        {
            _isSearchFound = false;
            RaisePropertyChangedEvent(nameof(IsSearchFound));
            UserAccountBinding();
        }

        #region Properties
        private string _username;
        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }

        private bool _isSearchFound;
        public bool IsSearchFound
        {
            get { return _isSearchFound; }
            set { _isSearchFound = value; }
        }

        private int _totalRecord;
        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }
        #endregion



        #region ListProperties
        private List<AppPolicyUser> _userAccountList;
        public List<AppPolicyUser> UserAccountList
        {
            get { return _userAccountList; }
            set { _userAccountList = value; }
        }
        #endregion



        #region Command
        private ICommand _onAddCommand;
        public ICommand OnAddCommand
        {
            get { return _onAddCommand ?? (_onAddCommand = new RelayCommand(OnAdd)); }
            set { _onAddCommand = value; }
        }

        private ICommand _onSearchCommand;
        public ICommand OnSearchCommand
        {
            get { return _onSearchCommand ?? (_onSearchCommand = new RelayCommand(OnSearch)); }
            set { _onSearchCommand = value; }
        }

        private ICommand _onRefreshCommand;
        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(OnRefresh)); }
            set { _onRefreshCommand = value; }
        }

        private ICommand _onAssignRoleCommand;
        public ICommand OnAssignRoleCommand
        {
            get { return _onAssignRoleCommand ?? (_onAssignRoleCommand = new RelayCommand(OnAssignRole)); }
            set { _onAssignRoleCommand = value; }
        }

        private ICommand _onDeleteCommand;
        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }
        #endregion



        #region Fucntion
        private void OnSearch(object e)
        {
            try
            {
                if (string.IsNullOrEmpty(_username))
                {
                    MessageBoxHelper.Warning("Username cannot be empty.");
                    OnFocusRequested(nameof(Username));
                    return;
                }

                if (!ActiveDirectoryHelper.ValidateUserNameInActiveDirectory(_username))
                {
                    MessageBoxHelper.Warning("Not found an username in the company active directory not found." +
                                            "Please contact IT administrator.");
                    _isSearchFound = false;
                    RaisePropertyChangedEvent(nameof(IsSearchFound));
                    return;
                }
                else
                {
                    _isSearchFound = true;
                    RaisePropertyChangedEvent(nameof(IsSearchFound));
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void OnAdd(object e)
        {
            try
            {
                if (string.IsNullOrEmpty(_username))
                {
                    MessageBoxHelper.Warning("โปรดกรอก username");
                    OnFocusRequested(nameof(Username));
                    return;
                }

                if (!_isSearchFound)
                    throw new ArgumentException("ไม่พบ username นี้ในระบบของบริษัท");

                BuyingFacade.PolicyUserAccountBL().Add(_username, Helper.user_setting.User.Username);
                MessageBoxHelper.Info("Added successfully.");
                RefreshForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void OnAssignRole(object e)
        {
            View.Admin.UserRole window = new View.Admin.UserRole();
            var vm = new vm_UserRole();
            vm.User = BuyingFacade.PolicyUserAccountBL().GetSingle(e.ToString());

            window.DataContext = vm;
            window.ShowDialog();
        }

        private void OnDelete(object e)
        {
            try
            {
                var message = MessageBox.Show("Do you want to delete this record?",
                    "warning!", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (MessageBoxHelper.Question("Do you want to delete this record?") == MessageBoxResult.No)
                    return;

                if (string.IsNullOrEmpty(e.ToString()))
                    return;

                BuyingFacade.PolicyUserAccountBL().Delete(e.ToString());
                UserAccountBinding();
                MessageBoxHelper.Info("Deleted succesfully!");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void OnRefresh(object e)
        {
            RefreshForm();
        }

        private void RefreshForm()
        {
            _username = "";
            _isSearchFound = false;

            RaisePropertyChangedEvent(nameof(Username));
            RaisePropertyChangedEvent(nameof(IsSearchFound));
            OnFocusRequested(nameof(Username));
            UserAccountBinding();
        }

        private void UserAccountBinding()
        {
            _userAccountList = BuyingFacade.PolicyUserAccountBL().GetAll();
            _totalRecord = _userAccountList.Count();
            RaisePropertyChangedEvent(nameof(TotalRecord));
            RaisePropertyChangedEvent(nameof(UserAccountList));
        }
        #endregion



        #region Event
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        private void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }
        #endregion
    }
}
