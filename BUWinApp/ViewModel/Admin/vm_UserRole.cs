﻿using BUWinApp.Helper;
using BUWinApp.MVVM;
using BusinessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BUWinApp.ViewModel.Admin
{
    public struct UserRoleDataGrid
    {
        public string Username { get; set; }
        public bool IsActive { get; set; }
        public Guid RoleID { get; set; }
        public string RoleName { get; set; }
    }

    public class vm_UserRole : ObservableObject, IRequestFocus
    {
        public vm_UserRole()
        {

        }

        #region Properties
        private AppPolicyUser _user;

        public AppPolicyUser User
        {
            get { return _user; }
            set
            {
                _user = value;
                UserRoleDataGridBinding();
            }
        }
        #endregion


        #region ListProperties
        private List<UserRoleDataGrid> _userRoleDataGridList;
        public List<UserRoleDataGrid> UserRoleDataGridList
        {
            get { return _userRoleDataGridList; }
            set { _userRoleDataGridList = value; }
        }
        #endregion


        #region Command
        private ICommand _onAddNewRoleCommand;
        public ICommand OnAddNewRoleCommand
        {
            get { return _onAddNewRoleCommand ?? (_onAddNewRoleCommand = new RelayCommand(OnAddNewRole)); }
            set { _onAddNewRoleCommand = value; }
        }

        private ICommand _onCheckedChangedCommand;
        public ICommand OnCheckedChangedCommand
        {
            get { return _onCheckedChangedCommand ?? (_onCheckedChangedCommand = new RelayCommand(OnCheckedChanged)); }
            set { _onCheckedChangedCommand = value; }
        }
        #endregion


        #region Function
        private void UserRoleDataGridBinding()
        {
            _userRoleDataGridList = (from r in BuyingFacade.PolicyRoleBL().GetAll()
                                     join u in BuyingFacade.PolicyUserRoleBL().GetByUsername(_user.Username)
                                     on r.RoleID equals u.RoleID into ur
                                     from rs in ur.DefaultIfEmpty()
                                     select new UserRoleDataGrid
                                     {
                                         Username = _user.Username,
                                         RoleID = r.RoleID,
                                         RoleName = r.RoleName,
                                         IsActive = rs == null ? false : true
                                     })
                                     .OrderBy(x => x.RoleName)
                                     .ToList();

            RaisePropertyChangedEvent(nameof(UserRoleDataGridList));
        }

        private void OnCheckedChanged(object e)
        {
            try
            {
                var model = BuyingFacade.PolicyUserRoleBL()
                    .GetSingle(_user.Username, Guid.Parse(e.ToString()));

                if (model == null)
                    BuyingFacade.PolicyUserRoleBL()
                        .Add(new AppPolicyUserRole
                        {
                            Username = _user.Username,
                            RoleID = Guid.Parse(e.ToString()),
                            CreateDate = DateTime.Now,
                            CreateBy = Helper.user_setting.User.Username
                        });
                else
                    BuyingFacade.PolicyUserRoleBL().Delete(_user.Username, model.RoleID);

                UserRoleDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void OnAddNewRole(object e)
        {
            View.Admin.Role window = new View.Admin.Role();
            var vm = new vm_Role();
            window.DataContext = vm;
            window.ShowDialog();
            UserRoleDataGridBinding();
        }
        #endregion


        #region Event
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }
        #endregion
    }
}
