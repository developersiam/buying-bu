﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using BusinessLayer.Model;
using BusinessLayer.Helper;
using BUWinApp.Helper;

namespace BUWinApp.ViewModel.Hessian
{
    public class vm_FarmerInfo : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_FarmerInfo()
        {

        }


        #region Properties
        private string _farmerCode;

        public string FarmerCode
        {
            get { return _farmerCode; }
            set
            {
                _farmerCode = value;
                FarmerInfoBinding();
            }
        }
        #endregion


        #region List
        private List<m_BuyingDocument> _documentList;

        public List<m_BuyingDocument> DocumentList
        {
            get { return _documentList; }
            set { _documentList = value; }
        }
        #endregion


        #region Function
        private void FarmerInfoBinding()
        {
            _documentList = BuyingDocumentHelper.GetByFarmer(user_setting.Crop.Crop1, _farmerCode);
            RaisePropertyChangedEvent(nameof(DocumentList));
        }
        #endregion
    }
}
