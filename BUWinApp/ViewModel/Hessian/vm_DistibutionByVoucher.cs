﻿using BusinessLayer;
using BusinessLayer.Helper;
using BusinessLayer.Model;
using BUWinApp.Helper;
using BUWinApp.MVVM;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace BUWinApp.ViewModel.Hessian
{
    public class vm_DistibutionByVoucher : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_DistibutionByVoucher()
        {
            _warehouseList = BuyingFacade.WarehouseBL()
                  .GetByArea(user_setting.User.StaffUser.AreaCode)
                  .ToList();

            _warehouseID = _warehouseList.FirstOrDefault().WarehouseID;
            WarehouseSummaryBinding();
            ReceivingListBinding();
            _receivingCode = _receivingList.FirstOrDefault().ReceivingCode;

            var model = _receivingList
                .SingleOrDefault(x => x.ReceivingCode == _receivingCode);
            if (model == null)
                return;
            _receivedOnHand = model.OnHand;

            RaisePropertyChangedEvent(nameof(WarehouseList));
            RaisePropertyChangedEvent(nameof(ReceivingList));
            RaisePropertyChangedEvent(nameof(ReceivingCode));
            RaisePropertyChangedEvent(nameof(ReceivedOnHand));
        }


        #region Properties
        private m_BuyingDocument _document;

        public m_BuyingDocument Document
        {
            get { return _document; }
            set
            {
                _document = value;
                DocumentBinding();
            }
        }

        private Guid _warehouseID;

        public Guid WarehouseID
        {
            get { return _warehouseID; }
            set
            {
                _warehouseID = value;
                WarehouseSummaryBinding();
                ReceivingListBinding();
            }
        }

        private DateTime _distributionDate;

        public DateTime DistributionDate
        {
            get { return _distributionDate; }
            set
            {
                _distributionDate = value;
                Clear();
            }
        }

        private string _receivingCode;

        public string ReceivingCode
        {
            get { return _receivingCode; }
            set
            {
                _receivingCode = value;
                var model = _receivingList
                    .SingleOrDefault(x => x.ReceivingCode == _receivingCode);

                if (model == null)
                    return;

                _receivedOnHand = model.OnHand;
                RaisePropertyChangedEvent(nameof(ReceivedOnHand));
            }
        }

        private int _receivedOnHand;

        public int ReceivedOnHand
        {
            get { return _receivedOnHand; }
            set { _receivedOnHand = value; }
        }

        private int _limitQty;

        public int LimitQty
        {
            get { return _limitQty; }
            set { _limitQty = value; }
        }

        private int _distributedQty;

        public int DistributedQty
        {
            get { return _distributedQty; }
            set { _distributedQty = value; }
        }

        private int _onHand;

        public int OnHand
        {
            get { return _onHand; }
            set { _onHand = value; }
        }

        private int _issued;

        public int Issued
        {
            get { return _issued; }
            set { _issued = value; }
        }

        private int _received;

        public int Received
        {
            get { return _received; }
            set { _received = value; }
        }

        private short _quantity;

        public short Quantity
        {
            get { return _quantity; }
            set
            {
                _quantity = value;

                if (_quantity > Convert.ToInt16(_limitQty - _distributedQty))
                {
                    MessageBoxHelper.Warning("จำนวนกระสอบที่จะจ่าย ต้องไม่เกินจำนวนที่กำหนด");
                    _quantity = Convert.ToInt16(_limitQty - _distributedQty);
                    OnFocusRequested(nameof(Quantity));
                    return;
                }

                if (_quantity > _receivedOnHand)
                {
                    MessageBoxHelper.Warning("จำนวนกระสอบที่จะจ่าย ต้องไม่เกินจำนวนที่กำหนด");
                    _quantity = Convert.ToInt16(_limitQty - _distributedQty);
                    OnFocusRequested(nameof(Quantity));
                    return;
                }
            }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }
        #endregion



        #region List
        private List<Warehouse> _warehouseList;

        public List<Warehouse> WarehouseList
        {
            get { return _warehouseList; }
            set { _warehouseList = value; }
        }

        private List<m_HessianReceiving> _receivingList;

        public List<m_HessianReceiving> ReceivingList
        {
            get { return _receivingList; }
            set { _receivingList = value; }
        }

        private List<HessianDistribution> _distributionList;

        public List<HessianDistribution> DistributionList
        {
            get { return _distributionList; }
            set { _distributionList = value; }
        }
        #endregion



        #region Command
        private ICommand _onAddCommand;

        public ICommand OnAddCommand
        {
            get { return _onAddCommand ?? (_onAddCommand = new RelayCommand(OnAdd)); }
            set { _onAddCommand = value; }
        }
        private void OnAdd(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_document.FarmerCode))
                    throw new ArgumentException("โปรดระบุ farmer code ของชาวไร่ที่จะจ่ายกระสอบให้");

                if (string.IsNullOrEmpty(_receivingCode))
                    throw new ArgumentException("โปรดระบุ receiving code ของกระสอบที่ได้รับที่อยู่ใน stock");

                if (_quantity <= 0)
                    throw new ArgumentException("จำนวนกระสอบที่จะจ่าย ต้องมากกว่า 1 ขึ้นไป");

                if (_quantity > Convert.ToInt16(_limitQty - _distributedQty))
                    throw new ArgumentException("จำนวนกระสอบที่จะจ่าย ต้องไม่เกินจำนวนที่กำหนด");

                if (_quantity > _receivedOnHand)
                    throw new ArgumentException("จำนวนกระสอบไม่พอจ่าย โปรดเลือก receiving code อื่น");

                if (_document == null)
                    throw new ArgumentException("ไม่พบรหัสเวาเชอร์ของชาวไร่ที่จะนำไปเบิกกระสอบ โปรดติดต่อผู้ดูและระบบเพื่อตรวจสอบ");

                BuyingFacade.HessianDistributionBL()
                    .Add(new HessianDistribution
                    {
                        DistributionID = Guid.NewGuid(),
                        Crop = user_setting.Crop.Crop1,
                        BuyingStationCode = _document.BuyingStationCode,
                        FarmerCode = _document.FarmerCode,
                        BuyingDocumentNumber = _document.BuyingDocumentNumber,
                        Quantity = _quantity,
                        ReceivingCode = _receivingCode,
                        ModifiedBy = user_setting.User.Username,
                        ModifiedDate = DateTime.Now,
                        DistributedBy = user_setting.User.Username,
                        DistributedDate = _distributionDate
                    });

                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onResetCommand;

        public ICommand OnResetCommand
        {
            get { return _onResetCommand ?? (_onResetCommand = new RelayCommand(OnReset)); }
            set { _onResetCommand = value; }
        }

        private void OnReset(object obj)
        {
            Clear();
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                var model = (HessianDistribution)obj;
                BuyingFacade.HessianDistributionBL().Delete(model.DistributionID);
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion



        #region Fucntion
        private void Clear()
        {
            DocumentBinding();
            WarehouseSummaryBinding();
        }

        private void DistributionListBinding()
        {
            _distributionList = BuyingFacade.HessianDistributionBL()
                .GetByVoucher(_document.Crop,
                _document.FarmerCode,
                _document.BuyingStationCode,
                _document.BuyingDocumentNumber);
            _totalRecord = _distributionList.Count();

            RaisePropertyChangedEvent(nameof(DistributionList));
            RaisePropertyChangedEvent(nameof(TotalRecord));
        }

        private void ReceivingListBinding()
        {
            _receivingList = HessianReceivingHelper
                     .GetByWarehouse(user_setting.Crop.Crop1, _warehouseID)
                     .Where(x => x.OnHand > 0)
                     .ToList();

            RaisePropertyChangedEvent(nameof(ReceivingList));
        }

        private void WarehouseSummaryBinding()
        {
            var list = HessianReceivingHelper
                .GetByWarehouse(user_setting.Crop.Crop1, _warehouseID);

            _received = list.Sum(x => x.Quantity);
            _issued = list.Sum(x => x.Issued);
            _onHand = list.Sum(x => x.OnHand);

            RaisePropertyChangedEvent(nameof(Received));
            RaisePropertyChangedEvent(nameof(Issued));
            RaisePropertyChangedEvent(nameof(OnHand));
        }

        private void DocumentBinding()
        {
            DistributionListBinding();

            _distributedQty = _distributionList.Sum(x => x.Quantity);
            _limitQty = _document.TotalWeight * 2;
            _quantity = Convert.ToInt16(_limitQty - _distributedQty);
            _distributionDate = Convert.ToDateTime(_document.BuyingDate);
            var temp_recevingCode = _receivingCode;

            ReceivingListBinding();
            _receivingCode = temp_recevingCode;

            var model = _receivingList
                .SingleOrDefault(x => x.ReceivingCode == temp_recevingCode);
            if (model != null)
            {
                _receivedOnHand = model.OnHand;
                RaisePropertyChangedEvent(nameof(ReceivedOnHand));
            }

            RaisePropertyChangedEvent(nameof(ReceivingCode));
            RaisePropertyChangedEvent(nameof(LimitQty));
            RaisePropertyChangedEvent(nameof(DistributedQty));
            RaisePropertyChangedEvent(nameof(DistributionDate));
            RaisePropertyChangedEvent(nameof(Quantity));

        }
        #endregion
    }
}
