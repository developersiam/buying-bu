﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using System.Windows.Input;
using BUWinApp.Helper;
using DomainModel;
using BusinessLayer;
using BusinessLayer.Helper;
using BusinessLayer.Model;
using System.Windows;

namespace BUWinApp.ViewModel.Hessian
{
    public class vm_Receiving : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_Receiving()
        {
            _receivedDate = DateTime.Now;
            RaisePropertyChangedEvent(nameof(ReceivedDate));
        }


        #region Properties

        public m_HessianReceiving _receiving { get; set; }

        private string _receivedCode;

        public string ReceivedCode
        {
            get { return _receivedCode; }
            set { _receivedCode = value; }
        }

        private DateTime _receivedDate;

        public DateTime ReceivedDate
        {
            get { return _receivedDate; }
            set { _receivedDate = value; }
        }

        private string _truckNo;

        public string TruckNo
        {
            get { return _truckNo; }
            set { _truckNo = value; }
        }

        private short _quantity;

        public short Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private Guid _warehouseID;

        public Guid WarehouseID
        {
            get { return _warehouseID; }
            set { _warehouseID = value; }
        }

        private Visibility _visibleAdd;

        public Visibility VisibleAdd
        {
            get { return _visibleAdd; }
            set { _visibleAdd = value; }
        }

        private Visibility _visibleEdit;

        public Visibility VisibleEdit
        {
            get { return _visibleEdit; }
            set { _visibleEdit = value; }
        }


        #endregion


        #region List
        private List<m_HessianReceiving> _receivingList;

        public List<m_HessianReceiving> ReceivingList
        {
            get { return _receivingList; }
            set { _receivingList = value; }
        }

        public List<Warehouse> WarehouseList
        {
            get
            {
                return BuyingFacade.WarehouseBL()
                    .GetByArea(user_setting.User.StaffUser.AreaCode)
                    .ToList();
            }
        }

        #endregion


        #region Command
        private ICommand _onAddCommand;

        public ICommand OnAddCommand
        {
            get { return _onAddCommand ?? (_onAddCommand = new RelayCommand(OnAdd)); }
            set { _onAddCommand = value; }
        }
        private void OnAdd(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_truckNo))
                {
                    MessageBoxHelper.Warning("โปรดกรอกทะเบียนรถ");
                    OnFocusRequested(nameof(TruckNo));
                    return;
                }

                if (_quantity <= 0)
                {
                    MessageBoxHelper.Warning("จำนวนที่ได้รับ ต้องมากกว่า 0");
                    OnFocusRequested(nameof(Quantity));
                    return;
                }

                if (_warehouseID == null)
                {
                    MessageBoxHelper.Warning("โปรดระบุโกดังที่เก็บกระสอบ");
                    OnFocusRequested(nameof(WarehouseID));
                    return;
                }

                BuyingFacade.HessianReceivingBL()
                    .Add(new HessianReceiving
                    {
                        ReceivedDate = _receivedDate,
                        ReceivedBy = user_setting.User.Username,
                        Crop = user_setting.Crop.Crop1,
                        WarehouseID = _warehouseID,
                        TruckNumber = _truckNo,
                        Quantity = _quantity,
                        ModifiedBy = user_setting.User.Username,
                        ModifiedDate = DateTime.Now
                    });

                ResetForm();
                ReceivingListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditCommand;

        public ICommand OnEditCommand
        {
            get { return _onEditCommand ?? (_onEditCommand = new RelayCommand(OnEdit)); }
            set { _onEditCommand = value; }
        }

        private void OnEdit(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_truckNo))
                {
                    MessageBoxHelper.Warning("โปรดกรอกทะเบียนรถ");
                    OnFocusRequested(nameof(TruckNo));
                    return;
                }

                if (_quantity <= 0)
                {
                    MessageBoxHelper.Warning("จำนวนที่ได้รับ ต้องมากกว่า 0");
                    OnFocusRequested(nameof(Quantity));
                    return;
                }

                if (string.IsNullOrEmpty(ReceivedCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ Received Code");
                    OnFocusRequested(nameof(WarehouseID));
                    return;
                }

                if (_warehouseID == null)
                {
                    MessageBoxHelper.Warning("โปรดระบุโกดังที่เก็บกระสอบ");
                    OnFocusRequested(nameof(WarehouseID));
                    return;
                }

                if (MessageBoxHelper.Question("ท่านต้องการแก้ไขรายการข้อมูลนี้ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                _receiving.ReceivedDate = _receivedDate;
                _receiving.WarehouseID = _warehouseID;
                _receiving.TruckNumber = _truckNo;
                _receiving.Quantity = _quantity;
                _receiving.ReceivedBy = user_setting.User.Username;
                _receiving.ModifiedDate = DateTime.Now;
                _receiving.ModifiedBy = user_setting.User.Username;

                BuyingFacade.HessianReceivingBL().Update(_receiving);
                ResetForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onResetCommand;

        public ICommand OnResetCommand
        {
            get { return _onResetCommand ?? (_onResetCommand = new RelayCommand(OnReset)); }
            set { _onResetCommand = value; }
        }

        private void OnReset(object obj)
        {
            ResetForm();
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("ท่านต้องการลบรายการข้อมูลนี้ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                var model = (m_HessianReceiving)obj;
                BuyingFacade.HessianReceivingBL().Delete(model.ReceivingCode);
                ReceivingListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditSelectedCommand;

        public ICommand OnEditSelectedCommand
        {
            get { return _onEditSelectedCommand ?? (_onEditSelectedCommand = new RelayCommand(OnEditSelected)); }
            set { _onEditSelectedCommand = value; }
        }

        private void OnEditSelected(object obj)
        {
            try
            {
                var model = (m_HessianReceiving)obj;
                if (model == null)
                    return;

                _receiving = model;
                _receivedCode = model.ReceivingCode;
                _warehouseID = model.WarehouseID;
                _truckNo = model.TruckNumber;
                _quantity = model.Quantity;

                _visibleAdd = Visibility.Collapsed;
                _visibleEdit = Visibility.Visible;

                RaisePropertyChangedEvent(nameof(ReceivedCode));
                RaisePropertyChangedEvent(nameof(WarehouseID));
                RaisePropertyChangedEvent(nameof(TruckNo));
                RaisePropertyChangedEvent(nameof(Quantity));
                RaisePropertyChangedEvent(nameof(VisibleAdd));
                RaisePropertyChangedEvent(nameof(VisibleEdit));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion


        #region Function
        private void ReceivingListBinding()
        {
            _receivingList = HessianReceivingHelper
                    .GetByWarehouse(user_setting.Crop.Crop1, _warehouseID)
                    .ToList();
            _totalRecord = _receivingList.Count();

            RaisePropertyChangedEvent(nameof(ReceivingList));
            RaisePropertyChangedEvent(nameof(TotalRecord));
        }

        private void ResetForm()
        {
            _receiving = null;
            _receivedCode = "";
            _truckNo = "";
            _quantity = 0;

            _visibleAdd = Visibility.Visible;
            _visibleEdit = Visibility.Collapsed;

            RaisePropertyChangedEvent(nameof(ReceivedCode));
            RaisePropertyChangedEvent(nameof(TruckNo));
            RaisePropertyChangedEvent(nameof(Quantity));
            RaisePropertyChangedEvent(nameof(VisibleAdd));
            RaisePropertyChangedEvent(nameof(VisibleEdit));
        }
        #endregion
    }
}
