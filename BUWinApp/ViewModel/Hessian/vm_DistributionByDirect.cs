﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using System.Windows.Input;
using DomainModel;
using BusinessLayer.Model;
using BusinessLayer;
using BUWinApp.Helper;
using BusinessLayer.Helper;
using System.Windows;

namespace BUWinApp.ViewModel.Hessian
{
    public class vm_DistributionByDirect : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_DistributionByDirect()
        {
            _distributionDate = DateTime.Now;
        }


        #region Properties
        private m_BuyingDocument _document;

        public m_BuyingDocument Document
        {
            get { return _document; }
            set { _document = value; }
        }

        private HessianDistribution _distribution;

        public HessianDistribution Distribution
        {
            get { return _distribution; }
            set { _distribution = value; }
        }

        private Guid _warehouseID;

        public Guid WarehouseID
        {
            get { return _warehouseID; }
            set
            {
                _warehouseID = value;
                WarehouseSummaryBinding();
                ReceivingListBinding();
            }
        }

        private DateTime _distributionDate;

        public DateTime DistributionDate
        {
            get { return _distributionDate; }
            set
            {
                _distributionDate = value;
                Clear();
            }
        }

        private string _receivingCode;

        public string ReceivingCode
        {
            get { return _receivingCode; }
            set
            {
                _receivingCode = value;

                _receivedOnHand = _receivingList
                    .SingleOrDefault(x=>x.ReceivingCode == _receivingCode)
                    .OnHand;
                RaisePropertyChangedEvent(nameof(ReceivedOnHand));
            }
        }

        private int _receivedOnHand;

        public int ReceivedOnHand
        {
            get { return _receivedOnHand; }
            set { _receivedOnHand = value; }
        }

        private int _onHand;

        public int OnHand
        {
            get { return _onHand; }
            set { _onHand = value; }
        }

        private int _issued;

        public int Issued
        {
            get { return _issued; }
            set { _issued = value; }
        }

        private int _received;

        public int Received
        {
            get { return _received; }
            set { _received = value; }
        }

        private Farmer _farmer;

        public Farmer Farmer
        {
            get { return _farmer; }
            set { _farmer = value; }
        }

        private short _quantity;

        public short Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private Visibility _visibleAdd;

        public Visibility VisibleAdd
        {
            get { return _visibleAdd; }
            set { _visibleAdd = value; }
        }

        private Visibility _visibleEdit;

        public Visibility VisibleEdit
        {
            get { return _visibleEdit; }
            set { _visibleEdit = value; }
        }
        #endregion



        #region List
        public List<Warehouse> WarehouseList
        {
            get
            {
                return BuyingFacade.WarehouseBL()
                  .GetByArea(user_setting.User.StaffUser.AreaCode)
                  .ToList();
            }
        }

        private List<m_HessianReceiving> _receivingList;

        public List<m_HessianReceiving> ReceivingList
        {
            get { return _receivingList; }
            set { _receivingList = value; }
        }

        private List<HessianDistribution> _distributionList;

        public List<HessianDistribution> DistributionList
        {
            get { return _distributionList; }
            set { _distributionList = value; }
        }

        #endregion



        #region Command
        private ICommand _onAddCommand;

        public ICommand OnAddCommand
        {
            get { return _onAddCommand ?? (_onAddCommand = new RelayCommand(OnAdd)); }
            set { _onAddCommand = value; }
        }

        private void OnAdd(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_farmer.FarmerCode))
                    throw new ArgumentException("โปรดระบุ farmer code ของชาวไร่ที่จะจ่ายกระสอบให้");

                if (string.IsNullOrEmpty(_receivingCode))
                    throw new ArgumentException("โปรดระบุ receiving code ของกระสอบที่ได้รับที่อยู่ใน stock");

                if (Convert.ToInt16(_quantity) <= 0)
                    throw new ArgumentException("จำนวนกระสอบที่จะจ่าย ต้องมากกว่า 1 ขึ้นไป");

                if (_document == null)
                    throw new ArgumentException("ไม่พบรหัสเวาเชอร์ของชาวไร่ที่จะนำไปเบิกกระสอบ โปรดติดต่อผู้ดูและระบบเพื่อตรวจสอบ");

                BuyingFacade.HessianDistributionBL()
                    .Add(new HessianDistribution
                    {
                        DistributionID = Guid.NewGuid(),
                        FarmerCode = _farmer.FarmerCode,
                        Quantity = _quantity,
                        ReceivingCode = _receivingCode,
                        ModifiedBy = user_setting.User.Username,
                        ModifiedDate = DateTime.Now,
                        Crop = user_setting.Crop.Crop1,
                        DistributedBy = user_setting.User.Username,
                        DistributedDate = _distributionDate
                    });

                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditCommand;

        public ICommand OnEditCommand
        {
            get { return _onEditCommand ?? (_onEditCommand = new RelayCommand(OnEdit)); }
            set { _onEditCommand = value; }
        }

        private void OnEdit(object obj)
        {
            try
            {
                if (_distribution == null)
                    throw new ArgumentException("ไม่พบข้อมูลที่ต้องการแก้ไข โปรดคลิกเลือกจากแถวข้อมูลที่ต้องการแก้ไขอีกครั้ง");

                if (string.IsNullOrEmpty(_farmer.FarmerCode))
                    throw new ArgumentException("โปรดระบุ farmer code ของชาวไร่ที่จะจ่ายกระสอบให้");

                if (string.IsNullOrEmpty(_receivingCode))
                    throw new ArgumentException("โปรดระบุ receiving code ของกระสอบที่ได้รับที่อยู่ใน stock");

                if (_quantity <= 0)
                    throw new ArgumentException("จำนวนกระสอบที่จะจ่าย ต้องมากกว่า 1 ขึ้นไป");

                _distribution.DistributedBy = user_setting.User.Username;
                _distribution.DistributedDate = _distributionDate;
                _distribution.ModifiedBy = user_setting.User.Username;
                _distribution.ModifiedDate = DateTime.Now;
                _distribution.ReceivingCode = _receivingCode;
                _distribution.FarmerCode = _farmer.FarmerCode;
                _distribution.Quantity = _quantity;

                BuyingFacade.HessianDistributionBL().Update(_distribution, _document);
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onResetCommand;

        public ICommand OnResetCommand
        {
            get { return _onResetCommand ?? (_onResetCommand = new RelayCommand(OnReset)); }
            set { _onResetCommand = value; }
        }

        private void OnReset(object obj)
        {
            Clear();
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                var model = (HessianDistribution)obj;
                BuyingFacade.HessianDistributionBL().Delete(model.DistributionID);
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditSelectedCommand;

        public ICommand OnEditSelectedCommand
        {
            get { return _onEditSelectedCommand ?? (_onEditSelectedCommand = new RelayCommand(OnEditSelected)); }
            set { _onEditSelectedCommand = value; }
        }

        private void OnEditSelected(object obj)
        {
            try
            {
                _distribution = (HessianDistribution)obj;

                _visibleAdd = Visibility.Collapsed;
                _visibleEdit = Visibility.Visible;

                RaisePropertyChangedEvent(nameof(Distribution));
                RaisePropertyChangedEvent(nameof(VisibleAdd));
                RaisePropertyChangedEvent(nameof(VisibleEdit));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onSearchCommand;

        public ICommand OnSearchCommand
        {
            get { return _onSearchCommand ?? (_onSearchCommand = new RelayCommand(OnSearch)); }
            set { _onSearchCommand = value; }
        }

        private void OnSearch(object obj)
        {
            View.Hessian.FarmerList window = new View.Hessian.FarmerList();
            var vm = new vm_FarmerList(window);
            window.DataContext = vm;
            window.ShowDialog();

            _farmer = vm.Farmer;
            RaisePropertyChangedEvent(nameof(Farmer));
        }

        private ICommand _onDetailsCommand;

        public ICommand OnDetailsCommand
        {
            get { return _onDetailsCommand ?? (_onDetailsCommand = new RelayCommand(OnDetails)); }
            set { _onDetailsCommand = value; }
        }

        private void OnDetails(object obj)
        {
            if (_farmer == null)
            {
                MessageBoxHelper.Warning("โปรดระบุ Farmer Code");
                OnFocusRequested(nameof(Farmer.FarmerCode));
                return;
            }

            View.Hessian.FarmerInfo window = new View.Hessian.FarmerInfo();
            var vm = new vm_FarmerInfo();
            window.DataContext = vm;
            vm.FarmerCode = _farmer.FarmerCode;
            window.ShowDialog();
        }
        #endregion



        #region Fucntion

        private void Clear()
        {
            DistributionListBinding();
            WarehouseSummaryBinding();
            ReceivingListBinding();

            _quantity = 0;
            _farmer = null;
            _distribution = null;

            _visibleAdd = Visibility.Visible;
            _visibleEdit = Visibility.Collapsed;

            RaisePropertyChangedEvent(nameof(Farmer));
            RaisePropertyChangedEvent(nameof(Quantity));
            RaisePropertyChangedEvent(nameof(VisibleAdd));
            RaisePropertyChangedEvent(nameof(VisibleEdit));
            RaisePropertyChangedEvent(nameof(Distribution));
        }

        private void DistributionListBinding()
        {
            _distributionList = BuyingFacade.HessianDistributionBL()
                .GetByDistributionDate(user_setting.User.StaffUser.AreaCode, _distributionDate);
            _totalRecord = _distributionList.Count();

            RaisePropertyChangedEvent(nameof(DistributionList));
            RaisePropertyChangedEvent(nameof(TotalRecord));
        }

        private void ReceivingListBinding()
        {
            _receivingList = HessianReceivingHelper
                     .GetByWarehouse(user_setting.Crop.Crop1, _warehouseID)
                     .Where(x => x.OnHand > 0)
                     .ToList();

            RaisePropertyChangedEvent(nameof(ReceivingList));
        }

        private void WarehouseSummaryBinding()
        {
            var list = HessianReceivingHelper
                .GetByWarehouse(user_setting.Crop.Crop1, _warehouseID);

            _received = list.Sum(x => x.Quantity);
            _issued = list.Sum(x => x.Issued);
            _onHand = list.Sum(x => x.OnHand);

            RaisePropertyChangedEvent(nameof(Received));
            RaisePropertyChangedEvent(nameof(Issued));
            RaisePropertyChangedEvent(nameof(OnHand));
        }
        #endregion
    }
}
