﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using System.Windows.Input;
using DomainModel;
using BusinessLayer;
using BUWinApp.Helper;

namespace BUWinApp.ViewModel.Hessian
{
    public class vm_FarmerList : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }
        
        View.Hessian.FarmerList _window;

        public vm_FarmerList(View.Hessian.FarmerList window)
        {
            _window = window;
        }


        #region Properties
        private Farmer _farmer;

        public Farmer Farmer
        {
            get { return _farmer; }
            set { _farmer = value; }
        }


        private string _farmerName;

        public string FarmerName
        {
            get { return _farmerName; }
            set { _farmerName = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }
        #endregion


        #region List
        private List<Farmer> _farmerList;

        public List<Farmer> FarmerList
        {
            get { return _farmerList; }
            set { _farmerList = value; }
        }

        #endregion


        #region Command
        private ICommand _onSearchCommand;

        public ICommand OnSearchCommand
        {
            get { return _onSearchCommand ?? (_onSearchCommand = new RelayCommand(OnSearch)); }
            set { _onSearchCommand = value; }
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(OnClear)); }
            set { _onClearCommand = value; }
        }

        private ICommand _onSelectCommand;

        public ICommand OnSelectCommand
        {
            get { return _onSelectCommand ?? (_onSelectCommand = new RelayCommand(OnSelect)); }
            set { _onSelectCommand = value; }
        }

        private ICommand _onDetailsCommand;

        public ICommand OnDetailsCommand
        {
            get { return _onDetailsCommand ?? (_onDetailsCommand = new RelayCommand(OnDetails)); }
            set { _onDetailsCommand = value; }
        }
        #endregion


        #region Function
        private void OnSearch(object obj)
        {
            if(string.IsNullOrEmpty(_farmerName))
            {
                MessageBoxHelper.Warning("โปรดกรอกชื่อชาวไร่ที่ต้องการค้นหา");
                OnFocusRequested(nameof(FarmerName));
                return;
            }

            FarmerListDataBinding();
        }

        private void OnClear(object obj)
        {
            _farmerName = "";

            RaisePropertyChangedEvent(nameof(FarmerName));
            OnFocusRequested(nameof(FarmerName));
            FarmerListDataBinding();
        }

        private void OnSelect(object obj)
        {
            var model = (Farmer)obj;
            if (model == null)
                return;

            _farmer = model;
            _window.Close();
        }

        private void OnDetails(object obj)
        {
            var model = (Farmer)obj;
            if (model == null)
                return;

            View.Hessian.FarmerInfo window = new View.Hessian.FarmerInfo();
            var vm = new vm_FarmerInfo();
            window.DataContext = vm;
            vm.FarmerCode = _farmer.FarmerCode;
            window.ShowDialog();
        }

        private void FarmerListDataBinding()
        {
            _farmerList = BuyingFacade.FarmerBL()
                    .GetByFarmerName(_farmerName)
                    .Where(x => x.Supplier1.SupplierArea == user_setting.User.StaffUser.AreaCode)
                    .ToList();
            _totalRecord = _farmerList.Count();

            if (_totalRecord == 0)
                MessageBoxHelper.Info("ไม่พบชาวไร่ชื่อ " + _farmerName + " ในเขตพื้นที่รับผิดชอบของ " +
                    user_setting.User.StaffUser.Area.AreaName);

            RaisePropertyChangedEvent(nameof(FarmerList));
            RaisePropertyChangedEvent(nameof(TotalRecord));
        }
        #endregion
    }
}
