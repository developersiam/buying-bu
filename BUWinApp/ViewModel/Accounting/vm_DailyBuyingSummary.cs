﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using DomainModel;
using BUWinApp.Helper;
using BusinessLayer;
using System.Windows.Input;
using BUWinApp.View.BuyingStaff;
using BUWinApp.ViewModel.BuyingStaff;
using BusinessLayer.Model;

namespace BUWinApp.ViewModel.Accounting
{
    public class vm_DailyBuyingSummary : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_DailyBuyingSummary()
        {
            _buyingDate = DateTime.Now;
            _extensionAgentList = BuyingFacade.SupplierBL().GetAll();
            RaisePropertyChangedEvent(nameof(ExtensionAgentList));
        }


        #region Properties
        private DateTime _buyingDate;

        public DateTime BuyingDate
        {
            get { return _buyingDate; }
            set
            {
                _buyingDate = value;
                DocumentListBinding();
            }
        }

        private string _extensionAgentCode;

        public string ExtensionAgentCode
        {
            get { return _extensionAgentCode; }
            set
            {
                _extensionAgentCode = value;
                DocumentListBinding();
            }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private bool _isSuccess;

        public bool IsSuccess
        {
            get { return _isSuccess; }
            set
            {
                _isSuccess = value;

                if (_isSuccess == true)
                {
                    DocumentListBinding();

                    _documentList = _documentList.Where(x => x.Type == "G").ToList();
                    _totalRecord = _documentList.Count();
                    _isWarning = false;
                    _isDanger = false;
                    _isShowAll = false;

                    RaisePropertyChangedEvent(nameof(DocumentList));
                    RaisePropertyChangedEvent(nameof(TotalRecord));
                    RaisePropertyChangedEvent(nameof(IsWarning));
                    RaisePropertyChangedEvent(nameof(IsDanger));
                    RaisePropertyChangedEvent(nameof(IsShowAll));
                }
            }
        }

        private bool _isWarning;

        public bool IsWarning
        {
            get { return _isWarning; }
            set
            {
                _isWarning = value;
                if (_isWarning == true)
                {
                    DocumentListBinding();

                    _documentList = _documentList.Where(x => x.Type == "Y").ToList();
                    _totalRecord = _documentList.Count();
                    _isSuccess = false;
                    _isDanger = false;
                    _isShowAll = false;

                    RaisePropertyChangedEvent(nameof(DocumentList));
                    RaisePropertyChangedEvent(nameof(TotalRecord));
                    RaisePropertyChangedEvent(nameof(IsSuccess));
                    RaisePropertyChangedEvent(nameof(IsDanger));
                    RaisePropertyChangedEvent(nameof(IsShowAll));
                }
            }
        }

        private bool _isDanger;

        public bool IsDanger
        {
            get { return _isDanger; }
            set
            {
                _isDanger = value;
                if (_isDanger == true)
                {
                    DocumentListBinding();

                    _documentList = _documentList.Where(x => x.Type == "R").ToList();
                    _totalRecord = _documentList.Count();
                    _isSuccess = false;
                    _isWarning = false;
                    _isShowAll = false;

                    RaisePropertyChangedEvent(nameof(DocumentList));
                    RaisePropertyChangedEvent(nameof(TotalRecord));
                    RaisePropertyChangedEvent(nameof(IsSuccess));
                    RaisePropertyChangedEvent(nameof(IsWarning));
                    RaisePropertyChangedEvent(nameof(IsShowAll));
                }
            }
        }

        private bool _isShowAll;

        public bool IsShowAll
        {
            get { return _isShowAll; }
            set
            {
                _isShowAll = value;

                _isSuccess = false;
                _isWarning = false;
                _isDanger = false;

                DocumentListBinding();

                RaisePropertyChangedEvent(nameof(IsSuccess));
                RaisePropertyChangedEvent(nameof(IsWarning));
                RaisePropertyChangedEvent(nameof(IsDanger));
            }
        }
        #endregion



        #region List
        private List<m_ReceivingPerBuyingDocument> _documentList;

        public List<m_ReceivingPerBuyingDocument> DocumentList
        {
            get { return _documentList; }
            set
            {
                _documentList = value;
                _totalRecord = _documentList.Count();
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
        }

        private List<Supplier> _extensionAgentList;

        public List<Supplier> ExtensionAgentList
        {
            get { return _extensionAgentList; }
            set { _extensionAgentList = value; }
        }
        #endregion



        #region Command
        private ICommand _onRefreshCommand;

        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(OnRefresh)); }
            set { _onRefreshCommand = value; }
        }

        private void OnRefresh(object obj)
        {
            DocumentListBinding();
        }

        private ICommand _onDetailsCommand;

        public ICommand OnDetailsCommand
        {
            get { return _onDetailsCommand ?? (_onDetailsCommand = new RelayCommand(OnDeltails)); }
            set { _onDetailsCommand = value; }
        }

        private void OnDeltails(object obj)
        {
            try
            {
                var item = (sp_Receiving_SEL_BUBuyingAndReceivedPerDay_Result)obj;
                if (item == null)
                    throw new ArgumentException("Get data grid record error,Please contact to administrator.");

                var payment = BuyingFacade.PaymentHistoryBL()
                    .GetSingle(item.Crop,
                    item.BuyingStationCode,
                    item.FarmerCode,
                    item.BuyingDocumentNumber);

                if (payment == null)
                    throw new ArgumentException("find payment record not found,Please contact to administrator.");

                if (payment.IsFinish == true &&
                    user_setting.UserRoles.Where(x => x.RoleName == "Accounting")
                    .Count() <= 0)
                    throw new ArgumentException("ใบเวาเชอร์นี้มีการยืนยันการหักชำระหนี้จากทางแผนกบัญชีแล้ว ไม่สามารถคำนวนยอดหนี้เพื่อหักชำระใหม่ได้อีก");

                OutstandingDebtCalculation window = new OutstandingDebtCalculation();
                var vm = new vm_OutstandingDebtCalculation();
                window.DataContext = vm;
                vm.Document = new m_BuyingDocument
                {
                    DocumentCode = item.DocumentCode,
                    Crop = item.Crop,
                    BuyingStationCode = item.BuyingStationCode,
                    FarmerCode = item.FarmerCode,
                    BuyingDocumentNumber = item.BuyingDocumentNumber
                };

                window.ShowDialog();
                DocumentListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onFinishAllCommand;

        public ICommand OnFinishAllCommand
        {
            get { return _onFinishAllCommand ?? (_onFinishAllCommand = new RelayCommand(OnFinishAll)); }
            set { _onFinishAllCommand = value; }
        }

        private void OnFinishAll(object obj)
        {
            try
            {
                var documents = _documentList
                    .Where(x => x.PaymentAmount != 0 &&
                    x.IsFinish != true &&
                    x.Type == "G");

                if (documents.Count() <= 0)
                    throw new ArgumentException("ไม่พบรายการที่รอการ confirm จากท่าน โปรดตรวจสอบให้แน่ใจอีกครั้ง");

                if (MessageBoxHelper.Question("มีเวาเชอร์จำนวน " + documents.Count() +
                    " รายการที่รอการ confirm ท่านต้องการ finish เวาเชอร์เหล่านี้ใช่หรือไม่?")
                    == System.Windows.MessageBoxResult.No)
                    return;

                var counter = 0;
                foreach (var item in documents)
                {
                    BuyingFacade.PaymentHistoryBL()
                        .Finish(item.Crop,
                        item.BuyingStationCode,
                        item.FarmerCode,
                        item.BuyingDocumentNumber,
                        user_setting.User.Username);
                    counter++;
                }

                DocumentListBinding();
                MessageBoxHelper.Info("Finish " + counter + " documents already.");

                if (documents.Count() - counter > 0)
                    MessageBoxHelper.Info("You have " + (documents.Count() - counter) + " documents cannot be finish.");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onUnfinishAllCommand;

        public ICommand OnUnfinishAllCommand
        {
            get { return _onUnfinishAllCommand ?? (_onUnfinishAllCommand = new RelayCommand(OnUnfinishAll)); }
            set { _onUnfinishAllCommand = value; }
        }

        private void OnUnfinishAll(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("Do you want to unfinish " + _documentList.Where(x => x.IsFinish == true).Count() + " documents?")
                    == System.Windows.MessageBoxResult.No)
                    return;

                var counter = 0;

                foreach (var item in _documentList
                    .Where(x => x.IsFinish == true))
                {
                    BuyingFacade.PaymentHistoryBL()
                        .UnFinish(item.Crop,
                        item.BuyingStationCode,
                        item.FarmerCode,
                        item.BuyingDocumentNumber);
                    counter++;
                }

                DocumentListBinding();
                MessageBoxHelper.Info("Unfinish " + counter + " documents already.");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onPrintCommand;

        public ICommand OnPrintCommand
        {
            get { return _onPrintCommand ?? (_onPrintCommand = new RelayCommand(OnPrintReceiptMoneyList)); }
            set { _onPrintCommand = value; }
        }

        private void OnPrintReceiptMoneyList(object obj)
        {
            try
            {
                System.Collections.IList items = (System.Collections.IList)obj;
                var collection = items.Cast<m_ReceivingPerBuyingDocument>().ToList();

                if (collection.Where(x => x.Type != "G").Count() > 0)
                    throw new ArgumentException("มีจำนวน " +
                        collection.Where(x => x.Type != "G").Count() + " รายการที่ไม่ถุกต้องโปรดตรวจสอบอีกครั้ง");

                foreach (var item in collection)
                    ReportHelper.PrintReceiptOfMoney(item, 2);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private ICommand _onPrintVoucherCommand;

        public ICommand OnPrintVoucherCommand
        {
            get { return _onPrintVoucherCommand ?? (_onPrintVoucherCommand = new RelayCommand(OnPrintVoucher)); }
            set { _onPrintVoucherCommand = value; }
        }

        private void OnPrintVoucher(object obj)
        {
            try
            {
                ReportHelper.PrintBuyingVoucher((m_ReceivingPerBuyingDocument)obj, 3);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private ICommand _onPrintReceiptMoneyCommand;

        public ICommand OnPrintReceiptMoneyCommand
        {
            get { return _onPrintReceiptMoneyCommand ?? (_onPrintReceiptMoneyCommand = new RelayCommand(OnPrintReceiptMoney)); }
            set { _onPrintReceiptMoneyCommand = value; }
        }

        private void OnPrintReceiptMoney(object obj)
        {
            try
            {
                var model = (m_ReceivingPerBuyingDocument)obj;
                if (model == null)
                    return;

                if (model.IsFinish != true)
                    throw new ArgumentException("This document not have to finish from the account dept." +
                        " the system cannot be print.");

                ReportHelper.PrintReceiptOfMoney(model, 2);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion



        #region Function
        private void DocumentListBinding()
        {
            try
            {
                if (_buyingDate == null)
                    return;

                if (string.IsNullOrEmpty(_extensionAgentCode))
                    return;

                _documentList = BuyingFacade.AccountingAndReceivingBL()
                    .GetByBuyingDate(_buyingDate, _extensionAgentCode);
                _totalRecord = _documentList.Count();

                RaisePropertyChangedEvent(nameof(DocumentList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
