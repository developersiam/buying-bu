﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using BusinessLayer;
using BusinessLayer.Model;
using DomainModel;
using System.Windows.Input;
using BUWinApp.Helper;
using BUWinApp.View.BuyingStaff;
using BUWinApp.ViewModel.BuyingStaff;

namespace BUWinApp.ViewModel.Accounting
{
    public class vm_DailyReceivingSummary : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_DailyReceivingSummary()
        {
            _receivedDate = DateTime.Now;
            _extensionAgentList = BuyingFacade.SupplierBL().GetAll();
            RaisePropertyChangedEvent(nameof(ExtensionAgentList));
        }


        #region Properties
        private DateTime _receivedDate;

        public DateTime ReceivedDate
        {
            get { return _receivedDate; }
            set
            {
                _receivedDate = value;
                DocumentListBinding();
            }
        }

        private string _extensionAgentCode;

        public string ExtensionAgentCode
        {
            get { return _extensionAgentCode; }
            set
            {
                _extensionAgentCode = value;
                DocumentListBinding();
            }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private bool _isSuccess;

        public bool IsSuccess
        {
            get { return _isSuccess; }
            set
            {
                _isSuccess = value;

                if (_isSuccess == true)
                {
                    DocumentListBinding();

                    if (_documentList == null)
                        return;

                    _documentList = _documentList.Where(x => x.Type == "G").ToList();
                    _isWarning = false;
                    _isDanger = false;
                    _isShowAll = false;

                    _totalRecord = _documentList.Count();
                    _totalBuyingBale = (int)_documentList.Sum(x => x.BuyingBale);
                    _totalReceivedBale = (int)_documentList.Sum(x => x.ReceivedBale);

                    RaisePropertyChangedEvent(nameof(TotalRecord));
                    RaisePropertyChangedEvent(nameof(TotalBuyingBale));
                    RaisePropertyChangedEvent(nameof(TotalReceivedBale));

                    RaisePropertyChangedEvent(nameof(DocumentList));
                    RaisePropertyChangedEvent(nameof(IsWarning));
                    RaisePropertyChangedEvent(nameof(IsDanger));
                    RaisePropertyChangedEvent(nameof(IsShowAll));
                }
            }
        }

        private bool _isWarning;

        public bool IsWarning
        {
            get { return _isWarning; }
            set
            {
                _isWarning = value;
                if (_isWarning == true)
                {
                    DocumentListBinding();

                    if (_documentList == null)
                        return;

                    _documentList = _documentList.Where(x => x.Type == "Y").ToList();
                    _isSuccess = false;
                    _isDanger = false;
                    _isShowAll = false;

                    _totalRecord = _documentList.Count();
                    _totalBuyingBale = (int)_documentList.Sum(x => x.BuyingBale);
                    _totalReceivedBale = (int)_documentList.Sum(x => x.ReceivedBale);

                    RaisePropertyChangedEvent(nameof(TotalRecord));
                    RaisePropertyChangedEvent(nameof(TotalBuyingBale));
                    RaisePropertyChangedEvent(nameof(TotalReceivedBale));

                    RaisePropertyChangedEvent(nameof(DocumentList));
                    RaisePropertyChangedEvent(nameof(IsSuccess));
                    RaisePropertyChangedEvent(nameof(IsDanger));
                    RaisePropertyChangedEvent(nameof(IsShowAll));
                }
            }
        }

        private bool _isDanger;

        public bool IsDanger
        {
            get { return _isDanger; }
            set
            {
                _isDanger = value;
                if (_isDanger == true)
                {
                    DocumentListBinding();

                    if (_documentList == null)
                        return;

                    _documentList = _documentList.Where(x => x.Type == "R").ToList();
                    _isSuccess = false;
                    _isWarning = false;
                    _isShowAll = false;

                    _totalRecord = _documentList.Count();
                    _totalBuyingBale = (int)_documentList.Sum(x => x.BuyingBale);
                    _totalReceivedBale = (int)_documentList.Sum(x => x.ReceivedBale);

                    RaisePropertyChangedEvent(nameof(TotalRecord));
                    RaisePropertyChangedEvent(nameof(TotalBuyingBale));
                    RaisePropertyChangedEvent(nameof(TotalReceivedBale));

                    RaisePropertyChangedEvent(nameof(DocumentList));
                    RaisePropertyChangedEvent(nameof(IsSuccess));
                    RaisePropertyChangedEvent(nameof(IsWarning));
                    RaisePropertyChangedEvent(nameof(IsShowAll));
                }
            }
        }

        private bool _isShowAll;

        public bool IsShowAll
        {
            get { return _isShowAll; }
            set
            {
                _isShowAll = value;

                _isSuccess = false;
                _isWarning = false;
                _isDanger = false;

                DocumentListBinding();

                RaisePropertyChangedEvent(nameof(IsSuccess));
                RaisePropertyChangedEvent(nameof(IsWarning));
                RaisePropertyChangedEvent(nameof(IsDanger));
            }
        }

        private int _totalBuyingBale;

        public int TotalBuyingBale
        {
            get { return _totalBuyingBale; }
            set { _totalBuyingBale = value; }
        }

        private int _totalReceivedBale;

        public int TotalReceivedBale
        {
            get { return _totalReceivedBale; }
            set { _totalReceivedBale = value; }
        }
        #endregion



        #region List
        private List<m_AccountCheckList> _documentList;

        public List<m_AccountCheckList> DocumentList
        {
            get { return _documentList; }
            set { _documentList = value; }
        }

        private List<Supplier> _extensionAgentList;

        public List<Supplier> ExtensionAgentList
        {
            get { return _extensionAgentList; }
            set { _extensionAgentList = value; }
        }
        #endregion



        #region Command
        private ICommand _onRefreshCommand;

        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(OnRefresh)); }
            set { _onRefreshCommand = value; }
        }

        private void OnRefresh(object obj)
        {
            DocumentListBinding();
        }

        private ICommand _onDetailsCommand;

        public ICommand OnDetailsCommand
        {
            get { return _onDetailsCommand ?? (_onDetailsCommand = new RelayCommand(OnDeltails)); }
            set { _onDetailsCommand = value; }
        }

        private void OnDeltails(object obj)
        {
            try
            {
                var item = (m_AccountCheckList)obj;
                if (item == null)
                    throw new ArgumentException("Get data grid record error,Please contact to administrator.");

                var payment = BuyingFacade.PaymentHistoryBL()
                    .GetSingle(Convert.ToInt16(item.Crop),
                    item.BuyingStationCode,
                    item.FarmerCode,
                    Convert.ToInt16(item.BuyingDocumentNumber));

                if (payment == null)
                    throw new ArgumentException("ไม่พบข้อมูลการหักชำระหนี้ โปรดตรวจสอบอีกครั้ง.");

                if (payment.IsFinish == true &&
                    user_setting.UserRoles.Where(x => x.RoleName == "Accounting")
                    .Count() <= 0)
                    throw new ArgumentException("ใบเวาเชอร์นี้มีการยืนยันการหักชำระหนี้จากทางแผนกบัญชีแล้ว ไม่สามารถคำนวนยอดหนี้เพื่อหักชำระใหม่ได้อีก");

                OutstandingDebtCalculation window = new OutstandingDebtCalculation();
                var vm = new vm_OutstandingDebtCalculation();
                window.DataContext = vm;
                vm.Document = new m_BuyingDocument
                {
                    Crop = Convert.ToInt16(item.Crop),
                    BuyingStationCode = item.BuyingStationCode,
                    FarmerCode = item.FarmerCode,
                    BuyingDocumentNumber = Convert.ToInt16(item.BuyingDocumentNumber)
                };

                window.ShowDialog();
                DocumentListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onFinishAllCommand;

        private void OnFinishAll(object obj)
        {
            try
            {
                var documents = _documentList
                    .Where(x => x.IsAccountFinish != true &&
                    x.Type == "G")
                    .ToList();

                if (documents.Count() <= 0)
                    throw new ArgumentException("ไม่พบรายการที่รอการ confirm จากท่าน โปรดตรวจสอบให้แน่ใจอีกครั้ง");

                if (MessageBoxHelper.Question("มีเวาเชอร์จำนวน " + documents.Count() +
                    " รายการที่รอการ confirm ท่านต้องการ finish เวาเชอร์เหล่านี้ใช่หรือไม่?")
                    == System.Windows.MessageBoxResult.No)
                    return;

                var counter = 0;
                foreach (var item in documents)
                {
                    BuyingFacade.BuyingDocumentBL().AccountFinish(item, user_setting.User.Username);
                    counter++;
                }

                DocumentListBinding();
                MessageBoxHelper.Info("Finish " + counter + " documents already.");

                if (documents.Count() - counter > 0)
                    MessageBoxHelper.Info("You have " + (documents.Count() - counter) + " documents cannot be finish.");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public ICommand OnFinishAllCommand
        {
            get { return _onFinishAllCommand ?? (_onFinishAllCommand = new RelayCommand(OnFinishAll)); }
            set { _onFinishAllCommand = value; }
        }

        private ICommand _onUnfinishAllCommand;

        public ICommand OnUnfinishAllCommand
        {
            get { return _onUnfinishAllCommand ?? (_onUnfinishAllCommand = new RelayCommand(OnUnfinishAll)); }
            set { _onUnfinishAllCommand = value; }
        }

        private void OnUnfinishAll(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("Do you want to unfinish " +
                    _documentList.Where(x => x.IsAccountFinish == true).Count() + " documents?")
                    == System.Windows.MessageBoxResult.No)
                    return;

                var counter = 0;
                foreach (var item in _documentList
                    .Where(x => x.IsAccountFinish == true))
                {
                    BuyingFacade.BuyingDocumentBL().AccountUnfinish(item, user_setting.User.Username);
                    counter++;
                }

                DocumentListBinding();
                MessageBoxHelper.Info("Unfinish " + counter + " documents already.");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion



        #region Function
        private void DocumentListBinding()
        {
            try
            {
                if (_receivedDate == null)
                    return;

                if (string.IsNullOrEmpty(_extensionAgentCode))
                    return;

                _documentList = BuyingFacade.AccountingAndReceivingBL()
                    .GetSummaryByReceivedDate(_receivedDate, _extensionAgentCode);
                _totalRecord = _documentList.Count();
                _totalBuyingBale = (int)_documentList.Sum(x => x.BuyingBale);
                _totalReceivedBale = (int)_documentList.Sum(x => x.ReceivedBale);

                RaisePropertyChangedEvent(nameof(TotalRecord));
                RaisePropertyChangedEvent(nameof(TotalBuyingBale));
                RaisePropertyChangedEvent(nameof(TotalReceivedBale));
                RaisePropertyChangedEvent(nameof(DocumentList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
