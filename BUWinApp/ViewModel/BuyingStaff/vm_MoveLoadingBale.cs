﻿using BusinessLayer;
using BUWinApp.Helper;
using BUWinApp.MVVM;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Windows.Media;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace BUWinApp.ViewModel.BuyingStaff
{
    public class vm_MoveLoadingBale : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_MoveLoadingBale()
        {
            _sourceStatusText = "N/A";
            _sourceStatusBGColor = Brushes.White;
            _sourceFinishButtonVisibility = Visibility.Collapsed;
            _sourceUnFinishButtonVisibility = Visibility.Collapsed;
            _sourcePrintButtonVisibility = Visibility.Collapsed;
            _destinationStatusText = "N/A";
            _destinationStatusBGColor = Brushes.White;
            _destinationFinishButtonVisibility = Visibility.Collapsed;
            _destinationUnFinishButtonVisibility = Visibility.Collapsed;
            _destinationPrintButtonVisibility = Visibility.Collapsed;
            _selectedItemSourceList = new List<Buying>();
            _selectedItemDestinationList = new List<Buying>();

            RaisePropertyChangedEvent(nameof(SourceStatusText));
            RaisePropertyChangedEvent(nameof(SourceStatusBGColor));
            RaisePropertyChangedEvent(nameof(SourceFinishButtonVisibility));
            RaisePropertyChangedEvent(nameof(SourceUnFinishButtonVisibility));
            RaisePropertyChangedEvent(nameof(SourcePrintButtonVisibility));
            RaisePropertyChangedEvent(nameof(DestinationStatusText));
            RaisePropertyChangedEvent(nameof(DestinationStatusBGColor));
            RaisePropertyChangedEvent(nameof(DestinationFinishButtonVisibility));
            RaisePropertyChangedEvent(nameof(DestinationUnFinishButtonVisibility));
            RaisePropertyChangedEvent(nameof(DestinationPrintButtonVisibility));
            RaisePropertyChangedEvent(nameof(SelectedItemSourceList));
            RaisePropertyChangedEvent(nameof(SelectedItemDestinationList));

            TransportationCodeListBinding();
        }


        #region Properties
        private string _transportationCodeSource;

        public string TransportationCodeSource
        {
            get { return _transportationCodeSource; }
            set
            {
                _transportationCodeSource = value;
                SourceListBinding();
                SourceCodeChanged();
            }
        }

        private string _transportationCodeDestination;

        public string TransportationCodeDestination
        {
            get { return _transportationCodeDestination; }
            set
            {
                _transportationCodeDestination = value;
                DestinationListBinding();
                DestinationCodeChanged();
            }
        }

        private int _selectedItemSource;

        public int SelectedItemSource
        {
            get { return _selectedItemSource; }
            set { _selectedItemSource = value; }
        }

        private int _selectedItemDestination;

        public int SelectedItemDestination
        {
            get { return _selectedItemDestination; }
            set { _selectedItemDestination = value; }
        }

        private int _totalBaleSource;

        public int TotalBaleSource
        {
            get { return _totalBaleSource; }
            set { _totalBaleSource = value; }
        }

        private int _totalBaleDestination;

        public int TotalBaleDestination
        {
            get { return _totalBaleDestination; }
            set { _totalBaleDestination = value; }
        }

        private Brush _sourceStatusBGColor;

        public Brush SourceStatusBGColor
        {
            get { return _sourceStatusBGColor; }
            set { _sourceStatusBGColor = value; }
        }

        private string _sourceStatusText;

        public string SourceStatusText
        {
            get { return _sourceStatusText; }
            set { _sourceStatusText = value; }
        }

        private Brush _destinationStatusBGColor;

        public Brush DestinationStatusBGColor
        {
            get { return _destinationStatusBGColor; }
            set { _destinationStatusBGColor = value; }
        }

        private string _destinationStatusText;

        public string DestinationStatusText
        {
            get { return _destinationStatusText; }
            set { _destinationStatusText = value; }
        }

        private bool _sourceStatus;

        public bool SourceStatus
        {
            get { return _sourceStatus; }
            set { _sourceStatus = value; }
        }

        private bool _destinationStatus;

        public bool DestinationStatus
        {
            get { return _destinationStatus; }
            set { _destinationStatus = value; }
        }

        private Visibility _sourceFinishButtonVisibility;

        public Visibility SourceFinishButtonVisibility
        {
            get { return _sourceFinishButtonVisibility; }
            set { _sourceFinishButtonVisibility = value; }
        }

        private Visibility _sourceUnFinishButtonVisibility;

        public Visibility SourceUnFinishButtonVisibility
        {
            get { return _sourceUnFinishButtonVisibility; }
            set { _sourceUnFinishButtonVisibility = value; }
        }

        private Visibility _destinationFinishButtonVisibility;

        public Visibility DestinationFinishButtonVisibility
        {
            get { return _destinationFinishButtonVisibility; }
            set { _destinationFinishButtonVisibility = value; }
        }

        private Visibility _destinationUnFinishButtonVisibility;

        public Visibility DestinationUnFinishButtonVisibility
        {
            get { return _destinationUnFinishButtonVisibility; }
            set { _destinationUnFinishButtonVisibility = value; }
        }

        private Visibility _sourcePrintButtonVisibility;

        public Visibility SourcePrintButtonVisibility
        {
            get { return _sourcePrintButtonVisibility; }
            set { _sourcePrintButtonVisibility = value; }
        }

        private Visibility _destinationPrintButtonVisibility;

        public Visibility DestinationPrintButtonVisibility
        {
            get { return _destinationPrintButtonVisibility; }
            set { _destinationPrintButtonVisibility = value; }
        }
        #endregion


        #region List
        private List<Buying> _sourceList;

        public List<Buying> SourceList
        {
            get { return _sourceList; }
            set { _sourceList = value; }
        }

        private List<Buying> _destinationList;

        public List<Buying> DestinationList
        {
            get { return _destinationList; }
            set { _destinationList = value; }
        }

        private List<Buying> _selectedItemSourceList;

        public List<Buying> SelectedItemSourceList
        {
            get { return _selectedItemSourceList; }
            set { _selectedItemSourceList = value; }
        }

        private List<Buying> _selectedItemDestinationList;

        public List<Buying> SelectedItemDestinationList
        {
            get { return _selectedItemDestinationList; }
            set { _selectedItemDestinationList = value; }
        }

        private List<TransportationDocument> _transportationDocList;

        public List<TransportationDocument> TransportationDocList
        {
            get { return _transportationDocList; }
            set { _transportationDocList = value; }
        }
        #endregion


        #region Command
        private ICommand _onSourceFinishCommand;

        public ICommand OnSourceFinishCommand
        {
            get { return _onSourceFinishCommand ?? (_onSourceFinishCommand = new RelayCommand(OnSourchFinish)); }
            set { _onSourceFinishCommand = value; }
        }

        private void OnSourchFinish(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_transportationCodeSource))
                {
                    MessageBoxHelper.Warning("โปรดระบุ transport code ต้นทาง");
                    OnFocusRequested(nameof(TransportationCodeSource));
                    return;
                }

                if (MessageBoxHelper.Question("ท่านต้องการล็อคใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                BuyingFacade.TransportationBL()
                    .Finish(_transportationCodeSource, user_setting.User.Username);
                SourceCodeChanged();
                MessageBoxHelper.Warning("โปรดพิมพ์เอกสารใบนำส่งฉบับแก้ไขล่าสุดให้ทางคนขับรถบรรทุกเพื่อมอบให้แผนกใบยาดิบอีกครั้ง");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onSourceUnFinishCommand;

        public ICommand OnSourceUnFinishCommand
        {
            get { return _onSourceUnFinishCommand ?? (_onSourceUnFinishCommand = new RelayCommand(OnSourceUnFinish)); }
            set { _onSourceUnFinishCommand = value; }
        }

        private void OnSourceUnFinish(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_transportationCodeSource))
                {
                    MessageBoxHelper.Warning("โปรดระบุ transport code ต้นทาง");
                    OnFocusRequested(nameof(TransportationCodeSource));
                    return;
                }

                if (MessageBoxHelper.Question("การปลดล็อคนี้อาจส่งผลกระทบต่อแผนกใบยาดิบในการจ่ายเงินค่าขนส่งในภายหลัง" +
                    " ท่านต้องการปลดล็อคใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                BuyingFacade.TransportationBL()
                    .Unfinish(_transportationCodeSource, user_setting.User.Username);
                SourceCodeChanged();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onSourceMoveCommand;

        public ICommand OnSourceMoveCommand
        {
            get { return _onSourceMoveCommand ?? (_onSourceUnFinishCommand = new RelayCommand(OnSourceMove)); }
            set { _onSourceMoveCommand = value; }
        }

        private void OnSourceMove(object obj)
        {
            try
            {
                if (_transportationCodeSource == _transportationCodeDestination)
                    throw new ArgumentException("ไม่สามารถย้ายยาไปคันรถเดียวกันได้ โปรดเลือกคันรถที่แตกต่างกัน");

                if (_destinationStatus == true)
                    throw new ArgumentException("Transportation Code ปลายทางอยู่ในสถานะ Finish ไม่สามารถย้ายยาจากปลายทางนี้ได้");

                if (_sourceStatus == true)
                    throw new ArgumentException("Transportation Code ต้นทางอยู่ในสถานะ Finish ไม่สามารถย้ายยาจากต้นทางนี้ได้");

                if (_selectedItemSourceList.Count() <= 0)
                    throw new ArgumentException("โปรดเลือกแถวข้อมูลของห่อยาที่ต้องการย้ายอย่างน้อย 1 รายการ");

                if (MessageBoxHelper.Question("การย้ายห่อยาอาจส่งผลกระทบกับการคิดค่าขนส่งของแผนกใบยาดิบ" +
                    " ท่านต้องการย้ายห่อยาดังกล่าวใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                BuyingFacade.BuyingBL()
                    .MoveLoadingBale(_selectedItemSourceList,
                    user_setting.User.Username,
                    _transportationCodeSource,
                    _transportationCodeDestination);

                SourceCodeChanged();
                DestinationCodeChanged();
                SourceListBinding();
                DestinationListBinding();
                MessageBoxHelper.Info("ย้ายห่อยาสำเร็จ โปรดพิมพ์เอกสารให้กับคนขับรถบรรทุกอีกครั้ง" +
                    "และแจ้งไปยังแผนกใบยาดิบทุกครั้งที่มีการแก้ไขข้อมูล");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDestinationFinishCommand;

        public ICommand OnDestinationFinishCommand
        {
            get { return _onDestinationFinishCommand ?? (_onSourceUnFinishCommand = new RelayCommand(OnDestinationFinish)); }
            set { _onDestinationFinishCommand = value; }
        }

        private void OnDestinationFinish(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_transportationCodeDestination))
                {
                    MessageBoxHelper.Warning("โปรดระบุ transport code ปลายทาง");
                    OnFocusRequested(nameof(TransportationCodeDestination));
                    return;
                }

                if (MessageBoxHelper.Question("ท่านต้องการล็อคใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                BuyingFacade.TransportationBL()
                    .Finish(_transportationCodeDestination, user_setting.User.Username);
                DestinationCodeChanged();
                MessageBoxHelper.Warning("โปรดพิมพ์เอกสารใบนำส่งฉบับแก้ไขล่าสุดให้ทางคนขับรถบรรทุกเพื่อมอบให้แผนกใบยาดิบอีกครั้ง");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDestinationUnFinishCommand;

        public ICommand OnDestinationUnFinishCommand
        {
            get { return _onDestinationUnFinishCommand ?? (_onSourceUnFinishCommand = new RelayCommand(OnDestinationUnFinish)); }
            set { _onDestinationUnFinishCommand = value; }
        }

        private void OnDestinationUnFinish(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_transportationCodeDestination))
                {
                    MessageBoxHelper.Warning("โปรดระบุ transport code ปลายทาง");
                    OnFocusRequested(nameof(TransportationCodeDestination));
                    return;
                }

                if (MessageBoxHelper.Question("การปลดล็อคนี้อาจส่งผลกระทบต่อแผนกใบยาดิบในการจ่ายเงินค่าขนส่งในภายหลัง" +
                    " ท่านต้องการปลดล็อคใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                BuyingFacade.TransportationBL()
                    .Unfinish(_transportationCodeDestination, user_setting.User.Username);
                DestinationCodeChanged();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDestinationMoveCommand;

        public ICommand OnDestinationMoveCommand
        {
            get { return _onDestinationMoveCommand ?? (_onSourceUnFinishCommand = new RelayCommand(OnDestinationMove)); }
            set { _onDestinationMoveCommand = value; }
        }

        private void OnDestinationMove(object obj)
        {
            try
            {
                if (_transportationCodeSource == _transportationCodeDestination)
                    throw new ArgumentException("ไม่สามารถย้ายยาไปคันรถเดียวกันได้ โปรดเลือกคันรถที่แตกต่างกัน");

                if (_destinationStatus == true)
                    throw new ArgumentException("Transportation Code ปลายทางอยู่ในสถานะ Finish ไม่สามารถย้ายยาจากปลายทางนี้ได้");

                if (_sourceStatus == true)
                    throw new ArgumentException("Transportation Code ต้นทางอยู่ในสถานะ Finish ไม่สามารถย้ายยาจากต้นทางนี้ได้");

                if (_selectedItemDestinationList.Count() <= 0)
                    throw new ArgumentException("โปรดเลือกแถวข้อมูลของห่อยาที่ต้องการย้ายอย่างน้อย 1 รายการ");

                if (MessageBoxHelper.Question("การย้ายห่อยาอาจส่งผลกระทบกับการคิดค่าขนส่งของแผนกใบยาดิบ" +
                    " ท่านต้องการย้ายห่อยาดังกล่าวใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                BuyingFacade.BuyingBL()
                    .MoveLoadingBale(_selectedItemDestinationList,
                    user_setting.User.Username,
                    _transportationCodeDestination,
                    _transportationCodeSource);

                SourceCodeChanged();
                DestinationCodeChanged();
                SourceListBinding();
                DestinationListBinding();
                MessageBoxHelper.Info("ย้ายห่อยาสำเร็จ โปรดพิมพ์เอกสารให้กับคนขับรถบรรทุกอีกครั้ง" +
                    "และแจ้งไปยังแผนกใบยาดิบทุกครั้งที่มีการแก้ไขข้อมูล");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onSourceDataGridSelectedCommand;

        public ICommand OnSourceDataGridSelectedCommand
        {
            get { return _onSourceDataGridSelectedCommand ?? (_onSourceDataGridSelectedCommand = new RelayCommand(SourceDataGridSelected)); }
            set { _onSourceDataGridSelectedCommand = value; }
        }

        private void SourceDataGridSelected(object obj)
        {
            try
            {
                System.Collections.IList items = (System.Collections.IList)obj;
                var selectedItems = items.Cast<Buying>().ToList();

                _selectedItemSourceList = selectedItems;
                _selectedItemSource = selectedItems.Count();
                RaisePropertyChangedEvent(nameof(SelectedItemSourceList));
                RaisePropertyChangedEvent(nameof(SelectedItemSource));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDestinationDataGridSelectedCommand;

        public ICommand OnDestinationDataGridSelectedCommand
        {
            get { return _onDestinationDataGridSelectedCommand ?? (_onDestinationDataGridSelectedCommand = new RelayCommand(DestinationDataGridSelected)); }
            set { _onDestinationDataGridSelectedCommand = value; }
        }

        private void DestinationDataGridSelected(object obj)
        {
            try
            {
                System.Collections.IList items = (System.Collections.IList)obj;
                var selectedItems = items.Cast<Buying>().ToList();

                _selectedItemDestinationList = selectedItems;
                _selectedItemDestination = selectedItems.Count();
                RaisePropertyChangedEvent(nameof(SelectedItemDestinationList));
                RaisePropertyChangedEvent(nameof(SelectedItemDestination));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onSourcePrintCommand;

        public ICommand OnSourcePrintCommand
        {
            get { return _onSourcePrintCommand ?? (_onSourcePrintCommand = new RelayCommand(SourcePrint)); }
            set { _onSourcePrintCommand = value; }
        }

        private void SourcePrint(object obj)
        {
            try
            {
                if (_sourceStatus == false)
                    throw new ArgumentException("โปรดทำการล็อคใบนำส่งนี้ก่อนทำการพิมพ์เอกสาร โดยการกดปุ่ม finish ก่อน");

                PrintDocument(_transportationCodeSource);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _OnDestinationPrintCommand;

        public ICommand OnDestinationPrintCommand
        {
            get { return _OnDestinationPrintCommand ?? (_OnDestinationPrintCommand = new RelayCommand(DestinationPrint)); }
            set { _OnDestinationPrintCommand = value; }
        }

        private void DestinationPrint(object obj)
        {
            try
            {
                if (_sourceStatus == false)
                    throw new ArgumentException("โปรดทำการล็อคใบนำส่งนี้ก่อนทำการพิมพ์เอกสาร โดยการกดปุ่ม finish ก่อน");

                PrintDocument(_transportationCodeDestination);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion


        #region Function
        private void SourceListBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_transportationCodeSource))
                    return;

                _sourceList = BuyingFacade.BuyingBL()
                    .GetByTransportationCode(_transportationCodeSource)
                    .OrderByDescending(x => x.MovingTransportationDate)
                    .ThenByDescending(x => x.LoadBaleToTruckDate)
                    .ToList();
                _totalBaleSource = _sourceList.Count();
                RaisePropertyChangedEvent(nameof(SourceList));
                RaisePropertyChangedEvent(nameof(TotalBaleSource));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DestinationListBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_transportationCodeDestination))
                    return;

                _destinationList = BuyingFacade.BuyingBL()
                    .GetByTransportationCode(_transportationCodeDestination)
                    .OrderByDescending(x => x.MovingTransportationDate)
                    .ThenByDescending(x => x.LoadBaleToTruckDate)
                    .ToList();
                _totalBaleDestination = _destinationList.Count();
                RaisePropertyChangedEvent(nameof(DestinationList));
                RaisePropertyChangedEvent(nameof(TotalBaleDestination));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void TransportationCodeListBinding()
        {
            _transportationDocList = BuyingFacade.TransportationBL()
                .GetByStation(user_setting.Crop.Crop1,
                user_setting.StactionCode)
                .OrderByDescending(x => x.SequenceNumber)
                .ToList();
            var crop = user_setting.Crop.Crop1;
            var stationCode = user_setting.StactionCode;
            RaisePropertyChangedEvent(nameof(TransportationDocList));
        }

        private void SourceCodeChanged()
        {
            try
            {
                _sourceStatus = false;
                _sourceStatusBGColor = Brushes.White;
                _sourceStatusText = "N/A";
                _sourceFinishButtonVisibility = Visibility.Collapsed;
                _sourceUnFinishButtonVisibility = Visibility.Collapsed;
                _sourcePrintButtonVisibility = Visibility.Collapsed;

                if (!string.IsNullOrEmpty(_transportationCodeSource))
                {
                    var transportation = BuyingFacade.TransportationBL().GetSingle(_transportationCodeSource);
                    if (transportation != null)
                    {
                        _sourceStatus = transportation.IsFinish;
                        _sourceStatusText = _sourceStatus == true ? "Finish" : "Unfinish";
                        _sourceStatusBGColor = _sourceStatus == true ? Brushes.LightGreen : Brushes.Salmon;
                        _sourceFinishButtonVisibility = _sourceStatus == true ? Visibility.Collapsed : Visibility.Visible;
                        _sourceUnFinishButtonVisibility = _sourceStatus == true ? Visibility.Visible : Visibility.Collapsed;
                        _sourcePrintButtonVisibility = _sourceStatus == true ? Visibility.Visible : Visibility.Collapsed;
                    }
                }

                RaisePropertyChangedEvent(nameof(SourceStatus));
                RaisePropertyChangedEvent(nameof(SourceStatusText));
                RaisePropertyChangedEvent(nameof(SourceStatusBGColor));
                RaisePropertyChangedEvent(nameof(SourceFinishButtonVisibility));
                RaisePropertyChangedEvent(nameof(SourceUnFinishButtonVisibility));
                RaisePropertyChangedEvent(nameof(SourcePrintButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DestinationCodeChanged()
        {
            try
            {
                _destinationStatus = false;
                _destinationStatusBGColor = Brushes.White;
                _destinationStatusText = "N/A";
                _destinationFinishButtonVisibility = Visibility.Collapsed;
                _destinationUnFinishButtonVisibility = Visibility.Collapsed;
                _destinationPrintButtonVisibility = Visibility.Collapsed;

                if (!string.IsNullOrEmpty(_transportationCodeSource))
                {
                    var transportation = BuyingFacade.TransportationBL().GetSingle(_transportationCodeDestination);
                    if (transportation != null)
                    {
                        _destinationStatus = transportation.IsFinish;
                        _destinationStatusText = _destinationStatus == true ? "Finish" : "Unfinish";
                        _destinationStatusBGColor = _destinationStatus == true ? Brushes.LightGreen : Brushes.Salmon;
                        _destinationFinishButtonVisibility = _destinationStatus == true ? Visibility.Collapsed : Visibility.Visible;
                        _destinationUnFinishButtonVisibility = _destinationStatus == true ? Visibility.Visible : Visibility.Collapsed;
                        _destinationPrintButtonVisibility = _destinationStatus == true ? Visibility.Visible : Visibility.Collapsed;
                    }
                }

                RaisePropertyChangedEvent(nameof(DestinationStatus));
                RaisePropertyChangedEvent(nameof(DestinationStatusText));
                RaisePropertyChangedEvent(nameof(DestinationStatusBGColor));
                RaisePropertyChangedEvent(nameof(DestinationFinishButtonVisibility));
                RaisePropertyChangedEvent(nameof(DestinationUnFinishButtonVisibility));
                RaisePropertyChangedEvent(nameof(DestinationPrintButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PrintDocument(string transportationCode)
        {
            Forms.BuyingStaff.TransportationDocumentPrintPreview window =
                new Forms.BuyingStaff.TransportationDocumentPrintPreview(transportationCode);
            window.ShowDialog();
        }
        #endregion
    }
}
