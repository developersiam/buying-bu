﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using DomainModel;
using System.Windows.Input;
using BUWinApp.Helper;
using BusinessLayer;
using System.Windows;

namespace BUWinApp.ViewModel.BuyingStaff
{
    public class vm_BarcodeInfo : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_BarcodeInfo()
        {
            _rejectStatus = Visibility.Collapsed;
            _cancelRejectStatus = Visibility.Collapsed;
            RaisePropertyChangedEvent(nameof(RejectStatus));
        }


        #region Properties
        private string _baleBarcode;
        public string BaleBarcode
        {
            get { return _baleBarcode; }
            set { _baleBarcode = value; }
        }

        private Buying _buying;
        public Buying Buying
        {
            get { return _buying; }
            set { _buying = value; }
        }

        private string _documentCode;
        public string DocumentCode
        {
            get { return _documentCode; }
            set { _documentCode = value; }
        }

        private Visibility _rejectStatus;
        public Visibility RejectStatus
        {
            get { return _rejectStatus; }
            set { _rejectStatus = value; }
        }

        private Visibility _cancelRejectStatus;
        public Visibility CancelRejectStatus
        {
            get { return _cancelRejectStatus; }
            set { _cancelRejectStatus = value; }
        }
        #endregion


        #region List
        private List<Buying> _buyingList;
        public List<Buying> BuyingList
        {
            get { return _buyingList; }
            set { _buyingList = value; }
        }
        #endregion


        #region Command
        private ICommand _onBaleBarCodeEnterCommand;

        public ICommand OnBaleBarcodeEnterCommand
        {
            get { return _onBaleBarCodeEnterCommand ?? (_onBaleBarCodeEnterCommand = new RelayCommand(OnBaleBarcodeEnter)); }
            set { _onBaleBarCodeEnterCommand = value; }
        }

        private void OnBaleBarcodeEnter(object obj)
        {
            BuyingBinding();
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(OnClear)); }
            set { _onClearCommand = value; }
        }

        private void OnClear(object obj)
        {
            _baleBarcode = "";
            _buying = null;
            _buyingList = null;
            _documentCode = "";
            _rejectStatus = Visibility.Collapsed;
            _cancelRejectStatus = Visibility.Collapsed;

            RaisePropertyChangedEvent(nameof(BaleBarcode));
            RaisePropertyChangedEvent(nameof(Buying));
            RaisePropertyChangedEvent(nameof(BuyingList));
            RaisePropertyChangedEvent(nameof(DocumentCode));
            RaisePropertyChangedEvent(nameof(RejectStatus));
            RaisePropertyChangedEvent(nameof(CancelRejectStatus));

            OnFocusRequested(nameof(BaleBarcode));
        }

        private ICommand _onNTRMInspectionCommand;

        public ICommand OnNTRMInspectionCommand
        {
            get { return _onNTRMInspectionCommand ?? (_onNTRMInspectionCommand = new RelayCommand(OnNTRMInpectionClick)); }
            set { _onNTRMInspectionCommand = value; }
        }

        private void OnNTRMInpectionClick(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_baleBarcode))
                {
                    MessageBoxHelper.Warning("โปรดสแกนบาร์โค้ต");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                View.BuyingStaff.NTRMInspection window = new View.BuyingStaff.NTRMInspection();
                var vm = new vm_NTRMInspection();
                window.DataContext = vm;
                vm.BaleBarcode = _baleBarcode;
                window.ShowDialog();
                _buying = null;
                RaisePropertyChangedEvent(nameof(Buying));
                BuyingBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onRejectCommand;

        public ICommand OnRejectCommand
        {
            get { return _onRejectCommand ?? (_onRejectCommand = new RelayCommand(OnReject)); }
            set { _onRejectCommand = value; }
        }

        private void OnReject(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_baleBarcode))
                {
                    MessageBoxHelper.Warning("โปรดสแกนบาร์โค้ต");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                View.BuyingStaff.Rejection window = new View.BuyingStaff.Rejection();
                var vm = new vm_Rejection(window);
                window.DataContext = vm;
                vm.BaleBarcode = _baleBarcode;
                vm.BuyerCode = _buying.BuyerCode;
                window.ShowDialog();
                var result = vm.RejectReason;
                if (!string.IsNullOrEmpty(result))
                {
                    BuyingFacade.BuyingBL()
                               .RejectBale(_baleBarcode,
                               result,
                               user_setting.User.Username,
                               _buying.BuyerCode);

                    BuyingBinding();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onCancelRejectCommand;

        public ICommand OnCancelRejectCommand
        {
            get { return _onCancelRejectCommand ?? (_onCancelRejectCommand = new RelayCommand(OnCancelReject)); }
            set { _onCancelRejectCommand = value; }
        }

        private void OnCancelReject(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_baleBarcode))
                {
                    MessageBoxHelper.Warning("โปรดสแกนบาร์โค้ต");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                BuyingFacade.BuyingBL().RemoveRejectBale(_baleBarcode);
                RaisePropertyChangedEvent(nameof(Buying));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion


        #region Function
        private void BuyingBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_baleBarcode))
                {
                    MessageBoxHelper.Warning("โปรดสแกนบาร์โค้ต");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                _buying = BuyingFacade.BuyingBL().GetByBaleBarcode(_baleBarcode);

                if (_buying == null)
                {
                    MessageBoxHelper.Warning("ไม่พบข้อมูลห่อยารหัส " + _baleBarcode + " ในระบบ");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                _documentCode = _buying.Crop + "-" +
                    _buying.BuyingStationCode + "-" +
                    _buying.FarmerCode + "-" +
                    _buying.BuyingDocumentNumber;

                _buyingList = BuyingFacade.BuyingBL()
                    .GetByBuyingDocument(_buying.Crop,
                    _buying.FarmerCode,
                    _buying.BuyingStationCode,
                    _buying.BuyingDocumentNumber);

                if (string.IsNullOrEmpty(_buying.RejectReason))
                {
                    _rejectStatus = Visibility.Visible;
                    _cancelRejectStatus = Visibility.Collapsed;
                }
                else
                {
                    _rejectStatus = Visibility.Collapsed;
                    _cancelRejectStatus = Visibility.Visible;
                }

                RaisePropertyChangedEvent(nameof(Buying));
                RaisePropertyChangedEvent(nameof(DocumentCode));
                RaisePropertyChangedEvent(nameof(BuyingList));
                RaisePropertyChangedEvent(nameof(RejectStatus));
                RaisePropertyChangedEvent(nameof(CancelRejectStatus));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
