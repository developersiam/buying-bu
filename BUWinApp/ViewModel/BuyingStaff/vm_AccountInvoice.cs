﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using BUWinApp.Helper;
using BusinessLayer.Model;
using BusinessLayer;
using DomainModel;

namespace BUWinApp.ViewModel.BuyingStaff
{
    public class vm_AccountInvoice : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }



        #region Properties
        private string _accountInvoiceNo;

        public string AccountInvoiceNo
        {
            get { return _accountInvoiceNo; }
            set
            {
                _accountInvoiceNo = value;
                if (string.IsNullOrEmpty(_accountInvoiceNo))
                    return;

                _accountInvoice = BuyingFacade.AccountInvoiceBL()
                    .GetSingle(_accountInvoiceNo);

                AccountInvoiceListBinding();
                RaisePropertyChangedEvent(nameof(AccountInvoice));
            }
        }

        private decimal _totalRecord;

        public decimal TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private decimal _totalPrice;

        public decimal TotalPrice
        {
            get { return _totalPrice; }
            set { _totalPrice = value; }
        }

        private AccountInvoice _accountInvoice;

        public AccountInvoice AccountInvoice
        {
            get { return _accountInvoice; }
            set { _accountInvoice = value; }
        }

        #endregion



        #region ListProperties
        private List<m_InvoiceDetails> _accountInvoiceList;

        public List<m_InvoiceDetails> AccountInvoiceList
        {
            get { return _accountInvoiceList; }
            set { _accountInvoiceList = value; }
        }
        #endregion



        #region Command

        #endregion



        #region Function
        private void AccountInvoiceListBinding()
        {
            try
            {
                _accountInvoiceList = BusinessLayer.Helper.InvoiceDetailsHelper
                    .GetByAccountInvoiceNo(_accountInvoiceNo)
                    .ToList();

                _totalRecord = _accountInvoiceList.Count();
                _totalPrice = _accountInvoiceList.Sum(x => x.Quantity * x.UnitPrice);

                RaisePropertyChangedEvent(nameof(AccountInvoiceList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
                RaisePropertyChangedEvent(nameof(TotalPrice));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
