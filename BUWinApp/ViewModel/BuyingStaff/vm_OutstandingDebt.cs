﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using DomainModel;
using System.Windows.Input;
using BusinessLayer.Model;
using BusinessLayer;
using BusinessLayer.Helper;
using BUWinApp.Helper;
using BUWinApp.View.BuyingStaff;
using System.Windows;

namespace BUWinApp.ViewModel.BuyingStaff
{
    public class vm_OutstandingDebt : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_OutstandingDebt()
        {
        }


        #region Properties
        public short _crop { get; set; }
        public string _stationCode { get; set; }
        public string _farmerCode { get; set; }
        public short _docNumber { get; set; }

        private m_BuyingDocument _document;

        public m_BuyingDocument Document
        {
            get { return _document; }
            set
            {
                _document = value;

                _documentList = BuyingFacade.BuyingDocumentBL()
                    .GetByFarmer(_document.Crop, _document.FarmerCode)
                    .Select(item => new m_BuyingDocument
                    {
                        DocumentCode = item.Crop + "-" +
                        item.BuyingStationCode + "-" +
                        item.FarmerCode + "-" +
                        item.BuyingDocumentNumber,
                        Crop = item.Crop,
                        BuyingStationCode = item.BuyingStationCode,
                        FarmerCode = item.FarmerCode,
                        BuyingDocumentNumber = item.BuyingDocumentNumber
                    })
                    .ToList();

                RaisePropertyChangedEvent(nameof(DocumentList));

                _documentCode = _document.DocumentCode;

                SummaryDataBinding();
                AccountInvoiceListBinding();
                PaymentHistoryListBinding();
            }
        }

        private string _documentCode;

        public string DocumentCode
        {
            get { return _documentCode; }
            set
            {
                _documentCode = value;
                SummaryDataBinding();
            }
        }

        private PaymentHistory _payment;

        public PaymentHistory Payment
        {
            get { return _payment; }
            set { _payment = value; }
        }

        private decimal _totalDebt;

        public decimal TotalDebt
        {
            get { return _totalDebt; }
            set { _totalDebt = value; }
        }

        private decimal _creditNoteAmount;

        public decimal CreditNoteAmount
        {
            get { return _creditNoteAmount; }
            set { _creditNoteAmount = value; }
        }

        private decimal _finalDebt;

        public decimal FinalDebt
        {
            get { return _finalDebt; }
            set { _finalDebt = value; }
        }

        private decimal _totalPayment;

        public decimal TotalPayment
        {
            get { return _totalPayment; }
            set { _totalPayment = value; }
        }

        private decimal _totalRemaining;

        public decimal TotalRemaining
        {
            get { return _totalRemaining; }
            set { _totalRemaining = value; }
        }

        private decimal _buyingAmount;

        public decimal BuyingAmount
        {
            get { return _buyingAmount; }
            set { _buyingAmount = value; }
        }

        private decimal _balanceBuyingAmount;

        public decimal BalanceBuyingAmount
        {
            get { return _balanceBuyingAmount; }
            set { _balanceBuyingAmount = value; }
        }

        private Visibility _isPayment;

        public Visibility IsPayment
        {
            get { return _isPayment; }
            set { _isPayment = value; }
        }

        #endregion



        #region List
        private List<m_AccountInvoiceDetails> _accountInvoiceList;

        public List<m_AccountInvoiceDetails> AccountInvoiceList
        {
            get { return _accountInvoiceList; }
            set { _accountInvoiceList = value; }
        }

        private List<m_PaymentHistory> _paymentHistoryList;

        public List<m_PaymentHistory> PaymentHistoryList
        {
            get { return _paymentHistoryList; }
            set { _paymentHistoryList = value; }
        }

        private List<m_BuyingDocument> _documentList;

        public List<m_BuyingDocument> DocumentList
        {
            get { return _documentList; }
            set { _documentList = value; }
        }
        #endregion



        #region Command
        private ICommand _onDeductionCommand;

        public ICommand OnDeductionCommand
        {
            get { return _onDeductionCommand ?? (_onDeductionCommand = new RelayCommand(OnDeduction)); }
            set { _onDeductionCommand = value; }
        }

        public void OnDeduction(object e)
        {
            try
            {
                if (_payment != null &&
                    _payment.IsFinish == true &&
                    user_setting.UserRoles
                    .Where(x => x.RoleName == "Accounting")
                    .Count() <= 0)
                    throw new ArgumentException("ใบเวาเชอร์นี้มีการยืนยันการหักชำระหนี้จากทางแผนกบัญชีแล้ว " +
                        "ไม่สามารถคำนวนยอดหนี้เพื่อหักชำระใหม่ได้อีก");

                if (_totalRemaining == 0)
                    throw new ArgumentException("ชาวไร่ได้ยื่นชำระเงินครบตามยอดทั้งหมดแล้ว ไม่ต้องคำนวนยอดหักอีก");


                OutstandingDebtCalculation window = new OutstandingDebtCalculation();
                var vm = new vm_OutstandingDebtCalculation();

                window.DataContext = vm;

                vm.TotalDebt = _totalDebt;
                vm.CreditNoteAmount = _creditNoteAmount;
                vm.TotalPayment = _totalPayment;
                vm.BuyingAmount = _buyingAmount;
                vm.Document = new m_BuyingDocument
                {
                    DocumentCode = _documentCode,
                    Crop = _crop,
                    BuyingStationCode = _stationCode,
                    FarmerCode = _farmerCode,
                    BuyingDocumentNumber = _docNumber
                };

                window.ShowDialog();
                PaymentHistoryListBinding();
                SummaryDataBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onAccountInvoiceDetailsCommand;

        public ICommand OnAccountInvoiceDetailsCommand
        {
            get { return _onAccountInvoiceDetailsCommand ?? (_onAccountInvoiceDetailsCommand = new RelayCommand(OnAccountInvoiceDetails)); }
            set { _onAccountInvoiceDetailsCommand = value; }
        }

        private void OnAccountInvoiceDetails(object obj)
        {
            try
            {
                View.BuyingStaff.AccountInvoice window = new View.BuyingStaff.AccountInvoice();
                var vm = new vm_AccountInvoice();
                window.DataContext = vm;
                vm.AccountInvoiceNo = obj.ToString();
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion



        #region Function
        private void PaymentHistoryListBinding()
        {
            _paymentHistoryList = DebtorsHelper.GetPaymentByFarmer(_document.Crop, _document.FarmerCode);
            _finalDebt = _totalDebt - _creditNoteAmount;
            _totalPayment = _paymentHistoryList.Sum(x => x.PaymentAmount);
            _totalRemaining = _totalDebt - _creditNoteAmount - _totalPayment;

            RaisePropertyChangedEvent(nameof(TotalRemaining));
            RaisePropertyChangedEvent(nameof(TotalPayment));
            RaisePropertyChangedEvent(nameof(PaymentHistoryList));
            RaisePropertyChangedEvent(nameof(FinalDebt));
        }

        private void AccountInvoiceListBinding()
        {
            _accountInvoiceList = DebtorsHelper
                .GetDebtByFarmer(_document.Crop, _document.FarmerCode)
                .OrderBy(x => x.CreateDate)
                .ToList();
            _totalDebt = _accountInvoiceList.Sum(x => x.Price);
            _finalDebt = _totalDebt - _creditNoteAmount;
            _totalRemaining = _finalDebt - _totalPayment;

            RaisePropertyChangedEvent(nameof(TotalRemaining));
            RaisePropertyChangedEvent(nameof(TotalDebt));
            RaisePropertyChangedEvent(nameof(AccountInvoiceList));
            RaisePropertyChangedEvent(nameof(FinalDebt));
        }

        private void SummaryDataBinding()
        {
            try
            {
                string[] splitter = _documentCode.Split('-');

                _crop = Convert.ToInt16(splitter[0]);
                _stationCode = splitter[1];
                _farmerCode = splitter[2];
                _docNumber = Convert.ToInt16(splitter[3]);

                _buyingAmount = (decimal)BuyingHelper
                    .GetByBuyingDocument(_crop,
                    _farmerCode,
                    _stationCode,
                    _docNumber)
                    .Sum(x => x.Price);

                _payment = BuyingFacade.PaymentHistoryBL()
                    .GetSingle(_crop,
                    _stationCode,
                    _farmerCode,
                    _docNumber);

                if (_payment == null)
                {
                    _isPayment = Visibility.Collapsed;
                }
                else
                {
                    _isPayment = Visibility.Visible;
                    _balanceBuyingAmount = _buyingAmount - _payment.PaymentAmount;
                }

                RaisePropertyChangedEvent(nameof(Payment));
                RaisePropertyChangedEvent(nameof(BuyingAmount));
                RaisePropertyChangedEvent(nameof(BalanceBuyingAmount));
                RaisePropertyChangedEvent(nameof(IsPayment));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
