﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using BusinessLayer.Model;
using System.Windows.Input;
using BusinessLayer.Helper;
using BUWinApp.Helper;
using BUWinApp.View.BuyingStaff;
using BusinessLayer;
using System.Windows;

namespace BUWinApp.ViewModel.BuyingStaff
{
    public class vm_Voucher : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_Voucher()
        {
            _buyingDate = DateTime.Now;
            DocumentListBinding();
            RaisePropertyChangedEvent(nameof(BuyingDate));
        }


        #region Properties
        private DateTime _buyingDate;
        public DateTime BuyingDate
        {
            get { return _buyingDate; }
            set
            {
                _buyingDate = value;
                DocumentListBinding();
            }
        }

        private string _firstName;
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private int _totalRecord;
        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private int _selectedItems;

        public int SelectedItems
        {
            get { return _selectedItems; }
            set { _selectedItems = value; }
        }

        #endregion



        #region List
        private List<m_BuyingDocument> _documentList;
        public List<m_BuyingDocument> DocumentList
        {
            get { return _documentList; }
            set { _documentList = value; }
        }

        private List<m_BuyingDocument> _selectedItemList;

        public List<m_BuyingDocument> SelectedItemList
        {
            get { return _selectedItemList; }
            set { _selectedItemList = value; }
        }
        #endregion



        #region Command
        private ICommand _onSearchCommand;

        public ICommand OnSearchCommand
        {
            get { return _onSearchCommand ?? (_onSearchCommand = new RelayCommand(OnSearch)); }
            set { _onSearchCommand = value; }
        }

        private void OnSearch(object obj)
        {
            if (string.IsNullOrEmpty(_firstName))
            {
                MessageBoxHelper.Warning("โปรดระบุชื่อชาวไร่ที่ต้องการค้น");
                OnFocusRequested(nameof(FirstName));
                return;
            }

            DocumentListBinding();
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(OnClear)); }
            set { _onClearCommand = value; }
        }

        private void OnClear(object obj)
        {
            _firstName = "";
            RaisePropertyChangedEvent(nameof(FirstName));
            OnFocusRequested(nameof(FirstName));
            DocumentListBinding();
        }

        private ICommand _onRefreshCommand;

        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(OnRefresh)); }
            set { _onRefreshCommand = value; }
        }

        private void OnRefresh(object obj)
        {
            DocumentListBinding();
        }

        private ICommand _onPrintVoucherCommand;

        public ICommand OnPrintVoucherCommand
        {
            get { return _onPrintVoucherCommand ?? (_onPrintVoucherCommand = new RelayCommand(OnPrintVoucher)); }
            set { _onPrintVoucherCommand = value; }
        }

        private void OnPrintVoucher(object obj)
        {
            var document = (m_BuyingDocument)obj;

            VoucherDetails window = new VoucherDetails();
            var vm = new vm_VoucherDetails();
            window.DataContext = vm;
            vm.Document = document;
            window.ShowDialog();
        }

        private ICommand _onPrintSelectedReceiptMoneyCommand;

        public ICommand OnPrintSelectedReceiptMoneyCommand
        {
            get { return _onPrintSelectedReceiptMoneyCommand ?? (_onPrintSelectedReceiptMoneyCommand = new RelayCommand(OnPrintSelectedReceiptMoney)); }
            set { _onPrintSelectedReceiptMoneyCommand = value; }
        }

        private void OnPrintSelectedReceiptMoney(object obj)
        {
            try
            {
                System.Collections.IList items = (System.Collections.IList)obj;
                var _selectedItemList = items.Cast<m_BuyingDocument>().ToList();

                if (_selectedItemList.Count() <= 0)
                    throw new ArgumentException("โปรดเลือกรายการที่จะพิมพ์ โดยคลิกเลือกจากตารางด้านล่าง");

                var waitForAccountFinish = _selectedItemList.Where(x => x.IsAccountFinish == false);
                if (waitForAccountFinish.Count() > 0)
                    throw new ArgumentException("มีรายการที่ท่านเลือกจำนวน " + waitForAccountFinish.Count() +
                        " รายการ ที่รอการยืนยันจากแผนกบัญชี โปรดเลือกรายการใหม่อีกครั้ง");

                PrintReceiptMoney window = new PrintReceiptMoney();
                var vm = new vm_PrintReceiptMoney();
                window.DataContext = vm;
                vm.SelectedList = _selectedItemList;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDataGridSelectedCommand;

        public ICommand OnDataGridSelectedCommand
        {
            get { return _onDataGridSelectedCommand ?? (_onDataGridSelectedCommand = new RelayCommand(OnDataGridSelected)); }
            set { _onDataGridSelectedCommand = value; }
        }

        private void OnDataGridSelected(object obj)
        {
            try
            {
                System.Collections.IList items = (System.Collections.IList)obj;
                var _selectedItemList = items.Cast<m_BuyingDocument>().ToList();

                _selectedItems = _selectedItemList.Count();
                RaisePropertyChangedEvent(nameof(SelectedItems));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion



        #region Function
        private void DocumentListBinding()
        {
            if (string.IsNullOrEmpty(_firstName))
                _documentList = BuyingDocumentHelper
                        .GetByStationAndBuyingDate(_buyingDate, user_setting.StactionCode)
                        .OrderBy(x => x.IsFinish)
                        .ThenBy(x => x.FinishDate)
                        .ToList();
            else
                _documentList = BuyingDocumentHelper
                            .GetByStationAndBuyingDate(_buyingDate, user_setting.StactionCode)
                            .Where(x => x.FirstName.Contains(_firstName))
                            .OrderBy(x => x.IsFinish)
                            .ThenBy(x => x.FinishDate)
                            .ToList();

            _totalRecord = _documentList.Count();
            RaisePropertyChangedEvent(nameof(TotalRecord));
            RaisePropertyChangedEvent(nameof(DocumentList));
        }
        #endregion
    }
}
