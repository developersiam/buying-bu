﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using BusinessLayer.Model;
using DomainModel;
using BusinessLayer;
using BUWinApp.Helper;
using System.Windows.Input;
using BUWinApp.View.BuyingStaff;
using BusinessLayer.Helper;

namespace BUWinApp.ViewModel.BuyingStaff
{
    public class vm_AreaSummary : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_AreaSummary()
        {
            _createDate = DateTime.Now;
            _stationCode = user_setting.StactionCode;

            DocumentListBinding();

            RaisePropertyChangedEvent(nameof(CreateDate));
            RaisePropertyChangedEvent(nameof(StationCode));
        }



        #region Properties
        private DateTime _createDate;

        public DateTime CreateDate
        {
            get { return _createDate; }
            set
            {
                _createDate = value;
                DocumentListBinding();
            }
        }

        private string _stationCode;

        public string StationCode
        {
            get { return _stationCode; }
            set
            {
                _stationCode = value;
                DocumentListBinding();
            }
        }

        private string _firstName;

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        private int _totalBale;

        public int TotalBale
        {
            get { return _totalBale; }
            set { _totalBale = value; }
        }

        private int _rejectedBale;

        public int RejectedBale
        {
            get { return _rejectedBale; }
            set { _rejectedBale = value; }
        }

        private int _buyingBale;

        public int BuyingBale
        {
            get { return _buyingBale; }
            set { _buyingBale = value; }
        }

        private decimal _totalWeight;

        public decimal TotalWeight
        {
            get { return _totalWeight; }
            set { _totalWeight = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        #endregion



        #region ListProperties
        private List<m_BuyingDocument> _documentList;

        public List<m_BuyingDocument> DocumentList
        {
            get { return _documentList; }
            set { _documentList = value; }
        }

        public List<BuyingStation> StationList
        {
            get
            {
                return BuyingFacade.BuyingStationBL()
                    .GetByArea(user_setting.User.StaffUser.AreaCode);
            }
        }

        #endregion



        #region Command

        private ICommand _onSearchCommand;
        public ICommand OnSearchCommand
        {
            get { return _onSearchCommand ?? (_onSearchCommand = new RelayCommand(OnSearch)); }
            set { _onSearchCommand = value; }
        }

        private ICommand _onClearCommand;
        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(OnClear)); }
            set { _onClearCommand = value; }
        }

        private ICommand _onRefreshCommand;
        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(OnRefresh)); }
            set { _onRefreshCommand = value; }
        }

        private ICommand _onDetailsCommand;

        public ICommand OnDetailsCommand
        {
            get { return _onDetailsCommand ?? (_onDetailsCommand = new RelayCommand(OnDetails)); }
            set { _onDetailsCommand = value; }
        }
        #endregion



        #region Function
        private void OnSearch(object obj)
        {
            if (string.IsNullOrEmpty(_firstName))
            {
                MessageBoxHelper.Warning("โปรดระบุชื่อชาวไร่ที่ต้องการค้น");
                OnFocusRequested(nameof(FirstName));
                return;
            }

            DocumentListBinding();
        }

        private void OnClear(object obj)
        {
            _firstName = "";
            RaisePropertyChangedEvent(nameof(FirstName));
            OnFocusRequested(nameof(FirstName));
            DocumentListBinding();
        }

        private void OnRefresh(object obj)
        {
            DocumentListBinding();
        }

        private void OnDetails(object obj)
        {
            var document = (m_BuyingDocument)obj;

            VoucherDetails window = new VoucherDetails();
            var vm = new vm_VoucherDetails();
            window.DataContext = vm;
            vm.Document = document;
            window.ShowDialog();
        }

        private void DocumentListBinding()
        {
            if (string.IsNullOrEmpty(_firstName))
                _documentList = BuyingDocumentHelper
                        .GetByStationAndCreateDate(_createDate, _stationCode)
                        .OrderBy(x => x.IsFinish)
                        .ThenBy(x => x.FinishDate)
                        .ToList();
            else
                _documentList = BuyingDocumentHelper
                            .GetByStationAndCreateDate(_createDate, _stationCode)
                            .Where(x => x.FirstName.Contains(_firstName))
                            .OrderBy(x => x.IsFinish)
                            .ThenBy(x => x.FinishDate)
                            .ToList();

            _totalRecord = _documentList.Count();
            _totalBale = _documentList.Sum(x => x.TotalBale);
            _rejectedBale = _documentList.Sum(x => x.TotalReject);
            _buyingBale = _documentList.Sum(x => x.TotalWeight);
            _totalWeight = (decimal)_documentList.Sum(x => x.TotalSold);

            RaisePropertyChangedEvent(nameof(TotalRecord));
            RaisePropertyChangedEvent(nameof(TotalBale));
            RaisePropertyChangedEvent(nameof(RejectedBale));
            RaisePropertyChangedEvent(nameof(BuyingBale));
            RaisePropertyChangedEvent(nameof(TotalWeight));
            RaisePropertyChangedEvent(nameof(DocumentList));
        }
        #endregion
    }
}
