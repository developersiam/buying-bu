﻿using BusinessLayer;
using BusinessLayer.Helper;
using BusinessLayer.Model;
using BUWinApp.Helper;
using BUWinApp.MVVM;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace BUWinApp.ViewModel.BuyingStaff.DebtAndReceipt
{
    public class vm_DeductionDripProject : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_DeductionDripProject()
        {
            _deductOption1 = true;
            _deductOption2 = false;
            _deductOption3 = false;
            _deductOption1Visibility = Visibility.Visible;
            _deductOption2Visibility = Visibility.Collapsed;
            _deductOption3Visibility = Visibility.Collapsed;
            RaisePropertyChangedEvent(nameof(DeductOption1));
            RaisePropertyChangedEvent(nameof(DeductOption2));
            RaisePropertyChangedEvent(nameof(DeductOption3));
            RaisePropertyChangedEvent(nameof(DeductOption1Visibility));
            RaisePropertyChangedEvent(nameof(DeductOption2Visibility));
            RaisePropertyChangedEvent(nameof(DeductOption3Visibility));
        }

        #region DeductionOption
        private bool _deductOption1;

        public bool DeductOption1
        {
            get { return _deductOption1; }
            set
            {
                _deductOption1 = value;
                if (value == true)
                {
                    _deductOption2 = false;
                    _deductOption3 = false;
                    RaisePropertyChangedEvent(nameof(DeductOption2));
                    RaisePropertyChangedEvent(nameof(DeductOption3));

                    _deductOption1Visibility = Visibility.Visible;
                    _deductOption2Visibility = Visibility.Collapsed;
                    _deductOption3Visibility = Visibility.Collapsed;
                    RaisePropertyChangedEvent(nameof(DeductOption1Visibility));
                    RaisePropertyChangedEvent(nameof(DeductOption2Visibility));
                    RaisePropertyChangedEvent(nameof(DeductOption3Visibility));

                    CalculateDeductionPerKg();
                }
            }
        }

        private bool _deductOption2;

        public bool DeductOption2
        {
            get { return _deductOption2; }
            set
            {
                _deductOption2 = value;
                if (value == true)
                {
                    _deductOption1 = false;
                    _deductOption3 = false;
                    RaisePropertyChangedEvent(nameof(DeductOption1));
                    RaisePropertyChangedEvent(nameof(DeductOption3));

                    _deductOption1Visibility = Visibility.Collapsed;
                    _deductOption2Visibility = Visibility.Visible;
                    _deductOption3Visibility = Visibility.Collapsed;
                    RaisePropertyChangedEvent(nameof(DeductOption1Visibility));
                    RaisePropertyChangedEvent(nameof(DeductOption2Visibility));
                    RaisePropertyChangedEvent(nameof(DeductOption3Visibility));
                }
            }
        }

        private bool _deductOption3;

        public bool DeductOption3
        {
            get { return _deductOption3; }
            set
            {
                _deductOption3 = value;
                if (value == true)
                {
                    _deductOption1 = false;
                    _deductOption2 = false;
                    RaisePropertyChangedEvent(nameof(DeductOption1));
                    RaisePropertyChangedEvent(nameof(DeductOption2));

                    _deductOption1Visibility = Visibility.Collapsed;
                    _deductOption2Visibility = Visibility.Collapsed;
                    _deductOption3Visibility = Visibility.Visible;
                    RaisePropertyChangedEvent(nameof(DeductOption1Visibility));
                    RaisePropertyChangedEvent(nameof(DeductOption2Visibility));
                    RaisePropertyChangedEvent(nameof(DeductOption3Visibility));

                    DeductionFor3YearBinding();
                }
            }
        }

        private Visibility _deductOption1Visibility;

        public Visibility DeductOption1Visibility
        {
            get { return _deductOption1Visibility; }
            set { _deductOption1Visibility = value; }
        }

        private Visibility _deductOption2Visibility;

        public Visibility DeductOption2Visibility
        {
            get { return _deductOption2Visibility; }
            set { _deductOption2Visibility = value; }
        }

        private Visibility _deductOption3Visibility;

        public Visibility DeductOption3Visibility
        {
            get { return _deductOption3Visibility; }
            set { _deductOption3Visibility = value; }
        }
        #endregion

        #region TotalDebt
        private double _totalDebtForCurrentYearAmount;

        public double TotalDebtForCurrentYearAmount
        {
            get { return _totalDebtForCurrentYearAmount; }
            set { _totalDebtForCurrentYearAmount = value; }
        }

        private double _totalDebtFor3YearAmount;

        public double TotalDebtFor3YearAmount
        {
            get { return _totalDebtFor3YearAmount; }
            set { _totalDebtFor3YearAmount = value; }
        }
        #endregion

        #region DebtBalance
        private double _debtBalanceAmountForCurrentYear;

        public double DebtBalanceAmountForCurrentYear
        {
            get { return _debtBalanceAmountForCurrentYear; }
            set { _debtBalanceAmountForCurrentYear = value; }
        }

        private double _debtBalanceAmountFor3YearContinue;

        public double DebtBalanceAmountFor3YearContinue
        {
            get { return _debtBalanceAmountFor3YearContinue; }
            set { _debtBalanceAmountFor3YearContinue = value; }
        }
        #endregion

        #region ReceiptAmount
        private double _totalReceiptAmountForCurrentYear;

        public double TotalReceiptAmountForCurrentYear
        {
            get { return _totalReceiptAmountForCurrentYear; }
            set { _totalReceiptAmountForCurrentYear = value; }
        }

        private double _totalReceiptAmountFor3YearContinue;

        public double TotalReceiptAmountFor3YearContinue
        {
            get { return _totalReceiptAmountFor3YearContinue; }
            set { _totalReceiptAmountFor3YearContinue = value; }
        }
        #endregion

        #region Properties
        private BuyingDocument _buyingDocument;

        public BuyingDocument BuyingDocument
        {
            get { return _buyingDocument; }
            set
            {
                _buyingDocument = value;
                var doc = _buyingDocument;
                var buyingList = BuyingFacade.BuyingBL()
                    .GetByBuyingDocument(doc.Crop,
                    doc.BuyingStationCode,
                    doc.FarmerCode,
                    doc.BuyingDocumentNumber
                    )
                    .Where(x => x.Grade != null)
                    .ToList();

                _buyingAmount = (double)buyingList.Sum(x => x.BuyingGrade.UnitPrice * x.Weight);
                _buyingWeight = (double)buyingList.Sum(x => x.Weight);
                _debtSetup = BuyingFacade.DebtSetupBL().GetSingleByDeductionCrop(doc.Crop, doc.FarmerCode);

                RaisePropertyChangedEvent(nameof(BuyingAmount));
                RaisePropertyChangedEvent(nameof(BuyingWeight));
                RaisePropertyChangedEvent(nameof(DebtSetup));
                ReceiptByVoucherBinding();
                CalculateDeductionPerKg();
            }
        }

        private double _buyingAmount;

        public double BuyingAmount
        {
            get { return _buyingAmount; }
            set { _buyingAmount = value; }
        }

        private DebtSetup _debtSetup;

        public DebtSetup DebtSetup
        {
            get { return _debtSetup; }
            set { _debtSetup = value; }
        }


        private double _voucherDeductionAmount;

        public double VoucherDeductionAmount
        {
            get { return _voucherDeductionAmount; }
            set { _voucherDeductionAmount = value; }
        }
        #endregion

        #region DeductPerKg
        private double _deductionPricePerKg;

        public double DeductionPricePerKg
        {
            get { return _deductionPricePerKg; }
            set { _deductionPricePerKg = value; }
        }

        private double _buyingWeight;

        public double BuyingWeight
        {
            get { return _buyingWeight; }
            set { _buyingWeight = value; }
        }

        private double _deductionAmountPerKg;

        public double DeductionAmountPerKg
        {
            get { return _deductionAmountPerKg; }
            set { _deductionAmountPerKg = value; }
        }

        private double _netBuyingAmountForDeductionByKg;

        public double NetBuyingAmountForDeductionByKg
        {
            get { return _netBuyingAmountForDeductionByKg; }
            set { _netBuyingAmountForDeductionByKg = value; }
        }
        #endregion

        #region DeductionForCurrentYear
        private double _deductionForCurrentYearAmount;

        public double DeductionForCurrentYearAmount
        {
            get { return _deductionForCurrentYearAmount; }
            set { _deductionForCurrentYearAmount = value; }
        }

        private double _netBuyingAmountForCurrentYear;

        public double NetBuyingAmountForCurrentYear
        {
            get { return _netBuyingAmountForCurrentYear; }
            set { _netBuyingAmountForCurrentYear = value; }
        }

        #endregion

        #region DeductionFor3Year
        private double _netBuyingAmountFor3YearContinue;

        public double NetBuyingAmountFor3YearContinue
        {
            get { return _netBuyingAmountFor3YearContinue; }
            set { _netBuyingAmountFor3YearContinue = value; }
        }

        private double _deductionAmountFor3YearContinue;

        public double DeductionAmountFor3YearContinue
        {
            get { return _deductionAmountFor3YearContinue; }
            set { _deductionAmountFor3YearContinue = value; }
        }
        #endregion



        #region List
        private List<m_DebtReceipt> _receiptByVoucherList;

        public List<m_DebtReceipt> ReceiptByVoucherList
        {
            get { return _receiptByVoucherList; }
            set { _receiptByVoucherList = value; }
        }

        private List<DebtReceipt> _receiptForCurrentYearList;

        public List<DebtReceipt> ReceiptForCurrentYearList
        {
            get { return _receiptForCurrentYearList; }
            set { _receiptForCurrentYearList = value; }
        }

        private List<DebtReceipt> _receiptFor3YearList;

        public List<DebtReceipt> ReceiptFor3YearList
        {
            get { return _receiptFor3YearList; }
            set { _receiptFor3YearList = value; }
        }

        private List<DebtSetup> _debtSetupForCurrentYearList;

        public List<DebtSetup> DebtSetupForCurrentYearList
        {
            get { return _debtSetupForCurrentYearList; }
            set { _debtSetupForCurrentYearList = value; }
        }

        private List<DebtSetup> _debtSetupFor3YearList;

        public List<DebtSetup> DebtSetupFor3YearList
        {
            get { return _debtSetupFor3YearList; }
            set { _debtSetupFor3YearList = value; }
        }
        #endregion

        #region Command
        private ICommand _onDeductionCommand;

        public ICommand OnDeductionCommand
        {
            get { return _onDeductionCommand ?? (_onDeductionCommand = new RelayCommand(Deduction)); }
            set { _onDeductionCommand = value; }
        }

        private void Deduction(object obj)
        {
            Save();
        }

        private ICommand _onDeleteReceiptCommand;

        public ICommand OnDeleteReceiptCommand
        {
            get { return _onDeleteReceiptCommand ?? (_onDeleteReceiptCommand = new RelayCommand(OnDeleteReceipt)); }
            set { _onDeleteReceiptCommand = value; }
        }

        private void OnDeleteReceipt(object obj)
        {
            try
            {
                var receipt = (m_DebtReceipt)obj;
                if (receipt == null)
                    return;

                Delete(receipt);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #endregion

        #region Function
        private void Save()
        {
            try
            {
                var finalBuyingAmount = _buyingAmount - _voucherDeductionAmount;
                if (_deductOption1)
                {
                    if (finalBuyingAmount < _deductionAmountPerKg)
                        throw new ArgumentException("ยอดเงินจากการขายไม่พอหักชำระ");

                    if (_debtSetup == null)
                        throw new ArgumentException("ไม่พบข้อมูลการตั้งหนี้ของชาวไร่รายนี้ในฤดูปลูกปัจจุบัน");

                    BuyingFacade.DebtReceiptBL()
                        .Add(_buyingDocument.Crop,
                        _buyingDocument.BuyingStationCode,
                        _buyingDocument.FarmerCode,
                        _buyingDocument.BuyingDocumentNumber,
                        "RB",
                        (decimal)_deductionAmountPerKg,
                        DateTime.Now,
                        user_setting.User.Username,
                        _debtSetup.DebtSetupCode);

                    DebtSetupForCurrentYearBinding();
                    ReceiptForCurrentYearBinding();
                    ReceiptByVoucherBinding();
                    CalculateDeductionPerKg();
                    MessageBoxHelper.Info("บันทึกสำเร็จ");
                }
                else if (_deductOption2)
                {
                    if (finalBuyingAmount < _debtBalanceAmountForCurrentYear)
                        throw new ArgumentException("ยอดเงินจากการขายไม่พอหักชำระ");

                    if (_debtSetup == null)
                        throw new ArgumentException("ไม่พบข้อมูลการตั้งหนี้ของชาวไร่รายนี้ในฤดูปลูกปัจจุบัน");
                    /*
                     * Select debt setup item by currentCrop (deductionCrop == currentCrop)                     .
                     * Calculate balance debt.
                     * Deduct all balance debt.
                     */

                    BuyingFacade.DebtReceiptBL()
                        .Add(_buyingDocument.Crop,
                        _buyingDocument.BuyingStationCode,
                        _buyingDocument.FarmerCode,
                        _buyingDocument.BuyingDocumentNumber,
                        "RB",
                        (decimal)_debtBalanceAmountForCurrentYear,
                        DateTime.Now,
                        user_setting.User.Username,
                        _debtSetup.DebtSetupCode);

                    DebtSetupForCurrentYearBinding();
                    ReceiptForCurrentYearBinding();
                    ReceiptByVoucherBinding();
                    DebtBalanceCalculate();

                    MessageBoxHelper.Info("บันทึกสำเร็จ");
                }
                else if (_deductOption3)
                {
                    if (finalBuyingAmount < _debtBalanceAmountFor3YearContinue)
                        throw new ArgumentException("ยอดเงินจากการขายไม่พอหักชำระ");
                    /*
                     * Select debt setup item (deductionCrop == currentCrop).
                     * Get 3 crop association (deductionCrop == setupCrop).
                     * Deduct all debt amount in the last 2 crop.
                     * Deduct all balance debt in a current crop.
                    */

                    var totalDebtAmount = _debtSetupFor3YearList.Sum(x => x.Amount);
                    var totalReceiptAmount = _debtSetupFor3YearList.Sum(x => x.DebtReceipts.Sum(y => y.Amount));
                    var balanceAmount = totalDebtAmount - totalReceiptAmount;
                    if (Math.Round(balanceAmount, 2) <= 0)
                        throw new ArgumentException("หักหนี้ทั้งหมดครบตามจำนวนแล้ว");

                    foreach (var item in _debtSetupFor3YearList)
                    {
                        var debtAmount = item.Amount;
                        var receiptAmount = item.DebtReceipts.Sum(x => x.Amount);
                        var debtBalanceAmount = debtAmount - receiptAmount;
                        if (Math.Round(debtBalanceAmount, 2) > 0)
                            BuyingFacade.DebtReceiptBL()
                                .Add(_buyingDocument.Crop,
                                _buyingDocument.BuyingStationCode,
                                _buyingDocument.FarmerCode,
                                _buyingDocument.BuyingDocumentNumber,
                                "RB",
                                debtBalanceAmount,
                                DateTime.Now,
                                user_setting.User.Username,
                                item.DebtSetupCode);
                    }
                    DebtSetupFor3YearBinding();
                    ReceiptFor3YearBinding();
                    ReceiptByVoucherBinding();
                    DebtBalanceCalculate();
                    MessageBoxHelper.Info("บันทึกสำเร็จ");
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Delete(m_DebtReceipt debtReceipt)
        {
            try
            {
                if (MessageBoxHelper.Question("ท่านต้องการลบยอดชำระนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                if (debtReceipt.ReceiptTypeCode != "RB")
                    throw new ArgumentException("ท่านสามาลบได้เฉพาะยอดชำระที่เป็นประเภทหักเงินจากการขายเท่านั้น");

                var debtSetup = BuyingFacade.DebtSetupBL().GetSingle(debtReceipt.DebtSetupCode);
                if (debtSetup == null)
                    throw new ArgumentException("ไม่พบยอดหนี้รหัส " + debtSetup.DebtSetupCode + " ในระบบ");

                BuyingFacade.DebtReceiptBL().Delete(debtReceipt.ReceiptCode);

                DebtSetupForCurrentYearBinding();
                DebtSetupFor3YearBinding();
                ReceiptByVoucherBinding();
                ReceiptForCurrentYearBinding();
                ReceiptFor3YearBinding();
                DebtBalanceCalculate();

                MessageBoxHelper.Info("ลบข้อมูลสำเร็จ");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CalculateDeductionPerKg()
        {
            try
            {
                DebtSetupForCurrentYearBinding();
                ReceiptForCurrentYearBinding();
                DebtBalanceCalculate();

                var dripProjectList = _debtSetupForCurrentYearList
                    .Where(x => x.DebtTypeCode == "DP").ToList();
                if (dripProjectList
                    .Where(x => x.DeductionCrop == _buyingDocument.Crop)
                    .Count() > 1)
                    throw new ArgumentException("มีการตั้งหนี้ในโครงการน้ำหยดมากกว่า 1 รายการในปีเดียว โปรดแจ้ง Admin จังหวัดเพื่อตรวจสอบข้อมูล");

                var currentDripProject = dripProjectList
                    .SingleOrDefault(x => x.DeductionCrop == _buyingDocument.Crop);
                if (currentDripProject == null)
                    throw new ArgumentException("ไม่พบยอดหนี้ที่จะหักในฤดูปลูกปัจจุบัน");

                _deductionPricePerKg = (double)currentDripProject.DeductPerKg;
                var a = _deductionPricePerKg * _buyingWeight;
                var b = _debtBalanceAmountForCurrentYear;
                _deductionAmountPerKg = a < b ? a : b;
                _netBuyingAmountForDeductionByKg = _buyingAmount - _voucherDeductionAmount - _deductionAmountPerKg;
                RaisePropertyChangedEvent(nameof(DeductionPricePerKg));
                RaisePropertyChangedEvent(nameof(DeductionAmountPerKg));
                RaisePropertyChangedEvent(nameof(NetBuyingAmountForDeductionByKg));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeductionFor3YearBinding()
        {
            try
            {
                DebtSetupFor3YearBinding();
                ReceiptFor3YearBinding();
                DebtBalanceCalculate();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DebtSetupForCurrentYearBinding()
        {
            try
            {
                _debtSetupForCurrentYearList = BuyingFacade.DebtSetupBL().GetDripProjectByCurrentCrop(_buyingDocument.Crop, _buyingDocument.FarmerCode);
                _totalDebtForCurrentYearAmount = (double)_debtSetupForCurrentYearList.Sum(x => x.Amount);
                RaisePropertyChangedEvent(nameof(DebtSetupForCurrentYearList));
                RaisePropertyChangedEvent(nameof(TotalDebtForCurrentYearAmount));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DebtSetupFor3YearBinding()
        {
            try
            {
                _debtSetupFor3YearList = BuyingFacade.DebtSetupBL().GetDripProjectBy3YearConntinue(_buyingDocument.Crop, _buyingDocument.FarmerCode);
                _totalDebtFor3YearAmount = (double)_debtSetupFor3YearList.Sum(x => x.Amount);
                RaisePropertyChangedEvent(nameof(DebtSetupFor3YearList));
                RaisePropertyChangedEvent(nameof(TotalDebtFor3YearAmount));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ReceiptByVoucherBinding()
        {
            try
            {
                var doc = _buyingDocument;
                _receiptByVoucherList = DebtReceiptHelper.GetByBuyingDocumentCode(doc.Crop,
                    doc.BuyingStationCode,
                    doc.FarmerCode,
                    doc.BuyingDocumentNumber
                    );
                _voucherDeductionAmount = (double)_receiptByVoucherList.Sum(x => x.Amount);
                RaisePropertyChangedEvent(nameof(ReceiptByVoucherList));
                RaisePropertyChangedEvent(nameof(VoucherDeductionAmount));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ReceiptForCurrentYearBinding()
        {
            try
            {
                _receiptForCurrentYearList = BuyingFacade.DebtReceiptBL()
                    .GetFarmerDripProjectByDeductionCrop(_buyingDocument.Crop, _buyingDocument.FarmerCode);
                _totalReceiptAmountForCurrentYear = (double)_receiptForCurrentYearList.Sum(x => x.Amount);
                RaisePropertyChangedEvent(nameof(ReceiptForCurrentYearList));
                RaisePropertyChangedEvent(nameof(TotalReceiptAmountForCurrentYear));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ReceiptFor3YearBinding()
        {
            try
            {
                _receiptFor3YearList = BuyingFacade.DebtReceiptBL()
                    .GetFarmerDripProjectBy3YearContinue(_buyingDocument.Crop, _buyingDocument.FarmerCode);
                _totalReceiptAmountFor3YearContinue = (double)_receiptFor3YearList.Sum(x => x.Amount);
                RaisePropertyChangedEvent(nameof(ReceiptFor3YearList));
                RaisePropertyChangedEvent(nameof(TotalReceiptAmountFor3YearContinue));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DebtBalanceCalculate()
        {
            try
            {
                if (_deductOption1 || _deductOption2)
                {
                    _debtBalanceAmountForCurrentYear = _totalDebtForCurrentYearAmount - _totalReceiptAmountForCurrentYear;
                    _netBuyingAmountForCurrentYear = _buyingAmount - _voucherDeductionAmount - _debtBalanceAmountForCurrentYear;
                    RaisePropertyChangedEvent(nameof(DebtBalanceAmountForCurrentYear));
                    RaisePropertyChangedEvent(nameof(NetBuyingAmountForCurrentYear));
                }
                else if (_deductOption3)
                {
                    _debtBalanceAmountFor3YearContinue = _totalDebtFor3YearAmount - _totalReceiptAmountFor3YearContinue;
                    _netBuyingAmountFor3YearContinue = _buyingAmount - _voucherDeductionAmount - _debtBalanceAmountFor3YearContinue;
                    RaisePropertyChangedEvent(nameof(DebtBalanceAmountFor3YearContinue));
                    RaisePropertyChangedEvent(nameof(NetBuyingAmountFor3YearContinue));
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
