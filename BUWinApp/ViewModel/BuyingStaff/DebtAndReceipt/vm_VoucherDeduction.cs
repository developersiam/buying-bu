﻿using BusinessLayer;
using BusinessLayer.BL;
using BusinessLayer.Helper;
using BusinessLayer.Model;
using BUWinApp.Helper;
using BUWinApp.MVVM;
using BUWinApp.View.BuyingStaff.DebtAndReceipt;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace BUWinApp.ViewModel.BuyingStaff.DebtAndReceipt
{
    public class vm_VoucherDeduction : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_VoucherDeduction()
        {
        }


        #region Properties
        private bool _showAll;

        public bool ShowAll
        {
            get { return _showAll; }
            set
            {
                _showAll = value;

                if (value == true)
                {
                    _showAllDebt = false;
                    _showCurrentCropDebt = false;
                    RaisePropertyChangedEvent(nameof(ShowAllDebt));
                    RaisePropertyChangedEvent(nameof(ShowCurrentCropDebt));
                    DebtSummaryBinding();
                }
            }
        }

        private bool _showCurrentCropDebt;

        public bool ShowCurrentCropDebt
        {
            get { return _showCurrentCropDebt; }
            set
            {
                _showCurrentCropDebt = value;
                if (value == true)
                {
                    _showAllDebt = false;
                    _showAll = false;
                    RaisePropertyChangedEvent(nameof(ShowAllDebt));
                    RaisePropertyChangedEvent(nameof(ShowAll));
                    DebtSummaryBinding();
                }
            }
        }

        private bool _showAllDebt;

        public bool ShowAllDebt
        {
            get { return _showAllDebt; }
            set
            {
                _showAllDebt = value;

                if (value == true)
                {
                    _showCurrentCropDebt = false;
                    _showAll = false;
                    RaisePropertyChangedEvent(nameof(ShowCurrentCropDebt));
                    RaisePropertyChangedEvent(nameof(ShowAll));
                    DebtSummaryBinding();
                }
            }
        }

        private BuyingDocument _buyingDocument;

        public BuyingDocument BuyingDocument
        {
            get { return _buyingDocument; }
            set
            {
                _buyingDocument = value;
                ReceiptListBinding();
            }
        }

        private decimal _totalDebtAmount;

        public decimal TotalDebtAmount
        {
            get { return _totalDebtAmount; }
            set { _totalDebtAmount = value; }
        }

        private decimal _totalReceiptAmount;

        public decimal TotalReceiptAmount
        {
            get { return _totalReceiptAmount; }
            set { _totalReceiptAmount = value; }
        }

        private decimal _debtBalanceAmount;

        public decimal DebtBalanceAmount
        {
            get { return _debtBalanceAmount; }
            set { _debtBalanceAmount = value; }
        }

        private decimal _deductionInVoucherAmount;

        public decimal DeductionInVoucherAmount
        {
            get { return _deductionInVoucherAmount; }
            set { _deductionInVoucherAmount = value; }
        }
        #endregion



        #region List
        private List<m_DebtAndReceiptSummary> _debtSummaryList;

        public List<m_DebtAndReceiptSummary> DebtSummaryList
        {
            get { return _debtSummaryList; }
            set { _debtSummaryList = value; }
        }

        private List<DebtReceipt> _receiptInVoucherList;

        public List<DebtReceipt> ReceiptInVoucherList
        {
            get { return _receiptInVoucherList; }
            set { _receiptInVoucherList = value; }
        }

        private List<Buying> _buyingList;

        public List<Buying> BuyingList
        {
            get { return _buyingList; }
            set { _buyingList = value; }
        }
        #endregion



        #region Command
        private ICommand _onDeductionCommand;

        public ICommand OnDeductionCommand
        {
            get { return _onDeductionCommand ?? (_onDeductionCommand = new RelayCommand(Deduction)); }
            set { _onDeductionCommand = value; }
        }

        private void Deduction(object obj)
        {
            try
            {
                var selected = (m_DebtAndReceiptSummary)obj;
                if (selected == null)
                    return;

                var debtSetup = BuyingFacade.DebtSetupBL().GetSingle(selected.DebtSetupCode);
                if (debtSetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล DebtSetupCode " +
                        selected.DebtSetupCode + " ในระบบ");

                if (debtSetup.DebtTypeCode == "CI")
                {
                    var window = new DeductionCropInput();
                    var vm = new vm_DeductionCropInput();
                    window.DataContext = vm;
                    vm.Window = window;
                    vm.DebtSetup = debtSetup;
                    vm.BuyingList = _buyingList;
                    vm.BuyingDocument = _buyingDocument;
                    window.ShowDialog();
                }
                else
                {
                    if (debtSetup.DeductionCrop == _buyingDocument.Crop)
                    {
                        if (MessageBoxHelper.Question("ท่านต้องการหักชำระทั้งหมดใช่หรือไม่? " + 
                            Environment.NewLine +
                            "Yes = ชำระที่เหลือทั้งหมด" + 
                            Environment.NewLine +
                            "No = ชำระโดยหักเงินตามน้ำหนักยาที่มาขาย")
                            == MessageBoxResult.No)
                        {
                            var window = new DeductionTHBPerKg();
                            var vm = new vm_DeductionTHBPerKg();
                            window.DataContext = vm;
                            vm.Window = window;
                            vm.DebtSetup = debtSetup;
                            vm.BuyingList = _buyingList;
                            vm.BuyingDocument = _buyingDocument;
                            window.ShowDialog();
                        }
                        else
                        {
                            var window = new DeductionAllBalance();
                            var vm = new vm_DeductionAllBalance();
                            window.DataContext = vm;
                            vm.Window = window;
                            vm.DebtSetup = debtSetup;
                            vm.BuyingList = _buyingList;
                            vm.BuyingDocument = _buyingDocument;
                            window.ShowDialog();
                        }
                    }
                    else
                    {
                        var window = new DeductionAllBalance();
                        var vm = new vm_DeductionAllBalance();
                        window.DataContext = vm;
                        vm.Window = window;
                        vm.DebtSetup = debtSetup;
                        vm.BuyingList = _buyingList;
                        vm.BuyingDocument = _buyingDocument;
                        window.ShowDialog();
                    }
                }

                ReceiptListBinding();
                DebtSummaryBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeleteReceiptCommand;

        public ICommand OnDeleteReceiptCommand
        {
            get { return _onDeleteReceiptCommand ?? (_onDeleteReceiptCommand = new RelayCommand(OnDeleteReceipt)); }
            set { _onDeleteReceiptCommand = value; }
        }

        private void OnDeleteReceipt(object obj)
        {
            try
            {
                var receipt = (DebtReceipt)obj;
                if (receipt == null)
                    return;

                if (_buyingDocument.IsAccountFinish == true)
                    throw new ArgumentException("voucher นี้ถูกล็อคแล้วจากแผนกบัญชี");

                if (_buyingDocument.IsPrinted == true)
                    throw new ArgumentException("voucher นี้ถูกพิมพ์ออกจากระบบแล้วเมื่อ " +
                        _buyingDocument.PrintDate + " ไม่สามารถลบข้อมูลการหักหนี้นี้ได้");

                if (MessageBoxHelper.Question("ท่านต้องการลบยอดชำระนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                if (receipt.ReceiptTypeCode != "RB")
                    throw new ArgumentException("ท่านสามาลบได้เฉพาะยอดชำระที่เป็นประเภทหักเงินจากการขายเท่านั้น");

                var debtSetup = BuyingFacade.DebtSetupBL().GetSingle(receipt.DebtSetupCode);
                if (debtSetup == null)
                    throw new ArgumentException("ไม่พบยอดหนี้รหัส " + debtSetup.DebtSetupCode + " ในระบบ");

                BuyingFacade.DebtReceiptBL().Delete(receipt.ReceiptCode);
                DebtSummaryBinding();
                ReceiptListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDebtDetailsCommand;

        public ICommand OnDebtDetailsCommand
        {
            get { return _onDebtDetailsCommand ?? (_onDebtDetailsCommand = new RelayCommand(DebtDetails)); }
            set { _onDebtDetailsCommand = value; }
        }

        private void DebtDetails(object obj)
        {
            try
            {
                var model = (m_DebtAndReceiptSummary)obj;
                var window = new DebtSetupDetails();
                var vm = new vm_DebtSetupDetails();
                window.DataContext = vm;
                vm.DebtSetupCode = model.DebtSetupCode;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #endregion



        #region Function
        private void DebtSummaryBinding()
        {
            try
            {
                var all = DebtSetupHelper.GetByFarmer(_buyingDocument.FarmerCode).ToList();
                var balanceAll = all.Where(x => x.BalanceAmount > 0).ToList();
                if (_showAll)
                    _debtSummaryList = all;
                else if (_showAllDebt)
                    _debtSummaryList = balanceAll;
                else
                    _debtSummaryList = balanceAll
                        .Where(x => x.DeductionCrop == _buyingDocument.Crop)
                        .ToList();

                _totalDebtAmount = _debtSummaryList.Sum(x => x.Amount);
                _totalReceiptAmount = _debtSummaryList.Sum(x => x.ReceiptAmount);
                _debtBalanceAmount = _totalDebtAmount - _totalReceiptAmount;
                RaisePropertyChangedEvent(nameof(DebtSummaryList));
                RaisePropertyChangedEvent(nameof(TotalDebtAmount));
                RaisePropertyChangedEvent(nameof(TotalReceiptAmount));
                RaisePropertyChangedEvent(nameof(DebtBalanceAmount));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ReceiptListBinding()
        {
            try
            {
                _receiptInVoucherList = BuyingFacade.DebtReceiptBL()
                    .GetByBuyingDocument(_buyingDocument.Crop,
                    _buyingDocument.BuyingStationCode,
                    _buyingDocument.FarmerCode,
                    _buyingDocument.BuyingDocumentNumber);
                _deductionInVoucherAmount = _receiptInVoucherList.Sum(x => x.Amount);
                RaisePropertyChangedEvent(nameof(ReceiptInVoucherList));
                RaisePropertyChangedEvent(nameof(DeductionInVoucherAmount));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
