﻿using BusinessLayer;
using BusinessLayer.Helper;
using BusinessLayer.Model;
using BUWinApp.Helper;
using BUWinApp.MVVM;
using BUWinApp.View.BuyingStaff;
using DomainModel;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BUWinApp.ViewModel.BuyingStaff.DebtAndReceipt
{
    public class vm_Voucher : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_Voucher()
        {
            DataGridBinding();
        }

        private List<m_BuyingDocument> _buyingDocumentList;

        public List<m_BuyingDocument> BuyingDocumentList
        {
            get { return _buyingDocumentList; }
            set { _buyingDocumentList = value; }
        }

        private ICommand _onRefreshCommand;

        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(Refresh)); }
            set { _onRefreshCommand = value; }
        }

        private void Refresh(object obj)
        {
            DataGridBinding();
        }

        private ICommand _onDetailsCommand;

        public ICommand OnDetailsCommand
        {
            get { return _onDetailsCommand ?? (_onDetailsCommand = new RelayCommand(Details)); }
            set { _onDetailsCommand = value; }
        }

        private void Details(object obj)
        {
            try
            {
                var model = (m_BuyingDocument)obj;
                if (model == null)
                    return;

                var window = new VoucherDetails();
                var vm = new vm_VoucherDetails();
                vm.Document = model;
                window.DataContext = vm;
                window.ShowDialog();
                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DataGridBinding()
        {
            _buyingDocumentList = BuyingDocumentHelper.GetByFarmer(2023, "A002485");
            RaisePropertyChangedEvent(nameof(BuyingDocumentList));
        }
    }
}
