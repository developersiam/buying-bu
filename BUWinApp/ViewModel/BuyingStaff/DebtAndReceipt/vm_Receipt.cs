﻿using BusinessLayer;
using BusinessLayer.Helper;
using BusinessLayer.Model;
using BUWinApp.Helper;
using BUWinApp.MVVM;
using BUWinApp.View.BuyingStaff.DebtAndReceipt;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace BUWinApp.ViewModel.BuyingStaff.DebtAndReceipt
{
    public class vm_Receipt : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_Receipt()
        {
        }

        #region Properties

        private short _crop;

        public short Crop
        {
            get { return _crop; }
            set { _crop = value; }
        }

        private string _farmerCode;

        public string FarmerCode
        {
            get { return _farmerCode; }
            set
            {
                _farmerCode = value;
                ReceiptListBinding();
            }
        }

        private decimal _totalReceiptAmount;

        public decimal TotalReceiptAmount
        {
            get { return _totalReceiptAmount; }
            set { _totalReceiptAmount = value; }
        }
        #endregion


        #region List
        private List<DebtReceipt> _receiptList;

        public List<DebtReceipt> ReceiptList
        {
            get { return _receiptList; }
            set { _receiptList = value; }
        }
        #endregion


        #region Command
        private ICommand _onRefreshCommand;

        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(OnRefresh)); }
            set { _onRefreshCommand = value; }
        }

        private void OnRefresh(object obj)
        {
            ReceiptListBinding();
        }
        #endregion


        #region Function
        private void ReceiptListBinding()
        {
            try
            {
                _receiptList = BuyingFacade.DebtReceiptBL()
                    .GetByFarmerDeductionCrop(_crop, _farmerCode)
                    .Select(x => new DebtReceipt
                    {
                        ReceiptCode = x.ReceiptCode,
                        ReceiptBy = x.ReceiptBy,
                        ReceiptDate = x.ReceiptDate,
                        ReceiptTypeCode = x.ReceiptTypeCode,
                        Amount = x.Amount,
                        DebtSetupCode = x.DebtSetupCode,
                        ModifiedBy = x.Crop +
                        x.BuyingStationCode +
                        x.FarmerCode +
                        x.BuyingDocumentNumber
                        .ToString()
                        .PadLeft(2, '0'),
                    })
                    .ToList();
                _totalReceiptAmount = _receiptList.Sum(x => x.Amount);
                RaisePropertyChangedEvent(nameof(ReceiptList));
                RaisePropertyChangedEvent(nameof(TotalReceiptAmount));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
