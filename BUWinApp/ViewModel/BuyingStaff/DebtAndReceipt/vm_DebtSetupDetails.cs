﻿using BusinessLayer;
using BusinessLayer.Helper;
using BusinessLayer.Model;
using BUWinApp.Helper;
using BUWinApp.MVVM;
using DomainModel;
using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace BUWinApp.ViewModel.BuyingStaff.DebtAndReceipt
{
    public class vm_DebtSetupDetails : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_DebtSetupDetails()
        {
        }

        #region Properties
        private short _crop;

        public short Crop
        {
            get { return _crop; }
            set { _crop = value; }
        }

        private string _farmerCode;

        public string FarmerCode
        {
            get { return _farmerCode; }
            set { _farmerCode = value; }
        }

        private string _debtSetupCode;

        public string DebtSetupCode
        {
            get { return _debtSetupCode; }
            set
            {
                _debtSetupCode = value;
                RaisePropertyChangedEvent(nameof(DebtSetupCode));
                InvoiceListBinding();
                ReceiptListBinding();
            }
        }

        private double _debtSetupAmount;

        public double DebtSetupAmount
        {
            get { return _debtSetupAmount; }
            set { _debtSetupAmount = value; }
        }

        private double _invoiceAmount;

        public double InvoiceAmount
        {
            get { return _invoiceAmount; }
            set { _invoiceAmount = value; }
        }

        private double _receiptTotalAmount;

        public double ReceiptTotalAmount
        {
            get { return _receiptTotalAmount; }
            set { _receiptTotalAmount = value; }
        }

        private m_AccountInvoiceSummary _selectedInvoice;

        public m_AccountInvoiceSummary SelectedInvoice
        {
            get { return _selectedInvoice; }
            set { _selectedInvoice = value; }
        }

        #endregion


        #region List
        private List<m_AccountInvoiceSummary> _invoiceList;

        public List<m_AccountInvoiceSummary> InvoiceList
        {
            get { return _invoiceList; }
            set { _invoiceList = value; }
        }

        private List<m_InvoiceDetails> _invoiceDetailsList;

        public List<m_InvoiceDetails> InvoiceDetailsList
        {
            get { return _invoiceDetailsList; }
            set { _invoiceDetailsList = value; }
        }

        private List<DebtReceipt> _receiptList;

        public List<DebtReceipt> ReceiptList
        {
            get { return _receiptList; }
            set { _receiptList = value; }
        }
        #endregion


        #region Command
        private ICommand _onAccountInvoiceSelected;

        public ICommand OnAccountInvoiceSelected
        {
            get { return _onAccountInvoiceSelected ?? (_onAccountInvoiceSelected = new RelayCommand(AccountInvoiceSelected)); }
            set { _onAccountInvoiceSelected = value; }
        }

        private void AccountInvoiceSelected(object obj)
        {
            try
            {
                var model = (m_AccountInvoiceSummary)obj;
                if (model == null)
                    return;

                _selectedInvoice = model;
                RaisePropertyChangedEvent(nameof(SelectedInvoice));
                InvoiceDetailsListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion


        #region Function
        private void InvoiceListBinding()
        {
            try
            {
                _invoiceList = BuyingFacade.DebtSetupAccountInvoiceBL()
                    .GetByDebSetupCode(_debtSetupCode)
                    .Select(x => new m_AccountInvoiceSummary
                    {
                        InvoiceNo = x.AccountInvoiceNo,
                        Items = x.AccountInvoice.InvoiceDetails.Sum(y => y.Quantity),
                        Price = (double)x.AccountInvoice.InvoiceDetails.Sum(y => y.Quantity * y.UnitPrice),
                        PrintDate = x.AccountInvoice.PrintDate
                    })
                   .ToList();
                RaisePropertyChangedEvent(nameof(InvoiceList));

                _debtSetupAmount = (double)_invoiceList.Sum(x => x.Price);
                RaisePropertyChangedEvent(nameof(DebtSetupAmount));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void InvoiceDetailsListBinding()
        {
            try
            {
                if (_selectedInvoice == null)
                    throw new ArgumentException("Selected invoice cannot bt null.");
                _invoiceDetailsList = InvoiceDetailsHelper
                    .GetByAccountInvoiceNo(_selectedInvoice.InvoiceNo);
                RaisePropertyChangedEvent(nameof(InvoiceDetailsList));

                _invoiceAmount = (double)_invoiceDetailsList.Sum(x => x.Price);
                RaisePropertyChangedEvent(nameof(InvoiceAmount));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ReceiptListBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_debtSetupCode))
                    throw new ArgumentException("DebSetupCode cannot be empty.");

                _receiptList = BuyingFacade.DebtReceiptBL().GetByDebtSetupCode(_debtSetupCode);
                RaisePropertyChangedEvent(nameof(ReceiptList));

                _receiptTotalAmount = (double)_receiptList.Sum(x => x.Amount);
                RaisePropertyChangedEvent(nameof(ReceiptTotalAmount));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
