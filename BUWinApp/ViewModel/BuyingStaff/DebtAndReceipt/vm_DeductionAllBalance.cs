﻿using BusinessLayer;
using BUWinApp.Helper;
using BUWinApp.MVVM;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace BUWinApp.ViewModel.BuyingStaff.DebtAndReceipt
{
    public class vm_DeductionAllBalance : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_DeductionAllBalance()
        {
        }

        #region Properties
        private Window _window;

        public Window Window
        {
            get { return _window; }
            set { _window = value; }
        }

        private BuyingDocument _buyingDocument;

        public BuyingDocument BuyingDocument
        {
            get { return _buyingDocument; }
            set
            {
                _buyingDocument = value;
                DeductionCalcultion();
            }
        }

        private decimal _buyingAmount;

        public decimal BuyingAmount
        {
            get { return _buyingAmount; }
            set { _buyingAmount = value; }
        }

        private decimal _buyingNetAmount;

        public decimal BuyingNetAmount
        {
            get { return _buyingNetAmount; }
            set { _buyingNetAmount = value; }
        }

        private DebtSetup _debtSetup;

        public DebtSetup DebtSetup
        {
            get { return _debtSetup; }
            set { _debtSetup = value; }
        }

        private decimal _voucherDeductionAmount;

        public decimal VoucherDeductionAmount
        {
            get { return _voucherDeductionAmount; }
            set { _voucherDeductionAmount = value; }
        }

        private decimal _debtReceiptAmount;

        public decimal DebtReceiptAmount
        {
            get { return _debtReceiptAmount; }
            set { _debtReceiptAmount = value; }
        }

        private decimal _debtBalanceAmount;

        public decimal DebtBalanceAmount
        {
            get { return _debtBalanceAmount; }
            set { _debtBalanceAmount = value; }
        }

        private decimal _buyingAmountBeforeDeduction;

        public decimal BuyingAmountBeforeDeduction
        {
            get { return _buyingAmountBeforeDeduction; }
            set { _buyingAmountBeforeDeduction = value; }
        }

        private decimal _deductionPriceAmount;

        public decimal DeductionPriceAmount
        {
            get { return _deductionPriceAmount; }
            set { _deductionPriceAmount = value; }
        }
        #endregion


        #region List
        private List<DebtReceipt> _receiptList;

        public List<DebtReceipt> ReceiptList
        {
            get { return _receiptList; }
            set { _receiptList = value; }
        }

        private List<Buying> _buyingList;

        public List<Buying> BuyingList
        {
            get { return _buyingList; }
            set { _buyingList = value; }
        }
        #endregion


        #region Command
        private ICommand _onDeductionCommand;

        public ICommand OnDeductionCommand
        {
            get { return _onDeductionCommand ?? (_onDeductionCommand = new RelayCommand(Deduction)); }
            set { _onDeductionCommand = value; }
        }

        private void Deduction(object obj)
        {
            try
            {
                if (_buyingNetAmount <= 0)
                    throw new ArgumentException("ยอดเงินจากการขายที่เหลือไม่พอหักชำระ");

                if (_debtBalanceAmount <= 0)
                    throw new ArgumentException("ยอดหนี้นี้ได้รับการชำระครบถ้วนแล้ว");

                var receiptInVoucher = _receiptList
                    .Where(x => x.DebtSetupCode == _debtSetup.DebtSetupCode &&
                    x.Crop == _buyingDocument.Crop &&
                    x.BuyingStationCode == _buyingDocument.BuyingStationCode &&
                    x.FarmerCode == _buyingDocument.FarmerCode &&
                    x.BuyingDocumentNumber == _buyingDocument.BuyingDocumentNumber
                    ).ToList();

                if (receiptInVoucher.Count() > 0)
                    throw new ArgumentException("มีการชำระยอดหนี้นี้แล้วในเวาเชอร์นี้");

                if (MessageBoxHelper.Question("ท่านได้ตรวจสอบข้อมูลแล้ว " +
                    "และต้องการบันทึกข้อมูลนี้ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                BuyingFacade.DebtReceiptBL()
                        .Add(_buyingDocument.Crop,
                        _buyingDocument.BuyingStationCode,
                        _buyingDocument.FarmerCode,
                        _buyingDocument.BuyingDocumentNumber,
                        "RB",
                        _deductionPriceAmount,
                        DateTime.Now,
                        user_setting.User.Username,
                        _debtSetup.DebtSetupCode);

                //DeductionCalcultion();
                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion


        #region Function
        private void DeductionCalcultion()
        {
            try
            {
                _receiptList = BuyingFacade.DebtReceiptBL()
                    .GetByDebtSetupCode(_debtSetup.DebtSetupCode);
                _debtReceiptAmount = _receiptList.Sum(x => x.Amount);
                _debtBalanceAmount = _debtSetup.Amount - _debtReceiptAmount;

                RaisePropertyChangedEvent(nameof(ReceiptList));
                RaisePropertyChangedEvent(nameof(DebtReceiptAmount));
                RaisePropertyChangedEvent(nameof(DebtBalanceAmount));

                _voucherDeductionAmount = BuyingFacade.DebtReceiptBL()
                    .GetByBuyingDocument(_buyingDocument.Crop,
                    _buyingDocument.BuyingStationCode,
                    _buyingDocument.FarmerCode,
                    _buyingDocument.BuyingDocumentNumber)
                    .Sum(x => x.Amount);
                _buyingAmount = (decimal)_buyingList
                    .Where(x => x.Grade != null)
                    .Sum(x => x.Weight * x.BuyingGrade.UnitPrice);
                _deductionPriceAmount = _debtBalanceAmount;
                _buyingAmountBeforeDeduction = _buyingAmount - _voucherDeductionAmount;
                _buyingNetAmount = _buyingAmountBeforeDeduction - _deductionPriceAmount;

                RaisePropertyChangedEvent(nameof(VoucherDeductionAmount));
                RaisePropertyChangedEvent(nameof(BuyingAmount));
                RaisePropertyChangedEvent(nameof(DeductionPriceAmount));
                RaisePropertyChangedEvent(nameof(BuyingAmountBeforeDeduction));
                RaisePropertyChangedEvent(nameof(BuyingNetAmount));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
