﻿using BusinessLayer;
using BusinessLayer.Helper;
using BusinessLayer.Model;
using BUWinApp.Helper;
using BUWinApp.MVVM;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace BUWinApp.ViewModel.BuyingStaff.DebtAndReceipt
{
    public class vm_DeductionInVoucherDetails : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_DeductionInVoucherDetails()
        {
        }


        #region Properties
        private BuyingDocument _buyingDocument;

        public BuyingDocument BuyingDocument
        {
            get { return _buyingDocument; }
            set
            {
                _buyingDocument = value;
                DebtReceiptListByBuyingDocumentBinding();
            }
        }

        private double _deductionInVoucherAmount;

        public double DeductionInVoucherAmount
        {
            get { return _deductionInVoucherAmount; }
            set { _deductionInVoucherAmount = value; }
        }

        private List<m_DebtReceipt> _receiptList;

        public List<m_DebtReceipt> ReceiptList
        {
            get { return _receiptList; }
            set { _receiptList = value; }
        }
        #endregion



        #region Command
        private ICommand _onDeleteReceiptCommand;

        public ICommand OnDeleteReceiptCommand
        {
            get { return _onDeleteReceiptCommand ?? (_onDeleteReceiptCommand = new RelayCommand(OnDeleteReceipt)); }
            set { _onDeleteReceiptCommand = value; }
        }

        private void OnDeleteReceipt(object obj)
        {
            try
            {
                var receipt = (m_DebtReceipt)obj;
                if (receipt == null)
                    return;

                Delete(receipt);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion



        #region Function
        private void DebtReceiptListByBuyingDocumentBinding()
        {
            try
            {
                _receiptList = DebtReceiptHelper.GetByBuyingDocumentCode(
                    _buyingDocument.Crop,
                    _buyingDocument.BuyingStationCode,
                    _buyingDocument.FarmerCode,
                    _buyingDocument.BuyingDocumentNumber
                    );
                _deductionInVoucherAmount = (double)_receiptList.Sum(x => x.Amount);
                RaisePropertyChangedEvent(nameof(ReceiptList));
                RaisePropertyChangedEvent(nameof(DeductionInVoucherAmount));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Delete(m_DebtReceipt debtReceipt)
        {
            try
            {
                if (_buyingDocument.IsAccountFinish == true)
                    throw new ArgumentException("voucher นี้ถูกล็อคแล้วจากแผนกบัญชี");

                if (_buyingDocument.IsPrinted == true)
                    throw new ArgumentException("voucher นี้ถูกพิมพ์ออกจากระบบแล้วเมื่อ " +
                        _buyingDocument.PrintDate + " ไม่สามารถลบข้อมูลการหักหนี้นี้ได้");

                if (MessageBoxHelper.Question("ท่านต้องการลบยอดชำระนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                if (debtReceipt.ReceiptTypeCode != "RB")
                    throw new ArgumentException("ท่านสามาลบได้เฉพาะยอดชำระที่เป็นประเภทหักเงินจากการขายเท่านั้น");

                var debtSetup = BuyingFacade.DebtSetupBL().GetSingle(debtReceipt.DebtSetupCode);
                if (debtSetup == null)
                    throw new ArgumentException("ไม่พบยอดหนี้รหัส " + debtSetup.DebtSetupCode + " ในระบบ");

                BuyingFacade.DebtReceiptBL().Delete(debtReceipt.ReceiptCode);
                DebtReceiptListByBuyingDocumentBinding();
                MessageBoxHelper.Info("ลบข้อมูลสำเร็จ");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion

    }
}
