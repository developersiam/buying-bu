﻿using BusinessLayer;
using BusinessLayer.BL;
using BusinessLayer.Helper;
using BusinessLayer.Model;
using BUWinApp.Helper;
using BUWinApp.MVVM;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Input;

namespace BUWinApp.ViewModel.BuyingStaff.DebtAndReceipt
{
    public class vm_DeductionCropInput : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_DeductionCropInput()
        {
            _deductionPercentages = 100;
            RaisePropertyChangedEvent(nameof(DeductionPercentages));
        }

        #region Properties
        private Window _window;

        public Window Window
        {
            get { return _window; }
            set { _window = value; }
        }

        private DebtSetup _debtSetup;

        public DebtSetup DebtSetup
        {
            get { return _debtSetup; }
            set { _debtSetup = value; }
        }

        private double _buyingAmount;

        public double BuyingAmount
        {
            get { return _buyingAmount; }
            set { _buyingAmount = value; }
        }

        private double _buyingAmountBeforeDeduction;

        public double BuyingAmountBeforeDeduction
        {
            get { return _buyingAmountBeforeDeduction; }
            set { _buyingAmountBeforeDeduction = value; }
        }

        private double _remainingDebtBalance;

        public double RemainingDebtBalance
        {
            get { return _remainingDebtBalance; }
            set { _remainingDebtBalance = value; }
        }

        private int _deductionPercentages;

        public int DeductionPercentages
        {
            get { return _deductionPercentages; }
            set
            {
                _deductionPercentages = value;
                CalculationDeduction();
            }
        }

        private double _deductionAmount;

        public double DeductionAmount
        {
            get { return _deductionAmount; }
            set { _deductionAmount = value; }
        }

        private double _netDebtAmount;

        public double NetDebtAmount
        {
            get { return _netDebtAmount; }
            set { _netDebtAmount = value; }
        }

        private double _netBuyingAmount;

        public double NetBuyingAmount
        {
            get { return _netBuyingAmount; }
            set { _netBuyingAmount = value; }
        }

        private BuyingDocument _buyingDocument;

        public BuyingDocument BuyingDocument
        {
            get { return _buyingDocument; }
            set
            {
                _buyingDocument = value;
                RaisePropertyChangedEvent(nameof(BuyingDocument));
                CalculatBuyingPrice();
                ReceiptListByBuyingDocumentBinding();
                ReceiptListByDebtCodeBinding();
                CalculateDebt();
                CalculationDeduction();
            }
        }

        private double _deductionInVoucherAmount;

        public double DeductionInVoucherAmount
        {
            get { return _deductionInVoucherAmount; }
            set { _deductionInVoucherAmount = value; }
        }

        private double _totalReceiptAmountByDebtCode;

        public double TotalReceiptAmountByDebtCode
        {
            get { return _totalReceiptAmountByDebtCode; }
            set { _totalReceiptAmountByDebtCode = value; }
        }
        #endregion


        #region List
        private List<int> _deductionPercentagesList;

        public List<int> DeductionPercentagesList
        {
            get
            {
                if (_deductionPercentagesList == null)
                {
                    _deductionPercentagesList = new List<int>();
                    _deductionPercentagesList.Add(100);
                    _deductionPercentagesList.Add(75);
                    _deductionPercentagesList.Add(50);
                    _deductionPercentagesList.Add(25);
                }

                return _deductionPercentagesList;
            }
            set { _deductionPercentagesList = value; }
        }

        private List<Buying> _buyingList;

        public List<Buying> BuyingList
        {
            get { return _buyingList; }
            set { _buyingList = value; }
        }

        private List<m_DebtReceipt> _debtReceiptListByDebtSetupCode;

        public List<m_DebtReceipt> DebtReceiptListByDebtSetupCode
        {
            get { return _debtReceiptListByDebtSetupCode; }
            set { _debtReceiptListByDebtSetupCode = value; }
        }

        private List<m_DebtReceipt> _receiptByVoucherList;

        public List<m_DebtReceipt> ReceiptByVoucherList
        {
            get { return _receiptByVoucherList; }
            set { _receiptByVoucherList = value; }
        }
        #endregion


        #region Command
        private ICommand _onDeductionCommand;

        public ICommand OnDeductionCommand
        {
            get { return _onDeductionCommand ?? (_onDeductionCommand = new RelayCommand(OnDeduction)); }
            set { _onDeductionCommand = value; }
        }

        private void OnDeduction(object obj)
        {
            try
            {
                var receiptInVoucher = _receiptByVoucherList
                    .Where(x => x.DebtSetupCode == _debtSetup.DebtSetupCode &&
                    x.Crop == _buyingDocument.Crop &&
                    x.BuyingStationCode == _buyingDocument.BuyingStationCode &&
                    x.FarmerCode == _buyingDocument.FarmerCode &&
                    x.BuyingDocumentNumber == _buyingDocument.BuyingDocumentNumber
                    ).ToList();

                if (receiptInVoucher.Count() > 0)
                    throw new ArgumentException("มีการชำระยอดหนี้นี้แล้วในเวาเชอร์นี้");

                if (MessageBoxHelper.Question("ท่านได้ตรวจสอบข้อมูลแล้ว " +
                    "และต้องการบันทึกข้อมูลนี้ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;
                // RB : Receipt From Buying. (รับชำระโดยหักเงินจากการขาย)
                // RC : Receipt From Cash. (รับชำระโดยเงินสด)
                BuyingFacade.DebtReceiptBL()
                    .Add(_buyingDocument.Crop,
                    _buyingDocument.BuyingStationCode,
                    _buyingDocument.FarmerCode,
                    _buyingDocument.BuyingDocumentNumber,
                    "RB",
                    (decimal)_deductionAmount,
                    DateTime.Now,
                    user_setting.User.Username,
                    _debtSetup.DebtSetupCode
                    );
                //ReceiptListByDebtCodeBinding();
                //ReceiptListByBuyingDocumentBinding();
                //CalculateDebt();
                //CalculationDeduction();
                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion


        #region Function
        private void ReceiptListByBuyingDocumentBinding()
        {
            try
            {
                _receiptByVoucherList = DebtReceiptHelper.GetByBuyingDocumentCode(
                    _buyingDocument.Crop,
                    _buyingDocument.BuyingStationCode,
                    _buyingDocument.FarmerCode,
                    _buyingDocument.BuyingDocumentNumber
                    );
                _deductionInVoucherAmount = (double)_receiptByVoucherList.Sum(x => x.Amount);
                RaisePropertyChangedEvent(nameof(ReceiptByVoucherList));
                RaisePropertyChangedEvent(nameof(DeductionInVoucherAmount));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ReceiptListByDebtCodeBinding()
        {
            try
            {
                _debtReceiptListByDebtSetupCode = DebtReceiptHelper.GetByDebtSetupCode(_debtSetup.DebtSetupCode);
                _totalReceiptAmountByDebtCode = (double)_debtReceiptListByDebtSetupCode.Sum(x => x.Amount);
                RaisePropertyChangedEvent(nameof(DebtReceiptListByDebtSetupCode));
                RaisePropertyChangedEvent(nameof(TotalReceiptAmountByDebtCode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CalculatBuyingPrice()
        {
            try
            {
                _buyingAmount = (double)_buyingList
                    .Where(x=>x.Grade != null)
                    .Sum(x => x.Weight * x.BuyingGrade.UnitPrice);
                RaisePropertyChangedEvent(nameof(BuyingAmount));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CalculateDebt()
        {
            try
            {
                _remainingDebtBalance = (double)(_debtSetup.Amount - _debtReceiptListByDebtSetupCode.Sum(x => x.Amount));
                _netDebtAmount = _remainingDebtBalance - _deductionAmount;
                RaisePropertyChangedEvent(nameof(RemainingDebtBalance));
                RaisePropertyChangedEvent(nameof(NetDebtAmount));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CalculationDeduction()
        {
            try
            {
                var percent = (decimal)_deductionPercentages / 100;
                _deductionAmount = (double)((decimal)_remainingDebtBalance * percent);
                _netDebtAmount = _remainingDebtBalance - _deductionAmount;
                _buyingAmountBeforeDeduction = _buyingAmount - _deductionInVoucherAmount;
                _netBuyingAmount = _buyingAmountBeforeDeduction - _deductionAmount;
                RaisePropertyChangedEvent(nameof(DeductionAmount));
                RaisePropertyChangedEvent(nameof(NetDebtAmount));
                RaisePropertyChangedEvent(nameof(BuyingAmountBeforeDeduction));
                RaisePropertyChangedEvent(nameof(NetBuyingAmount));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
