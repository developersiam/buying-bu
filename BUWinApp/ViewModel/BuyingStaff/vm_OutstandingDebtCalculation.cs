﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using System.Windows.Input;
using DomainModel;
using BusinessLayer;
using BUWinApp.Helper;
using BusinessLayer.Helper;
using BusinessLayer.Model;
using System.Windows;

namespace BUWinApp.ViewModel.BuyingStaff
{
    public class vm_OutstandingDebtCalculation : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_OutstandingDebtCalculation()
        {

        }



        #region Properties
        private m_BuyingDocument _document;
        public m_BuyingDocument Document
        {
            get { return _document; }
            set
            {
                _document = value;

                _buyingDocumentCode = _document.Crop + "-" +
                    _document.BuyingStationCode + "-" +
                    _document.FarmerCode + "-" +
                    _document.BuyingDocumentNumber;

                PaymentHistoryBinding();
                RaisePropertyChangedEvent(nameof(BuyingDocumentCode));
            }
        }

        private PaymentHistory _paymentHistory;
        public PaymentHistory PaymentHistory
        {
            get { return _paymentHistory; }
            set { _paymentHistory = value; }
        }

        private string _buyingDocumentCode;
        public string BuyingDocumentCode
        {
            get { return _buyingDocumentCode; }
            set { _buyingDocumentCode = value; }
        }

        private decimal _totalDebt;
        public decimal TotalDebt
        {
            get { return _totalDebt; }
            set { _totalDebt = value; }
        }

        private decimal _creditNoteAmount;

        public decimal CreditNoteAmount
        {
            get { return _creditNoteAmount; }
            set { _creditNoteAmount = value; }
        }

        private decimal _totalPayment;
        public decimal TotalPayment
        {
            get { return _totalPayment; }
            set { _totalPayment = value; }
        }

        private decimal _remainingDebt;
        public decimal RemainingDebt
        {
            get { return _remainingDebt; }
            set { _remainingDebt = value; }
        }

        private decimal _buyingAmount;
        public decimal BuyingAmount
        {
            get { return _buyingAmount; }
            set { _buyingAmount = value; }
        }

        private decimal _balanceBuyingAmount;
        public decimal BalanceBuyingAmount
        {
            get { return _balanceBuyingAmount; }
            set { _balanceBuyingAmount = value; }
        }

        private DateTime _paymentDate;
        public DateTime PaymentDate
        {
            get { return _paymentDate; }
            set { _paymentDate = value; }
        }

        private decimal _remainingAfter;
        public decimal RemainingAfter
        {
            get { return _remainingAfter; }
            set { _remainingAfter = value; }
        }

        private short _percentage;
        public short Percentage
        {
            get { return _percentage; }
            set
            {
                _percentage = value;

                _paymentAmount = _remainingDebt * Convert.ToDecimal(_percentage / 100.0);
                _remainingAfter = _remainingDebt - _paymentAmount;
                _balanceBuyingAmount = _buyingAmount - _paymentAmount;

                if (_buyingAmount < _paymentAmount)
                    MessageBoxHelper.Warning("ยอดเงินจากการขายครั้งนี้ไม่พอหักชำระหนี้");

                RaisePropertyChangedEvent(nameof(PaymentAmount));
                RaisePropertyChangedEvent(nameof(RemainingAfter));
                RaisePropertyChangedEvent(nameof(BalanceBuyingAmount));
            }
        }

        private decimal _paymentAmount;
        public decimal PaymentAmount
        {
            get { return _paymentAmount; }
            set { _paymentAmount = value; }
        }

        private bool _isFinish;
        public bool IsFinish
        {
            get { return _isFinish; }
            set { _isFinish = value; }
        }

        private Visibility _isAccounting;
        public Visibility IsAccounting
        {
            get { return _isAccounting; }
            set { _isAccounting = value; }
        }
        #endregion



        #region List
        public List<short> PercentageList
        {
            get
            {
                var list = new List<short>();

                list.Add(100);
                list.Add(75);
                list.Add(50);
                list.Add(25);

                if (UserAccountHelper.IsInRole("Supervisor"))
                    list.Add(0);

                return list;
            }
        }
        #endregion



        #region Command
        private ICommand _onPaymentCommand;
        public ICommand OnPaymentCommand
        {
            get { return _onPaymentCommand ?? (_onPaymentCommand = new RelayCommand(OnPayment)); }
            set { _onPaymentCommand = value; }
        }

        private void OnPayment(object obj)
        {
            try
            {
                if (_percentage < 0 && !UserAccountHelper.IsInRole("Supervisor"))
                {
                    MessageBoxHelper.Warning("โปรดระบุเปอร์เซ็นของยอดหนี้ที่จะหัก");
                    OnFocusRequested(nameof(_percentage));
                    return;
                }

                if (_buyingAmount < _paymentAmount)
                {
                    MessageBoxHelper.Warning("ยอดเงินจากการขายครั้งนี้ไม่พอหักชำระหนี้");
                    OnFocusRequested(nameof(BuyingAmount));
                    return;
                }

                if (IsFinish == true)
                    throw new ArgumentException("หารหักชำระยอดหนี้นี้ได้ถูกเปลี่ยนสถานะเป็น Finish แล้วไม่สามารถดำเนินการแก้ไขได้");

                if (MessageBoxHelper.Question(
                    " ยอดขายรวมครั้งนี้ " + _buyingAmount.ToString("N1") +
                    " ยอดเงินที่จะหักคือ " + _paymentAmount.ToString("N1") + " บาท " +
                    " ยอดเงินคงเหลือหลังการหักคือ " + (_buyingAmount - _paymentAmount).ToString("N1") + " บาท" +
                    " ท่านต้องการหักชำระในครั้งนี้ใช่หรือไม่?"
                    ) == MessageBoxResult.No)
                    return;

                BuyingFacade.PaymentHistoryBL()
                    .Deduct(_document.Crop,
                    _document.BuyingStationCode,
                    _document.FarmerCode,
                    _document.BuyingDocumentNumber,
                    _paymentDate,
                    _percentage,
                    user_setting.User.Username);

                MessageBoxHelper.Info("หักชำระนี้ในการขายครั้งนี้สำเร็จ (จำนวน " +
                    _paymentAmount.ToString("N1") + " บาท)");

                PaymentHistoryBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onFinishCommand;

        public ICommand OnFinishCommand
        {
            get { return _onFinishCommand ?? (_onFinishCommand = new RelayCommand(OnFinish)); }
            set { _onFinishCommand = value; }
        }

        private void OnFinish(object obj)
        {
            try
            {
                if (user_setting.UserRoles
                    .Where(x => x.RoleName == "Accounting")
                    .Count() <= 0 && _percentage < 25)
                {
                    MessageBoxHelper.Warning("โปรดระบุเปอร์เซ็นของยอดหนี้ที่จะหัก");
                    OnFocusRequested(nameof(Percentage));
                    return;
                }

                if (_buyingAmount < _paymentAmount)
                {
                    MessageBoxHelper.Warning("ยอดขายในครั้งนี้ไม่พอหักชำระหนี้ได้ โปรดตรวจสอบกับเจ้าหน้าที่");
                    OnFocusRequested(nameof(BuyingAmount));
                    return;
                }

                if (IsFinish == true)
                    throw new ArgumentException("การหักชำระยอดหนี้นี้ได้ถูกเปลี่ยนสถานะเป็น Finish แล้วไม่สามารถดำเนินการแก้ไขได้");

                if (MessageBoxHelper.Question("หลังจากนี้จะไม่สามารถแก้ไขข้อมูลได้อีก ท่านต้องการหักชำระในครั้งนี้ใช่หรือไม่?" +
                    " ยอดเงินคงเหลือหลังการหักคือ " + (_buyingAmount - _paymentAmount).ToString("N1") + " บาท")
                    == MessageBoxResult.No)
                    return;

                BuyingFacade.PaymentHistoryBL()
                    .Finish(_document.Crop,
                    _document.BuyingStationCode,
                    _document.FarmerCode,
                    _document.BuyingDocumentNumber,
                    user_setting.User.Username);

                MessageBoxHelper.Info("Finish การหักชำระหนี้เรียบร้อย");
                PaymentHistoryBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onUnfinishCommand;

        public ICommand OnUnfinishCommand
        {
            get { return _onUnfinishCommand ?? (_onUnfinishCommand = new RelayCommand(OnUnfinish)); }
            set { _onUnfinishCommand = value; }
        }

        private void OnUnfinish(object obj)
        {
            try
            {
                if (user_setting.UserRoles.Where(x => x.RoleName == "Accounting").Count() <= 0)
                    throw new ArgumentException("ท่านจะต้องเป็น Accounting เท่านั้นถึงจะเข้าถึงฟังก์ชันนี้ได้");

                if (MessageBoxHelper.Question("หลังจากนี้จะไม่สามารถแก้ไขข้อมูลได้อีก ท่านต้องการหักชำระในครั้งนี้ใช่หรือไม่?" +
                    " ยอดเงินคงเหลือหลังการหักคือ " + (_buyingAmount - _paymentAmount).ToString("N1") + " บาท")
                    == MessageBoxResult.No)
                    return;

                BuyingFacade.PaymentHistoryBL()
                    .UnFinish(_document.Crop,
                    _document.BuyingStationCode,
                    _document.FarmerCode,
                    _document.BuyingDocumentNumber);

                MessageBoxHelper.Info("Unfinish การหักชำระหนี้เรียบร้อย");
                PaymentHistoryBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion



        #region Function
        public void PaymentHistoryBinding()
        {
            try
            {
                _paymentHistory = BuyingFacade.PaymentHistoryBL()
                    .GetSingle(_document.Crop,
                    _document.BuyingStationCode,
                    _document.FarmerCode,
                    _document.BuyingDocumentNumber);

                _buyingAmount = (decimal)BuyingHelper
                    .GetByBuyingDocument(_document.Crop,
                    _document.FarmerCode,
                    _document.BuyingStationCode,
                    _document.BuyingDocumentNumber)
                    .Sum(x => x.Price);

                _creditNoteAmount = BuyingFacade.AccountCreditNoteDetailBL()
                    .GetByFarmerCode(_document.Crop, _document.FarmerCode)
                    .Sum(x => x.Quantity * x.UnitPrice);
                _totalDebt = DebtorsHelper.GetTotalDebt(_document.Crop, _document.FarmerCode);
                _totalPayment = DebtorsHelper.GetTotalPayment(_document.Crop, _document.FarmerCode);
                _remainingDebt = _totalDebt - _creditNoteAmount - _totalPayment;

                if (_paymentHistory != null)
                {
                    ///ยอดคงเหลือล่าสุด + ยอดเดิมที่เคยหักไปแล้ว
                    _remainingDebt = _remainingDebt + _paymentHistory.PaymentAmount;
                    _percentage = _paymentHistory.PaymentPercentage;
                    _paymentAmount = _paymentHistory.PaymentAmount;
                    _remainingAfter = _remainingDebt - _paymentAmount;
                    _paymentDate = _paymentHistory.PaymentDate;
                    _isFinish = _paymentHistory.IsFinish;
                }
                else
                {
                    _paymentDate = DateTime.Now;
                    _isFinish = false;
                    _percentage = 100;
                    _paymentAmount = _remainingDebt * Convert.ToDecimal(_percentage / 100.0);
                    _remainingAfter = _remainingDebt - _paymentAmount;
                }

                _balanceBuyingAmount = _buyingAmount - _paymentAmount;

                if (user_setting.UserRoles.Where(x => x.RoleName == "Accounting").Count() > 0)
                    _isAccounting = Visibility.Visible;
                else
                    _isAccounting = Visibility.Collapsed;

                RaisePropertyChangedEvent(nameof(IsAccounting));
                RaisePropertyChangedEvent(nameof(BuyingAmount));
                RaisePropertyChangedEvent(nameof(BalanceBuyingAmount));
                RaisePropertyChangedEvent(nameof(RemainingDebt));
                RaisePropertyChangedEvent(nameof(IsFinish));
                RaisePropertyChangedEvent(nameof(Percentage));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
