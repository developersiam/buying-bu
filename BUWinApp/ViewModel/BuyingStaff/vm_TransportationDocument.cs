﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using System.Windows.Input;
using System.Windows;
using BusinessLayer.Model;
using BusinessLayer.Helper;
using BUWinApp.Helper;
using BusinessLayer;

namespace BUWinApp.ViewModel.BuyingStaff
{
    public class vm_TransportationDocument : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_TransportationDocument()
        {
            _createDate = DateTime.Now;
            ClearForm();
            RaisePropertyChangedEvent(nameof(CreateDate));
        }



        #region Properties
        private DateTime _createDate;
        public DateTime CreateDate
        {
            get { return _createDate; }
            set
            {
                _createDate = value;
                TransportationListBinding();
            }
        }

        private string _transportationCode;
        public string TransportationCode
        {
            get { return _transportationCode; }
            set { _transportationCode = value; }
        }

        private string _truckNumber;
        public string TruckNumber
        {
            get { return _truckNumber; }
            set { _truckNumber = value; }
        }

        private short _maxWeight;
        public short MaxWeight
        {
            get { return _maxWeight; }
            set { _maxWeight = value; }
        }

        private int _totalRecord;
        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private Visibility _isEdit;
        public Visibility IsEdit
        {
            get { return _isEdit; }
            set { _isEdit = value; }
        }

        private Visibility _isCreate;
        public Visibility IsCreate
        {
            get { return _isCreate; }
            set { _isCreate = value; }
        }
        #endregion



        #region List
        private List<m_TransportationDocument> _transportationList;
        public List<m_TransportationDocument> TransportationList
        {
            get { return _transportationList; }
            set { _transportationList = value; }
        }
        #endregion



        #region Command
        private ICommand _onCreateCommand;

        public ICommand OnCreateCommand
        {
            get { return _onCreateCommand ?? (_onCreateCommand = new RelayCommand(OnCreate)); }
            set { _onCreateCommand = value; }
        }

        private void OnCreate(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_truckNumber))
                {
                    MessageBoxHelper.Warning("โปรดระบุ Truck Number");
                    OnFocusRequested(nameof(TruckNumber));
                    return;
                }

                if (_maxWeight <= 0)
                {
                    MessageBoxHelper.Warning("โปรดระบุน้ำหนักที่มากกว่า 0");
                    OnFocusRequested(nameof(MaxWeight));
                    return;
                }

                BuyingFacade.TransportationBL()
                    .Add(user_setting.Crop.Crop1,
                    user_setting.StactionCode,
                    _createDate,
                    user_setting.User.Username,
                    _truckNumber,
                    _maxWeight);

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditCommand;

        public ICommand OnEditCommand
        {
            get { return _onEditCommand ?? (_onEditCommand = new RelayCommand(OnEdit)); }
            set { _onEditCommand = value; }
        }

        private void OnEdit(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_truckNumber))
                {
                    MessageBoxHelper.Warning("โปรดระบุ Truck Number");
                    OnFocusRequested(nameof(TruckNumber));
                    return;
                }

                if (_maxWeight <= 0)
                {
                    MessageBoxHelper.Warning("โปรดระบุน้ำหนักที่มากกว่า 0");
                    OnFocusRequested(nameof(MaxWeight));
                    return;
                }

                if (MessageBoxHelper.Question("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                BuyingFacade.TransportationBL()
                    .Update(_transportationCode,
                    _truckNumber,
                    _maxWeight,
                    user_setting.User.Username,
                    _createDate);

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                var item = (m_TransportationDocument)obj;
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูลที่ต้องการลบ หรือเกิดข้อผิดพลาดอื่น โปรดแจ้งผู้ดูแลระบบ");

                if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                BuyingFacade.TransportationBL().Delete(item.TransportationDocumentCode);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(OnClear)); }
            set { _onClearCommand = value; }
        }

        private void OnClear(object obj)
        {
            ClearForm();
        }

        private ICommand _onDeltailsCommand;

        public ICommand OnDetailsCommand
        {
            get { return _onDeltailsCommand ?? (_onDeltailsCommand = new RelayCommand(OnDetails)); }
            set { _onDeltailsCommand = value; }
        }

        private void OnDetails(object obj)
        {
            try
            {
                /// Show details popup.
                /// 

                View.BuyingStaff.TransportationDocumentDetails w =
                    new View.BuyingStaff.TransportationDocumentDetails();

                var vm = new vm_TransportationDocumentDetails(w);
                w.DataContext = vm;
                vm.TransportationCode = obj.ToString();
                w.ShowDialog();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onSelectedForEdit;

        public ICommand OnSelectedForEdit
        {
            get { return _onSelectedForEdit ?? (_onSelectedForEdit = new RelayCommand(OnSelectedEdit)); }
            set { _onSelectedForEdit = value; }
        }

        private void OnSelectedEdit(object obj)
        {
            try
            {
                var item = (m_TransportationDocument)obj;
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูลที่ต้องการลบ หรือเกิดข้อผิดพลาดอื่น โปรดแจ้งผู้ดูแลระบบ");

                _transportationCode = item.TransportationDocumentCode;
                _truckNumber = item.TruckNumber;
                _maxWeight = item.MaximumWeight;

                _isEdit = Visibility.Visible;
                _isCreate = Visibility.Collapsed;

                RaisePropertyChangedEvent(nameof(TransportationCode));
                RaisePropertyChangedEvent(nameof(TruckNumber));
                RaisePropertyChangedEvent(nameof(MaxWeight));
                RaisePropertyChangedEvent(nameof(IsEdit));
                RaisePropertyChangedEvent(nameof(IsCreate));

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion



        #region Function
        private void ClearForm()
        {
            _transportationCode = "";
            _truckNumber = "";
            _maxWeight = 0;
            _createDate = DateTime.Now;
            _isEdit = Visibility.Collapsed;
            _isCreate = Visibility.Visible;

            RaisePropertyChangedEvent(nameof(TransportationCode));
            RaisePropertyChangedEvent(nameof(TruckNumber));
            RaisePropertyChangedEvent(nameof(MaxWeight));
            RaisePropertyChangedEvent(nameof(CreateDate));
            RaisePropertyChangedEvent(nameof(IsEdit));
            RaisePropertyChangedEvent(nameof(IsCreate));

            TransportationListBinding();
        }

        private void TransportationListBinding()
        {
            try
            {
                _transportationList = TransportationHelper
                    .GetByCreateDate(user_setting.Crop.Crop1, user_setting.StactionCode, _createDate);
                _totalRecord = _transportationList.Count();

                RaisePropertyChangedEvent(nameof(TotalRecord));
                RaisePropertyChangedEvent(nameof(TransportationList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
