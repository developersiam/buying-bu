﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using System.Windows.Input;
using BUWinApp.Helper;
using BusinessLayer.Model;
using BusinessLayer;

namespace BUWinApp.ViewModel.BuyingStaff
{
    public class vm_PrintReceiptMoney : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_PrintReceiptMoney()
        {
            _numberOfCopy = 2;
            RaisePropertyChangedEvent(nameof(NumberOfCopy));
        }


        #region Properties
        private int _totalItem;

        public int TotalItems
        {
            get { return _totalItem; }
            set { _totalItem = value; }
        }

        private short _numberOfCopy;

        public short NumberOfCopy
        {
            get { return _numberOfCopy; }
            set { _numberOfCopy = value; }
        }

        private List<m_BuyingDocument> _selectedList;

        public List<m_BuyingDocument> SelectedList
        {
            get { return _selectedList; }
            set
            {
                _selectedList = value;
                _totalItem = _selectedList.Count();
                RaisePropertyChangedEvent(nameof(TotalItems));
            }
        }

        #endregion



        #region Command
        private ICommand _onPrintCommand;

        public ICommand OnPrintCommand
        {
            get { return _onPrintCommand ?? (_onPrintCommand = new RelayCommand(OnPrint)); }
            set { _onPrintCommand = value; }
        }

        private void OnPrint(object obj)
        {
            try
            {
                if (_selectedList.Count() <= 0)
                    throw new ArgumentException("ไม่พบรายการที่จะพิมพ์ โปรดตรวจสอบกับทางผู้ดูแลระบบ");

                if (_numberOfCopy <= 0)
                    throw new ArgumentException("โปรดระบุจำนวนชุดที่จะพิมพ์");

                foreach (var item in _selectedList)
                    PrintReceiptMoney(item);

                MessageBoxHelper.Info("พิมพ์ใบรับเงินจำนวน " + _totalItem + " รายการสำเร็จ!");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion



        #region Function
        private void PrintReceiptMoney(m_BuyingDocument doc)
        {
            try
            {
                if (doc.IsFinish == false)
                    throw new ArgumentException("ใบเวาเชอร์นี้ยังไม่ถูกปิดการขายจากลานรับซื้อ ไม่สามารถพิมพ์ใบรับเงินได้");

                if (doc.IsAccountFinish == false)
                    throw new ArgumentException("ใบเวาเชอร์นี้ยังไม่ถูกยืนยันข้อมูลโดย receiving ไม่สามารถพิมพ์ใบรับเงินได้");

                //var payment = BuyingFacade.PaymentHistoryBL()
                //    .GetSingle(doc.Crop,
                //    doc.BuyingStationCode,
                //    doc.FarmerCode,
                //    doc.BuyingDocumentNumber);

                //if (payment != null)
                //    if (!payment.IsFinish)
                //        throw new ArgumentException("สถานะการหักชำระครั้งนี้ของใบเวาเชอร์รหัส " + doc.DocumentCode +
                //            " ยังไม่ได้รับการยืนยันจากแผนกบัญชี โปรดตรวจสอบกับทางเจ้าหน้าที่ หรือพิมพ์อีกทีในภายหลัง");

                var person = BuyingFacade.PersonBL().GetSingle(doc.CitizenID);
                ReportHelper.PrintReceiptOfMoney(doc, person, _numberOfCopy);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
