﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using DomainModel;
using BusinessLayer;
using BusinessLayer.Model;
using System.Windows.Input;
using System.Windows;
using BUWinApp.Helper;
using BusinessLayer.Helper;
using BUWinApp.View.BuyingStaff;

namespace BUWinApp.ViewModel.BuyingStaff
{
    public class vm_BuyingDocument : ObservableObject, IRequestFocus
    {
        public vm_BuyingDocument()
        {
            _extentionAgentList = BuyingFacade.SupplierBL()
                .GetByArea(user_setting.User.StaffUser.AreaCode);

            _stationList = BuyingFacade.BuyingStationBL()
                .GetByArea(user_setting.User.StaffUser.AreaCode);

            _createDate = DateTime.Now;
            _crop = user_setting.Crop;
            //_citizenID = "3670300526252";
            //_extensionAgentCode = "STEC/PB";
            _isHeighRiskNTRM = Visibility.Collapsed;

            OnFocusRequested(nameof(CitizenID));

            RaisePropertyChangedEvent(nameof(CitizenID));
            RaisePropertyChangedEvent(nameof(ExtensionAgentCode));

            RaisePropertyChangedEvent(nameof(Crop));
            RaisePropertyChangedEvent(nameof(IsHeighRiskNTRM));
            RaisePropertyChangedEvent(nameof(ExtensionAgentList));
            RaisePropertyChangedEvent(nameof(StationList));
        }


        #region Properties
        private Crop _crop;

        public Crop Crop
        {
            get { return _crop; }
            set { _crop = value; }
        }

        private string _extensionAgentCode;

        public string ExtensionAgentCode
        {
            get { return _extensionAgentCode; }
            set { _extensionAgentCode = value; }
        }

        private DateTime _createDate;

        public DateTime CreateDate
        {
            get { return _createDate; }
            set { _createDate = value; }
        }

        private string _citizenID;

        public string CitizenID
        {
            get { return _citizenID; }
            set { _citizenID = value; }
        }

        private string _farmerCode;

        public string FarmerCode
        {
            get { return _farmerCode; }
            set
            {
                _farmerCode = value;
                RaisePropertyChangedEvent(nameof(FarmerCode));
            }
        }

        private Visibility _isHeighRiskNTRM;

        public Visibility IsHeighRiskNTRM
        {
            get { return _isHeighRiskNTRM; }
            set { _isHeighRiskNTRM = value; }
        }
        #endregion


        #region List
        private List<Supplier> _extentionAgentList;

        public List<Supplier> ExtensionAgentList
        {
            get { return _extentionAgentList; }
            set { _extentionAgentList = value; }
        }

        private List<BuyingStation> _stationList;

        public List<BuyingStation> StationList
        {
            get { return _stationList; }
            set { _stationList = value; }
        }

        private List<m_BuyingDocument> _buyingDocumentList;

        public List<m_BuyingDocument> BuyingDocumentList
        {
            get { return _buyingDocumentList; }
            set { _buyingDocumentList = value; }
        }

        private List<DomainModel.NTRMInspection> _ntrmInspectionList;

        public List<DomainModel.NTRMInspection> NTRMInspectionList
        {
            get { return _ntrmInspectionList; }
            set { _ntrmInspectionList = value; }
        }

        private List<CPASampleCollection2020> _cpaSampleCollectionList;

        public List<CPASampleCollection2020> CPASampleCollectionList
        {
            get { return _cpaSampleCollectionList; }
            set { _cpaSampleCollectionList = value; }
        }
        #endregion


        #region Command
        private ICommand _onCheckProfileCommand;

        public ICommand OnCheckProfileCommand
        {
            get { return _onCheckProfileCommand ?? (_onCheckProfileCommand = new RelayCommand(OnCheckProfile)); }
            set { _onCheckProfileCommand = value; }
        }

        private void OnCheckProfile(object e)
        {
            if (string.IsNullOrEmpty(_extensionAgentCode))
            {
                MessageBoxHelper.Warning("โปรดระบุ Extension Agent");
                OnFocusRequested(nameof(ExtensionAgentCode));
                return;
            }

            if (_createDate.Date < DateTime.Now.Date)
            {
                Helper.MessageBoxHelper.Warning("ไม่อนุญาตให้ทำรายการย้อนหลังได้");
                OnFocusRequested(nameof(CreateDate));
                return;
            }

            if (string.IsNullOrEmpty(_citizenID))
            {
                Helper.MessageBoxHelper.Warning("โปรดระบุ CitizenID");
                OnFocusRequested(nameof(CitizenID));
                return;
            }

            BindFarmerProfile();
            BindDocuments();
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(OnClear)); }
            set { _onClearCommand = value; }
        }

        private void OnClear(object e)
        {
            ClearForm();
        }

        private ICommand _onRegisterBarcodeCommand;

        public ICommand OnRegisterBarcodeCommand
        {
            get { return _onRegisterBarcodeCommand ?? (_onRegisterBarcodeCommand = new RelayCommand(OnRegisterBarcode)); }
            set { _onRegisterBarcodeCommand = value; }
        }

        private void OnRegisterBarcode(object e)
        {
            try
            {
                var gmsIncompleteList = BuyingFacade.GMSDataCollectionIncompleteBL()
                    .GetByFarmer(user_setting.Crop.Crop1, _farmerCode);

                if (gmsIncompleteList.Count() >= 1)
                {
                    string fields = "";
                    foreach (var item in gmsIncompleteList)
                        fields = fields + " " + item.GMSDataCollectionField.GMSFieldName;

                    if (MessageBoxHelper.Question("ข้อมูล GMS ไม่ครบถ้วน(" + fields +
                        ") ไม่สามารถจ่ายป้ายให้กับชาวไร่ได้ " +
                        "ท่านต้องการปลดล็อคโดย Supervisor หรือไม่?") == MessageBoxResult.No)
                        return;

                    SupervisorComfirmation win = new SupervisorComfirmation();
                    var confirmVM = new vm_SupervisorConfirmation(win);
                    win.DataContext = confirmVM;
                    win.ShowDialog();

                    if (!confirmVM.IsConfirm)
                        throw new ArgumentException("ไม่ได้รับการ confirm จาก supervisor " +
                            "ไม่สามารถจ่ายป้ายให้กับชาวไร่ได้");
                }

                //สร้างใบรายการซื้อขายใหม่ 
                var newDocument = BuyingFacade.BuyingDocumentBL()
                    .Add(_crop.Crop1,
                    user_setting.StactionCode,
                    _farmerCode,
                    user_setting.User.Username,
                    _createDate);

                var _document = BuyingDocumentHelper
                    .GetSingle(newDocument.Crop,
                    newDocument.BuyingStationCode,
                    newDocument.FarmerCode,
                    newDocument.BuyingDocumentNumber);

                var window = new BuyingDocumentDetails();
                var vm = new vm_BuyingDocumentDetails();
                vm.Document = _document;
                window.DataContext = vm;
                window.ShowDialog();

                BindDocuments();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onPrintCPAStickerCommand;

        public ICommand OnPrintCPAStickerCommand
        {
            get { return _onPrintCPAStickerCommand ?? (_onPrintCPAStickerCommand = new RelayCommand(OnPrintCPASticker)); }
            set { _onPrintCPAStickerCommand = value; }
        }

        private void OnPrintCPASticker(object e)
        {
            SampleCollection window = new SampleCollection();
            var vm = new vm_SampleCollection();
            window.DataContext = vm;
            vm.CitizenID = _citizenID;
            window.ShowDialog();
        }

        private ICommand _onDeleteDocumentCommand;

        public ICommand OnDeleteDocumentCommand
        {
            get { return _onDeleteDocumentCommand ?? (_onDeleteDocumentCommand = new RelayCommand(OnDeleteDocument)); }
            set { _onDeleteDocumentCommand = value; }
        }

        private void OnDeleteDocument(object e)
        {
            try
            {
                var model = (m_BuyingDocument)e;
                if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่") == MessageBoxResult.No)
                    return;

                if (model.IsFinish == true)
                    throw new ArgumentException("ใบรายการซื้อขายนี้ถูกปิดการซื้อขายแล้ว ไม่สามารถลบได้");

                if (model.TotalBale > 0)
                    throw new ArgumentException("ใบรายการซื้อขายนี้มีข้อมูลห่อยาอยู่ภายในจำนวน " + model.TotalBale + " ไม่สามารถลบได่้");

                BuyingFacade.BuyingDocumentBL()
                    .Delete(model.Crop,
                    model.BuyingStationCode,
                    model.FarmerCode,
                    model.BuyingDocumentNumber);

                BindDocuments();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDocumentDetailsCommand;

        public ICommand OnDocumentDetailsCommand
        {
            get { return _onDocumentDetailsCommand ?? (_onDocumentDetailsCommand = new RelayCommand(OnDocumentDetails)); }
            set { _onDocumentDetailsCommand = value; }
        }

        private void OnDocumentDetails(object e)
        {
            var window = new View.BuyingStaff.BuyingDocumentDetails();
            var vm = new vm_BuyingDocumentDetails();
            var document = BuyingDocumentHelper
                .GetSingle(_crop.Crop1,
                user_setting.StactionCode,
                _farmerCode,
                Convert.ToInt16(e.ToString()));

            vm.Document = document;
            window.DataContext = vm;
            window.ShowDialog();
            BindDocuments();
        }
        #endregion


        #region Function
        private void BindFarmerProfile()
        {
            try
            {
                //Get a farmer from selected supplier *
                var farmer = BuyingFacade.FarmerBL()
                    .GetBySupplierCodeAndCitizenID(_extensionAgentCode, _citizenID);

                if (farmer == null)
                {
                    MessageBoxHelper.Warning("ชาวไร่รายนี้ไม่ได้เป็นสมาชิกของ " + _extensionAgentCode);
                    OnFocusRequested(nameof(CitizenID));
                    return;
                }

                _farmerCode = farmer.FarmerCode;

                //Get a farmer contract **
                var regisModel = BuyingFacade.RegistrationBL()
                    .GetSingle(_crop.Crop1, _farmerCode);

                if (regisModel == null)
                {
                    MessageBoxHelper.Warning("ชาวไร่รหัส " + farmer.FarmerCode +
                        " ไม่ได้ลงทะเบียนทำสัญญาไว้กับ " + _extensionAgentCode +
                        " ในปี " + _crop.Crop1);
                    OnFocusRequested(nameof(CitizenID));
                    return;
                }

                /// NTRM Inspection checking in HighlyDangerous.
                if (BuyingFacade.NtrmInspectionBL()
                    .IsHighlyDangerousInspection(regisModel.FarmerCode) == true)
                    _isHeighRiskNTRM = Visibility.Visible;
                else
                    _isHeighRiskNTRM = Visibility.Collapsed;

                var nestingBale = BuyingFacade.BuyingBL()
                    .GetNestingBale(_crop.Crop1, _farmerCode)
                    .Count();

                if (nestingBale > 0)
                    MessageBoxHelper.Warning("ชาวไร่รายนี้เคยมีการบันทึกข้อมูลห่อยาที่มีการยัดไส้ (Nesting) ในปีนี้ จำนวน " + nestingBale + " ห่อ");

                /// Check incident farmer info and show alert if detect meet incident cases.
                /// 
                var incidentList = BuyingFacade.IncidentFarmerBL()
                    .GetByFarmer(_crop.Crop1, _citizenID)
                    .Where(x => x.IncidentDescription
                    .IncidentTopic
                    .IncidentCategory
                    .CategoryName == "Extreme Breaches");

                if (incidentList.Count() > 0)
                    foreach (var item in incidentList)
                        MessageBoxHelper.Warning("ชาวไร่รายนี้มีกรณีเข้าข่าย Farmer Incident " + Environment.NewLine +
                        "เรื่อง " + item.IncidentDescription.Description + Environment.NewLine +
                        "เมื่อวันที่ " + item.IncidentDate + Environment.NewLine +
                        "สถานะปัจจุบันคือ " + item.IncidentStatus + Environment.NewLine);

                var gapGroupCode = BuyingFacade.GAPGroupMember2020BL()
                   .GetSingleByCitizenID(_crop.Crop1, _citizenID);
                if (gapGroupCode != null)
                {
                    _cpaSampleCollectionList = BuyingFacade.CPASampleCollection2020BL()
                      .GetByFarmer(gapGroupCode.CitizenID, gapGroupCode.GAPGroupCode);
                    if (_cpaSampleCollectionList.Count <= 0)
                        MessageBoxHelper.Warning("ชาวไร่รายนี้ยังไม่เคยได้รับการบันทึก CPA Sample ในระบบ!");
                }

                var sampleResult = BuyingFacade.SampleResultBL().GetByPerson(user_setting.Crop.Crop1, _citizenID);
                if (sampleResult.Count() > 0)
                    MessageBoxHelper.Warning("ชาวไร่รายนี้มีผลตรวจนิโคตินเฉลี่ยอยู่ที่ " +
                        sampleResult.Where(x => x.SampleType == "Nicotine").Average(x => x.Result));

                /// NTRM inspection percentages.
                /// 
                //double ntrmInspectionPercentages = BuyingFacade.NtrmInspectionBL().GetNTRMInspectionPercentagesByFarmer(registrationFarmer.FarmerCode);

                /// ตรวจสอบชาวไร่ที่ถูกน้ำท่วม
                /// 
                if (regisModel.UnRegisterReason != null)
                    MessageBoxHelper.Warning("ชาวไร่รายนี้ได้รับผลกระทบจากเหตุพายุฤดูร้อนเมื่อช่วงก่อนหน้านี้");

                RaisePropertyChangedEvent(nameof(FarmerCode));
                RaisePropertyChangedEvent(nameof(IsHeighRiskNTRM));
                RaisePropertyChangedEvent(nameof(NTRMInspectionList));
                RaisePropertyChangedEvent(nameof(CPASampleCollectionList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void BindDocuments()
        {
            _buyingDocumentList = BuyingDocumentHelper.GetByFarmer(_crop.Crop1, _farmerCode);
            RaisePropertyChangedEvent(nameof(BuyingDocumentList));
        }

        private void ClearForm()
        {
            _citizenID = "";
            _farmerCode = "";

            _isHeighRiskNTRM = Visibility.Collapsed;
            _buyingDocumentList = null;
            _ntrmInspectionList = null;
            _cpaSampleCollectionList = null;

            RaisePropertyChangedEvent(nameof(CitizenID));
            RaisePropertyChangedEvent(nameof(FarmerCode));
            RaisePropertyChangedEvent(nameof(IsHeighRiskNTRM));
            RaisePropertyChangedEvent(nameof(NTRMInspectionList));
            RaisePropertyChangedEvent(nameof(CPASampleCollectionList));
            RaisePropertyChangedEvent(nameof(BuyingDocumentList));

            OnFocusRequested(nameof(CitizenID));
        }
        #endregion


        #region Event
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }
        #endregion
    }
}
