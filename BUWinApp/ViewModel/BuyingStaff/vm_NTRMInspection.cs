﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using DomainModel;
using System.Windows.Input;
using BUWinApp.Helper;
using BusinessLayer;
using BusinessLayer.Model;
using BusinessLayer.Helper;

namespace BUWinApp.ViewModel.BuyingStaff
{
    public class vm_NTRMInspection : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_NTRMInspection()
        {
            OnFocusRequested(nameof(BaleBarcode));
        }


        #region Properties
        private string _baleBarcode;
        public string BaleBarcode
        {
            get { return _baleBarcode; }
            set
            {
                _baleBarcode = value;
                if (!string.IsNullOrEmpty(_baleBarcode) && _baleBarcode.Length == 17)
                    NTRMInfoBinding();

                RaisePropertyChangedEvent(nameof(NTRMInspectionList));
            }
        }

        private byte _quantity;
        public byte Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }
        #endregion


        #region List
        private List<NTRMInspection> _ntrmInspectionList;

        public List<NTRMInspection> NTRMInspectionList
        {
            get { return _ntrmInspectionList; }
            set { _ntrmInspectionList = value; }
        }

        public List<NTRMType> NTRMTypeList
        {
            get
            {
                return BuyingFacade.NtrmInspectionBL()
                    .GetAllNTRMType()
                    .OrderBy(x => x.NTRMTypeName)
                    .ToList();
            }
        }

        #endregion


        #region Command
        private ICommand _onAddCommand;

        public ICommand OnAddCommand
        {
            get { return _onAddCommand ?? (_onAddCommand = new RelayCommand(OnAdd)); }
            set { _onAddCommand = value; }
        }

        private void OnAdd(object e)
        {
            try
            {
                if (string.IsNullOrEmpty(_baleBarcode))
                {
                    MessageBoxHelper.Warning("โปรดสแกนบาร์โค้ต");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                var model = BuyingFacade.BuyingBL().GetByBaleBarcode(_baleBarcode);
                if (model == null)
                {
                    MessageBoxHelper.Warning("ไม่พบข้อมูลห่อยารหัสบาร์โค้ต " + _baleBarcode + " ในระบบ");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                BuyingFacade.NtrmInspectionBL()
                    .Add(_baleBarcode,
                    e.ToString(),
                    _quantity,
                    user_setting.User.Username);

                NTRMInspectionListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeletedCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeletedCommand ?? (_onDeletedCommand = new RelayCommand(OnDelete)); }
            set { _onDeletedCommand = value; }
        }

        private void OnDelete(object e)
        {
            try
            {
                if (e == null)
                    return;
                var model = (NTRMInspection)e;

                BuyingFacade.NtrmInspectionBL().Delete(model.BaleBarcode,
                    model.NTRMTypeCode,
                    model.InspectionDate);

                NTRMInspectionListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(OnClear)); }
            set { _onClearCommand = value; }
        }

        private void OnClear(object e)
        {
            Clear();
        }

        private ICommand _onIncreaseCommmand;

        public ICommand OnIncreaseCommand
        {
            get { return _onIncreaseCommmand ?? (_onIncreaseCommmand = new RelayCommand(OnIncrease)); }
            set { _onIncreaseCommmand = value; }
        }

        private void OnIncrease(object e)
        {
            _quantity++;
            RaisePropertyChangedEvent(nameof(Quantity));
        }

        private ICommand _onDecreaseCommand;

        public ICommand OnDecreaseCommand
        {
            get { return _onDecreaseCommand ?? (_onDecreaseCommand = new RelayCommand(OnDecrease)); }
            set { _onDecreaseCommand = value; }
        }

        private void OnDecrease(object e)
        {
            /// A meaning of type 16.
            /// It mean "not found". 
            /// The system can be accept a zero value.
            /// 
            if (_quantity <= 0)
                return;

            _quantity--;
            RaisePropertyChangedEvent(nameof(Quantity));
        }

        private ICommand _onBarcodeEnterCommand;

        public ICommand OnBarcodeEnterCommand
        {
            get { return _onBarcodeEnterCommand ?? (_onBarcodeEnterCommand = new RelayCommand(OnBarcodeEnter)); }
            set { _onBarcodeEnterCommand = value; }
        }

        private void OnBarcodeEnter(object e)
        {
            NTRMInfoBinding();
        }
        #endregion


        #region Function
        private void NTRMInfoBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_baleBarcode))
                {
                    MessageBoxHelper.Warning("โปรดสแกนบาร์โค้ต");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                var model = BuyingFacade.BuyingBL().GetByBaleBarcode(_baleBarcode);
                if (model == null)
                {
                    MessageBoxHelper.Warning("ไม่พบข้อมูลห่อยารหัสบาร์โค้ต " + _baleBarcode + " ในระบบ");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                NTRMInspectionListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        private void NTRMInspectionListBinding()
        {
            _ntrmInspectionList = BuyingFacade.NtrmInspectionBL()
                .GetByBaleBarcode(_baleBarcode)
                .OrderByDescending(x=>x.InspectionDate)
                .ToList();

            RaisePropertyChangedEvent(nameof(NTRMInspectionList));
        }
        private void Clear()
        {
            _quantity = 1;
            _baleBarcode = "";
            _ntrmInspectionList = null;

            RaisePropertyChangedEvent(nameof(Quantity));
            RaisePropertyChangedEvent(nameof(BaleBarcode));
            RaisePropertyChangedEvent(nameof(NTRMInspectionList));

            OnFocusRequested(nameof(BaleBarcode));
        }
        #endregion
    }
}
