﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using System.Windows;
using BUWinApp.View.BuyingStaff;
using System.Windows.Input;
using BusinessLayer;
using BUWinApp.Helper;

namespace BUWinApp.ViewModel.BuyingStaff
{
    public class vm_TransportationDocumentDetails : ObservableObject, IRequestFocus
    {
        Window _window;
        DomainModel.TransportationDocument _document;

        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_TransportationDocumentDetails(Window w)
        {
            _window = w;

            RaisePropertyChangedEvent(nameof(TransportationCode));
            OnFocusRequested(nameof(BaleBarcode));
        }


        #region Properties
        private string _transportationCode;
        public string TransportationCode
        {
            get { return _transportationCode; }
            set
            {
                _transportationCode = value;
                DocumentBinding();
            }
        }

        private Visibility isFinish;
        public Visibility IsFinish
        {
            get { return isFinish; }
            set { isFinish = value; }
        }

        private Visibility _isUnfinish;
        public Visibility IsUnfinish
        {
            get { return _isUnfinish; }
            set { _isUnfinish = value; }
        }

        private string _baleBarcode;
        public string BaleBarcode
        {
            get { return _baleBarcode; }
            set { _baleBarcode = value; }
        }

        #endregion


        #region List
        private List<DomainModel.Buying> _detailsList;
        public List<DomainModel.Buying> DetailsList
        {
            get { return _detailsList; }
            set { _detailsList = value; }
        }
        #endregion


        #region Command
        private ICommand _onRefreshCommand;

        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(OnRefresh)); }
            set { _onRefreshCommand = value; }
        }

        private void OnRefresh(object obj)
        {
            DocumentBinding();
        }

        private ICommand _onFinishCommand;

        public ICommand OnFinishCommand
        {
            get { return _onFinishCommand ?? (_onFinishCommand = new RelayCommand(OnFinish)); }
            set { _onFinishCommand = value; }
        }

        private void OnFinish(object obj)
        {
            if (MessageBoxHelper.Question("ท่านต้องการล็อคใช่หรือไม่?") == MessageBoxResult.No)
                return;

            BuyingFacade.TransportationBL()
                .Finish(_transportationCode, user_setting.User.Username);

            DocumentBinding();
        }

        private ICommand _onUnfinishCommand;

        public ICommand OnUnfinishCommand
        {
            get { return _onUnfinishCommand ?? (_onUnfinishCommand = new RelayCommand(OnUnfinish)); }
            set { _onUnfinishCommand = value; }
        }

        private void OnUnfinish(object obj)
        {
            if (MessageBoxHelper.Question("ท่านต้องการปลดล็อคใช่หรือไม่?") == MessageBoxResult.No)
                return;

            BuyingFacade.TransportationBL()
                .Unfinish(_transportationCode, user_setting.User.Username);

            DocumentBinding();
        }

        private ICommand _onPrintCommand;

        public ICommand OnPrintCommand
        {
            get { return _onPrintCommand ?? (_onPrintCommand = new RelayCommand(OnPrint)); }
            set { _onPrintCommand = value; }
        }

        private void OnPrint(object obj)
        {
            Forms.BuyingStaff.TransportationDocumentPrintPreview window =
                new Forms.BuyingStaff.TransportationDocumentPrintPreview(_transportationCode);

            window.ShowDialog();
        }

        private ICommand _onScanBarcodeCommand;

        public ICommand OnScanBarcodeCommand
        {
            get { return _onScanBarcodeCommand ?? (_onScanBarcodeCommand = new RelayCommand(OnScanBarcode)); }
            set { _onScanBarcodeCommand = value; }
        }

        private void OnScanBarcode(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_baleBarcode))
                    throw new ArgumentException("โปรดสแกน Bale barcode อีกครั้ง");

                var model = BuyingFacade.BuyingBL().GetByBaleBarcode(_baleBarcode);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลยาห่อนี้ในระบบ");

                if (model.TransportationDocumentCode == null ||
                    model.TransportationDocumentCode == _transportationCode)
                {
                    BuyingFacade.TransportationBL()
                        .LoadBaleToTruck(_baleBarcode, _transportationCode, user_setting.User.Username);
                }
                else
                {
                    BuyingFacade.TransportationBL()
                        .MoveBale(_baleBarcode,
                        _transportationCode,
                        user_setting.User.Username,
                        DateTime.Now,
                        "Move From " + model.TransportationDocumentCode);
                }

                _baleBarcode = "";

                RaisePropertyChangedEvent(nameof(BaleBarcode));
                OnFocusRequested(nameof(BaleBarcode));

                DocumentBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onResetCommand;

        public ICommand OnResetCommand
        {
            get { return _onResetCommand ?? (_onResetCommand = new RelayCommand(OnReset)); }
            set { _onResetCommand = value; }
        }

        private void OnReset(object obj)
        {
            _baleBarcode = "";
            RaisePropertyChangedEvent(nameof(BaleBarcode));
            OnFocusRequested(nameof(BaleBarcode));
        }
        #endregion


        #region Function
        private void DocumentBinding()
        {
            _document = BuyingFacade.TransportationBL().GetSingle(_transportationCode);

            _transportationCode = "";
            RaisePropertyChangedEvent(nameof(TransportationCode));

            _transportationCode = _document.TransportationDocumentCode;

            if (_document.IsFinish == true)
            {
                isFinish = Visibility.Collapsed;
                IsUnfinish = Visibility.Visible;
            }
            else
            {
                isFinish = Visibility.Visible;
                IsUnfinish = Visibility.Collapsed;
            }
            _detailsList = BuyingFacade.BuyingBL()
                .GetByTransportationCode(_transportationCode);

            RaisePropertyChangedEvent(nameof(IsFinish));
            RaisePropertyChangedEvent(nameof(IsUnfinish));
            RaisePropertyChangedEvent(nameof(DetailsList));
            RaisePropertyChangedEvent(nameof(TransportationCode));
        }
        #endregion
    }
}
