﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using DomainModel;
using System.Windows.Input;
using BusinessLayer.Model;
using BusinessLayer;
using BUWinApp.Helper;
using BusinessLayer.Helper;
using System.Windows;
using BUWinApp.View.BuyingStaff;
using System.Collections.ObjectModel;
using System.IO.Ports;
using System.Threading;

namespace BUWinApp.ViewModel.BuyingStaff
{
    public class vm_Buying : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        vm_SecondaryMonitor window2VM;
        SecondaryMonitor window2;
        public m_Buying _buying { get; set; }

        public vm_Buying()
        {
            _gradeList = BuyingFacade.BuyingGradeBL()
                .GetByDefaultPricingSet();

            _buyerList = BuyingFacade.BuyerBL()
                .GetAll()
                .OrderBy(x => x.BuyerCode)
                .ToList();

            _createDate = DateTime.Now;
            _stationCode = user_setting.StactionCode;
            TransportationInfoBinding();

            _isManualInput = true;

            RaisePropertyChangedEvent(nameof(IsManualInput));
            RaisePropertyChangedEvent(nameof(GradeList));
            RaisePropertyChangedEvent(nameof(BuyerList));

            Clear();
        }


        #region Properties
        private int _crop;
        public int Crop
        {
            get { return _crop; }
            set
            {
                _crop = value;
                RaisePropertyChangedEvent(nameof(Crop));
            }
        }

        private string _farmerCode;
        public string FarmerCode
        {
            get { return _farmerCode; }
            set { _farmerCode = value; }
        }

        private string _documentCode;
        public string DocumentCode
        {
            get { return _documentCode; }
            set { _documentCode = value; }
        }

        private string _baleBarcode;
        public string BaleBarcode
        {
            get { return _baleBarcode; }
            set { _baleBarcode = value; }
        }

        private string _projectType;
        public string ProjectType
        {
            get { return _projectType; }
            set
            {
                _projectType = value;
                RaisePropertyChangedEvent(nameof(ProjectType));
            }
        }

        private string _grade;
        public string Grade
        {
            get { return _grade; }
            set
            {
                _grade = value;
                if (window2VM != null)
                    window2VM.Grade = _grade;
            }
        }

        private decimal _weight;
        public decimal Weight
        {
            get { return _weight; }
            set
            {
                _weight = value;
                if (window2VM != null)
                    window2VM.Weight = _weight;
            }
        }

        private string _transportationCode;
        public string TransportationCode
        {
            get { return _transportationCode; }
            set
            {
                _transportationCode = value;
                RaisePropertyChangedEvent(nameof(TransportationCode));
            }
        }

        private string _stationCode;
        public string StationCode
        {
            get { return _stationCode; }
            set
            {
                _stationCode = value;
                TransportationInfoBinding();
            }
        }

        private string _buyerCode;
        public string BuyerCode
        {
            get { return _buyerCode; }
            set { _buyerCode = value; }
        }

        private bool _isManualInput;
        public bool IsManualInput
        {
            get { return _isManualInput; }
            set
            {
                _isManualInput = value;
                try
                {
                    if (IsManualInput == true)
                    {
                        Thread CloseDown = new Thread(new ThreadStart(CloseSerialOnExit));
                        CloseDown.Start();
                        OnFocusRequested(nameof(Weight));
                    }
                    else
                    {
                        DigitalScaleConnect();
                    }
                }
                catch (Exception ex)
                {
                    MessageBoxHelper.Exception(ex);
                }
            }
        }

        private DateTime _createDate;
        public DateTime CreateDate
        {
            get { return _createDate; }
            set
            {
                _createDate = value;
                TransportationInfoBinding();
            }
        }

        private int _totalBale;
        public int TotalBale
        {
            get { return _totalBale; }
            set { _totalBale = value; }
        }

        #endregion


        #region List
        private List<DomainModel.TransportationDocument> _transportationDocumentList;

        public List<DomainModel.TransportationDocument> TransportationDocumentList
        {
            get { return _transportationDocumentList; }
            set { _transportationDocumentList = value; }
        }

        private List<m_Buying> _buyingDetailsList;

        public List<m_Buying> BuyingDetailsList
        {
            get { return _buyingDetailsList; }
            set { _buyingDetailsList = value; }
        }

        private List<BuyingGrade> _gradeList;

        public List<BuyingGrade> GradeList
        {
            get { return _gradeList; }
            set { _gradeList = value; }
        }

        public ObservableCollection<string> GradeObservableList
        {
            get
            {
                var list = new ObservableCollection<string>();

                foreach (var item in _gradeList)
                    list.Add(item.Grade);

                return list;
            }
        }

        private List<Buyer> _buyerList;

        public List<Buyer> BuyerList
        {
            get { return _buyerList; }
            set { _buyerList = value; }
        }

        public List<BuyingStation> StationList
        {
            get
            {
                return BuyingFacade.BuyingStationBL()
                    .GetByArea(user_setting.User.StaffUser.AreaCode);
            }
        }
        #endregion


        #region Command
        private ICommand _onBaleBarcodeEnterCommand;

        public ICommand OnBaleBarcodeEnterCommand
        {
            get { return _onBaleBarcodeEnterCommand ?? (_onBaleBarcodeEnterCommand = new RelayCommand(OnBaleBarcodeEnter)); }
            set { _onBaleBarcodeEnterCommand = value; }
        }

        private void OnBaleBarcodeEnter(object e)
        {
            try
            {
                if (string.IsNullOrEmpty(_baleBarcode))
                {
                    MessageBoxHelper.Warning("โปรดระบุรหัสป้ายบาร์โค้ต");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                _buying = BuyingHelper.GetByBaleBarcode(_baleBarcode);

                if (_buying == null)
                {
                    MessageBoxHelper.Warning("ไม่พบข้อมูลห่อยารหัสบาร์โค้ต " +
                        _baleBarcode + " โปรดสแกนบาร์โค้ตใหม่อีกครั้ง");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                if (!string.IsNullOrEmpty(_buying.RejectReason))
                    MessageBoxHelper.Info("ยาห่อนี้ถูก Reject ด้วยสาเหตุ " + _buying.RejectReason);

                _farmerCode = "";
                _projectType = "";
                RaisePropertyChangedEvent(nameof(FarmerCode));
                RaisePropertyChangedEvent(nameof(ProjectType));

                _farmerCode = _buying.FarmerCode;
                _projectType = _buying.ProjectType;
                _grade = _buying.Grade;
                _weight = Convert.ToDecimal(_buying.Weight);

                if (window2VM != null)
                {
                    window2VM.Grade = _grade;
                    window2VM.Weight = _weight;
                }

                _documentCode = _buying.Crop + "-" +
                    _buying.BuyingStationCode + "-" +
                    _buying.FarmerCode + "-" +
                    _buying.BuyingDocumentNumber;

                if (!string.IsNullOrEmpty(_buying.TransportationDocumentCode))
                {
                    _createDate = _buying.TransportationDocument.CreateDate;
                    _stationCode = _buying.TransportationDocument.BuyingStationCode;

                    TransportationInfoBinding();
                    _transportationCode = _buying.TransportationDocumentCode;

                    RaisePropertyChangedEvent(nameof(TransportationCode));
                    RaisePropertyChangedEvent(nameof(StationCode));
                    RaisePropertyChangedEvent(nameof(CreateDate));
                }

                if (!string.IsNullOrEmpty(_buying.BuyerCode))
                    _buyerCode = _buying.BuyerCode;

                /// Show bale info on the secondary monitor.
                /// 
                BuyingListBinding();

                RaisePropertyChangedEvent(nameof(FarmerCode));
                RaisePropertyChangedEvent(nameof(DocumentCode));
                RaisePropertyChangedEvent(nameof(ProjectType));
                RaisePropertyChangedEvent(nameof(Grade));
                RaisePropertyChangedEvent(nameof(Weight));
                RaisePropertyChangedEvent(nameof(BuyerCode));
                RaisePropertyChangedEvent(nameof(BuyingDetailsList));

                OnFocusRequested(nameof(Grade));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onGradeEnterCommand;

        public ICommand OnGradeEnterCommand
        {
            get { return _onGradeEnterCommand ?? (_onGradeEnterCommand = new RelayCommand(OnGradeEnter)); }
            set { _onGradeEnterCommand = value; }
        }

        private void OnGradeEnter(object e)
        {
            try
            {
                if (string.IsNullOrEmpty(_grade))
                {
                    MessageBoxHelper.Warning("โปรดระบุเกรดซื้อก่อนไปยังขั้นตอนบันทึกน้ำหนัก");
                    OnFocusRequested(nameof(Grade));
                    return;
                }

                if (_gradeList.Where(x => x.Grade == _grade).Count() < 1)
                {
                    MessageBoxHelper.Warning(_grade + " ไม่มีอยู่ในระบบ โปรดตรวจสอบอีกครั้ง");
                    OnFocusRequested(nameof(Grade));
                    return;
                }

                OnFocusRequested(nameof(Weight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(OnClear)); }
            set { _onClearCommand = value; }
        }

        private void OnClear(object e)
        {
            Clear();
        }

        private ICommand _onRefreshCommand;

        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(OnRefresh)); }
            set { _onRefreshCommand = value; }
        }

        private void OnRefresh(object e)
        {
            try
            {
                _gradeList = BuyingFacade.BuyingGradeBL().GetByDefaultPricingSet();
                _buyerList = BuyingFacade.BuyerBL()
                    .GetAll()
                    .OrderBy(x => x.BuyerCode)
                    .ToList();

                TransportationInfoBinding();

                RaisePropertyChangedEvent(nameof(BuyerList));
                RaisePropertyChangedEvent(nameof(GradeList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onRejectCommand;

        public ICommand OnRejectCommand
        {
            get { return _onRejectCommand ?? (_onRejectCommand = new RelayCommand(OnReject)); }
            set { _onRejectCommand = value; }
        }

        private void OnReject(object e)
        {
            try
            {
                if (_buying == null)
                {
                    MessageBoxHelper.Warning("ไม่พบข้อมูลห่อยา โปรดลองสแกนบาร์โค้ตใหม่อีกครั้ง");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                if (string.IsNullOrEmpty(_buyerCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุ buyer");
                    OnFocusRequested(nameof(BuyerCode));
                    return;
                }

                if (MessageBoxHelper.Question("ท่านต้องการแบ็คยาห่อนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                /// Show rejection choices popup
                /// 
                var rejection = "";
                BuyingFacade.BuyingBL()
                    .RejectBale(_baleBarcode,
                    rejection,
                    user_setting.User.Username,
                    _buyerCode);

                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onWeightEnterCommand;

        public ICommand OnWeightEnterCommand
        {
            get { return _onWeightEnterCommand ?? (_onWeightEnterCommand = new RelayCommand(OnWeightEnter)); }
            set { _onWeightEnterCommand = value; }
        }

        private void OnWeightEnter(object e)
        {
            try
            {
                if (_buying == null)
                {
                    MessageBoxHelper.Warning("ไม่พบข้อมูลห่อยารหัส" +
                        _baleBarcode + " โปรดสแกนบาร์โค้ตใหม่อีกครั้ง");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                if (string.IsNullOrEmpty(_grade))
                {
                    MessageBoxHelper.Warning("โปรดระบุเกรดซื้อ");
                    OnFocusRequested(nameof(Grade));
                    return;
                }

                if (_gradeList.Where(x => x.Grade == _grade).Count() < 1)
                {
                    MessageBoxHelper.Warning(_grade + " ไม่มีอยู่ในระบบ โปรดตรวจสอบอีกครั้ง");
                    OnFocusRequested(nameof(Grade));
                    return;
                }

                if (string.IsNullOrEmpty(_buyerCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุผู้ซื้อ (buyer code)");
                    OnFocusRequested(nameof(BuyerCode));
                    return;
                }

                if (string.IsNullOrEmpty(_transportationCode))
                {
                    MessageBoxHelper.Warning("โปรดระบุรถบรรทุกที่ทำการขนส่ง (transportation code)");
                    OnFocusRequested(nameof(TransportationCode));
                    return;
                }

                if (_weight < 1)
                {
                    MessageBoxHelper.Warning("น้ำหนักใบยาจะต้องมากกว่า 0");
                    OnFocusRequested(nameof(Weight));
                    return;
                }

                if (_buying.RejectReason != null)
                {
                    if (MessageBoxHelper.Question("ยาห่อนี้ถูกแบ็คเนื่องจาก " + _buying.RejectReason +
                        " ท่านต้องการยกเลิกการแบ็ค และกลับมาซื้อใช่หรือไม่?") == MessageBoxResult.No)
                    {
                        OnFocusRequested(nameof(Weight));
                        return;
                    }
                }

                //เงื่อนไขใหม่ในปี 2021 หาก project type เป็น NL จะต้องใช้เกรดในกลุ่ม /NL ทุกครั้ง
                //
                //if (_buying.ProjectType == "NL" && !_grade.Contains("NL"))
                //{
                //    MessageBoxHelper.Warning("ถ้าเป็นป้ายสีฟ้า NL เกรดซื้อจะต้องมี /NL ต่อท้าย");
                //    OnFocusRequested(nameof(Grade));
                //    return;
                //}

                var grade = _gradeList.SingleOrDefault(x => x.Grade == _grade);
                BuyingFacade.BuyingBL()
                    .CaptureAll(
                    _baleBarcode,
                    grade.PricingCrop,
                    grade.PricingNumber,
                    grade.Grade,
                    _buyerCode,
                    _weight,
                    _transportationCode,
                    user_setting.User.Username
                    );

                _baleBarcode = "";
                _grade = "";
                _weight = 0;

                RaisePropertyChangedEvent(nameof(BaleBarcode));
                RaisePropertyChangedEvent(nameof(Grade));
                RaisePropertyChangedEvent(nameof(Weight));

                var tmp_documentCode = _documentCode;
                var tmp_farmerCode = _farmerCode;
                var tmp_projectType = _projectType;
                var tmp_trasportCode = _transportationCode;

                _documentCode = "";
                _farmerCode = "";
                _projectType = "";
                _transportationCode = "";

                RaisePropertyChangedEvent(nameof(DocumentCode));
                RaisePropertyChangedEvent(nameof(FarmerCode));
                RaisePropertyChangedEvent(nameof(ProjectType));
                RaisePropertyChangedEvent(nameof(TransportationCode));

                _documentCode = tmp_documentCode;
                _farmerCode = tmp_farmerCode;
                _projectType = tmp_projectType;
                _transportationCode = tmp_trasportCode;

                RaisePropertyChangedEvent(nameof(DocumentCode));
                RaisePropertyChangedEvent(nameof(FarmerCode));
                RaisePropertyChangedEvent(nameof(ProjectType));
                RaisePropertyChangedEvent(nameof(TransportationCode));

                BuyingListBinding();
                OnFocusRequested(nameof(BaleBarcode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onCreateDateChangedCommand;

        public ICommand OnCreateDateChangedCommand
        {
            get { return _onCreateDateChangedCommand ?? (_onCreateDateChangedCommand = new RelayCommand(OnCreateDateChanged)); }
            set { _onCreateDateChangedCommand = value; }
        }

        private void OnCreateDateChanged(object e)
        {
            TransportationInfoBinding();
        }

        private ICommand _onSecondaryMonitorCommand;

        public ICommand OnSecondaryMonitorCommand
        {
            get { return _onSecondaryMonitorCommand ?? (_onSecondaryMonitorCommand = new RelayCommand(OnShowSecondaryMonitor)); }
            set { _onSecondaryMonitorCommand = value; }
        }

        private void OnShowSecondaryMonitor(object e)
        {
            window2 = new SecondaryMonitor();
            window2.DataContext = new vm_SecondaryMonitor();
            window2VM = ((vm_SecondaryMonitor)window2.DataContext);
            window2.Show();
        }

        private ICommand _onWindowClosedCommand;

        public ICommand OnWindowClosedCommand
        {
            get { return _onWindowClosedCommand ?? (_onWindowClosedCommand = new RelayCommand(OnWindowClosed)); }
            set { _onWindowClosedCommand = value; }
        }

        private void OnWindowClosed(object e)
        {
            if (window2 == null)
                return;
            window2.Close();
            CloseSerialOnExit();
        }

        private ICommand _onWindowLoadedCommand;

        public ICommand OnWindowLoadedCommand
        {
            get { return _onWindowLoadedCommand ?? (_onWindowLoadedCommand = new RelayCommand(OnWindowLoaded)); }
            set { _onWindowLoadedCommand = value; }
        }

        private void OnWindowLoaded(object e)
        {
            try
            {
                if (window2 == null)
                    return;
                window2 = new SecondaryMonitor();
                window2.DataContext = new vm_SecondaryMonitor();
                window2VM = ((vm_SecondaryMonitor)window2.DataContext);
                window2.Show();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion


        #region Function
        private void BuyingListBinding()
        {
            try
            {
                if (_buying == null)
                    return;

                _buyingDetailsList = BusinessLayer.Helper.BuyingHelper
                     .GetByBuyingDocument(_buying.Crop,
                     _buying.FarmerCode,
                     _buying.BuyingStationCode,
                     _buying.BuyingDocumentNumber)
                     .OrderByDescending(x => x.WeightDate)
                     .ToList();

                _totalBale = _buyingDetailsList.Count();

                var totalReject = _buyingDetailsList
                    .Where(x => x.RejectReason != null)
                    .Count();
                var totalBuying = _buyingDetailsList
                    .Where(x => x.Weight != null)
                    .Count();

                if (window2VM != null)
                {
                    window2VM.FarmerCode = _farmerCode;
                    window2VM.CountBale = totalBuying + "/" + (_totalBale - totalReject);
                }

                RaisePropertyChangedEvent(nameof(BuyingDetailsList));
                RaisePropertyChangedEvent(nameof(TotalBale));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void TransportationInfoBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_stationCode))
                    return;

                _transportationDocumentList = BuyingFacade.TransportationBL()
                    .GetByCreateDate(_createDate)
                    .Where(x => x.BuyingStationCode == _stationCode)
                    .ToList();

                RaisePropertyChangedEvent(nameof(TransportationDocumentList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Clear()
        {
            _farmerCode = "";
            _projectType = "";
            _documentCode = "";
            _baleBarcode = "";
            _grade = "";
            _weight = 0;
            _buyingDetailsList = null;
            _buying = null;

            RaisePropertyChangedEvent(nameof(FarmerCode));
            RaisePropertyChangedEvent(nameof(ProjectType));
            RaisePropertyChangedEvent(nameof(DocumentCode));
            RaisePropertyChangedEvent(nameof(BaleBarcode));
            RaisePropertyChangedEvent(nameof(Grade));
            RaisePropertyChangedEvent(nameof(Weight));
            RaisePropertyChangedEvent(nameof(BuyingDetailsList));

            OnFocusRequested(nameof(BaleBarcode));
        }
        #endregion


        #region DigitalScale
        private delegate void preventCrossThreading(string str);
        //private preventCrossThreading accessControlFromCentralThread;

        private void displayTextReadIn(string str)
        {
            try
            {
                decimal value;
                if (!Decimal.TryParse(str, out value))
                {
                    _weight = Convert.ToDecimal(str);
                    window2VM.Weight = _weight;
                    return;
                }

                _weight = Convert.ToDecimal(str);
                if (window2VM != null)
                    window2VM.Weight = _weight;

                RaisePropertyChangedEvent(nameof(Weight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            displayTextReadIn(DigitalScaleHelper.GetWeightResultFromTigerModel());
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (DigitalScaleHelper.serialPort.IsOpen)
            {
                e.Cancel = true; //cancel the fom closing
                Thread CloseDown = new Thread(new ThreadStart(CloseSerialOnExit)); //close port in new thread to avoid hang
                CloseDown.Start(); //close port in new thread to avoid hang
            }
        }

        private void CloseSerialOnExit()
        {
            try
            {
                DigitalScaleHelper.serialPort.DataReceived -= port_DataReceived;
                DigitalScaleHelper.serialPort.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DigitalScaleConnect()
        {
            try
            {
                DigitalScaleHelper.Setup();
                DigitalScaleHelper.serialPort.Open();
                DigitalScaleHelper.serialPort.DataReceived += port_DataReceived;
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
                _isManualInput = true;
                _weight = 0;
                RaisePropertyChangedEvent(nameof(IsManualInput));
                RaisePropertyChangedEvent(nameof(Weight));

                OnFocusRequested(nameof(Weight));
            }
        }
        #endregion
    }
}
