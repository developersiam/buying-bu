﻿using BUWinApp.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using System.Windows.Input;
using System.Windows;
using DomainModel;
using BusinessLayer;

namespace BUWinApp.ViewModel.BuyingStaff
{
    public class vm_Rejection : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        Window _window;
        public vm_Rejection(Window w)
        {
            _window = w;
            _rejectReasonList = user_setting.RejectReasons;
            RaisePropertyChangedEvent(nameof(RejectReasonList));
        }

        #region Properties
        private string _baleBarcode;
        public string BaleBarcode
        {
            get { return _baleBarcode; }
            set { _baleBarcode = value; }
        }

        private string _rejectReason;
        public string RejectReason
        {
            get { return _rejectReason; }
            set { _rejectReason = value; }
        }

        private string _buyerCode;
        public string BuyerCode
        {
            get { return _buyerCode; }
            set { _buyerCode = value; }
        }
        #endregion


        #region List
        public List<Buyer> BuyerList
        {
            get
            {
                return BuyingFacade.BuyerBL()
                  .GetAll()
                  .OrderBy(x => x.BuyerCode)
                  .ToList();
            }
        }

        private List<RejectReasonStruct> _rejectReasonList;

        public List<RejectReasonStruct> RejectReasonList
        {
            get { return _rejectReasonList; }
            set { _rejectReasonList = value; }
        }
        #endregion


        #region Command
        private ICommand _onClickCommand;

        public ICommand OnClickCommand
        {
            get { return _onClickCommand ?? (_onClickCommand = new RelayCommand(OnClick)); }
            set { _onClickCommand = value; }
        }

        private void OnClick(object e)
        {
            if (string.IsNullOrEmpty(_buyerCode))
            {
                MessageBoxHelper.Warning("โปรดระบุผู้ซื้อ (Buyer Code)");
                OnFocusRequested(nameof(BuyerCode));
                return;
            }

            _rejectReason = e.ToString();
            _window.Close();
        }
        #endregion
    }
}
