﻿using BusinessLayer;
using BUWinApp.Helper;
using BUWinApp.MVVM;
using BUWinApp.View.BuyingStaff;
using DomainModel;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BUWinApp.ViewModel.BuyingStaff
{
    public class vm_SupervisorConfirmation : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        SupervisorComfirmation _window;
        public vm_SupervisorConfirmation(SupervisorComfirmation window)
        {
            _window = window;
            _isConfirm = false;
        }


        #region Properties
        private string _username;

        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }

        private string _password;

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        private bool _isConfirm;

        public bool IsConfirm
        {
            get { return _isConfirm; }
            set { _isConfirm = value; }
        }

        #endregion


        #region Command

        private ICommand _onPasswordChangedCommand;

        public ICommand OnPasswordChangedCommand
        {
            get { return _onPasswordChangedCommand ?? (_onPasswordChangedCommand = new RelayCommand(OnPasswordChanged)); }
            set { _onPasswordChangedCommand = value; }
        }

        private void OnPasswordChanged(object obj)
        {
            _password = ((System.Windows.Controls.PasswordBox)obj).Password;
        }

        private ICommand _onSubmitCommand;

        public ICommand OnSubmitCommand
        {
            get { return _onSubmitCommand ?? (_onSubmitCommand = new RelayCommand(OnSubmit)); }
            set { _onSubmitCommand = value; }
        }

        private void OnSubmit(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_username))
                {
                    MessageBoxHelper.Warning("โปรดกรอกชื่อผู้ใช้");
                    OnFocusRequested(nameof(Username));
                    return;
                }

                if (string.IsNullOrEmpty(_password))
                {
                    MessageBoxHelper.Warning("โปรดกรอกรหัสผ่าน");
                    OnFocusRequested(nameof(Password));
                    return;
                }

                var user = BuyingFacade.UserBL().GetByUsername(_username);
                if (user == null)
                {
                    MessageBoxHelper.Warning("ไม่พบข้อมูลบัญชีผู้ใช้นี้ในระบบ");
                    OnFocusRequested(nameof(Username));
                    return;
                }

                if (user.Password != _password)
                {
                    MessageBoxHelper.Warning("รหัสผ่านไม่ถูกต้อง");
                    OnFocusRequested(nameof(Password));
                    return;
                }

                var roles = (from ur in BuyingFacade.UserRoleBL().GetByUser(user.Username)
                             from r in BuyingFacade.RoleBL().GetAll()
                             where ur.RoleID == r.RoleID
                             select new Role
                             {
                                 RoleID = ur.RoleID,
                                 RoleName = r.RoleName
                             }).ToList();

                if (roles.Where(x => x.RoleName == "Supervisor").Count() <= 0)
                {
                    MessageBoxHelper.Warning("สิทธิ์ผู้ confirm จะต้องเป็น supervisor เท่านั้น");
                    OnFocusRequested(nameof(Password));
                    return;
                }

                _isConfirm = true;
                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(OnClear)); }
            set { _onClearCommand = value; }
        }

        private void OnClear(object e)
        {
            _username = "";
            _password = "";

            RaisePropertyChangedEvent(nameof(Username));
            RaisePropertyChangedEvent(nameof(Password));
            OnFocusRequested(nameof(Username));
            OnFocusRequested(nameof(Password));
        }
        #endregion
    }
}
