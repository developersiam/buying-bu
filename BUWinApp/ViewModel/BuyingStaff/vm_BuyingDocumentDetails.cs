﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using BUWinApp.MVVM;
using BusinessLayer.Model;
using DomainModel;
using BusinessLayer.Helper;
using BusinessLayer;
using BUWinApp.Helper;
using System.Windows.Input;

namespace BUWinApp.ViewModel.BuyingStaff
{
    public class vm_BuyingDocumentDetails : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_BuyingDocumentDetails()
        {

        }


        #region Properties
        public RegistrationFarmer _register { get; set; }

        private string _documentCode;

        public string DocumentCode
        {
            get { return _documentCode; }
            set { _documentCode = value; }
        }

        private m_BuyingDocument _document;

        public m_BuyingDocument Document
        {
            get { return _document; }
            set
            {
                _document = value;
                _documentCode = _document.DocumentCode;
                _farmerCode = _document.FarmerCode;
                _register = BuyingFacade.RegistrationBL()
                    .GetSingle(_document.Crop, _document.FarmerCode);

                _isExtra = false;
                _isProject = false;

                RaisePropertyChangedEvent(nameof(DocumentCode));
                RaisePropertyChangedEvent(nameof(FarmerCode));
                RaisePropertyChangedEvent(nameof(IsExtra));
                RaisePropertyChangedEvent(nameof(IsProject));

                BuyingDetailsBinding();
            }
        }

        private string _farmerCode;

        public string FarmerCode
        {
            get { return _farmerCode; }
            set { _farmerCode = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private string _baleBarcode;

        public string BaleBarcode
        {
            get { return _baleBarcode; }
            set { _baleBarcode = value; }
        }

        private bool _isExtra;

        public bool IsExtra
        {
            get { return _isExtra; }
            set { _isExtra = value; }
        }

        private bool _isProject;

        public bool IsProject
        {
            get { return _isProject; }
            set { _isProject = value; }
        }
        #endregion



        #region List
        private List<m_FarmerProject> _farmerProjectList;

        public List<m_FarmerProject> FarmerProjectList
        {
            get { return _farmerProjectList; }
            set { _farmerProjectList = value; }
        }

        private List<m_Buying> _buyingList;

        public List<m_Buying> BuyingList
        {
            get { return _buyingList; }
            set { _buyingList = value; }
        }
        #endregion



        #region Command
        private ICommand _onAddCommand;

        public ICommand OnAddCommand
        {
            get { return _onAddCommand ?? (_onAddCommand = new RelayCommand(OnAdd)); }
            set { _onAddCommand = value; }
        }

        private void OnAdd(object e)
        {
            try
            {
                if (string.IsNullOrEmpty(_baleBarcode))
                {
                    MessageBoxHelper.Warning("โปรดสแกนหมายเลขบาร์โค้ตลงในช่อง Bale Barcode");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                if (RegularExpressionHelper.IsBaleBarcodeCorrectFormat(_baleBarcode) == false)
                    throw new ArgumentException("รหัสบาร์โค้ตไม่ตรงตามรูปแบบที่กำหนด โปรดตรวจสอบใหม่อีกครั้ง");

                if (IsExtra == true)
                {
                    BuyingFacade.BuyingBL()
                           .RegisterBarcodeInExtraQuota(_baleBarcode,
                           _document.Crop,
                           _document.BuyingStationCode,
                           _document.FarmerCode,
                           _document.BuyingDocumentNumber,
                           user_setting.User.Username,
                           _isProject);
                }
                else
                {
                    BuyingFacade.BuyingBL()
                        .RegisterBarcodeInNormalQuota(_baleBarcode,
                        _document.Crop,
                        _document.BuyingStationCode,
                        _document.FarmerCode,
                        _document.BuyingDocumentNumber,
                        user_setting.User.Username,
                        _isProject);
                }

                BuyingDetailsBinding();
                _baleBarcode = "";
                RaisePropertyChangedEvent(nameof(BaleBarcode));
                OnFocusRequested(nameof(BaleBarcode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object e)
        {
            try
            {
                var model = BuyingHelper.GetByBaleBarcode(e.ToString());
                if (model == null)
                    throw new ArgumentException("ไม่พบรหัสบาร์โค้ตของป้ายบาร์โค้ตนี้");

                BuyingFacade.BuyingBL().UnRisterBarcode(model.BaleBarcode);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(OnClear)); }
            set { _onClearCommand = value; }
        }

        private void OnClear(object e)
        {
            ClearForm();
        }

        private ICommand _onIsExtraCheckedCommand;

        public ICommand OnIsExtraCheckedCommand
        {
            get { return _onIsExtraCheckedCommand ?? (_onIsExtraCheckedCommand = new RelayCommand(OnIsExtra)); }
            set { _onIsExtraCheckedCommand = value; }
        }

        private void OnIsExtra(object e)
        {
            try
            {
                if (_isExtra)
                {
                    _farmerProjectList = FarmerProjectHelper
                        .GetQuotaAndSoldByFarmerVersion2(_document.Crop, _document.FarmerCode);
                    if (_farmerProjectList.Sum(x => x.ExtraQuota) <= 0)
                    {
                        MessageBoxHelper.Warning("ท่านไม่สามารถจ่ายป้ายใน Extra quota ได้ เนื่องจากชาวไร่รายนี้ไม่มี Extra quota");
                        _isExtra = false;
                        RaisePropertyChangedEvent(nameof(IsExtra));
                        return;
                    }
                    else
                        MessageBoxHelper.Warning("โปรดตรวจสอบให้แน่ใจอีกครั้ง เมื่อท่านจ่ายป้ายใน Extra quota " +
                            "ราคาขายจะถูกหักตามเงื่อนไขที่บริษัทกำหนด โปรดตรวจสอบเงื่อนไขกับผู้จัดการฝ่ายไร่อีกครั้ง");
                }
                else
                    _isExtra = false;

                OnFocusRequested(nameof(BaleBarcode));
                RaisePropertyChangedEvent(nameof(IsExtra));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onIsProjectCheckedCommand;

        public ICommand OnIsProjectCheckedCommand
        {
            get { return _onIsProjectCheckedCommand ?? (_onIsProjectCheckedCommand = new RelayCommand(OnIsProject)); }
            set { _onIsProjectCheckedCommand = value; }
        }

        private void OnIsProject(object e)
        {
            if (IsProject)
                MessageBoxHelper.Warning("เมื่อท่านเลือกทำเครื่องหมายถูกหน้าหัวข้อนี้ จะถือว่ายาห่อนี้เป็นยาในโครงการทันที (Project)");
            else
                MessageBoxHelper.Warning("เมื่อท่านเอาเครื่องหมายถูกหน้าหัวข้อนี้ออก จะเป็นการจ่ายป้ายแบบปกติ");

            OnFocusRequested(nameof(BaleBarcode));
        }
        #endregion



        #region Function
        private void ClearForm()
        {
            _baleBarcode = "";
            _isProject = false;
            _isExtra = false;

            RaisePropertyChangedEvent(nameof(FarmerCode));
            RaisePropertyChangedEvent(nameof(BaleBarcode));
            RaisePropertyChangedEvent(nameof(IsProject));
            RaisePropertyChangedEvent(nameof(IsExtra));

            OnFocusRequested(nameof(BaleBarcode));

            BuyingDetailsBinding();
        }

        private void BuyingDetailsBinding()
        {
            try
            {
                _buyingList = BuyingHelper.GetByBuyingDocument(
                        _document.Crop,
                        _document.FarmerCode,
                        _document.BuyingStationCode,
                        _document.BuyingDocumentNumber)
                        .OrderByDescending(x => x.RegisterBarcodeDate)
                        .ToList();

                _totalRecord = _buyingList.Count();

                RaisePropertyChangedEvent(nameof(BuyingList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #endregion
    }
}
