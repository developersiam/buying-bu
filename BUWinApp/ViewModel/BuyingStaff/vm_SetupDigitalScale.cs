﻿using BUWinApp.Helper;
using BUWinApp.MVVM;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace BUWinApp.ViewModel.BuyingStaff
{
    public class vm_SetupDigitalScale : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        Window _window;
        public vm_SetupDigitalScale(Window window)
        {
            _window = window;

            _portName = Properties.Settings.Default.PortName;
            _buardRate = Properties.Settings.Default.BaudRate;
            _parity = Properties.Settings.Default.Parity;
            _stopBit = Properties.Settings.Default.StopBits;
            _dataBit = Properties.Settings.Default.DataBit;
            _threadSleep = Properties.Settings.Default.ThreadSleep;
        }


        #region Properties
        private string _portName;

        public string PortName
        {
            get { return _portName; }
            set { _portName = value; }
        }

        private Parity _parity;

        public Parity Parity
        {
            get { return _parity; }
            set { _parity = value; }
        }

        private short _dataBit;

        public short DataBit
        {
            get { return _dataBit; }
            set { _dataBit = value; }
        }

        private int _buardRate;

        public int BuardRate
        {
            get { return _buardRate; }
            set { _buardRate = value; }
        }

        private StopBits _stopBit;

        public StopBits StopBit
        {
            get { return _stopBit; }
            set { _stopBit = value; }
        }

        private int _threadSleep;

        public int ThreadSleep
        {
            get { return _threadSleep; }
            set { _threadSleep = value; }
        }



        #endregion


        #region List
        private List<string> _portNameList;

        public List<string> PortNameList
        {
            get
            {
                var list = new List<string>();
                foreach (var item in SerialPort.GetPortNames())
                    list.Add(item);
                return list;
            }
            set { _portNameList = value; }
        }

        private List<Parity> _parityList;

        public List<Parity> ParityList
        {
            get
            {
                var list = new List<Parity>();
                list.Add(Parity.Even);
                list.Add(Parity.Odd);
                list.Add(Parity.None);
                list.Add(Parity.Mark);
                list.Add(Parity.Space);
                return list;
            }
            set { _parityList = value; }
        }

        private List<short> _dataBitList;

        public List<short> DataBitList
        {
            get
            {
                var list = new List<short>();
                list.Add(8);
                list.Add(7);
                list.Add(6);
                list.Add(5);
                return list;
            }
            set { _dataBitList = value; }
        }

        private List<int> _buardRateList;

        public List<int> BuardRateList
        {
            get
            {
                var list = new List<int>();
                list.Add(115200);
                list.Add(57600);
                list.Add(38400);
                list.Add(19200);
                list.Add(9600);
                list.Add(7200);
                list.Add(4800);
                list.Add(2400);
                return list;
            }
            set { _buardRateList = value; }
        }

        private List<StopBits> _stopBitList;

        public List<StopBits> StopBitList
        {
            get
            {
                var list = new List<StopBits>();
                list.Add(StopBits.None);
                list.Add(StopBits.One);
                list.Add(StopBits.OnePointFive);
                list.Add(StopBits.Two);
                return list;
            }
            set { _stopBitList = value; }
        }

        private List<int> _threadSleepList;

        public List<int> ThreadSleepList
        {
            get
            {
                var list = new List<int>();
                list.Add(100);
                list.Add(200);
                list.Add(300);
                list.Add(400);
                list.Add(500);
                list.Add(600);
                list.Add(700);
                list.Add(800);
                list.Add(900);
                list.Add(1000);
                return list;
            }
            set { _threadSleepList = value; }
        }

        #endregion


        #region Command
        private ICommand _onSaveCommand;

        public ICommand OnSaveCommand
        {
            get { return _onSaveCommand ?? (_onSaveCommand = new RelayCommand(OnSave)); }
        }

        private void OnSave(object e)
        {
            if (string.IsNullOrEmpty(_portName))
            {
                MessageBoxHelper.Warning("โปรดระบุ " + nameof(PortName));
                OnFocusRequested(nameof(PortName));
            }
            if (string.IsNullOrEmpty(_buardRate.ToString()))
            {
                MessageBoxHelper.Warning("โปรดระบุ " + nameof(BuardRate));
                OnFocusRequested(nameof(BuardRate));
            }
            if (string.IsNullOrEmpty(_dataBit.ToString()))
            {
                MessageBoxHelper.Warning("โปรดระบุ " + nameof(DataBit));
                OnFocusRequested(nameof(DataBit));
            }
            if (string.IsNullOrEmpty(_threadSleep.ToString()))
            {
                MessageBoxHelper.Warning("โปรดระบุ " + nameof(ThreadSleep));
                OnFocusRequested(nameof(ThreadSleep));
            }

            Properties.Settings.Default.PortName = _portName;
            Properties.Settings.Default.BaudRate = _buardRate;
            Properties.Settings.Default.StopBits = _stopBit;
            Properties.Settings.Default.DataBit = _dataBit;
            Properties.Settings.Default.Parity = _parity;
            Properties.Settings.Default.ThreadSleep = _threadSleep;

            Properties.Settings.Default.Save();

            MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ!");
            _window.Close();
        }
        #endregion
    }
}
