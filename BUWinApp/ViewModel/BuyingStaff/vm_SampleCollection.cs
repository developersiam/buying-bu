﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using System.Windows.Input;
using DomainModel;
using BUWinApp.Helper;
using BusinessLayer;

namespace BUWinApp.ViewModel.BuyingStaff
{
    public class vm_SampleCollection : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_SampleCollection()
        {
            _isCPA = true;
            _bag = 1;

            RaisePropertyChangedEvent(nameof(IsNicotine));
            RaisePropertyChangedEvent(nameof(Bag));
        }


        #region Properties
        private string _citizenID;

        public string CitizenID
        {
            get { return _citizenID; }
            set { _citizenID = value; }
        }

        private string _gapGroupCode;

        public string GAPGroupCode
        {
            get { return _gapGroupCode; }
            set { _gapGroupCode = value; }
        }

        private Person _person;

        public Person Person
        {
            get { return _person; }
            set { _person = value; }
        }

        private string _position;

        public string Position
        {
            get { return _position; }
            set { _position = value; }
        }

        private short _bag;

        public short Bag
        {
            get { return _bag; }
            set { _bag = value; }
        }

        private bool _isCPA;

        public bool IsCPA
        {
            get { return _isCPA; }
            set
            {
                _isCPA = value;
                _isNicotine = !_isCPA;
                RaisePropertyChangedEvent(nameof(IsNicotine));
            }
        }

        private bool _isNicotine;

        public bool IsNicotine
        {
            get { return _isNicotine; }
            set
            {
                _isNicotine = value;
                _isCPA = !_isNicotine;
                RaisePropertyChangedEvent(nameof(IsCPA));
            }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }
        #endregion


        #region List
        private List<CPASampleCollection2020> _collectionList;

        public List<CPASampleCollection2020> CollectionList
        {
            get { return _collectionList; }
            set { _collectionList = value; }
        }

        public List<string> TypeList
        {
            get
            {
                var list = new List<string>();
                list.Add("CPA");
                list.Add("Nicotine");
                return list;
            }
        }

        public List<string> PositionList
        {
            get
            {
                var list = new List<string>();
                list.Add("X");
                list.Add("C");
                list.Add("B");
                list.Add("T");
                return list;
            }
        }
        #endregion


        #region Command
        private ICommand _onSearchCommand;

        public ICommand OnSearchCommand
        {
            get { return _onSearchCommand ?? (_onSearchCommand = new RelayCommand(OnSearch)); }
            set { _onSearchCommand = value; }
        }

        private void OnSearch(object obj)
        {
            CitizenIDBinding();
        }

        private ICommand _onResetCommand;

        public ICommand OnResetCommand
        {
            get { return _onResetCommand ?? (_onResetCommand = new RelayCommand(OnReset)); }
            set { _onResetCommand = value; }
        }

        private void OnReset(object obj)
        {
            _citizenID = null;
            _person = null;
            _gapGroupCode = null;
            _bag = 1;

            RaisePropertyChangedEvent(nameof(CitizenID));
            RaisePropertyChangedEvent(nameof(Person));
            RaisePropertyChangedEvent(nameof(Bag));
            CollectionListBinding();
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                var item = (CPASampleCollection2020)obj;
                if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลรายการนี้ใช่หรือไม่?") == System.Windows.MessageBoxResult.No)
                    return;

                BuyingFacade.CPASampleCollection2020BL().Delete(item.Id);
                CollectionListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onSaveCommand;

        public ICommand OnSaveCommand
        {
            get { return _onSaveCommand ?? (_onSaveCommand = new RelayCommand(OnSave)); }
            set { _onSaveCommand = value; }
        }

        private void OnSave(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_citizenID))
                {
                    MessageBoxHelper.Warning("โปรดระบุ citizenID");
                    OnFocusRequested(nameof(CitizenID));
                    return;
                }

                if (_bag <= 0)
                {
                    MessageBoxHelper.Warning("จำนวนถุงจะต้องมากกว่า 0");
                    OnFocusRequested(nameof(Bag));
                    return;
                }

                if (string.IsNullOrEmpty(_gapGroupCode))
                {
                    MessageBoxHelper.Warning("ไม่พบ GAP group code ของชาวไร่รายนี้ โปรดติดต่อผู้ดูแลระบบ");
                    OnFocusRequested(nameof(Bag));
                    return;
                }

                var position = obj.ToString();
                if (string.IsNullOrEmpty(position))
                    throw new ArgumentException("Position cannot be empty.");

                BuyingFacade.CPASampleCollection2020BL()
                    .Add(new CPASampleCollection2020
                    {
                        Id = Guid.NewGuid(),
                        CitizenID = _citizenID,
                        GAPGroupCode = _gapGroupCode,
                        Position = position,
                        SampleType = _isCPA == true ? "CPA" : "Nicotine",
                        Bag = _bag,
                        CollectionDate = DateTime.Now,
                        CollectionUser = user_setting.User.Username,
                    });

                CollectionListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onPrintStickerCommand;

        public ICommand OnPrintStickerCommand
        {
            get { return _onPrintStickerCommand ?? (_onPrintStickerCommand = new RelayCommand(OnPrintSticker)); }
            set { _onPrintStickerCommand = value; }
        }

        private void OnPrintSticker(object obj)
        {
            try
            {
                if (_person == null)
                    throw new ArgumentException("ไม่พบข้อมูลชาวไร่รายนี้ โปรดติดต่อผู้ดูแลระบบ");

                if (string.IsNullOrEmpty(_gapGroupCode))
                    throw new ArgumentException("ไม่พบ GAP group code ของชาวไร่รายนี้ โปรดติดต่อผู้ดูแลระบบ");

                if (string.IsNullOrEmpty(_position))
                {
                    MessageBoxHelper.Warning("โปรดเลือก Position ก่อนกดปริ้นท์สติกเกอร์");
                    OnFocusRequested(nameof(Position));
                    return;
                }


                TSCHelper.PrintSampleCollectionSticker(this);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion


        #region Function
        private void CitizenIDBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_citizenID))
                {
                    MessageBoxHelper.Warning("โปรดระบุ citizenID");
                    OnFocusRequested(nameof(CitizenID));
                    return;
                }

                _person = BuyingFacade.PersonBL().GetSingle(_citizenID);
                if (_person == null)
                {
                    MessageBoxHelper.Warning("ไม่พบข้อมูลชาวไร่รหัสประจำตัวประชาชน " + _citizenID + " ในระบบ");
                    OnFocusRequested(nameof(CitizenID));
                    return;
                }

                _person.FirstName = _person.Prefix + _person.FirstName + " " + _person.LastName;

                var gapGrop = BuyingFacade.GAPGroupMember2020BL()
                    .GetSingleByCitizenID(user_setting.Crop.Crop1, _citizenID);
                if (gapGrop == null)
                    throw new ArgumentException("ไม่พบ GAP group code ของชาวไร่รายนี้ โปรดติดต่อผู้ดูแลระบบ");

                _gapGroupCode = gapGrop.GAPGroupCode;

                RaisePropertyChangedEvent(nameof(GAPGroupCode));
                RaisePropertyChangedEvent(nameof(Person));

                CollectionListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CollectionListBinding()
        {
            try
            {
                _collectionList = BuyingFacade.CPASampleCollection2020BL()
                    .GetByFarmer2(_citizenID, user_setting.Crop.Crop1);
                _totalRecord = _collectionList.Count();

                RaisePropertyChangedEvent(nameof(CollectionList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
