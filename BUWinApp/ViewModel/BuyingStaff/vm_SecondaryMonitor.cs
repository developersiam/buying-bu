﻿using BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;

namespace BUWinApp.ViewModel.BuyingStaff
{
    public class vm_SecondaryMonitor : ObservableObject
    {
        private decimal _weight;

        public decimal Weight
        {
            get { return _weight; }
            set
            {
                _weight = value;
                RaisePropertyChangedEvent(nameof(Weight));
            }
        }


        private string _grade;

        public string Grade
        {
            get { return _grade; }
            set
            {
                _grade = value;
                RaisePropertyChangedEvent(nameof(Grade));
            }
        }


        private string _farmerCode;

        public string FarmerCode
        {
            get { return _farmerCode; }
            set
            {
                _farmerCode = value;
                var model = BuyingFacade.FarmerBL()
                    .GetByFarmerCode(_farmerCode);

                if (model == null)
                    return;

                _farmerName = model.Person.Prefix +
                    model.Person.FirstName + "  " +
                    model.Person.LastName;

                RaisePropertyChangedEvent(nameof(FarmerName));
            }
        }


        private string _farmerName;

        public string FarmerName
        {
            get { return _farmerName; }
            set { _farmerName = value; }
        }


        private string _countBale;

        public string CountBale
        {
            get { return _countBale; }
            set
            {
                _countBale = value;
                RaisePropertyChangedEvent(nameof(CountBale));
            }
        }

    }
}
