﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using System.Windows;
using System.Windows.Input;
using BusinessLayer.Model;
using BusinessLayer.Helper;
using BUWinApp.Helper;
using BusinessLayer;
using BUWinApp.View.BuyingStaff;
using BUWinApp.ViewModel.Hessian;
using DomainModel;
using BUWinApp.ViewModel.BuyingStaff.DebtAndReceipt;
using BUWinApp.View.BuyingStaff.DebtAndReceipt;
using Microsoft.Office.Interop.Excel;
using BusinessLayer.BL;
using Microsoft.Reporting.WinForms;
using System.IO;
using System.Drawing.Printing;
using System.Diagnostics;

namespace BUWinApp.ViewModel.BuyingStaff
{
    public class vm_VoucherDetails : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_VoucherDetails()
        {
            _copies = Properties.Settings.Default.VoucherCopies;
            RaisePropertyChangedEvent(nameof(Copies));
            RaisePropertyChangedEvent(nameof(Document));
        }


        #region VoucherInfo
        private m_BuyingDocument _document;

        public m_BuyingDocument Document
        {
            get { return _document; }
            set
            {
                _document = value;
                BuyingDocumentBinding();
                DebtSummaryBinding();
            }
        }

        private string _farmerCode;

        public string FarmerCode
        {
            get { return _farmerCode; }
            set { _farmerCode = value; }
        }

        private string _documentCode;

        public string DocumentCode
        {
            get { return _documentCode; }
            set { _documentCode = value; }
        }

        private bool _isFinish;

        public bool IsFinish
        {
            get { return _isFinish; }
            set { _isFinish = value; }
        }

        private decimal _buyingAmount;

        public decimal BuyingAmount
        {
            get { return _buyingAmount; }
            set { _buyingAmount = value; }
        }

        private decimal _deductionInVoucher;

        public decimal DeductionInVoucher
        {
            get { return _deductionInVoucher; }
            set { _deductionInVoucher = value; }
        }

        private decimal _netBuyingAmount;

        public decimal NetBuyingAmount
        {
            get { return _netBuyingAmount; }
            set { _netBuyingAmount = value; }
        }

        private short _estimateKgRemaining;

        public short EstimateKgRemaining
        {
            get { return _estimateKgRemaining; }
            set { _estimateKgRemaining = value; }
        }

        private short _copies;

        public short Copies
        {
            get { return _copies; }
            set { _copies = value; }
        }

        #endregion



        #region DebtAndReceiptInfo
        private decimal _totalDebtAmount;

        public decimal TotalDebtAmount
        {
            get { return _totalDebtAmount; }
            set { _totalDebtAmount = value; }
        }

        private decimal _totalReceiptAmount;

        public decimal TotalReceiptAmount
        {
            get { return _totalReceiptAmount; }
            set { _totalReceiptAmount = value; }
        }

        private decimal _debtBalanceAmount;

        public decimal DebtBalanceAmount
        {
            get { return _debtBalanceAmount; }
            set { _debtBalanceAmount = value; }
        }
        #endregion



        #region List
        private List<m_Buying> _buyingList;

        public List<m_Buying> BuyingList
        {
            get { return _buyingList; }
            set { _buyingList = value; }
        }

        private List<m_DebtAndReceiptSummary> _debtAndReceiptList;

        public List<m_DebtAndReceiptSummary> DebtAndReceiptList
        {
            get { return _debtAndReceiptList; }
            set { _debtAndReceiptList = value; }
        }

        private List<DebtReceipt> _receiptListInVoucher;

        public List<DebtReceipt> ReceiptListInVoucher
        {
            get { return _receiptListInVoucher; }
            set { _receiptListInVoucher = value; }
        }

        #endregion



        #region Command
        private ICommand _onRefreshCommand;

        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(OnRefresh)); }
            set { _onRefreshCommand = value; }
        }

        private void OnRefresh(object obj)
        {
            _documentCode = "";
            RaisePropertyChangedEvent(nameof(DocumentCode));
            BuyingDocumentBinding();
            DebtSummaryBinding();
        }

        private ICommand _onFinishCommand;

        public ICommand OnFinishCommand
        {
            get { return _onFinishCommand ?? (_onFinishCommand = new RelayCommand(OnFinish)); }
            set { _onFinishCommand = value; }
        }

        private void OnFinish(object obj)
        {
            try
            {
                if (_document.TotalBale <= 0)
                    if (MessageBoxHelper.Question("ไม่พบห่อยาใน voucher นี้ ท่านต้องการปิดการขาย voucher นี้ใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;

                var notFinishBale = _document.TotalBale - _document.TotalReject;
                if (notFinishBale != _document.TotalWeight)
                {
                    MessageBoxHelper.Warning("มีป้ายบาร์โค้ตอีกจำนวน " +
                        (notFinishBale - _document.TotalWeight) +
                        " ห่อ ที่ยังไม่ได้บันทึกข้อมูลน้ำหนัก" + Environment.NewLine +
                        "โปรดตรวจสอบให้ครบถ้วนอีกครั้ง");
                    return;
                }

                BuyingFacade.BuyingDocumentBL()
                    .Finish(_document.Crop,
                    _document.BuyingStationCode,
                    _document.FarmerCode,
                    _document.BuyingDocumentNumber,
                    user_setting.User.Username);

                BuyingDocumentBinding();
                DebtSummaryBinding();
                MessageBoxHelper.Info("ใบเวาเชอร์นี้ถูกเปลี่ยนสถานะเป็น Finish แล้ว");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onUnfinishCommand;

        public ICommand OnUnfinishCommand
        {
            get { return _onUnfinishCommand ?? (_onUnfinishCommand = new RelayCommand(OnUnfinish)); }
            set { _onUnfinishCommand = value; }
        }

        private void OnUnfinish(object obj)
        {
            try
            {
                if (_document.IsAccountFinish)
                    throw new ArgumentException("เวาเชอร์นี้แผนกบัญชียืนยันข้อมูลแล้ว ไม่สามารถยกเลิกสถานะปิดการขายได้");

                if (MessageBoxHelper.Question("หากในเวาเชอร์นี้มีการหักชำระหนี้ไว้แล้ว " +
                    "ระบบจะลบข้อมูลการชำระหนี้โดยอัตโนมัติ " +
                    "โดยท่านจะต้องหักหนี้และพิมพ์เวาเชอร์ให้กับชาวไร่ใหม่อีกครั้ง" +
                    Environment.NewLine +
                    "ท่านต้องการ Unfinish ใบเวาเชอร์นี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                BuyingFacade.BuyingDocumentBL()
                    .Unfinish(_document.Crop,
                    _document.BuyingStationCode,
                    _document.FarmerCode,
                    _document.BuyingDocumentNumber,
                    user_setting.User.Username);

                BuyingDocumentBinding();
                DebtSummaryBinding();
                MessageBoxHelper.Info("ใบเวาเชอร์นี้ถูกเปลี่ยนสถานะเป็น Unfinish แล้ว");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onHessianCommand;

        public ICommand OnHessianCommand
        {
            get { return _onHessianCommand ?? (_onHessianCommand = new RelayCommand(OnHassian)); }
            set { _onHessianCommand = value; }
        }

        private void OnHassian(object obj)
        {
            try
            {
                View.Hessian.DistibutionByVoucher window = new View.Hessian.DistibutionByVoucher();
                var vm = new vm_DistibutionByVoucher();
                window.DataContext = vm;
                vm.Document = _document;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDebtDeductionCommand;

        public ICommand OnDebtDeductionCommand
        {
            get { return _onDebtDeductionCommand ?? (_onDebtDeductionCommand = new RelayCommand(DebtDeduction)); }
            set { _onDebtDeductionCommand = value; }
        }

        private void DebtDeduction(object obj)
        {
            try
            {
                if (!_document.IsFinish)
                    throw new ArgumentException("โปรดทำการปิดการขายก่อนเพื่อเริ่มต้นทำการหักชำระหนี้");

                if (_document.IsAccountFinish)
                    throw new ArgumentException("Leaf Account ได้ทำการ lock เวาเชอร์นี้แล้ว ไม่สามารถแก้ไขข้อมูลการหักชำระได้");

                if (_buyingAmount <= 0)
                    throw new ArgumentException("ไม่พบยอดขายรวมใน voucher นี้ ไม่สามารถหักเงินจากการขายได้");

                var window = new VoucherDeduction();
                var vm = new vm_VoucherDeduction();
                vm.BuyingDocument = _document;
                vm.BuyingList = _buyingList
                    .Select(x => new DomainModel.Buying
                    {
                        BaleBarcode = x.BaleBarcode,
                        ProjectType = x.ProjectType,
                        Weight = x.Weight,
                        Grade = x.Grade,
                        BuyingGrade = x.BuyingGrade
                    })
                    .ToList();
                vm.ShowCurrentCropDebt = true;
                window.DataContext = vm;
                window.ShowDialog();

                BuyingDocumentBinding();
                DebtSummaryBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onPrintCommand;

        public ICommand OnPrintCommand
        {
            get { return _onPrintCommand ?? (_onPrintCommand = new RelayCommand(OnPrint)); }
            set { _onPrintCommand = value; }
        }

        private void OnPrint(object obj)
        {
            try
            {
                if (_copies != Properties.Settings.Default.VoucherCopies)
                {
                    Properties.Settings.Default.VoucherCopies = _copies;
                    Properties.Settings.Default.Save();
                    RaisePropertyChangedEvent(nameof(Copies));
                }

                if (_document.IsPrinted)
                    throw new ArgumentException("เวาเชอร์นี้ถูกพิมพ์แล้วเมื่อ " + _document.PrintDate + " ไม่สามารถพิมพ์ซ้ำได้");

                if (_buyingAmount <= 0)
                    throw new ArgumentException("ไม่พบยอดขายรวมใน voucher นี้ ไม่สามารถพิมพ์เวาเชอร์ได้");

                decimal dripProjectCropBalance = _debtAndReceiptList
                    .Where(x => x.DebtTypeCode == "DP" &&
                    x.DeductionCrop == _document.Crop)
                    .Sum(x => x.BalanceAmount);

                decimal voucherDeductionAmount = _receiptListInVoucher
                    .Where(x => x.DebtSetup.DebtTypeCode == "DP")
                    .Sum(x => x.Amount);

                if (dripProjectCropBalance > 0 && voucherDeductionAmount <= 0)
                    throw new ArgumentException("ท่านยังไม่ได้หักชำระหนี้โครงการน้ำหยด(บังคับหัก) ระบบจะไม่สามารถพิมพ์ใบเวาเชอร์ได้");

                ReportHelper.PrintBuyingVoucher2023(_document, _debtAndReceiptList, _receiptListInVoucher, _copies);
                BuyingFacade.BuyingDocumentBL().Printed(_document.Crop,
                    _document.BuyingStationCode,
                    _document.FarmerCode,
                    _document.BuyingDocumentNumber,
                    user_setting.User.Username);
                MessageBoxHelper.Info("พิมพ์เอกสารสำเร็จ");
                BuyingDocumentBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEstimateKgSaveCommand;

        public ICommand OnEstimateKgSaveCommand
        {
            get { return _onEstimateKgSaveCommand ?? (_onEstimateKgSaveCommand = new RelayCommand(OnEstimateKgSave)); }
            set { _onEstimateKgSaveCommand = value; }
        }

        private void OnEstimateKgSave(object obj)
        {
            try
            {
                if (_estimateKgRemaining <= 0)
                {
                    MessageBoxHelper.Warning("จำนวนยาที่เหลือไม่ควรน้อยกว่า 1 กก.");
                    OnFocusRequested(nameof(EstimateKgRemaining));
                    return;
                }

                BuyingFacade.RegistrationBL()
                    .UpdateEstimateBalanceKgAfterSold(_document.Crop,
                    _document.FarmerCode,
                    _estimateKgRemaining,
                    user_setting.User.Username);

                MessageBoxHelper.Info("บันทึกข้อมูลจำนวนยาคงเหลือที่แจ้งไว้เรียบร้อย");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDebtAndReceiptSummaryPreviewCommand;

        public ICommand OnDebtAndReceiptSummaryPreviewCommand
        {
            get { return _onDebtAndReceiptSummaryPreviewCommand ?? (_onDebtAndReceiptSummaryPreviewCommand = new RelayCommand(DebtAndReceiptSummaryPreview)); }
            set { _onDebtAndReceiptSummaryPreviewCommand = value; }
        }

        private void DebtAndReceiptSummaryPreview(object obj)
        {
            try
            {
                var window = new Receipt();
                var vm = new vm_Receipt();
                vm.Crop = _document.Crop;
                vm.FarmerCode = _document.FarmerCode;
                window.DataContext = vm;
                window.ShowDialog();
                BuyingDocumentBinding();
                DebtSummaryBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion



        #region Function
        private void BuyingDocumentBinding()
        {
            try
            {
                _document = BuyingDocumentHelper.GetSingle(
                    _document.Crop,
                    _document.BuyingStationCode,
                    _document.FarmerCode,
                    _document.BuyingDocumentNumber);

                _farmerCode = _document.FarmerCode;
                _isFinish = _document.IsFinish;
                _documentCode = _document.Crop + "-" +
                    _document.BuyingStationCode + "-" +
                    _document.FarmerCode + "-" +
                    _document.BuyingDocumentNumber;

                _buyingList = BuyingHelper
                    .GetByBuyingDocument(_document.Crop,
                    _document.FarmerCode,
                    _document.BuyingStationCode,
                    _document.BuyingDocumentNumber);

                _buyingAmount = (decimal)_buyingList.Sum(x => x.Price);
                _receiptListInVoucher = BuyingFacade.DebtReceiptBL()
                    .GetByBuyingDocument(_document.Crop,
                    _document.BuyingStationCode,
                    _document.FarmerCode,
                    _document.BuyingDocumentNumber);
                _deductionInVoucher = _receiptListInVoucher.Sum(x => x.Amount);
                _netBuyingAmount = _buyingAmount - _deductionInVoucher;

                RaisePropertyChangedEvent(nameof(Document));
                RaisePropertyChangedEvent(nameof(FarmerCode));
                RaisePropertyChangedEvent(nameof(DocumentCode));
                RaisePropertyChangedEvent(nameof(BuyingList));
                RaisePropertyChangedEvent(nameof(IsFinish));
                RaisePropertyChangedEvent(nameof(BuyingAmount));
                RaisePropertyChangedEvent(nameof(ReceiptListInVoucher));
                RaisePropertyChangedEvent(nameof(DeductionInVoucher));
                RaisePropertyChangedEvent(nameof(NetBuyingAmount));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DebtSummaryBinding()
        {
            try
            {
                _debtAndReceiptList = DebtSetupHelper.GetByFarmerDeductionCrop(_document.Crop, _document.FarmerCode);
                RaisePropertyChangedEvent(nameof(DebtAndReceiptList));

                _totalDebtAmount = _debtAndReceiptList.Sum(x => x.Amount);
                _totalReceiptAmount = _debtAndReceiptList.Sum(x => x.ReceiptAmount);
                _debtBalanceAmount = _totalDebtAmount - _totalReceiptAmount;

                var dripProjectList = _debtAndReceiptList
                    .Where(x => x.DebtTypeCode == "DP")
                    .ToList();
                var cropInputsList = _debtAndReceiptList
                    .Where(x => x.DebtTypeCode == "CI")
                    .ToList();

                RaisePropertyChangedEvent(nameof(TotalDebtAmount));
                RaisePropertyChangedEvent(nameof(TotalReceiptAmount));
                RaisePropertyChangedEvent(nameof(DebtBalanceAmount));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Print()
        {
            try
            {
                LocalReport report = new LocalReport();
                ReportDataSource datasource = new ReportDataSource();
                var reportPath = "BUWinApp.RDLCReport.Report1.rdlc";
                var data = BuyingHelper
                    .GetByBuyingDocument(_document.Crop,
                    _document.FarmerCode,
                    _document.BuyingStationCode,
                    _document.BuyingDocumentNumber);
                datasource.Name = "m_Buying";
                datasource.Value = data;

                report.DataSources.Add(datasource);
                report.ReportEmbeddedResource = reportPath;

                string filePath = Path.GetTempFileName();
                Export(report, filePath);

                ProcessStartInfo info = new ProcessStartInfo(filePath);
                info.Verb = "Print";
                info.CreateNoWindow = true;
                info.WindowStyle = ProcessWindowStyle.Hidden;
                Process.Start(info);

                report.Dispose();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public string Export(LocalReport report, string filePath)
        {
            string ack = "";
            try
            {
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;

                byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);
                using (FileStream stream = System.IO.File.OpenWrite(filePath))
                {
                    stream.Write(bytes, 0, bytes.Length);
                }
                return ack;
            }
            catch (Exception ex)
            {
                ack = ex.InnerException.Message;
                return ack;
            }
        }
        #endregion
    }
}
