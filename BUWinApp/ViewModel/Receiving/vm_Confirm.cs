﻿using BUWinApp.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;
using System.Windows;
using BusinessLayer;
using BUWinApp.Helper;
using DomainModel;
using BusinessLayer.Model;
using BusinessLayer.Helper;
using System.Windows.Input;
using BUWinApp.View.Receiving;

namespace BUWinApp.ViewModel.Receiving
{
    public class vm_Confirm : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_Confirm()
        {
            _crop = Convert.ToInt16(DateTime.Now.Year);
            _buyingDate = DateTime.Now.Date;
            _inComplete = true;
            RaisePropertyChangedEvent(nameof(Crop));
            RaisePropertyChangedEvent(nameof(BuyingDate));
            RaisePropertyChangedEvent(nameof(InComplete));
            SupplierBinding();
        }

        #region Properties
        private DateTime _buyingDate;

        public DateTime BuyingDate
        {
            get { return _buyingDate; }
            set
            {
                _buyingDate = value;
                var crop = Convert.ToInt16(_buyingDate.Year);
                if (_crop != crop)
                {
                    _crop = crop;
                    RaisePropertyChangedEvent(nameof(Crop));
                    SupplierBinding();
                }
            }
        }

        private bool _inComplete;

        public bool InComplete
        {
            get { return _inComplete; }
            set
            {
                _inComplete = value;
                if (value)
                {
                    _complete = !value;
                    RaisePropertyChangedEvent(nameof(Complete));
                }
                RaisePropertyChangedEvent(nameof(InComplete));
            }
        }

        private bool _complete;

        public bool Complete
        {
            get { return _complete; }
            set
            {
                _complete = value;
                if (value)
                {
                    _inComplete = !value;
                    RaisePropertyChangedEvent(nameof(InComplete));
                }
                RaisePropertyChangedEvent(nameof(Complete));
            }
        }

        private string _supplierCode;

        public string SupplierCode
        {
            get { return _supplierCode; }
            set { _supplierCode = value; }
        }

        private int _totalItem;

        public int TotalItem
        {
            get { return _totalItem; }
            set { _totalItem = value; }
        }

        private short _crop;

        public short Crop
        {
            get { return _crop; }
            set { _crop = value; }
        }

        private int _receivedBale;

        public int ReceivedBale
        {
            get { return _receivedBale; }
            set { _receivedBale = value; }
        }

        private int _waitForReceiveBale;

        public int WaitForReceiveBale
        {
            get { return _waitForReceiveBale; }
            set { _waitForReceiveBale = value; }
        }

        private int _buyingBale;

        public int BuyingBale
        {
            get { return _buyingBale; }
            set { _buyingBale = value; }
        }
        #endregion


        #region List
        private List<Supplier> _supplierList;

        public List<Supplier> SupplierList
        {
            get { return _supplierList; }
            set { _supplierList = value; }
        }

        private List<m_ReceivingSummary> _documentList;

        public List<m_ReceivingSummary> DocumentList
        {
            get { return _documentList; }
            set { _documentList = value; }
        }
        #endregion


        #region Command
        private ICommand _onRefreshCommand;

        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(Refesh)); }
            set { _onRefreshCommand = value; }
        }

        private void Refesh(object obj)
        {
            BuyingDocumentBinding();
        }

        private ICommand _onConfirmCommand;

        public ICommand OnConfirmCommand
        {
            get { return _onConfirmCommand ?? (_onConfirmCommand = new RelayCommand(Confirm)); }
            set { _onConfirmCommand = value; }
        }

        private void Confirm(object obj)
        {
            try
            {
                // เงื่อนไขในการ confirm document คือ
                // 1.ลานรับซื้อจะต้องปิดการขายแล้ว (เงื่อนไขนี้อยู่ใน store แล้ว column "IsFinish")
                // 2.จำนวนห่อยาที่ซื้อและรับจะต้องเท่ากัน (Diff bale จะต้อง <= 0 )
                // 3.Column "IsAccountFinish" จะต้องเป็น False

                if (MessageBoxHelper.Question("หลังจากนี้จะไม่สามารถยกเลิกการยืนยันข้อมูลได้อีกต่อไป " + Environment.NewLine +
                    "ท่านต้องการ confirm ข้อมูลเหล่านี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                if (_documentList == null)
                    throw new ArgumentException("ไม่พบรายการข้อมูลที่จะยืนยันข้อมูลในตารางด้านล่าง");

                if (_documentList.Count() <= 0)
                    throw new ArgumentException("ไม่พบรายการข้อมูลที่จะยืนยันข้อมูลในตารางด้านล่าง");

                var diffBaleRecord = _documentList.Where(x => x.DiffBale > 0).Count();
                if (diffBaleRecord > 0)
                    if (MessageBoxHelper.Question("มีบางเวาเชอร์ที่ยังรับไม่ครบ จำนวน " +
                        diffBaleRecord.ToString("N0") + " รายการ " + Environment.NewLine +
                        "ระบบจะทำการยืนยันข้อมูลให้เฉพาะใบเวาเชอร์ที่รับครบแล้วเท่านั้น" + Environment.NewLine +
                        "ท่านต้องการยืนยันข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                        return;

                int i = 0;
                foreach (var item in _documentList
                    .Where(x => x.IsAccountFinish == false && x.DiffBale <= 0))
                {
                    BuyingFacade.BuyingDocumentBL()
                        .ReceivingFinish(item.Crop,
                        item.BuyingStationCode,
                        item.FarmerCode,
                        item.BuyingDocumentNumber,
                        user_setting.User.Username);
                    i++;
                }

                MessageBoxHelper.Info("ยืนยันข้อมูลสำเร็จ จำนวนทั้งสิ้น " + i.ToString("N0") + " รายการ" + Environment.NewLine +
                    "จากทั้งหมด " + _documentList.Count().ToString("N0") + " รายการ");

                BuyingDocumentBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onRowDoubleClick;

        public ICommand OnRowDoubleClick
        {
            get { return _onRowDoubleClick ?? (_onRowDoubleClick = new RelayCommand(RowDoubleClick)); }
            set { _onRowDoubleClick = value; }
        }

        private void RowDoubleClick(object obj)
        {
            var item = (m_ReceivingSummary)obj;
            var window = new BuyingDetails();
            var vm = new vm_BuyingDetails();
            window.DataContext = vm;
            vm.Crop = item.Crop;
            vm.StationCode = item.BuyingStationCode;
            vm.FarmerCode = item.FarmerCode;
            vm.DocumentNumber = item.BuyingDocumentNumber;

            //Send document code to custom control for show buying document summary.
            vm.DocumentCode = item.Crop + "-" +
                item.BuyingStationCode + "-" +
                item.FarmerCode + "-" +
                item.BuyingDocumentNumber;
            vm.BuyingDetailsBinding();
            window.ShowDialog();
        }
        #endregion


        #region Function
        private void SupplierBinding()
        {
            _supplierList = BuyingFacade.KilogramsPerRaiConfigurationBL()
                    .GetByCrop(Convert.ToInt16(_buyingDate.Year))
                    .GroupBy(x => x.SupplierCode)
                    .Select(x => new Supplier
                    {
                        SupplierCode = x.Key
                    })
                    .OrderBy(x => x.SupplierCode)
                    .ToList();
            RaisePropertyChangedEvent(nameof(SupplierList));
        }

        private void BuyingDocumentBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_supplierCode))
                    throw new ArgumentException("โปรดระบุตัวแทน");

                _documentList = ReceivingHelper.GetBySupplierCode(_buyingDate, _supplierCode, _complete);
                _totalItem = _documentList.Count();
                _buyingBale = (int)_documentList.Sum(x => x.BuyingBale);
                _receivedBale = (int)_documentList.Sum(x => x.ReceivingBale);
                _waitForReceiveBale = _buyingBale - _receivedBale;
                RaisePropertyChangedEvent(nameof(DocumentList));
                RaisePropertyChangedEvent(nameof(TotalItem));
                RaisePropertyChangedEvent(nameof(BuyingBale));
                RaisePropertyChangedEvent(nameof(ReceivedBale));
                RaisePropertyChangedEvent(nameof(WaitForReceiveBale));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
