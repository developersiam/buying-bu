﻿using BusinessLayer.Helper;
using BusinessLayer.Model;
using BUWinApp.Helper;
using BUWinApp.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUWinApp.ViewModel.Receiving
{
    public class vm_BuyingDetails : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_BuyingDetails()
        {
        }


        #region Properties
        private short _crop;

        public short Crop
        {
            get { return _crop; }
            set { _crop = value; }
        }

        private string _stationCode;

        public string StationCode
        {
            get { return _stationCode; }
            set { _stationCode = value; }
        }

        private string _farmerCode;

        public string FarmerCode
        {
            get { return _farmerCode; }
            set { _farmerCode = value; }
        }

        private short _documentNumber;

        public short DocumentNumber
        {
            get { return _documentNumber; }
            set { _documentNumber = value; }
        }

        private string _documentCode;

        public string DocumentCode
        {
            get { return _documentCode; }
            set { _documentCode = value; }
        }

        #endregion


        #region List
        private List<m_Buying> _buyingList;

        public List<m_Buying> BuyingList
        {
            get { return _buyingList; }
            set { _buyingList = value; }
        }
        #endregion


        #region Function
        public void BuyingDetailsBinding()
        {
            try
            {
                _buyingList = BuyingHelper
                    .GetByBuyingDocument(_crop,
                    _farmerCode,
                    _stationCode,
                    _documentNumber);
                RaisePropertyChangedEvent(nameof(BuyingList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
