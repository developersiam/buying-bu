﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BUWinApp.MVVM;
using System.Windows.Input;
using DomainModel;
using BusinessLayer;
using BusinessLayer.Model;
using BusinessLayer.Helper;
using BUWinApp.Helper;
using System.Windows;
using System.Threading;

namespace BUWinApp.ViewModel.QC
{
    public class vm_MoistureResult : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_MoistureResult()
        {
            _resultDate = DateTime.Now;
            ResultListBinding();
        }


        #region Properties
        private m_Buying _buying;

        public m_Buying Buying
        {
            get { return _buying; }
            set { _buying = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private string _baleBarcode;

        public string BaleBarcode
        {
            get { return _baleBarcode; }
            set
            {
                _baleBarcode = value;
                //BaleBarcodeInfoBinding();
            }
        }

        private DateTime _resultDate;

        public DateTime ResultDate
        {
            get { return _resultDate; }
            set
            {
                _resultDate = value;
                ResultListBinding();
            }
        }

        private decimal _result;

        public decimal Result
        {
            get { return _result; }
            set { _result = value; }
        }
        #endregion


        #region List
        private List<m_Buying> _resultList;

        public List<m_Buying> ResultList
        {
            get { return _resultList; }
            set { _resultList = value; }
        }
        #endregion


        #region Command
        private ICommand _onBaleBarcodeEnterCommnad;

        public ICommand OnBaleBarcodeEnterCommnad
        {
            get { return _onBaleBarcodeEnterCommnad ?? (_onBaleBarcodeEnterCommnad = new RelayCommand(OnBarcodeEnter)); }
            set { _onBaleBarcodeEnterCommnad = value; }
        }

        private void OnBarcodeEnter(object obj)
        {
            BaleBarcodeInfoBinding();
        }

        private ICommand _onSaveCommand;

        public ICommand OnSaveCommand
        {
            get { return _onSaveCommand ?? (_onSaveCommand = new RelayCommand(OnSave)); }
            set { _onSaveCommand = value; }
        }

        private void OnSave(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_baleBarcode))
                {
                    MessageBoxHelper.Warning("โปรดสแกนบาร์โค้ตก่อนบันทึก");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                ///ใช้คู่กับ ResultTextBox binding โดยเซ็ต UpdateSourceTrigger=LostFocus 
                ///บังคับให้เกิด Event LostFocus ที่ ResultTextBox ก่อนเพื่อเปลี่ยนค่าในตัวแปร _result
                ///จึงค่อยบันทึกข้อมูล เนื่องจากถ้าใช้ UpdateSourceTrigger=PropertyChanged 
                ///ตัวแปร _result จะถูก Validate Decimal Format ตลอดเวลา
                ///ทำให้หน้า UI แสดงข้อความ Validation Error ขึ้น ไม่สะดวกต่อผู้ใช้งาน รำคาญ!!
                OnFocusRequested(nameof(ResultDate));

                BuyingFacade.BuyingBL()
                    .InputMoistureResult(_baleBarcode,
                    _result,
                    _resultDate,
                    user_setting.User.Username);

                ResultListBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeleteCommand;

        public ICommand OnDeleteCommand
        {
            get { return _onDeleteCommand ?? (_onDeleteCommand = new RelayCommand(OnDelete)); }
            set { _onDeleteCommand = value; }
        }

        private void OnDelete(object obj)
        {
            try
            {
                var model = (m_Buying)obj;
                if (MessageBoxHelper.Question("ท่านต้องการลบผลตรวจความชื้นของห่อยาหมายเลข " + model.BaleBarcode + " นี้ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                BuyingFacade.BuyingBL().RemoveMoistureResult(model.BaleBarcode);
                ResultListBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onRefreshCommand;

        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(OnRefresh)); }
            set { _onRefreshCommand = value; }
        }

        private void OnRefresh(object obj)
        {
            ResultListBinding();
        }

        private ICommand _onClearFormCommand;

        public ICommand OnClearFormCommand
        {
            get { return _onClearFormCommand ?? (_onClearFormCommand = new RelayCommand(OnClearForm)); }
            set { _onClearFormCommand = value; }
        }

        private void OnClearForm(object obj)
        {
            ClearForm();
        }

        private ICommand _onReportCommand;

        public ICommand OnReportCommand
        {
            get { return _onReportCommand ?? (_onReportCommand = new RelayCommand(OnReport)); }
            set { _onReportCommand = value; }
        }

        private void OnReport(object obj)
        {
            Forms.QC.MoistureResultReport report = new Forms.QC.MoistureResultReport(_resultDate);
            report.ShowDialog();
        }
        #endregion


        #region Function
        private void BaleBarcodeInfoBinding()
        {
            try
            {
                _buying = BuyingHelper.GetByBaleBarcode(_baleBarcode);
                _result = Convert.ToDecimal(_buying.MoistureResult);

                if (_buying == null)
                {
                    MessageBoxHelper.Warning("ไม่พบข้อมูลห่อยานี้ในระบบ");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                if (_buying.Grade == null)
                {
                    MessageBoxHelper.Warning("ไม่พบข้อมูลเกรดซื้อ ไม่สามารถบันทึกผลตรวจได้");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                if (_buying.TransportationDocumentCode == null)
                {
                    MessageBoxHelper.Warning("ไม่พบข้อมูลรหัสใบนำส่ง ไม่สามารถบันทึกผลตรวจได้");
                    OnFocusRequested(nameof(BaleBarcode));
                    return;
                }

                RaisePropertyChangedEvent(nameof(Buying));
                RaisePropertyChangedEvent(nameof(Result));
                OnFocusRequested(nameof(Result));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearForm()
        {
            _baleBarcode = "";
            _result = 0;
            _buying = null;

            RaisePropertyChangedEvent(nameof(BaleBarcode));
            RaisePropertyChangedEvent(nameof(Result));
            RaisePropertyChangedEvent(nameof(Buying));

            OnFocusRequested(nameof(BaleBarcode));
        }

        private void ResultListBinding()
        {
            _resultList = BuyingHelper.GetMoistureResultByResultDate(_resultDate)
                .OrderByDescending(b => b.MoistureResultDate)
                .ToList();

            _totalRecord = _resultList.Count();

            RaisePropertyChangedEvent(nameof(ResultList));
            RaisePropertyChangedEvent(nameof(TotalRecord));
        }
        #endregion
    }
}
