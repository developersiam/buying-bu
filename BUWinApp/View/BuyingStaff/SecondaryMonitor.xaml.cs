﻿using BUWinApp.ViewModel.BuyingStaff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BUWinApp.View.BuyingStaff
{
    /// <summary>
    /// Interaction logic for SecondaryMonitor.xaml
    /// </summary>
    public partial class SecondaryMonitor : Window
    {
        public SecondaryMonitor()
        {
            InitializeComponent();
            DataContext = new vm_SecondaryMonitor();
        }
    }
}
