﻿using BUWinApp.MVVM;
using BUWinApp.ViewModel.BuyingStaff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BUWinApp.View.BuyingStaff
{
    /// <summary>
    /// Interaction logic for PrintReceiptMoney.xaml
    /// </summary>
    public partial class PrintReceiptMoney : Window
    {
        public PrintReceiptMoney()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            NumberOfCopyTextBox.Focus();

            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_PrintReceiptMoney)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.NumberOfCopy):
                    NumberOfCopyTextBox.SelectAll();
                    NumberOfCopyTextBox.Focus();
                    break;
            }
        }
    }
}
