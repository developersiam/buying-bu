﻿using BUWinApp.MVVM;
using BUWinApp.ViewModel.BuyingStaff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BUWinApp.View.BuyingStaff
{
    /// <summary>
    /// Interaction logic for TransportationDocument.xaml
    /// </summary>
    public partial class TransportationDocument : Page
    {
        public TransportationDocument()
        {
            InitializeComponent();
            DataContext = new vm_TransportationDocument();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            CreateDocumentDatePicker.Focus();

            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_TransportationDocument)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.CreateDate):
                    CreateDocumentDatePicker.Focus();
                    break;
                case nameof(vm.TruckNumber):
                    TruckNoTextBox.SelectAll();
                    TruckNoTextBox.Focus();
                    break;
                case nameof(vm.MaxWeight):
                    MaxWeightTextBox.SelectAll();
                    MaxWeightTextBox.Focus();
                    break;
            }
        }
    }
}
