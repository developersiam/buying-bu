﻿using BUWinApp.Helper;
using BUWinApp.MVVM;
using BUWinApp.ViewModel.BuyingStaff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BUWinApp.View.BuyingStaff
{
    /// <summary>
    /// Interaction logic for Buying.xaml
    /// </summary>
    public partial class Buying : Window
    {
        public Buying()
        {
            InitializeComponent();
            DataContext = new vm_Buying();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            BaleBarcodeTextBox.Focus();

            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_Buying)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.BaleBarcode):
                    BaleBarcodeTextBox.SelectAll();
                    BaleBarcodeTextBox.Focus();
                    break;
                case nameof(vm.Grade):
                    BuyingGradeTextBox.SelectAll();
                    BuyingGradeTextBox.Focus();
                    break;
                case nameof(vm.Weight):
                    WeightTextBox.SelectAll();
                    WeightTextBox.Focus();
                    break;
                case nameof(vm.BuyerCode):
                    BuyerComboBox.Focus();
                    break;
                default:
                    break;
            }
        }
    }
}
