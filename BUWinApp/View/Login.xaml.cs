﻿using BUWinApp.Helper;
using BUWinApp.MVVM;
using BUWinApp.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BUWinApp.View
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Page
    {
        public Login()
        {
            InitializeComponent();
            DataContext = new vm_Login();
        }
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            //UsernameTextBox.Focus();
            //UsernameTextBox.Clear();

            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_Login)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.Username):
                    UsernameTextBox.Focus();
                    UsernameTextBox.Clear();
                    break;
                case nameof(vm.Password):
                    PasswordBox.Focus();
                    PasswordBox.Clear();
                    break;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (StationComboBox.SelectedIndex < 0)
                {
                    MessageBoxHelper.Warning("โปรดระบุลานรับซื้อ");
                    StationComboBox.Focus();
                    return;
                }

                user_setting.StactionCode = StationComboBox.SelectedValue.ToString();
                NavigationService.Navigate(new Home());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
