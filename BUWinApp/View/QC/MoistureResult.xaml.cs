﻿using BUWinApp.MVVM;
using BUWinApp.ViewModel.QC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BUWinApp.View.QC
{
    /// <summary>
    /// Interaction logic for MoistureResult.xaml
    /// </summary>
    public partial class MoistureResult : Page
    {
        public MoistureResult()
        {
            InitializeComponent();
            DataContext = new vm_MoistureResult();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            BaleBarcodeTextBox.Focus();
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_MoistureResult)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.BaleBarcode):
                    BaleBarcodeTextBox.SelectAll();
                    BaleBarcodeTextBox.Focus();
                    break;
                case nameof(vm.Result):
                    ResultTextBox.SelectAll();
                    ResultTextBox.Focus();
                    break;
                case nameof(vm.ResultDate):
                    ResultDateTextBox.Focus();
                    break;
            }
        }
    }
}
