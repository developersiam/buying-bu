﻿using BUWinApp.MVVM;
using BUWinApp.ViewModel.Hessian;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BUWinApp.View.Hessian
{
    /// <summary>
    /// Interaction logic for DistibutionByVoucher.xaml
    /// </summary>
    public partial class DistibutionByVoucher : Window
    {
        public DistibutionByVoucher()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            WarehouseComboBox.Focus();
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_DistibutionByVoucher)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.WarehouseID):
                    WarehouseComboBox.Focus();
                    break;
                case nameof(vm.ReceivingCode):
                    WarehouseComboBox.Focus();
                    break;
                case nameof(vm.Quantity):
                    QuantityTextBox.SelectAll();
                    QuantityTextBox.Focus();
                    break;
            }
        }
    }
}
