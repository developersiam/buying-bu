﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUWinApp.Helper
{
    public static class UserAccountHelper
    {
        public static bool IsInRole(string roleName)
        {
            if (user_setting.UserRoles
                .Where(x => x.RoleName == roleName).Count() > 0)
                return true;
            else
                return false;
        }
    }
}
