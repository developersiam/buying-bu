﻿using BusinessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUWinApp.Helper
{
    public struct RejectReasonStruct
    {
        public string RejectReason { get; set; }
    }
    public static class user_setting
    {
        public static List<RejectReasonStruct> RejectReasons
        {
            get
            {

                List<RejectReasonStruct> list = new List<RejectReasonStruct>();
                list.Add(new RejectReasonStruct { RejectReason = "Buyer Reject (ไม่ซื้อ)" });
                list.Add(new RejectReasonStruct { RejectReason = "Farmer Reject (ไม่ขาย)" });
                list.Add(new RejectReasonStruct { RejectReason = "Green (เขียว)" });
                list.Add(new RejectReasonStruct { RejectReason = "Mix Grade (ปน)" });
                list.Add(new RejectReasonStruct { RejectReason = "Moisture (ชื้น)" });
                list.Add(new RejectReasonStruct { RejectReason = "NTRM" });
                list.Add(new RejectReasonStruct { RejectReason = "Nesting (ยัดไส้)" });
                list.Add(new RejectReasonStruct { RejectReason = "X low grade over limit" });
                list.Add(new RejectReasonStruct { RejectReason = "Yellow (เหลือง)" });
                return list;
            }
        }
        public static UserAccount User { get; set; }
        public static List<Role> UserRoles { get; set; }
        public static Crop Crop { get { return BuyingFacade.CropBL().GetDefault(); } }
        public static List<AppPolicyRole> userRoles { get; set; }
        public static bool IsRoleAuthorized(string roleName)
        {
            if (Helper.user_setting.User == null)
                throw new ArgumentException("โปรดทำการล็อคอินเพื่อเข้าใช้งานระบบก่อน");

            if (Helper.user_setting.userRoles
                .Where(x => x.RoleName == roleName)
                .Count() <= 0)
                return false;
            else
                return true;
        }
        public static string StactionCode { get; set; }
    }
}
