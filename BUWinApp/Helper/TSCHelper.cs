﻿using BUWinApp.ViewModel.BuyingStaff;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BUWinApp.Helper
{
    public static class TSCHelper
    {
        public static void PrintSampleCollectionSticker(vm_SampleCollection vm)
        {
            try
            {
                if (string.IsNullOrEmpty(vm.CitizenID))
                    throw new ArgumentException("Citizen ID cannot be empty.");

                if (string.IsNullOrEmpty(vm.GAPGroupCode))
                    throw new ArgumentException("GAP Group Code cannot be empty.");

                var sampleType = vm.IsCPA == true ? "CPA" : "Nicotine";
                var address = vm.Person.HouseNumber + " หมู่" +
                    vm.Person.Village + " ต." + 
                    vm.Person.Tumbon + " อ." + 
                    vm.Person.Amphur + " จ." + 
                    vm.Person.Province;

                TSCPrinter.openport("TSC TTP-247");
                TSCPrinter.setup("70", "35", "2.0", "6", "0", "0", "0");
                TSCPrinter.sendcommand("GAP  2 mm,0");
                TSCPrinter.sendcommand("DIRECTION 1");
                TSCPrinter.clearbuffer();

                if(vm.IsCPA == true)
                    TSCPrinter.windowsfont(400, 50, 70, 0, 0, 0, "arial", sampleType);
                else
                    TSCPrinter.windowsfont(320, 50, 70, 0, 0, 0, "arial", sampleType);

                TSCPrinter.windowsfont(470, 130, 70, 0, 0, 0, "arial", vm.Position);
                TSCPrinter.windowsfont(15, 20, 30, 0, 0, 0, "arial", vm.GAPGroupCode);
                TSCPrinter.barcode("15", "80", "128", "50", "0", "0", "2", "2", vm.CitizenID);
                TSCPrinter.windowsfont(15, 150, 60, 0, 0, 0, "cordia new", vm.Person.FirstName);
                TSCPrinter.windowsfont(15, 205, 50, 0, 0, 0, "cordia new", address);

                TSCPrinter.printlabel("1", "1");
                TSCPrinter.closeport();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
