﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.Model;
using BusinessLayer.Helper;
using BusinessLayer;
using DomainModel;
using BUWinApp.Model.BuyingVoucher;
using Microsoft.Reporting.WinForms;
using System.IO;
using System.Drawing.Printing;
using System.Diagnostics;
using System.Windows.Controls;
using IronPdf;
using System.Reflection.Emit;

namespace BUWinApp.Helper
{
    public static class ReportHelper
    {
        public static void Print(m_DocumentInfo doc, m_FarmerInfo farmerInfo, m_DebtorInfo debtorInfo,
            List<m_FarmerQuota> farmerQuotaList, List<m_CreditProject> creditProjectList,
            List<Buying> buyingList, List<Buying> accuBuyingList, string reportEmbeddedResource)
        {
            try
            {
                var accuPrice = accuBuyingList
                    .Where(x => x.Grade != null)
                    .Sum(x => x.Weight * x.BuyingGrade.UnitPrice);
                var accuWeight = accuBuyingList
                    .Sum(x => x.Weight);

                doc.TotalBale = buyingList.Count();
                doc.TotalReject = buyingList.Where(x => x.RejectReason != null).Count();
                doc.TotalWeight = (decimal)buyingList.Sum(x => x.Weight);
                doc.TotalPrice = (decimal)buyingList.Where(x => x.Grade != null)
                    .Sum(x => x.Weight * x.BuyingGrade.UnitPrice);
                doc.NetPrice = doc.TotalPrice - debtorInfo.CurrentPayment;
                doc.AvgPrice = doc.TotalPrice / doc.TotalWeight;
                doc.AccumulativeAvgPrice = (decimal)accuPrice / (decimal)accuWeight;

                var buyingDetailList = buyingList
                     .Select(x => new m_BuyingDetail
                     {
                         BaleBarcode = x.BaleBarcode,
                         WeightDate = x.WeightDate,
                         ProjectType = x.ProjectType,
                         RejectReason = x.RejectReason,
                         Grade = x.Grade,
                         Quality = x.Grade != null ? x.BuyingGrade.Quality : "",
                         UnitPrice = x.Grade != null ? x.BuyingGrade.UnitPrice : (decimal)0.0,
                         Weight = x.Weight,
                         Price = (decimal)(x.Weight == null ? 0 : x.Weight) * (x.Grade != null ? x.BuyingGrade.UnitPrice : (decimal)0.0)
                     }).ToList();

                List<m_FarmerInfo> farmerInfoList = new List<m_FarmerInfo>();
                List<m_DocumentInfo> docList = new List<m_DocumentInfo>();
                List<m_DebtorInfo> debtInfoList = new List<m_DebtorInfo>();

                docList.Add(doc);
                debtInfoList.Add(debtorInfo);
                farmerInfoList.Add(farmerInfo);

                var m_FarmerInfo = new ReportDataSource();
                var m_FarmerQuota = new ReportDataSource();
                var m_DocumentInfo = new ReportDataSource();
                var m_DebtorInfo = new ReportDataSource();
                var m_BuyingDetail = new ReportDataSource();
                var m_CreditProject = new ReportDataSource();

                m_FarmerInfo.Name = "m_FarmerInfo";
                m_FarmerQuota.Name = "m_FarmerQuota";
                m_DocumentInfo.Name = "m_DocumentInfo";
                m_DebtorInfo.Name = "m_DebtorInfo";
                m_BuyingDetail.Name = "m_BuyingDetail";
                m_CreditProject.Name = "m_CreditProject";

                m_FarmerInfo.Value = farmerInfoList;
                m_FarmerQuota.Value = farmerQuotaList;
                m_DocumentInfo.Value = docList;
                m_DebtorInfo.Value = debtInfoList;
                m_BuyingDetail.Value = buyingDetailList;
                m_CreditProject.Value = creditProjectList;

                ReportViewer r = new ReportViewer();
                r.Reset();
                r.LocalReport.DataSources.Add(m_FarmerInfo);
                r.LocalReport.DataSources.Add(m_FarmerQuota);
                r.LocalReport.DataSources.Add(m_DocumentInfo);
                r.LocalReport.DataSources.Add(m_DebtorInfo);
                r.LocalReport.DataSources.Add(m_BuyingDetail);
                r.LocalReport.DataSources.Add(m_CreditProject);
                r.LocalReport.ReportEmbeddedResource = reportEmbeddedResource;
                r.RefreshReport();
                r.LocalReport.PrintToPrinter();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Print2023(m_DocumentInfo doc, m_FarmerInfo farmerInfo, m_DebtorInfo debtorInfo,
            List<m_FarmerQuota> farmerQuotaList, List<m_CreditProject> creditProjectList,
            List<Buying> buyingList, List<Buying> accuBuyingList, string reportEmbeddedResource)
        {
            try
            {
                var accuPrice = accuBuyingList
                    .Where(x => x.Grade != null)
                    .Sum(x => x.Weight * x.BuyingGrade.UnitPrice);
                var accuWeight = accuBuyingList
                    .Sum(x => x.Weight);

                doc.TotalBale = buyingList.Count();
                doc.TotalReject = buyingList.Where(x => x.RejectReason != null).Count();
                doc.TotalWeight = (decimal)buyingList.Sum(x => x.Weight);
                doc.TotalPrice = (decimal)buyingList.Where(x => x.Grade != null)
                    .Sum(x => x.Weight * x.BuyingGrade.UnitPrice);
                doc.NetPrice = doc.TotalPrice - debtorInfo.CurrentPayment;
                doc.AvgPrice = doc.TotalPrice / doc.TotalWeight;
                doc.AccumulativeAvgPrice = (decimal)accuPrice / (decimal)accuWeight;

                var buyingDetailList = buyingList
                     .Select(x => new m_BuyingDetail
                     {
                         BaleBarcode = x.BaleBarcode,
                         WeightDate = x.WeightDate,
                         ProjectType = x.ProjectType,
                         RejectReason = x.RejectReason,
                         Grade = x.Grade,
                         Quality = x.Grade != null ? x.BuyingGrade.Quality : "",
                         UnitPrice = x.Grade != null ? x.BuyingGrade.UnitPrice : (decimal)0.0,
                         Weight = x.Weight,
                         Price = (decimal)(x.Weight == null ? 0 : x.Weight) * (x.Grade != null ? x.BuyingGrade.UnitPrice : (decimal)0.0)
                     }).ToList();

                List<m_FarmerInfo> farmerInfoList = new List<m_FarmerInfo>();
                List<m_DocumentInfo> docList = new List<m_DocumentInfo>();
                List<m_DebtorInfo> debtInfoList = new List<m_DebtorInfo>();

                docList.Add(doc);
                debtInfoList.Add(debtorInfo);
                farmerInfoList.Add(farmerInfo);

                var m_FarmerInfo = new ReportDataSource();
                var m_FarmerQuota = new ReportDataSource();
                var m_DocumentInfo = new ReportDataSource();
                var m_DebtorInfo = new ReportDataSource();
                var m_BuyingDetail = new ReportDataSource();
                var m_CreditProject = new ReportDataSource();

                m_FarmerInfo.Name = "m_FarmerInfo";
                m_FarmerQuota.Name = "m_FarmerQuota";
                m_DocumentInfo.Name = "m_DocumentInfo";
                m_DebtorInfo.Name = "m_DebtorInfo";
                m_BuyingDetail.Name = "m_BuyingDetail";
                m_CreditProject.Name = "m_CreditProject";

                m_FarmerInfo.Value = farmerInfoList;
                m_FarmerQuota.Value = farmerQuotaList;
                m_DocumentInfo.Value = docList;
                m_DebtorInfo.Value = debtInfoList;
                m_BuyingDetail.Value = buyingDetailList;
                m_CreditProject.Value = creditProjectList;

                ReportViewer r = new ReportViewer();
                r.Reset();
                r.LocalReport.DataSources.Add(m_FarmerInfo);
                r.LocalReport.DataSources.Add(m_FarmerQuota);
                r.LocalReport.DataSources.Add(m_DocumentInfo);
                r.LocalReport.DataSources.Add(m_DebtorInfo);
                r.LocalReport.DataSources.Add(m_BuyingDetail);
                r.LocalReport.DataSources.Add(m_CreditProject);
                r.LocalReport.ReportEmbeddedResource = reportEmbeddedResource;
                r.RefreshReport();
                r.LocalReport.PrintToPrinter();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string Export(LocalReport report, string filePath)
        {
            string ack = "";
            try
            {
                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string extension;

                byte[] bytes = report.Render("PDF", null, out mimeType, out encoding, out extension, out streamids, out warnings);
                using (FileStream stream = System.IO.File.OpenWrite(filePath))
                {
                    stream.Write(bytes, 0, bytes.Length);
                }
                return ack;
            }
            catch (Exception ex)
            {
                ack = ex.InnerException.Message;
                return ack;
            }
        }

        public static void PrintBuyingVoucher2023(BuyingDocument document,
            List<m_DebtAndReceiptSummary> cropDebtSummaryList,
            List<DebtReceipt> receiptInVoucherList,
            short copies)
        {
            try
            {
                //Farmer info. *************************************************************************************************************
                var regisInfo = BuyingFacade.RegistrationBL().GetSingle(document.Crop, document.FarmerCode);
                if (regisInfo == null)
                    throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รายนี้");

                if (string.IsNullOrEmpty(regisInfo.BookBank))
                    throw new ArgumentException("ไม่พบเลขที่บัญชีของชาวไร่รายนี้ โปรดบันทึกเลขที่บัญชีในระบบ BU on web");

                var person = regisInfo.Farmer.Person;
                var citizenID = "";
                for (int i = 0; i < person.CitizenID.Length; i++)
                {
                    if (i >= 4 && i <= 7)
                        citizenID = citizenID + "x";
                    else
                        citizenID = citizenID + person.CitizenID[i];
                }
                var bankAccount = "";
                for (int i = 0; i < regisInfo.BookBank.Length; i++)
                {
                    if (i >= 4 && i <= 7)
                        bankAccount = bankAccount + "x";
                    else
                        bankAccount = bankAccount + regisInfo.BookBank[i];
                }
                m_FarmerInfo farmerInfo = new m_FarmerInfo();
                farmerInfo.FarmerCode = regisInfo.FarmerCode;
                farmerInfo.CitizenID = citizenID;
                farmerInfo.FarmerName = person.Prefix + person.FirstName + " " + person.LastName;
                farmerInfo.Address = person.HouseNumber +
                    " หมู่ " + person.Village +
                    " ต." + person.Tumbon +
                    " อ." + person.Amphur +
                    " จ." + person.Province;
                farmerInfo.GAPGroupCode = regisInfo.GAPGroupCode;
                farmerInfo.RegisterStatus = regisInfo.ActiveStatus == true && regisInfo.QuotaFromSignContract != null ? "ปกติ" : "รอตรวจสอบ";
                farmerInfo.BankCode = regisInfo.BookBank;
                farmerInfo.BankBranch = regisInfo.BankBranch;


                // Farmer quota **************************************************************************************************************
                // For CY 2023 5.5฿ Bonus
                // Show quota in 500 kg/rai (400 in 5.5฿ bonus and 100 in 1.5฿ bonus)


                // Show in a bottom tabale of the buying voucher. 
                List<m_FarmerProject> farmerProjectList = FarmerProjectHelper
                    .GetQuotaAndSoldByFarmerVersion2(document.Crop, document.FarmerCode);

                // Show in a top right table of a buying voucher.
                List<m_FarmerQuota> farmerQuotaList = new List<m_FarmerQuota>();
                farmerQuotaList = farmerProjectList
                    .Select(x => new m_FarmerQuota
                    {
                        ProjectType = x.ProjectType,
                        Quota = x.ActualKgPerRai,
                        Extra = x.ExtraQuota,
                        Sold = (decimal)x.Sold + (decimal)x.SoldInExtra
                    })
                    .ToList();


                //Debt info ******************************************************************************************************************
                m_DebtorInfo debtInfo = new m_DebtorInfo();
                debtInfo.TotalDebt = cropDebtSummaryList.Sum(x => x.Amount);
                debtInfo.TotalPayment = cropDebtSummaryList.Sum(x => x.ReceiptAmount);
                debtInfo.DebtBalance = cropDebtSummaryList.Sum(x => x.BalanceAmount);
                debtInfo.CurrentPayment = receiptInVoucherList.Sum(x => x.Amount);


                //Debt Details List ***********************************************************************************************************
                // ใช้ m_DebtReceipt ในการออกรายงานเนื่องจากจะได้ไม่ต้องสร้าง class ใหม่เพื่อใช้กับ report เพียงงานเดียว
                // ModifiedBy represent debt type name.
                // RefCode represent debt type code.
                List<m_DebtReceipt> receiptDetailsList = new List<m_DebtReceipt>();
                receiptDetailsList = receiptInVoucherList
                    .Select(x => new m_DebtReceipt
                    {
                        DebtSetupCode = x.DebtSetupCode,
                        ReceiptCode = x.ReceiptCode,
                        RefCode = x.DebtSetup.DebtTypeCode,
                        ModifiedBy = x.DebtSetup.DebtType.DebtTypeName,
                        Amount = x.Amount,
                    })
                    .ToList();


                //Credit project for AMC Sukhothai info ***************************************************************************************
                //ปี 2023 นี้ในรายงานใช้ model ของ credit project ไปก่อน เนื่องจากมิทัน ปีหน้าว่ากันใหม่
                List<m_CreditProject> creditProjectList = BuyingFacade.AMCSUKProjectBL()
                    .GetByFarmer(document.Crop, person.CitizenID)
                    .Select(x => new m_CreditProject
                    {
                        CreditProjectName = x.ProjectName
                    })
                    .ToList();


                //H and L price info ***********************************************************************************************************
                var cropByingList = BuyingFacade.BuyingBL()
                    .GetByFarmer(document.Crop, document.FarmerCode);
                var voucherBuyingList = cropByingList
                    .Where(x => x.Crop == document.Crop &&
                    x.FarmerCode == document.FarmerCode &&
                    x.BuyingStationCode == document.BuyingStationCode &&
                    x.BuyingDocumentNumber == document.BuyingDocumentNumber)
                    .ToList();

                string[] lowGrade = {
                "XL5","X5Y","X5V","XK","XKY",
                    "CL5","C5Y","C5V","CKY","CK",
                    "B5V","B5Y","BKY","BK",
                    "TK","ND","T4","T5",
                "XL5A","X5YA","X5VA","XKA","XKYA",
                    "CL5A","C5YA","C5VA","CKYA","CKA",
                    "B5VA","B5YA","BKYA","BK",
                    "TKA","NDA","T4A","T5A"
                };

                var notRejectAllList = cropByingList.Where(x => x.Grade != null);
                var hGradeAll = notRejectAllList
                    .Where(x => x.Grade.Any(y => !lowGrade.Contains(x.Grade)))
                    .ToList();
                var lGradeAll = notRejectAllList
                    .Where(x => x.Grade.Any(y => lowGrade.Contains(x.Grade)))
                    .ToList();


                //Document info ***************************************************************************************************************
                m_DocumentInfo doc = new m_DocumentInfo();
                doc.DocumentCode = document.Crop +
                    "-" + document.BuyingStationCode +
                    "-" + document.FarmerCode +
                    "-" + document.BuyingDocumentNumber;

                switch (regisInfo.Farmer.Supplier)
                {
                    case "ABC/S":
                        doc.DocumentOwner = "สหกรณ์การเกษตรเพื่อการตลาดลูกค้าธ.ก.ส. สุโขทัย จำกัด";
                        break;
                    case "ABC/P":
                        doc.DocumentOwner = "สหกรณ์การเกษตรเพื่อการตลาดลูกค้าธ.ก.ส. เพชรบูรณ์ จำกัด";
                        break;
                    default:
                        doc.DocumentOwner = "SIAM TOBACCO EXPORT CORP.,LTD.";
                        break;
                }

                doc.PrintDate = DateTime.Now;
                doc.Station = document.BuyingStationCode;
                doc.CreateDate = document.CreateDate;
                doc.FinishBuyingDate = (DateTime)document.FinishDate;
                doc.AccumulativeHighGradeAvgPrice = hGradeAll.Count() < 1 ? 0 : (decimal)(hGradeAll.Sum(x => x.Weight * x.BuyingGrade.UnitPrice) / hGradeAll.Sum(x => x.Weight));
                doc.AccumulativeLowGradeAvgPrice = lGradeAll.Count() < 1 ? 0 : (decimal)(lGradeAll.Sum(x => x.Weight * x.BuyingGrade.UnitPrice) / lGradeAll.Sum(x => x.Weight));

                var accuPrice = cropByingList
                    .Where(x => x.Grade != null)
                    .Sum(x => x.Weight * x.BuyingGrade.UnitPrice);
                var accuWeight = cropByingList
                    .Sum(x => x.Weight);

                doc.AccumulativeAvgPrice = (decimal)accuPrice / (decimal)accuWeight;
                doc.TotalBale = voucherBuyingList.Count();
                doc.TotalReject = voucherBuyingList.Where(x => x.RejectReason != null).Count();
                doc.TotalWeight = (decimal)voucherBuyingList.Sum(x => x.Weight);
                doc.TotalPrice = (decimal)voucherBuyingList.Where(x => x.Grade != null)
                    .Sum(x => x.Weight * x.BuyingGrade.UnitPrice);
                doc.NetPrice = doc.TotalPrice - debtInfo.CurrentPayment;
                doc.AvgPrice = doc.TotalPrice / doc.TotalWeight;

                var buyingDetailList = voucherBuyingList
                     .Select(x => new m_BuyingDetail
                     {
                         BaleBarcode = x.BaleBarcode,
                         WeightDate = x.WeightDate,
                         ProjectType = x.ProjectType,
                         RejectReason = x.RejectReason,
                         Grade = x.Grade,
                         Quality = x.Grade != null ? x.BuyingGrade.Quality : "",
                         UnitPrice = x.Grade != null ? x.BuyingGrade.UnitPrice : (decimal)0.0,
                         Weight = x.Weight,
                         Price = (decimal)(x.Weight == null ? 0 : x.Weight) * (x.Grade != null ? x.BuyingGrade.UnitPrice : (decimal)0.0)
                     }).ToList();


                // Setup local report ******************************************************************************************************
                List<m_FarmerInfo> farmerInfoList = new List<m_FarmerInfo>();
                List<m_DocumentInfo> docList = new List<m_DocumentInfo>();
                List<m_DebtorInfo> debtInfoList = new List<m_DebtorInfo>();

                docList.Add(doc);
                debtInfoList.Add(debtInfo);
                farmerInfoList.Add(farmerInfo);

                var m_FarmerInfo = new ReportDataSource();
                var m_FarmerQuota = new ReportDataSource();
                var m_DocumentInfo = new ReportDataSource();
                var m_DebtorInfo = new ReportDataSource();
                var m_BuyingDetail = new ReportDataSource();
                var m_CreditProject = new ReportDataSource();
                var m_DebtReceipt = new ReportDataSource();
                var m_FarmerProject = new ReportDataSource();

                m_FarmerInfo.Name = "m_FarmerInfo";
                m_FarmerQuota.Name = "m_FarmerQuota";
                m_DocumentInfo.Name = "m_DocumentInfo";
                m_DebtorInfo.Name = "m_DebtorInfo";
                m_BuyingDetail.Name = "m_BuyingDetail";
                m_CreditProject.Name = "m_CreditProject";
                m_DebtReceipt.Name = "m_DebtReceipt";
                m_FarmerProject.Name = "m_FarmerProject";

                m_FarmerInfo.Value = farmerInfoList;
                m_FarmerQuota.Value = farmerQuotaList;
                m_DocumentInfo.Value = docList;
                m_DebtorInfo.Value = debtInfoList;
                m_BuyingDetail.Value = buyingDetailList;
                m_CreditProject.Value = creditProjectList;
                m_DebtReceipt.Value = receiptDetailsList;
                m_FarmerProject.Value = farmerProjectList;

                LocalReport report = new LocalReport();
                var reportPath = "BUWinApp.RDLCReport.RPTBUY00122v1.rdlc";

                report.DataSources.Add(m_FarmerInfo);
                report.DataSources.Add(m_FarmerQuota);
                report.DataSources.Add(m_DocumentInfo);
                report.DataSources.Add(m_DebtorInfo);
                report.DataSources.Add(m_BuyingDetail);
                report.DataSources.Add(m_CreditProject);
                report.DataSources.Add(m_DebtReceipt);
                report.DataSources.Add(m_FarmerProject);
                report.ReportEmbeddedResource = reportPath;

                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string filenameExtension;

                byte[] bytes = report.Render(
                    "PDF", null, out mimeType, out encoding, out filenameExtension,
                    out streamids, out warnings);

                var tempPath = Path.GetTempPath();
                var fileName = "output.pdf";
                var result = tempPath + fileName;
                using (FileStream fs = new FileStream(result, FileMode.Create))
                {
                    fs.Write(bytes, 0, bytes.Length);
                }

                PrinterSettings settings = new PrinterSettings()
                {
                    //PrinterName = "Microsoft Print to PDF",
                    Copies = copies
                };
                PdfDocument pdf = PdfDocument.FromFile(result);
                PrintDocument pdfDoc = pdf.GetPrintDocument(settings);
                pdfDoc.Print();
                report.Dispose();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void PrintReceiptOfMoney(m_BuyingDocument doc, Person person, short copies)
        {
            ExcelHelper.Open(@"C:\BUBuying\Template\ReceiptOfMoney.xlsx");

            /// Copy 1
            /// 
            var date = Convert.ToDateTime(doc.FinishDate);
            ExcelHelper.PutDataToCell("F2", date.Day.ToString());
            ExcelHelper.PutDataToCell("H3", date.Month.ToString());
            ExcelHelper.PutDataToCell("J2", (date.Year + 543).ToString());

            ExcelHelper.PutDataToCell("B4", person.Prefix + person.FirstName + " " + person.LastName);
            ExcelHelper.PutDataToCell("B5", person.CitizenID);
            ExcelHelper.PutDataToCell("B6", person.HouseNumber);
            ExcelHelper.PutDataToCell("D6", person.Village);
            ExcelHelper.PutDataToCell("F6", person.Tumbon);
            ExcelHelper.PutDataToCell("B7", person.Amphur);
            ExcelHelper.PutDataToCell("D7", person.Province);
            ExcelHelper.PutDataToCell("D11", Convert.ToInt16(doc.TotalLoadTruck).ToString());
            ExcelHelper.PutDataToCell("F11", Convert.ToDecimal(doc.TotalSold).ToString());
            ExcelHelper.PutDataToCell("B13", Convert.ToDecimal(doc.TotalPrice).ToString());

            /// Copy 2
            /// 
            ExcelHelper.PutDataToCell("F21", date.Day.ToString());
            ExcelHelper.PutDataToCell("H22", date.Month.ToString());
            ExcelHelper.PutDataToCell("J21", (date.Year + 543).ToString());

            ExcelHelper.PutDataToCell("B23", person.Prefix + person.FirstName + " " + person.LastName);
            ExcelHelper.PutDataToCell("B24", person.CitizenID);
            ExcelHelper.PutDataToCell("B25", person.HouseNumber);
            ExcelHelper.PutDataToCell("D25", person.Village);
            ExcelHelper.PutDataToCell("F25", person.Tumbon);
            ExcelHelper.PutDataToCell("B26", person.Amphur);
            ExcelHelper.PutDataToCell("D26", person.Province);
            ExcelHelper.PutDataToCell("D30", Convert.ToInt16(doc.TotalLoadTruck).ToString());
            ExcelHelper.PutDataToCell("F30", Convert.ToDecimal(doc.TotalSold).ToString());
            ExcelHelper.PutDataToCell("B32", Convert.ToDecimal(doc.TotalPrice).ToString());

            ExcelHelper.Print(copies);
        }

        //public static void PrintBuyingVoucherVersion2(BuyingDocument document, short copies)
        //{
        //    try
        //    {
        //        var regisInfo = BuyingFacade.RegistrationBL().GetSingle(document.Crop, document.FarmerCode);
        //        if (regisInfo == null)
        //            throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รายนี้");

        //        var person = regisInfo.Farmer.Person;
        //        m_FarmerInfo farmerInfo = new m_FarmerInfo();
        //        farmerInfo.FarmerCode = regisInfo.FarmerCode;
        //        farmerInfo.CitizenID = regisInfo.Farmer.CitizenID;
        //        farmerInfo.FarmerName = person.Prefix + person.FirstName + " " + person.LastName;
        //        farmerInfo.Address = person.HouseNumber +
        //            " หมู่ " + person.Village +
        //            " ต." + person.Tumbon +
        //            " อ." + person.Amphur +
        //            " จ." + person.Province;
        //        farmerInfo.GAPGroupCode = regisInfo.GAPGroupCode;
        //        farmerInfo.RegisterStatus = regisInfo.ActiveStatus == true && regisInfo.QuotaFromSignContract != null ? "ปกติ" : "รอตรวจสอบ";
        //        farmerInfo.BankCode = regisInfo.BookBank;
        //        farmerInfo.BankBranch = regisInfo.BankBranch;

        //        List<m_FarmerQuota> farmerQuotaList = new List<m_FarmerQuota>();
        //        farmerQuotaList = FarmerProjectHelper
        //            .GetQuotaAndSoldByFarmerVersion2(document.Crop, document.FarmerCode)
        //            .Select(x => new m_FarmerQuota
        //            {
        //                ProjectType = x.ProjectType,
        //                Quota = x.ActualKgPerRai,
        //                Extra = x.ExtraQuota,
        //                Sold = (decimal)x.Sold + (decimal)x.SoldInExtra
        //            }).ToList();

        //        var creditNote = BuyingFacade.AccountCreditNoteDetailBL()
        //            .GetByFarmerCode(document.Crop, document.FarmerCode)
        //            .Sum(x => x.UnitPrice * x.Quantity);

        //        m_DebtorInfo debtInfo = new m_DebtorInfo();
        //        debtInfo.TotalDebt = DebtorsHelper.GetTotalDebt(document.Crop, document.FarmerCode) - creditNote;
        //        debtInfo.TotalPayment = DebtorsHelper.GetTotalPayment(document.Crop, document.FarmerCode);
        //        debtInfo.DebtBalance = debtInfo.TotalDebt - debtInfo.TotalPayment;
        //        debtInfo.CreditNoteStatus = creditNote > 0 ? "*มีการลดหนี้" : "";
        //        debtInfo.CurrentPayment = BuyingFacade.PaymentHistoryBL()
        //            .GetSingle(document.Crop,
        //            document.BuyingStationCode,
        //            document.FarmerCode,
        //            document.BuyingDocumentNumber)
        //            .PaymentAmount;

        //        List<m_CreditProject> creditProjectList = BuyingFacade.FarmerCreditProjectBL()
        //            .GetByFarmer(document.Crop, document.FarmerCode)
        //            .Select(x => new m_CreditProject { CreditProjectName = x.CreditProject.CreditProjectName })
        //            .ToList();

        //        m_DocumentInfo doc = new m_DocumentInfo();
        //        doc.DocumentCode = document.Crop +
        //            "-" + document.BuyingStationCode +
        //            "-" + document.FarmerCode +
        //            "-" + document.BuyingDocumentNumber;
        //        doc.DocumentOwner = regisInfo.Farmer.Supplier == "ABC/S" ?
        //            "สหกรณ์การเกษตรเพื่อการตลาดลูกค้าธ.ก.ส. สุโขทัย จำกัด" :
        //            "SIAM TOBACCO EXPORT CORP.,LTD.";
        //        doc.PrintDate = DateTime.Now;
        //        doc.Station = document.BuyingStationCode;
        //        doc.CreateDate = document.CreateDate;
        //        doc.FinishBuyingDate = (DateTime)document.FinishDate;

        //        var allBuyingList = BuyingFacade.BuyingBL()
        //            .GetByFarmer(document.Crop, document.FarmerCode);
        //        var buyingList = allBuyingList
        //            .Where(x => x.Crop == document.Crop &&
        //            x.FarmerCode == document.FarmerCode &&
        //            x.BuyingStationCode == document.BuyingStationCode &&
        //            x.BuyingDocumentNumber == document.BuyingDocumentNumber)
        //            .ToList();

        //        string[] lowGrade = {
        //        "XL5","X5Y","X5V","XK","XKY",
        //            "CL5","C5Y","C5V","CKY","CK",
        //            "B5V","B5Y","BKY","BK",
        //            "TK","ND","T4","T5"
        //        };

        //        var notRejectList = buyingList.Where(x => x.Grade != null);
        //        var hGrade = notRejectList
        //            .Where(x => x.Grade.Any(y => !lowGrade.Contains(x.Grade)))
        //            .ToList();
        //        var lGrade = notRejectList
        //            .Where(x => x.Grade.Any(y => lowGrade.Contains(x.Grade)))
        //            .ToList();

        //        var notRejectAllList = allBuyingList.Where(x => x.Grade != null);
        //        var hGradeAll = notRejectAllList
        //            .Where(x => x.Grade.Any(y => !lowGrade.Contains(x.Grade)))
        //            .ToList();
        //        var lGradeAll = notRejectAllList
        //            .Where(x => x.Grade.Any(y => lowGrade.Contains(x.Grade)))
        //            .ToList();
        //        var rejectList = buyingList.Where(x => x.RejectReason != null).ToList();

        //        for (int i = 1; i <= copies; i++)
        //        {
        //            if (hGrade.Count() > 0)
        //            {
        //                if (rejectList.Count() > 0)
        //                {
        //                    hGrade.AddRange(rejectList);
        //                    rejectList = new List<Buying>();
        //                }
        //                Print(doc, farmerInfo, debtInfo, farmerQuotaList,
        //                    creditProjectList, hGrade, hGradeAll, "BUWinApp.RDLCReport.RPTBUY00122v2.rdlc");
        //            }

        //            if (lGrade.Count() > 0)
        //            {
        //                if (rejectList.Count() > 0)
        //                {
        //                    lGrade.AddRange(rejectList);
        //                    rejectList = new List<Buying>();
        //                }
        //                Print(doc, farmerInfo, debtInfo, farmerQuotaList,
        //                    creditProjectList, lGrade, lGradeAll, "BUWinApp.RDLCReport.RPTBUY00122v2.rdlc");
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}


        //public static void PrintBuyingVoucherVersion1(BuyingDocument document, short copies)
        //{
        //    try
        //    {
        //        var regisInfo = BuyingFacade.RegistrationBL().GetSingle(document.Crop, document.FarmerCode);
        //        if (regisInfo == null)
        //            throw new ArgumentException("ไม่พบข้อมูลการลงทะเบียนของชาวไร่รายนี้");

        //        var person = regisInfo.Farmer.Person;
        //        m_FarmerInfo farmerInfo = new m_FarmerInfo();
        //        farmerInfo.FarmerCode = regisInfo.FarmerCode;
        //        farmerInfo.CitizenID = regisInfo.Farmer.CitizenID;
        //        farmerInfo.FarmerName = person.Prefix + person.FirstName + " " + person.LastName;
        //        farmerInfo.Address = person.HouseNumber +
        //            " หมู่ " + person.Village +
        //            " ต." + person.Tumbon +
        //            " อ." + person.Amphur +
        //            " จ." + person.Province;
        //        farmerInfo.GAPGroupCode = regisInfo.GAPGroupCode;
        //        farmerInfo.RegisterStatus = regisInfo.ActiveStatus == true && regisInfo.QuotaFromSignContract != null ? "ปกติ" : "รอตรวจสอบ";
        //        farmerInfo.BankCode = regisInfo.BookBank;
        //        farmerInfo.BankBranch = regisInfo.BankBranch;

        //        List<m_FarmerQuota> farmerQuotaList = new List<m_FarmerQuota>();
        //        farmerQuotaList = FarmerProjectHelper
        //            .GetQuotaAndSoldByFarmerVersion2(document.Crop, document.FarmerCode)
        //            .Select(x => new m_FarmerQuota
        //            {
        //                ProjectType = x.ProjectType,
        //                Quota = x.ActualKgPerRai,
        //                Extra = x.ExtraQuota,
        //                Sold = (decimal)x.Sold + (decimal)x.SoldInExtra
        //            }).ToList();

        //        var creditNote = BuyingFacade.AccountCreditNoteDetailBL()
        //            .GetByFarmerCode(document.Crop, document.FarmerCode)
        //            .Sum(x => x.UnitPrice * x.Quantity);

        //        m_DebtorInfo debtInfo = new m_DebtorInfo();
        //        debtInfo.TotalDebt = DebtorsHelper.GetTotalDebt(document.Crop, document.FarmerCode) - creditNote;
        //        debtInfo.TotalPayment = DebtorsHelper.GetTotalPayment(document.Crop, document.FarmerCode);
        //        debtInfo.DebtBalance = debtInfo.TotalDebt - debtInfo.TotalPayment;
        //        debtInfo.CreditNoteStatus = creditNote > 0 ? "*มีการลดหนี้" : "";
        //        var currentPayment = BuyingFacade.PaymentHistoryBL()
        //            .GetSingle(document.Crop,
        //            document.BuyingStationCode,
        //            document.FarmerCode,
        //            document.BuyingDocumentNumber);
        //        debtInfo.CurrentPayment = currentPayment == null ? 0 : currentPayment.PaymentAmount;

        //        List<m_CreditProject> creditProjectList = BuyingFacade.FarmerCreditProjectBL()
        //            .GetByFarmer(document.Crop, document.FarmerCode)
        //            .Select(x => new m_CreditProject { CreditProjectName = x.CreditProject.CreditProjectName })
        //            .ToList();

        //        m_DocumentInfo doc = new m_DocumentInfo();
        //        doc.DocumentCode = document.Crop +
        //            "-" + document.BuyingStationCode +
        //            "-" + document.FarmerCode +
        //            "-" + document.BuyingDocumentNumber;
        //        doc.DocumentOwner = regisInfo.Farmer.Supplier == "ABC/S" ?
        //            "สหกรณ์การเกษตรเพื่อการตลาดลูกค้าธ.ก.ส. สุโขทัย จำกัด" :
        //            "SIAM TOBACCO EXPORT CORP.,LTD.";
        //        doc.PrintDate = DateTime.Now;
        //        doc.Station = document.BuyingStationCode;
        //        doc.CreateDate = document.CreateDate;
        //        doc.FinishBuyingDate = (DateTime)document.FinishDate;

        //        var allBuyingList = BuyingFacade.BuyingBL()
        //            .GetByFarmer(document.Crop, document.FarmerCode);
        //        var buyingList = allBuyingList
        //            .Where(x => x.Crop == document.Crop &&
        //            x.FarmerCode == document.FarmerCode &&
        //            x.BuyingStationCode == document.BuyingStationCode &&
        //            x.BuyingDocumentNumber == document.BuyingDocumentNumber)
        //            .ToList();

        //        string[] lowGrade = {
        //        "XL5","X5Y","X5V","XK","XKY",
        //            "CL5","C5Y","C5V","CKY","CK",
        //            "B5V","B5Y","BKY","BK",
        //            "TK","ND","T4","T5"
        //        };

        //        var notRejectAllList = allBuyingList.Where(x => x.Grade != null);
        //        var hGradeAll = notRejectAllList
        //            .Where(x => x.Grade.Any(y => !lowGrade.Contains(x.Grade)))
        //            .ToList();
        //        var lGradeAll = notRejectAllList
        //            .Where(x => x.Grade.Any(y => lowGrade.Contains(x.Grade)))
        //            .ToList();

        //        doc.AccumulativeHighGradeAvgPrice = hGradeAll.Count() < 1 ? 0 : (decimal)(hGradeAll.Sum(x => x.Weight * x.BuyingGrade.UnitPrice) / hGradeAll.Sum(x => x.Weight));
        //        doc.AccumulativeLowGradeAvgPrice = lGradeAll.Count() < 1 ? 0 : (decimal)(lGradeAll.Sum(x => x.Weight * x.BuyingGrade.UnitPrice) / lGradeAll.Sum(x => x.Weight));

        //        for (int i = 1; i <= copies; i++)
        //            Print(doc, farmerInfo, debtInfo, farmerQuotaList, creditProjectList,
        //                buyingList, allBuyingList, "BUWinApp.RDLCReport.RPTBUY00122v1.rdlc");
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
    }
}
