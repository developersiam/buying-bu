﻿using BUWinApp.Helper;
using BusinessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BUWinApp.Forms.Hessian
{
    /// <summary>
    /// Interaction logic for SearchFarmer.xaml
    /// </summary>
    public partial class SearchFarmer : Window
    {
        static string _farmerCode;

        public SearchFarmer()
        {
            InitializeComponent();
            _farmerCode = null;
        }

        public static string ReturnResult()
        {
            SearchFarmer window = new SearchFarmer();
            window.ShowDialog();

            return _farmerCode;
        }

        private void FarmerDataGridBinding()
        {
            try
            {
                if (FarmerNameTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุชื่อชาวไร่ที่ต้องการค้นหา", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    FarmerNameTextBox.Focus();
                    return;
                }

                FarmerDataGrid.ItemsSource = null;
                FarmerDataGrid.ItemsSource = BuyingFacade.FarmerBL()
                    .GetByFarmerName(FarmerNameTextBox.Text)
                    .Where(x => x.Supplier1.SupplierArea == user_setting.User.StaffUser.AreaCode);

                totalItemLabel.Content = FarmerDataGrid.Items.Count.ToString("N0");

                if (FarmerDataGrid.Items.Count <= 0)
                    MessageBox.Show("ไม่พบชาวไร่ชื่อ " + FarmerNameTextBox.Text + " ในเขตพื้นที่รับผิดชอบของ " +
                        user_setting.User.StaffUser.Area.AreaName, "info", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SelectFarmerOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FarmerDataGrid.SelectedIndex < 0)
                    return;

                if (FarmerDataGrid.SelectedIndex < 0)
                    return;

                var model = (Farmer)FarmerDataGrid.SelectedItem;

                if (model.RegistrationFarmers.Where(x => x.Crop == user_setting.Crop.Crop1).Count() <= 0)
                    throw new ArgumentException("ในปีนี้ชาวไร่รายดังกล่าวไม่ได้ลงทะเบียนทำสัญญาไว้กับทางบริษัท");

                _farmerCode = ((Farmer)FarmerDataGrid.SelectedItem).FarmerCode;

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ViewProfileButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FarmerDataGrid.SelectedIndex < 0)
                    return;

                var model = (Farmer)FarmerDataGrid.SelectedItem;

                if (model.RegistrationFarmers.Where(x => x.Crop == user_setting.Crop.Crop1).Count() <= 0)
                    throw new ArgumentException("ในปีนี้ชาวไร่รายดังกล่าวไม่ได้ลงทะเบียนทำสัญญาไว้กับทางบริษัท");

                FarmerProfiles window = new FarmerProfiles(model.FarmerCode);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            FarmerDataGridBinding();
        }

        private void FarmerNameTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            FarmerNameTextBox.Clear();
        }

        private void FarmerNameTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                FarmerDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
