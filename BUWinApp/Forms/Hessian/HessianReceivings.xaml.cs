﻿using BUWinApp.Helper;
using BusinessLayer;
using BusinessLayer.Model;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BUWinApp.Forms.Hessian
{
    /// <summary>
    /// Interaction logic for HessianReceivings.xaml
    /// </summary>
    public partial class HessianReceivings : Page
    {
        m_HessianReceiving _hessianReceiving;

        public HessianReceivings()
        {
            InitializeComponent();

            _hessianReceiving = new m_HessianReceiving();
            _hessianReceiving = null;

            ReceivingDatePicker.SelectedDate = DateTime.Now;

            WarehouseComboBox.ItemsSource = null;
            WarehouseComboBox.ItemsSource = BuyingFacade.WarehouseBL()
                .GetByArea(user_setting.User.StaffUser.AreaCode);
        }

        private void ReceivingDataGridBinding()
        {
            try
            {
                if (WarehouseComboBox.SelectedIndex < 0)
                    return;

                var list = BusinessLayer.Helper.HessianReceivingHelper
                    .GetByWarehouse(user_setting.Crop.Crop1,
                    ((Warehouse)WarehouseComboBox.SelectedItem).WarehouseID);

                ReceivedDataGrid.ItemsSource = null;
                ReceivedDataGrid.ItemsSource = list.OrderByDescending(x => x.ReceivingCode);

                ReceivedLabel.Content = list.Sum(x => x.Quantity).ToString("N0");
                IssuedLabel.Content = list.Sum(x => x.Issued).ToString("N0");
                OnHandLabel.Content = list.Sum(x => x.OnHand).ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearForm()
        {
            ReceivingDatePicker.SelectedDate = DateTime.Now;
            TruckNoTextBox.Clear();
            QuantityTextBox.Clear();
            EditButton.Visibility = Visibility.Collapsed;
            AddButton.Visibility = Visibility.Visible;

            _hessianReceiving = null;

            ReceivingDataGridBinding();
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_hessianReceiving == null)
                    throw new ArgumentException("ไม่พบข้อมูลที่ต้องการแก้ไข");

                if (ReceivingDatePicker.SelectedDate == null)
                    throw new ArgumentException("โปรดระบุวันที่รับ");

                if (WarehouseComboBox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดระบุโกดังที่เก็บกระสอบ");

                if (TruckNoTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุทะเบียนระที่ใช้ในการขนส่ง");

                if (QuantityTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุจำนวนกระสอบที่ได้รับ");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(QuantityTextBox.Text) == false)
                    throw new ArgumentException("จำนวนกระสอบที่ได้รับจะต้องเป็นตัวเลขเท่านั้น");

                _hessianReceiving.WarehouseID = Guid.Parse(WarehouseComboBox.SelectedValue.ToString());
                _hessianReceiving.ReceivedDate = Convert.ToDateTime(ReceivingDatePicker.SelectedDate);
                _hessianReceiving.ReceivedBy = user_setting.User.Username;
                _hessianReceiving.TruckNumber = TruckNoTextBox.Text;
                _hessianReceiving.Quantity = Convert.ToInt16(QuantityTextBox.Text);

                if (MessageBox.Show("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่", "Confirm", MessageBoxButton.YesNo,
                    MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BuyingFacade.HessianReceivingBL().Update(_hessianReceiving);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void WarehouseComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (WarehouseComboBox.SelectedIndex < 0)
                    return;

                ReceivingDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ReceivingDatePicker.SelectedDate == null)
                    throw new ArgumentException("โปรดระบุวันที่รับ");

                if (WarehouseComboBox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดระบุโกดังที่เก็บกระสอบ");

                if (TruckNoTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุทะเบียนระที่ใช้ในการขนส่ง");

                if (QuantityTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุจำนวนกระสอบที่ได้รับ");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(QuantityTextBox.Text) == false)
                    throw new ArgumentException("จำนวนกระสอบที่ได้รับจะต้องเป็นตัวเลขเท่านั้น");

                BuyingFacade.HessianReceivingBL().Add(new HessianReceiving
                {
                    ReceivedDate = Convert.ToDateTime(ReceivingDatePicker.SelectedDate),
                    ReceivedBy = user_setting.User.Username,
                    Crop = user_setting.Crop.Crop1,
                    WarehouseID = Guid.Parse(WarehouseComboBox.SelectedValue.ToString()),
                    TruckNumber = TruckNoTextBox.Text,
                    Quantity = Convert.ToInt16(QuantityTextBox.Text),
                    ModifiedBy = user_setting.User.Username,
                    ModifiedDate = DateTime.Now
                });

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ReceivedDataGrid.SelectedIndex < 0)
                    return;

                _hessianReceiving = (m_HessianReceiving)ReceivedDataGrid.SelectedItem;

                ReceivingDatePicker.SelectedDate = _hessianReceiving.ReceivedDate;
                WarehouseComboBox.SelectedValue = _hessianReceiving.WarehouseID;
                TruckNoTextBox.Text = _hessianReceiving.TruckNumber;
                QuantityTextBox.Text = _hessianReceiving.Quantity.ToString();

                EditButton.Visibility = Visibility.Visible;
                AddButton.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ReceivedDataGrid.SelectedIndex < 0)
                    return;

                var selectedItem = (m_HessianReceiving)ReceivedDataGrid.SelectedItem;

                BuyingFacade.HessianReceivingBL().Delete(selectedItem.ReceivingCode);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
