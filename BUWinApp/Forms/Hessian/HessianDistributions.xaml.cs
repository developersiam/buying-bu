﻿using BUWinApp.Helper;
using BusinessLayer;
using BusinessLayer.Model;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BUWinApp.Forms.Hessian
{
    /// <summary>
    /// Interaction logic for HessianDistributions.xaml
    /// </summary>
    public partial class HessianDistributions : Page
    {
        HessianDistribution _hessian;

        public HessianDistributions()
        {
            InitializeComponent();

            _hessian = new HessianDistribution();

            WarehouseDataBinding();
            DistributionDataGridBinding();
        }

        private void WarehouseDataBinding()
        {
            try
            {
                WarehouseComboBox.ItemsSource = null;
                WarehouseComboBox.ItemsSource = BuyingFacade.WarehouseBL()
                    .GetByArea(user_setting.User.StaffUser.AreaCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void HessianReceivingDataBinding()
        {
            try
            {
                if (WarehouseComboBox.SelectedIndex < 0)
                    return;

                var list = BusinessLayer.Helper.HessianReceivingHelper
                    .GetByWarehouse(user_setting.Crop.Crop1,
                    Guid.Parse(WarehouseComboBox.SelectedValue.ToString()))
                    .Where(x => x.OnHand > 0);

                ReceivingCodeComboBox.ItemsSource = null;
                ReceivingCodeComboBox.ItemsSource = list;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DistributionDataGridBinding()
        {
            if (DistributionDatePicker.SelectedDate == null)
                return;

            var list = BuyingFacade.HessianDistributionBL()
                .GetByDistributionDate(user_setting.User.StaffUser.AreaCode,
                Convert.ToDateTime(DistributionDatePicker.SelectedDate));
            DistributionDataGrid.ItemsSource = null;
            DistributionDataGrid.ItemsSource = list;

            totalItemLabel.Content = DistributionDataGrid.Items.Count.ToString("N0");
            totalBaleLabel.Content = list.Sum(x => x.Quantity).ToString("N0");
        }

        private void SummaryByWarehouseDataBinding()
        {
            var list = BusinessLayer.Helper.HessianReceivingHelper
                    .GetByWarehouse(user_setting.Crop.Crop1,
                    ((Warehouse)WarehouseComboBox.SelectedItem).WarehouseID);

            ReceivedLabel.Content = list.Sum(x => x.Quantity).ToString("N0");
            IssuedLabel.Content = list.Sum(x => x.Issued).ToString("N0");
            OnHandLabel.Content = list.Sum(x => x.OnHand).ToString("N0");
        }

        private void ClearFrom()
        {
            HessianReceivingDataBinding();
            DistributionDataGridBinding();
            SummaryByWarehouseDataBinding();

            FarmerCodeTextBox.Clear();
            QuantityTextBox.Clear();
            OnHandTextBox.Clear();

            AddButton.Visibility = Visibility.Visible;
            EditButton.Visibility = Visibility.Collapsed;

            _hessian = null;
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FarmerCodeTextBox.Clear();
                var farmerCode = SearchFarmer.ReturnResult();

                if (farmerCode == null)
                    throw new ArgumentException("ไม่พบข้อมูล Farmer Code ของชาวไร่ที่ท่านค้นหา โปรดลองใหม่อีกครั้งหรือแจ้งแผนกไอทีเพื่อทำการตรวจสอบ");

                var regisModel = BuyingFacade.RegistrationBL().GetSingle(user_setting.Crop.Crop1, farmerCode);
                if (regisModel == null)
                    throw new ArgumentException("ชาวไร่รหัส " + farmerCode +
                        " ไม่ได้ลงทะเบียนทำสัญญาในปี " + user_setting.Crop.Crop1 + " ไม่สามารถจ่ายกระสอบให้กับชาวไร่รายนี้ได้");

                FarmerCodeTextBox.Text = farmerCode;
                FirstNameTextBox.Text = regisModel.Farmer.Person.FirstName;
                LastNameTextBox.Text = regisModel.Farmer.Person.LastName;
                QuantityTextBox.Clear();
                QuantityTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ShowFarmerProfileButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FarmerCodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอก Farmer Code", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    FarmerCodeTextBox.Focus();
                    return;
                }

                FarmerProfiles window = new FarmerProfiles(FarmerCodeTextBox.Text);
                window.ShowDialog();

                var regisModel = BuyingFacade.RegistrationBL().GetSingle(user_setting.Crop.Crop1, FarmerCodeTextBox.Text);
                if (regisModel == null)
                    throw new ArgumentException("ชาวไร่รหัส " + FarmerCodeTextBox.Text +
                        " ไม่ได้ลงทะเบียนทำสัญญาในปี " + user_setting.Crop.Crop1 + " ไม่สามารถจ่ายกระสอบให้กับชาวไร่รายนี้ได้");

                FirstNameTextBox.Text = regisModel.Farmer.Person.FirstName;
                LastNameTextBox.Text = regisModel.Farmer.Person.LastName;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FarmerCodeTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุ farmer code ของชาวไร่ที่จะจ่ายกระสอบให้");

                if (ReceivingCodeComboBox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดระบุ receiving code ของกระสอบที่ได้รับที่อยู่ใน stock");

                if (DistributionDatePicker.SelectedDate == null)
                    throw new ArgumentException("โปรดระบุ วันที่จ่ายกระสอบ (distribution date)");

                if (QuantityTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุ จำนวนกระสอบที่จะจ่าย (quantity)");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(QuantityTextBox.Text) == false)
                    throw new ArgumentException("จำนวนกระสอบที่จะจ่าย ต้องเป็นตัวเลขเท่านั้น");

                if (Convert.ToInt16(QuantityTextBox.Text) <= 0)
                    throw new ArgumentException("จำนวนกระสอบที่จะจ่าย ต้องมากกว่า 1 ขึ้นไป");

                //BuyingFacade.HessianDistributionBL()
                //    .Add(new HessianDistribution
                //    {
                //        DistributionID = Guid.NewGuid(),
                //        FarmerCode = FarmerCodeTextBox.Text,
                //        Quantity = Convert.ToInt16(QuantityTextBox.Text),
                //        ReceivingCode = ReceivingCodeComboBox.SelectedValue.ToString(),
                //        ModifiedBy = user_setting.User.Username,
                //        ModifiedDate = DateTime.Now,
                //        Crop = user_setting.Crop.Crop1,
                //        DistributedBy = user_setting.User.Username,
                //        DistributedDate = Convert.ToDateTime(DistributionDatePicker.SelectedDate)
                //    });
                ClearFrom();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_hessian == null)
                    throw new ArgumentException("ไม่พบข้อมูลที่ต้องการแก้ไข โปรดคลิกเลือกจากแถวข้อมูลที่ต้องการแก้ไขอีกครั้ง");

                if (FarmerCodeTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุ farmer code ของชาวไร่ที่จะจ่ายกระสอบให้");

                if (ReceivingCodeComboBox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดระบุ receiving code ของกระสอบที่ได้รับที่อยู่ใน stock");

                if (DistributionDatePicker.SelectedDate == null)
                    throw new ArgumentException("โปรดระบุ วันที่จ่ายกระสอบ (distribution date)");

                if (QuantityTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุ จำนวนกระสอบที่จะจ่าย (quantity)");

                if (Helper.RegularExpressionHelper.IsNumericCharacter(QuantityTextBox.Text) == false)
                    throw new ArgumentException("จำนวนกระสอบที่จะจ่าย ต้องเป็นตัวเลขเท่านั้น");

                if (Convert.ToInt16(QuantityTextBox.Text) <= 0)
                    throw new ArgumentException("จำนวนกระสอบที่จะจ่าย ต้องมากกว่า 1 ขึ้นไป");

                _hessian.DistributedBy = user_setting.User.Username;
                _hessian.DistributedDate = Convert.ToDateTime(DistributionDatePicker.SelectedDate);
                _hessian.ModifiedBy = user_setting.User.Username;
                _hessian.ModifiedDate = DateTime.Now;
                _hessian.ReceivingCode = ReceivingCodeComboBox.SelectedValue.ToString();
                _hessian.FarmerCode = FarmerCodeTextBox.Text;
                _hessian.Quantity = Convert.ToInt16(QuantityTextBox.Text);

                //BuyingFacade.HessianDistributionBL().Update(_hessian);
                ClearFrom();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearFrom();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DistributionDataGrid.SelectedIndex < 0)
                    return;

                _hessian = (HessianDistribution)DistributionDataGrid.SelectedItem;

                EditButton.Visibility = Visibility.Visible;
                AddButton.Visibility = Visibility.Collapsed;

                DistributionDatePicker.SelectedDate = _hessian.DistributedDate;
                ReceivingCodeComboBox.SelectedValue = _hessian.ReceivingCode;
                FarmerCodeTextBox.Text = _hessian.FarmerCode;
                QuantityTextBox.Text = _hessian.Quantity.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DistributionDataGrid.SelectedIndex < 0)
                    return;

                _hessian = (HessianDistribution)DistributionDataGrid.SelectedItem;

                if (MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "Confirm!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BuyingFacade.HessianDistributionBL().Delete(_hessian.DistributionID);
                ClearFrom();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FarmerCodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                if (FarmerCodeTextBox.Text == null)
                    throw new ArgumentException("โปรดระบุ Farmer code");

                var regisModel = BuyingFacade.RegistrationBL().GetSingle(user_setting.Crop.Crop1, FarmerCodeTextBox.Text);
                if (regisModel == null)
                {
                    FirstNameTextBox.Clear();
                    LastNameTextBox.Clear();
                    FarmerCodeTextBox.Focus();
                    throw new ArgumentException("ชาวไร่รหัส " + FarmerCodeTextBox.Text +
                        " ไม่ได้ลงทะเบียนทำสัญญาในปี " + user_setting.Crop.Crop1 + " ไม่สามารถจ่ายกระสอบให้กับชาวไร่รายนี้ได้");
                }

                FirstNameTextBox.Text = regisModel.Farmer.Person.FirstName;
                LastNameTextBox.Text = regisModel.Farmer.Person.LastName;

                QuantityTextBox.Clear();
                QuantityTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void WarehouseComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (WarehouseComboBox.SelectedIndex < 0)
                    return;

                SummaryByWarehouseDataBinding();
                HessianReceivingDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReceivingCodeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (ReceivingCodeComboBox.SelectedIndex < 0)
                    return;

                var model = (m_HessianReceiving)ReceivingCodeComboBox.SelectedItem;
                OnHandTextBox.Text = model.OnHand.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DistributionDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DistributionDataGridBinding();
        }
    }
}
