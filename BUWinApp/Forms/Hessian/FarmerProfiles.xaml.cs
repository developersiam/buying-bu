﻿using BUWinApp.Helper;
using BusinessLayer;
using BusinessLayer.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BUWinApp.Forms.Hessian
{
    /// <summary>
    /// Interaction logic for FarmerProfiles.xaml
    /// </summary>
    public partial class FarmerProfiles : Window
    {
        public FarmerProfiles(string farmerCode)
        {
            InitializeComponent();

            //Get a farmer from selected supplier *
            var farmer = BuyingFacade.FarmerBL()
                .GetByFarmerCode(farmerCode);

            if (farmer == null)
                throw new ArgumentException("ไม่พบข้อมูลชาวไร่ รหัส " + farmerCode + " นี้ในระบบ");

            FarmerCodeTextBox.Text = farmer.FarmerCode;
            FarmerNameTextBox.Text = farmer.Person.Prefix + farmer.Person.FirstName + " " + farmer.Person.LastName;
            AddressTextBox.Text = farmer.Person.HouseNumber + " หมู่ " + farmer.Person.Village + " ต." +
                farmer.Person.Tumbon + " อ." + farmer.Person.Amphur + " จ." + farmer.Person.Province;

            //Get a farmer contract **
            var regisModel = BuyingFacade.RegistrationBL()
                .GetSingle(user_setting.Crop.Crop1, FarmerCodeTextBox.Text);

            if (regisModel == null)
            {
                MessageBox.Show("ชาวไร่รายนี้ไม่ได้ลงทะเบียนทำสัญญาไว้กับทางบริษัทในปี " + user_setting.Crop.Crop1,
                    "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            ContractCodeTextBox.Text = regisModel.GAPContractCode;
            ContractQuotaTextBox.Text = regisModel.QuotaFromSignContract.ToString();

            BuyingDocumentDataGridBinding(regisModel.Crop, regisModel.FarmerCode);
            FarmerBalanceQuotaDataGridBinding(regisModel.Crop, regisModel.FarmerCode);
        }


        private void BuyingDocumentDataGridBinding(short crop, string farmerCode)
        {
            try
            {
                var _documentList = BuyingDocumentHelper.GetByFarmer(crop, farmerCode);

                BuyingDocumentDataGrid.ItemsSource = null;
                BuyingDocumentDataGrid.ItemsSource = _documentList.OrderByDescending(x => x.BuyingDocumentNumber);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FarmerBalanceQuotaDataGridBinding(short crop, string farmerCode)
        {
            try
            {
                if (crop.ToString() == "")
                {
                    MessageBox.Show("ไม่พบข้อมูล crop โปรดลองตรวจสอบข้อมูลใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (farmerCode == "")
                {
                    MessageBox.Show("ไม่พบข้อมูล farmerCode โปรดลองตรวจสอบข้อมูลใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                FarmerBalanceQuotaDataGrid.ItemsSource = null;
                FarmerBalanceQuotaDataGrid.ItemsSource = FarmerProjectHelper
                    .GetQuotaAndSoldByFarmerVersion2(crop, farmerCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
