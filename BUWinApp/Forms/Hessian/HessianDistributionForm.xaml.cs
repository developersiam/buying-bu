﻿using BUWinApp.Helper;
using BusinessLayer;
using BusinessLayer.Model;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BUWinApp.Forms.Hessian
{
    /// <summary>
    /// Interaction logic for HessianDistributionForm.xaml
    /// </summary>
    public partial class HessianDistributionForm : Window
    {
        HessianDistribution _hessian;
        m_BuyingDocument _document;

        public HessianDistributionForm(m_BuyingDocument document)
        {
            try
            {
                InitializeComponent();

                _hessian = new HessianDistribution();
                _document = new m_BuyingDocument();
                _document = document;

                WarehouseDataBinding();
                DistributionDataGridBinding();

                WarehouseComboBox.SelectedIndex = 0;
                ReceivingCodeComboBox.SelectedIndex = 0;
                DistributedDateTextBox.Text = Convert.ToDateTime(_document.BuyingDate).Date.ToShortDateString();
                MaximumQuantityTextBox.Text = (document.TotalWeight * 2).ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void WarehouseDataBinding()
        {
            try
            {
                WarehouseComboBox.ItemsSource = null;
                WarehouseComboBox.ItemsSource = BuyingFacade.WarehouseBL()
                    .GetByArea(user_setting.User.StaffUser.AreaCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void HessianReceivingDataBinding()
        {
            try
            {
                if (WarehouseComboBox.SelectedIndex < 0)
                    return;

                var list = BusinessLayer.Helper.HessianReceivingHelper
                    .GetByWarehouse(user_setting.Crop.Crop1,
                    Guid.Parse(WarehouseComboBox.SelectedValue.ToString()))
                    .Where(x => x.OnHand > 0);

                ReceivingCodeComboBox.ItemsSource = null;
                ReceivingCodeComboBox.ItemsSource = list;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DistributionDataGridBinding()
        {
            try
            {
                if (_document.BuyingDate == null)
                    return;

                var list = BuyingFacade.HessianDistributionBL()
                    .GetByVoucher(_document.Crop,
                    _document.FarmerCode,
                    _document.BuyingStationCode,
                    _document.BuyingDocumentNumber);

                DistributionDataGrid.ItemsSource = null;
                DistributionDataGrid.ItemsSource = list;

                totalItemLabel.Content = DistributionDataGrid.Items.Count.ToString("N0");
                totalBaleLabel.Content = list.Sum(x => x.Quantity).ToString("N0");
                distributedQuantityTextBox.Text = list.Sum(x => x.Quantity).ToString("N0");

                if (list.Sum(x => x.Quantity) >= _document.TotalWeight * 2)
                {
                    MaximumQuantityTextBox.Background = Brushes.Salmon;
                    distributedQuantityTextBox.Background = Brushes.Salmon;
                    QuantityTextBox.Background = Brushes.Salmon;
                }
                else
                {
                    MaximumQuantityTextBox.Background = Brushes.LightGreen;
                    distributedQuantityTextBox.Background = Brushes.LightGreen;
                    QuantityTextBox.Background = Brushes.LightGreen;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SummaryByWarehouseDataBinding()
        {
            var list = BusinessLayer.Helper.HessianReceivingHelper
                    .GetByWarehouse(user_setting.Crop.Crop1,
                    ((Warehouse)WarehouseComboBox.SelectedItem).WarehouseID);

            ReceivedLabel.Content = list.Sum(x => x.Quantity).ToString("N0");
            IssuedLabel.Content = list.Sum(x => x.Issued).ToString("N0");
            OnHandLabel.Content = list.Sum(x => x.OnHand).ToString("N0");
        }

        private void ClearFrom()
        {
            HessianReceivingDataBinding();
            DistributionDataGridBinding();
            SummaryByWarehouseDataBinding();
            QuantityTextBox.Clear();
            OnHandTextBox.Clear();

            ReceivingCodeComboBox.SelectedIndex = 0;

            AddButton.Visibility = Visibility.Visible;
            EditButton.Visibility = Visibility.Collapsed;

            _hessian = null;
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_document.FarmerCode == "")
                    throw new ArgumentException("โปรดระบุ farmer code ของชาวไร่ที่จะจ่ายกระสอบให้");

                if (ReceivingCodeComboBox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดระบุ receiving code ของกระสอบที่ได้รับที่อยู่ใน stock");

                if (RegularExpressionHelper.IsNumericCharacter(QuantityTextBox.Text) == false)
                    throw new ArgumentException("จำนวนกระสอบที่จะจ่าย ต้องเป็นตัวเลขเท่านั้น");

                BuyingFacade.HessianDistributionBL()
                    .Add(new HessianDistribution
                    {
                        DistributionID = Guid.NewGuid(),
                        Quantity = Convert.ToInt16(QuantityTextBox.Text),
                        ReceivingCode = ReceivingCodeComboBox.SelectedValue.ToString(),
                        ModifiedBy = user_setting.User.Username,
                        ModifiedDate = DateTime.Now,
                        Crop = _document.Crop,
                        FarmerCode = _document.FarmerCode,
                        BuyingStationCode = _document.BuyingStationCode,
                        BuyingDocumentNumber = _document.BuyingDocumentNumber,
                        DistributedBy = user_setting.User.Username,
                        DistributedDate = Convert.ToDateTime(_document.BuyingDate)
                    });
                ClearFrom();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                if (_hessian == null)
                    throw new ArgumentException("ไม่พบข้อมูลที่ต้องการแก้ไข โปรดคลิกเลือกจากแถวข้อมูลที่ต้องการแก้ไขอีกครั้ง");

                if (ReceivingCodeComboBox.SelectedIndex < 0)
                    throw new ArgumentException("โปรดระบุ receiving code ของกระสอบที่ได้รับที่อยู่ใน stock");

                if (QuantityTextBox.Text == "")
                    throw new ArgumentException("โปรดระบุ จำนวนกระสอบที่จะจ่าย (quantity)");

                if (RegularExpressionHelper.IsNumericCharacter(QuantityTextBox.Text) == false)
                    throw new ArgumentException("จำนวนกระสอบที่จะจ่าย ต้องเป็นตัวเลขเท่านั้น");

                //if (Convert.ToInt16(QuantityTextBox.Text) <= 0)
                //    throw new ArgumentException("จำนวนกระสอบที่จะจ่าย ต้องมากกว่า 1 ขึ้นไป");

                _hessian.DistributedBy = user_setting.User.Username;
                _hessian.DistributedDate = Convert.ToDateTime(_document.BuyingDate);
                _hessian.ModifiedBy = user_setting.User.Username;
                _hessian.ModifiedDate = DateTime.Now;
                _hessian.ReceivingCode = ReceivingCodeComboBox.SelectedValue.ToString();
                _hessian.Quantity = Convert.ToInt16(QuantityTextBox.Text);

                BuyingFacade.HessianDistributionBL().Update(_hessian, _document);
                ClearFrom();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearFrom();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DistributionDataGrid.SelectedIndex < 0)
                    return;

                _hessian = (HessianDistribution)DistributionDataGrid.SelectedItem;

                EditButton.Visibility = Visibility.Visible;
                AddButton.Visibility = Visibility.Collapsed;

                DistributedDateTextBox.Text = _hessian.DistributedDate.ToShortDateString();
                ReceivingCodeComboBox.SelectedValue = _hessian.ReceivingCode;
                _document.FarmerCode = _hessian.FarmerCode;
                QuantityTextBox.Text = _hessian.Quantity.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DistributionDataGrid.SelectedIndex < 0)
                    return;

                _hessian = (HessianDistribution)DistributionDataGrid.SelectedItem;

                if (MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "Confirm!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BuyingFacade.HessianDistributionBL().Delete(_hessian.DistributionID);
                ClearFrom();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void WarehouseComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (WarehouseComboBox.SelectedIndex < 0)
                    return;

                SummaryByWarehouseDataBinding();
                HessianReceivingDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReceivingCodeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (ReceivingCodeComboBox.SelectedIndex < 0)
                {
                    OnHandTextBox.Clear();
                    return;
                }

                var model = (m_HessianReceiving)ReceivingCodeComboBox.SelectedItem;
                OnHandTextBox.Text = model.OnHand.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
