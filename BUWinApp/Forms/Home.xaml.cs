﻿using BUWinApp.Forms.BuyingStaff;
using BUWinApp.Helper;
using BusinessLayer;
using BusinessLayer.Helper;
using BusinessLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BUWinApp.Forms
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : Page
    {
        public object BuyingDatePicker { get; private set; }

        public Home()
        {
            InitializeComponent();

            BuyingStationComboBox.ItemsSource = null;
            BuyingStationComboBox.ItemsSource = BuyingFacade.BuyingStationBL()
                .GetByArea(user_setting.User.StaffUser.AreaCode)
                .OrderBy(x => x.StationCode);

            CreateDocumentDatePicker.SelectedDate = DateTime.Now;
        }

        private void DataGridBinding()
        {
            try
            {
                if (CreateDocumentDatePicker.SelectedDate == null)
                    return;

                if (BuyingStationComboBox.SelectedIndex < 0)
                    return;

                var buyingDocInfoList = BuyingDocumentHelper
                        .GetByStationAndCreateDate(Convert.ToDateTime(CreateDocumentDatePicker.SelectedDate),
                        BuyingStationComboBox.SelectedValue.ToString())
                        .ToList();

                VoucherListDataGrid.ItemsSource = null;
                VoucherListDataGrid.ItemsSource = FarmerNameTextBox.Text.Length <= 0 ?
                    buyingDocInfoList :
                    buyingDocInfoList.Where(x => x.FirstName.Contains(FarmerNameTextBox.Text));

                totalItemLabel.Content = VoucherListDataGrid.Items.Count.ToString("N0");

                TotalBaleLabel.Content = buyingDocInfoList.Sum(x => x.TotalBale).ToString("N0");
                RejectedBaleLabel.Content = buyingDocInfoList.Sum(x => x.TotalReject).ToString("N0");
                BuyingBaleLabel.Content = buyingDocInfoList.Sum(x => x.TotalWeight).ToString("N0");
                BuyingWeightLabel.Content = Convert.ToDouble(buyingDocInfoList.Sum(x => x.TotalSold)).ToString("N1");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CreateDocumentDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGridBinding();
        }

        private void BuyingStationComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGridBinding();
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            DataGridBinding();
        }

        private void ViewVoucherDetailsButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BuyingStationComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("โปรดระบุลานรับซื้อ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (CreateDocumentDatePicker.SelectedDate.ToString() == "")
                {
                    MessageBox.Show("โปรดระบุวันที่ซื้อขาย", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var document = (m_BuyingDocument)VoucherListDataGrid.SelectedItem;
                BuyingVoucherDetails window = new BuyingVoucherDetails(document);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FarmerNameTextBox.Clear();
                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
