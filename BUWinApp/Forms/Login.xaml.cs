﻿using BusinessLayer;
using DomainModel;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using BUWinApp.Helper;

namespace BUWinApp.Forms
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Page
    {
        public Login()
        {
            InitializeComponent();

            UsernameTextBox.Focus();
            UsernameTextBox.Clear();
            PasswordTextBox.Clear();
        }

        private void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text.Length <= 0)
                {
                    MessageBox.Show("โปรดกรอกชื่อผู้ใช้", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (PasswordTextBox.Password.Length <= 0)
                {
                    MessageBox.Show("โปรดกรอกรหัสผ่าน", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var user = BuyingFacade.UserBL().GetByUsername(UsernameTextBox.Text);
                if (user == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลบัญชีผู้ใช้นี้ในระบบ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);

                    UsernameTextBox.Focus();
                    return;
                }

                if (user.Password != PasswordTextBox.Password)
                {
                    MessageBox.Show("รหัสผ่านไม่ถูกต้อง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);

                    PasswordTextBox.Password = "";
                    PasswordTextBox.Focus();
                    return;
                }

                user_setting.User = user;
                user_setting.UserRoles = (from ur in BuyingFacade.UserRoleBL().GetByUser(user.Username)
                                            from r in BuyingFacade.RoleBL().GetAll()
                                            where ur.RoleID == r.RoleID
                                            select new Role
                                            {
                                                RoleID = ur.RoleID,
                                                RoleName = r.RoleName
                                            }).ToList();

                this.NavigationService.Navigate(new Home());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UsernameTextBox.Text = "";
                PasswordTextBox.Password = "";
                UsernameTextBox.Focus();

                user_setting.UserRoles = null;
                user_setting.User = null;

                Application.Current.Resources["UserName"] = "Guest";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UsernameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text.Length > 0 && PasswordTextBox.Password.Length > 0)
                    LoginButton.IsEnabled = true;
                else
                    LoginButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PasswordTextBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text.Length > 0 && PasswordTextBox.Password.Length > 0)
                    LoginButton.IsEnabled = true;
                else
                    LoginButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            UsernameTextBox.Focus();
        }
    }
}
