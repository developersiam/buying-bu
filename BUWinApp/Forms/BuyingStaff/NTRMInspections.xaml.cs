﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BUWinApp.Helper;

namespace BUWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for NTRMInspectionPopup.xaml
    /// </summary>
    public partial class NTRMInspections : Window
    {
        string _baleBarcode;

        public NTRMInspections(string baleBarcode)
        {
            InitializeComponent();

            _baleBarcode = baleBarcode;
            BaleBarcodeTextBox.Text = _baleBarcode;
            BaleBarcodeTextBox.Focus();

            NTRMTypeItemsControlDataBinding();
            NTRMInspectionDataGridBinding();
        }

        private void NTRMInspectionDataGridBinding()
        {
            try
            {
                NTRMInspectionDataGrid.ItemsSource = null;
                NTRMInspectionDataGrid.ItemsSource = BuyingFacade.NtrmInspectionBL()
                     .GetByBaleBarcode(BaleBarcodeTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void NTRMTypeItemsControlDataBinding()
        {
            try
            {
                NTRMTypeItemsControl.ItemsSource = null;
                NTRMTypeItemsControl.ItemsSource = BuyingFacade.NtrmInspectionBL()
                    .GetAllNTRMType()
                    .OrderBy(x => x.NTRMTypeName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void NTRMTypeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                /// การบันทึกข้อมูลการตรวจ NTRM มี 2 กรณีคือ
                /// 1.ตรวจแล้วพบ NTRM ต้องระบุจำนวน
                /// 2.ตรวจแล้วไม่พบ NTRM ไม่ต้องระบุจำนวน
                /// เมื่อตรวจแล้วไม่ว่าจะพบหรือไม่พบ NTRM ให้ update column "IsNTRMInspection" = true 
                /// เพื่อบ่งบอกว่าห่อยาดังกล่าวได้ผ่านการตรวจ NTRM แล้ว
                /// NTRMCode 16 คือ Not found หรือตรวจแล้วไม่พบ NTRM
                /// 

                if (BaleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุ Bale barcode ห่อยาที่ต้องการบันทึก NTRM", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    return;
                }

                if(Helper.RegularExpressionHelper.IsNumericCharacter(QuantityTextBox.Text) == false)
                {
                    MessageBox.Show("จำนวนที่พบจะต้องเป็นตัวเลขเท่านั้น", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    QuantityTextBox.Clear();
                    QuantityTextBox.Focus();
                    return;
                }

                Button btn = (Button)sender;

                if (QuantityTextBox.Text == "" && btn.ToolTip.ToString() != "16")
                {
                    MessageBox.Show("โปรดระบุจำนวน NTRM ที่พบ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    QuantityTextBox.Focus();
                    return;
                }

                BuyingFacade.NtrmInspectionBL()
                    .Add(BaleBarcodeTextBox.Text,
                    btn.ToolTip.ToString(),
                    btn.ToolTip.ToString() == "16" ? Convert.ToByte(0) : Convert.ToByte(QuantityTextBox.Text),
                    user_setting.User.Username);

                NTRMInspectionDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RemoveNTRMButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (NTRMInspectionDataGrid.SelectedIndex <= -1)
                    return;

                var ntrmInspection = (NTRMInspection)NTRMInspectionDataGrid.SelectedItem;

                BuyingFacade.NtrmInspectionBL().Delete(ntrmInspection.BaleBarcode,
                    ntrmInspection.NTRMTypeCode,
                    ntrmInspection.InspectionDate);

                NTRMInspectionDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                QuantityTextBox.Text = "1";
                BaleBarcodeTextBox.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void InCreaseButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                QuantityTextBox.Text = (Convert.ToInt16(QuantityTextBox.Text) + 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DecreaseButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (QuantityTextBox.Text == "1")
                    return;

                QuantityTextBox.Text = (Convert.ToInt16(QuantityTextBox.Text) - 1).ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BaleBarcodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter)
                return;

            var model = BuyingFacade.BuyingBL().GetByBaleBarcode(BaleBarcodeTextBox.Text);
            if (model == null)
            {
                MessageBox.Show("ไม่พบข้อมูลห่อยาดังกล่าวในระบบ", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                BaleBarcodeTextBox.SelectAll();
                BaleBarcodeTextBox.Focus();
                return;
            }

            QuantityTextBox.Focus();
            QuantityTextBox.SelectAll();

            NTRMInspectionDataGridBinding();
        }
    }
}
