﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO.Ports;
using System.Windows.Threading;
using System.Threading;
using BusinessLayer;
using DomainModel;
using BusinessLayer.Model;
using BusinessLayer.Helper;

namespace BUBuyingSystemWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for CaptureWeightMainScreen.xaml
    /// 
    /// ขั้นตอนการทำงานของระบบ
    /// 1. ผู้ใช้สแกนบาร์โค้ตห่อยาที่ต้องการชั่งน้ำหนัก
    /// 2. ระบบแสดงข้อมูลได้แก่
    ///     2.1 ข้อมูลประวัติชาวไร่ ประวัติการลงทะเบียน
    ///     2.2 ข้อมูลใบรายการซื้อขาย
    ///     2.3 ข้อมูลโควต้าคงเหลือ ยอดรวมที่ขายไป
    ///     2.4 Summary of Sold by Position
    ///     2.5 Summary of Sold by Quality
    ///     2.6 ข้อมูลเกี่ยวกับห่อยาที่สแกนหมายเลขบาร์โค้ต
    ///     2.7 รายการห่อยาที่ทำการรับป้าย บันทึกเกรดซื้อ บันทึกน้ำหนักและขนขึ้นรถยรรทุก
    /// 3. ผู้ใช้บันทึกน้ำหนัก
    /// 
    /// เงื่อนไขที่ใช้ตรวจสอบข้อมูลก่อนการบันทึกน้ำหนัก
    /// 1. มีห่อยาในระบบหรือไม่
    /// 2. ห่อยานั้นได้ถูกบันทึกเกรดมาแล้วหรือไม่
    /// 3. ห่อยาที่ถูกบันทึกเกรดแล้วจะต้องไม่ใช่ห่อยาที่ถูก Back
    /// 4. ห่อยานั้นจะต้องยังไม่ถูกขนขึ้นรถบรรทุก 
    /// 5. ใบรายการซื้อขายจะต้องไม่ถูก Finish
    /// 6. สถานะสัญญาของชาวไร่จะต้อง Active
    /// 7. สถานะการขายของชาวไร่จะต้อง Active
    /// 8. X low grade limit จะต้องไม่เกิน 3.5%
    /// 9. น้ำหนักรวมที่ขายไปแล้วจะต้องไม่เกินจำนวนโควต้าที่ตั้งไว้ในแต่ละโปรเจค
    /// 
    /// </summary>
    public partial class CaptureWeightMainScreen : Window
    {
        PricingSet _pricingSet;
        List<BuyingGrade> _gradeList;
        m_BuyingDocument _buyingDocumentInfo;
        RegistrationFarmer _registrationFarmer;
        Buying _buying;
        List<m_FarmerProject> _farmerProjectAndSoldList;        
        List<Role> _roleName;
        CaptureWeightSecondaryScreen monitor2;
        BUBuyingSystemConfiguration _config = BUBuyingSystemConfiguration.GetInstance();
        SerialPort serialPort = new SerialPort();
        string lineReadIn;

        private delegate void preventCrossThreading(string x);
        private preventCrossThreading accessControlFromCentralThread;

        struct WeighString
        {
            public int index { get; set; }
            public string WeightString { get; set; }
        }

        private void SetupSerialPort()
        {
            try
            {
                if (SerialPort.GetPortNames().Count() < 1)
                {
                    MessageBox.Show("ไม่พบเครื่องชั่งดิจิตอลที่เชื่อมต่อมายังเครื่องคอมพิวเตอร์นี้ โปรดตรวจสอบอุปกรณ์ หรือแจ้งแผนกไอทีเพื่อทำการตรวจสอบ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);

                    IsDigitalScaleCheckBox.IsChecked = true;

                    WeightTextBox.IsReadOnly = false;
                    WeightTextBox.Text = "";

                    return;
                }

                serialPort.PortName = Properties.Settings.Default.PortName;
                serialPort.BaudRate = Properties.Settings.Default.BaudRate;
                serialPort.Parity = Properties.Settings.Default.Parity == "N" ? Parity.None : Parity.Even;
                serialPort.DataBits = Properties.Settings.Default.Databit;
                serialPort.StopBits = Properties.Settings.Default.StopBits == "N" ? StopBits.None : StopBits.One;

                serialPort.Open();

                if (serialPort.IsOpen)
                {
                    accessControlFromCentralThread = displayTextReadIn;
                    serialPort.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        // this, hopefully, will prevent cross threading.
        private void displayTextReadIn(string ToBeDisplayed)
        {
            if (WeightTextBox.Dispatcher.CheckAccess())
            {
                WeightTextBox.Text = ToBeDisplayed;
                monitor2.WeightLabel.Content = WeightTextBox.Text;
            }
            else
            {
                WeightTextBox.Dispatcher.Invoke(() =>
                {
                    WeightTextBox.Text = ToBeDisplayed;
                    monitor2.WeightLabel.Content = WeightTextBox.Text;
                });
            }
        }

        // this is called when the serial port has receive-data for us.
        private void port_DataReceived(object sender, SerialDataReceivedEventArgs rcvdData)
        {
            //if (Properties.Settings.Default.StationCode == "STEC")
            //{
            //    //
            //    //
            //    //STEC Go down 2 digital scale.
            //    lineReadIn = lineReadIn + serialPort.ReadExisting();
            //    Thread.Sleep(300);

            //    // display what we've acquired.
            //    string resultInput = lineReadIn;



            //    string[] splitString;
            //    string[] stringSeparators = new string[] { "\r" };

            //    splitString = resultInput.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

            //    if (splitString.Count() < 2)
            //        return;


            //    string[] splitString2;

            //    splitString2 = splitString[1].Split(' ');

            //    int i = 0;
            //    List<WeighString> resultWeightString = new List<WeighString>();

            //    foreach (string item in splitString2)
            //    {
            //        if (item != "")
            //        {
            //            resultWeightString.Add(new WeighString { index = i, WeightString = item.Replace(" ", "") });
            //            i++;
            //        }
            //    }

            //    if (resultWeightString.Where(r => r.index == 1).Count() < 1)
            //        return;//resultInput = "";
            //    else
            //        resultInput = resultWeightString.Where(r => r.index == 1).FirstOrDefault().WeightString;

            //    double resultWeight;

            //    if (resultInput != "")
            //    {
            //        resultWeight = Convert.ToDouble(resultInput);

            //        if (resultWeight > 0)
            //            resultWeight = resultWeight / 10;

            //        string weight = resultWeightString.Where(r => r.index == 0).FirstOrDefault().WeightString;

            //        resultWeight = resultWeight - 2.3;
            //        resultInput = resultWeight.ToString("N1");
            //    }
            //    else
            //    {
            //        resultInput = "0.0";
            //    }

            //    displayTextReadIn(resultInput);
            //    lineReadIn = "";
            //}
            //else if (Properties.Settings.Default.StationCode == "CR")
            //{
                //
                //
                //Chiangrai digital scale.

                lineReadIn += serialPort.ReadExisting();
                Thread.Sleep(300);

                // display what we've acquired.
                string resultInput = lineReadIn;

                if (resultInput.Length <= Properties.Settings.Default.SubStringLength)
                    return;

                //if (resultInput.IndexOf("U") == 0)
                //    return;

                string[] splitString;
                string[] stringSeparators = new string[] { "\r\n" };

                splitString = resultInput.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                if (splitString.Count() < 2)
                    return;

                resultInput = splitString[1];

                resultInput = resultInput.Replace(" ", string.Empty);
                resultInput = resultInput.Replace("", string.Empty);


                foreach (char item in Properties.Settings.Default.ReplaceChar)
                    resultInput = resultInput.Replace(item.ToString(), "");

                if (resultInput != "")
                {
                    decimal resultWeight = Convert.ToDecimal(resultInput);

                    ///ถ้าใช้ในระบบ BU Buying System จะไม่มีการหักน้ำหนักกระสอบในระบบ จะใช้การหักน้ำหนักกระสอบจากการ Tare ที่หน้าเครื่องชั่งเอง
                    //resultInput = (resultWeight - Convert.ToDecimal(2.3)).ToString("N1");
                    resultInput = resultWeight.ToString("N1");
                }

                displayTextReadIn(resultInput);
                lineReadIn = "";

                //
                //
            //}
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            monitor2.Close();

            if (serialPort.IsOpen)
            {
                e.Cancel = true; //cancel the fom closing

                Thread CloseDown = new Thread(new ThreadStart(CloseSerialOnExit)); //close port in new thread to avoid hang

                CloseDown.Start(); //close port in new thread to avoid hang
            }
        }

        private void CloseSerialNonExit()
        {
            try
            {
                serialPort.DataReceived -= port_DataReceived;
                serialPort.Close(); //close the serial port
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); //catch any serial port closing error messages
            }
        }

        private void CloseSerialOnExit()
        {
            try
            {
                serialPort.DataReceived -= port_DataReceived;
                serialPort.Close(); //close the serial port
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); //catch any serial port closing error messages
            }

            //this.Dispatcher.BeginInvoke(new EventHandler(NowClose)); //now close back in the main thread
            this.Dispatcher.Invoke(() =>
            {
                this.Close();
            });
        }

        private void NowClose(object sender, EventArgs e)
        {
            this.Close(); //now close the form
        }







        public CaptureWeightMainScreen()
        {
            InitializeComponent();
            _roleName = new List<Role>();
            _roleName = null;

            _pricingSet = new PricingSet();
            _gradeList = new List<BuyingGrade>();
            _buyingDocumentInfo = new m_BuyingDocument();
            _registrationFarmer = new RegistrationFarmer();
            _buying = new Buying();
            _farmerProjectAndSoldList = new List<m_FarmerProject>();


            monitor2 = new CaptureWeightSecondaryScreen();

            //Set ค่าให้กับ object เพื่อเอาไว้ใช้เป็นพารามิเตอร์หลักในการบันทึกเกรด
            _pricingSet = BuyingFacade.PricingSetBL().GetByDefault();
            if(_pricingSet == null)
            {
                MessageBox.Show("Please set default of pricing set.", "alert", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            _gradeList = BuyingFacade.BuyingGradeBL().GetByPricingSet(_pricingSet.Crop, _pricingSet.PricingNumber);
            BuyingGradeComboBox.ItemsSource = null;
            BuyingGradeComboBox.ItemsSource = _gradeList;
        }

        private void ClearForm()
        {
            try
            {
                FarmerCodeTextBlock.Text = "";
                FarmerNameTextBlock.Text = "";
                GAPGroupCodeTextBox.Text = "";
                ContractStatusCheckBox.IsChecked = false;

                BaleBarcodeTextBox.Text = "";

                DocumentCodeTextBlock.Text = "";
                TotalBaleTextBlock.Text = "";
                TotalScanTextBlock.Text = "";
                TotalRejectTextBlock.Text = "";
                FinishStatusCheckBox.IsChecked = false;

                WeightTextBox.Text = "";

                BaleBarCodeTextBlock.Text = "";
                ProjectTypeTextBlock.Text = "";
                IsExtraTextBlock.Text = "";
                GradeTextBlock.Text = "";
                WeightTextBlock.Text = "";
                TransportationCodeTextBlock.Text = "";

                BuyingDataGrid.ItemsSource = null;
                FarmerBalanceQuotaDataGrid.ItemsSource = null;

                TotalWeightTextBox.Text = "0";

                BaleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private bool CheckBarcodeInfo(string baleBarcode)
        {
            //Check invalid STEC barcode digit(17 digit).
            if (baleBarcode.Length != 17)
            {
                MessageBox.Show("หมายเลขบาร์โค้ตจะต้องมี 17 หลักเท่านั้น", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            //Check invalid prifix STEC barcode (2 digit).
            if (baleBarcode.Substring(0, 2) != "00" &&
                baleBarcode.Substring(0, 2) != "01" &&
                baleBarcode.Substring(0, 2) != "02" &&
                baleBarcode.Substring(0, 2) != "03" &&
                baleBarcode.Substring(0, 2) != "04" &&
                baleBarcode.Substring(0, 2) != "06")
            {
                MessageBox.Show("ตัวเลข 2 หลักด้านหน้าของบาร์โค้ตจะต้องเป็น 00,01,02,03,04,06 เท่านั้น", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            double n;
            bool isNumeric = double.TryParse(baleBarcode, out n);
            if (isNumeric == false)
            {
                MessageBox.Show("หมายเลข Bale barcode ทุกตัวจะต้องเป็นตัวเลขทั้งหมด", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            return true;
        }

        private void BuyingDocumentDetailsDataBinding(Buying buying)
        {
            try
            {
                //Set ค่าให้กับ object เพื่อเอาไว้ใช้เป็นพารามิเตอร์หลักในการบันทึกเกรด
                _buyingDocumentInfo = BuyingDocumentHelper.GetSingle(buying.Crop, 
                    buying.BuyingStationCode, 
                    buying.FarmerCode, 
                    buying.BuyingDocumentNumber);


                DocumentCodeTextBlock.Text = _buyingDocumentInfo.DocumentCode;
                TotalBaleTextBlock.Text = _buyingDocumentInfo.TotalBale.ToString("N0");
                TotalScanTextBlock.Text = _buyingDocumentInfo.TotalScan.ToString("N0");
                TotalRejectTextBlock.Text = _buyingDocumentInfo.TotalReject.ToString();
                FinishStatusCheckBox.IsChecked = _buyingDocumentInfo.IsFinish;

                BuyingDataGrid.ItemsSource = null;
                BuyingDataGrid.ItemsSource = _buyingDocumentInfo.Buyings.OrderByDescending(x => x.WeightDate);

                //TotalIntemsTextBlock.Text = BuyingDataGrid.Items.Count.ToString();

                TotalWeightTextBox.Text = _buyingDocumentInfo.TotalCaptureWeight + "/" + _buyingDocumentInfo.TotalScan;

                monitor2.BaleLabel.Content = TotalWeightTextBox.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchBuyingInfoFromBaleBarcode(string baleBarcode)
        {
            try
            {
                if (baleBarcode == "")
                {
                    MessageBox.Show("โปรดสแกนบาร์โค้ตเพื่อเริ่มต้นค้นหาข้อมูล", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    return;
                }

                //Set ค่าให้กับ object เพื่อเอาไว้ใช้เป็นพารามิเตอร์หลักในการบันทึกเกรด
                _buying = BuyingFacade.BuyingBL().GetByBaleBarcode(baleBarcode);

                if (_buying == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลยาห่อนี้ในระบบ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (_pricingSet == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลการกำหนดชุดราคาให้กับเกรดซื้อในระบบ จะทำให้ไม่สามารถบันทึกเกรดซื้อ โปรดติดต่อแผนกไอที", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                FarmerCodeTextBlock.Text = "";
                FarmerNameTextBlock.Text = "";
                GAPGroupCodeTextBox.Text = "";
                ContractStatusCheckBox.IsChecked = false;

                DocumentCodeTextBlock.Text = "";
                TotalBaleTextBlock.Text = "";
                TotalScanTextBlock.Text = "";
                TotalRejectTextBlock.Text = "";
                FinishStatusCheckBox.IsChecked = false;

                WeightTextBox.Text = "";

                BaleBarCodeTextBlock.Text = "";
                ProjectTypeTextBlock.Text = "";
                IsExtraTextBlock.Text = "";
                GradeTextBlock.Text = "";
                WeightTextBlock.Text = "";
                TransportationCodeTextBlock.Text = "";


                monitor2.GradeLabel.Content = "";
                monitor2.FarmerNameLable.Content = "";
                monitor2.BaleLabel.Content = "";

                BuyingDataGrid.ItemsSource = null;
                FarmerBalanceQuotaDataGrid.ItemsSource = null;

                TotalWeightTextBox.Text = "0";

                BuyingDocumentDetailsDataBinding(_buying);
                

                //Set ค่าให้กับ object เพื่อเอาไว้ใช้เป็นพารามิเตอร์หลักในการบันทึกน้ำหนัก
                _registrationFarmer = BuyingFacade.RegistrationBL().GetFarmerAndCrop(_buying.Crop, _buying.FarmerCode);                

                FarmerCodeTextBlock.Text = _registrationFarmer.FarmerCode;
                FarmerNameTextBlock.Text = _registrationFarmer.Farmer.Person.Prefix
                    + _registrationFarmer.Farmer.Person.FirstName + " "
                    + _registrationFarmer.Farmer.Person.LastName;

                GAPGroupCodeTextBox.Text = _registrationFarmer.GAPGroupCode;
                ContractStatusCheckBox.IsChecked = _registrationFarmer.ActiveStatus;

                //Get and set m_FarmerProjectList
                _farmerProjectAndSoldList = FarmerProjectHelper
                    .GetQuotaAndSoldByFarmerVersion2(_registrationFarmer.Crop, _registrationFarmer.FarmerCode);

                FarmerBalanceQuotaDataGrid.ItemsSource = _farmerProjectAndSoldList;

                BaleBarCodeTextBlock.Text = _buying.BaleBarcode;
                ProjectTypeTextBlock.Text = _buying.ProjectType;
                IsExtraTextBlock.Text = _buying.IsExtraQuota == true ? "Extra" : "Normal";
                GradeTextBlock.Text = _buying.Grade;
                BuyingGradeComboBox.SelectedValue = _buying.Grade;
                WeightTextBlock.Text = _buying.Weight.ToString();
                TransportationCodeTextBlock.Text = _buying.TransportationDocumentCode;

                monitor2.FarmerNameLable.Content = FarmerNameTextBlock.Text;
                monitor2.GradeLabel.Content = GradeTextBlock.Text;
                
                if (_buying.Grade == null && _buying.RejectReason == null)
                {
                    GradeTextBlock.Text = "N/A";
                    MessageBox.Show("ไม่พบข้อมูลเกรดซื้อในระบบ โปรดตรวจสอบข้อมูลจากป้ายหน้าห่อยาอีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //ClearForm();
                    return;
                }
                else if (_buying.Grade == null && _buying.RejectReason != null)
                {
                    GradeTextBlock.Text = _buying.RejectReason;
                    MessageBox.Show("ยาห่อนี้ถูกแบ็ค (" + _buying.RejectReason + ") ไม่สามารถนำมาชั่งน้ำหนักได้ โปรดตรวจสอบข้อมูลจากป้ายหน้าห่อยาอีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Text = "";
                    BaleBarcodeTextBox.Focus();
                    
                    BaleBarCodeTextBlock.Text = "";
                    ProjectTypeTextBlock.Text = "";
                    IsExtraTextBlock.Text = "";
                    GradeTextBlock.Text = "";
                    WeightTextBlock.Text = "";
                    TransportationCodeTextBlock.Text = "";
                    
                    monitor2.GradeLabel.Content = "";
                    monitor2.FarmerNameLable.Content = "";
                    monitor2.BaleLabel.Content = "";
                    return;
                }
                else
                {
                    GradeTextBlock.Text = _buying.Grade;
                }                
                WeightTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CaptureWeight()
        {
            try
            {
                /// User validation *
                /// 
                if (CheckBarcodeInfo(BaleBarcodeTextBox.Text) == false)
                    return;

                if (BaleBarCodeTextBlock.Text == "")
                {
                    MessageBox.Show("ข้อมูลเกี่ยวกับห่อยาไม่แสดงบนหน้าจอ โปรดลองสแกนบาร์โค้ตใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ClearForm();
                    return;
                }

                if (WeightTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุน้ำหนักห่อยา", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (_buying.Weight != null)
                {
                    if (MessageBox.Show("ยาห่อนี้ถูกบันทึกน้ำหนักซื้อไปก่อนหน้านี้แล้ว ท่านต้องการแก้ไขน้ำหนักซื้อใช่หรือไม่", "การแจ้งเตือน", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        return;
                }

                /// User validation **
                
                /// Create buying object for update info **
                /// 
                BuyingDocument buyingDoc = new BuyingDocument
                {
                    Crop = _buyingDocumentInfo.Crop,
                    BuyingStationCode = _buyingDocumentInfo.BuyingStationCode,
                    FarmerCode = _buyingDocumentInfo.FarmerCode,
                    BuyingDocumentNumber = _buyingDocumentInfo.BuyingDocumentNumber,
                    FinishDate = _buyingDocumentInfo.FinishDate,
                    IsFinish = _buyingDocumentInfo.IsFinish
                };

                BuyingFacade.BuyingBL().CaptureWeight(BaleBarcodeTextBox.Text,
                    Convert.ToDecimal(WeightTextBox.Text), 
                    _config.currentUser);
                
                /// Create buying object for update info **.
                
                SearchBuyingInfoFromBaleBarcode(BaleBarCodeTextBlock.Text);
                BaleBarcodeTextBox.Text = "";
                BaleBarcodeTextBox.Focus();

                BaleBarCodeTextBlock.Text = "";
                ProjectTypeTextBlock.Text = "";
                IsExtraTextBlock.Text = "";
                TransportationCodeTextBlock.Text = "";
                GradeTextBlock.Text = "";
                WeightTextBlock.Text = "";

                ////ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BaleBarcodeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter && BaleBarcodeTextBox.Text.Length == 17)
                    SearchBuyingInfoFromBaleBarcode(BaleBarcodeTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CaptureWeight();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearFormButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void IsDigitalScaleCheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (WeightTextBox.IsReadOnly == true)//หมายถึง สั่งให้มีการรับค่าจากคีย์บอร์ดแทนการรับค่าจากเครื่องชั่งดิจิตอล ให้ปิดการทำงานของ Com port
                {
                    if (serialPort.IsOpen)
                    {
                        Thread CloseDown = new Thread(new ThreadStart(CloseSerialNonExit)); //close port in new thread to avoid hang

                        CloseDown.Start(); //close port in new thread to avoid hang
                    }
                    
                    IsDigitalScaleCheckBox.IsChecked = true;
                    WeightTextBox.Text = "";
                    WeightTextBox.IsReadOnly = false;
                }
                else//หมายถึง ให้มีการรับค่าจากเครื่องชั่งดิจิตอล แทนการรับค่าจากคีย์บอร์ด จึงสั่งให้โปรแกรมเปิดการเชื่อมต่อของ Com port เพื่อรับข้อมูลจากเครื่องชั่งดิจิตอล
                {
                    WeightTextBox.Text = "";
                    WeightTextBox.IsReadOnly = true;

                    IsDigitalScaleCheckBox.IsChecked = false;

                    SetupSerialPort();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ViewDetailsButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (WeightTextBox.IsReadOnly == true)//หมายถึง สั่งให้มีการรับค่าจากคีย์บอร์ดแทนการรับค่าจากเครื่องชั่งดิจิตอล ให้ปิดการทำงานของ Com port
                {
                    if (serialPort.IsOpen)
                    {
                        Thread CloseDown = new Thread(new ThreadStart(CloseSerialNonExit)); //close port in new thread to avoid hang

                        CloseDown.Start(); //close port in new thread to avoid hang
                    }

                    WeightTextBox.Text = "";
                    WeightTextBox.IsReadOnly = false;

                    IsDigitalScaleCheckBox.IsChecked = true;
                }
                else//หมายถึง ให้มีการรับค่าจากเครื่องชั่งดิจิตอล แทนการรับค่าจากคีย์บอร์ด จึงสั่งให้โปรแกรมเปิดการเชื่อมต่อของ Com port เพื่อรับข้อมูลจากเครื่องชั่งดิจิตอล
                {
                    WeightTextBox.Text = "";
                    WeightTextBox.IsReadOnly = true;

                    IsDigitalScaleCheckBox.IsChecked = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        
        private void WeightTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter && IsDigitalScaleCheckBox.IsChecked == true && WeightTextBox.Text != "")
                    CaptureWeight();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Escape)
                    ClearForm();

                if (e.Key == Key.F12)
                    CaptureWeight();

                if (e.Key == Key.Enter && GradeTextBlock.Text != "" && BaleBarcodeTextBox.Text.Length == 17)
                    CaptureWeight();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                SetupSerialPort();

                if(System.Windows.Forms.Screen.AllScreens.Length > 1)
                {
                    var screen2 = System.Windows.Forms.Screen.AllScreens[1];
                    var rectangle2 = screen2.WorkingArea;

                    monitor2.Top = rectangle2.Top;
                    monitor2.Left = rectangle2.Left;

                    monitor2.Show();
                }

                BaleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ChangeBuyingGradeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CheckBarcodeInfo(BaleBarcodeTextBox.Text) == false)
                    return;

                if (_registrationFarmer == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลการลงทะเบียนในระบบ โปรดตรวจสอบข้อมูลอีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (_buyingDocumentInfo == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลเกี่ยวกับใบรายการซื้อขายในระบบ โปรดตรวจสอบข้อมูลอีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                ChangeBuyingGradeAtWeight changeGrade = new ChangeBuyingGradeAtWeight(BaleBarcodeTextBox.Text, 
                    _registrationFarmer, 
                    _buyingDocumentInfo);
                changeGrade.ShowDialog();

                SearchBuyingInfoFromBaleBarcode(BaleBarcodeTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RejectAtWeightScaleButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BaleBarCodeTextBlock.Text == "")
                {
                    MessageBox.Show("ข้อมูลเกี่ยวกับห่อยาไม่แสดงบนหน้าจอ โปรดลองสแกนบาร์โค้ตใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ClearForm();
                    return;
                }

                if (MessageBox.Show("การยกเลิกห่อยา (Reject) อาจส่งผลกระทบต่อข้อมูลการซื้อขาย โปรดแจ้งผู้ควบคุมการซื้อขายเพื่อทำการยกเลิกห่อยานี้ "+ 
                    Environment.NewLine +"ท่านต้องการยกเลิกห่อยานี้หรือไม่? หากใช่ กด Yes และทำการยืนยันตัวตน", "การแจ้งเตือน", 
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                RejectPopupAtWeightScale rejectOnTheScale = new RejectPopupAtWeightScale(_buying, _roleName);
                rejectOnTheScale.ShowDialog();

                SearchBuyingInfoFromBaleBarcode(BaleBarcodeTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UsernameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text.Length > 0 && PasswordTextBox.Password.Length > 0)
                    SubmitButton.IsEnabled = true;
                else
                    SubmitButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PasswordTextBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text.Length > 0 && PasswordTextBox.Password.Length > 0)
                    SubmitButton.IsEnabled = true;
                else
                    SubmitButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text.Length <= 0)
                {
                    MessageBox.Show("โปรดกรอกชื่อผู้ใช้", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (PasswordTextBox.Password.Length <= 0)
                {
                    MessageBox.Show("โปรดกรอกรหัสผ่าน", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                UserAccount user = new UserAccount();

                user = BuyingFacade.UserBL().GetByUsername(UsernameTextBox.Text);

                if (user == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลบัญชีผู้ใช้นี้ในระบบ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);

                    UsernameTextBox.Focus();
                    return;
                }

                if (user.Password != PasswordTextBox.Password)
                {
                    MessageBox.Show("รหัสผ่านไม่ถูกต้อง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);

                    PasswordTextBox.Password = "";
                    PasswordTextBox.Focus();
                    return;
                }

                _roleName = (from ur in BuyingFacade.UserRoleBL().GetByUser(user.Username)
                             from r in BuyingFacade.RoleBL().GetAll()
                             where ur.RoleID == r.RoleID
                             select new Role
                             {
                                 RoleID = ur.RoleID,
                                 RoleName = r.RoleName
                             }).ToList();


                if (_roleName.Where(r => r.RoleName == "Supervisor").Count() <= 0)
                {
                    MessageBox.Show("สิทธิ์ของบัญชีผู้ใช้นี้ไม่พบสิทธิ์ในระดับ Supervisor ไม่สามารถยกเลิกห่อยา (Reject) นี้ได้", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                SupervisorApprovePopup.IsOpen = false;

                RejectPopupAtWeightScale rejectOnTheScale = new RejectPopupAtWeightScale(_buying, _roleName);
                rejectOnTheScale.ShowDialog();

                _roleName = null;

                SearchBuyingInfoFromBaleBarcode(BaleBarcodeTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UsernameTextBox.Text = "";
                PasswordTextBox.Password = "";
                UsernameTextBox.Focus();

                _roleName = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _roleName = null;
                SupervisorApprovePopup.IsOpen = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingGradeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            monitor2.GradeLabel.Content = BuyingGradeComboBox.SelectedValue;
        }
    }
}
