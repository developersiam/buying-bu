﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BusinessLayer.Model;
using BusinessLayer.Helper;
using BUWinApp.Helper;

namespace BUWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for SearchBuyingDocumentForPrintVoucher.xaml
    /// </summary>
    public partial class BuyingVouchers : Page
    {
        public BuyingVouchers()
        {
            InitializeComponent();

            BuyingStationComboBox.ItemsSource = null;
            BuyingStationComboBox.ItemsSource = BuyingFacade.BuyingStationBL()
                .GetByArea(user_setting.User.StaffUser.AreaCode)
                .OrderBy(x => x.StationCode);

            BuyingDatePicker.SelectedDate = DateTime.Now;
        }

        private void DataGridBinding()
        {
            try
            {
                if (BuyingDatePicker.SelectedDate == null)
                    return;

                if (BuyingStationComboBox.SelectedIndex < 0)
                    return;

                var buyingDocumentInfoList = BuyingDocumentHelper
                        .GetByStationAndBuyingDate(Convert.ToDateTime(BuyingDatePicker.SelectedDate),
                        BuyingStationComboBox.SelectedValue.ToString())
                        .ToList();

                VoucherListDataGrid.ItemsSource = null;
                VoucherListDataGrid.ItemsSource = FarmerNameTextBox.Text.Length <= 0 ?
                    buyingDocumentInfoList :
                    buyingDocumentInfoList.Where(x => x.FirstName.Contains(FarmerNameTextBox.Text));

                totalItemLabel.Content = VoucherListDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ViewVoucherDetailsButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BuyingStationComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("โปรดระบุลานรับซื้อ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (BuyingDatePicker.SelectedDate.ToString() == "")
                {
                    MessageBox.Show("โปรดระบุวันที่ซื้อขาย", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var document = (m_BuyingDocument)VoucherListDataGrid.SelectedItem;
                BuyingVoucherDetails window = new BuyingVoucherDetails(document);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGridBinding();
        }

        private void BuyingStationComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGridBinding();
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            DataGridBinding();
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FarmerNameTextBox.Clear();
                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
