﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BusinessLayer.Model;
using BusinessLayer.Helper;
using BUBuyingSystemWinApp.Helper;

namespace BUBuyingSystemWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for CaptureBuyingGrade.xaml
    /// </summary>
    public partial class CaptureBuyingGrade : Window
    {
        PricingSet _pricingSet;
        m_BuyingDocument _buyingDocumentInfo;
        RegistrationFarmer _registrationFarmer;
        Buying _buying;

        struct BuyingGradeWigtPrice
        {
            public short PricingCrop { get; set; }
            public short PricingNumber { get; set; }
            public decimal UnitPrice { get; set; }
            public string Grade { get; set; }
            public string GradeWithPrice { get; set; }
        }

        public CaptureBuyingGrade()
        {
            InitializeComponent();

            _pricingSet = new PricingSet();
            _buyingDocumentInfo = new m_BuyingDocument();
            _registrationFarmer = new RegistrationFarmer();
            _buying = new Buying();

            BaleBarcodeTextBox.Focus();

            BuyerComboBox.ItemsSource = BuyingFacade.BuyerBL().GetAll().OrderBy(x => x.BuyerCode);


            //Set ค่าให้กับ object เพื่อเอาไว้ใช้เป็นพารามิเตอร์หลักในการบันทึกเกรด
            _pricingSet = BuyingFacade.PricingSetBL().GetByDefault();


            PricingCropTextBlock.Text = _pricingSet.Crop.ToString();
            PricingNumberTextBlock.Text = _pricingSet.PricingNumber.ToString();
        }
        
        private void ClearForm()
        {
            FarmerCodeTextBlock.Text = "";
            FarmerNameTextBlock.Text = "";
            AddressTextBlock.Text = "";
            ContractIDTextBlock.Text = "";
            //EstimateRaiTextBlock.Text = "";
            GAPGroupCodeTextBox.Text = "";
            ContractStatusCheckBox.IsChecked = false;

            BaleBarcodeTextBox.Text = "";
            BaleBarcodeTextBox.Focus();

            DocumentCodeTextBlock.Text = "";
            TotalBaleTextBlock.Text = "";
            TotalScanTextBlock.Text = "";
            TotalRejectTextBlock.Text = "";
            FinishStatusCheckBox.IsChecked = false;

            GradeTextBlock.Text = "";
        }

        private void BuyingDocumentDetailsDataBinding(Buying buying)
        {
            try
            {
                //Set ค่าให้กับ object เพื่อเอาไว้ใช้เป็นพารามิเตอร์หลักในการบันทึกเกรด
                _buyingDocumentInfo = BuyingDocumentHelper.GetSingle(buying.Crop, 
                    buying.BuyingStationCode, 
                    buying.FarmerCode, 
                    buying.BuyingDocumentNumber);

                
                DocumentCodeTextBlock.Text = _buyingDocumentInfo.DocumentCode;
                TotalBaleTextBlock.Text = _buyingDocumentInfo.TotalBale.ToString("N0");
                TotalScanTextBlock.Text = _buyingDocumentInfo.TotalScan.ToString("N0");
                TotalRejectTextBlock.Text = _buyingDocumentInfo.TotalReject.ToString();
                FinishStatusCheckBox.IsChecked = _buyingDocumentInfo.IsFinish;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FarmerRegistrationInfoDataBinding(string baleBarcode)
        {
            try
            {
                if (baleBarcode == "")
                {
                    MessageBox.Show("โปรดสแกนบาร์โค้ตเพื่อเริ่มต้นค้นหาข้อมูล", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    return;
                }


                //Set ค่าให้กับ object เพื่อเอาไว้ใช้เป็นพารามิเตอร์หลักในการบันทึกเกรด
                _buying = BuyingFacade.BuyingBL().GetByBaleBarcode(baleBarcode);

                
                if (_buying == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลยาห่อนี้ในระบบ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


                if(_pricingSet == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลการกำหนดชุดราคาให้กับเกรดซื้อในระบบ จะทำให้ไม่สามารถบันทึกเกรดซื้อ โปรดติดต่อแผนกไอที", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


                if(_buying.Grade == null && _buying.RejectReason == null)
                {
                    GradeTextBlock.Text = "N/A";
                    PricingCropTextBlock.Text = _pricingSet.Crop.ToString();
                    PricingNumberTextBlock.Text = _pricingSet.PricingNumber.ToString();
                }
                else if (_buying.Grade == null && _buying.RejectReason != null)
                {
                    GradeTextBlock.Text = _buying.RejectReason;
                    PricingCropTextBlock.Text = _buying.PricingCrop.ToString();
                    PricingNumberTextBlock.Text = _buying.PricingNumber.ToString();
                }
                else
                {
                    GradeTextBlock.Text = _buying.Grade;
                    BuyerComboBox.SelectedValue = _buying.BuyerCode;
                    PricingCropTextBlock.Text = _buying.PricingCrop.ToString();
                    PricingNumberTextBlock.Text = _buying.PricingNumber.ToString();
                }


                BuyingDocumentDetailsDataBinding(_buying);                


                //Set ค่าให้กับ object เพื่อเอาไว้ใช้เป็นพารามิเตอร์หลักในการบันทึกเกรด
                _registrationFarmer = BuyingFacade.RegistrationBL().GetFarmerAndCrop(_buying.Crop, _buying.FarmerCode);
                

                FarmerCodeTextBlock.Text = "";
                FarmerNameTextBlock.Text = "";
                AddressTextBlock.Text = "";
                ContractIDTextBlock.Text = "";
                //EstimateRaiTextBlock.Text = "";
                GAPGroupCodeTextBox.Text = "";
                ContractStatusCheckBox.IsChecked = false;


                FarmerCodeTextBlock.Text = _registrationFarmer.FarmerCode;
                FarmerNameTextBlock.Text = _registrationFarmer.Farmer.Person.Prefix + _registrationFarmer.Farmer.Person.FirstName + " " + _registrationFarmer.Farmer.Person.LastName;
                AddressTextBlock.Text = _registrationFarmer.Farmer.Person.HouseNumber +
                    " หมู่ " + _registrationFarmer.Farmer.Person.Village +
                    " ต." + _registrationFarmer.Farmer.Person.Tumbon +
                    " อ." + _registrationFarmer.Farmer.Person.Amphur +
                    " จ." + _registrationFarmer.Farmer.Person.Province;


                ContractIDTextBlock.Text = _registrationFarmer.GAPContractCode;
                //EstimateRaiTextBlock.Text = _registrationFarmer.EstimatRai.ToString("N0");
                GAPGroupCodeTextBox.Text = _registrationFarmer.GAPGroupCode;
                ContractStatusCheckBox.IsChecked = _registrationFarmer.ActiveStatus;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingGradeItemsControlDataBinging(string gradeGroup)
        {
            try
            {
                if (gradeGroup == "")
                {
                    MessageBox.Show("โปรดเลือกกลุ่มของเกรดที่ต้องการ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                List<BuyingGradeWigtPrice> buyingGradeWithPriceList = new List<BuyingGradeWigtPrice>();

                foreach (BuyingGrade item in BuyingFacade.BuyingGradeBL().GetByPricingSet(_pricingSet.Crop, _pricingSet.PricingNumber).Where(x => x.Grade.Substring(0, 1) == gradeGroup))
                {
                    buyingGradeWithPriceList.Add(new BuyingGradeWigtPrice
                    {
                        PricingCrop = item.PricingCrop,
                        PricingNumber = item.PricingNumber,
                        Grade = item.Grade,
                        GradeWithPrice = item.Grade + "\n(" + item.UnitPrice.ToString("N0") + ")",
                        UnitPrice = item.UnitPrice
                    });
                }

                BuyingGradeItemsControl.ItemsSource = null;
                BuyingGradeItemsControl.ItemsSource = buyingGradeWithPriceList.OrderBy(x => x.Grade);


                //List<BuyingGrade> buyingGradeList = new List<BuyingGrade>();
                //buyingGradeList = BuyingFacade.BuyingGradeBL().GetBuyingGradeByPricingSet(_pricingSet.Crop, _pricingSet.PricingNumber);


                //BuyingGradeItemsControl.ItemsSource = null;
                //BuyingGradeItemsControl.ItemsSource = buyingGradeList.Where(x => x.Grade.Substring(0, 1) == gradeGroup).OrderBy(x => x.Grade);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CheckBarcodeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                FarmerRegistrationInfoDataBinding(BaleBarcodeTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void BuyingGradeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = (Button)sender;

                if (BaleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดสแกนรหัสบาร์โค้ต", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    return;
                }

                if (BuyerComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดระบุผู้ซื้อ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (_pricingSet == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลการกำหนดชุดราคาให้กับเกรดซื้อในระบบ จะทำให้ไม่สามารถบันทึกเกรดซื้อ โปรดติดต่อแผนกไอที", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                BuyingDocument buyingDocument = new BuyingDocument{
                    Crop = _buyingDocumentInfo.Crop,
                    BuyingStationCode = _buyingDocumentInfo.BuyingStationCode,
                    FarmerCode = _buyingDocumentInfo.FarmerCode,
                    BuyingDocumentNumber = _buyingDocumentInfo.BuyingDocumentNumber,
                    IsFinish = _buyingDocumentInfo.IsFinish
                };

                BuyingFacade.BuyingBL().CaptureGrade(BaleBarcodeTextBox.Text,
                    _pricingSet.Crop,
                    _pricingSet.PricingNumber,
                    btn.ToolTip.ToString(),
                    BuyerComboBox.SelectedValue.ToString(),
                    user_setting.User.Username);

                LastBaleBarcodeTextBlock.Text = BaleBarcodeTextBox.Text;
                LastBuyingGradeTextBlock.Text = btn.ToolTip.ToString();
                LastCaptureDateTextBlock.Text = DateTime.Now.ToShortDateString() + DateTime.Now.ToShortTimeString();
                LastCaptureUserTextBlock.Text = user_setting.User.Username;

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void XGradeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BuyingGradeItemsControlDataBinging(XGradeButton.Content.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CGradeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BuyingGradeItemsControlDataBinging(CGradeButton.Content.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BGradeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BuyingGradeItemsControlDataBinging(BGradeButton.Content.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void TGradeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BuyingGradeItemsControlDataBinging(TGradeButton.Content.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RejectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BaleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดสแกนบาร์โค้ตเพื่อเริ่มต้นค้นหาข้อมูล", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    return;
                }

                if (_buying == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลยาห่อนี้ในระบบ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                
                if (BuyerComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดระบุรายชื่อผู้ซื้อ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                RejectPopup rejectPopup = new RejectPopup(_buying, BuyerComboBox.SelectedValue.ToString());
                rejectPopup.ShowDialog();

                LastBaleBarcodeTextBlock.Text = BaleBarcodeTextBox.Text;
                LastBuyingGradeTextBlock.Text = "Back";
                LastCaptureDateTextBlock.Text = DateTime.Now.ToShortDateString() + DateTime.Now.ToShortTimeString();
                LastCaptureUserTextBlock.Text = user_setting.User.Username;

                ClearForm();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BaleBarcodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Escape)
                    ClearForm();


                if(e.Key == Key.Enter)
                {
                    FarmerRegistrationInfoDataBinding(BaleBarcodeTextBox.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void NTRMButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BaleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดสแกนบาร์โค้ตเพื่อเริ่มต้นค้นหาข้อมูล", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    return;
                }

                if (_buying == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลยาห่อนี้ในระบบ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                NTRMInspectionPopup ntrmPopup = new NTRMInspectionPopup(BaleBarcodeTextBox.Text);
                ntrmPopup.ShowDialog();

                LastBaleBarcodeTextBlock.Text = BaleBarcodeTextBox.Text;
                LastBuyingGradeTextBlock.Text = "Back";
                LastCaptureDateTextBlock.Text = DateTime.Now.ToShortDateString() + DateTime.Now.ToShortTimeString();
                LastCaptureUserTextBlock.Text = user_setting.User.Username;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void NGradeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BuyingGradeItemsControlDataBinging(NGradeButton.Content.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
