﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BusinessLayer.Helper;
using BusinessLayer.Model;
using BUBuyingSystemWinApp.Helper;

namespace BUBuyingSystemWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for ManageTrasportationDocument.xaml
    /// </summary>
    public partial class ManageTrasportationDocument : Window
    {
        public ManageTrasportationDocument()
        {
            InitializeComponent();

            CreateDatePicker.SelectedDate = DateTime.Now;

            StationComboBox.ItemsSource = null;
            StationComboBox.ItemsSource = BuyingFacade.BuyingStationBL()
                .GetByArea(user_setting.User.StaffUser.AreaCode)
                .OrderBy(x => x.StationCode);
        }

        private void TransportationDataGridBinding()
        {
            try
            {
                if (StationComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดเลือกลานรับซื้อและกด Display เพื่อแสดงรายการข้อมูล", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (CreateDatePicker.SelectedDate.ToString() == "")
                {
                    MessageBox.Show("โปรดเลือกลานรับซื้อและกด Display เพื่อแสดงรายการข้อมูล", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                TransportationDataGrid.ItemsSource = null;
                TransportationDataGrid.ItemsSource = TransportationHelper
                    .GetByStationAndCrop(Convert.ToInt16(Convert.ToDateTime(CreateDatePicker.SelectedDate).Year),
                    StationComboBox.SelectedValue.ToString(),
                    Convert.ToDateTime(CreateDatePicker.SelectedDate))
                    .OrderByDescending(t => t.TransportationDocumentCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DisplayButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TransportationDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (StationComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดเลือกลานรับซื้อที่ต้องการสร้างใบนำส่ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                TransportationDocuments createTransportationDocument = new TransportationDocuments();
                createTransportationDocument.ShowDialog();

                TransportationDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void StartLoadingButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (TransportationDataGrid.SelectedIndex <= -1)
                    return;

                var document = (m_TransportationDocument)TransportationDataGrid.SelectedItem;

                LoadBaleToTruck loadBaleToTruck = new LoadBaleToTruck(document.TransportationDocumentCode);
                loadBaleToTruck.ShowDialog();

                TransportationDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (TransportationDataGrid.SelectedIndex <= -1)
                    return;

                var model = (m_TransportationDocument)TransportationDataGrid.SelectedItem;

                if (MessageBox.Show("ท่านต้องการลบใบนำส่งหมายเลข " + model.TransportationDocumentCode + 
                    " นี้ใช่หรือไม่", "การแจ้งเตือน", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BuyingFacade.TransportationBL().Delete(model.TransportationDocumentCode);

                TransportationDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void MoveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (StationComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดเลือกลานรับซื้อที่ต้องการสร้างใบรายการซื้อขายก่อน", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (CreateDatePicker.SelectedDate.ToString() == "")
                {
                    MessageBox.Show("โปรดเลือกลานรับซื้อและกด Display เพื่อแสดงรายการข้อมูล", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MovingTheBaleBetweenTheTruck moving = 
                    new MovingTheBaleBetweenTheTruck(user_setting.Crop.Crop1, 
                    StationComboBox.SelectedValue.ToString(),
                    Convert.ToDateTime(CreateDatePicker.SelectedDate));
                moving.ShowDialog();

                TransportationDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (TransportationDataGrid.SelectedIndex <= -1)
                    return;

                var model = (m_TransportationDocument)TransportationDataGrid.SelectedItem;

                EditTransportationDocument window = new EditTransportationDocument(model);
                window.ShowDialog();

                TransportationDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
