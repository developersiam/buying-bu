﻿using BUBuyingSystemWinApp.Helper;
using BusinessLayer;
using BusinessLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BUBuyingSystemWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for EditTransportationDocument.xaml
    /// </summary>
    public partial class EditTransportationDocument : Window
    {
        m_TransportationDocument _model;
        public EditTransportationDocument(m_TransportationDocument model)
        {
            InitializeComponent();
            _model = model;
            Reset();
        }

        private void Reset()
        {
            try
            {
                TransportationDocumentNoTextBox.Text = _model.TransportationDocumentCode;
                CreateDateDatePicker.SelectedDate = _model.CreateDate;
                CreatByTextBox.Text = _model.CreateUser;
                TruckNoTextBox.Text = _model.TruckNumber;
                MaximumWeightTextBox.Text = _model.MaximumWeight.ToString("D0");
                TruckNoTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var transportationList = BuyingFacade.BuyingBL()
                    .GetByTransportationCode(_model.TransportationDocumentCode);

                if (Convert.ToDecimal(MaximumWeightTextBox.Text) < transportationList.Sum(x => x.Weight))
                {
                    MessageBox.Show("Maximum weight ที่แก้ไขใหม่นี้ น้อยกว่าน้ำหนักรวมที่สแกนขึ้นรถคันนี้ไปแล้ว โปรดตรวจสอบอีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MaximumWeightTextBox.Text = "";
                    MaximumWeightTextBox.Focus();
                    return;
                }

                BuyingFacade.TransportationBL()
                    .Update(_model.TransportationDocumentCode,
                    TruckNoTextBox.Text,
                    Convert.ToInt16(MaximumWeightTextBox.Text),
                    user_setting.User.Username, 
                    Convert.ToDateTime(CreateDateDatePicker.Text));

                MessageBox.Show("บันทึกสำเร็จ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Reset();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
