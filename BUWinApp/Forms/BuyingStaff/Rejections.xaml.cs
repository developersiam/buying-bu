﻿using BUWinApp.Helper;
using BusinessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BUWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for Rejections.xaml
    /// </summary>
    /// 
    public partial class Rejections : Window
    {
        Buying _buying;

        public Rejections()
        {
            InitializeComponent();

            _buying = new Buying();

            RejectReasonItemsControl.ItemsSource = null;
            RejectReasonItemsControl.ItemsSource = user_setting.RejectReasons;

            BuyerComboBox.ItemsSource = null;
            BuyerComboBox.ItemsSource = BuyingFacade.BuyerBL()
                .GetAll()
                .OrderBy(x => x.BuyerCode);

            BaleBarcodeTextBox.Focus();
            BaleBarcodeTextBox.Clear();
        }

        private void BuyingDataBinding()
        {
            try
            {
                if (BaleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุรหัสป้ายบาร์โค้ต", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    return;
                }

                _buying = BuyingFacade.BuyingBL().GetByBaleBarcode(BaleBarcodeTextBox.Text);

                if (_buying == null)
                    throw new ArgumentException("ไม่พบข้อมูลห่อยารหัส" + BaleBarcodeTextBox.Text + " โปรดสแกนบาร์โค้ตใหม่อีกครั้ง");

                FarmerCodeTextBox.Text = _buying.FarmerCode;
                BuyerComboBox.SelectedValue = _buying.BuyerCode;
                ProjectTypeTextBox.Text = _buying.ProjectType;
                RegisterBarcodeDateTextBox.Text = _buying.RegisterBarcodeDate.ToString();
                BuyingWeightDateTextBox.Text = _buying.WeightDate.ToString();
                GradeTextBox.Text = _buying.Grade;
                WeightTextBox.Text = Convert.ToDecimal(_buying.Weight).ToString("N1");
                TransportationCodeTextBox.Text = _buying.TransportationDocumentCode;
                IsExtraQuotaCheckBox.IsChecked = _buying.IsExtraQuota;
                RejectReasonTextBox.Text = _buying.RejectReason;

                if (_buying.RejectReason != null)
                {
                    RejectReasonTextBox.Visibility = Visibility.Visible;
                    CancelRejectionButton.Visibility = Visibility.Visible;
                }
                else
                {
                    RejectReasonTextBox.Visibility = Visibility.Collapsed;
                    CancelRejectionButton.Visibility = Visibility.Collapsed;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BaleBarcodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                BuyingDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearForm()
        {
            _buying = null;

            FarmerCodeTextBox.Clear();
            ProjectTypeTextBox.Clear();
            RegisterBarcodeDateTextBox.Clear();
            BuyingWeightDateTextBox.Clear();
            GradeTextBox.Clear();
            WeightTextBox.Clear();
            TransportationCodeTextBox.Clear();
            IsExtraQuotaCheckBox.IsChecked = false;
            RejectReasonTextBox.Visibility = Visibility.Collapsed;

            BaleBarcodeTextBox.Focus();
            BaleBarcodeTextBox.Clear();
        }

        private void ClearFormButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void RejectReasonButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ท่านต้องการแบ็คยาห่อนี้ใช่หรือไม่?",
                    "warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                if (BaleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุรหัสป้ายบาร์โค้ต", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    return;
                }

                if (_buying == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลห่อยารหัส" + BaleBarcodeTextBox.Text +
                        " โปรดสแกนบาร์โค้ตใหม่อีกครั้ง", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    BaleBarcodeTextBox.SelectAll();
                    return;
                }

                if (BuyerComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("โปรดระบุชื่อผู้ซื้อ", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BuyerComboBox.Focus();
                    return;
                }

                var _buyingDocument = BuyingFacade.BuyingDocumentBL()
                    .GetSingle(_buying.Crop,
                    _buying.BuyingStationCode,
                    _buying.FarmerCode,
                    _buying.BuyingDocumentNumber);

                if (_buyingDocument == null)
                    throw new ArgumentException("ไม่พบข้อมูลห่อยารหัส" + BaleBarcodeTextBox.Text + " โปรดสแกนบาร์โค้ตใหม่อีกครั้ง");

                if (_buyingDocument.IsFinish == true)
                    throw new ArgumentException("ใบเวาเชอร์ของยาห่อนี้ถูก Finish ไปแล้ว ไม่สามารถ reject ยาห่อนี้ได้");

                if (_buying.Grade != null)
                {
                    if (MessageBox.Show("ยาห่อนี้มีเกรดซื้อแล้ว ท่านต้องการ Reject หรือไม่?",
                    "warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        return;
                }

                Button btn = (Button)sender;
                BuyingFacade.BuyingBL().RejectBale(BaleBarcodeTextBox.Text,
                    btn.Content.ToString(),
                    user_setting.User.Username,
                    BuyerComboBox.SelectedValue.ToString());

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CancelRejectionButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ท่านต้องการยกเลิกการแบ็คยาห่อนี้ใช่หรือไม่?",
                      "warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                if (BaleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุรหัสป้ายบาร์โค้ต", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    return;
                }

                if (_buying == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลห่อยารหัส" + BaleBarcodeTextBox.Text +
                        " โปรดสแกนบาร์โค้ตใหม่อีกครั้ง", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    BaleBarcodeTextBox.SelectAll();
                    return;
                }

                var _buyingDocument = BuyingFacade.BuyingDocumentBL()
                    .GetSingle(_buying.Crop,
                    _buying.BuyingStationCode,
                    _buying.FarmerCode,
                    _buying.BuyingDocumentNumber);

                if (_buyingDocument == null)
                    throw new ArgumentException("ไม่พบข้อมูลห่อยารหัส" + BaleBarcodeTextBox.Text + " โปรดสแกนบาร์โค้ตใหม่อีกครั้ง");

                if (_buyingDocument.IsFinish == true)
                    throw new ArgumentException("ใบเวาเชอร์ของยาห่อนี้ถูก Finish ไปแล้ว ไม่สามารถ reject ยาห่อนี้ได้");
                
                BuyingFacade.BuyingBL().RemoveRejectBale(BaleBarcodeTextBox.Text);

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
