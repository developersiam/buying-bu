﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BUBuyingSystemWinApp.Helper;

namespace BUBuyingSystemWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for RejectPopup.xaml
    /// </summary>
    public partial class RejectPopup : Window
    {
        string _baleBarcode;
        string _buyerCode;
        Buying _buying;

        public RejectPopup(Buying buying, string buyerCode)
        {
            InitializeComponent();
            _buying = new Buying();

            _buying = buying;
            _baleBarcode = _buying.BaleBarcode;
            _buyerCode = buyerCode;

            List<RejectReasonStruct> rejectReasonList = new List<RejectReasonStruct>();

            rejectReasonList.Add(new RejectReasonStruct { RejectReason = "Mix Grade (ปน)" });
            rejectReasonList.Add(new RejectReasonStruct { RejectReason = "Moisture (ชื้น)" });
            rejectReasonList.Add(new RejectReasonStruct { RejectReason = "Buyer Reject (ไม่ซื้อ)" });
            rejectReasonList.Add(new RejectReasonStruct { RejectReason = "X low grade over limit" });
            rejectReasonList.Add(new RejectReasonStruct { RejectReason = "Farmer Reject (ไม่ขาย)" });
            rejectReasonList.Add(new RejectReasonStruct { RejectReason = "Green (เขียว)" });
            rejectReasonList.Add(new RejectReasonStruct { RejectReason = "Yellow (เหลือง)" });
            rejectReasonList.Add(new RejectReasonStruct { RejectReason = "NTRM" });
            rejectReasonList.Add(new RejectReasonStruct { RejectReason = "Nesting (ยัดไส้)" });

            RejectReasonItemsControl.ItemsSource = null;
            RejectReasonItemsControl.ItemsSource = rejectReasonList;
        }

        struct RejectReasonStruct {
            public string RejectReason { get; set; }
        }

        private void RejectReasonButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = (Button)sender;

                BuyingFacade.BuyingBL().RejectBale(_buying.BaleBarcode, 
                    btn.Content.ToString(), 
                    user_setting.User.Username,
                    _buyerCode);

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
