﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BusinessLayer.Model;
using BUBuyingSystemWinApp.Helper;

namespace BUBuyingSystemWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for ChangeBuyingGradeAtWeight.xaml
    /// </summary>
    public partial class ChangeBuyingGradeAtWeight : Window
    {
        PricingSet _pricingSet;
        RegistrationFarmer _registrationFarmer;
        Buying _buying;
        m_BuyingDocument _buyingDocumentInfo;

        string _baleBarcode;

        public ChangeBuyingGradeAtWeight(string baleBarcode, RegistrationFarmer registrationFarmer, m_BuyingDocument buyingDocumentInfo)
        {
            InitializeComponent();

            _pricingSet = new PricingSet();
            _registrationFarmer = new RegistrationFarmer();
            _buying = new Buying();
            _buyingDocumentInfo = new m_BuyingDocument();

            _registrationFarmer = registrationFarmer;
            _baleBarcode = baleBarcode;
            _buyingDocumentInfo = buyingDocumentInfo;


            BuyerComboBox.ItemsSource = BuyingFacade.BuyerBL().GetAll().OrderBy(x => x.BuyerCode);


            //Set ค่าให้กับ object เพื่อเอาไว้ใช้เป็นพารามิเตอร์หลักในการบันทึกเกรด
            _pricingSet = BuyingFacade.PricingSetBL().GetByDefault();


            PricingCropTextBlock.Text = _pricingSet.Crop.ToString();
            PricingNumberTextBlock.Text = _pricingSet.PricingNumber.ToString();
        }

        private void BuyingGradeItemsControlDataBinging(string gradeGroup)
        {
            try
            {
                if (gradeGroup == "")
                {
                    MessageBox.Show("โปรดเลือกกลุ่มของเกรดที่ต้องการ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                List<BuyingGrade> buyingGradeList = new List<BuyingGrade>();
                buyingGradeList = BuyingFacade.BuyingGradeBL().GetByPricingSet(_pricingSet.Crop, _pricingSet.PricingNumber);
                
                BuyingGradeItemsControl.ItemsSource = null;
                BuyingGradeItemsControl.ItemsSource = buyingGradeList.Where(x => x.Grade.Substring(0, 1) == gradeGroup).OrderBy(x => x.Grade);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingGradeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = (Button)sender;

                if (_baleBarcode == "")
                {
                    MessageBox.Show("โปรดสแกนรหัสบาร์โค้ต", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (BuyerComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดระบุผู้ซื้อ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (_pricingSet == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลการกำหนดชุดราคาให้กับเกรดซื้อในระบบ จะทำให้ไม่สามารถบันทึกเกรดซื้อ โปรดติดต่อแผนกไอที", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (MessageBox.Show("ท่านต้องการแก้ไขข้อมูลเกรดซื้อ ณ จุดชั่งใช่หรือไม่?", "การแจ้งเตือน", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BuyingDocument buyingDocument = new BuyingDocument
                {
                    Crop = _buyingDocumentInfo.Crop,
                    BuyingStationCode = _buyingDocumentInfo.BuyingStationCode,
                    FarmerCode = _buyingDocumentInfo.FarmerCode,
                    BuyingDocumentNumber = _buyingDocumentInfo.BuyingDocumentNumber,
                    IsFinish = _buyingDocumentInfo.IsFinish
                };


                BuyingFacade.BuyingBL().ChangeGradeAtWeightScale(_baleBarcode,
                    _pricingSet.Crop,
                    _pricingSet.PricingNumber,
                    btn.Content.ToString(),
                    BuyerComboBox.SelectedValue.ToString(),
                    user_setting.User.Username);

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void XGradeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BuyingGradeItemsControlDataBinging(XGradeButton.Content.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CGradeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BuyingGradeItemsControlDataBinging(CGradeButton.Content.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BGradeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BuyingGradeItemsControlDataBinging(BGradeButton.Content.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void TGradeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BuyingGradeItemsControlDataBinging(TGradeButton.Content.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
