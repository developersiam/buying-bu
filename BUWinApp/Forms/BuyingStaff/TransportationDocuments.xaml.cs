﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BUWinApp.Helper;
using BusinessLayer.Model;

namespace BUWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for CreateTransportationDocument.xaml
    /// </summary>
    public partial class TransportationDocuments : Window
    {
        m_TransportationDocument _document;
        public TransportationDocuments()
        {
            InitializeComponent();

            _document = new m_TransportationDocument();

            BuyingStationComboBox.ItemsSource = null;
            BuyingStationComboBox.ItemsSource = BuyingFacade.BuyingStationBL()
                .GetByArea(user_setting.User.StaffUser.AreaCode);
        }

        private void DataGridBinding()
        {
            try
            {
                if (CreateDocumentDatePicker.SelectedDate == null)
                    return;

                if (BuyingStationComboBox.SelectedIndex < 0)
                    return;

                TransportationDataGrid.ItemsSource = null;
                TransportationDataGrid.ItemsSource = BusinessLayer
                    .Helper.TransportationHelper
                    .GetByCreateDate(user_setting.Crop.Crop1,
                    BuyingStationComboBox.SelectedValue.ToString(),
                    Convert.ToDateTime(CreateDocumentDatePicker.SelectedDate));

                totalItemLabel.Content = TransportationDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Clear()
        {
            try
            {
                CreateDocumentDatePicker.SelectedDate = DateTime.Now;
                TruckNoTextBox.Text = "";
                MaximumWeightTextBox.Text = "";
                TruckNoTextBox.Focus();
                TruckNoTextBox.SelectAll();

                _document = null;

                if (_document != null)
                {
                    AddButton.Visibility = Visibility.Collapsed;
                    EditButton.Visibility = Visibility.Visible;
                    TransportationCodeTextBox.Visibility = Visibility.Visible;
                }
                else
                {
                    AddButton.Visibility = Visibility.Visible;
                    EditButton.Visibility = Visibility.Collapsed;
                    TransportationCodeTextBox.Visibility = Visibility.Collapsed;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (TruckNoTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอกเลขทะเบียนรถ", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    TruckNoTextBox.Focus();
                    return;
                }

                if (CreateDocumentDatePicker.SelectedDate == null)
                {
                    MessageBox.Show("โปรดระบุวันที่สร้างเอกสาร", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    CreateDocumentDatePicker.Focus();
                    return;
                }

                if (MaximumWeightTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอกน้ำหนักที่ต้องการบรรทุก", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MaximumWeightTextBox.Focus();
                    return;
                }

                if (Helper.RegularExpressionHelper.IsNumericCharacter(MaximumWeightTextBox.Text) == false)
                {
                    MessageBox.Show("รูปแบบตัวเลขไม่ถูกต้อง", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MaximumWeightTextBox.Focus();
                    MaximumWeightTextBox.SelectAll();
                    return;
                }

                if (Convert.ToInt32(MaximumWeightTextBox.Text) <= 0)
                {
                    MessageBox.Show("น้ำหนักที่ต้องการบรรทุก ควรมากกว่า 0 ขึ้นไป", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    MaximumWeightTextBox.Focus();
                    MaximumWeightTextBox.SelectAll();
                    return;
                }

                string transportationCode = BuyingFacade.TransportationBL()
                    .Add(user_setting.Crop.Crop1,
                    BuyingStationComboBox.SelectedValue.ToString(),
                    Convert.ToDateTime(CreateDocumentDatePicker.SelectedDate),
                    user_setting.User.Username, TruckNoTextBox.Text,
                    Convert.ToInt16(MaximumWeightTextBox.Text));

                MessageBox.Show("สร้างใบนำส่งหมายเลข " + transportationCode + " สำเร็จ สามารถเริ่มต้นการบันทึกข้อมูลได้ทันที",
                    "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information);

                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            Clear();
        }

        private void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (TransportationDataGrid.SelectedIndex < 0)
                    return;

                _document = (m_TransportationDocument)TransportationDataGrid.SelectedItem;

                if (MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "warning",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BuyingFacade.TransportationBL().Delete(_document.TransportationDocumentCode);

                _document = null;
                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (TransportationDataGrid.SelectedIndex < 0)
                    return;

                _document = (m_TransportationDocument)TransportationDataGrid.SelectedItem;

                TransportationCodeTextBox.Text = _document.TransportationDocumentCode;
                BuyingStationComboBox.SelectedValue = _document.BuyingStationCode;
                TruckNoTextBox.Text = _document.TruckNumber;
                MaximumWeightTextBox.Text = _document.MaximumWeight.ToString();

                if (_document != null)
                {
                    AddButton.Visibility = Visibility.Collapsed;
                    EditButton.Visibility = Visibility.Visible;
                    TransportationCodeTextBox.Visibility = Visibility.Visible;
                }
                else
                {
                    AddButton.Visibility = Visibility.Visible;
                    EditButton.Visibility = Visibility.Collapsed;
                    TransportationCodeTextBox.Visibility = Visibility.Collapsed;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_document == null)
                    throw new ArgumentException("โปรดเลือกรายการที่ต้องการแก้ไขข้อมูล");

                BuyingFacade.TransportationBL().Update(_document.TransportationDocumentCode,
                    TruckNoTextBox.Text,
                    Convert.ToInt16(MaximumWeightTextBox.Text),
                    user_setting.User.Username,
                    Convert.ToDateTime(CreateDocumentDatePicker.SelectedDate));

                _document = null;
                DataGridBinding();
                Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CreateDocumentDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGridBinding();
        }

        private void BuyingStationComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGridBinding();
        }

        private void PrintButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (TransportationDataGrid.SelectedIndex < 0)
                    return;

                var model = (m_TransportationDocument)TransportationDataGrid.SelectedItem;

                var transportDoc = BuyingFacade.TransportationBL().GetSingle(model.TransportationDocumentCode);
                if (transportDoc == null)
                    throw new ArgumentException("ไม่พบข้อมูลใบนำส่งหมายเลข " + model.TransportationDocumentCode + " ในระบบ");

                TransportationDetails window = new TransportationDetails(transportDoc.TransportationDocumentCode);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
