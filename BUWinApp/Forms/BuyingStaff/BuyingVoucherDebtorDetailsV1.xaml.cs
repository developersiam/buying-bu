﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BusinessLayer.Model;
using BusinessLayer.Helper;
using BUWinApp.Helper;

namespace BUWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for DebterPaymentDetails.xaml
    /// 
    /// 1.ตรวจสอบว่ามียอดค้างชำระหรือไม่ ถ้ามีให้แสดงรายการพร้อมทั้งปุ่ม Payment เพื่อให้ User สามารถคลิกเพื่อชำระได้
    /// 2.เมื่อมีการชำระครบถ้วนแล้ว ให้แสดงรายการที่ชำระพร้อมทั้ง Disable ปุ่ม Payment และแสดงยอดที่ได้ชำระ (แสดงสถานะการชำระเงิน)
    /// 
    /// </summary>
    public partial class BuyingVoucherDebtorDetailsV1 : Window
    {
        m_BuyingDocument _document;
        List<m_ReceiveInputFactor> _receiveInputFactorList;
        List<m_ReceiveSeedAndMedia> _receiveSeedList;

        public BuyingVoucherDebtorDetailsV1(m_BuyingDocument model)
        {
            InitializeComponent();

            _receiveInputFactorList = new List<m_ReceiveInputFactor>();
            _receiveSeedList = new List<m_ReceiveSeedAndMedia>();

            _document = new m_BuyingDocument();
            _document = model;

            CheckDebtorPayment();
        }

        private void CheckDebtorPayment()
        {
            decimal totalDebt = DebtorDetailsDataBinding();
            decimal totalPayment = PaymentListDetailsDataBinding();

            if (totalDebt - totalPayment == 0)
            {
                /// ชำระไว้ครบตามจำนวน
                /// 
                DebtorPaymentStatusGrid.Background = Brushes.Green;
                DebtorPaymentStatusLabel.Content = "payment complete";

                PaymentDetailsGrid.Visibility = System.Windows.Visibility.Visible;
                PaymentInDocumentCodeTextBlock.Text = _document.DocumentCode;

                PaymentButton.IsEnabled = false;
            }
            else if (totalDebt - totalPayment < 0)
            {
                /// ชำระไว้เกินจากยอดที่ต้งไว้
                /// 
                DebtorPaymentStatusGrid.Background = Brushes.Yellow;
                DebtorPaymentStatusLabel.Content = "calculate fail";
                PaymentButton.IsEnabled = false;
            }
            else
            {
                /// ยังชำระหนี้ไม่ครบตามจำนวน
                /// 
                DebtorPaymentStatusGrid.Background = Brushes.IndianRed;
                DebtorPaymentStatusLabel.Content = "no payment";
                PaymentButton.IsEnabled = true;
            }
        }

        private decimal DebtorDetailsDataBinding()
        {
            _receiveSeedList = ReceiveSeedAndMediaHelper
                .GetByFarmerAndCrop(_document.Crop,_document.FarmerCode);
            _receiveInputFactorList = BuyingFacade.ProductionFactorBL()
                .GetReceiveInputFactorInfoByFarmer(_document.Crop, _document.FarmerCode);

            ReceiveSeedDataGrid.ItemsSource = null;
            ReceiveSeedDataGrid.ItemsSource = _receiveSeedList;

            ReceiveInputFactorDataGrid.ItemsSource = null;
            ReceiveInputFactorDataGrid.ItemsSource = _receiveInputFactorList;

            decimal totalReceiveSeed = _receiveSeedList.Sum(rs => rs.Price);
            SeedAndMediaTotalPriceTextBox.Text = totalReceiveSeed.ToString("N1");

            decimal totalReceiveInputFactor = _receiveInputFactorList.Sum(rif => rif.Price);
            PPEKitsTotalPriceTextBox.Text = totalReceiveInputFactor.ToString("N1");

            TotalPriceTextBox.Text = (totalReceiveSeed + totalReceiveInputFactor).ToString("N2");
            return totalReceiveSeed + totalReceiveInputFactor;
        }

        private decimal PaymentListDetailsDataBinding()
        {
            var inputFactorPayment = BuyingFacade.DebtorPaymentBL()
                .GetPaymentForInputFactor(_document.Crop, _document.FarmerCode);

            InputFactorPaymentDataGrid.ItemsSource = null;
            InputFactorPaymentDataGrid.ItemsSource = inputFactorPayment;

            decimal totalPaymentForInputFactor = Convert.ToDecimal(inputFactorPayment.Sum(pi => pi.Price));
            TotalPaymentForPPEKitsTextBox.Text = totalPaymentForInputFactor.ToString("N1");

            var seedPayment = BuyingFacade.DebtorPaymentBL()
                .GetPaymentForSeedAndMedia(_document.Crop, _document.FarmerCode);

            SeedPaymentDataGrid.ItemsSource = null;
            SeedPaymentDataGrid.ItemsSource = seedPayment;

            decimal totalPaymentForSeed = Convert.ToDecimal(seedPayment.Sum(ps => ps.Price));
            TotalPaymentForSeedAndMediaTextBox.Text = totalPaymentForSeed.ToString("N1");

            TotalPaymentTextBox.Text = (totalPaymentForInputFactor + totalPaymentForSeed).ToString("N1");

            return totalPaymentForInputFactor + totalPaymentForSeed;
        }

        private void PaymentButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ท่านต้องการชำระค่าเมล็ดพันธุ์และปัจจัยการผลิตใช่หรือไม่?", "การแจ้งเตือน", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                decimal totalPriceOfBuyingDocument = Convert.ToDecimal(BuyingHelper
                    .GetByBuyingDocument(
                    _document.Crop,
                    _document.FarmerCode,
                    _document.BuyingStationCode,
                    _document.BuyingDocumentNumber)
                    .Sum(b => b.Price));

                BuyingFacade.DebtorPaymentBL().DeductionFromBuying(_document,
                    user_setting.User.Username,
                    _receiveSeedList,
                    _receiveInputFactorList, 
                    totalPriceOfBuyingDocument);

                CheckDebtorPayment();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
