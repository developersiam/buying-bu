﻿using BUWinApp.Helper;
using BusinessLayer;
using BusinessLayer.Model;
using DomainModel;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BUWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for Buyings.xaml
    /// </summary>
    public partial class Buyings : Window
    {
        Buying _buying;
        m_BuyingDocument _buyingDocument;
        List<Buying> _buyings;
        List<BuyingGrade> _grades;
        List<TransportationDocument> _transportations;
        Farmer _farmer;
        GAPGroupMember2020 _gapGroupMember;

        BuyingSecondaryScreen monitor2;

        private delegate void preventCrossThreading(string str);
        private preventCrossThreading accessControlFromCentralThread;

        public Buyings()
        {
            InitializeComponent();

            _buying = new Buying();
            _buyings = new List<Buying>();
            _buyingDocument = new m_BuyingDocument();
            _grades = new List<BuyingGrade>();
            _transportations = new List<TransportationDocument>();
            _farmer = new Farmer();
            _gapGroupMember = new GAPGroupMember2020();

            /// open a serial port for communicate with a digital scale.
            /// 
            if (SerialPort.GetPortNames().Count() <= 0)
            {
                MessageBox.Show("ไม่พบ Comport สำหรับใช้ในเชื่อมต่อกับเครื่องชั่งดิจิตอล บนเครื่องคอมพิวเตอร์ของคุณ",
                    "warning", MessageBoxButton.OK, MessageBoxImage.Warning);

                IsManualCheckBox.IsChecked = true;
            }
            else
            {
                DigitalScaleHelper.Setup();

                if (DigitalScaleHelper.serialPort.IsOpen == true)
                {
                    MessageBox.Show("มีโปรแกรมอื่นกำลังเข้าถึง port การเชื่อมต่อนี้อยู่ ไม่สามารถเชื่อมต่อได้ในขณะนี้", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    IsManualCheckBox.IsChecked = false;
                    return;
                }

                DigitalScaleHelper.serialPort.Open();
                DigitalScaleHelper.serialPort.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
                accessControlFromCentralThread = displayTextReadIn;
                WeightTextBox.IsReadOnly = true;
            }

            //CreateDocumentDatePicker.SelectedDate = DateTime.Now;
            _transportations = BuyingFacade.TransportationBL()
                    .GetByCreateDate(DateTime.Now)
                    .Where(x => x.BuyingStation.AreaCode == user_setting.User.StaffUser.AreaCode)
                    .ToList();

            TransportationComboBox.ItemsSource = null;
            TransportationComboBox.ItemsSource = _transportations;

            BuyerDataBinding();
            BuyingGradeDataBinding();
            BaleBarcodeTextBox.Clear();
            BaleBarcodeTextBox.Focus();

            monitor2 = new BuyingSecondaryScreen();
        }

        private void FarmerProfileDataBinding()
        {
            try
            {
                _farmer = BuyingFacade.FarmerBL().GetByFarmerCode(_buying.FarmerCode);
                if (_farmer == null)
                    throw new ArgumentException("ไม่พบข้อมูลชาวไร่รหัส" + _buying.FarmerCode);

                var _registration = BuyingFacade.RegistrationBL().GetSingle(_buying.Crop, _buying.FarmerCode);
                if (_registration == null)
                    throw new ArgumentException("ไม่พบข้อมูลชาวไร่รหัส" + _buying.FarmerCode);

                FarmerCodeTextBox.Text = _farmer.FarmerCode;
                FarmerNameTextBox.Text = _farmer.Person.FirstName + "  " + _farmer.Person.LastName;
                AddressTextBox.Text = _farmer.Person.HouseNumber + "  " +
                    _farmer.Person.Village + "  " +
                    _farmer.Person.Tumbon + "  " +
                    _farmer.Person.Amphur + "  " +
                    _farmer.Person.Province;
                ContractCodeTextBox.Text = _registration.GAPContractCode;

                _gapGroupMember = BuyingFacade.GAPGroupMember2020BL()
                    .GetSingleByCitizenID(user_setting.Crop.Crop1, _farmer.CitizenID);

                if (_gapGroupMember != null)
                    GAPGroupCodeTextBox.Text = _gapGroupMember.GAPGroupCode;

                monitor2.FarmerNameLable.Content = FarmerNameTextBox.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FarmerQuoaDataBinding()
        {
            try
            {
                var quotas = BusinessLayer.Helper.FarmerProjectHelper.
                    GetQuotaAndSoldByFarmerVersion2(_buying.Crop, _buying.FarmerCode);

                FarmerBalanceQuotaDataGrid.ItemsSource = null;
                FarmerBalanceQuotaDataGrid.ItemsSource = quotas;

                var model = quotas.SingleOrDefault(x => x.ProjectType == _buying.ProjectType);

                ProjectTypoTextBox.Text = model.ProjectType;
                NormalQuotaTextBox.Text = model.ActualKgPerRai.ToString("N0");
                SoldTextBox.Text = Convert.ToDecimal(model.Sold).ToString("N1");
                RemainingTextBox.Text = Convert.ToDecimal(model.Balance).ToString("N1");
                ExtraQuotaTextBox.Text = model.ExtraQuota.ToString("N0");
                SoldInExtraTextBox.Text = Convert.ToDecimal(model.SoldInExtra).ToString("N1");
                RemainingInExtraTextBox.Text = Convert.ToDecimal(model.BalanceExtra).ToString("N1");

                switch (_buying.ProjectType)
                {
                    case "HB":
                        FarmerProjectStackPanel.Background = Brushes.MediumPurple;
                        break;
                    case "HPP":
                        FarmerProjectStackPanel.Background = Brushes.White;
                        break;
                    case "NL":
                        FarmerProjectStackPanel.Background = Brushes.SkyBlue;
                        break;
                    case "SC":
                        FarmerProjectStackPanel.Background = Brushes.Tomato;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingDocumentDataBinding()
        {
            try
            {
                _buyingDocument = BusinessLayer.Helper.BuyingDocumentHelper
                    .GetSingle(_buying.Crop,
                    _buying.BuyingStationCode,
                    _buying.FarmerCode,
                    _buying.BuyingDocumentNumber);

                _buyings = BuyingFacade.BuyingBL().GetByBuyingDocument(_buying.Crop,
                    _buying.FarmerCode,
                    _buying.BuyingStationCode,
                    _buying.BuyingDocumentNumber);

                BuyingDataGrid.ItemsSource = null;
                BuyingDataGrid.ItemsSource = _buyings.OrderByDescending(x => x.WeightDate);

                DocumentCodeTextBox.Text = _buyingDocument.DocumentCode;
                TotalBaleTextBox.Text = _buyingDocument.TotalBale.ToString("N0");
                TotalBuyingTextBox.Text = _buyingDocument.TotalWeight.ToString("N0");
                TotalRejectTextBox.Text = _buyingDocument.TotalReject.ToString("N0");
                FinishStatusCheckBox.IsChecked = _buyingDocument.IsFinish;

                monitor2.BaleLabel.Content = _buyingDocument.TotalWeight + "/" +
                    (_buyingDocument.TotalBale - _buyingDocument.TotalReject);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingGradeDataBinding()
        {
            try
            {
                _grades = BuyingFacade.BuyingGradeBL().GetByDefaultPricingSet();
                BuyingGradeComboBox.ItemsSource = null;
                BuyingGradeComboBox.ItemsSource = _grades;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyerDataBinding()
        {
            BuyerComboBox.ItemsSource = null;
            BuyerComboBox.ItemsSource = BuyingFacade.BuyerBL()
                .GetAll()
                .OrderBy(x => x.BuyerCode);
        }

        private void TransportationDocumentDataBinding()
        {
            try
            {
                if (TransportationComboBox.SelectedIndex < 0)
                    return;

                TruckNoTextBox.Clear();
                MaximumWeightTextBox.Clear();
                LoadingWeightTextBox.Clear();
                RemainingWeightTextBox.Clear();
                TransportationStackPanel.Background = Brushes.White;

                var model = BusinessLayer.Helper.TransportationHelper
                    .GetByTransportationCode(((TransportationDocument)TransportationComboBox.SelectedItem)
                    .TransportationDocumentCode);

                TruckNoTextBox.Text = model.TruckNumber;
                MaximumWeightTextBox.Text = model.MaximumWeight.ToString("N0");
                LoadingWeightTextBox.Text = Convert.ToDecimal(model.TotalWeight).ToString("N0");
                RemainingWeightTextBox.Text = Convert.ToDecimal((model.MaximumWeight - model.TotalWeight)).ToString("N0");

                /// แจ้งเตือนด้วยแถบสีแดงอ่อน กรณีเหลืออีก 100 กก. ยาจะเต๊มคันรถ
                /// 
                var loadingBales = BuyingFacade.BuyingBL().GetByTransportationCode(model.TransportationDocumentCode);
                if (model.MaximumWeight - (loadingBales.Sum(x => x.Weight)) <= 100)
                    TransportationStackPanel.Background = Brushes.Salmon;
                else
                    TransportationStackPanel.Background = Brushes.White;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (System.Windows.Forms.Screen.AllScreens.Length > 1)
                {
                    var screen2 = System.Windows.Forms.Screen.AllScreens[1];
                    var rectangle2 = screen2.WorkingArea;

                    monitor2.Top = rectangle2.Top;
                    monitor2.Left = rectangle2.Left;

                    monitor2.Show();
                }

                BaleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BaleBarcodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                if (BaleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุรหัสป้ายบาร์โค้ต", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    return;
                }

                _buying = BuyingFacade.BuyingBL().GetByBaleBarcode(BaleBarcodeTextBox.Text);

                if (_buying == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลห่อยารหัสบาร์โค้ต " + BaleBarcodeTextBox.Text + " โปรดสแกนบาร์โค้ตใหม่อีกครั้ง",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    BaleBarcodeTextBox.SelectAll();
                    return;
                }

                FarmerProfileDataBinding();
                FarmerQuoaDataBinding();
                BuyingDocumentDataBinding();

                if (_buying.TransportationDocumentCode != null)
                    TransportationComboBox.SelectedValue = _buying.TransportationDocumentCode;

                TransportationDocumentDataBinding();

                BuyingGradeTextBox.Text = _buying.Grade;
                BuyingGradeTextBox.Focus();
                BuyingGradeTextBox.SelectAll();

                BuyingGradeComboBox.SelectedValue = _buying.Grade;
                monitor2.GradeLabel.Content = _buying.Grade;
                BuyingGradeComboBox.Focus();

                if (_buying.BuyerCode != null)
                    BuyerComboBox.SelectedValue = _buying.BuyerCode;

                WeightTextBox.Text = Convert.ToDecimal(_buying.Weight).ToString("N1");

                BuyingGradeTextBox.Focus();
                BuyingGradeTextBox.Clear();
                BuyingGradeComboBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingGradeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                WeightTextBox.Clear();
                WeightTextBox.Focus();
                WeightTextBox.SelectAll();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingGradeComboBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                if (BuyingGradeComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("โปรดเลือกเกรดของห่อยาก่อนจะดำเนินการในขั้นตอนถัดไป", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BuyingGradeComboBox.Focus();
                    return;
                }

                monitor2.GradeLabel.Content = BuyingGradeComboBox.SelectedValue.ToString();

                if (_gapGroupMember != null)
                {
                    var cpaSample = BuyingFacade.CPASampleCollection2020BL()
                        .GetByFarmer(_gapGroupMember.CitizenID, _gapGroupMember.GAPGroupCode);

                    /// Code เก่า 2019
                    //var cpaSample = BuyingFacade.CPASampleCollectionBL().GetByFarmer(_buying.Crop, _buying.FarmerCode);

                    if (cpaSample.Where(x => x.Position == "X").Count() <= 0)
                        if (MessageBox.Show("ชาวไร่รายนี้ยังไม่มีข้อมูลการสุ่มตรวจยา X" + Environment.NewLine +
                            "หากต้องการบันทึกข้อมูล " + Environment.NewLine +
                            "โปรดเจาะห่อยานี้ และกด Yes" + Environment.NewLine +
                            "แต่ถ้าไม่ต้องการให้กด No", "warning",
                            MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                        {
                            BuyingFacade.CPASampleCollection2020BL().Add(new CPASampleCollection2020
                            {
                                Id = Guid.NewGuid(),
                                GAPGroupCode = _gapGroupMember.GAPGroupCode,
                                CitizenID = _gapGroupMember.CitizenID,
                                Position = BuyingGradeComboBox.SelectedValue.ToString().Substring(0, 1),
                                Bag = 1,
                                CollectionDate = DateTime.Now,
                                CollectionUser = user_setting.User.Username
                            });

                            /// Code เก่า 2019
                            //BuyingFacade.CPASampleCollectionBL().Add(new CPASampleCollection
                            //{
                            //    Id = Guid.NewGuid(),
                            //    Crop = _buying.Crop,
                            //    FarmerCode = _buying.FarmerCode,
                            //    Position = BuyingGradeComboBox.SelectedValue.ToString().Substring(0, 1),
                            //    Bag = 1,
                            //    CollectionDate = DateTime.Now,
                            //    CollectionUser = user_setting.User.Username
                            //});
                        }
                }

                WeightTextBox.Clear();
                WeightTextBox.Focus();
                WeightTextBox.SelectAll();

                if (IsManualCheckBox.IsChecked == true)
                    WeightTextBox.IsReadOnly = false;
                else
                    WeightTextBox.IsReadOnly = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearFormButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearForm()
        {
            BuyingGradeTextBox.Clear();
            FarmerCodeTextBox.Clear();
            FarmerNameTextBox.Clear();
            AddressTextBox.Clear();
            ContractCodeTextBox.Clear();
            GAPGroupCodeTextBox.Clear();
            DocumentCodeTextBox.Clear();
            WeightTextBox.Clear();
            TotalBaleTextBox.Clear();
            TotalBuyingTextBox.Clear();
            TotalRejectTextBox.Clear();
            FinishStatusCheckBox.IsChecked = false;

            FarmerBalanceQuotaDataGrid.ItemsSource = null;
            BuyingDataGrid.ItemsSource = null;

            _buying = null;
            _buyings = null;
            _buyingDocument = null;
            _gapGroupMember = null;

            BaleBarcodeTextBox.Clear();
            BaleBarcodeTextBox.Focus();
        }

        private void ClearAfterSave()
        {
            try
            {
                WeightTextBox.Clear();
                BuyingGradeTextBox.Clear();
                BaleBarcodeTextBox.Clear();
                BaleBarcodeTextBox.Focus();

                FarmerQuoaDataBinding();
                BuyingDocumentDataBinding();
                //BuyingDataBinding();
                TransportationDocumentDataBinding();

                _buying = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RejectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_buying == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลห่อยา โปรดลองสแกนบาร์โค้ตใหม่อีกครั้ง", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Clear();
                    BaleBarcodeTextBox.Focus();
                    return;
                }

                if (MessageBox.Show("ท่านต้องการแบ็คยา (Reject) ห่อนี้หรือใช่หรือไม่?", "การแจ้งเตือน",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                var returnSelected = PopupRejects.ReturnResult();

                if (returnSelected == null || returnSelected == "")
                    throw new ArgumentException("โปรดเลือกเหตุผลการแบ็คยาใหม่อีกครั้ง เนื่องจากไม่พบข้อมูลที่ท่านเลือก");

                BuyingFacade.BuyingBL().RejectBale(_buying.BaleBarcode,
                    returnSelected,
                    user_setting.User.Username,
                    BuyerComboBox.SelectedValue.ToString());

                ClearAfterSave();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void IsManualCheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IsManualCheckBox.IsChecked == true)
                {
                    /// close a serial port for communicate with a digital scale.
                    /// 
                    WeightTextBox.IsReadOnly = false;
                    WeightTextBox.Text = "";
                    WeightTextBox.Focus();
                    WeightTextBox.SelectAll();

                    if (Helper.DigitalScaleHelper.serialPort.IsOpen)
                    {
                        Thread CloseDown = new Thread(new ThreadStart(CloseSerialNonExit)); //close port in new thread to avoid hang
                        CloseDown.Start(); //close port in new thread to avoid hang
                    }
                }
                else
                {
                    /// open a serial port for communicate with a digital scale.
                    /// 
                    if (SerialPort.GetPortNames().Count() <= 0)
                    {
                        MessageBox.Show("ไม่พบ Comport สำหรับใช้ในเชื่อมต่อกับเครื่องชั่งดิจิตอล บนเครื่องคอมพิวเตอร์ของคุณ",
                            "warning", MessageBoxButton.OK, MessageBoxImage.Warning);

                        IsManualCheckBox.IsChecked = true;
                        return;
                    }

                    DigitalScaleHelper.Setup();

                    if (DigitalScaleHelper.serialPort.IsOpen == true)
                    {
                        MessageBox.Show("มีโปรแกรมอื่นกำลังเข้าถึง port การเชื่อมต่อนี้อยู่ ไม่สามารถเชื่อมต่อได้ในขณะนี้", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        IsManualCheckBox.IsChecked = false;
                        return;
                    }

                    DigitalScaleHelper.serialPort.Open();
                    DigitalScaleHelper.serialPort.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
                    accessControlFromCentralThread = displayTextReadIn;
                    WeightTextBox.IsReadOnly = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                IsManualCheckBox.IsChecked = false;
            }
        }

        private void WeightTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                if (_buying == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลห่อยารหัส" + BaleBarcodeTextBox.Text + " โปรดสแกนบาร์โค้ตใหม่อีกครั้ง",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    BaleBarcodeTextBox.SelectAll();
                    return;
                }

                if (BuyingGradeComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("โปรดระบุเกรดซื้อ", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BuyingGradeTextBox.Focus();
                    return;
                }

                if (BuyerComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("โปรดระบุรหัสผู้ซื้อ Buyer code", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BuyerComboBox.Focus();
                    return;
                }

                if (TransportationComboBox.SelectedIndex < 0)
                {
                    MessageBox.Show("โปรดระบุรถบรรทุกที่ทำการขนส่ง (transportation code)", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BuyerComboBox.Focus();
                    return;
                }

                if (!RegularExpressionHelper.IsDecimalCharacter(WeightTextBox.Text))
                {
                    MessageBox.Show("ค่าน้ำหนักไม่อยู่ในรูปแบบทศนิยมตามที่ระบบกำหนด", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    WeightTextBox.Clear();
                    WeightTextBox.Focus();
                    WeightTextBox.SelectAll();
                    return;
                }

                double _weight = Convert.ToDouble(WeightTextBox.Text);

                if (_weight < 1)
                {
                    MessageBox.Show("น้ำหนักใบยาจะต้องมากกว่า 0", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    WeightTextBox.Text = "";
                    WeightTextBox.Focus();
                    WeightTextBox.SelectAll();
                    return;
                }

                if (_buying.RejectReason != null)
                {
                    if (MessageBox.Show("ยาห่อนี้ถูกแบ็คเนื่องจาก " + _buying.RejectReason + " ท่านต้องการยกเลิกการแบ็ค และกลับมาซื้อใช่หรือไม่?",
                        "warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    {
                        WeightTextBox.Focus();
                        WeightTextBox.SelectAll();
                        return;
                    }
                }

                var grade = (BuyingGrade)BuyingGradeComboBox.SelectedItem;
                BuyingFacade.BuyingBL()
                    .CaptureAll(BaleBarcodeTextBox.Text,
                    grade.PricingCrop,
                    grade.PricingNumber,
                    grade.Grade,
                    BuyerComboBox.SelectedValue.ToString(),
                    Convert.ToDecimal(WeightTextBox.Text),
                    TransportationComboBox.SelectedValue.ToString(),
                    user_setting.User.Username);

                ClearAfterSave();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void TransportationComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                TransportationDocumentDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CreateDocumentDatePicker_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CreateDocumentDatePicker.SelectedDate == null)
                return;

            _transportations = BuyingFacade.TransportationBL()
                    .GetByCreateDate(Convert.ToDateTime(CreateDocumentDatePicker.SelectedDate))
                    .Where(x => x.BuyingStation.AreaCode == user_setting.User.StaffUser.AreaCode)
                    .ToList();

            TransportationComboBox.ItemsSource = null;
            TransportationComboBox.ItemsSource = _transportations;
        }

        private void RefreshDataButton_Click(object sender, RoutedEventArgs e)
        {
            TransportationDocumentDataBinding();
            BuyingGradeDataBinding();
        }

        private void displayTextReadIn(string str)
        {
            try
            {
                decimal value;
                if (!Decimal.TryParse(str, out value))
                {
                    WeightTextBox.Dispatcher.Invoke(() =>
                    {
                        WeightTextBox.Text = str;
                        monitor2.WeightLabel.Content = WeightTextBox.Text;
                    });
                    return;
                }

                var _weight = Convert.ToDouble(str);

                if (WeightTextBox.Dispatcher.CheckAccess())
                {
                    monitor2.WeightLabel.Content = WeightTextBox.Text;
                    WeightTextBox.Text = _weight.ToString();
                }
                else
                    WeightTextBox.Dispatcher.Invoke(() =>
                    {
                        WeightTextBox.Text = _weight.ToString();
                        monitor2.WeightLabel.Content = WeightTextBox.Text;
                    });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //useDigitalScaleCheckBox.IsChecked = false;
            }
        }

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            displayTextReadIn(Helper.DigitalScaleHelper.GetWeightResultFromTigerModel());
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            monitor2.Close();

            if (DigitalScaleHelper.serialPort.IsOpen)
            {
                e.Cancel = true; //cancel the fom closing
                Thread CloseDown = new Thread(new ThreadStart(CloseSerialOnExit)); //close port in new thread to avoid hang
                CloseDown.Start(); //close port in new thread to avoid hang
            }
        }

        private void CloseSerialOnExit()
        {
            try
            {
                DigitalScaleHelper.serialPort.DataReceived -= port_DataReceived;
                DigitalScaleHelper.serialPort.Close(); //close the serial port
                this.Dispatcher.Invoke(() =>
                {
                    this.Close();
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); //catch any serial port closing error messages
            }
        }

        private void CloseSerialNonExit()
        {
            try
            {
                DigitalScaleHelper.serialPort.DataReceived -= port_DataReceived;
                DigitalScaleHelper.serialPort.Close(); //close the serial port
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); //catch any serial port closing error messages
            }
        }

        private void BuyingGradeComboBox_GotFocus(object sender, RoutedEventArgs e)
        {
            BuyingGradeComboBox.Background = Brushes.AntiqueWhite;
        }

        private void BuyerComboBox_GotFocus(object sender, RoutedEventArgs e)
        {
            BuyerComboBox.Background = Brushes.AntiqueWhite;
        }

        private void WeightTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            WeightTextBox.Background = Brushes.AntiqueWhite;
        }

        private void BuyingGradeComboBox_LostFocus(object sender, RoutedEventArgs e)
        {
            BuyingGradeComboBox.Background = Brushes.White;
        }

        private void BuyerComboBox_LostFocus(object sender, RoutedEventArgs e)
        {
            BuyerComboBox.Background = Brushes.White;
        }

        private void WeightTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            WeightTextBox.Background = Brushes.White;
        }
    }
}
