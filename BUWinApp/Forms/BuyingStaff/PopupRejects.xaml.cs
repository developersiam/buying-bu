﻿using BUWinApp.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BUWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for PopupRejects.xaml
    /// </summary>
    /// 

    public partial class PopupRejects : Window
    {
        static string _rejectReason;

        public PopupRejects()
        {
            InitializeComponent();
            
            RejectReasonItemsControl.ItemsSource = null;
            RejectReasonItemsControl.ItemsSource = user_setting.RejectReasons;
        }

        public static string ReturnResult()
        {
            PopupRejects window = new PopupRejects();
            window.ShowDialog();

            return _rejectReason;
        }

        private void RejectReasonButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                _rejectReason = btn.ToolTip.ToString();

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
