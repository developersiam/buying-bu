﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using Microsoft.Reporting.WinForms;
using BusinessLayer.Model;

namespace BUWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for BuyingVoucherPrintPreview.xaml
    /// </summary>
    public partial class BuyingVoucherPrintPreview : Window
    {
        m_BuyingDocument model;

        public BuyingVoucherPrintPreview(m_BuyingDocument buyingDocumentInfo)
        {
            InitializeComponent();
            
            model = new m_BuyingDocument();
            model = buyingDocumentInfo;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                BuyingVoucherReportViewer.Reset();

                var datasource1 = BuyingFacade.StoreProcetureBL().sp_CY2018GetDebtorPaymentByVoucher(model.Crop, model.FarmerCode, model.BuyingStationCode, model.BuyingDocumentNumber);
                var datasource2 = BuyingFacade.StoreProcetureBL().sp_CY2018GetDebtorPaymentByFarmer(model.Crop, model.FarmerCode);
                var datasource3 = BuyingFacade.StoreProcetureBL().sp_CY2018GetPPEKitsDistributionByFarmer(model.Crop, model.FarmerCode);
                var datasource4 = BuyingFacade.StoreProcetureBL().sp_CY2018GetSeedDistributionByFarmer(model.Crop, model.FarmerCode);

                var datasource5 = BuyingFacade.StoreProcetureBL().sp_GetCalculationPercentOfSold(model.Crop, model.FarmerCode);
                var datasource6 = BuyingFacade.StoreProcetureBL().sp_GetFarmerQuotaFixedAndSold(model.Crop, model.FarmerCode);
                var datasource7 = BuyingFacade.StoreProcetureBL().sp_GetBuyingDocumentByBuyingDocument(model.Crop, model.BuyingStationCode, model.FarmerCode, model.BuyingDocumentNumber);
                var datasource8 = BuyingFacade.StoreProcetureBL().sp_GetBuyingDetailsByBuyingDocument(model.Crop, model.FarmerCode, model.BuyingStationCode, model.BuyingDocumentNumber);
                var datasource9 = BuyingFacade.StoreProcetureBL().sp_GetBuyingDetailsByFarmer(model.Crop, model.FarmerCode);

                ReportDataSource sp_CY2018GetDebtorPaymentByVoucherDataSource = new ReportDataSource();
                ReportDataSource sp_CY2018GetDebtorPaymentByFarmerDataSource = new ReportDataSource();
                ReportDataSource sp_CY2018GetPPEKitsDistributionByFarmerDataSource = new ReportDataSource();
                ReportDataSource sp_CY2018GetSeedDistributionByFarmerDataSource = new ReportDataSource();

                ReportDataSource sp_GetCalculationPercentOfSoldDataSource = new ReportDataSource();
                ReportDataSource sp_GetFarmerQuotaFixedAndSoldDataSource = new ReportDataSource();
                ReportDataSource sp_GetBuyingDetailsByFarmerDataSource = new ReportDataSource();
                ReportDataSource sp_GetBuyingDetailsByBuyingDocumentDataSource = new ReportDataSource();
                ReportDataSource sp_GetBuyingDocumentByBuyingDocumentDataSource = new ReportDataSource();

                sp_CY2018GetDebtorPaymentByVoucherDataSource.Name = "sp_CY2018GetDebtorPaymentByVoucher";
                sp_CY2018GetDebtorPaymentByFarmerDataSource.Name = "sp_CY2018GetDebtorPaymentByFarmer";
                sp_CY2018GetPPEKitsDistributionByFarmerDataSource.Name = "sp_CY2018GetPPEKitsDistributionByFarmer";
                sp_CY2018GetSeedDistributionByFarmerDataSource.Name = "sp_CY2018GetSeedDistributionByFarmer";

                sp_GetCalculationPercentOfSoldDataSource.Name = "sp_GetCalculationPercentOfSold";
                sp_GetFarmerQuotaFixedAndSoldDataSource.Name = "sp_GetFarmerQuotaFixedAndSold";
                sp_GetBuyingDetailsByFarmerDataSource.Name = "sp_GetBuyingDetailsByFarmer";
                sp_GetBuyingDetailsByBuyingDocumentDataSource.Name = "sp_GetBuyingDetailsByBuyingDocument";
                sp_GetBuyingDocumentByBuyingDocumentDataSource.Name = "sp_GetBuyingDocumentByBuyingDocument";

                sp_CY2018GetDebtorPaymentByVoucherDataSource.Value = datasource1;
                sp_CY2018GetDebtorPaymentByFarmerDataSource.Value = datasource2;
                sp_CY2018GetPPEKitsDistributionByFarmerDataSource.Value = datasource3;
                sp_CY2018GetSeedDistributionByFarmerDataSource.Value = datasource4;

                sp_GetCalculationPercentOfSoldDataSource.Value = datasource5;
                sp_GetFarmerQuotaFixedAndSoldDataSource.Value = datasource6;
                sp_GetBuyingDocumentByBuyingDocumentDataSource.Value = datasource7;
                sp_GetBuyingDetailsByBuyingDocumentDataSource.Value = datasource8;
                sp_GetBuyingDetailsByFarmerDataSource.Value = datasource9;

                BuyingVoucherReportViewer.LocalReport.DataSources.Add(sp_CY2018GetDebtorPaymentByVoucherDataSource);
                BuyingVoucherReportViewer.LocalReport.DataSources.Add(sp_CY2018GetDebtorPaymentByFarmerDataSource);
                BuyingVoucherReportViewer.LocalReport.DataSources.Add(sp_CY2018GetPPEKitsDistributionByFarmerDataSource);
                BuyingVoucherReportViewer.LocalReport.DataSources.Add(sp_CY2018GetSeedDistributionByFarmerDataSource);

                BuyingVoucherReportViewer.LocalReport.DataSources.Add(sp_GetCalculationPercentOfSoldDataSource);
                BuyingVoucherReportViewer.LocalReport.DataSources.Add(sp_GetFarmerQuotaFixedAndSoldDataSource);
                BuyingVoucherReportViewer.LocalReport.DataSources.Add(sp_GetBuyingDetailsByFarmerDataSource);
                BuyingVoucherReportViewer.LocalReport.DataSources.Add(sp_GetBuyingDetailsByBuyingDocumentDataSource);
                BuyingVoucherReportViewer.LocalReport.DataSources.Add(sp_GetBuyingDocumentByBuyingDocumentDataSource);

                if (BuyingFacade.DebtorPaymentBL().GetByBuyingDocument(model).Count > 0)
                    BuyingVoucherReportViewer.LocalReport.ReportEmbeddedResource = "BUWinApp.RDLCReport.RPTBUY001-1.rdlc";
                else
                    BuyingVoucherReportViewer.LocalReport.ReportEmbeddedResource = "BUWinApp.RDLCReport.RPTBUY001.rdlc";

                BuyingVoucherReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
