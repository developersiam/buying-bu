﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BusinessLayer.Model;
using BUBuyingSystemWinApp.Helper;

namespace BUBuyingSystemWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for DebtorPaymentDetailForCY2018.xaml
    /// </summary>
    public partial class DebterPaymentDetailForCY2018 : Window
    {
        decimal _totalSeedAmount;
        decimal _totalFarmMaterialAmount;
        BuyingDocument _buyingDoc;

        public DebterPaymentDetailForCY2018(m_BuyingDocument document)
        {
            try
            {
                InitializeComponent();

                _buyingDoc = new BuyingDocument();
                _buyingDoc = document;

                SummaryDebtor();
                SeedDistributionDataBinding();
                FarmMaterialDataBinding();
                PaymentDetailDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void SummaryDebtor()
        {
            try
            {
                _totalSeedAmount = BuyingFacade.SeedDistributionBL()
                    .GetByFarmer(_buyingDoc.Crop, _buyingDoc.FarmerCode)
                    .Sum(x => x.Quantity * x.SeedForDistribution.UnitPrice);

                _totalFarmMaterialAmount = BuyingFacade.CropInputDistributionBL()
                    .GetByFarmer(_buyingDoc.Crop, _buyingDoc.FarmerCode)
                    .Sum(x => x.Quantity * x.CropInputItem.UnitPrice);

                TotalSeedAndMediaPriceTextBlock.Text = _totalSeedAmount.ToString("N2");
                TotalInputFactorPriceTextBlock.Text = _totalFarmMaterialAmount.ToString("N2");
                TotalPriceTextBlock.Text = (_totalSeedAmount + _totalFarmMaterialAmount).ToString("N2");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void SeedDistributionDataBinding()
        {
            try
            {
                SeedDistributionDataGrid.ItemsSource = null;
                SeedDistributionDataGrid.ItemsSource = BuyingFacade.SeedDistributionBL().GetByFarmer(_buyingDoc.Crop, _buyingDoc.FarmerCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void FarmMaterialDataBinding()
        {
            try
            {
                FarmMaterialDataGrid.ItemsSource = null;
                FarmMaterialDataGrid.ItemsSource = BuyingFacade.CropInputDistributionBL().GetByFarmer(_buyingDoc.Crop, _buyingDoc.FarmerCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void PaymentDetailDataBinding()
        {
            try
            {
                var model = BuyingFacade.DebtorPaymentBL().GetByFarmerAndCrop(_buyingDoc.Crop, _buyingDoc.FarmerCode);

                if (model.Sum(x => x.PaymentAmount) > 0)
                {
                    DebtorPaymentStatusGrid.Background = Brushes.Green;
                    DebtorPaymentStatusLabel.Content = "มีการหักเงินแล้ว";
                    PaymentButton.IsEnabled = false;
                }
                else
                {
                    DebtorPaymentStatusGrid.Background = Brushes.IndianRed;
                    DebtorPaymentStatusLabel.Content = "ยังไม่มีการหักเงิน";
                    PaymentButton.IsEnabled = true;
                }

                //หมายเลขเวาเชอร์ที่ถูกหักค่าเมล็ดพันธู์และปัจจัยการผลิตเพื่อใช้ในการแสดง
                if (model.Sum(x => x.PaymentAmount) <= 0)
                    return;

                string documentNo = model.First().VoucherCrop.ToString() + '-' +
                    model.First().VoucherFarmerCode + '-' +
                    model.First().VoucherBuyingStationCode + '-' +
                    model.First().VoucherBuyingDocumentNumber.ToString();

                PaymentInDocumentCodeTextBlock.Text = documentNo;
                PaymentAmountTextBlock.Text = model.Sum(x => x.PaymentAmount).ToString();
                PaymentDateTextBlock.Text = model.First().PaymentDate.ToShortDateString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PaymentButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if(_buyingDoc.IsFinish == false)
                {
                    MessageBox.Show("ท่านยังไม่ได้ Finish ใบเวาเชอร์นี้ โปรดย้อนกลับไป Finish ก่อนแล้วค่อยกลับมาดำเนินการอีกครั้ง", "การแจ้งเตือนจากระบบ", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                //Insert payment transection into debtor payment table.
                BuyingFacade.DebtorPaymentBL().DeductFromBuyingVersionCY2018(new DebtorPayment
                {
                    PaymentCrop = _buyingDoc.Crop,
                    PaymentFarmerCode = _buyingDoc.FarmerCode,
                    PaymentSequence = 1,
                    PaymentAmount = _totalSeedAmount + _totalFarmMaterialAmount,
                    PaymentUser = user_setting.User.Username,
                    PaymentDate = DateTime.Now,
                    PaymentType = "Buying",
                    VoucherCrop = _buyingDoc.Crop,
                    VoucherFarmerCode = _buyingDoc.FarmerCode,
                    VoucherBuyingStationCode = _buyingDoc.BuyingStationCode,
                    VoucherBuyingDocumentNumber = _buyingDoc.BuyingDocumentNumber
                });

                SummaryDebtor();
                SeedDistributionDataBinding();
                FarmMaterialDataBinding();
                PaymentDetailDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
