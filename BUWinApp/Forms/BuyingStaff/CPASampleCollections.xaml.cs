﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BUWinApp.Helper;

namespace BUWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for CPASampleCollectionWindow.xaml
    /// </summary>
    public partial class CPASampleCollections : Window
    {
        GAPGroupMember2020 _gapGroupMember;
        RegistrationFarmer _regisModel;

        public CPASampleCollections(string farmerCode)
        {
            InitializeComponent();

            _gapGroupMember = new GAPGroupMember2020();
            _regisModel = new RegistrationFarmer();

            if (farmerCode != "")
            {
                FarmerCodeTextBox.Text = farmerCode;
                BindingDataToForm(farmerCode);
            }

            FarmerCodeTextBox.Focus();
        }
        private void ClearForm()
        {
            FarmerCodeTextBox.Text = "";
            FarmerNameTextBox.Text = "";
            GAPGroupCodeTextBox.Text = "";
            CPASampleCollectionDataGrid.ItemsSource = null;
            SummaryOfXCBTSoldDataGrid.ItemsSource = null;
            FarmerCodeTextBox.Focus();

            _regisModel = null;
            _gapGroupMember = null;
        }

        private void CPACollectionDataGridBinding(string farmerCode)
        {

            /// Code เดิม
            //var sampleList = BuyingFacade.CPASampleCollectionBL()
            //    .GetByFarmer(user_setting.Crop.Crop1, farmerCode);

            var sampleList = BuyingFacade.CPASampleCollection2020BL()
                .GetByFarmer(_gapGroupMember.CitizenID, _gapGroupMember.GAPGroupCode);

            CPASampleCollectionDataGrid.ItemsSource = null;
            CPASampleCollectionDataGrid.ItemsSource = sampleList;
            totalItemLabel.Content = sampleList.Sum(x => x.Bag).ToString("N0");
        }

        private void BindingDataToForm(string farmerCode)
        {
            try
            {
                if (farmerCode == "")
                {
                    MessageBox.Show("โปรดกรอกรหัสชาวไร่", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ClearForm();
                    return;
                }

                _regisModel = BuyingFacade.RegistrationBL().GetSingle(user_setting.Crop.Crop1, farmerCode);

                if (_regisModel == null)
                {
                    MessageBox.Show("ไม่พบข้อมูล", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ClearForm();
                    return;
                }

                FarmerNameTextBox.Text = _regisModel.Farmer.Person.Prefix +
                    _regisModel.Farmer.Person.FirstName + " " +
                    _regisModel.Farmer.Person.LastName;

                CitizenIDTextBox.Text = _regisModel.Farmer.CitizenID;

                SummaryOfXCBTSoldDataGrid.ItemsSource = null;
                SummaryOfXCBTSoldDataGrid.ItemsSource = BuyingFacade.BuyingBL()
                    .GetSummaryBuyingOfFarmerByPosition(user_setting.Crop.Crop1, farmerCode);

                //GAPGroupCodeTextBox.Text = _regModel.GAPGroupCode; /// Code เดิม

                /// มีการเปลี่ยนแปลงวิธีการสร้างกลุ่มชาวไร่ใหม่ โดยชาวไร่ 1 คน ไม่ว่าจะลงทะเบียนไว้กับกี่ตัวแทน ก็จะได้ GAP group code เพียงตัวเดียวเท่านั้น
                /// โดยอิงจากรหัสประจำตัวประชาชนของชาวไร่ จากเดิมอิงจากรหัส farmer code
                /// 
                _gapGroupMember = BuyingFacade.GAPGroupMember2020BL().GetSingleByCitizenID(user_setting.Crop.Crop1, CitizenIDTextBox.Text);
                if (_gapGroupMember == null)
                    throw new ArgumentException("ไม่พบข้อมูล GAP Group ของชาวไร่รายนี้ในระบบ โปรดแจ้งแผนกไอทีเพื่อทำการตรวจสอบข้อมูล");

                GAPGroupCodeTextBox.Text = _gapGroupMember.GAPGroupCode;
                CPACollectionDataGridBinding(farmerCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BindingDataToForm(FarmerCodeTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FarmerCodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                BindingDataToForm(FarmerCodeTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Save(string position, string farmerCode)
        {
            try
            {
                if (farmerCode == "")
                {
                    MessageBox.Show("โปรดกรอกรหัสชาวไร่", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ClearForm();
                    return;
                }

                if (CitizenIDTextBox.Text == "")
                {
                    MessageBox.Show("ไม่พบข้อมูลรหัสประจำตัวประชาชนของชาวไร่รหัส " + farmerCode + " โปรดติดต่อแผนกไอทีเพื่อตรวจสอบข้อมูล",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ClearForm();
                    return;
                }

                if (Helper.RegularExpressionHelper.IsNumericCharacter(BagTextBox.Text) == false)
                {
                    MessageBox.Show("จำนวนที่ตรวจจะต้องเป็นตัวเลขเท่านั้น", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BagTextBox.Clear();
                    BagTextBox.Focus();
                    return;
                }

                var _regModel = BuyingFacade.RegistrationBL().GetSingle(user_setting.Crop.Crop1, farmerCode);

                if (_regModel == null)
                {
                    MessageBox.Show("ไม่พบข้อมูล", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ClearForm();
                    return;
                }

                //if (_regModel.GAPGroupCode == null || _regModel.GAPGroupCode == "")
                //{
                //    MessageBox.Show("ยังไม่ได้ระบุ GAP Group Code ให้กลับชาวไร่รายนี้", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}

                if (BagTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุจำนวน Sample", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (_gapGroupMember == null)
                    throw new ArgumentException("ไม่พบข้อมูล GAP Group ของชาวไร่รายนี้ในระบบ โปรดแจ้งแผนกไอทีเพื่อทำการตรวจสอบข้อมูล");

                /// Code เดิม
                //BuyingFacade.CPASampleCollectionBL().Add(new CPASampleCollection
                //{
                //    Id = Guid.NewGuid(),
                //    Crop = _regModel.Crop,
                //    FarmerCode = _regModel.FarmerCode,
                //    Position = position,
                //    Bag = Convert.ToByte(BagTextBox.Text),
                //    CollectionDate = DateTime.Now,
                //    CollectionUser = user_setting.User.Username
                //});


                /// Code ใหม่ปี 2020
                BuyingFacade.CPASampleCollection2020BL().Add(new CPASampleCollection2020
                {
                    Id = Guid.NewGuid(),
                    GAPGroupCode = _gapGroupMember.GAPGroupCode,
                    CitizenID = _gapGroupMember.CitizenID,
                    Position = position,
                    Bag = Convert.ToByte(BagTextBox.Text),
                    CollectionDate = DateTime.Now,
                    CollectionUser = user_setting.User.Username
                });

                CPACollectionDataGridBinding(_regModel.FarmerCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void XGradeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Save("X", FarmerCodeTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CGradeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Save("C", FarmerCodeTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BGradeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Save("B", FarmerCodeTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void TGradeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Save("T", FarmerCodeTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ViewGroupSummaryButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (GAPGroupCodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุ GAP Group Code", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                CPASampleCollectionGroupSummary window = new CPASampleCollectionGroupSummary(GAPGroupCodeTextBox.Text);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CPASampleCollectionDataGrid.SelectedIndex <= -1)
                    return;

                ///Code เดิม
                //var item = (CPASampleCollection)CPASampleCollectionDataGrid.SelectedItem;

                var item = (CPASampleCollection2020)CPASampleCollectionDataGrid.SelectedItem;

                if (MessageBox.Show("คุณต้องการลบข้อมูลนี้ใช่หรือไม่?", "ยืนยัน", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;
                // Code เดิม
                //BuyingFacade.CPASampleCollectionBL().Delete(item.Id);

                BuyingFacade.CPASampleCollection2020BL().Delete(item.Id);
                CPACollectionDataGridBinding(_regisModel.FarmerCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }
    }
}
