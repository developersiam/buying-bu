﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BusinessLayer.Helper;
using BUBuyingSystemWinApp.Helper;

namespace BUBuyingSystemWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for MovingTheBaleBetweenTheTruck.xaml
    /// </summary>
    public partial class MovingTheBaleBetweenTheTruck : Window
    {
        string _transportationCode;
        DateTime _movingDate;

        public MovingTheBaleBetweenTheTruck(short crop, string stationCode, DateTime createDate)
        {
            InitializeComponent();
            
            BaleBarcodeTextBox.Focus();
            _movingDate = createDate;

            TransportationCodeComboBox.ItemsSource = null;
            TransportationCodeComboBox.ItemsSource = TransportationHelper
                .GetByStationAndCrop(crop,
                stationCode,
                createDate)
                .OrderByDescending(t => t.TransportationDocumentCode);
        }

        private void TransportationDocumentDataBinding()
        {
            try
            {
                if (_transportationCode == "")
                {
                    MessageBox.Show("ไม่พบข้อมูลรหัสใบรายการนำส่ง " + _transportationCode, "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var document = TransportationHelper.GetByTransportationCode(_transportationCode);

                CreateDateTextBox.Text = document.CreateDate.ToShortDateString();
                CreatByTextBox.Text = document.CreateUser;
                TruckNoTextBox.Text = document.TruckNumber;
                BuyingStationTextBox.Text = document.BuyingStationCode;
                MaximumWeightTextBox.Text = document.MaximumWeight.ToString("N0");
                LoadingWeightTextBox.Text = Convert.ToInt16(document.TotalWeight).ToString("N0");
                RemainingWeightTextBox.Text = (document.MaximumWeight - Convert.ToInt16(document.Buyings.Sum(x => x.Weight))).ToString("N0");
                TotalBaleBox.Text = Convert.ToInt16(document.TotalBale).ToString("N0");

                if (document.IsFinish == true)
                {
                    FinishButton.IsEnabled = false;
                    UnFinishButton.IsEnabled = true;
                    PrintButton.IsEnabled = true;
                }
                else
                {
                    FinishButton.IsEnabled = true;
                    UnFinishButton.IsEnabled = false;
                    PrintButton.IsEnabled = false;
                }

                TransportationDetailsDataGrid.ItemsSource = null;
                TransportationDetailsDataGrid.ItemsSource = document.Buyings.OrderByDescending(x => x.LoadBaleToTruckDate).Take(30);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BaleBarcodeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;


                if (BaleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดสแกนบาร์โค้ตใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                BuyingFacade.TransportationBL().MoveBale(BaleBarcodeTextBox.Text, 
                    TransportationCodeComboBox.SelectedValue.ToString(), 
                    user_setting.User.Username,
                    _movingDate, "เพื่อย้ายห่อยาระหว่างรถบรรทุก");


                TransportationDocumentDataBinding();

                BaleBarcodeTextBox.Text = "";
                BaleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteBarcodeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (TransportationDetailsDataGrid.SelectedIndex <= -1)
                    return;

                var model = (Buying)TransportationDetailsDataGrid.SelectedItem;

                if (model == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลยาหมายเลขห่อ " + model.BaleBarcode + 
                        " นี้ในระบบ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (MessageBox.Show("ท่านต้องการลบข้อมูลการขนส่งของห่อยาหมายเลขบาร์โค้ต " + model.BaleBarcode + 
                    " นี้ออกจากระบบใช่หรือไม่?", "การแจ้งเตือน", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BuyingFacade.TransportationBL().RemoveBaleFromTruck(model.BaleBarcode);
                TransportationDocumentDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Escape)
                {
                    BaleBarcodeTextBox.Text = "";
                    BaleBarcodeTextBox.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BaleBarcodeTextBox.Text = "";
                BaleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FinishButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BuyingFacade.TransportationBL().Finish(_transportationCode, user_setting.User.Username);
                TransportationDocumentDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UnFinishButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BuyingFacade.TransportationBL().Unfinish(_transportationCode, user_setting.User.Username);
                TransportationDocumentDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PrintButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TransportationDocumentPrintPreview printDocumentVoucher = new TransportationDocumentPrintPreview(_transportationCode);
                printDocumentVoucher.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void TransportationDocumentNoComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (TransportationCodeComboBox.SelectedIndex <= -1)
                    return;


                _transportationCode = TransportationCodeComboBox.SelectedValue.ToString();
                TransportationDocumentDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}