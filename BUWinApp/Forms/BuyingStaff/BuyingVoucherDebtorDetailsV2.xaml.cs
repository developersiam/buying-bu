﻿
using BusinessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer.Model;
using BUWinApp.Helper;

namespace BUWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for FarmerDeductionWindow.xaml
    /// </summary>
    public partial class BuyingVoucherDebtorDetailsV2 : Window
    {
        m_BuyingDocument _documentInfo;
        List<m_SeedDistribution> _seedDistribution;
        List<m_FarmMaterialDistribution> _farmMaterialDistribution;
        DebtorPayment _payment;
        decimal _totalFarmMaterialAmount;
        decimal _totalSeedAmount;

        public BuyingVoucherDebtorDetailsV2(m_BuyingDocument model)
        {
            InitializeComponent();

            _payment = new DebtorPayment();
            _seedDistribution = new List<m_SeedDistribution>();
            _farmMaterialDistribution = new List<m_FarmMaterialDistribution>();
            _documentInfo = new m_BuyingDocument();
            _documentInfo = model;

            PageLoad();
        }

        public void PageLoad()
        {
            try
            {
                _seedDistribution = BuyingFacade.SeedDistributionBL()
                    .GetByFarmer(_documentInfo.Crop, 
                    _documentInfo.FarmerCode)
                    .Select(x => new m_SeedDistribution
                    {
                        Crop = x.Crop,
                        FarmerCode = x.FarmerCode,
                        SeedForDistributionID = x.SeedForDistributionID,
                        Name = x.SeedForDistribution.SeedVariety.SeedVarietyCode,
                        Quantity = x.Quantity,
                        Price = x.Quantity * x.SeedForDistribution.UnitPrice,
                        IsDeduction = x.IsDeduction,
                        IsPayment = x.IsPayment,
                        DistributionDate = x.DistributionDate,
                        PaymentDate = x.PaymentDate,
                        SeedForDistribution = x.SeedForDistribution
                    }).ToList();

                SeedDistributionDataGrid.ItemsSource = null;
                SeedDistributionDataGrid.ItemsSource = _seedDistribution;

                _totalSeedAmount = _seedDistribution.Where(x => x.IsDeduction == true)
                    .Sum(x => x.Price);

                _farmMaterialDistribution = BuyingFacade.CropInputDistributionBL()
                    .GetByFarmer(_documentInfo.Crop, 
                    _documentInfo.FarmerCode)
                    .Select(x => new m_FarmMaterialDistribution {
                        Crop = x.Crop,
                        FarmerCode = x.FarmerCode,
                        CropInputItemID = x.CropInputItemID,
                        Name = x.CropInputItem.CropInputCategory.CropInputCategoryName,
                        Quantity = x.Quantity,
                        Price = x.Quantity * x.CropInputItem.UnitPrice,
                        IsDeduction = x.IsDeduction,
                        IsPayment = x.IsPayment,
                        DistributionDate = x.DistributionDate,
                        PaymentDate = x.PaymentDate,
                        CropInputItem = x.CropInputItem
                    }).ToList();

                FarmMaterialDistributionDataGrid.ItemsSource = null;
                FarmMaterialDistributionDataGrid.ItemsSource = _farmMaterialDistribution;

                _totalFarmMaterialAmount = _farmMaterialDistribution.Where(x => x.IsDeduction == true)
                    .Sum(x => x.Price);

                SeedAndMediaTotalPriceTextBox.Text = _totalSeedAmount.ToString("N1");
                PPEKitsTotalPriceTextBox.Text = _totalFarmMaterialAmount.ToString("N1");
                TotalPriceTextBox.Text = (_totalSeedAmount + _totalFarmMaterialAmount).ToString("N1");

                _payment = BuyingFacade.DebtorPaymentBL()
                    .GetByFarmerAndCrop(_documentInfo.Crop, 
                    _documentInfo.FarmerCode).FirstOrDefault();

                if (_payment == null)
                {
                    PaymentCodeTextBox.Text = "ยังไม่มีการหักเงิน";
                    PaymentCodeTextBox.Foreground = Brushes.Red;
                    return;
                }

                PaymentCodeTextBox.Text = _payment.PaymentCrop + "-" +
                    _payment.PaymentFarmerCode + "-" +
                    _payment.PaymentSequence;

                PaymentCodeDetailTextBox.Text = " (in voucher no: " +
                    _payment.VoucherCrop + "-" +
                    _payment.VoucherFarmerCode + "-" +
                    _payment.VoucherBuyingStationCode + "-" +
                    _payment.VoucherBuyingDocumentNumber +
                    ") *เมื่อวันที่ " +
                    _payment.PaymentDate;

                PaymentButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PaymentButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // ถ้ามีการหักชำระแล้วจะไม่มีการหักซ้ำ ให้ตรวจสอบข้อมูลก่อน
                if(_payment != null)
                {
                    MessageBox.Show("มีการหักยอดแล้ว เมื่อวันที่ " + _payment.PaymentDate + 
                        " จำนวน " + _payment.PaymentAmount.ToString("N1") + " บาท" +
                        " โปรดแจ้งทางผู้จัดการลานรับซื้อเพื่อตรวจสอบข้อมูลอีกครั้ง", "การแจ้งเตือน",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                //ตรวจสอบยอดเงินจากการขาย ว่าพอหักชำระหรือไม่
                var _buyingList = BuyingFacade.BuyingBL().GetByBuyingDocument(_documentInfo.Crop,
                    _documentInfo.FarmerCode,
                    _documentInfo.BuyingStationCode,
                    _documentInfo.BuyingDocumentNumber).Where(x=>x.Grade != null).ToList();
                
                if (_buyingList.Sum(x=>x.Weight * x.BuyingGrade.UnitPrice) < _totalSeedAmount + _totalFarmMaterialAmount)
                {
                    MessageBox.Show("ยอดเงินรวมจากการขาย ไม่พอหักชำระในครั้งนี้ (ยอดรวมการขายครั้งนี้คือ " +
                        Convert.ToDecimal(_buyingList.Sum(x => x.Weight * x.BuyingGrade.UnitPrice)).ToString("N0") + 
                        " บาท ยอดค้างชำระคือ " + (_totalSeedAmount + _totalFarmMaterialAmount).ToString("N0") + " บาท)"
                        , "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                // บันทึกข้อมูลการหักชำระ
                BuyingFacade.DebtorPaymentBL().DeductionFromBuyingVersionCY2019(_documentInfo, user_setting.User.Username);
                PageLoad();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
