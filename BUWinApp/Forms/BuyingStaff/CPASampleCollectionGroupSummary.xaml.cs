﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DomainModel;
using BusinessLayer;

namespace BUWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for CPASampleCollectionGroupSummary.xaml
    /// </summary>
    public partial class CPASampleCollectionGroupSummary : Window
    {
        string _gapGroupCode;

        public CPASampleCollectionGroupSummary(string gapGroupCode)
        {
            InitializeComponent();

            _gapGroupCode = gapGroupCode;
            if (_gapGroupCode != null)
            {
                GAPGroupCodeTextBox.Text = _gapGroupCode;
                ShowReport();
            }
        }

        private void ShowReport()
        {
            try
            {
                GAPGroupSummaryReportViewer.Reset();             
                   
                ReportDataSource dataSource = new ReportDataSource();
                dataSource.Name = "sp_GetCPASampleCollectionByGapGroup_Result";

                dataSource.Value = BuyingFacade.CPASampleCollectionBL().GetByGAPGroup(GAPGroupCodeTextBox.Text);                
                GAPGroupSummaryReportViewer.LocalReport.DataSources.Add(dataSource);

                GAPGroupSummaryReportViewer.LocalReport.ReportEmbeddedResource = "BUWinApp.RDLCReport.RPTCPA01.rdlc";
                GAPGroupSummaryReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ShowButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ShowReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PrintGAPGroupStickerButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var gapGroup = BuyingFacade.GAPGroup2020BL().GetSingle(_gapGroupCode);

                TSCPrinter.openport("TSC TTP-247");
                TSCPrinter.setup("70", "35", "2.0", "6", "0", "0", "0");
                TSCPrinter.sendcommand("GAP  2 mm,0");
                TSCPrinter.sendcommand("DIRECTION 1");
                TSCPrinter.clearbuffer();
                
                TSCPrinter.windowsfont(20, 20, 30, 0, 0, 0, "arial", _gapGroupCode + " (" + gapGroup.GAPGroupMember2020.Count + " farmers)");
                TSCPrinter.barcode("20", "65", "128", "50", "0", "0", "2", "2", _gapGroupCode);
                TSCPrinter.windowsfont(20, 120, 80, 0, 0, 0, "cordia new", gapGroup.AreaCode);
                TSCPrinter.windowsfont(20, 180, 60, 0, 0, 0, "cordia new", DateTime.Now.ToString());

                TSCPrinter.printlabel("1", "1");
                TSCPrinter.closeport();

                ///// Code เดิม
                ////var gapGroup = BuyingFacade.GapGroupBL().GetGAPGroupByGapGroupCode(_gapGroupCode);

                //TSCPrinter.openport("TSC TTP-247");
                //TSCPrinter.setup("70", "35", "2.0", "6", "0", "0", "0");
                //TSCPrinter.sendcommand("GAP  2 mm,0");
                //TSCPrinter.sendcommand("DIRECTION 1");
                //TSCPrinter.clearbuffer();

                ////TSCPrinter.sendcommand("BAR 410,230,2,40");
                ////TSCPrinter.sendcommand("BAR 542,230,2,40");
                ////TSCPrinter.sendcommand("BAR 410,230,133,2");
                ////TSCPrinter.sendcommand("BAR 410,270,133,2");
                ////TSCPrinter.windowsfont(435, 240, 30, 0, 0, 0, "arial", "CPA");
                ////TSCPrinter.windowsfont(425, 175, 30, 0, 0, 0, "arial", GAPGroupCodeTextBox.Text);
                //TSCPrinter.windowsfont(20, 20, 30, 0, 0, 0, "arial", _gapGroupCode + " (" + gapGroup.RegistrationFarmers.Count + " farmers)");
                //TSCPrinter.barcode("20", "65", "128", "50", "0", "0", "2", "2", _gapGroupCode);
                //TSCPrinter.windowsfont(20, 120, 80, 0, 0, 0, "cordia new", gapGroup.SupplierCode);
                //TSCPrinter.windowsfont(20, 180, 60, 0, 0, 0, "cordia new", DateTime.Now.ToString());

                //TSCPrinter.printlabel("1", "1");
                //TSCPrinter.closeport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
