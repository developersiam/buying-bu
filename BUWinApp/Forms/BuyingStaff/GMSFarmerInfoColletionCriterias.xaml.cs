﻿using BUWinApp.Helper;
using BusinessLayer;
using DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BUWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for GMSFarmerInfoColletionCriterias.xaml
    /// </summary>
    public partial class GMSFarmerInfoColletionCriterias : Window
    {
        static bool _result;
        public static string _citizenID;

        public GMSFarmerInfoColletionCriterias()
        {
            InitializeComponent();
            _result = false;

            var model = BuyingFacade.GMSFarmerInfoCollectionCriteriaBL().GetSingle(_citizenID);

            if (model == null)
                throw new ArgumentException("ไม่พบข้อมูล GMS ของชาวไร่รายนี้");

            CropInputQtyCheckBox.IsChecked = model.CropInputQty;
            GPSFieldLatLongCheckBox.IsChecked = model.GPSFieldLatLong;
            GPSHouseLatLongCheckBox.IsChecked = model.GPSHouseLatLong;
            EstOfToppingCheckBox.IsChecked = model.EstOfTopping;
            EstOfTransplantingCheckBox.IsChecked = model.EstOfTransplanting;
            TransplantingCompletionDateCheckBox.IsChecked = model.TransplantingCompletionDate;
        }

        public static bool ReturnResult()
        {
            GMSFarmerInfoColletionCriterias window = new GMSFarmerInfoColletionCriterias();
            window.ShowDialog();

            return _result;
        }

        private void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text.Length <= 0)
                {
                    MessageBox.Show("โปรดกรอกชื่อผู้ใช้", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (PasswordTextBox.Password.Length <= 0)
                {
                    MessageBox.Show("โปรดกรอกรหัสผ่าน", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var user = BuyingFacade.UserBL().GetByUsername(UsernameTextBox.Text);
                if (user == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลบัญชีผู้ใช้นี้ในระบบ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);

                    UsernameTextBox.Focus();
                    return;
                }

                if (user.Password != PasswordTextBox.Password)
                {
                    MessageBox.Show("รหัสผ่านไม่ถูกต้อง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);

                    PasswordTextBox.Password = "";
                    PasswordTextBox.Focus();
                    return;
                }

                var userRoles = (from ur in BuyingFacade.UserRoleBL().GetByUser(user.Username)
                                          from r in BuyingFacade.RoleBL().GetAll()
                                          where ur.RoleID == r.RoleID
                                          select new Role
                                          {
                                              RoleID = ur.RoleID,
                                              RoleName = r.RoleName
                                          }).ToList();

                if(userRoles.Where(x=>x.RoleName == "Supervisor").Count()<=0)
                {
                    MessageBox.Show("สิทธิ์บัญชีผู้ใช้ของท่าน ไม่ใช่ Supervisor ไม่สามารถยืนยันให้จ่ายป้ายได้", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                }

                _result = true;

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UsernameTextBox.Text = "";
                PasswordTextBox.Password = "";
                UsernameTextBox.Focus();

                user_setting.UserRoles = null;
                user_setting.User = null;

                Application.Current.Resources["UserName"] = "Guest";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UsernameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text.Length > 0 && PasswordTextBox.Password.Length > 0)
                    SubmitButton.IsEnabled = true;
                else
                    SubmitButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PasswordTextBox_PasswordChanged(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UsernameTextBox.Text.Length > 0 && PasswordTextBox.Password.Length > 0)
                    SubmitButton.IsEnabled = true;
                else
                    SubmitButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
