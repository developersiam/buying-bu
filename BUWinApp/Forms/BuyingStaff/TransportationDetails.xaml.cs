﻿using BUWinApp.Helper;
using BusinessLayer;
using BusinessLayer.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BUWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for TransportationDetails.xaml
    /// </summary>
    public partial class TransportationDetails : Window
    {
        string _transportationCode;
        m_TransportationDocument _transportationDocument;
        public TransportationDetails(string transportCode)
        {
            InitializeComponent();
            _transportationDocument = new m_TransportationDocument();
            _transportationCode = transportCode;

            TransportationDocumentBinding();
            TransportationDataGridBinding();
        }

        private void TransportationDocumentBinding()
        {
            try
            {
                _transportationDocument = BusinessLayer.Helper.TransportationHelper
                    .GetByTransportationCode(_transportationCode);

                TransportationCodeTextBox.Text = _transportationDocument.TransportationDocumentCode;
                TruckNumberTextBox.Text = _transportationDocument.TruckNumber;
                CreateDateTextBox.Text = _transportationDocument.CreateDate.ToShortDateString();
                TotalBaleTextBox.Text = _transportationDocument.TotalBale.ToString("N0");
                TotalWeightTextBox.Text = Convert.ToDecimal(_transportationDocument.TotalWeight).ToString("N1");
                MaxWeightTextBox.Text = _transportationDocument.MaximumWeight.ToString("N0");

                if(_transportationDocument.IsFinish == true)
                {
                    FinishButton.IsEnabled = false;
                    UnFinishButton.IsEnabled = true;
                    PrintVoucherPreviewButton.IsEnabled = true;
                }
                else
                {
                    FinishButton.IsEnabled = true;
                    UnFinishButton.IsEnabled = false;
                    PrintVoucherPreviewButton.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void TransportationDataGridBinding()
        {
            try
            {
                TransportationDetailsDataGrid.ItemsSource = null;
                TransportationDetailsDataGrid.ItemsSource = BuyingFacade.BuyingBL()
                    .GetByTransportationCode(_transportationCode)
                    .OrderByDescending(x => x.LoadBaleToTruckDate);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            TransportationDocumentBinding();
            TransportationDataGridBinding();
        }

        private void FinishButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ท่านต้องการ Finish ใบนำส่งนี้ใช่หรือไม่?", "warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BuyingFacade.TransportationBL()
                    .Finish(_transportationDocument.TransportationDocumentCode, user_setting.User.Username);

                MessageBox.Show("ใบนำส่งนี้ถูกเปลี่ยนสถานะเป็น Finish แล้ว", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information);

                TransportationDocumentBinding();
                TransportationDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UnFinishButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ท่านต้องการ Unfinish ใบนำส่งนี้ใช่หรือไม่?", "warning", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BuyingFacade.TransportationBL()
                    .Unfinish(_transportationDocument.TransportationDocumentCode, user_setting.User.Username);

                MessageBox.Show("ใบนำส่งนี้ถูกเปลี่ยนสถานะเป็น Unfinish แล้ว", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information);

                TransportationDocumentBinding();
                TransportationDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PrintVoucherPreviewButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_transportationDocument.IsFinish == false)
                    throw new ArgumentException("ใบนำส่งนี้ยังไม่ได้ทำการ Finish ไม่สามารถพิมพ์ใบนำส่งได้");

                TransportationDocumentPrintPreview window = new TransportationDocumentPrintPreview(_transportationCode);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
