﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BusinessLayer.Model;
using BusinessLayer.Helper;
using BUWinApp.Helper;

namespace BUWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for RegisterBarcodeTagInfo.xaml
    /// 
    /// ข้อมูลที่ต้องใช้แสดงในหน้าจอนี้ได้แก่
    /// 1.ข้อมูลเกี่ยวกับประวัติชาวไร่ (BuyingFacade.RegistrationBL())
    /// 2.ข้อมูลโควต้าคงเหลือในแต่ละโปรเจค (BuyingFacade.FarmerProjectBL())
    /// 3.ข้อมูลการขายจำแนกตาม Position (_buyingBL)
    /// 4.ข้อมูลการขายจำแนกตาม Quality (_buyingBL)
    /// 5.ข้อมูลการตรวจ NTRM (_buyingBL)
    /// 6.ข้อมูลใบรายการซื้อขาย (BuyingFacade.BuyingDocumentBL())
    /// 
    /// 
    /// ขั้นตอนการทำงาน
    /// 1.เลือก Suppler
    /// 2.เลือก Buying Station
    /// 3.กรอกรหัสประจำตัวประชาชนเพื่อค้นหาข้อมูลชาวไร่
    /// 4.ดูรายละเอียดข้อมูลที่แสดงบนหน้าจอ
    ///     4.1 หากพบ % ของ NTRM เกิน 10% ให้แจ้งเตือนผู้ใช้
    /// 5.สร้างใบรายการซื้อขาย
    /// 
    /// </summary>


    public partial class RegisterCheckProfile : Page
    {
        RegistrationFarmer _regisModel;
        GAPGroupMember2020 _gapGroupMember;
        List<m_BuyingDocument> _documentList;

        public RegisterCheckProfile()
        {
            InitializeComponent();

            _regisModel = new RegistrationFarmer();
            _gapGroupMember = new GAPGroupMember2020();
            _documentList = new List<m_BuyingDocument>();

            SupplierComboBox.ItemsSource = null;
            SupplierComboBox.ItemsSource = BuyingFacade.SupplierBL().GetByArea(user_setting.User.StaffUser.AreaCode);

            BuyingStationComboBox.ItemsSource = null;
            BuyingStationComboBox.ItemsSource = BuyingFacade.BuyingStationBL().GetByArea(user_setting.User.StaffUser.AreaCode);

            CitizenIDSearchTextBox.Focus();

            CreateDocumentDatePicker.SelectedDate = DateTime.Now;
        }

        private void SearchAndBindFarmerProfile(string citizenID)
        {
            try
            {
                if (citizenID == null)
                {
                    MessageBox.Show("โปรดระบุรหัสประจำตัวประชาชนของชาวไร่", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    CitizenIDSearchTextBox.Focus();
                    return;
                }

                if (citizenID.Length != 13)
                {
                    MessageBox.Show("รหัสประจำตัวประชาชนไม่ใช่ 13 หลัก", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    CitizenIDSearchTextBox.Focus();
                    return;
                }

                if (SupplierComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดเลือกตัวแทน", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    SupplierComboBox.Focus();
                    CitizenIDSearchTextBox.Focus();
                    return;
                }

                if (BuyingStationComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดระบุลานรับซื้อ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BuyingStationComboBox.Focus();
                    CitizenIDSearchTextBox.Focus();
                    return;
                }

                //Clear form **
                FarmerCodeTextBox.Clear();
                FarmerNameTextBox.Clear();
                AddressTextBox.Clear();
                ContractCodeTextBox.Clear();
                ContractQuotaTextBox.Clear();
                GAPGroupCodeTextBox.Clear();
                NTRMStatusLable.Content = "";

                _regisModel = null;

                FarmerBalanceQuotaDataGrid.ItemsSource = null;
                BuyingDocumentDataGrid.ItemsSource = null;
                SummaryOfHMLSoldDataGrid.ItemsSource = null;
                SummaryOfXCBTSoldDataGrid.ItemsSource = null;
                NTRMInspectionDataGrid.ItemsSource = null;

                //Get a farmer from selected supplier *
                var farmer = BuyingFacade.FarmerBL()
                    .GetBySupplierCodeAndCitizenID(SupplierComboBox.SelectedValue.ToString(), citizenID);

                if (farmer == null)
                {
                    MessageBox.Show("ชาวไร่รายนี้ไม่ได้เป็นสมาชิกของ " + SupplierComboBox.SelectedValue.ToString(),
                        "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    CitizenIDSearchTextBox.Focus();
                    return;
                }

                FarmerCodeTextBox.Text = farmer.FarmerCode;
                FarmerNameTextBox.Text = farmer.Person.Prefix + farmer.Person.FirstName + " " + farmer.Person.LastName;
                AddressTextBox.Text = farmer.Person.HouseNumber + " หมู่ " + farmer.Person.Village + " ต." +
                    farmer.Person.Tumbon + " อ." + farmer.Person.Amphur + " จ." + farmer.Person.Province;

                //Get a farmer contract **
                var regisModel = BuyingFacade.RegistrationBL()
                    .GetSingle(user_setting.Crop.Crop1, FarmerCodeTextBox.Text);

                if (regisModel == null)
                {
                    MessageBox.Show("ชาวไร่รหัส " + farmer.FarmerCode + " ไม่ได้ลงทะเบียนทำสัญญาไว้กับ " +
                        SupplierComboBox.SelectedValue.ToString() + " ในปี " + user_setting.Crop.Crop1,
                        "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    CitizenIDSearchTextBox.Focus();
                    return;
                }

                _regisModel = regisModel;

                /// แสดงข้อมูลการลงทะเบียน โควต้า การขาย และใบรายการซื้อขาย **
                ContractCodeTextBox.Text = regisModel.GAPContractCode;
                //EstimateRaiTextBlock.Text = registrationFarmer.EstimatRai.ToString("N0");
                // pen แก้ไข ESTIMATERATO  TO QUOTAFROMSIGNCONTRACT เนื่องจากปี 2018 มีการเปลี่ยนการเรียก column
                ContractQuotaTextBox.Text = regisModel.QuotaFromSignContract.ToString();

                /// NTRM inspection percentages.
                /// 
                //double ntrmInspectionPercentages = BuyingFacade.NtrmInspectionBL().GetNTRMInspectionPercentagesByFarmer(registrationFarmer.FarmerCode);

                //if (ntrmInspectionPercentages > 5)
                //{
                //    FarmerProfileGrid.Background = Brushes.IndianRed;
                //    NTRMStatusLable.Content = "Check NTRM";
                //}
                //else
                //{
                //    FarmerProfileGrid.Background = Brushes.Green;
                //    NTRMStatusLable.Content = "NTRM % Pass";
                //}

                /// NTRM Inspection checking in HighlyDangerous.
                if (BuyingFacade.NtrmInspectionBL().IsHighlyDangerousInspection(regisModel.FarmerCode) == true)
                {
                    NTRMStatusLable.Background = Brushes.Red;
                    NTRMStatusLable.Content = "Check NTRM!";
                }
                else
                {
                    NTRMStatusLable.Background = Brushes.Green;
                    NTRMStatusLable.Content = "NTRM Pass";
                }

                NTRMStatusLable.Visibility = Visibility.Visible;

                var nestingBale = BuyingFacade.BuyingBL().GetNestingBale(_regisModel.Crop, _regisModel.FarmerCode).Count();
                if (nestingBale > 0)
                    MessageBox.Show("ชาวไร่รายนี้เคยมีการบันทึกข้อมูลห่อยาที่มีการยัดไส้ (Nesting) ในปีนี้ จำนวน " + nestingBale + " ห่อ",
                        "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);

                /// Check incident farmer info and show alert if detect meet incident cases.
                /// 
                var incidentList = BuyingFacade.IncidentFarmerBL()
                    .GetByFarmer(user_setting.Crop.Crop1, citizenID);

                if (incidentList.Count() > 0)
                {
                    foreach (var item in incidentList)
                    {
                        MessageBox.Show("ชาวไร่รายนี้พบเหตุการณ์ที่ไม่ได้มาตรฐาน " + Environment.NewLine +
                        "เรื่อง " + item.IncidentDescription.Description + Environment.NewLine +
                        "เมื่อวันที่ " + item.IncidentDate + Environment.NewLine +
                        "สถานะปัจจุบันคือ " + item.IncidentStatus + Environment.NewLine, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }

                BuyingDocumentDataGridBinding(regisModel.Crop, regisModel.FarmerCode);
                FarmerBalanceQuotaDataGridBinding(regisModel.Crop, regisModel.FarmerCode);
                SummaryOfHMLSoldDataGridBinding(regisModel.Crop, regisModel.FarmerCode);
                SummaryOfXCBTSoldDataGridBinding(regisModel.Crop, regisModel.FarmerCode);
                NTRMInspectionDataGridBinding(regisModel.Crop, regisModel.FarmerCode);

                CPACollectionDataGridBinding(regisModel.FarmerCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void NTRMInspectionDataGridBinding(short crop, string farmerCode)
        {
            try
            {
                NTRMInspectionDataGrid.ItemsSource = null;
                NTRMInspectionDataGrid.ItemsSource = BuyingFacade.NtrmInspectionBL()
                    .GetByFarmer(farmerCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingDocumentDataGridBinding(short crop, string farmerCode)
        {
            try
            {
                _documentList = BuyingDocumentHelper.GetByFarmer(crop, farmerCode);

                BuyingDocumentDataGrid.ItemsSource = null;
                BuyingDocumentDataGrid.ItemsSource = _documentList.OrderByDescending(x => x.BuyingDocumentNumber);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FarmerBalanceQuotaDataGridBinding(short crop, string farmerCode)
        {
            try
            {
                if (crop.ToString() == "")
                {
                    MessageBox.Show("ไม่พบข้อมูล crop โปรดลองตรวจสอบข้อมูลใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (farmerCode == "")
                {
                    MessageBox.Show("ไม่พบข้อมูล farmerCode โปรดลองตรวจสอบข้อมูลใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                FarmerBalanceQuotaDataGrid.ItemsSource = null;
                FarmerBalanceQuotaDataGrid.ItemsSource = FarmerProjectHelper
                    .GetQuotaAndSoldByFarmerVersion2(crop, farmerCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SummaryOfXCBTSoldDataGridBinding(short crop, string farmerCode)
        {
            try
            {
                if (crop.ToString() == "")
                {
                    MessageBox.Show("ไม่พบข้อมูล crop โปรดลองตรวจสอบข้อมูลใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (farmerCode == "")
                {
                    MessageBox.Show("ไม่พบข้อมูล farmerCode โปรดลองตรวจสอบข้อมูลใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                SummaryOfXCBTSoldDataGrid.ItemsSource = null;
                SummaryOfXCBTSoldDataGrid.ItemsSource = BuyingFacade.BuyingBL()
                    .GetSummaryBuyingOfFarmerByPosition(crop, farmerCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SummaryOfHMLSoldDataGridBinding(short crop, string farmerCode)
        {
            try
            {
                if (crop.ToString() == "")
                {
                    MessageBox.Show("ไม่พบข้อมูล crop โปรดลองตรวจสอบข้อมูลใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (farmerCode == "")
                {
                    MessageBox.Show("ไม่พบข้อมูล farmerCode โปรดลองตรวจสอบข้อมูลใหม่อีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                SummaryOfHMLSoldDataGrid.ItemsSource = null;
                SummaryOfHMLSoldDataGrid.ItemsSource = BuyingFacade.BuyingBL().GetSummaryBuyingOfFarmerByQuality(crop, farmerCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CPACollectionDataGridBinding(string farmerCode)
        {
            ///Code เดิม 2019
            //List<CPASampleCollection> sampleList = new List<CPASampleCollection>();
            //sampleList = BuyingFacade.CPASampleCollectionBL().GetByFarmer(user_setting.Crop.Crop1, farmerCode);

            _gapGroupMember = BuyingFacade.GAPGroupMember2020BL()
                .GetSingleByCitizenID(user_setting.Crop.Crop1, CitizenIDSearchTextBox.Text);

            if (_gapGroupMember == null)
                return;

            GAPGroupCodeTextBox.Text = _gapGroupMember.GAPGroupCode;

            var sampleList = BuyingFacade.CPASampleCollection2020BL()
                .GetByFarmer(_gapGroupMember.CitizenID, _gapGroupMember.GAPGroupCode);

            CPASampleCollectionDataGrid.ItemsSource = null;
            CPASampleCollectionDataGrid.ItemsSource = sampleList;
            if (sampleList.Count > 0)
            {
                MessageBox.Show("ชาวไร่รายนี้มีการบันทึก CPA Sample Collection แล้ว โปรดดูรายละเอียดได้จากตารางทางด้านซ้ายมือ",
                    "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearForm()
        {
            try
            {
                CitizenIDSearchTextBox.Text = "";
                CitizenIDSearchTextBox.Focus();

                FarmerCodeTextBox.Text = "";
                FarmerNameTextBox.Text = "";
                AddressTextBox.Text = "";
                ContractCodeTextBox.Text = "";
                ContractQuotaTextBox.Text = "";
                GAPGroupCodeTextBox.Text = "";

                FarmerBalanceQuotaDataGrid.ItemsSource = null;
                BuyingDocumentDataGrid.ItemsSource = null;
                SummaryOfHMLSoldDataGrid.ItemsSource = null;
                SummaryOfXCBTSoldDataGrid.ItemsSource = null;
                NTRMInspectionDataGrid.ItemsSource = null;
                NTRMStatusLable.Content = "";
                NTRMStatusLable.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CheckFarmerProfileButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ///เป็นการอ่านข้อมูลจากบัตรประจำตัวประชาชนด้วย Smart card
                if (IDCardReaderCheckBox.IsChecked == true)
                {
                    CitizenIDSearchTextBox.Text = "";

                    //SCardContext cardContext = new SCardContext();
                    //cardContext.Establish(SCardScope.System);

                    //string[] scReader = cardContext.GetReaders();
                    //if (scReader.Length <= 0)
                    //{
                    //    throw new PCSCException(SCardError.NoReadersAvailable, "ไม่พบเครื่องอ่านบัตรสมาร์ทการ์ดที่เชื่อมต่อเข้ากับเครื่องคอมพิวเตอร์ของท่าน.");
                    //}
                    SearchAndBindFarmerProfile(CitizenIDSearchTextBox.Text);
                }

                else
                {
                    if (CitizenIDSearchTextBox.Text == "")
                    {
                        MessageBox.Show("โปรดระบุรหัสประจำตัวประชาชนของชาวไร่", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                        CitizenIDSearchTextBox.Focus();
                        return;
                    }

                    SearchAndBindFarmerProfile(CitizenIDSearchTextBox.Text);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void PrintCAPStickerButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FarmerCodeTextBox.Text == "")
                    throw new ArgumentException("ไม่สามารถพิมพ์ได้ เนื่องจากไม่พบข้อมูล Farmer Code");

                /// Code เดิม
                //if (GAPGroupCodeTextBox.Text == "")
                //    throw new ArgumentException("ไม่สามารถพิมพ์ได้ เนื่องจากไม่พบข้อมูล GAP Group Code");

                if (GAPGroupCodeTextBox.Text == "")
                    throw new ArgumentException("ไม่สามารถพิมพ์ได้ เนื่องจากไม่พบข้อมูล GAP Group Code โปรดตรวจสอบกับแผนกไอที");

                TSCPrinter.openport("TSC TTP-247");
                TSCPrinter.setup("70", "35", "2.0", "6", "0", "0", "0");
                TSCPrinter.sendcommand("GAP  2 mm,0");
                TSCPrinter.sendcommand("DIRECTION 1");
                TSCPrinter.clearbuffer();

                //TSCPrinter.sendcommand("BAR 410,230,2,40");
                //TSCPrinter.sendcommand("BAR 542,230,2,40");
                //TSCPrinter.sendcommand("BAR 410,230,133,2");
                //TSCPrinter.sendcommand("BAR 410,270,133,2");
                //TSCPrinter.windowsfont(435, 240, 30, 0, 0, 0, "arial", "CPA");
                //TSCPrinter.windowsfont(425, 175, 30, 0, 0, 0, "arial", GAPGroupCodeTextBox.Text);
                TSCPrinter.windowsfont(15, 20, 30, 0, 0, 0, "arial", FarmerCodeTextBox.Text + "," + GAPGroupCodeTextBox.Text + "," + CitizenIDSearchTextBox.Text);
                TSCPrinter.barcode("15", "65", "128", "50", "0", "0", "2", "2", FarmerCodeTextBox.Text);
                //TSCPrinter.barcode("15", "60", "128", "50", "0", "0", "2", "2", FarmerCodeTextBox.Text + "0000000000000" + DateTime.Now.ToShortDateString());
                TSCPrinter.windowsfont(15, 120, 80, 0, 0, 0, "cordia new", FarmerNameTextBox.Text);
                TSCPrinter.windowsfont(15, 180, 50, 0, 0, 0, "cordia new", AddressTextBox.Text);

                TSCPrinter.printlabel("1", "1");
                TSCPrinter.closeport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RegisterBarcodeTagButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //เงื่อนไขที่ใช้ในการตรวจสอบก่อนเริ่มต้นการสร้างใบรายการซื้อขายเพื่อจ่ายป้ายบาร์โค้ตให้แก่ชาวไร่มีดังนี้
                /*
                 * 1.ตรวจสอบจำนวนโควต้าคงเหลือ
                 * 2.ตรวจสอบสถานะสัญญา
                 * 3.ตรวจสอบสถานะการซื้อขาย
                 * 4.ตรวจสอบการกำหนดกิโลกรัมต่อไร่ในแต่ละซัพพลายเออร์ (KilogramPerRaiConfiguration table)
                 * 5.ตรวจสอบใบรายการซื้อขายที่ยังไม่ได้ Finish (ต้อง Finish ครบทุกใบรายการซื้อขายจึงจะสร้างใบรายการซื้อขายใหม่ได้)
                 
                 */
                if (FarmerCodeTextBox.Text == "")
                {
                    MessageBox.Show("ไม่พบ FarmerCode โปรดลองตรวจสอบอีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    CitizenIDSearchTextBox.Focus();
                    return;
                }

                if (CreateDocumentDatePicker.SelectedDate.ToString() == "")
                {
                    MessageBox.Show("โปรดระบุวันที่สร้างใบรายการซื้อขาย", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (Convert.ToDateTime(CreateDocumentDatePicker.SelectedDate).Date < DateTime.Now.Date)
                {
                    MessageBox.Show("ไม่อนุญาตให้ทำรายการย้อนหลังได้ โปรดตรวจสอบข้อมูลอีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (BuyingFacade.ChemicalDistributionBL()
                    .GetByFarmer(user_setting.Crop.Crop1,
                    _regisModel.FarmerCode).Count() < 1)
                {
                    MessageBox.Show("ไม่พบข้อมูลการรับปัจจัยการผลิต(ปุ๋ย, ยา)ของชาวไร่รายนี้ " + Environment.NewLine
                        + "ไม่สามารถจ่ายป้ายบาร์โค้ตได้ กรุณาแจ้งผู้จัดการลานรับซื้อ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var farmerQuotaList = FarmerProjectHelper.GetQuotaAndSoldByFarmerVersion2(_regisModel.Crop, _regisModel.FarmerCode);

                if (farmerQuotaList.Count < 1)
                    throw new ArgumentException("ไม่พบโควต้าในแต่ละโปรเจคของชาวไร่รายนี้ โปรดติดต่อเจ้าหน้าที่");

                if (farmerQuotaList.Where(x => x.BalanceTag + x.BalanceExtraTag > 0).Count() <= 0)
                    throw new ArgumentException("จำนวนโควต้าคงเหลือไม่เพียงพอ ไม่สามารถจ่ายป้ายให้ได้ โปรดตรวจสอบข้อมูลอีกครั้ง");

                var gmsInfo = BuyingFacade.GMSFarmerInfoCollectionCriteriaBL()
                    .GetSingle(CitizenIDSearchTextBox.Text);
                if (gmsInfo != null)
                {
                    bool result = true;
                    if (gmsInfo.CropInputQty == false ||
                        gmsInfo.GPSHouseLatLong == false ||
                        gmsInfo.GPSFieldLatLong == false ||
                        gmsInfo.EstOfTransplanting == false ||
                        gmsInfo.EstOfTopping == false ||
                        gmsInfo.TransplantingCompletionDate == false)
                    {
                        GMSFarmerInfoColletionCriterias._citizenID = CitizenIDSearchTextBox.Text;
                        result = GMSFarmerInfoColletionCriterias.ReturnResult();
                    }

                    if (result == false)
                    {
                        MessageBox.Show("ไม่สามารถจ่ายป้ายบาร์โค้ตให้กับชาวไร่ได้ เนื่องจากไม่ได้รับการยืนยันจาก Supervisor", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }

                //สร้างใบรายการซื้อขายใหม่ 
                var newBuyingDocument = BuyingFacade.BuyingDocumentBL()
                    .Add(user_setting.Crop.Crop1,
                    BuyingStationComboBox.SelectedValue.ToString(),
                    FarmerCodeTextBox.Text,
                    user_setting.User.Username,
                    Convert.ToDateTime(CreateDocumentDatePicker.SelectedDate));


                var _document = BuyingDocumentHelper.GetSingle(newBuyingDocument.Crop
                    , newBuyingDocument.BuyingStationCode
                    , newBuyingDocument.FarmerCode
                    , newBuyingDocument.BuyingDocumentNumber);

                RegisterBarcodeTag registerBarcodeTag = new RegisterBarcodeTag(_document, _regisModel);
                registerBarcodeTag.ShowDialog();

                SearchAndBindFarmerProfile(CitizenIDSearchTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ViewBuyingDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BuyingDocumentDataGrid.SelectedIndex <= -1)
                    return;

                if (FarmerBalanceQuotaDataGrid.Items.Count <= 0)
                {
                    MessageBox.Show("ไม่พบข้อมูล การตั้งค่า กก./ไร่ โปรดแจ้งผู้ดูแลระบบหรือแผนกไอที เพื่อดำเนินการดังกล่าว", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var buyingDocument = (BuyingDocument)BuyingDocumentDataGrid.SelectedItem;
                var documentInfo = BuyingDocumentHelper.GetSingle(buyingDocument.Crop
                    , buyingDocument.BuyingStationCode
                    , buyingDocument.FarmerCode
                    , buyingDocument.BuyingDocumentNumber);


                RegisterBarcodeTag registerBarcodeTag = new RegisterBarcodeTag(documentInfo, _regisModel);
                registerBarcodeTag.ShowDialog();

                SearchAndBindFarmerProfile(CitizenIDSearchTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BuyingDocumentDataGrid.SelectedIndex <= -1)
                    return;

                var _document = (m_BuyingDocument)BuyingDocumentDataGrid.SelectedItem;

                if (MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่", "การแจ้งเตือน", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                if (_document.IsFinish == true)
                {
                    MessageBox.Show("ใบรายการซื้อขายนี้ถูกปิดการซื้อขายแล้ว ไม่สามารถลบได้", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (_document.TotalBale > 0)
                {
                    MessageBox.Show("ใบรายการซื้อขายนี้มีข้อมูลห่อยาอยู่ภายในจำนวน " + _document.TotalBale + " ไม่สามารถลบได่้", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                BuyingFacade.BuyingDocumentBL().Delete(_document.Crop, _document.BuyingStationCode, _document.FarmerCode, _document.BuyingDocumentNumber);

                BuyingDocumentDataGridBinding(_document.Crop, _document.FarmerCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CitizenIDSearchTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter)
                return;

            SearchAndBindFarmerProfile(CitizenIDSearchTextBox.Text);
        }

        //private void CPASampleStatusButton_Click(object sender, RoutedEventArgs e)
        //{
        //    try
        //    {
        //        if (_registrationFarmer.FarmerCode == null || _registrationFarmer.FarmerCode == "")
        //        {
        //            MessageBox.Show("ไม่พบข้อมูล Farmer code", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
        //            return;
        //        }

        //        CPASampleCollectionWindow window = new CPASampleCollectionWindow(_registrationFarmer.FarmerCode);
        //        window.ShowDialog();
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
        //    }
    }
}
