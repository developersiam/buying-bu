﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;

namespace BUBuyingSystemWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for RejectPopupAtWeightScale.xaml
    /// </summary>
    public partial class RejectPopupAtWeightScale : Window
    {
        string _baleBarcode;
        Buying _buying;
        List<Role> _roleName;
        BUBuyingSystemConfiguration _config = BUBuyingSystemConfiguration.GetInstance();

        public RejectPopupAtWeightScale(Buying buying, List<Role> roleName)
        {
            InitializeComponent();
            _buying = new Buying();
            _roleName = new List<Role>();
            _roleName = roleName;
            _buying = buying;
            _baleBarcode = _buying.BaleBarcode;

            List<RejectReasonStruct> rejectReasonList = new List<RejectReasonStruct>();

            rejectReasonList.Add(new RejectReasonStruct { RejectReason = "Mix Grade (ปน)" });
            rejectReasonList.Add(new RejectReasonStruct { RejectReason = "Moisture (ชื้น)" });
            rejectReasonList.Add(new RejectReasonStruct { RejectReason = "Buyer Reject (ไม่ซื้อ)" });
            rejectReasonList.Add(new RejectReasonStruct { RejectReason = "X low grade over limit" });
            rejectReasonList.Add(new RejectReasonStruct { RejectReason = "Farmer Reject (ไม่ขาย)" });
            rejectReasonList.Add(new RejectReasonStruct { RejectReason = "Green (เขียว)" });
            rejectReasonList.Add(new RejectReasonStruct { RejectReason = "Yellow (เหลือง)" });
            rejectReasonList.Add(new RejectReasonStruct { RejectReason = "NTRM" });
            rejectReasonList.Add(new RejectReasonStruct { RejectReason = "Nesting (ยัดไส้)" });

            RejectReasonItemsControl.ItemsSource = null;
            RejectReasonItemsControl.ItemsSource = rejectReasonList;
        }

        struct RejectReasonStruct
        {
            public string RejectReason { get; set; }
        }

        private void RejectReasonButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                BuyingFacade.BuyingBL().RejectBaleOnTheScale(_buying.BaleBarcode,
                    btn.Content.ToString(),
                    _config.currentUser);

                MessageBox.Show("ยกเลิกห่อยาสำเร็จ!", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information);

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
