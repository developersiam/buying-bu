﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using Microsoft.Reporting.WinForms;
using BUWinApp.Helper;

namespace BUWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for TransportationDocumentPrintPreview.xaml
    /// </summary>
    public partial class TransportationDocumentPrintPreview : Window
    {
        string _transportationDocumentCode;

        public TransportationDocumentPrintPreview(string transportationDocumentCode)
        {
            InitializeComponent();
            _transportationDocumentCode = transportationDocumentCode;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                reportViewer.Reset();

                var datasource1 = BuyingFacade.StoreProcetureBL().sp_GetTransportationDocumentByTransportationCode(_transportationDocumentCode);
                var datasource2 = BuyingFacade.StoreProcetureBL().sp_GetTransportationDetailsByTransportationCode(_transportationDocumentCode);

                ReportDataSource sp_GetTransportationDetailsByTransportationCodeDataSource = new ReportDataSource();
                ReportDataSource sp_GetTransportationDocumentByTransportationCodeDataSource = new ReportDataSource();

                sp_GetTransportationDocumentByTransportationCodeDataSource.Name = "sp_GetTransportationDocumentByTransportationCode";
                sp_GetTransportationDocumentByTransportationCodeDataSource.Value = datasource1;                

                sp_GetTransportationDetailsByTransportationCodeDataSource.Name = "sp_GetTransportationDetailsByTransportationCode";
                sp_GetTransportationDetailsByTransportationCodeDataSource.Value = datasource2;

                reportViewer.LocalReport.DataSources.Add(sp_GetTransportationDocumentByTransportationCodeDataSource);
                reportViewer.LocalReport.DataSources.Add(sp_GetTransportationDetailsByTransportationCodeDataSource);

                reportViewer.LocalReport.ReportEmbeddedResource = "BUWinApp.RDLCReport.RPTBUY002.rdlc";
                reportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
