﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using System.IO.Ports;
using System.Windows.Threading;
using System.Threading;
using BusinessLayer.BusinessObject;

namespace BUBuyingSystemWinApp.BuyingStaff
{
    /// <summary>
    /// Interaction logic for CaptureWeightMainScreen.xaml
    /// 
    /// ขั้นตอนการทำงานของระบบ
    /// 1. ผู้ใช้สแกนบาร์โค้ตห่อยาที่ต้องการชั่งน้ำหนัก
    /// 2. ระบบแสดงข้อมูลได้แก่
    ///     2.1 ข้อมูลประวัติชาวไร่ ประวัติการลงทะเบียน
    ///     2.2 ข้อมูลใบรายการซื้อขาย
    ///     2.3 ข้อมูลโควต้าคงเหลือ ยอดรวมที่ขายไป
    ///     2.4 Summary of Sold by Position
    ///     2.5 Summary of Sold by Quality
    ///     2.6 ข้อมูลเกี่ยวกับห่อยาที่สแกนหมายเลขบาร์โค้ต
    ///     2.7 รายการห่อยาที่ทำการรับป้าย บันทึกเกรดซื้อ บันทึกน้ำหนักและขนขึ้นรถยรรทุก
    /// 3. ผู้ใช้บันทึกน้ำหนัก
    /// 
    /// เงื่อนไขที่ใช้ตรวจสอบข้อมูลก่อนการบันทึกน้ำหนัก
    /// 1. มีห่อยาในระบบหรือไม่
    /// 2. ห่อยานั้นได้ถูกบันทึกเกรดมาแล้วหรือไม่
    /// 3. ห่อยาที่ถูกบันทึกเกรดแล้วจะต้องไม่ใช่ห่อยาที่ถูก Back
    /// 4. ห่อยานั้นจะต้องยังไม่ถูกขนขึ้นรถบรรทุก 
    /// 5. ใบรายการซื้อขายจะต้องไม่ถูก Finish
    /// 6. สถานะสัญญาของชาวไร่จะต้อง Active
    /// 7. สถานะการขายของชาวไร่จะต้อง Active
    /// 8. X low grade limit จะต้องไม่เกิน 3.5%
    /// 9. น้ำหนักรวมที่ขายไปแล้วจะต้องไม่เกินจำนวนโควต้าที่ตั้งไว้ในแต่ละโปรเจค
    /// 
    /// </summary>
    public partial class CaptureGradeWeightLoadTruckMainScreen : Window
    {
        IBuyingBL _buyingBL;
        IBuyingDocumentBL _buyingDocumentBL;
        IBuyerBL _buyerBL;
        IBuyingStationBL _buyingStationBL;
        IBuyingGradeBL _buyingGradeBL;
        IRegistrationBL _registrationBL;
        IFarmerProjectBL _farmerProjectBL;
        ITransportationBL _transportationBL;

        PricingSet _pricingSet;
        BuyingDocumentInfo _buyingDocumentInfo;
        RegistrationFarmer _registrationFarmer;
        Buying _buying;
        List<FarmerProjectAndSold> _farmerProjectAndSoldList;

        CaptureWeightSecondaryScreen monitor2;

        BUBuyingSystemConfiguration _config = BUBuyingSystemConfiguration.GetInstance();

        SerialPort serialPort = new SerialPort();
        string lineReadIn;

        private delegate void preventCrossThreading(string x);
        private preventCrossThreading accessControlFromCentralThread;

        struct WeighString
        {
            public int index { get; set; }
            public string WeightString { get; set; }
        }

        private void SetupSerialPort()
        {
            try
            {
                if (SerialPort.GetPortNames().Count() < 1)
                {
                    MessageBox.Show("ไม่พบเครื่องชั่งดิจิตอลที่เชื่อมต่อมายังเครื่องคอมพิวเตอร์นี้ โปรดตรวจสอบอุปกรณ์ หรือแจ้งแผนกไอทีเพื่อทำการตรวจสอบ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);

                    IsDigitalScaleCheckBox.IsChecked = true;

                    WeightTextBox.IsReadOnly = false;
                    WeightTextBox.Text = "";

                    return;
                }

                serialPort.PortName = Properties.Settings.Default.PortName;
                serialPort.BaudRate = Properties.Settings.Default.BaudRate;
                serialPort.Parity = Properties.Settings.Default.Parity == "N" ? Parity.None : Parity.Even;
                serialPort.DataBits = Properties.Settings.Default.Databit;
                serialPort.StopBits = Properties.Settings.Default.StopBits == "N" ? StopBits.None : StopBits.One;

                serialPort.Open();

                if (serialPort.IsOpen)
                {
                    accessControlFromCentralThread = displayTextReadIn;
                    serialPort.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error message: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        // this, hopefully, will prevent cross threading.
        private void displayTextReadIn(string ToBeDisplayed)
        {
            if (WeightTextBox.Dispatcher.CheckAccess())
            {
                WeightTextBox.Text = ToBeDisplayed;
                monitor2.WeightLabel.Content = WeightTextBox.Text;
            }
            else
            {
                WeightTextBox.Dispatcher.Invoke(() =>
                {
                    WeightTextBox.Text = ToBeDisplayed;
                    monitor2.WeightLabel.Content = WeightTextBox.Text;
                });
            }
        }

        // this is called when the serial port has receive-data for us.
        private void port_DataReceived(object sender, SerialDataReceivedEventArgs rcvdData)
        {
            //if (Properties.Settings.Default.StationCode == "STEC")
            //{
            //    //******************************************************************************
            //    //******************************************************************************
            //    //STEC Go down 2 digital scale.
            //    lineReadIn = lineReadIn + serialPort.ReadExisting();
            //    Thread.Sleep(300);

            //    // display what we've acquired.
            //    string resultInput = lineReadIn;



            //    string[] splitString;
            //    string[] stringSeparators = new string[] { "\r" };

            //    splitString = resultInput.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

            //    if (splitString.Count() < 2)
            //        return;


            //    string[] splitString2;

            //    splitString2 = splitString[1].Split(' ');

            //    int i = 0;
            //    List<WeighString> resultWeightString = new List<WeighString>();

            //    foreach (string item in splitString2)
            //    {
            //        if (item != "")
            //        {
            //            resultWeightString.Add(new WeighString { index = i, WeightString = item.Replace(" ", "") });
            //            i++;
            //        }
            //    }

            //    if (resultWeightString.Where(r => r.index == 1).Count() < 1)
            //        return;//resultInput = "";
            //    else
            //        resultInput = resultWeightString.Where(r => r.index == 1).FirstOrDefault().WeightString;

            //    double resultWeight;

            //    if (resultInput != "")
            //    {
            //        resultWeight = Convert.ToDouble(resultInput);

            //        if (resultWeight > 0)
            //            resultWeight = resultWeight / 10;

            //        string weight = resultWeightString.Where(r => r.index == 0).FirstOrDefault().WeightString;

            //        resultWeight = resultWeight - 2.3;
            //        resultInput = resultWeight.ToString("N1");
            //    }
            //    else
            //    {
            //        resultInput = "0.0";
            //    }

            //    displayTextReadIn(resultInput);
            //    lineReadIn = "";
            //}
            //else if (Properties.Settings.Default.StationCode == "CR")
            //{
            //******************************************************************************
            //******************************************************************************
            //Chiangrai digital scale.

            lineReadIn += serialPort.ReadExisting();
            Thread.Sleep(300);

            // display what we've acquired.
            string resultInput = lineReadIn;

            if (resultInput.Length <= Properties.Settings.Default.SubStringLength)
                return;

            //if (resultInput.IndexOf("U") == 0)
            //    return;

            string[] splitString;
            string[] stringSeparators = new string[] { "\r\n" };

            splitString = resultInput.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

            if (splitString.Count() < 2)
                return;

            resultInput = splitString[1];

            resultInput = resultInput.Replace(" ", string.Empty);
            resultInput = resultInput.Replace("", string.Empty);


            foreach (char item in Properties.Settings.Default.ReplaceChar)
                resultInput = resultInput.Replace(item.ToString(), "");

            if (resultInput != "")
            {
                decimal resultWeight = Convert.ToDecimal(resultInput);

                ///ถ้าใช้ในระบบ BU Buying System จะไม่มีการหักน้ำหนักกระสอบในระบบ จะใช้การหักน้ำหนักกระสอบจากการ Tare ที่หน้าเครื่องชั่งเอง
                //resultInput = (resultWeight - Convert.ToDecimal(2.3)).ToString("N1");
                resultInput = resultWeight.ToString("N1");
            }

            displayTextReadIn(resultInput);
            lineReadIn = "";

            //******************************************************************************
            //******************************************************************************
            //}
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            monitor2.Close();

            if (serialPort.IsOpen)
            {
                e.Cancel = true; //cancel the fom closing

                Thread CloseDown = new Thread(new ThreadStart(CloseSerialOnExit)); //close port in new thread to avoid hang

                CloseDown.Start(); //close port in new thread to avoid hang
            }
        }

        private void CloseSerialNonExit()
        {
            try
            {
                serialPort.DataReceived -= port_DataReceived;
                serialPort.Close(); //close the serial port
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); //catch any serial port closing error messages
            }
        }

        private void CloseSerialOnExit()
        {
            try
            {
                serialPort.DataReceived -= port_DataReceived;
                serialPort.Close(); //close the serial port
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message); //catch any serial port closing error messages
            }

            //this.Dispatcher.BeginInvoke(new EventHandler(NowClose)); //now close back in the main thread
            this.Dispatcher.Invoke(() =>
            {
                this.Close();
            });
        }

        private void NowClose(object sender, EventArgs e)
        {
            this.Close(); //now close the form
        }







        public CaptureGradeWeightLoadTruckMainScreen()
        {
            InitializeComponent();

            _transportationBL = new TransportationBL();

            _buyingBL = new BuyingBL();
            _buyerBL = new BuyerBL();
            _buyingStationBL = new BuyingStationBL();
            _buyingDocumentBL = new BuyingDocumentBL();
            _buyingGradeBL = new BuyingGradeBL();
            _registrationBL = new RegistrationBL();
            _farmerProjectBL = new FarmerProjectBL();

            _pricingSet = new PricingSet();
            _buyingDocumentInfo = new BuyingDocumentInfo();
            _registrationFarmer = new RegistrationFarmer();
            _buying = new Buying();
            _farmerProjectAndSoldList = new List<FarmerProjectAndSold>();

            monitor2 = new CaptureWeightSecondaryScreen();

            CreateDateDatePicker.SelectedDate = DateTime.Now;

            BuyerComboBox.ItemsSource = null;
            BuyerComboBox.ItemsSource = _buyerBL.GetAllBuyers().OrderBy(b => b.BuyerCode);

            BuyingGradeComboBox.ItemsSource = null;
            BuyingGradeComboBox.ItemsSource = _buyingGradeBL.GetBuyingGradeByDefaultPricingSet();

            BuyingStationComboBox.ItemsSource = null;
            BuyingStationComboBox.ItemsSource = _buyingStationBL.GetAllStations().OrderBy(bs => bs.StationCode);
        }

        private void TransportationDocumentComboBoxDataBinding()
        {
            try
            {
                TransportationDocumentComboBox.ItemsSource = null;
                TransportationDocumentComboBox.ItemsSource = _transportationBL.GetTransportationByStationAndCrop(_config.currentCrop,
                    BuyingStationComboBox.SelectedValue.ToString(),
                    Convert.ToDateTime(CreateDateDatePicker.SelectedDate)).ToList().
                    OrderByDescending(t => t.TransportationDocumentCode);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void TransportationDocumentDataBinding()
        {
            try
            {
                if (TransportationDocumentComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดเลือกใบนำส่งที่ต้องการพิมพ์", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                TransportationDocumentInfo transportationDocumentInfo = new TransportationDocumentInfo();
                transportationDocumentInfo = _transportationBL.GetTransportationByTransportationCode(TransportationDocumentComboBox.SelectedValue.ToString());


                MaximumWeightTextBox.Text = transportationDocumentInfo.MaximumWeight.ToString("N0");
                LoadingWeightTextBox.Text = Convert.ToInt16(transportationDocumentInfo.TotalWeight).ToString("N0");
                TotalBaleTextBox.Text = transportationDocumentInfo.TotalBale.ToString();
                RemainingWeightTextBox.Text = (transportationDocumentInfo.MaximumWeight - Convert.ToInt16(transportationDocumentInfo.Buyings.Sum(x => x.Weight))).ToString("N0");
                TruckNumberTextBox.Text = transportationDocumentInfo.TruckNumber;


                if (transportationDocumentInfo.IsFinish == true)
                {
                    FinishButton.IsEnabled = false;
                    UnFinishButton.IsEnabled = true;
                    PrintButton.IsEnabled = true;
                }
                else
                {
                    FinishButton.IsEnabled = true;
                    UnFinishButton.IsEnabled = false;
                    PrintButton.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearForm()
        {
            try
            {
                FarmerCodeTextBlock.Text = "";
                FarmerNameTextBlock.Text = "";
                GAPGroupCodeTextBox.Text = "";
                ContractStatusCheckBox.IsChecked = false;

                BaleBarcodeTextBox.Text = "";

                DocumentCodeTextBlock.Text = "";
                TotalBaleTextBlock.Text = "";
                TotalScanTextBlock.Text = "";
                TotalRejectTextBlock.Text = "";
                FinishStatusCheckBox.IsChecked = false;

                WeightTextBox.Text = "";

                BuyingDataGrid.ItemsSource = null;
                FarmerBalanceQuotaDataGrid.ItemsSource = null;
                FarmerBalanceQuotaInExtraDataGrid.ItemsSource = null;

                TotalWeightTextBox.Text = "0";

                BaleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private bool CheckBarcodeInfo(string baleBarcode)
        {
            //Check invalid STEC barcode digit(17 digit).
            if (baleBarcode.Length != 17)
            {
                MessageBox.Show("หมายเลขบาร์โค้ตจะต้องมี 17 หลักเท่านั้น", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            //Check invalid prifix STEC barcode (2 digit).
            if (baleBarcode.Substring(0, 2) != "00" &&
                baleBarcode.Substring(0, 2) != "01" &&
                baleBarcode.Substring(0, 2) != "02" &&
                baleBarcode.Substring(0, 2) != "03" &&
                baleBarcode.Substring(0, 2) != "04" &&
                baleBarcode.Substring(0, 2) != "06")
            {
                MessageBox.Show("ตัวเลข 2 หลักด้านหน้าของบาร์โค้ตจะต้องเป็น 00,01,02,03,04,06 เท่านั้น", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            double n;
            bool isNumeric = double.TryParse(baleBarcode, out n);
            if (isNumeric == false)
            {
                MessageBox.Show("หมายเลข Bale barcode ทุกตัวจะต้องเป็นตัวเลขทั้งหมด", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }

            return true;
        }

        private void BuyingDocumentDetailsDataBinding(Buying buying)
        {
            try
            {
                //Set ค่าให้กับ object เพื่อเอาไว้ใช้เป็นพารามิเตอร์หลักในการบันทึกเกรด
                _buyingDocumentInfo = _buyingDocumentBL.GetBuyingDocumentInfoByBuyingDocument(buying.Crop,
                    buying.BuyingStationCode,
                    buying.FarmerCode,
                    buying.BuyingDocumentNumber);


                DocumentCodeTextBlock.Text = _buyingDocumentInfo.DocumentCode;
                TotalBaleTextBlock.Text = _buyingDocumentInfo.TotalBale.ToString("N0");
                TotalScanTextBlock.Text = _buyingDocumentInfo.TotalScan.ToString("N0");
                TotalRejectTextBlock.Text = _buyingDocumentInfo.TotalReject.ToString();
                FinishStatusCheckBox.IsChecked = _buyingDocumentInfo.IsFinish;

                BuyingDataGrid.ItemsSource = null;
                BuyingDataGrid.ItemsSource = _buyingDocumentInfo.Buyings.OrderByDescending(x => x.WeightDate);

                //TotalIntemsTextBlock.Text = BuyingDataGrid.Items.Count.ToString();

                TotalWeightTextBox.Text = _buyingDocumentInfo.TotalCaptureWeight + "/" + _buyingDocumentInfo.TotalScan;

                monitor2.BaleLabel.Content = TotalWeightTextBox.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SearchBuyingInfoFromBaleBarcode(string baleBarcode)
        {
            try
            {
                if (baleBarcode == "")
                {
                    MessageBox.Show("โปรดสแกนบาร์โค้ตเพื่อเริ่มต้นค้นหาข้อมูล", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    return;
                }


                //Set ค่าให้กับ object เพื่อเอาไว้ใช้เป็นพารามิเตอร์หลักในการบันทึกเกรด
                _buying = _buyingBL.GetBuyingByBaleBarcode(baleBarcode);


                if (_buying == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลยาห่อนี้ในระบบ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


                if (_pricingSet == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลการกำหนดชุดราคาให้กับเกรดซื้อในระบบ จะทำให้ไม่สามารถบันทึกเกรดซื้อ โปรดติดต่อแผนกไอที", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                FarmerCodeTextBlock.Text = "";
                FarmerNameTextBlock.Text = "";
                GAPGroupCodeTextBox.Text = "";
                ContractStatusCheckBox.IsChecked = false;

                DocumentCodeTextBlock.Text = "";
                TotalBaleTextBlock.Text = "";
                TotalScanTextBlock.Text = "";
                TotalRejectTextBlock.Text = "";
                FinishStatusCheckBox.IsChecked = false;

                WeightTextBox.Text = "";

                monitor2.GradeLabel.Content = "";
                monitor2.FarmerNameLable.Content = "";
                monitor2.BaleLabel.Content = "";

                BuyingDataGrid.ItemsSource = null;
                FarmerBalanceQuotaDataGrid.ItemsSource = null;
                FarmerBalanceQuotaInExtraDataGrid.ItemsSource = null;

                TotalWeightTextBox.Text = "0";

                BuyingDocumentDetailsDataBinding(_buying);
                


                //Set ค่าให้กับ object เพื่อเอาไว้ใช้เป็นพารามิเตอร์หลักในการบันทึกน้ำหนัก
                _registrationFarmer = _registrationBL.GetRegistrationByFarmerAndCrop(_buying.Crop, _buying.FarmerCode);


                FarmerCodeTextBlock.Text = _registrationFarmer.FarmerCode;
                FarmerNameTextBlock.Text = _registrationFarmer.Farmer.Person.Prefix
                    + _registrationFarmer.Farmer.Person.FirstName + " "
                    + _registrationFarmer.Farmer.Person.LastName;


                GAPGroupCodeTextBox.Text = _registrationFarmer.GAPGroupCode;
                ContractStatusCheckBox.IsChecked = _registrationFarmer.ActiveStatus;


                //Get and set FarmerProjectAndSoldList
                _farmerProjectAndSoldList = _farmerProjectBL.GetFarmerProjectAndSoldByFarmer(_registrationFarmer.Crop, _registrationFarmer.FarmerCode);

                FarmerBalanceQuotaDataGrid.ItemsSource = _farmerProjectAndSoldList;
                FarmerBalanceQuotaInExtraDataGrid.ItemsSource = _farmerProjectAndSoldList;


                monitor2.FarmerNameLable.Content = FarmerNameTextBlock.Text;

                if (BuyingGradeComboBox.SelectedIndex >= 0)
                {
                    monitor2.GradeLabel.Content = BuyingGradeComboBox.SelectedValue.ToString();
                    BuyerComboBox.SelectedValue = _buying.BuyerCode;
                }


                if (_buying.Grade == null && _buying.RejectReason != null)
                {
                    monitor2.GradeLabel.Content = "แบ็ค";

                    MessageBox.Show("ยาห่อนี้ถูกแบ็ค (" + _buying.RejectReason + ") ไม่สามารถนำมาชั่งน้ำหนักได้ โปรดตรวจสอบข้อมูลจากป้ายหน้าห่อยาอีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);

                    BaleBarcodeTextBox.Text = "";
                    BaleBarcodeTextBox.Focus();

                    monitor2.GradeLabel.Content = "";
                    monitor2.FarmerNameLable.Content = "";
                    monitor2.BaleLabel.Content = "";
                    return;
                }
                else if (_buying.Grade == null && _buying.RejectReason == null)
                {
                    monitor2.GradeLabel.Content = "-";
                }
                else
                {
                    BuyingGradeComboBox.SelectedIndex = 0;
                    BuyingGradeComboBox.SelectedValue = _buying.Grade;
                    BuyerComboBox.SelectedValue = _buying.BuyerCode;
                    monitor2.GradeLabel.Content = _buying.Grade;
                }

                WeightTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void CaptureWeight()
        {
            try
            {
                /// User validation ****************************************************************************************************************
                /// 
                if (CheckBarcodeInfo(BaleBarcodeTextBox.Text) == false)
                    return;


                if(_buyingDocumentInfo.IsFinish == true)
                {
                    MessageBox.Show("ใบรายการซื้อขายนี้ถูก Finish ไปแล้ว ไม่สามารถบันทึกข้อมูลได้", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (TransportationDocumentComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดเลือกใบนำส่ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


                TransportationDocument transportation = new TransportationDocument();
                transportation = _transportationBL.GetTransportationDocumentById(TransportationDocumentComboBox.SelectedValue.ToString());

                if (transportation.IsFinish == true)
                {
                    MessageBox.Show("ใบนำส่งนี้ถูก Finish แล้ว ไม่สามารถบันทึกน้ำหนักห่อยาได้ โปรดเลือกใบนำส่งอื่นในการบันทึกข้อมูล", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }



                if (WeightTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุน้ำหนักห่อยา", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (_buying.Weight != null)
                {
                    if (MessageBox.Show("ยาห่อนี้ถูกบันทึกน้ำหนักซื้อไปก่อนหน้านี้แล้ว ท่านต้องการแก้ไขน้ำหนักซื้อใช่หรือไม่", "การแจ้งเตือน", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                        return;
                }

                if(BuyingGradeComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดระบุเกรดซื้อ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (BuyerComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดระบุชื่อผู้ซื้อ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                /// User validation *****************************************************************************************************************





                /// Create buying object for update info ********************************************************************************************
                /// 
                BuyingDocument buyingDoc = new BuyingDocument
                {
                    Crop = _buyingDocumentInfo.Crop,
                    BuyingStationCode = _buyingDocumentInfo.BuyingStationCode,
                    FarmerCode = _buyingDocumentInfo.FarmerCode,
                    BuyingDocumentNumber = _buyingDocumentInfo.BuyingDocumentNumber,
                    FinishDate = _buyingDocumentInfo.FinishDate,
                    IsFinish = _buyingDocumentInfo.IsFinish
                };

                /// Create buying object for update info ********************************************************************************************.
                /// 


                BuyingGrade buyingGrade = new BuyingGrade();
                buyingGrade = (BuyingGrade)BuyingGradeComboBox.SelectedItem;

                _buyingBL.CaptureBuyingGrade(BaleBarcodeTextBox.Text, 
                    _config.currentCrop, 
                    buyingGrade.PricingNumber, 
                    buyingGrade.Grade, 
                    BuyerComboBox.SelectedValue.ToString(), 
                    _config.currentUser, 
                    _registrationFarmer, 
                    buyingDoc);


                _buyingBL.CaptureBuyingWeight(BaleBarcodeTextBox.Text, 
                    Convert.ToDecimal(WeightTextBox.Text), 
                    _config.currentUser, 
                    _registrationFarmer, 
                    buyingDoc, 
                    _farmerProjectAndSoldList);


                _buyingBL.LoadBaleToTruck(BaleBarcodeTextBox.Text, 
                    TransportationDocumentComboBox.SelectedValue.ToString(), 
                    _config.currentUser);


                SearchBuyingInfoFromBaleBarcode(BaleBarcodeTextBox.Text);
                BaleBarcodeTextBox.Text = "";
                BaleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error message: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BaleBarcodeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter && BaleBarcodeTextBox.Text.Length == 17)
                    SearchBuyingInfoFromBaleBarcode(BaleBarcodeTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ClearFormButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void IsDigitalScaleCheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (WeightTextBox.IsReadOnly == true)//หมายถึง สั่งให้มีการรับค่าจากคีย์บอร์ดแทนการรับค่าจากเครื่องชั่งดิจิตอล ให้ปิดการทำงานของ Com port
                {
                    if (serialPort.IsOpen)
                    {
                        Thread CloseDown = new Thread(new ThreadStart(CloseSerialNonExit)); //close port in new thread to avoid hang

                        CloseDown.Start(); //close port in new thread to avoid hang
                    }

                    IsDigitalScaleCheckBox.IsChecked = true;
                    WeightTextBox.Text = "";
                    WeightTextBox.IsReadOnly = false;
                }
                else//หมายถึง ให้มีการรับค่าจากเครื่องชั่งดิจิตอล แทนการรับค่าจากคีย์บอร์ด จึงสั่งให้โปรแกรมเปิดการเชื่อมต่อของ Com port เพื่อรับข้อมูลจากเครื่องชั่งดิจิตอล
                {
                    WeightTextBox.Text = "";
                    WeightTextBox.IsReadOnly = true;

                    IsDigitalScaleCheckBox.IsChecked = false;

                    SetupSerialPort();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error message: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ViewDetailsButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (WeightTextBox.IsReadOnly == true)//หมายถึง สั่งให้มีการรับค่าจากคีย์บอร์ดแทนการรับค่าจากเครื่องชั่งดิจิตอล ให้ปิดการทำงานของ Com port
                {
                    if (serialPort.IsOpen)
                    {
                        Thread CloseDown = new Thread(new ThreadStart(CloseSerialNonExit)); //close port in new thread to avoid hang

                        CloseDown.Start(); //close port in new thread to avoid hang
                    }

                    WeightTextBox.Text = "";
                    WeightTextBox.IsReadOnly = false;

                    IsDigitalScaleCheckBox.IsChecked = true;
                }
                else//หมายถึง ให้มีการรับค่าจากเครื่องชั่งดิจิตอล แทนการรับค่าจากคีย์บอร์ด จึงสั่งให้โปรแกรมเปิดการเชื่อมต่อของ Com port เพื่อรับข้อมูลจากเครื่องชั่งดิจิตอล
                {
                    WeightTextBox.Text = "";
                    WeightTextBox.IsReadOnly = true;

                    IsDigitalScaleCheckBox.IsChecked = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error message: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void WeightTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter && IsDigitalScaleCheckBox.IsChecked == true && WeightTextBox.Text != "")
                    CaptureWeight();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Escape)
                    ClearForm();

                if (e.Key == Key.F12)
                    CaptureWeight();

                if (e.Key == Key.Enter && BuyingGradeComboBox.SelectedIndex > -1 && BaleBarcodeTextBox.Text.Length == 17)
                    CaptureWeight();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                SetupSerialPort();

                if (System.Windows.Forms.Screen.AllScreens.Length > 1)
                {
                    var screen2 = System.Windows.Forms.Screen.AllScreens[1];
                    var rectangle2 = screen2.WorkingArea;

                    monitor2.Top = rectangle2.Top;
                    monitor2.Left = rectangle2.Left;

                    monitor2.Show();
                }

                BaleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void ChangeBuyingGradeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CheckBarcodeInfo(BaleBarcodeTextBox.Text) == false)
                    return;

                if (_registrationFarmer == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลการลงทะเบียนในระบบ โปรดตรวจสอบข้อมูลอีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (_buyingDocumentInfo == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลเกี่ยวกับใบรายการซื้อขายในระบบ โปรดตรวจสอบข้อมูลอีกครั้ง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                ChangeBuyingGradeAtWeight changeGrade = new ChangeBuyingGradeAtWeight(BaleBarcodeTextBox.Text, _registrationFarmer, _buyingDocumentInfo);
                changeGrade.ShowDialog();

                SearchBuyingInfoFromBaleBarcode(BaleBarcodeTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error message: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void RejectAtWeightScaleButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BaleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดสแกนหมายเลขบาร์โค้ตที่ต้องการ Reject", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (BuyerComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดระบุ Buyer", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (MessageBox.Show("การยกเลิกห่อยา (Reject) อาจส่งผลกระทบต่อข้อมูลการซื้อขาย โปรดแจ้งผู้ควบคุมการซื้อขายเพื่อทำการยกเลิกห่อยานี้ " + Environment.NewLine + "ท่านต้องการยกเลิกห่อยานี้หรือไม่? หากใช่ กด Yes และทำการยืนยันตัวตน", "การแจ้งเตือน", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;


                RejectPopupAtWeightScale rejectOnTheScale = new RejectPopupAtWeightScale(_buying, new List<Role>());
                rejectOnTheScale.ShowDialog();

                SearchBuyingInfoFromBaleBarcode(BaleBarcodeTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error message: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void NewTransportationDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BuyingStationComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดเลือกลานรับซื้อที่ต้องการสร้างใบรายการซื้อขายก่อน", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                CreateTransportationDocumentFromCaptureWeightScreen createTransportationDocument = new CreateTransportationDocumentFromCaptureWeightScreen(BuyingStationComboBox.SelectedValue.ToString(), Convert.ToDateTime(CreateDateDatePicker.SelectedDate));
                createTransportationDocument.ShowDialog();

                TransportationDocumentComboBoxDataBinding();
                TransportationDocumentComboBox.SelectedIndex = 0;

                TransportationDocumentDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error message: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BuyingStationComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (BuyingStationComboBox.SelectedIndex <= -1)
                    return;

                TransportationDocumentComboBoxDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error message: " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void FinishButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (TransportationDocumentComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดเลือกใบนำส่งที่ต้องการพิมพ์", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                _transportationBL.FinishTransportationDocument(TransportationDocumentComboBox.SelectedValue.ToString(), _config.currentUser);
                TransportationDocumentDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void UnFinishButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (TransportationDocumentComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดเลือกใบนำส่งที่ต้องการพิมพ์", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                _transportationBL.UnfinishTransportationDocument(TransportationDocumentComboBox.SelectedValue.ToString(), _config.currentUser);
                TransportationDocumentDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void PrintButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (TransportationDocumentComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดเลือกใบนำส่งที่ต้องการพิมพ์", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                TransportationDocumentPrintPreview printDocumentVoucher = new TransportationDocumentPrintPreview(TransportationDocumentComboBox.SelectedValue.ToString());
                printDocumentVoucher.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BuyingGradeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (BuyingGradeComboBox.SelectedIndex <= -1)
                    return;

                monitor2.GradeLabel.Content = BuyingGradeComboBox.SelectedValue.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void TransportationDocumentComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (TransportationDocumentComboBox.SelectedIndex <= -1)
                    return;

                TransportationDocumentDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CaptureWeight();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Exception", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
