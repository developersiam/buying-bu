﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BusinessLayer.Model;
using BusinessLayer.Helper;
using BUWinApp.Helper;

namespace BUWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for RegisterBarcodeTag.xaml
    /// 
    /// ขั้นตอนการทำงานของระบบ
    /// 1.ผู้ใช้สแกนหมายเลขบาร์โค้ตจากป้ายบาร์โค้ต
    /// 2.ระบบทำการตรวจสอบข้อมูล
    /// 3.ระบบบันทึกข้อมูลการจ่ายป้ายบาร์โค้ต
    /// 4.ระบบแสดงผลข้อมูลรายการป้ายบาร์โค้ตออกทางหน้าจอ
    /// 
    /// 
    /// เงื่อนไขที่ใช้ในการตรวจสอบ ก่อนการบันทึกข้อมูลการจ่ายป้าย
    /// 1.ป้ายบาร์โค้ตจะต้องไม่ซ้ำกัน (ไม่ถูกใช้ไปแล้วในการซื้อขายก่อนหน้านี้)
    /// 2.สถานะสัญญาชาวไร่จะต้อง Active (ActiveStaus)
    /// 3.สถานะการขายชาวไร่จะต้อง Active (SellingStatus)
    /// 4.ใบรายการซื้อขายจะต้องไม่ถูก Finish
    /// 5.จะต้องไม่จ่ายป้ายเกินจากโควต้าคงเหลือ (1 bale = 45 kgs.)
    /// 6.เป็น Extra Barcode หรือไม่?
    /// 
    /// </summary>
    public partial class RegisterBarcodeTag : Window
    {
        m_BuyingDocument _document;
        List<m_FarmerProject> _farmerQuota;
        RegistrationFarmer _registrationFarmer;

        public RegisterBarcodeTag(m_BuyingDocument buyingDocumentInfo, RegistrationFarmer registrationFarmer)
        {
            InitializeComponent();

            _document = new m_BuyingDocument();
            _farmerQuota = new List<m_FarmerProject>();
            _registrationFarmer = new RegistrationFarmer();

            _document = buyingDocumentInfo;
            _registrationFarmer = registrationFarmer;

            DocumentCodeTextBlock.Text = _document.DocumentCode;

            _farmerQuota = FarmerProjectHelper.GetQuotaAndSoldByFarmerVersion2(_registrationFarmer.Crop,
                _registrationFarmer.FarmerCode);

            FarmerBalanceQuotaDataGridBinding();
            BuyingDetailsDataGridBindig();

            BaleBarcodeTextBox.Focus();
        }

        private void BuyingDetailsDataGridBindig()
        {
            try
            {
                BuyingDetailsDataGrid.ItemsSource = null;
                BuyingDetailsDataGrid.ItemsSource = BuyingHelper.GetByBuyingDocument(
                    _document.Crop,
                    _document.FarmerCode,
                    _document.BuyingStationCode,
                    _document.BuyingDocumentNumber)
                    .OrderByDescending(x => x.RegisterBarcodeDate);

                totalItemLabel.Content = BuyingDetailsDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FarmerBalanceQuotaDataGridBinding()
        {
            try
            {
                FarmerBalanceQuotaDataGrid.ItemsSource = null;
                FarmerBalanceQuotaDataGrid.ItemsSource = _farmerQuota;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void ClearForm()
        {
            try
            {
                BaleBarcodeTextBox.Text = "";
                BaleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BaleBarcodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    if (BaleBarcodeTextBox.Text == "")
                    {
                        MessageBox.Show("โปรดสแกนหมายเลขบาร์โค้ตลงในช่อง Bale barcode", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                        BaleBarcodeTextBox.Focus();
                        return;
                    }

                    if (Helper.RegularExpressionHelper.IsBaleBarcodeCorrectFormat(BaleBarcodeTextBox.Text) == false)
                        throw new ArgumentException("รหัสบาร์โค้ตไม่ตรงตามรูปแบบที่กำหนด โปรดตรวจสอบใหม่อีกครั้ง");

                    if (IsExtraQuotaCheckBox.IsChecked == false)
                    {
                        BuyingFacade.BuyingBL()
                            .RegisterBarcodeInNormalQuota(BaleBarcodeTextBox.Text,
                            _document.Crop,
                            _document.BuyingStationCode,
                            _document.FarmerCode,
                            _document.BuyingDocumentNumber,
                            user_setting.User.Username,
                            Convert.ToBoolean(IsProjectCheckBox.IsChecked));
                    }
                    else
                    {
                        BuyingFacade.BuyingBL()
                            .RegisterBarcodeInExtraQuota(BaleBarcodeTextBox.Text,
                            _document.Crop,
                            _document.BuyingStationCode,
                            _document.FarmerCode,
                            _document.BuyingDocumentNumber,
                            user_setting.User.Username,
                            Convert.ToBoolean(IsProjectCheckBox.IsChecked));
                    }

                    BuyingDetailsDataGridBindig();
                    ClearForm();
                }
                if (e.Key == Key.Escape)
                {
                    ClearForm();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RemoveBaleBarcodeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BuyingDetailsDataGrid.SelectedIndex <= -1)
                    return;

                var buying = (Buying)BuyingDetailsDataGrid.SelectedItem;

                if (buying.BaleBarcode == "")
                {
                    MessageBox.Show("ไม่พบรหัสบาร์โค้ตของป้ายบาร์โค้ตนี้", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                BuyingFacade.BuyingBL().UnRisterBarcode(buying.BaleBarcode);

                BuyingDetailsDataGridBindig();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void IsExtraQuotaCheckBox_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IsExtraQuotaCheckBox.IsChecked == true)
                {
                    if (_farmerQuota.Sum(x => x.ExtraQuota) <= 0)
                    {
                        MessageBox.Show("ท่านไม่สามารถจ่ายป้ายใน Extra quota ได้ เนื่องจากชาวไร่รายนี้ไม่มี Extra quota",
                            "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                        IsExtraQuotaCheckBox.IsChecked = false;
                        return;
                    }
                    else
                        MessageBox.Show("โปรดตรวจสอบให้แน่ใจอีกครั้ง เมื่อท่านจ่ายป้ายใน Extra quota ราคาขายจะถูกหัก กก. ละ 2 บาท ทันที (ยกเว้นป้ายแดง/SC)",
                                "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void IsProjectCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (IsProjectCheckBox.IsChecked == true)
                MessageBox.Show("เมื่อท่านเลือกทำเครื่องหมายถูกหน้าหัวข้อนี้ จะถือว่ายาห่อนี้เป็นยาในโครงการทันที (Project)",
                    "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            else
                MessageBox.Show("เมื่อท่านเอาเครื่องหมายถูกหน้าหัวข้อนี้ออก จะเป็นการจ่ายป้ายแบบปกติ",
                    "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
        }
    }
}
