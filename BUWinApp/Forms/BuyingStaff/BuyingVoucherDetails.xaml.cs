﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using DomainModel;
using BusinessLayer;
using BusinessLayer.Model;
using BusinessLayer.Helper;
using BUWinApp.Helper;

namespace BUWinApp.Forms.BuyingStaff
{
    /// <summary>
    /// Interaction logic for BuyingVoucherDetails.xaml
    /// </summary>
    public partial class BuyingVoucherDetails : Window
    {
        decimal _totalPayment; // ยอดการชำระหนี้
        decimal _totalDebtor; // ยอดรวมรายการค่าเมล็ดพันธุ์ และ PPEKits
        decimal _waitForPayment; // ยอดที่ยังค้างชำระ
        decimal _totalPrice; // ยอดรวมการขายครั้งนี้

        RegistrationFarmer _regisInfo;
        m_BuyingDocument _documentInfo;

        public BuyingVoucherDetails(m_BuyingDocument model)
        {
            InitializeComponent();

            _regisInfo = new RegistrationFarmer();
            _documentInfo = new m_BuyingDocument();

            _documentInfo = model;
            RefreshForm();
        }

        public void DebtorTotalSummary()
        {
            try
            {
                // seed and farm material distributions
                var _seedForDeduction = BuyingFacade.SeedDistributionBL()
                    .GetByFarmer(_documentInfo.Crop, _documentInfo.FarmerCode)
                    .Where(s => s.IsDeduction == true).ToList();

                var _farmMaterialDeduction = BuyingFacade.CropInputDistributionBL().GetByFarmer(_documentInfo.Crop, _documentInfo.FarmerCode)
                    .Where(c => c.IsDeduction == true).ToList();

                // ดึงยอดรวมค่าปัจจัยการผลิต
                _totalDebtor = _seedForDeduction.Sum(x => x.Quantity * x.SeedForDistribution.UnitPrice) +
                    _farmMaterialDeduction.Sum(x => x.Quantity * x.CropInputItem.UnitPrice);

                //ดึงยอดรวมการชำระที่ผ่านมา ที่ comment ไว้คือเงื่อนไขเดิมปีที่แล้ว
                //_totalPayment = BuyingFacade.DebtorPaymentBL()
                //    .GetDebtorPaymentByFarmerAndCrop(_buyingDocInfo.Crop, _buyingDocInfo.FarmerCode)
                //    .Sum(x => x.PaymentAmount);

                _totalPayment = _seedForDeduction.Where(s => s.IsPayment == true)
                    .Sum(s => s.Quantity * s.SeedForDistribution.UnitPrice) +
                    _farmMaterialDeduction.Where(f => f.IsPayment == true)
                    .Sum(f => f.Quantity * f.CropInputItem.UnitPrice);

                _waitForPayment = _totalDebtor - _totalPayment; //ยอดค้างชำระ
                TotalDebtorTextBox.Text = _waitForPayment.ToString("N1");

                // ปุ่มแสดงข้อมูลการหักชำระ จะแสดงทุกเมื่อ เพื่อให้ผู้ใช้เข้าไปดูรายละเอียดได้
                DebtorPaymentButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RefreshForm()
        {
            try
            {
                _regisInfo = BuyingFacade.RegistrationBL()
                    .GetSingle(_documentInfo.Crop,
                    _documentInfo.FarmerCode);

                _documentInfo = BuyingDocumentHelper
                    .GetSingle(_documentInfo.Crop,
                    _documentInfo.BuyingStationCode,
                    _documentInfo.FarmerCode,
                    _documentInfo.BuyingDocumentNumber);

                BuyingDocumentDetailsDataBinding();
                BuyingDetailsItemDataGridBinding();
                DebtorTotalSummary();

                FarmerBalanceQuotaDataGridBinding(_regisInfo.Crop, _regisInfo.FarmerCode);
                SummaryOfHMLSoldDataGridBinding(_regisInfo.Crop, _regisInfo.FarmerCode);
                SummaryOfXCBTSoldDataGridBinding(_regisInfo.Crop, _regisInfo.FarmerCode);

                if (_documentInfo.IsFinish == true)
                {
                    FinishButton.IsEnabled = false;
                    UnFinishButton.IsEnabled = true;
                    DebtorPaymentButton.IsEnabled = true;
                    PrintVoucherPreviewButton.IsEnabled = true;
                    HessianDistributionButton.IsEnabled = true;
                }
                else
                {
                    FinishButton.IsEnabled = true;
                    UnFinishButton.IsEnabled = false;
                    DebtorPaymentButton.IsEnabled = false;
                    PrintVoucherPreviewButton.IsEnabled = false;
                    HessianDistributionButton.IsEnabled = false;
                }

                //จะหักเงินเฉพาะที่เป็นลูกค้า สกต.เท่านั้น
                //if (_regisInfo.Farmer.Supplier == "ABC/S" || _regisInfo.Farmer.Supplier == "ABC/P" || _regisInfo.Farmer.Supplier == "Uttaradit")
                //    DebtorPaymentButton.Visibility = System.Windows.Visibility.Visible;
                //else
                //    DebtorPaymentButton.Visibility = System.Windows.Visibility.Hidden;

                //if (_buyingDocInfo.IsFinish == true)
                //{
                //    if (_regisInfo.Farmer.Supplier == "ABC/S" || _regisInfo.Farmer.Supplier == "ABC/P" || _regisInfo.Farmer.Supplier == "Uttaradit")
                //    {
                //        DebtorPaymentButton.IsEnabled = true;

                //        if (BuyingFacade.DebtorPaymentBL().GetTotalDebtByCropAndFarmerCode(_regisInfo.Crop, _regisInfo.FarmerCode) > 0)
                //            PrintVoucherPreviewButton.IsEnabled = false;
                //        else
                //            PrintVoucherPreviewButton.IsEnabled = true;
                //    }
                //    else
                //        PrintVoucherPreviewButton.IsEnabled = true;
                //}
                //else
                //    PrintVoucherPreviewButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingDetailsItemDataGridBinding()
        {
            try
            {
                List<m_Buying> buyingDetailInfoList = new List<m_Buying>();
                buyingDetailInfoList = BuyingHelper.GetByBuyingDocument(_documentInfo.Crop,
                    _documentInfo.FarmerCode,
                    _documentInfo.BuyingStationCode,
                    _documentInfo.BuyingDocumentNumber).ToList();

                BuyingDetailsItemDataGrid.ItemsSource = null;
                BuyingDetailsItemDataGrid.ItemsSource = buyingDetailInfoList.OrderBy(x => x.Grade);

                _totalPrice = Convert.ToDecimal(buyingDetailInfoList.Sum(x => x.Price));
                TotalPriceTextBox.Text = Convert.ToDecimal(buyingDetailInfoList.Sum(x => x.Price)).ToString("N1");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BuyingDocumentDetailsDataBinding()
        {
            try
            {
                DocumentCodeTextBox.Text = _documentInfo.DocumentCode;
                FinishStatusCheckBox.IsChecked = _documentInfo.IsFinish;
                TotalBaleTextBox.Text = _documentInfo.TotalBale.ToString("N0");
                TotalBuyingTextBox.Text = _documentInfo.TotalGrade.ToString("N0");
                TotalRejectTextBox.Text = _documentInfo.TotalReject.ToString("N0");
                TotalWeightTextBox.Text = Convert.ToDecimal(_documentInfo.TotalSold).ToString("N1");

                FarmerCodeTextBox.Text = _regisInfo.FarmerCode;
                FarmerNameTextBox.Text = _regisInfo.Farmer.Person.Prefix + _regisInfo.Farmer.Person.FirstName + " " + _regisInfo.Farmer.Person.LastName;
                AddressTextBox.Text = _regisInfo.Farmer.Person.HouseNumber +
                    " หมู่ " + _regisInfo.Farmer.Person.Village +
                    " ต." + _regisInfo.Farmer.Person.Tumbon +
                    " อ." + _regisInfo.Farmer.Person.Amphur +
                    " จ." + _regisInfo.Farmer.Person.Province;

                EstimateBalanceKgTextBox.Text = Convert.ToInt16(_regisInfo.EstimateTobaccoAmount).ToString("N0");

                if (_documentInfo.IsFinish == true)
                {
                    FinishButton.IsEnabled = false;
                    UnFinishButton.IsEnabled = true;
                    DebtorPaymentButton.IsEnabled = true;
                }
                else
                {
                    PrintVoucherPreviewButton.IsEnabled = false;
                    FinishButton.IsEnabled = true;
                    UnFinishButton.IsEnabled = false;
                    DebtorPaymentButton.IsEnabled = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FarmerBalanceQuotaDataGridBinding(short crop, string farmerCode)
        {
            try
            {
                FarmerBalanceQuotaDataGrid.ItemsSource = null;
                FarmerBalanceQuotaDataGrid.ItemsSource = FarmerProjectHelper.GetQuotaAndSoldByFarmerVersion2(crop, farmerCode); ;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SummaryOfXCBTSoldDataGridBinding(short crop, string farmerCode)
        {
            try
            {
                SummaryOfXCBTSoldDataGrid.ItemsSource = null;
                SummaryOfXCBTSoldDataGrid.ItemsSource = BuyingFacade.BuyingBL().GetSummaryBuyingOfFarmerByPosition(crop, farmerCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SummaryOfHMLSoldDataGridBinding(short crop, string farmerCode)
        {
            try
            {
                SummaryOfHMLSoldDataGrid.ItemsSource = null;
                SummaryOfHMLSoldDataGrid.ItemsSource = BuyingFacade.BuyingBL().GetSummaryBuyingOfFarmerByQuality(crop, farmerCode);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                RefreshForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FinishButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_documentInfo.TotalBale - _documentInfo.TotalReject != _documentInfo.TotalWeight)
                {
                    MessageBox.Show("มีป้ายบาร์โค้ตอีกจำนวน " + (_documentInfo.TotalBale - _documentInfo.TotalReject - _documentInfo.TotalWeight) +
                        " ห่อ ที่ยังไม่ได้บันทึกข้อมูลการซื้อขาย" + Environment.NewLine +
                        "ระบบไม่สามารถ finish ได้ โปรดตรวจสอบให้ครบถ้วนอีกครั้ง",
                        "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                //if (_buyingDocumentInfo.TotalCaptureWeight != _buyingDocumentInfo.TotalLoadToTruck)
                //{
                //    MessageBox.Show("ชั่งน้ำหนักแล้วจำนวน " + _buyingDocumentInfo.TotalCaptureWeight + " ห่อ" + Environment.NewLine +
                //           "แต่ยาถูกบันทึกข้อมูลการขนขึ้นรถเพียง " + _buyingDocumentInfo.TotalLoadToTruck + Environment.NewLine +
                //           "ไม่สามารถ Finish ใบรายการซื้อขายนี้ได้ โปรดแจ้ง Supervisor เพื่อดำเนินการต่อไป", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}

                //if (_documentInfo.TotalWeight != _documentInfo.TotalLoadTruck)
                //{
                //    MessageBox.Show("ชั่งน้ำหนักแล้วจำนวน " + _documentInfo.TotalWeight + " ห่อ" + Environment.NewLine +
                //           "แต่ยาถูกบันทึกข้อมูลการขนขึ้นรถเพียง " + _documentInfo.TotalLoadTruck + Environment.NewLine +
                //           "โปรดตรวจสอบข้อมูลอีกครั้งเพื่อความถูกต้อง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                //    return;
                //}

                BuyingFacade.BuyingDocumentBL().Finish(_documentInfo.Crop,
                    _documentInfo.BuyingStationCode,
                    _documentInfo.FarmerCode,
                    _documentInfo.BuyingDocumentNumber,
                    user_setting.User.Username);

                MessageBox.Show("ใบรายการซื้อขายนี้ถูกเปลี่ยนสถานะเป็น Finish แล้ว ท่านสามารถพิมพ์ใบเวาเชอร์เพื่อมอบไว้ให้กับชาวไร่ได้", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information);

                RefreshForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UnFinishButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("ท่านต้องการ Unfinish ใบรายการซื้อขายนี้ใช่หรือไม่?", "การแจ้งเตือน", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                var model = BuyingFacade.DebtorPaymentBL().GetByBuyingDocument(_documentInfo);

                if (model.Count() > 0)
                {
                    MessageBox.Show("มีการชำระค่าเมล็ดพันธุ์และค่าปัจจัยการผลิตในใบรายการขายนี้แล้ว ไม่สามารถยกเลิกการปิดการขายนี้ได้", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }

                BuyingFacade.BuyingDocumentBL().Unfinish(_documentInfo.Crop,
                    _documentInfo.BuyingStationCode,
                    _documentInfo.FarmerCode,
                    _documentInfo.BuyingDocumentNumber,
                    user_setting.User.Username);

                MessageBox.Show("ใบรายการซื้อขายนี้ถูกเปลี่ยนสถานะเป็น UnFinish แล้ว", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information);

                RefreshForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DebtorPaymentButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BuyingVoucherDebtorDetailsV2 window = new BuyingVoucherDebtorDetailsV2(_documentInfo);
                window.ShowDialog();

                RefreshForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PrintVoucherPreviewButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                /*
                //เงื่อนไขเก่าของปี 2017 ใช้กับตารางเก่า โดยในปี 2018 มีการเพิ่มตางรางการจ่ายเมล็ดพันธ์และ PPE Kits ตามเงื่อนไขที่ถูกเพิ่มเข้ามาใหม่
                //*
                //decimal paymentAmount = BuyingFacade.DebtorPaymentBL().GetDebtorPaymentByFarmerAndCrop(_buyingDocInfo.Crop, _buyingDocInfo.FarmerCode).Sum(dp => dp.PaymentAmount);

                //decimal totalPriceOfSeedAndMediaReceive = BuyingFacade.ReceiveSeedAndMediaBL().GetReceiveSeedAndMediaInfoByFarmerAndCrop(_buyingDocInfo.Crop, _buyingDocInfo.FarmerCode).Sum(inf => inf.Price);
                //decimal totalPriceOfInputFactorReceive = BuyingFacade.ProductionFactorBL().GetReceiveInputFactorInfoByFarmer(_buyingDocInfo.Crop, _buyingDocInfo.FarmerCode).Sum(rs => rs.Price);

                //decimal totalDebt = totalPriceOfInputFactorReceive + totalPriceOfSeedAndMediaReceive;

                //if (paymentAmount > totalDebt)
                //{
                //    if (MessageBox.Show("ชาวไร่รายนี้ ยังค้างชำระค่าเมล็ดพันธุ์และปัจจัยการผลิต ท่านต้องการหักค่าเมล็ดพันธุ์และค่าปัจจัยการผลิตจากใบรายการซื้อขายนี้หรือไม่? ", "การแจ้งเตือน", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                //    {
                //        DebterPaymentDetails debtorPaymentDetails = new DebterPaymentDetails(_buyingDocInfo);
                //        debtorPaymentDetails.ShowDialog();
                //    }
                //}

                //BuyingVoucherPrintPreview printVoucherPreview = new BuyingVoucherPrintPreview(_buyingDocInfo);
                //printVoucherPreview.ShowDialog();
                //

                //if (_waitForPayment > 0)
                //{
                //    if (MessageBox.Show("ชาวไร่รายนี้ ยังค้างชำระค่าเมล็ดพันธุ์และปัจจัยการผลิต ท่านต้องการหักค่าเมล็ดพันธุ์และค่าปัจจัยการผลิตจากใบรายการซื้อขายนี้หรือไม่? ", "การแจ้งเตือน", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                //    {
                //        DebterPaymentDetailForCY2018 window = new DebterPaymentDetailForCY2018(_buyingDocInfo);
                //        window.ShowDialog();
                //    }
                //}
                */

                // ถ้ามียอดค้างชำระ จะต้องบังคับให้ชำระ (ยกเว้นกรณีเงินในการขายครั้งนั้น ไม่พอหักชำระ)
                if (_waitForPayment > 0)
                {
                    if (_totalPrice >= _waitForPayment)
                    {
                        MessageBox.Show("ท่านมียอดค้างชำระค่าเมล็ดพันธุ์และปัจจัยการผลิต ท่านจะต้องทำการหักชำระหนี้ก่อน จึงจะสามารถพิมพ์ใบเวาเชอร์ได้",
                            "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information);
                        return;
                    }
                    else
                    {
                        MessageBox.Show("ท่านมียอดค้างชำระค่าเมล็ดพันธุ์และปัจจัยการผลิต แต่ยอดเงินจากการขายครั้งนี้ไม่พอหักชำระ โปรดชำระในการนำยามาขายครั้งถัดไป",
                            "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }


                BuyingVoucherPrintPreviewV2 window = new BuyingVoucherPrintPreviewV2(_documentInfo);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EstimateBalanceKgSubmitButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BuyingFacade.RegistrationBL()
                    .UpdateEstimateBalanceKgAfterSold(_regisInfo.Crop,
                    _regisInfo.FarmerCode,
                    Convert.ToInt16(EstimateBalanceKgTextBox.Text),
                    user_setting.User.Username);

                MessageBox.Show("บันทึกข้อมูลจำนวนยาคงเหลือที่แจ้งไว้เรียบร้อย", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void HessianDistributionButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Hessian.HessianDistributionForm window = new Hessian.HessianDistributionForm(_documentInfo);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
