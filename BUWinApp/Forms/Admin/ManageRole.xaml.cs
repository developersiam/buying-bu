﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BUWinApp.Helper;

namespace BUWinApp.Forms.Admin
{
    /// <summary>
    /// Interaction logic for ManageRole.xaml
    /// </summary>
    public partial class ManageRole : Window
    {
        public ManageRole()
        {
            try
            {
                InitializeComponent();
                RoleDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void RoleDataGridBinding()
        {
            try
            {
                RoleDataGrid.ItemsSource = null;
                RoleDataGrid.ItemsSource = BuyingFacade.RoleBL()
                    .GetAll()
                    .OrderBy(x => x.RoleName);

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void ClearForm()
        {
            try
            {
                RoleNameTextBox.Text = "";

                AddButton.IsEnabled = true;
                EditButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RoleDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (RoleDataGrid.SelectedIndex <= -1)
                    return;

                var role = (Role)RoleDataGrid.SelectedItem;

                RoleNameTextBox.Text = role.RoleName;
                AddButton.IsEnabled = false;
                EditButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RoleDataGrid.SelectedIndex <= -1)
                    return;

                BuyingFacade.RoleBL().Delete(((Role)RoleDataGrid.SelectedItem).RoleID);
                RoleDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Role role = new Role
                {
                    RoleID = Guid.NewGuid(),
                    RoleName = RoleNameTextBox.Text,
                    ModifiedByUser = user_setting.User.Username,
                    LastModifiedDate = DateTime.Now,
                    CreateDate = DateTime.Now
                };

                BuyingFacade.RoleBL().Add(role);
                RoleDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RoleDataGrid.SelectedIndex <= -1)
                    return;

                var role = (Role)RoleDataGrid.SelectedItem;
                role.RoleName = RoleNameTextBox.Text;

                BuyingFacade.RoleBL().Update(role);
                RoleDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
