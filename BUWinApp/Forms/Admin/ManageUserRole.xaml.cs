﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BUWinApp.Helper;

namespace BUWinApp.Forms.Admin
{
    /// <summary>
    /// Interaction logic for ManageUserRole.xaml
    /// </summary>
    public partial class ManageUserRole : Window
    {
        UserAccount _userParameter;
        public ManageUserRole()
        {
            InitializeComponent();
        }

        struct UserRoleInfo
        {
            public Guid RoleId { get; set; }
            public string RoleName { get; set; }
            public bool IsRole { get; set; }
        }

        public ManageUserRole(UserAccount user)
        {
            try
            {
                InitializeComponent();
                _userParameter = new UserAccount();
                _userParameter = user;
                UserRoleDataPopulation(_userParameter.Username);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void UserRoleDataPopulation(string username)
        {
            try
            {
                var query = from r in BuyingFacade.RoleBL().GetAll()
                            join ur in BuyingFacade.UserRoleBL().GetByUser(_userParameter.Username)
                            on r.RoleID equals ur.RoleID into urp
                            from subur in urp.DefaultIfEmpty()
                            select new
                            {
                                r.RoleID,
                                r.RoleName,
                                IsRole = subur == null ? false : true
                            };

                List<UserRoleInfo> userRoleInfoList = new List<UserRoleInfo>();

                foreach (var item in query)
                {
                    UserRoleInfo userRoleInfo = new UserRoleInfo();

                    userRoleInfo.RoleId = item.RoleID;
                    userRoleInfo.RoleName = item.RoleName;
                    userRoleInfo.IsRole = item.IsRole;

                    userRoleInfoList.Add(userRoleInfo);
                }

                UserRoleDataGrid.ItemsSource = null;
                UserRoleDataGrid.ItemsSource = userRoleInfoList;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UserRoleDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (UserRoleDataGrid.SelectedIndex <= -1)
                    return;

                var userRole = (UserRoleInfo)UserRoleDataGrid.SelectedItem;

                if (userRole.IsRole == false)
                    BuyingFacade.UserRoleBL()
                        .Add(_userParameter.Username, userRole.RoleId, user_setting.User.Username);
                else
                    BuyingFacade.UserRoleBL()
                        .Delete(_userParameter.Username,userRole.RoleId);

                UserRoleDataPopulation(_userParameter.Username);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ManageRoleButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageRole manageRoleWindow = new ManageRole();
                manageRoleWindow.ShowDialog();

                UserRoleDataPopulation(_userParameter.Username);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
