﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BUWinApp.Helper;

namespace BUWinApp.Forms.Admin
{
    /// <summary>
    /// Interaction logic for ManageUser.xaml
    /// </summary>
    public partial class ManageUser : Page
    {
        public ManageUser()
        {
            InitializeComponent();
            UserDataBinding();
        }

        public void UserDataBinding()
        {
            try
            {
                UserAccountDataGrid.ItemsSource = null;
                UserAccountDataGrid.ItemsSource = BuyingFacade.UserBL().GetAll().OrderBy(x => x.Username);

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        public void ClearForm()
        {
            try
            {
                InputFormGrid.Visibility = Visibility;

                UsernameTextBox.Text = "";
                PasswordPasswordBox.Password = "";
                FirstNameTextBox.Text = "";
                LastNameTextBox.Text = "";

                AddButton.IsEnabled = true;
                EditButton.IsEnabled = false;
                DeleteButton.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UserAccount user = new UserAccount
                {
                    Username = UsernameTextBox.Text,
                    Password = PasswordPasswordBox.Password,
                    FirstName = FirstNameTextBox.Text,
                    LastName = LastNameTextBox.Text,
                    ModifiedByUser = user_setting.User.Username,
                };

                BuyingFacade.UserBL().Add(user);

                UserDataBinding();

                MessageBox.Show("บันทึกสำเร็จ", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                UserAccount user = new UserAccount();
                user = (UserAccount)UserAccountDataGrid.SelectedItem;

                user.Password = PasswordPasswordBox.Password;
                user.FirstName = FirstNameTextBox.Text;
                user.LastName = LastNameTextBox.Text;
                user.ActiveStatus = Convert.ToBoolean(ActiveStatusCheckBox.IsChecked);
                user.ModifiedByUser = user_setting.User.Username;

                BuyingFacade.UserBL().Update(user);

                UserDataBinding();

                MessageBox.Show("บันทึกสำเร็จ", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UserAccountDataGrid.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดเลือกข้อมูลที่ต้องการลบจากตารางด้านล่าง", "ข้อความแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (((UserAccount)UserAccountDataGrid.SelectedItem).UserRoles.Count > 0)
                {
                    MessageBox.Show("โปรดลบสิทธิ์ของผู้ใช้ก่อนทำการลบข้อมูลบัญชีผู้ใช้นี้", "ข้อความแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "ยืนยัน", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BuyingFacade.UserBL().Delete(((UserAccount)UserAccountDataGrid.SelectedItem).Username);

                UserDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RoleItemButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (UserAccountDataGrid.SelectedIndex <= -1)
                    return;

                ManageUserRole manageUseRole = new ManageUserRole((UserAccount)UserAccountDataGrid.SelectedItem);

                manageUseRole.ShowDialog();

                UserDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UserAccountButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (InputFormGrid.Visibility == System.Windows.Visibility.Collapsed)
                {
                    InputFormGrid.Visibility = System.Windows.Visibility.Visible;
                    (sender as Button).Content = "^";
                }
                else
                {
                    InputFormGrid.Visibility = System.Windows.Visibility.Collapsed;
                    (sender as Button).Content = "v";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UserAccountDataGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (UserAccountDataGrid.SelectedIndex <= -1)
                    return;

                InputFormGrid.Visibility = Visibility;

                UserAccount user = new UserAccount();
                user = (UserAccount)UserAccountDataGrid.SelectedItem;

                UsernameTextBox.Text = user.Username;
                PasswordPasswordBox.Password = user.Password;
                FirstNameTextBox.Text = user.FirstName;
                LastNameTextBox.Text = user.LastName;
                ActiveStatusCheckBox.IsChecked = user.ActiveStatus;

                AddButton.IsEnabled = false;
                EditButton.IsEnabled = true;
                DeleteButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
