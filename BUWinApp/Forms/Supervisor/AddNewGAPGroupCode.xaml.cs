﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BUWinApp.Helper;

namespace BUWinApp.Forms.Supervisor
{
    /// <summary>
    /// Interaction logic for AddNewGAPGroupCode.xaml
    /// </summary>
    public partial class AddNewGAPGroupCode : Window
    {
        GAPGroupLeaderByCrop _leaderByCrop;

        public AddNewGAPGroupCode(GAPGroupLeaderByCrop leader)
        {
            InitializeComponent();

            _leaderByCrop = new GAPGroupLeaderByCrop();
            _leaderByCrop = leader;
            
            LeaderNameTextBlock.Text = "กลุ่ม : " + leader.GAPGroupLeader.Person.Prefix + 
                leader.GAPGroupLeader.Person.FirstName + "  " + 
                leader.GAPGroupLeader.Person.LastName;

            SupplierCodeTextBlock.Text = leader.SupplierCode;

            GAPGroupDataGridBinding();
        }

        public void GAPGroupDataGridBinding()
        {
            try
            {
                GAPGroupCodeDataGrid.ItemsSource = null;
                GAPGroupCodeDataGrid.ItemsSource = BuyingFacade.GapGroupBL()
                    .GetGAPGroupByLeader(_leaderByCrop.Crop, 
                    _leaderByCrop.CitizenID, 
                    _leaderByCrop.SupplierCode);

                TotalItems.Text = GAPGroupCodeDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddNewGAPGroupCodeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (GAPGroupCodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดกรอก GAP Group Code", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                BuyingFacade.GapGroupBL().AddGAPGroup(_leaderByCrop.Crop, 
                    _leaderByCrop.CitizenID,
                    _leaderByCrop.SupplierCode,
                    user_setting.User.Username,
                    GAPGroupCodeTextBox.Text);
                
                GAPGroupDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteItemButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (GAPGroupCodeDataGrid.SelectedIndex <= -1)
                    return;

                GAPGroup gapGroup = new GAPGroup();
                gapGroup = (GAPGroup)GAPGroupCodeDataGrid.SelectedItem;

                BuyingFacade.GapGroupBL().DeleteGAPGroup(gapGroup.GAPGroupCode);

                GAPGroupDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
