﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BusinessLayer.Helper;
using BusinessLayer.Model;
using BUWinApp.Helper;

namespace BUWinApp.Forms.Supervisor
{
    /// <summary>
    /// Interaction logic for FarmerProject.xaml
    /// </summary>
    public partial class FarmerProjects : Window
    {
        RegistrationFarmer _regis;

        public FarmerProjects()
        {
            InitializeComponent();

            _regis = new RegistrationFarmer();
            FarmerCodeTextBox.Focus();
        }

        private void FarmerProfileBinding()
        {
            try
            {
                _regis = BuyingFacade.RegistrationBL()
                .GetSingle(user_setting.Crop.Crop1,
                FarmerCodeTextBox.Text);

                if (_regis == null)
                    throw new ArgumentException("ไม่พบข้อมูล! โปรดลองใส่รหัสชาวไร่ (Farmer code) และกด enter เพื่อค้นหาชาวไร่ที่ต้องการแก้ไขข้อมูลอีกครั้ง");

                ProjectTypeComboBox.ItemsSource = null;
                ProjectTypeComboBox.ItemsSource = BuyingFacade.FarmerProjectBL().GetAll();

                FarmerCodeTextBox.Text = _regis.FarmerCode;
                FarmerNameTextBox.Text = _regis.Farmer.Person.Prefix + " " + _regis.Farmer.Person.FirstName + " " + _regis.Farmer.Person.LastName;
                AddressTextBox.Text = "ต." + _regis.Farmer.Person.Tumbon + " อ." + _regis.Farmer.Person.Amphur + " จ." + _regis.Farmer.Person.Province;
                ContractCodeTextBox.Text = _regis.GAPContractCode;
                GAPGroupCodeTextBox.Text = _regis.GAPGroupCode;
                QuotaFromSignContractTextBox.Text = _regis.QuotaFromSignContract.Value.ToString("N0");
                FarmerProjectDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FarmerProjectDataGridBinding()
        {
            try
            {
                if (_regis == null)
                    return;

                FarmerProjectDataGrid.ItemsSource = null;
                FarmerProjectDataGrid.ItemsSource = FarmerProjectHelper
                    .GetQuotaAndSoldByFarmerVersion2(_regis.Crop,
                    _regis.FarmerCode).ToList();

                TotalItems.Text = FarmerProjectDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearForm()
        {
            try
            {
                ActualQuotaRaiTextBox.Clear();
                ExtarQuotaTextBox.Clear();
                AddNewFarmerProjectButton.IsEnabled = true;
                EditFarmerProjectButton.IsEnabled = false;
                ProjectTypeComboBox.IsEnabled = true;
                FarmerCodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddNewFarmerProjectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ProjectTypeComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดเลือก Project type", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                if (ActualQuotaRaiTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุ จำนวนไร่ที่ต้องการแบ่งโควต้า", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ActualQuotaRaiTextBox.Focus();
                    return;
                }
                //// NotSignContact
                if (BuyingFacade.RegistrationBL().NotSignContact(_regis.Crop, _regis.FarmerCode) == false)
                {
                    MessageBox.Show("ยังไม่ได้เซนสัญญา กรุณาตรวจสอบการเซ็นสัญญาของชาวไร่", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                /// NotDistributionSeed
                if (BuyingFacade.SeedDistributionBL().NotDistributionSeed(_regis.Crop, _regis.FarmerCode) == false)
                {
                    MessageBox.Show("ยังไม่ได้จ่ายเมล็ดพันธุ์ให้กับชาวไร่ รายนี้ กรุณาตรวจสอบการข้อมูลการจ่ายเมล็ดพันธุ์", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                // NotDistributionChemical
                if (BuyingFacade.ChemicalDistributionBL().NotChemicaldistribution(_regis.Crop, _regis.FarmerCode) == false)
                {
                    MessageBox.Show("ยังไม่มีข้อมูลการจ่ายปัจจัยการผลิตของชาวไร่ รายนี้ กรุณาตรวจสอบการข้อมูลการจ่ายปัจจัยการผลิต", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                FarmerProject farmerProject = new FarmerProject();

                farmerProject.Crop = _regis.Crop;
                farmerProject.FarmerCode = _regis.FarmerCode;
                farmerProject.ProjectType = ProjectTypeComboBox.SelectedValue.ToString();
                farmerProject.ActualRai = Convert.ToByte(ActualQuotaRaiTextBox.Text);
                farmerProject.ExtraQuota = ExtarQuotaTextBox.Text == "" ? Convert.ToInt16(0) : Convert.ToInt16(ExtarQuotaTextBox.Text);
                farmerProject.LastModified = DateTime.Now;
                farmerProject.ModifiedByUser = user_setting.User.Username;

                BuyingFacade.FarmerProjectBL().Add(farmerProject);

                FarmerProjectDataGridBinding();

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditFarmerProjectButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FarmerProjectDataGrid.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดเลือก Project type ที่ต้องการได้จากตารางด้านล่าง", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (ProjectTypeComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดเลือก Project type", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (ActualQuotaRaiTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุ จำนวนไร่ที่ต้องการแบ่งโควต้า", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    ActualQuotaRaiTextBox.Focus();
                    return;
                }

                var farmerProjectAndSold = (m_FarmerProject)FarmerProjectDataGrid.SelectedItem;

                //เป็นการแก้ไขโควต้าไร่ หรือโควต้า Extra
                if (farmerProjectAndSold.ActualRai != Convert.ToInt16(ActualQuotaRaiTextBox.Text))
                {
                    //เป็นการแก้ไขจำนวนไร่ในโปรเจค Actual rai
                    BuyingFacade.FarmerProjectBL()
                        .UpdateActualQuota(_regis.Crop,
                        _regis.FarmerCode,
                        farmerProjectAndSold.ProjectType,
                        Convert.ToInt16(ActualQuotaRaiTextBox.Text),
                        user_setting.User.Username);
                }
                else if (farmerProjectAndSold.ActualRai == Convert.ToInt16(ActualQuotaRaiTextBox.Text) &&
                    farmerProjectAndSold.ExtraQuota != Convert.ToDecimal(ExtarQuotaTextBox.Text))
                {
                    //เป็นการแก้ไขจำนวน Extra quota
                    BuyingFacade.FarmerProjectBL()
                        .UpdateExtraQuota(_regis.Crop,
                        _regis.FarmerCode, farmerProjectAndSold.ProjectType, Convert.ToInt16(ExtarQuotaTextBox.Text), user_setting.User.Username);
                }
                else if (farmerProjectAndSold.ActualRai != Convert.ToInt16(ActualQuotaRaiTextBox.Text) && farmerProjectAndSold.ExtraQuota != Convert.ToDecimal(ExtarQuotaTextBox.Text))
                {
                    //เป็นการแก้ไขทั้ง Actual rai และ Extra quota.
                    BuyingFacade.FarmerProjectBL().UpdateActualQuota(_regis.Crop,
                        _regis.FarmerCode,
                        farmerProjectAndSold.ProjectType,
                        Convert.ToInt16(ActualQuotaRaiTextBox.Text),
                        user_setting.User.Username);

                    BuyingFacade.FarmerProjectBL().UpdateExtraQuota(_regis.Crop,
                        _regis.FarmerCode,
                        farmerProjectAndSold.ProjectType,
                        Convert.ToInt16(ExtarQuotaTextBox.Text),
                        user_setting.User.Username);
                }

                FarmerProjectDataGridBinding();
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearFormButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditItemButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FarmerProjectDataGrid.SelectedIndex <= -1)
                    return;

                m_FarmerProject farmerProjectAndSold = new m_FarmerProject();
                farmerProjectAndSold = (m_FarmerProject)FarmerProjectDataGrid.SelectedItem;

                ProjectTypeComboBox.SelectedValue = farmerProjectAndSold.ProjectType;
                ActualQuotaRaiTextBox.Text = farmerProjectAndSold.ActualRai.ToString();
                ExtarQuotaTextBox.Text = farmerProjectAndSold.ExtraQuota.ToString("N0");

                EditFarmerProjectButton.IsEnabled = true;
                AddNewFarmerProjectButton.IsEnabled = false;
                ProjectTypeComboBox.IsEnabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteItemButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var model = (m_FarmerProject)FarmerProjectDataGrid.SelectedItem;

                if (user_setting.Crop.Crop1 != model.Crop)
                {
                    MessageBox.Show("ข้อมูลที่ท่านต้องการลบคือข้อมูลปี(" + model.Crop +
                        ") ไม่อยู่ในปีปัจจุบันของระบบ(" + user_setting.Crop.Crop1 +
                        ") ระบบไม่สามารถลบข้อมูลนี้ได้", "", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (MessageBox.Show("ท่านต้องการลบข้อมูลโปรเจค " + model.ProjectType +
                    " ของชาวไร่รหัส " + model.FarmerCode + " นี้ใช่หรือไม่?",
                    "การแจ้งเตือน", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BuyingFacade.FarmerProjectBL().Delete(model.Crop,
                    model.FarmerCode,
                    model.ProjectType);

                FarmerProjectDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void FarmerCodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter)
                return;

            FarmerProfileBinding();
            FarmerProjectDataGridBinding();
        }
    }
}
