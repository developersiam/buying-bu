﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BusinessLayer.Model;
using BusinessLayer.Helper;

namespace BUBuyingSystemWinApp.Forms.Supervisor
{
    /// <summary>
    /// Interaction logic for UnlockWeightForChangeGrade.xaml
    /// </summary>
    public partial class UnlockWeightForChangeGrade : Page
    {
        public UnlockWeightForChangeGrade()
        {
            InitializeComponent();
            BaleBarcodeTextBox.Focus();
        }

        void BaleDetailsDataBinding()
        {
            try
            {
                m_Buying buyingDetails = new m_Buying();
                buyingDetails = BuyingHelper.GetByBaleBarcode(BaleBarcodeTextBox.Text);

                //Get a farmer from selected supplier *
                Farmer farmer = new Farmer();
                farmer = BuyingFacade.FarmerBL().GetFarmerBySupplierCodeAndCitizenID(buyingDetails.SupplierCode,
                    buyingDetails.BuyingDocument.RegistrationFarmer.Farmer.CitizenID);

                if (farmer == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลชาวไร่เจ้าของยาห่อนี้ในระบบ โปรดแจ้งแผนกไอที เพื่อทำการตรวจสอบข้อมูล",
                        "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                SupplierTextBox.Text = buyingDetails.SupplierCode;
                BuyingStationTextBox.Text = buyingDetails.BuyingStationCode;
                BuyingDateTextBox.Text = buyingDetails.RegisterBarcodeDate.ToShortDateString();
                BuyingWeightTextBox.Text = Convert.ToDecimal(buyingDetails.Weight).ToString("N1"); ;
                BuyingGradeTextBox.Text = buyingDetails.Grade;

                TransportationCodeTextBox.Text = buyingDetails.TransportationDocumentCode;
                TruckNoTextBox.Text = buyingDetails.TransportationDocumentCode == null ? "" : buyingDetails.TransportationDocument.TruckNumber;
                FarmerCodeTextBox.Text = farmer.FarmerCode;
                FarmerNameTextBox.Text = farmer.Person.Prefix
                    + farmer.Person.FirstName
                    + " " + farmer.Person.LastName;
                AddressTextBox.Text = farmer.Person.HouseNumber
                    + " หมู่ " + farmer.Person.Village
                    + " ต." + farmer.Person.Tumbon
                    + " อ." + farmer.Person.Amphur
                    + " จ." + farmer.Person.Province;
                CitizenIDTextBox.Text = farmer.CitizenID;
                BuyingDocumentCodeTextBox.Text = buyingDetails.Crop + "-"
                    + buyingDetails.BuyingStationCode + "-"
                    + buyingDetails.FarmerCode + "-"
                    + buyingDetails.BuyingDocumentNumber;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UnlockWeightButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BaleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุหมายเลขบาร์โค้ต", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (BaleBarcodeTextBox.Text.Length != 17)
                {
                    MessageBox.Show("บาร์โค้ตไม่ครบ 17 หลัก", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                BuyingFacade.BuyingBL().UnlockBuyingInfo(BaleBarcodeTextBox.Text);
                BaleDetailsDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (BaleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุหมายเลขบาร์โค้ต", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (BaleBarcodeTextBox.Text.Length != 17)
                {
                    MessageBox.Show("บาร์โค้ตไม่ครบ 17 หลัก", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                BaleDetailsDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BaleBarcodeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                if (BaleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุหมายเลขบาร์โค้ต", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (BaleBarcodeTextBox.Text.Length != 17)
                {
                    MessageBox.Show("บาร์โค้ตไม่ครบ 17 หลัก", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                BaleDetailsDataBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BaleBarcodeTextBox.Clear();
                BaleBarcodeTextBox.Focus();

                SupplierTextBox.Clear();
                BuyingStationTextBox.Clear();
                BuyingDateTextBox.Clear();
                BuyingWeightTextBox.Clear();
                BuyingGradeTextBox.Clear();

                TransportationCodeTextBox.Clear();
                TruckNoTextBox.Clear();
                FarmerCodeTextBox.Clear();
                FarmerNameTextBox.Clear();
                AddressTextBox.Clear();
                CitizenIDTextBox.Clear();
                BuyingDocumentCodeTextBox.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
