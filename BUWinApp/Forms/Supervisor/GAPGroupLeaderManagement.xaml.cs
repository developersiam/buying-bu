﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using System.Windows.Controls.Primitives;
using BUWinApp.Helper;

namespace BUWinApp.Forms.Supervisor
{
    /// <summary>
    /// Interaction logic for GAPGroupLeaderManagement.xaml
    /// </summary>
    public partial class GAPGroupLeaderManagement : Page
    {
        public GAPGroupLeaderManagement()
        {
            InitializeComponent();

            CropComboBox.ItemsSource = null;
            CropComboBox.ItemsSource = BuyingFacade.CropBL().GetAll().OrderByDescending(x => x.Crop1);

            SupplierComboBox.ItemsSource = null;
            SupplierComboBox.ItemsSource = BuyingFacade.SupplierBL()
                .GetByArea(user_setting.User.StaffUser.AreaCode)
                .OrderBy(x => x.SupplierCode);
        }

        public void GapGroupLeaderDataGridBinding()
        {
            try
            {
                if (CropComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดเลือก Crop", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (SupplierComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดเลือก Supplier", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                GAPGroupLeaderDataGrid.ItemsSource = null;
                GAPGroupLeaderDataGrid.ItemsSource = BuyingFacade.GapGroupBL()
                    .GetGAPGroupLeaderByCropBySupplier(Convert.ToInt16(CropComboBox.SelectedValue.ToString()),
                    SupplierComboBox.SelectedValue.ToString())
                    .OrderBy(x => x.GAPGroupLeader.Person.FirstName);

                TotalItems.Text = GAPGroupLeaderDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteItemButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (GAPGroupLeaderDataGrid.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดเลือก Leader ที่ต้องการลบข้อมูลโดยคลิกจากตารางด้านล่าง", "การแจ้งเตือน",
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                GAPGroupLeaderByCrop leader = new GAPGroupLeaderByCrop();
                leader = (GAPGroupLeaderByCrop)GAPGroupLeaderDataGrid.SelectedItem;

                if (user_setting.Crop.Crop1 != leader.Crop)
                {
                    MessageBox.Show("ไม่อนุญาติให้จัดการข้อมูลนอกจากปีที่ทางระบบได้กำหนดไว้ ปีปัจจุบันในระบบคือ " +
                        user_setting.Crop.Crop1, "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (MessageBox.Show("ท่านต้องการลบข้อมูล leader citizenID : " + leader.CitizenID +
                    " นี้ออกจาก Supplier " + leader.SupplierCode + " ปี " + leader.Crop + " ใช่หรือไม่",
                    "การแจ้งเตือน", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;
                
                BuyingFacade.GapGroupBL()
                    .DeleteGAPGroupLeaderByCrop(leader.Crop, 
                    leader.CitizenID, 
                    leader.SupplierCode);

                GapGroupLeaderDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddNewFarmerLeaderButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CropComboBox.SelectedIndex <= -1 || SupplierComboBox.SelectedIndex <= -1)
                {
                    MessageBox.Show("โปรดเลือก Crop หรือ Supplier ก่อน", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (user_setting.Crop.Crop1 != Convert.ToInt16(CropComboBox.SelectedValue.ToString()))
                {
                    MessageBox.Show("ไม่อนุญาติให้จัดการข้อมูลนอกจากปีที่ทางระบบได้กำหนดไว้ ปีปัจจุบันในระบบคือ " +
                        user_setting.Crop.Crop1, "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                GAPGroupLeaderList leaderList = new GAPGroupLeaderList(Convert.ToInt16(CropComboBox.SelectedValue.ToString()),
                    SupplierComboBox.SelectedValue.ToString());
                leaderList.ShowDialog();

                GapGroupLeaderDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CropComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (CropComboBox.SelectedIndex <= -1)
                    return;

                if (SupplierComboBox.SelectedIndex <= -1)
                    return;

                GapGroupLeaderDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SupplierComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                try
                {
                    if (CropComboBox.SelectedIndex <= -1)
                        return;

                    if (SupplierComboBox.SelectedIndex <= -1)
                        return;

                    SupplierCodeTextBlock.Text = SupplierComboBox.SelectedValue.ToString();

                    GapGroupLeaderDataGridBinding();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddGAPGroupButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGridRow row = (DataGridRow)(GAPGroupLeaderDataGrid
                    .ItemContainerGenerator
                    .ContainerFromItem(GAPGroupLeaderDataGrid.SelectedItem));

                //// Getting the ContentPresenter of the row details
                //DataGridDetailsPresenter presenter = FindVisualChild<DataGridDetailsPresenter>(row);

                //// Finding Remove button from the DataTemplate that is set on that ContentPresenter
                //DataTemplate template = presenter.ContentTemplate;
                //Button button = (Button)template.FindName("RemoveItemButton", presenter);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteGAPGroupButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = (Button)sender;

                GAPGroup gapGroup = new GAPGroup();
                gapGroup = BuyingFacade.GapGroupBL().GetGAPGroupByGapGroupCode(btn.ToolTip.ToString());

                if (user_setting.Crop.Crop1 != gapGroup.Crop)
                {
                    MessageBox.Show("ไม่อนุญาตให้จัดการข้อมูลที่ไม่ตรงกับปีปัจจุบันที่ระบบกำหนดไว้ ปีปัจจุบันในระบบคือ " +
                        user_setting.Crop.Crop1.ToString(), "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (gapGroup == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลในระบบ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (gapGroup.RegistrationFarmers.Count > 0)
                {
                    MessageBox.Show("GAP Code นี้มีสมาชิกอยู่เป็นจำนวน " + gapGroup.RegistrationFarmers.Count.ToString("N0") +
                        " ไม่สามารถลบได้", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (MessageBox.Show("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?", "การแจ้งเตือน",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;
                
                BuyingFacade.GapGroupBL().DeleteGAPGroup(gapGroup.GAPGroupCode);

                GapGroupLeaderDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddGAPGroupCodeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (GAPGroupLeaderDataGrid.SelectedIndex <= -1)
                    return;

                GAPGroupLeaderByCrop leader = new GAPGroupLeaderByCrop();
                leader = (GAPGroupLeaderByCrop)GAPGroupLeaderDataGrid.SelectedItem;

                if (user_setting.Crop.Crop1 != leader.Crop)
                {
                    MessageBox.Show("ไม่อนุญาตให้จัดการข้อมูลที่ไม่ตรงกับปีปัจจุบันที่ระบบกำหนดไว้ ปีปัจจุบันในระบบคือ " +
                        user_setting.Crop.Crop1.ToString(), "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                AddNewGAPGroupCode AddGapGroup = new AddNewGAPGroupCode(leader);
                AddGapGroup.ShowDialog();

                GapGroupLeaderDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ManageGAPGroupMemberButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button btn = (Button)sender;

                GAPGroup gapGroup = new GAPGroup();
                gapGroup = BuyingFacade.GapGroupBL().GetGAPGroupByGapGroupCode(btn.ToolTip.ToString());

                if (user_setting.Crop.Crop1 != gapGroup.Crop)
                {
                    MessageBox.Show("ไม่อนุญาตให้จัดการข้อมูลที่ไม่ตรงกับปีปัจจุบันที่ระบบกำหนดไว้ ปีปัจจุบันในระบบคือ " +
                        user_setting.Crop.Crop1.ToString(), "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (gapGroup == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลในระบบ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                GAPGroupMember addMemberToGroup = new GAPGroupMember(gapGroup);
                addMemberToGroup.ShowDialog();

                GapGroupLeaderDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
