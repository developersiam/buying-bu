﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BUWinApp.Helper;

namespace BUWinApp.Forms.Supervisor
{
    /// <summary>
    /// Interaction logic for ListOfFarmerNoGAPGroup.xaml
    /// </summary>
    public partial class ListOfFarmerNoGAPGroup : Window
    {
        GAPGroup _gapGroup;

        public ListOfFarmerNoGAPGroup(GAPGroup gapGroup)
        {
            InitializeComponent();

            _gapGroup = new GAPGroup();
            _gapGroup = gapGroup;

            CropTextBlock.Text = _gapGroup.Crop.ToString();
            SupplierCodeTextBlock.Text = _gapGroup.SupplierCode;


            //RegistrationFarmerNoGroupDataGridBinding();
        }

        public void RegistrationFarmerNoGroupDataGridBinding()
        {
            try
            {
                List<RegistrationFarmer> registerFarmerList = new List<RegistrationFarmer>();

                registerFarmerList = (from rf in BuyingFacade.RegistrationBL()
                                      .GetBySupplier(_gapGroup.Crop,
                                      _gapGroup.SupplierCode)
                                      .Where(gg => gg.GAPGroupCode == null)
                                      where !(from gg in BuyingFacade.RegistrationBL()
                                              .GetByGAPGroup(_gapGroup.GAPGroupCode)
                                              select gg.FarmerCode)
                                              .Contains(rf.FarmerCode)
                                      select rf)
                                      .ToList();

                RegistrationFarmerNoGAPGroupDataGrid.ItemsSource = null;
                RegistrationFarmerNoGAPGroupDataGrid.ItemsSource = registerFarmerList.OrderBy(x => x.Farmer.Person.FirstName);

                TotalItems.Text = RegistrationFarmerNoGAPGroupDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddMemberToGroupButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (RegistrationFarmerNoGAPGroupDataGrid.SelectedIndex <= -1)
                    return;

                var _registration = (RegistrationFarmer)RegistrationFarmerNoGAPGroupDataGrid.SelectedItem;

                BuyingFacade.RegistrationBL()
                    .AddFarmerToGAPGroup(_registration.Crop,
                    _registration.FarmerCode,
                    _gapGroup.GAPGroupCode,
                    user_setting.User.Username);


                //RegistrationFarmerNoGroupDataGridBinding();
                MessageBox.Show("บันทึกเรียบร้อย");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<RegistrationFarmer> registerFarmerList = new List<RegistrationFarmer>();

                registerFarmerList = (from rf in BuyingFacade.RegistrationBL()
                                      .GetBySupplier(_gapGroup.Crop, _gapGroup.SupplierCode)
                                      .Where(gg => gg.GAPGroupCode == null &&
                                      gg.Farmer.Person.FirstName
                                      .Contains(FirstNameSearchTextBox.Text))
                                      where !(from gg in BuyingFacade.RegistrationBL()
                                              .GetByGAPGroup(_gapGroup.GAPGroupCode)
                                              select gg.FarmerCode)
                                              .Contains(rf.FarmerCode)
                                      select rf).ToList();

                RegistrationFarmerNoGAPGroupDataGrid.ItemsSource = null;
                RegistrationFarmerNoGAPGroupDataGrid.ItemsSource = registerFarmerList;

                TotalItems.Text = RegistrationFarmerNoGAPGroupDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
