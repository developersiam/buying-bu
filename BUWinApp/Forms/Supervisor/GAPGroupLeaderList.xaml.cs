﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BUWinApp.Helper;

namespace BUWinApp.Forms.Supervisor
{
    /// <summary>
    /// Interaction logic for GAPGroupLeaderList.xaml
    /// </summary>
    public partial class GAPGroupLeaderList : Window
    {
        short _crop;
        string _supplierCode;

        public GAPGroupLeaderList(short crop, string supplierCode)
        {
            InitializeComponent();

            _crop = crop;
            _supplierCode = supplierCode;
            SupplierCodeTextBlock.Text = _supplierCode;
            GAPGroupLeaderDataGridBinding();
        }

        public void GAPGroupLeaderDataGridBinding()
        {
            try
            {
                var query = from l in BuyingFacade.GapGroupBL()
                            .GetGAPGroupLeaderBySupplier(_supplierCode)
                            where !(from cl in BuyingFacade.GapGroupBL()
                                    .GetGAPGroupLeaderByCropBySupplier(_crop, _supplierCode)
                                    select cl.CitizenID).
                                    Contains(l.CitizenID)
                            select l;

                GAPGroupLeaderDataGrid.ItemsSource = null;
                GAPGroupLeaderDataGrid.ItemsSource = query.OrderBy(x => x.Person.FirstName);

                TotalItems.Text = GAPGroupLeaderDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var query = from l in BuyingFacade.GapGroupBL()
                            .GetGAPGroupLeaderBySupplier(_supplierCode)
                            where !(from cl in BuyingFacade.GapGroupBL()
                                    .GetGAPGroupLeaderByCropBySupplier(_crop,
                                    _supplierCode)
                                    select cl.CitizenID)
                                    .Contains(l.CitizenID)
                            select l;

                GAPGroupLeaderDataGrid.ItemsSource = null;
                GAPGroupLeaderDataGrid.ItemsSource = query
                    .Where(gg => gg.Person.FirstName
                    .Contains(FirstNameSearchTextBox.Text))
                    .OrderBy(x => x.Person.FirstName);

                TotalItems.Text = GAPGroupLeaderDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            GAPGroupLeaderDataGridBinding();
        }

        private void AddNewLeaderButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddGAPGroupLeader addLeader = new AddGAPGroupLeader(_crop, _supplierCode);
                addLeader.ShowDialog();

                GAPGroupLeaderDataGridBinding();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void AddLeaderToCropButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (GAPGroupLeaderDataGrid.SelectedIndex <= -1)
                    return;

                GAPGroupLeader leader = new GAPGroupLeader();
                leader = (GAPGroupLeader)GAPGroupLeaderDataGrid.SelectedItem;

                BuyingFacade.GapGroupBL().AddGAPGroupLeaderByCrop(_crop,
                    leader.CitizenID,
                    leader.SupplierCode,
                    user_setting.User.Username);

                GAPGroupLeaderDataGridBinding();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void DeleteLeaderButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (GAPGroupLeaderDataGrid.SelectedIndex <= -1)
                    return;

                GAPGroupLeader leaderFromDataGrid = new GAPGroupLeader();
                leaderFromDataGrid = (GAPGroupLeader)GAPGroupLeaderDataGrid.SelectedItem;


                GAPGroupLeader leader = new GAPGroupLeader();
                leader = BuyingFacade.GapGroupBL().GetGAPGroupLeaderByID(leaderFromDataGrid.CitizenID, leaderFromDataGrid.SupplierCode);


                if (MessageBox.Show("ท่านต้องการลบหัวหน้าชาวไร่หมายเลข " + leader.CitizenID + " ออกจากการเป็นหัวหน้าไร่ของซัพพลายเออร์ " +
                    leader.SupplierCode + " ใช่หรือไม่?", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning) == MessageBoxResult.No)
                    return;

                if (leader.GAPGroupLeaderByCrops.Count() > 0)
                {
                    MessageBox.Show("หัวหน้าชาวไร่หมายเลข " + leader.CitizenID + " มีข้อมูลการการเป็นหัวชาวไร่ในปีแต่ละปี จึงไม่สามารถลบข้อมูลนี้ได้",
                        "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                BuyingFacade.GapGroupBL().DeleteGAPGroupLeader(leader.CitizenID, leader.SupplierCode);

                GAPGroupLeaderDataGridBinding();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
