﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BUWinApp.Helper;

namespace BUWinApp.Forms.Supervisor
{
    /// <summary>
    /// Interaction logic for AddGAPGroupMember.xaml
    /// </summary>
    public partial class GAPGroupMember : Window
    {
        GAPGroup _gapGroup;

        public GAPGroupMember(GAPGroup gapGroup)
        {
            InitializeComponent();

            _gapGroup = new GAPGroup();
            _gapGroup = gapGroup;

            GAPGroupCodeTextBox.Text = _gapGroup.GAPGroupCode;
            SupplierCodeTextBox.Text = _gapGroup.SupplierCode;
            LeaderTextBox.Text = _gapGroup.GAPGroupLeaderByCrop.GAPGroupLeader.Person.Prefix +
                _gapGroup.GAPGroupLeaderByCrop.GAPGroupLeader.Person.FirstName + "   " +
                _gapGroup.GAPGroupLeaderByCrop.GAPGroupLeader.Person.LastName;

            GAPGroupMemberDataGridBinding();
        }

        public void GAPGroupMemberDataGridBinding()
        {
            try
            {
                GAPGroupMemberDataGrid.ItemsSource = null;
                GAPGroupMemberDataGrid.ItemsSource = BuyingFacade.RegistrationBL()
                    .GetByGAPGroup(_gapGroup.GAPGroupCode);

                TotalItems.Text = GAPGroupMemberDataGrid.Items.Count.ToString("N0");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void AddGAPGroupMemberButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ListOfFarmerNoGAPGroup listOfNoGAPGroup = new ListOfFarmerNoGAPGroup(_gapGroup);
                listOfNoGAPGroup.ShowDialog();

                GAPGroupMemberDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteMemberButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (GAPGroupMemberDataGrid.SelectedIndex <= -1)
                    return;

                RegistrationFarmer gapGroupMember = new RegistrationFarmer();
                gapGroupMember = (RegistrationFarmer)GAPGroupMemberDataGrid.SelectedItem;

                if (MessageBox.Show("ท่านต้องการลบชาวไร่รหัส " + gapGroupMember.FarmerCode + " ออกจากกลุ่ม " + 
                    gapGroupMember.GAPGroupCode + " ใช่หรือไม่?", 
                    "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BuyingFacade.RegistrationBL()
                    .RemoveGAPGroupMember(gapGroupMember.Crop, 
                    gapGroupMember.FarmerCode, 
                    user_setting.User.Username);

                GAPGroupMemberDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
