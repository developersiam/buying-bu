﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
//using PCSC;
using BUWinApp.Helper;

namespace BUWinApp.Forms.Supervisor
{
    /// <summary>
    /// Interaction logic for AddGAPGroupLeader.xaml
    /// </summary>
    public partial class AddGAPGroupLeader : Window
    {
        short _crop;
        string _supplierCode;

        public AddGAPGroupLeader(short crop, string supplierCode)
        {
            InitializeComponent();

            _crop = crop;
            _supplierCode = supplierCode;
            SupplierCodeTextBlock.Text = _supplierCode;
        }

        private bool VerifyPeopleID(string PID)
        {
            //ตรวจสอบว่าทุก ๆ ตัวอักษรเป็นตัวเลข
            if (PID.ToCharArray().All(c => char.IsNumber(c)) == false)
                return false;

            //ตรวจสอบว่าข้อมูลมีทั้งหมด 13 ตัวอักษร
            if (PID.Trim().Length != 13)
                return false;

            int sumValue = 0;

            for (int i = 0; i < PID.Length - 1; i++)
                sumValue += int.Parse(PID[i].ToString()) * (13 - i);

            int v = 11 - (sumValue % 11);


            //return PID[12].ToString() == v.ToString();

            if (v.ToString().Length > 1)
            {
                if (v.ToString()[1] == PID[12])
                    return true;
                else
                    return false;
            }
            else
            {
                if (v.ToString() == PID[12].ToString())
                    return true;
                else
                    return false;
            }
        }

        public void SearchAndBindData(string citizenID)
        {
            try
            {
                if (VerifyPeopleID(citizenID) == false)
                {
                    MessageBox.Show("รูปแบบเลขประจำตัวประชาชนของท่านไม่ถูกต้อง", "การแจ้งเตือน!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


                Person person = new Person();
                person = BuyingFacade.GapGroupBL().GetLeaderProfileByIDCard(citizenID);


                if (person == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลใดๆ ในระบบ!", "การแจ้งเตือน!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                else
                {
                    PrefixTextBox.Text = person.Prefix;
                    FirstNameTextBox.Text = person.FirstName;
                    LastNameTextBox.Text = person.LastName;
                    CitizenIDTextBox.Text = person.CitizenID;
                    DateOfBirthDatePicker.SelectedDate = person.BirthDate;
                    HomeAddressTextBox.Text = person.HouseNumber;
                    MooTextBox.Text = person.Village;
                    TumbonTextBox.Text = person.Tumbon;
                    AmphurTextBox.Text = person.Amphur;
                    ProvinceTextBox.Text = person.Province;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CheckLeaderProfileButton_Click(object sender, RoutedEventArgs e)
        {
            if (CitizenIDTextBox.Text == "")
            {
                MessageBox.Show("โปรดป้อนข้อมูล CitizenID", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            
            SearchAndBindData(CitizenIDTextBox.Text);
        }

        private void AddNewFarmerButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Begin "User validation"
                if (VerifyPeopleID(CitizenIDTextBox.Text) == false)
                {
                    MessageBox.Show("รหัสประจำตัวประชาชนไม่ถูกต้องตามรูปแบบที่กำหนด กรุณาลองใหม่อีกครั้ง", "การแจ้งเตือน", 
                        MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (PrefixTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุคำนำหน้าชื่อ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (FirstNameTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุชื่อ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (LastNameTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุนามสกุล", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (CitizenIDTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุหมายเลขประจำตัวประชาชน 13 หลัก", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (DateOfBirthDatePicker.Text == "")
                {
                    MessageBox.Show("โปรดระบุวันเดือนปีเกิด", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (HomeAddressTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุบ้านเลขที่", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (MooTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุหมู่ที่", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (TumbonTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุตำบล", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (AmphurTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุอำเภอ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (ProvinceTextBox.Text == "")
                {
                    MessageBox.Show("โปรดระบุจังหวัด", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }


                // End "User validation"
                Person person = new Person();
                person = BuyingFacade.GapGroupBL().GetLeaderProfileByIDCard(CitizenIDTextBox.Text);


                if (person == null)
                {
                    person.CitizenID = CitizenIDTextBox.Text;
                    person.Prefix = PrefixTextBox.Text;
                    person.FirstName = FirstNameTextBox.Text;
                    person.LastName = LastNameTextBox.Text;
                    person.BirthDate = Convert.ToDateTime(DateOfBirthDatePicker.Text);
                    person.HouseNumber = HomeAddressTextBox.Text;
                    person.Village = MooTextBox.Text;
                    person.Tumbon = TumbonTextBox.Text;
                    person.Amphur = AmphurTextBox.Text;
                    person.Province = ProvinceTextBox.Text;
                    person.ModifiedByUser = user_setting.User.Username;
                    person.LastModified = DateTime.Now;

                    BuyingFacade.GapGroupBL().AddNewPerson(person);
                }

                BuyingFacade.GapGroupBL().AddNewGAPGroupLeader(person, _supplierCode, user_setting.User.Username);

                MessageBox.Show("บันทึกข้อมูล Leader รายใหม่เข้าเป็น Leader ของ Supplier : " +
                    _supplierCode + " เรียบร้อยแล้ว", "", MessageBoxButton.OK, MessageBoxImage.Information);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PrefixTextBox.Text = "";
                FirstNameTextBox.Text = "";
                LastNameTextBox.Text = "";
                CitizenIDTextBox.Text = "";
                HomeAddressTextBox.Text = "";
                MooTextBox.Text = "";
                TumbonTextBox.Text = "";
                AmphurTextBox.Text = "";
                ProvinceTextBox.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
