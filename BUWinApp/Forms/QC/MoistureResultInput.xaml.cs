﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using DomainModel;
using BusinessLayer.Model;
using BusinessLayer.Helper;
using BUWinApp.Helper;

namespace BUWinApp.Forms.QC
{
    /// <summary>
    /// Interaction logic for MoistureResultInput.xaml
    /// </summary>
    public partial class MoistureResultInput : Page
    {
        m_Buying _buying;

        public MoistureResultInput()
        {
            InitializeComponent();

            _buying = new m_Buying();

            InputResultDatePicker.SelectedDate = DateTime.Now;
            MoistureResultDataGridBinding();
            BaleBarcodeTextBox.Focus();
        }

        void MoistureResultDataGridBinding()
        {
            try
            {
                var list = BuyingHelper.GetMoistureResultByResultDate(Convert.ToDateTime(InputResultDatePicker.SelectedDate));

                MoistureResultDetailsDataGrid.ItemsSource = null;
                MoistureResultDetailsDataGrid.ItemsSource = list.OrderByDescending(b => b.MoistureResultDate);

                NumberOfTestTextBlock.Text = list.Count().ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        void ClearForm()
        {
            try
            {
                BaleBarcodeTextBox.Clear();
                MoistureResultTextBox.Clear();
                BaleBarcodeTextBox.Focus();

                BuyingStationTextBox.Clear();
                ExtentionAgentTextBox.Clear();
                TruckNoTextBox.Clear();
                BuyingDateTextBox.Clear();
                GradeTextBox.Clear();
                WeightTextBox.Clear();
                FarmerCodeTextBox.Clear();

                MoistureResultDataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BaleBarcodeTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                _buying = BuyingHelper.GetByBaleBarcode(BaleBarcodeTextBox.Text);

                if (_buying == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลยาหมายเลขบาร์โค้ต " + BaleBarcodeTextBox.Text +
                        " นี้ในระบบ!!", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    BaleBarcodeTextBox.SelectAll();
                    return;
                }

                if (_buying.Grade == null)
                    throw new ArgumentException("ไม่พบข้อมูลเกรดซื้อของยาห่อนี้ ไม่สามารถบันทึกผลตรวจได้");

                if (_buying.TransportationDocumentCode == null)
                    throw new ArgumentException("ไม่พบข้อมูลรหัสใบนำส่งของยาห่อนี้ ไม่สามารถบันทึกผลตรวจได้");

                BuyingStationTextBox.Text = _buying.BuyingStationCode;
                ExtentionAgentTextBox.Text = _buying.SupplierCode;
                TruckNoTextBox.Text = _buying.TransportationDocument.TruckNumber;
                BuyingDateTextBox.Text = _buying.WeightDate.ToString();
                GradeTextBox.Text = _buying.Grade;
                WeightTextBox.Text = _buying.Weight.ToString();
                FarmerCodeTextBox.Text = _buying.FarmerCode;

                MoistureResultTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MoistureResultDetailsDataGrid.SelectedIndex <= -1)
                    return;

                m_Buying _buyingInfo = new m_Buying();
                _buyingInfo = (m_Buying)MoistureResultDetailsDataGrid.SelectedItem;

                if (_buyingInfo == null)
                {
                    MessageBox.Show("ไม่พบข้อมูลยาห่อนี้ในระบบ (หมายเลข " + _buyingInfo.BaleBarcode + ")",
                        "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (MessageBox.Show("ท่านต้องการลบผลตรวจความชื้นของห่อยาหมายเลข " + _buyingInfo.BaleBarcode + " นี้ใช่หรือไม่?",
                    "การแจ้เงตือน", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                BuyingFacade.BuyingBL().RemoveMoistureResult(_buyingInfo.BaleBarcode);

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void MoistureResultTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                if (BaleBarcodeTextBox.Text == "")
                {
                    MessageBox.Show("โปรดสแกนบาร์โค้ตก่อนบันทึก", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    BaleBarcodeTextBox.SelectAll();
                    return;
                }

                DateTime inputDate = Convert.ToDateTime(InputResultDatePicker.SelectedDate);
                inputDate = inputDate.AddHours(DateTime.Now.Hour);
                inputDate = inputDate.AddMinutes(DateTime.Now.Minute);
                inputDate = inputDate.AddSeconds(DateTime.Now.Second);

                BuyingFacade.BuyingBL()
                    .InputMoistureResult(BaleBarcodeTextBox.Text,
                    Convert.ToDecimal(MoistureResultTextBox.Text),
                    inputDate,
                    user_setting.User.Username);

                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            ClearForm();
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Escape)
                    ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ViewReportButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (InputResultDatePicker.SelectedDate.ToString() == "")
                {
                    MessageBox.Show("โปรดเลือกวันที่บันทึกผล", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                MoistureResultReport report = new MoistureResultReport(Convert.ToDateTime(InputResultDatePicker.SelectedDate));
                report.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
