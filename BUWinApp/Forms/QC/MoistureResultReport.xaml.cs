﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using BusinessLayer;
using Microsoft.Reporting.WinForms;
using BusinessLayer.Helper;

namespace BUWinApp.Forms.QC
{
    /// <summary>
    /// Interaction logic for MoistureResultPreviewReport.xaml
    /// </summary>
    public partial class MoistureResultReport : Window
    {
        DateTime _inputResultDate;

        public MoistureResultReport(DateTime inputResultDate)
        {
            InitializeComponent();
            _inputResultDate = inputResultDate;
            InputResultDatePicker.SelectedDate = inputResultDate;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            BuyingStationComboBox.ItemsSource = null;
            BuyingStationComboBox.ItemsSource = BuyingFacade.BuyingStationBL()
                .GetAll()
                .OrderBy(bs => bs.StationName);
        }

        private void DisplayButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MoistureDailyReportReportViewer.Reset();
                ReportDataSource sp_GetMoistureResultDetailsDataSource = new ReportDataSource();

                if (ShowAllCheckBox.IsChecked == true)
                {
                    if (BuyingStationComboBox.SelectedIndex <= -1)
                    {
                        MessageBox.Show("โปรดเลือกลานรับซื้อ", "การแจ้งเตือน", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }

                    string buyingStationCode = BuyingStationComboBox.SelectedValue.ToString();

                    sp_GetMoistureResultDetailsDataSource.Value = BuyingHelper
                        .GetMoistureResultByResultDate(_inputResultDate)
                        .Where(m => m.BuyingStationCode == buyingStationCode)
                        .OrderBy(m => m.Grade);
                }
                else
                {
                    sp_GetMoistureResultDetailsDataSource.Value = BuyingHelper
                        .GetMoistureResultByResultDate(_inputResultDate)
                        .OrderBy(m => m.Grade);
                }

                sp_GetMoistureResultDetailsDataSource.Name = "sp_GetMoistureResultDetails";
                MoistureDailyReportReportViewer.LocalReport.ReportEmbeddedResource = "BUWinApp.RDLCReport.RPTBUY003.rdlc";
                MoistureDailyReportReportViewer.LocalReport.DataSources.Add(sp_GetMoistureResultDetailsDataSource);

                MoistureDailyReportReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
