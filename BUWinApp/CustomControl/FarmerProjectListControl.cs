﻿using BusinessLayer.Helper;
using BusinessLayer.Model;
using BUWinApp.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BUWinApp.CustomControl
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:BUWinApp.CustomControl"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:BUWinApp.CustomControl;assembly=BUWinApp.CustomControl"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Browse to and select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:FarmerProjectListControl/>
    ///
    /// </summary>
    public class FarmerProjectListControl : Control
    {
        static FarmerProjectListControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(FarmerProjectListControl),
                new FrameworkPropertyMetadata(typeof(FarmerProjectListControl)));
        }



        public string FarmerCode
        {
            get { return (string)GetValue(FarmerCodeProperty); }
            set { SetValue(FarmerCodeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FarmerCode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FarmerCodeProperty =
            DependencyProperty.Register(
                nameof(FarmerCode),
                typeof(string),
                typeof(FarmerProjectListControl),
                new UIPropertyMetadata(null, new PropertyChangedCallback(FarmerCodeChanged)));

        private static void FarmerCodeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                var me = (FarmerProjectListControl)d;
                if (string.IsNullOrEmpty(me.FarmerCode))
                    me.FarmerProjectList = null;
                else
                    me.FarmerProjectList = FarmerProjectHelper
                            .GetQuotaAndSoldByFarmerVersion2(user_setting.Crop.Crop1, me.FarmerCode);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }



        public List<m_FarmerProject> FarmerProjectList
        {
            get { return (List<m_FarmerProject>)GetValue(FarmerProjectListProperty); }
            set { SetValue(FarmerProjectListProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FarmerProjectList.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FarmerProjectListProperty =
            DependencyProperty.Register(
                nameof(FarmerProjectList),
                typeof(List<m_FarmerProject>),
                typeof(FarmerProjectListControl),
                new PropertyMetadata(default(List<m_FarmerProject>)));


    }
}
