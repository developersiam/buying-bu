﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BusinessLayer;

namespace BUWinApp.CustomControl
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:BUWinApp.CustomControl"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:BUWinApp.CustomControl;assembly=BUWinApp.CustomControl"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Browse to and select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:TransportationControl/>
    ///
    /// </summary>
    public class TransportationControl : Control
    {
        static TransportationControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(TransportationControl),
                new FrameworkPropertyMetadata(typeof(TransportationControl)));
        }



        public string TransportationCode
        {
            get { return (string)GetValue(TransportationCodeProperty); }
            set { SetValue(TransportationCodeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TransportationCode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TransportationCodeProperty =
            DependencyProperty.Register(
                nameof(TransportationCode),
                typeof(string),
                typeof(TransportationControl),
                new UIPropertyMetadata(null, TransportationChanged));

        private static void TransportationChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var me = (TransportationControl)d;

            if (string.IsNullOrEmpty(me.TransportationCode))
            {
                me.TruckNo = "";
                me.TotalBale = 0;
                me.MaximumWeight = 0;
                me.LoadingWeight = 0;
                me.RemainingWeight = 0;
                return;
            }

            var model = BusinessLayer.Helper.TransportationHelper
                .GetByTransportationCode(me.TransportationCode);

            if (model == null)
            {
                me.TruckNo = "";
                me.TotalBale = 0;
                me.MaximumWeight = 0;
                me.LoadingWeight = 0;
                me.RemainingWeight = 0;
            }
            else
            {
                me.TruckNo = model.TruckNumber;
                me.TotalBale = model.TotalBale;
                me.MaximumWeight = model.MaximumWeight;
                me.LoadingWeight = Convert.ToDecimal(model.TotalWeight);
                me.RemainingWeight = Convert.ToDecimal(model.MaximumWeight - model.TotalWeight);
            }
        }

        public string TruckNo
        {
            get { return (string)GetValue(TruckNoProperty); }
            set { SetValue(TruckNoProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TruckNo.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TruckNoProperty =
            DependencyProperty.Register(
                nameof(TruckNo),
                typeof(string),
                typeof(TransportationControl),
                new PropertyMetadata(default(string)));



        public int TotalBale
        {
            get { return (int)GetValue(TotalBaleProperty); }
            set { SetValue(TotalBaleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TotalBale.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TotalBaleProperty =
            DependencyProperty.Register(
                nameof(TotalBale),
                typeof(int),
                typeof(TransportationControl),
                new PropertyMetadata(default(int)));



        public int MaximumWeight
        {
            get { return (int)GetValue(MaximumWeightProperty); }
            set { SetValue(MaximumWeightProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MaximumWeight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MaximumWeightProperty =
            DependencyProperty.Register(
                nameof(MaximumWeight),
                typeof(int),
                typeof(TransportationControl),
                new PropertyMetadata(default(int)));



        public decimal LoadingWeight
        {
            get { return (decimal)GetValue(LoadingWeightProperty); }
            set { SetValue(LoadingWeightProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LoadingWeight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LoadingWeightProperty =
            DependencyProperty.Register(
                nameof(LoadingWeight),
                typeof(decimal),
                typeof(TransportationControl),
                new PropertyMetadata(default(decimal)));



        public decimal RemainingWeight
        {
            get { return (decimal)GetValue(RemainingWeightProperty); }
            set { SetValue(RemainingWeightProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RemainingWeight.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RemainingWeightProperty =
            DependencyProperty.Register(
                nameof(RemainingWeight),
                typeof(decimal),
                typeof(TransportationControl),
                new PropertyMetadata(default(decimal)));


    }
}
