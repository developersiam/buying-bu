﻿using BUWinApp.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BUWinApp.CustomControl
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:BUWinApp.CustomControl"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:BUWinApp.CustomControl;assembly=BUWinApp.CustomControl"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Browse to and select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:FarmerProjectControl/>
    ///
    /// </summary>
    public class FarmerProjectControl : Control
    {
        static FarmerProjectControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(FarmerProjectControl),
                new FrameworkPropertyMetadata(typeof(FarmerProjectControl)));
        }



        public string ProjectType
        {
            get { return (string)GetValue(ProjectTypeProperty); }
            set { SetValue(ProjectTypeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ProjectType.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ProjectTypeProperty =
            DependencyProperty.Register(
                "ProjectType",
                typeof(string),
                typeof(FarmerProjectControl),
                new UIPropertyMetadata(null, FarmerCodeChanged));



        public string FarmerCode
        {
            get { return (string)GetValue(FarmerCodeProperty); }
            set { SetValue(FarmerCodeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FarmerCode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FarmerCodeProperty =
            DependencyProperty.Register(
                "FarmerCode",
                typeof(string),
                typeof(FarmerProjectControl),
                new PropertyMetadata(default(string)));

        private static void FarmerCodeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Binding((FarmerProjectControl)d);
        }



        public int Quota
        {
            get { return (int)GetValue(QuotaProperty); }
            set { SetValue(QuotaProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Quota.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty QuotaProperty =
            DependencyProperty.Register(
                "Quota",
                typeof(int),
                typeof(FarmerProjectControl),
                new PropertyMetadata(default(int)));



        public decimal Sold
        {
            get { return (decimal)GetValue(SoldProperty); }
            set { SetValue(SoldProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Sold.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SoldProperty =
            DependencyProperty.Register(
                "Sold",
                typeof(decimal),
                typeof(FarmerProjectControl),
                new PropertyMetadata(default(decimal)));



        public decimal Remaining
        {
            get { return (decimal)GetValue(RemainingProperty); }
            set { SetValue(RemainingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Remainning.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RemainingProperty =
            DependencyProperty.Register(
                "Remaining",
                typeof(decimal),
                typeof(FarmerProjectControl),
                new PropertyMetadata(default(decimal)));



        public int ExtraQuota
        {
            get { return (int)GetValue(ExtraQuotaProperty); }
            set { SetValue(ExtraQuotaProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ExtraQuota.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ExtraQuotaProperty =
            DependencyProperty.Register(
                "ExtraQuota",
                typeof(int),
                typeof(FarmerProjectControl),
                new PropertyMetadata(default(int)));



        public decimal ExtraSold
        {
            get { return (decimal)GetValue(ExtraSoldProperty); }
            set { SetValue(ExtraSoldProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ExtraSold.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ExtraSoldProperty =
            DependencyProperty.Register(
                "ExtraSold",
                typeof(decimal),
                typeof(FarmerProjectControl),
                new PropertyMetadata(default(decimal)));



        public decimal ExtraRemaining
        {
            get { return (decimal)GetValue(ExtraRemainingProperty); }
            set { SetValue(ExtraRemainingProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ExtraRemainning.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ExtraRemainingProperty =
            DependencyProperty.Register(
                "ExtraRemaining",
                typeof(decimal),
                typeof(FarmerProjectControl),
                new PropertyMetadata(default(decimal)));


        private static void Binding(FarmerProjectControl me)
        {
            if (string.IsNullOrEmpty(me.FarmerCode) || string.IsNullOrEmpty(me.ProjectType))
            {
                me.Quota = 0;
                me.Sold = 0;
                me.Remaining = 0;
                me.ExtraQuota = 0;
                me.ExtraSold = 0;
                me.ExtraRemaining = 0;
            }
            else
            {
                var model = BusinessLayer.Helper.FarmerProjectHelper
                    .GetQuotaAndSoldByFarmerVersion2(Helper.user_setting.Crop.Crop1, me.FarmerCode)
                    .SingleOrDefault(x => x.ProjectType == me.ProjectType);

                if (model == null)
                {
                    me.Quota = 0;
                    me.Sold = 0;
                    me.Remaining = 0;
                    me.ExtraQuota = 0;
                    me.ExtraSold = 0;
                    me.ExtraRemaining = 0;
                }
                else
                {
                    me.Quota = Convert.ToInt32(model.TotalQuota);
                    me.Sold = Convert.ToDecimal(model.Sold);
                    me.Remaining = Convert.ToDecimal(model.Balance);
                    me.ExtraQuota = Convert.ToInt32(model.ExtraQuota);
                    me.ExtraSold = Convert.ToDecimal(model.SoldInExtra);
                    me.ExtraRemaining = Convert.ToDecimal(model.BalanceExtra);

                    //if (me.Remaining <= 10 && me.Remaining > 0 && me.ExtraQuota <= 0)
                    //    MessageBoxHelper.Warning("ชาวไร่ขายยาใกล้เต๊มโควต้าแล้ว (อีกไม่เกิน 10 กก.จะเต๊มโควต้า)");
                }
            }
        }
    }
}
