﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BusinessLayer;

namespace BUWinApp.CustomControl
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:BUWinApp.CustomControl"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:BUWinApp.CustomControl;assembly=BUWinApp.CustomControl"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Browse to and select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:FarmerInfoControl/>
    ///
    /// </summary>
    public class FarmerInfoControl : Control
    {
        static FarmerInfoControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(FarmerInfoControl),
                new FrameworkPropertyMetadata(typeof(FarmerInfoControl)));
        }

        static void FarmerCodeChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var me = d as FarmerInfoControl;
            var profile = BuyingFacade.RegistrationBL()
                .GetSingle(Helper.user_setting.Crop.Crop1, me.FarmerCode);

            if (profile == null)
            {
                me.FarmerName = "";
                me.FarmerAddress = "";
                me.GapGroupCode = "";
                me.ContractCode = "";
                me.QuotaFromSignContract = 0;
            }
            else
            {
                var gapGroupCode = BuyingFacade.GAPGroupMember2020BL()
                    .GetSingleByCitizenID(profile.Crop, profile.Farmer.CitizenID);
                if (gapGroupCode != null)
                    me.GapGroupCode = gapGroupCode.GAPGroupCode;

                me.FarmerName = profile.Farmer.Person.Prefix +
                    profile.Farmer.Person.FirstName + " " +
                    profile.Farmer.Person.LastName;
                me.FarmerAddress = profile.Farmer.Person.HouseNumber + " หมู่" +
                    profile.Farmer.Person.Village + " ต." +
                    profile.Farmer.Person.Tumbon + " อ." +
                    profile.Farmer.Person.Amphur + " จ." +
                    profile.Farmer.Person.Province;
                me.ContractCode = profile.GAPContractCode;
                me.QuotaFromSignContract = Convert.ToInt16(profile.QuotaFromSignContract);
            }
        }

        public string FarmerCode
        {
            get { return (string)GetValue(FarmerCodeProperty); }
            set { SetValue(FarmerCodeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FarmerCode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FarmerCodeProperty =
            DependencyProperty.Register(
                nameof(FarmerCode),
                typeof(string),
                typeof(FarmerInfoControl),
                new UIPropertyMetadata(null, FarmerCodeChanged));


        public string FarmerName
        {
            get { return (string)GetValue(FarmerNameProperty); }
            set { SetValue(FarmerNameProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FarmerName.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FarmerNameProperty =
            DependencyProperty.Register(
                nameof(FarmerName),
                typeof(string),
                typeof(FarmerInfoControl),
                new PropertyMetadata(default(string)));


        public string FarmerAddress
        {
            get { return (string)GetValue(FarmerAddressProperty); }
            set { SetValue(FarmerAddressProperty, value); }
        }

        // Using a DependencyProperty as the backing store for FarmerAddress.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty FarmerAddressProperty =
            DependencyProperty.Register(
                nameof(FarmerAddress),
                typeof(string),
                typeof(FarmerInfoControl),
                new PropertyMetadata(default(string)));


        public string ContractCode
        {
            get { return (string)GetValue(ContractCodeProperty); }
            set { SetValue(ContractCodeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ContractCode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ContractCodeProperty =
            DependencyProperty.Register(
                nameof(ContractCode),
                typeof(string),
                typeof(FarmerInfoControl),
                new PropertyMetadata(default(string)));



        public int QuotaFromSignContract
        {
            get { return (int)GetValue(QuotaFromSignContractProperty); }
            set { SetValue(QuotaFromSignContractProperty, value); }
        }

        // Using a DependencyProperty as the backing store for QuotaFromSignContract.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty QuotaFromSignContractProperty =
            DependencyProperty.Register(
                nameof(QuotaFromSignContract),
                typeof(int),
                typeof(FarmerInfoControl),
                new PropertyMetadata(default(int)));



        public string GapGroupCode
        {
            get { return (string)GetValue(GapGroupCodeProperty); }
            set { SetValue(GapGroupCodeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for GapGroupCode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty GapGroupCodeProperty =
            DependencyProperty.Register(
                nameof(GapGroupCode),
                typeof(string),
                typeof(FarmerInfoControl),
                new PropertyMetadata(default(string)));

    }
}
