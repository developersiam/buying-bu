﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BusinessLayer;

namespace BUWinApp.CustomControl
{
    /// <summary>
    /// Follow steps 1a or 1b and then 2 to use this custom control in a XAML file.
    ///
    /// Step 1a) Using this custom control in a XAML file that exists in the current project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:BUWinApp.CustomControl"
    ///
    ///
    /// Step 1b) Using this custom control in a XAML file that exists in a different project.
    /// Add this XmlNamespace attribute to the root element of the markup file where it is 
    /// to be used:
    ///
    ///     xmlns:MyNamespace="clr-namespace:BUWinApp.CustomControl;assembly=BUWinApp.CustomControl"
    ///
    /// You will also need to add a project reference from the project where the XAML file lives
    /// to this project and Rebuild to avoid compilation errors:
    ///
    ///     Right click on the target project in the Solution Explorer and
    ///     "Add Reference"->"Projects"->[Browse to and select this project]
    ///
    ///
    /// Step 2)
    /// Go ahead and use your control in the XAML file.
    ///
    ///     <MyNamespace:BuyingDocumentControl/>
    ///
    /// </summary>
    public class BuyingDocumentControl : Control
    {
        static BuyingDocumentControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(BuyingDocumentControl),
                new FrameworkPropertyMetadata(typeof(BuyingDocumentControl)));
        }

        public string DocumentCode
        {
            get { return (string)GetValue(DocumentCodeProperty); }
            set { SetValue(DocumentCodeProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DocumentCode.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DocumentCodeProperty =
            DependencyProperty.Register(
                nameof(DocumentCode),
                typeof(string),
                typeof(BuyingDocumentControl),
                new UIPropertyMetadata(null, DocumentCodePropertyChanged));

        private static void DocumentCodePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            try
            {
                var me = d as BuyingDocumentControl;
                if(string.IsNullOrEmpty(me.DocumentCode))
                {
                    me.TotalBale = 0;
                    me.RecordedGradeBale = 0;
                    me.RejectedBale = 0;
                    me.TransportationBale = 0;
                    me.IsFinish = false;
                    return;
                }

                var spliter = me.DocumentCode.Split('-');
                if (spliter.Count() <= 1)
                    return;

                var crop = Convert.ToInt16(spliter[0]);
                var stationCode = spliter[1];
                var farmerCode = spliter[2];
                var documentNumber = Convert.ToInt16(spliter[3]);

                var document = BusinessLayer.Helper.BuyingDocumentHelper
                    .GetSingle(crop, stationCode, farmerCode, documentNumber);

                if (document != null)
                {
                    me.TotalBale = document.TotalBale;
                    me.RecordedGradeBale = document.TotalGrade;
                    me.RejectedBale = document.TotalReject;
                    me.TransportationBale = document.TotalLoadTruck;
                    me.IsFinish = document.IsFinish;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int TotalBale
        {
            get { return (int)GetValue(TotalBaleProperty); }
            set { SetValue(TotalBaleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TotalBale.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TotalBaleProperty =
            DependencyProperty.Register(
                nameof(TotalBale),
                typeof(int),
                typeof(BuyingDocumentControl),
                new PropertyMetadata(default(int)));


        public int RecordedGradeBale
        {
            get { return (int)GetValue(RecordedGradeBaleProperty); }
            set { SetValue(RecordedGradeBaleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RecordedGradeBale.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RecordedGradeBaleProperty =
            DependencyProperty.Register(
                nameof(RecordedGradeBale),
                typeof(int),
                typeof(BuyingDocumentControl),
                new PropertyMetadata(default(int)));


        public int RejectedBale
        {
            get { return (int)GetValue(RejectedBaleProperty); }
            set { SetValue(RejectedBaleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for RejectedBale.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty RejectedBaleProperty =
            DependencyProperty.Register(
                nameof(RejectedBale),
                typeof(int),
                typeof(BuyingDocumentControl),
                new PropertyMetadata(default(int)));



        public int TransportationBale
        {
            get { return (int)GetValue(TransportationBaleProperty); }
            set { SetValue(TransportationBaleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TransportationBale.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TransportationBaleProperty =
            DependencyProperty.Register(
                nameof(TransportationBale),
                typeof(int),
                typeof(BuyingDocumentControl),
                new PropertyMetadata(default(int)));


        public bool IsFinish
        {
            get { return (bool)GetValue(IsFinishProperty); }
            set { SetValue(IsFinishProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsFinish.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsFinishProperty =
            DependencyProperty.Register(
                nameof(IsFinish),
                typeof(bool),
                typeof(BuyingDocumentControl),
                new PropertyMetadata(default(bool)));



        public string TotalBaleStr
        {
            get { return (string)GetValue(TotalBaleStrProperty); }
            set { SetValue(TotalBaleStrProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TotalBaleStr.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TotalBaleStrProperty =
            DependencyProperty.Register(
                nameof(TotalBaleStr),
                typeof(string),
                typeof(BuyingDocumentControl),
                new PropertyMetadata(default(string)));


    }
}
